
/** \file pfe_DynamicState.h
 * \brief Defines the class pfe::DynamicState
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <petscvec.h>

namespace pfe
{
  //! \ingroup time
  class DynamicState
  {
  public:

    Vec U;     //!< Displacement
    Vec V;     //!< Velocity
    Vec A;     //!< Acceleration
    
    //!< Constructor
    DynamicState(const int ndof);

    //! Destructor
    ~DynamicState();

    //! Disable copy and assignment
    DynamicState(const DynamicState&) = delete;
    DynamicState operator=(const DynamicState&) = delete;

    //! Destroy
    void Destroy();

  private:
    bool isDestroyed;
  };
    
  
}
