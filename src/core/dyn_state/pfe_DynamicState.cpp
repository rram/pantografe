
/** \file pfe_DynamicState.cpp
 * \brief Implements the class pfe::DynamicState
 * \author Ramsharan Rangarajan
 */

#include <pfe_DynamicState.h>
#include <fstream>
#include <cassert>

namespace pfe
{
  // Constructor
  DynamicState::DynamicState(const int ndof)
    :isDestroyed(false)
  {
    PetscErrorCode ierr;
    ierr = VecCreate(PETSC_COMM_WORLD, &U);    CHKERRV(ierr);
    ierr = VecSetSizes(U, PETSC_DECIDE, ndof); CHKERRV(ierr);
    ierr = VecSetFromOptions(U);               CHKERRV(ierr);
    ierr = VecDuplicate(U, &V);                CHKERRV(ierr);
    ierr = VecDuplicate(U, &A);                CHKERRV(ierr);
    ierr = VecZeroEntries(U);                  CHKERRV(ierr);
    ierr = VecZeroEntries(V);                  CHKERRV(ierr);
    ierr = VecZeroEntries(A);                  CHKERRV(ierr);
    
  }

  // Destructor
  DynamicState::~DynamicState()
  { assert(isDestroyed); }

  // Destroy
  void DynamicState::Destroy()
  {
    assert(isDestroyed==false);
    PetscErrorCode ierr;
    PetscBool isFinalized;
    ierr = PetscFinalized(&isFinalized); CHKERRV(ierr);
    assert(isFinalized==PETSC_FALSE);

    ierr = VecDestroy(&U); CHKERRV(ierr);
    ierr = VecDestroy(&V); CHKERRV(ierr);
    ierr = VecDestroy(&A); CHKERRV(ierr);
    isDestroyed = true;
  }

  
}
