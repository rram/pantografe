
/** \file pfe_TimeIntegrator.h
 * \brief Defines the class pfe::TimeIntegrator
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_DynamicState.h>
#include <pfe_LinearSolver.h>
#include <vector>
#include <petscmat.h>
#include <cassert>

namespace pfe
{
  //! \ingroup time
  class TimeIntegrator
  {
  public:
    //! Constructor
    TimeIntegrator(const double BETA, const double GAMMA, const int ndof, const double delta);

    //! Destructor
    virtual ~TimeIntegrator();

    //! Destroy data structures
    void Destroy();

    //! Set operators
    void SetOperators(const Mat& mass, const Mat& stiffness, const std::vector<int> dbc_dofs);

    //! Set initial conditions
    void SetInitialConditions(const double t0, const double* disp,
			      const double* vel, const double* accn);

    //! Get the evaluation time
    virtual double GetNextEvaluationTime() const = 0;

    //! Main functionality
    void Advance(const Vec& Fvec);

    //! Access the solution
    inline DynamicState& GetState()
    {
      assert(isInitialized==true && isDestroyed==false);
      return state;
    }

    //! Access the time
    inline double GetTime() const
    { return time; }

    //! Access the timestep
    inline double GetTimestep() const
    { return dt; }

  protected:

    //! Algorithm specific matrix operator
    virtual void ComputeAlgorithmicLHSMat(const Mat& m, const Mat& k, Mat& mat) = 0;

    //! Algorithm specific RHS vector
    virtual void ComputeAlgorithmicRHSVec(const Vec& f, const Vec& kdn, const Vec& kdnp,
					  const Vec& man, Vec& rhs) = 0;

  private:
    bool isInitialized; //!< have initial conditions been set
    bool isSet;         //!< have the operators been set
    bool isDestroyed;   //!< state of petsc data structures

    const double beta;  //!< Algorithmic parameter
    const double gamma; //!< Algorithmic parameter

    const double dt;    //!< time step
    const int    nDof;  //!< number of dofs
    double       time;  //!< current time
    std::vector<int> dirichlet_dofs; //!< dirichlet dofs

    DynamicState state;          //!< Current dynamic state
    Mat          M;              //!< mass
    Mat          K;              //!< stiffness
    Mat          LHSmat;         //!< composite matrix
    Vec          RHS;            //!< RHS vector
    Vec          Anp1;           //!< Acceleration
    Vec          dnp;            //!< predictor displacement
    Vec          MAn, Kdn, Kdnp; //!< Intermediate vectors
    LinearSolver linSolver;      //!< Linear solver
  };
  
}
