
/** \file pfe_HHTAlpha.cpp
 * \brief Implements the class pfe::HHTAlpha
 * \author Ramsharan Rangarajan
 */

#include <pfe_HHTAlpha.h>

namespace pfe
{
  // Constructor
  HHTAlpha::HHTAlpha(const double aleph, const int ndof, const double delta)
    :TimeIntegrator(0.25*(1.-aleph)*(1.-aleph), 0.5-aleph, ndof, delta),
     alpha(aleph),
     BETA(0.25*(1.-alpha)*(1.-alpha)),
     GAMMA(0.5-alpha),
     tstep(delta) {}

  
  // Algorithm specific matrix operator
  void HHTAlpha::ComputeAlgorithmicLHSMat(const Mat& m, const Mat& k, Mat& mat)
  {
    // A = M + (1+alpha)*beta*dt^2 K
    PetscErrorCode ierr;
    ierr = MatDuplicate(m, MAT_COPY_VALUES, &mat);                                  CHKERRV(ierr);
    ierr = MatSetOption(mat, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);                CHKERRV(ierr);
    ierr = MatAXPY(mat, (1.+alpha)*BETA*tstep*tstep, k, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);

    // done
    return;
  }


  // Algorithm specific RHS vector
  void HHTAlpha::ComputeAlgorithmicRHSVec(const Vec& f, const Vec& kdn,
					  const Vec& kdnp, const Vec& man, Vec& rhs)
  {
    // Solve rhs = F-(1+alpha)*Kdnp+alpha*Kdn
    PetscErrorCode ierr;
    ierr = VecCopy(f, rhs);                 CHKERRV(ierr);
    ierr = VecAXPY(rhs, -(1.+alpha), kdnp); CHKERRV(ierr);
    ierr = VecAXPY(rhs, alpha, kdn);        CHKERRV(ierr);

    // done
    return;
  }

}
