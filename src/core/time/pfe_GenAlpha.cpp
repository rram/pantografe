
/** \file pfe_GenAlpha.cpp
 * \brief Implements the class pfe::GenAlpha
 * \author Ramsharan Rangarajan
 */

#include <pfe_GenAlpha.h>

namespace pfe
{
  // Constructor
  GenAlpha::GenAlpha(const double rho, const int ndof, const double delta)
    :
    TimeIntegrator(0.25*std::pow(1.+(1.-rho)/(1.+rho),2.),
		   0.5+(1.-rho)/(1.+rho),
		   ndof, delta),
    alpha_m((2.*rho-1.)/(rho+1.)),
    alpha_f(rho/(1.+rho)),
    BETA(0.25*(1.-alpha_m+alpha_f)*(1.-alpha_m+alpha_f)),
    GAMMA(0.5-alpha_m+alpha_f),
    tstep(delta) {}

  // Get the next evaluation time
  double GenAlpha::GetNextEvaluationTime() const
  {
    const double t = GetTime();
    return (1.-alpha_f)*(t+tstep) + alpha_f*t;
  }

  // Algorithm specific matrix operator
  void GenAlpha::ComputeAlgorithmicLHSMat(const Mat& m, const Mat& k, Mat& mat)
  {
    // mat = (1-alpha_m)*M + (1-alpha_f)*beta*dt^2 K
    PetscErrorCode ierr;
    ierr = MatDuplicate(m, MAT_COPY_VALUES, &mat);                              CHKERRV(ierr);
    ierr = MatScale(mat, 1.-alpha_m);                                           CHKERRV(ierr);
    ierr = MatSetOption(mat, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);            CHKERRV(ierr);
    ierr = MatAXPY(mat, (1.-alpha_f)*BETA*tstep*tstep, k, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);
    
    // done
    return;
  }

  // Algorithm specific RHS vector
  void GenAlpha::ComputeAlgorithmicRHSVec(const Vec& f, const Vec& kdn,
					  const Vec& kdnp, const Vec& man, Vec& rhs)
  {
    // rhs = F - alpha_m*M*An -(1-alpha_f)*K*dnp - alpha_f*K*dn    
    PetscErrorCode ierr;
    ierr = VecCopy(f, rhs);                   CHKERRV(ierr);
    ierr = VecAXPY(rhs, -alpha_m, man);       CHKERRV(ierr);
    ierr = VecAXPY(rhs, -(1.-alpha_f), kdnp); CHKERRV(ierr);
    ierr = VecAXPY(rhs, -alpha_f, kdn);       CHKERRV(ierr);

    // done
    return;
  }
  
}
