
/** \file pfe_HHTAlpha.h
 * \brief Defines the class pfe::HHTAlpha
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_TimeIntegrator.h>

namespace pfe
{
  //! \ingroup time
  class HHTAlpha: public TimeIntegrator
  {
  public:
    //! Constructor
    HHTAlpha(const double aleph, const int ndof, const double delta);
    
    //! Disable copy and assignment
    HHTAlpha(const HHTAlpha&) = delete;
    HHTAlpha operator=(const HHTAlpha&) = delete;

    //! Destructor
    inline ~HHTAlpha() {}

    //! Get the next evaluation time
    inline double GetNextEvaluationTime() const override
    { return GetTime()+tstep; }
    
  protected:
    //! Algorithm specific matrix operator
    void ComputeAlgorithmicLHSMat(const Mat& m, const Mat& k, Mat& mat) override;

    //! Algorithm specific RHS vector
    void ComputeAlgorithmicRHSVec(const Vec& f, const Vec& kdn, const Vec& kdnp,
				  const Vec& man, Vec& rhs) override;

  private:
    const double alpha;
    const double BETA, GAMMA;
    const double tstep;
  };
  
}
