
/** \file pfe_TimeIntegrator.cpp
 * \brief Implements the class pfe::TimeIntegrator
 * \author Ramsharan Rangarajan
 */

#include <pfe_TimeIntegrator.h>

namespace pfe
{
  // Constructor
  TimeIntegrator::TimeIntegrator(const double BETA, const double GAMMA,
				 const int ndof, const double delta)
    :isInitialized(false),
     isSet(false),
     isDestroyed(false),
     beta(BETA),
     gamma(GAMMA),
     time(0.),
     dt(delta),
     nDof(ndof),
     state(nDof),
     dirichlet_dofs{} {}

  // Destructor
  TimeIntegrator::~TimeIntegrator()
  {
    assert(isDestroyed==true);
  }

  // Destroy data structures
  void TimeIntegrator::Destroy()
  {
    assert(isDestroyed==false);
    state.Destroy();
    if(isSet)
      {
	PetscErrorCode ierr;
	ierr = MatDestroy(&M);      CHKERRV(ierr);
	ierr = MatDestroy(&K);      CHKERRV(ierr);
	ierr = MatDestroy(&LHSmat); CHKERRV(ierr);
	ierr = VecDestroy(&Anp1);   CHKERRV(ierr);
	ierr = VecDestroy(&RHS);    CHKERRV(ierr);
	ierr = VecDestroy(&dnp);    CHKERRV(ierr);
	ierr = VecDestroy(&MAn);    CHKERRV(ierr);
	ierr = VecDestroy(&Kdn);    CHKERRV(ierr);
	ierr = VecDestroy(&Kdnp);   CHKERRV(ierr);
      }
    linSolver.Destroy();
    isDestroyed = true;
  }


  // Set operators
  void TimeIntegrator::SetOperators(const Mat& mass, const Mat& stiff,
				    const std::vector<int> dbc_dofs)
  {
    // Copy M & K
    PetscErrorCode ierr;
    ierr = MatDuplicate(mass,  MAT_COPY_VALUES, &M); CHKERRV(ierr);
    ierr = MatDuplicate(stiff, MAT_COPY_VALUES, &K); CHKERRV(ierr);
    dirichlet_dofs = dbc_dofs;

    // algorithmic operator
    ComputeAlgorithmicLHSMat(M, K, LHSmat);

    // Set dirichlet dofs in operator
    if(!dirichlet_dofs.empty())
      {
	const int nbcs = static_cast<int>(dirichlet_dofs.size());
	ierr = MatZeroRows(LHSmat, nbcs, dirichlet_dofs.data(), 1., PETSC_NULLPTR, PETSC_NULLPTR); CHKERRV(ierr);
      }

    // Set operators in the linear solver
    linSolver.SetOperator(LHSmat);

    // allocate vectors
    ierr = VecCreateSeq(PETSC_COMM_WORLD, nDof, &RHS); CHKERRV(ierr);
    ierr = VecDuplicate(RHS, &Anp1);                   CHKERRV(ierr);
    ierr = VecDuplicate(RHS, &dnp);                    CHKERRV(ierr);
    ierr = VecDuplicate(RHS, &MAn);                    CHKERRV(ierr);
    ierr = VecDuplicate(RHS, &Kdn);                    CHKERRV(ierr);
    ierr = VecDuplicate(RHS, &Kdnp);                   CHKERRV(ierr);

    // done
    isSet       = true;
    isDestroyed = false;
    return;
  }

  // Initial conditions
  void TimeIntegrator::SetInitialConditions(const double t0,
					    const double* disp,
					    const double* vel,
					    const double* accn)
  {
    time = t0;
    assert(isDestroyed==false && isInitialized==false);

    // Aliases
    Vec& U = state.U;
    Vec& V = state.V;
    Vec& A = state.A;
    std::vector<int> indx(nDof);
    for(int i=0; i<nDof; ++i)
      indx[i] = i;

    // Copy
    PetscErrorCode ierr;
    ierr = VecSetValues(U, nDof, indx.data(), disp, INSERT_VALUES); CHKERRV(ierr);
    ierr = VecSetValues(V, nDof, indx.data(), vel,  INSERT_VALUES); CHKERRV(ierr);
    ierr = VecSetValues(A, nDof, indx.data(), accn, INSERT_VALUES); CHKERRV(ierr);
    ierr = VecAssemblyBegin(U); CHKERRV(ierr);
    ierr = VecAssemblyBegin(V); CHKERRV(ierr);
    ierr = VecAssemblyBegin(A); CHKERRV(ierr);
    ierr = VecAssemblyEnd(U);   CHKERRV(ierr);
    ierr = VecAssemblyEnd(V);   CHKERRV(ierr);
    ierr = VecAssemblyEnd(A);   CHKERRV(ierr);

    // done
    isInitialized = true;
    return;
  }

  // Advance in time
  void TimeIntegrator::Advance(const Vec& F)
  {
    assert(isSet==true && isDestroyed==false && isInitialized==true);
    const int nbcs = static_cast<int>(dirichlet_dofs.size());
    const std::vector<double> zero(nbcs, 0.);

    // State at time tn
    Vec& Un = state.U;
    Vec& Vn = state.V;
    Vec& An = state.A;

    // predictor displacement dnp = dn + dt*Vn + (1/2-beta)*dt^2*An
    PetscErrorCode ierr;
    ierr = VecCopy(Un, dnp);                   CHKERRV(ierr);
    ierr = VecAXPY(dnp, dt, Vn);               CHKERRV(ierr);
    ierr = VecAXPY(dnp, (0.5-beta)*dt*dt, An); CHKERRV(ierr);

    // intermediate quantities
    ierr = VecZeroEntries(MAn);   CHKERRV(ierr);
    ierr = VecZeroEntries(Kdn);   CHKERRV(ierr);
    ierr = VecZeroEntries(Kdnp);  CHKERRV(ierr);
    ierr = MatMult(M, An, MAn);   CHKERRV(ierr);
    ierr = MatMult(K, Un, Kdn);   CHKERRV(ierr);
    ierr = MatMult(K, dnp, Kdnp); CHKERRV(ierr);

    // Algorithm specific RHS vec
    ComputeAlgorithmicRHSVec(F, Kdn, Kdnp, MAn, RHS);

    // Solve LHSmat*Anp1 = RHS
    if(nbcs!=0)
      {
	ierr = VecSetValues(RHS, nbcs, dirichlet_dofs.data(), zero.data(), INSERT_VALUES); CHKERRV(ierr);
	ierr = VecAssemblyBegin(RHS); CHKERRV(ierr);
	ierr = VecAssemblyEnd(RHS);   CHKERRV(ierr);
      }
    linSolver.Solve(RHS, Anp1);

    // dn+1 = dnp + beta*dt^2*Anp1
    ierr = VecCopy(dnp, Un);              CHKERRV(ierr);
    ierr = VecAXPY(Un, beta*dt*dt, Anp1); CHKERRV(ierr);
      
    // Vn+1 = Vn + (1-gamma)*dt*An + gamma*dt*Anp1
    ierr = VecAXPY(Vn, (1.-gamma)*dt, An); CHKERRV(ierr);
    ierr = VecAXPY(Vn, gamma*dt, Anp1);    CHKERRV(ierr);

    // An+1 = Anp1
    ierr = VecCopy(Anp1, An); CHKERRV(ierr);

    // update time
    time += dt;

    // done
    return;
  }
  
}

