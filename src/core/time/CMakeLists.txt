
# add library
add_library(pfe_time STATIC
  		     pfe_NewmarkIntegrator.cpp
		     pfe_TimeIntegrator.cpp
		     pfe_Newmark.cpp
		     pfe_GenAlpha.cpp
     		     pfe_HHTAlpha.cpp)

# headers
target_include_directories(pfe_time PUBLIC
  				    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)

# link
target_link_libraries(pfe_time PUBLIC pfe_assembly pfe_petsc pfe_dyn_state)

# Add required flags
target_compile_features(pfe_time PUBLIC ${pfe_COMPILE_FEATURES})

install(FILES
		pfe_TimeIntegrator.h
		pfe_NewmarkIntegrator.h
		pfe_Newmark.h
		pfe_GenAlpha.h
		pfe_HHTAlpha.h
		DESTINATION ${PROJECT_NAME}/include)

