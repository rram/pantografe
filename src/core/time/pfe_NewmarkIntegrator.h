
/** \file pfe_NewmarkIntegrator.h
 * \brief Defines the class pfe::NewmarkIntegrator
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_DynamicState.h>
#include <pfe_LinearSolver.h>
#include <vector>
#include <petscmat.h>
#include <cassert>

namespace pfe
{
  //! \ingroup time
  class NewmarkIntegrator
  {
  public:
    //! Constructor
    NewmarkIntegrator(const int ndof, const double delta);

    //! Disable copy and assignment
    NewmarkIntegrator(const NewmarkIntegrator&) = delete;
    NewmarkIntegrator& operator=(const NewmarkIntegrator&) = delete;
    
    //! Destructor
    virtual ~NewmarkIntegrator();

    //! Destroy data structures
    void Destroy();

    //! Set operators
    void SetOperators(const Mat& mass, const Mat& stiffness, const std::vector<int>& dbcs_dofs);

    //! Main functionality: initialize acceleration
    void SetInitialConditions(const double t0, const double* disp,
			      const double* vel, const double* accn);
    
    //! Main functionality
    void Advance(const Vec& Fnext, const std::vector<double>& dirichlet_values);

    //! Access the solution
    inline DynamicState& GetState()
    {
      assert(isInitialized==true && isDestroyed==false);
      return state;
    }

    //! Access the time
    inline double GetTime() const
    { return time; }

    //! Access the timestep
    inline double GetTimestep() const
    { return dt; }
    
  private:
    bool isInitialized;     //!< have initial conditions been set
    bool isSet;             //!< are the operators set?
    bool isDestroyed;       //!< state of the petsc data structures
    
    double time;            //!< current time
    const double dt;        //!< Time step
    const int nDof;         //!< # dofs
    
    DynamicState state;     //!< Current dynamic state
    Vec RHS;                //!< RHS vector
    Vec Fprev;              //!< Force vector at the previous time step
    Vec temp;               //!< Misc calcs

    std::vector<int> dirichlet_dofs;     //!< Set of dirichlet bcs
    Mat              M;                  //!< mass matrix
    Mat              K;                  //!< stiffness matrix
    Mat              compositeMat;       //!< Intermediate matrix
    LinearSolver     linSolver;          //!< Linear solver

    const double beta  = 1./4.;
    const double gamma = 1./2.;
  };
}
