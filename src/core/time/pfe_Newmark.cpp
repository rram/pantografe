
/** \file pfe_Newmark.cpp
 * \brief Implements the class pfe::Newmark
 * \author Ramsharan Rangarajan
 */

#include <pfe_Newmark.h>

namespace pfe
{
  // Algorithm specific matrix operator
  void Newmark::ComputeAlgorithmicLHSMat(const Mat& m, const Mat& k, Mat& mat)
  {
    // mat = M + beta*dt^2 K
    PetscErrorCode ierr;
    ierr = MatDuplicate(m, MAT_COPY_VALUES, &mat);                       CHKERRV(ierr);
    ierr = MatSetOption(mat, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);     CHKERRV(ierr);
    ierr = MatAXPY(mat, 0.25*tstep*tstep, k, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);
    
    // done
    return;
  }

  
  // Algorithm specific RHS vector
  void Newmark::ComputeAlgorithmicRHSVec(const Vec& f, const Vec& kdn,
					 const Vec& kdnp, const Vec& man, Vec& rhs)
  {
    // rhs = F - K*dnp 
    PetscErrorCode ierr;
    ierr = VecCopy(f, rhs);         CHKERRV(ierr);
    ierr = VecAXPY(rhs, -1., kdnp); CHKERRV(ierr);
    
    // done
    return;
  }

    
}
