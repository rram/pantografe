
/** \file pfe_Newmark.h
 * \brief Defines the class pfe::Newmark
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_TimeIntegrator.h>

namespace pfe
{
  //! \ingroup time
  class Newmark: public TimeIntegrator
  {
  public:
    //! Constructor
    inline Newmark(const int ndof, const double delta)
      :TimeIntegrator(0.25, 0.5, ndof, delta),
      tstep(delta) {}

    //! Disable copy and assignment
    Newmark(const Newmark&) = delete;
    Newmark operator=(const Newmark&) = delete;

    //! Destructor
    inline ~Newmark() {}

    //! Get the next evaluation time
    inline double GetNextEvaluationTime() const override
    { return GetTime()+GetTimestep(); }

  protected:
    //! Algorithm specific matrix operator
    void ComputeAlgorithmicLHSMat(const Mat& m, const Mat& k, Mat& mat) override;

    //! Algorithm specific RHS vector
    void ComputeAlgorithmicRHSVec(const Vec& f, const Vec& kdn, const Vec& kdnp, const Vec& man, Vec& rhs) override;

  private:
    const double tstep;
  };
  
}
