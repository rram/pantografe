
/** \file pfe_GenAlpha.h
 * \brief Defines the class pfe::GenAlpha
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_TimeIntegrator.h>

namespace pfe
{
  //! \ingroup time
  class GenAlpha: public TimeIntegrator
  {
  public:
    //! Constructor
    GenAlpha(const double rho, const int ndof, const double delta);
    
    //! Disable copy and assignment
    GenAlpha(const GenAlpha&) = delete;
    GenAlpha operator=(const GenAlpha&) = delete;

    //! Destructor
    inline ~GenAlpha() {}

    //! Get the next evaluation time
    double GetNextEvaluationTime() const override;
    
  protected:
    //! Algorithm specific matrix operator
    void ComputeAlgorithmicLHSMat(const Mat& m, const Mat& k, Mat& mat) override;

    //! Algorithm specific RHS vector
    void ComputeAlgorithmicRHSVec(const Vec& f, const Vec& kdn, const Vec& kdnp,
				  const Vec& man, Vec& rhs) override;

  private:
    const double alpha_m, alpha_f;
    const double BETA, GAMMA;
    const double tstep;
  };
  
}
