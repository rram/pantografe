
/** \file pfe_NewmarkIntegrator.cpp
 * \brief Implements the class pfe::NewmarkIntegrator
 * \author Ramsharan Rangarajan
 */

#include <pfe_NewmarkIntegrator.h>

namespace pfe
{
  // Constructor
  NewmarkIntegrator::NewmarkIntegrator(const int ndof, const double delta)
    :isInitialized(false),
     isSet(false),
     isDestroyed(false),
     time(0.),
     dt(delta),
     nDof(ndof),
     state(nDof),
     dirichlet_dofs{}
  {
    // Create RHS
    VecCreate(PETSC_COMM_WORLD, &RHS);    
    VecSetSizes(RHS, PETSC_DECIDE, nDof); 
    VecSetFromOptions(RHS);

    // Create force vector for prev time step
    VecCreate(PETSC_COMM_WORLD, &Fprev);       
    VecSetSizes(Fprev, PETSC_DECIDE, nDof);    
    VecSetFromOptions(Fprev);                  

    // Create temporary vector
    VecDuplicate(Fprev, &temp);                
  }
  
  // Destructor
  NewmarkIntegrator::~NewmarkIntegrator()
  {
    assert(isDestroyed==true);
  }

  // Destroy data structures
  void NewmarkIntegrator::Destroy()
  {
    assert(isDestroyed==false);
    state.Destroy();
    if(isSet)
      {
	PetscErrorCode ierr;
	ierr = MatDestroy(&M);            CHKERRV(ierr);
	ierr = MatDestroy(&K);            CHKERRV(ierr);
	ierr = MatDestroy(&compositeMat); CHKERRV(ierr);
	ierr = VecDestroy(&RHS);          CHKERRV(ierr);
	ierr = VecDestroy(&Fprev);        CHKERRV(ierr);
	ierr = VecDestroy(&temp);         CHKERRV(ierr);
      }
    linSolver.Destroy();
    isDestroyed = true;
  }

  // Set operators and time step
  void NewmarkIntegrator::SetOperators(const Mat& mass, const Mat& stiffness,
				       const std::vector<int>& dbc_dofs)
  {
    // Dirichlet BCs
    dirichlet_dofs = dbc_dofs;

    // Note: MatDuplicate replicates the non-zero structure.
    // Hence SetOperators can be called with matrices having modified sparsity
    
    // Copy mass and stiffness matrices
    PetscErrorCode ierr;
    ierr = MatDuplicate(mass,      MAT_COPY_VALUES, &M); CHKERRV(ierr);
    ierr = MatDuplicate(stiffness, MAT_COPY_VALUES, &K); CHKERRV(ierr);
		     
    // Check
    int nrows, ncols;
    ierr = MatGetSize(M, &nrows, &ncols);                CHKERRV(ierr);
    assert(nrows==nDof && ncols==nDof);
    
    // Composite matrix: M/(dt^2/4) + K
    ierr = MatDuplicate(K, MAT_COPY_VALUES, &compositeMat);                 CHKERRV(ierr);  // mat = K
    ierr = MatAXPY(compositeMat, 4./(dt*dt), M, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);  // mat = K + (4/dt^2) M

    // Zero rows of composite matrix corresponding to dirichlet bcs
    ierr = MatSetOption(compositeMat, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);                                          CHKERRV(ierr);
    ierr = MatZeroRows(compositeMat, dirichlet_dofs.size(), dirichlet_dofs.data(), 1., PETSC_NULLPTR, PETSC_NULLPTR); CHKERRV(ierr);

    // Set the operator in the linear solver
    linSolver.SetOperator(compositeMat);

    isSet       = true;
    isDestroyed = false;
    return;
  }

  // Main functionality: initialize acceleration
  void NewmarkIntegrator::SetInitialConditions(const double t0,
					       const double* disp,
					       const double* vel,
					       const double* accn)
  {
    assert(isDestroyed==false && isSet==true && isInitialized==false);

    // Initial time
    time = t0;
    
    // Set initial displacement and velocity in the dynamic configuration
    Vec& U = state.U;
    Vec& V = state.V;
    Vec& A = state.A;
    std::vector<int> indx(nDof);
    for(int i=0; i<nDof; ++i)
      indx[i] = i;

    PetscErrorCode ierr;

    // Copy displacements, velocities and accelerations
    ierr = VecSetValues(U, nDof, indx.data(), disp, INSERT_VALUES); CHKERRV(ierr);
    ierr = VecSetValues(V, nDof, indx.data(), vel,  INSERT_VALUES); CHKERRV(ierr);
    ierr = VecSetValues(A, nDof, indx.data(), accn, INSERT_VALUES); CHKERRV(ierr);

    ierr = VecAssemblyBegin(U); CHKERRV(ierr);
    ierr = VecAssemblyBegin(V); CHKERRV(ierr);
    ierr = VecAssemblyBegin(A); CHKERRV(ierr);
    
    ierr = VecAssemblyEnd(U);   CHKERRV(ierr);
    ierr = VecAssemblyEnd(V);   CHKERRV(ierr);
    ierr = VecAssemblyEnd(A);   CHKERRV(ierr);

    // F = MA + KU
    ierr = MatMult(M, A, Fprev);           CHKERRV(ierr);   // F = MA
    ierr = MatMultAdd(K, U, Fprev, Fprev); CHKERRV(ierr);   // F = MA + KU
    
    isInitialized = true;
  }
  
  
  // Main functionality
  void NewmarkIntegrator::Advance(const Vec& Fnext,
				  const std::vector<double>& dirichlet_values)
  {
    assert(isSet==true && isDestroyed==false);
    const int nbcs = static_cast<int>(dirichlet_dofs.size());
    assert(static_cast<int>(dirichlet_values.size())==nbcs);
    PetscErrorCode ierr;
    
    // state at time tn
    Vec& Un = state.U;
    Vec& Vn = state.V;
    Vec& An = state.A;
    
    // Scheme: (M/(dt^2)/4 + K) (dn+1-dn) = (Fn+1-Fn) + 2 M An + M Vn/(dt/4)

    // RHS
    ierr = VecCopy(Fnext, RHS);       CHKERRV(ierr);  // RHS = Fn+1
    ierr = VecAXPY(RHS, -1., Fprev);  CHKERRV(ierr);  // RHS = Fn+1 - Fn
    ierr = MatMult(M, An, temp);      CHKERRV(ierr);  // M An
    ierr = VecAXPY(RHS, 2., temp);    CHKERRV(ierr);  // RHS = (Fn+1 - Fn) + 2 M An
    ierr = MatMult(M, Vn, temp);      CHKERRV(ierr);  // M Vn
    ierr = VecAXPY(RHS, 4./dt, temp); CHKERRV(ierr);  // RHS = (Fn+1 - Fn) + 2 M An + 4 M Vn /dt

    // Set dirichlet values for (dn+1 - dn)
    std::vector<double> bcvals(nbcs);
    ierr = VecGetValues(Un, nbcs, dirichlet_dofs.data(), bcvals.data()); CHKERRV(ierr);
    for(int i=0; i<nbcs; ++i)
      {
	bcvals[i] *= -1.;
	bcvals[i] += dirichlet_values[i];
      }
    ierr = VecSetValues(RHS, nbcs, dirichlet_dofs.data(), bcvals.data(), INSERT_VALUES);
    ierr = VecAssemblyBegin(RHS); CHKERRV(ierr);
    ierr = VecAssemblyEnd(RHS);   CHKERRV(ierr);

    // Solve: temp = (Un+1 - Un)
    linSolver.Solve(RHS, temp);

    // Un+1 = (Un+1 - Un) + Un
    ierr = VecAXPY(Un, 1., temp); CHKERRV(ierr);

    // temp = (An + An+1)/2 = ((dn+1 - dn) - dt Vn)(2/dt^2) 
    ierr = VecScale(temp, 2./(dt*dt)); CHKERRV(ierr);      
    ierr = VecAXPY(temp, -2./dt, Vn);  CHKERRV(ierr); 

    // Vn+1 = Vn + dt (An + An+1)/2
    ierr = VecAXPY(Vn, dt, temp); CHKERRV(ierr);

    // An+1 = 2 ((An+An+1)/2) - An
    ierr = VecScale(An, -1.);     CHKERRV(ierr);
    ierr = VecAXPY(An, 2., temp); CHKERRV(ierr);

    // (Un, Vn, An) has now been updated to the next time step
    // save the current force vector for the next iteration
    ierr = VecCopy(Fnext, Fprev); CHKERRV(ierr);

    // update the time
    time += dt;
    
    // done
    return;
  }

}
    
  
