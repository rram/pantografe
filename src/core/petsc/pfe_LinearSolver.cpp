
/** \file pfe_LinearSolver.coo
 * \brief Implements the service class implementing a wrapper for PETSc KSP.
 * \author Ramsharan Rangarajan
 */

#include <pfe_LinearSolver.h>
#include <cassert>

namespace pfe
{
  // Constructor
  LinearSolver::LinearSolver()
    :isSet(false),
     isDestroyed(false)
  {
    // KSP solver
    PetscErrorCode ierr;
    ierr = KSPCreate(PETSC_COMM_WORLD,&(kspSOLVER)); CHKERRV(ierr);
    ierr = KSPSetFromOptions(kspSOLVER); CHKERRV(ierr);

    // Preconditioner
    PC prec;  
    ierr = KSPGetPC(kspSOLVER,&prec); CHKERRV(ierr);
    ierr = PCSetType(prec, PCLU);     CHKERRV(ierr);
    ierr = PCSetFromOptions(prec);    CHKERRV(ierr);
  }

  // Destroy
  void LinearSolver::Destroy()
  {
    assert(isDestroyed==false);
    PetscErrorCode ierr = KSPDestroy(&kspSOLVER); CHKERRV(ierr);
    isDestroyed         = true;
    isSet               = false;
  }

  // Destructor
  LinearSolver::~LinearSolver()
  {
    assert(isDestroyed==true);
  }

  // Set the matrix operator
  void LinearSolver::SetOperator(const Mat& A)
  {
    assert(isDestroyed==false);

    // Check assembly status of A
    PetscErrorCode ierr;
    PetscBool flag;
    ierr = MatAssembled(A, &flag); CHKERRV(ierr);
    assert(flag==PETSC_TRUE);

    // Set operators
    ierr = KSPSetOperators(kspSOLVER, A, A); CHKERRV(ierr);

    // done
    isSet = true;
    return;
  }
    

  // Main functionality: solve
  void LinearSolver::Solve(Vec& RHS, Vec& X)
  {
    assert(isDestroyed==false && isSet==true);

    // Initialize solution vector
    PetscErrorCode ierr;
    ierr = VecSet(X, 0.); CHKERRV(ierr);
    
    // Solve
    ierr = KSPSolve(kspSOLVER, RHS, X); CHKERRV(ierr);

    // How did it go?
    KSPConvergedReason reason;
    ierr = KSPGetConvergedReason(kspSOLVER, &reason); CHKERRV(ierr);
    if(reason<0)
      {
	PetscPrintf(PETSC_COMM_WORLD, "KSPConvergedReason: %d\n", reason);
	assert(false && "LinearSolver::Solve(). Solution has diverged.");
      }

    return;
  }

}
