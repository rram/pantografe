
/** \file pfe_LinearSolver.h
 * \brief Defines the service implementing a wrapper for PETSc KSP solver
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <petscksp.h>
#include <petscmat.h>
#include <petscvec.h>

namespace pfe
{
  //! \ingroup assembly
  class LinearSolver
  {
  public:
    //! Constructor
    LinearSolver();

    //! Destructor
    virtual ~LinearSolver();

    //! Set the matrix operator
    void SetOperator(const Mat& A);
    
    //! Main functionality: solve A x = b using a direct solver
    void Solve(Vec& b, Vec& x);

    //! Destroy
    void Destroy();

  private:
    bool isSet;
    bool isDestroyed;
    KSP  kspSOLVER;
  };
  
}

