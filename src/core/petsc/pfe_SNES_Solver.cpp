// Sriramajayam

/** \file pfe_SNES_Solver.cpp
 * \brief Implements the struct pfe::SNES_Solver
 * \author Ramsharan Rangarajan
 */

#include <pfe_SNES_Solver.h>
#include <cassert>
#include <iostream>

namespace pfe
{
  // Constructor
  SNESSolver::SNESSolver()
    :is_Initialized(false) {}
  
  // Initialize
  void SNESSolver::Initialize(const std::vector<int>& nnz,
			      PetscErrorCode (*resFunc)(SNES,Vec,Vec,void*),
			      PetscErrorCode (*jacFunc)(SNES,Vec,Mat,Mat,void*))
  {
    // Check that PETSc has been initialized
    PetscBool flag = PETSC_FALSE;
    ierr = PetscInitialized(&flag); CHKERRV(ierr);
    assert(flag==PETSC_TRUE && "pfe::SNESSolver::Initialize- PETSc not initialized");

    // Cannot initialize multiple times
    assert(is_Initialized==false && "pfe::SNESSolver::Initialize- data structure previously initialized");
    
    // Size
    const int ndof = static_cast<int>(nnz.size());

     // Index set of dof numbers
    std::vector<int> dofnums(ndof);
    for(int a=0; a<ndof; ++a)
      dofnums[a] = a;
    IS IS_dofnums;
    ierr = ISCreateGeneral(PETSC_COMM_WORLD, ndof, dofnums.data(), PETSC_COPY_VALUES, &IS_dofnums); CHKERRV(ierr);

    // Local to global map = identity for sequential matrix
    ISLocalToGlobalMapping L2G;
    ierr = ISLocalToGlobalMappingCreateIS(IS_dofnums, &L2G); CHKERRV(ierr);
    
    // Create data structures

    // Jacobian matrix
    ierr = MatCreateSeqAIJ(PETSC_COMM_WORLD, ndof, ndof, PETSC_DEFAULT, &nnz[0], &kMat); CHKERRV(ierr);
    ierr = MatSetOption(kMat, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE); CHKERRV(ierr);
    ierr = MatSetLocalToGlobalMapping(kMat, L2G, L2G); CHKERRV(ierr);
    
    // Residual and solution vectors
    ierr = VecCreate(PETSC_COMM_WORLD, &resVec); CHKERRV(ierr);
    ierr = VecSetSizes(resVec, PETSC_DECIDE, ndof); CHKERRV(ierr);
    ierr = VecSetFromOptions(resVec); CHKERRV(ierr);
    ierr = VecDuplicate(resVec, &solVec); CHKERRV(ierr);
    ierr = VecSetFromOptions(solVec); CHKERRV(ierr);

    // Create the nonlinear solver
    ierr = SNESCreate(PETSC_COMM_WORLD, &snes); CHKERRV(ierr);

    // Set the nonlinear solver
    ierr = SNESSetType(snes, SNESNEWTONLS); CHKERRV(ierr);
    
    // Set a direct LU linear solver
    KSP ksp;
    ierr = SNESGetKSP(snes, &ksp); CHKERRV(ierr);
    PC pc;
    ierr = KSPGetPC(ksp, &pc); CHKERRV(ierr);
    ierr = PCSetType(pc, PCLU); CHKERRV(ierr);

    // Set the function for residual evaluations
    ierr = SNESSetFunction(snes, resVec, resFunc, PETSC_NULLPTR); CHKERRV(ierr);

    // Set the function for Jacobian evaluations
    ierr = SNESSetJacobian(snes, kMat, kMat, jacFunc, PETSC_NULLPTR); CHKERRV(ierr);

    // Set from options
    ierr = SNESSetFromOptions(snes); CHKERRV(ierr);
    
    // done
    ierr = ISLocalToGlobalMappingDestroy(&L2G); CHKERRV(ierr);
    ierr = ISDestroy(&IS_dofnums);              CHKERRV(ierr);
    is_Initialized = true;
    is_Destroyed = false;
    return;
  }

  // Destroy data structures
  void SNESSolver::Destroy()
  {
    // Petsc should not be finalized
    PetscBool flag;
    ierr = PetscFinalized(&flag); CHKERRV(ierr);
    assert(flag==PETSC_FALSE && "pfe::SNESSolver::Destroy- PETSc finalized early");
  
    // Check that the data structure has been initialized and not previously destroyed
    assert((is_Initialized==true && is_Destroyed==false) && "pfe::SNESSolver::Destroy()- data structure in incorrect state");
    ierr = SNESDestroy(&snes); CHKERRV(ierr);
    ierr = MatDestroy(&kMat); CHKERRV(ierr);
    ierr = VecDestroy(&resVec); CHKERRV(ierr);
    ierr = VecDestroy(&solVec); CHKERRV(ierr);
    is_Initialized = false;
    is_Destroyed = true;
  }

  // Destructor: check that the data structure has been destroyed
  SNESSolver::~SNESSolver()
  { assert(is_Destroyed==true && "pfe::SNESSolver- Destroy() method not called"); }

  // Solve
  void SNESSolver::Solve(double* init_guess,
			 void* ctx,
			 const bool verbose)
  {
    // Set the initial guess
    assert(init_guess!=nullptr &&  "pfe::SNESSolver::Solve- initial guess is null");
    int nDof;
    ierr = VecGetSize(solVec, &nDof); CHKERRV(ierr);
    std::vector<int> indx(nDof);
    for(int i=0; i<nDof; ++i)
      indx[i] = i;
    ierr = VecSetValues(solVec, nDof, &indx[0], init_guess, INSERT_VALUES); CHKERRV(ierr);

    // Set the application context
    ierr = SNESSetApplicationContext(snes, ctx); CHKERRV(ierr);
    
    // Set from options
    ierr = SNESSetFromOptions(snes); CHKERRV(ierr);
    
    // Solve
    ierr = SNESSolve(snes, PETSC_NULLPTR, solVec); CHKERRV(ierr);
        
    // Check convergence
    SNESConvergedReason reason;
    ierr =  SNESGetConvergedReason(snes, &reason); CHKERRV(ierr);
    if(reason==SNESConvergedReason::SNES_CONVERGED_FNORM_ABS ||      // Residual smaller than an absolute tolerance
       reason==SNESConvergedReason::SNES_CONVERGED_FNORM_RELATIVE || // Residual/Residual_0 smaller than a tolerance
       reason==SNESConvergedReason::SNES_CONVERGED_SNORM_RELATIVE )  // Newton step x/x_0 size small than a tolerance
      {
	ierr = VecGetValues(solVec, nDof, &indx[0], init_guess); CHKERRV(ierr);
	return;
      }
    else
      {
	std::cout << std::endl << "SNESSolver failed. Reason: " << std::endl;
	switch(reason)
	  {
	  case SNESConvergedReason::SNES_CONVERGED_ITS:         { assert(false && "Max. iterations exceeded."); break; }
	  case SNESConvergedReason::SNES_DIVERGED_TR_DELTA:     { assert(false && "Diverged" ); break; }
	  case SNESConvergedReason::SNES_DIVERGED_LINEAR_SOLVE: { assert(false && "Linear solve failed"); break; }
	  case SNESConvergedReason::SNES_DIVERGED_FNORM_NAN:    { assert(false && "Residual is NAN"); break; }
	  case SNESConvergedReason::SNES_DIVERGED_LINE_SEARCH:  { assert(false && "Line search failed"); break; }

	  default: { std::cout << reason << std::endl; assert(false && "Unexpected diverged reason"); break; }
	  }
      }
    return;
  }

}
