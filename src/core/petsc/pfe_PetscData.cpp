
/** \file pfe_PetscData.cpp
 * \brief Implements the helper struct PestcData
 * \author Ramsharan Rangarajan
 * Last modified: September 28, 2021
 */

#include <pfe_PetscData.h>
#include <cassert>

namespace pfe
{
  void PetscData::Initialize(const std::vector<int>& nz)
  {
    const int ndof = static_cast<int>(nz.size());

    // Set the sizes, and options
    PetscErrorCode ierr;
  
    ierr = MatCreateSeqAIJ(PETSC_COMM_WORLD, ndof, ndof, PETSC_DEFAULT, &nz[0], &stiffnessMAT); CHKERRV(ierr);
    ierr = MatCreateSeqAIJ(PETSC_COMM_WORLD, ndof, ndof, PETSC_DEFAULT, &nz[0], &massMAT);      CHKERRV(ierr);
    ierr = VecCreate(PETSC_COMM_WORLD,&(resVEC)); CHKERRV(ierr);
    ierr = VecSetSizes(resVEC,PETSC_DECIDE, ndof); CHKERRV(ierr);
    ierr = VecSetFromOptions(resVEC); CHKERRV(ierr);
    ierr = VecDuplicate(resVEC,&(solutionVEC)); CHKERRV(ierr);

    // Index set of dof numbers
    std::vector<int> dofnums(ndof);
    for(int a=0; a<ndof; ++a)
      dofnums[a] = a;
    IS IS_dofnums;
    ierr = ISCreateGeneral(PETSC_COMM_WORLD, ndof, dofnums.data(), PETSC_COPY_VALUES, &IS_dofnums); CHKERRV(ierr);

    // Local to global map = identity for sequential matrix
    ISLocalToGlobalMapping L2G;
    ierr = ISLocalToGlobalMappingCreateIS(IS_dofnums, &L2G); CHKERRV(ierr);

    // Set the local to global map for the stiffness and mass matrix
    ierr = MatSetLocalToGlobalMapping(stiffnessMAT, L2G, L2G); CHKERRV(ierr);
    ierr = MatSetLocalToGlobalMapping(massMAT, L2G, L2G);      CHKERRV(ierr);
				       
    // We can zero rows for dirichlet BCs without losing the zero structure
    ierr = MatSetOption(stiffnessMAT,MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE); CHKERRV(ierr);
    ierr = MatSetOption(massMAT,MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);      CHKERRV(ierr);
	
  
    // clean up
    ierr = ISLocalToGlobalMappingDestroy(&L2G); CHKERRV(ierr);
    ierr = ISDestroy(&IS_dofnums); CHKERRV(ierr);

    // ready for assembly
    ierr = MatZeroEntries(stiffnessMAT); CHKERRV(ierr);
    ierr = MatZeroEntries(massMAT);      CHKERRV(ierr);
    ierr = VecZeroEntries(resVEC);       CHKERRV(ierr);
    ierr = VecZeroEntries(solutionVEC);  CHKERRV(ierr);
  } 


  // Set dirichlet bcs
  void PetscData::SetDirichletBCs(const std::vector<int> &dirichletrows,
				  const std::vector <double> &dirichletvals)
  {
    // If the assembly is not complete
    PetscBool flag;
    PetscErrorCode ierr;
    ierr = MatAssembled(stiffnessMAT, &flag); CHKERRV(ierr);
    //ierr = MatAssembled(massMAT, &flag);      CHKERRV(ierr);
    if(flag==PETSC_FALSE)
      {
	ierr = MatAssemblyBegin(stiffnessMAT,MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyEnd(stiffnessMAT,MAT_FINAL_ASSEMBLY);   CHKERRV(ierr);

	//ierr = MatAssemblyBegin(massMAT,MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	//ierr = MatAssemblyEnd(massMAT,MAT_FINAL_ASSEMBLY);   CHKERRV(ierr);

	ierr = VecAssemblyBegin(resVEC); CHKERRV(ierr);
	ierr = VecAssemblyEnd(resVEC); CHKERRV(ierr);
      }

    // Set boundary conditions
    const int nDirichlet = static_cast<int>(dirichletrows.size());

    ierr = MatSetOption(stiffnessMAT, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);                              CHKERRV(ierr);
    ierr = MatZeroRows(stiffnessMAT, nDirichlet, &(dirichletrows[0]), 1.0, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRV(ierr);

    //ierr = MatSetOption(massMAT, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);                              CHKERRV(ierr);
    //ierr = MatZeroRows(massMAT, nDirichlet, &(dirichletrows[0]), 1.0, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRV(ierr);
  
    // Update the residuals using the values of the specified bcs
    double *ResArray;
    ierr = VecGetArray(resVEC, &ResArray); CHKERRV(ierr);
    for(int i=0; i<nDirichlet; ++i)
      ResArray[dirichletrows[i]] = dirichletvals[i];
    ierr = VecRestoreArray(resVEC, &ResArray); CHKERRV(ierr);
  }


  void PetscData::Destroy()
  {
    PetscErrorCode ierr;
    ierr = VecDestroy(&solutionVEC);  CHKERRV(ierr);
    ierr = VecDestroy(&resVEC);       CHKERRV(ierr);
    ierr = MatDestroy(&stiffnessMAT); CHKERRV(ierr);
    ierr = MatDestroy(&massMAT);      CHKERRV(ierr);
  }
  
}
