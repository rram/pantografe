
/** \file pfe_PetscData.h
 * \brief Defines the helper struct PestcData
 * \author Ramsharan Rangarajan
 */


#pragma once

#include <vector>
#include <petscmat.h>
#include <petscvec.h>

namespace pfe
{
  struct PetscData
  {
    Vec  resVEC;
    Vec  solutionVEC;
    Mat  stiffnessMAT;
    Mat  massMAT;
    
    //! \param[in] nz Number of nonzeros per row.
    void Initialize(const std::vector<int>& nz);

    // Set dirichlet bcs
    void SetDirichletBCs(const std::vector<int> &dirichletrows,
			 const std::vector <double> &forces);

    // Destroy data structures
    void Destroy();
  };
}

