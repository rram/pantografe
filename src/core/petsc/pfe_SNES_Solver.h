// Sriramajayam

/** \file pfe_SNES_Solver.h
 * \brief Defines the struct pfe::SNES_Solver as a wrapper for PETSc SNES.
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <petscsnes.h>
#include <vector>

namespace pfe
{
  struct SNESSolver
  {
    SNES snes; //!< nonlinear solver context
    Vec resVec; //!< residual vector
    Vec solVec; //!< solution vector
    Mat kMat; //!< Jacobian matrix
    PetscErrorCode ierr; //!< Error checking

    //! Disable assignment and copy
    SNESSolver(const SNESSolver&) = delete;
    SNESSolver& operator=(const SNESSolver&) = delete;

    //! Constructor
    SNESSolver();
    
    //! Destructor
    virtual ~SNESSolver();
    
    //! Initialize
    //! \param[in] nnz Nonzeros
    //! \param[in] resFunc Residual function evaluation
    //! \param[in] jacFunc Jacobian evaluation function
    void Initialize(const std::vector<int>& nnz,
		    PetscErrorCode (*resFunc)(SNES,Vec,Vec,void*),
		    PetscErrorCode (*jacFunc)(SNES,Vec,Mat,Mat,void*));
    
    //! Clean up data structures
    void Destroy();

    //! Main functionality- solve
    //! \param[in] ctx Application context
    void Solve(double* init_guess, void* ctx, const bool verbose=false);

  private:
    bool is_Initialized; //!< Has the data structure been initialized
    bool is_Destroyed; //!< Has the data structure been destroyed
    
  };
}
