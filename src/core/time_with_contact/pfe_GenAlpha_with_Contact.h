
/** \file pfe_GenAlpha_with_Contact.h
 * \brief Defines the class pfe::GenAlpha_with_Contact
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_TimeIntegrator_with_Contact.h>

namespace pfe
{
  //! \ingroup time_contact
  class GenAlpha_with_Contact: public TimeIntegrator_with_Contact
  {
  public:
    //! Constructor
    GenAlpha_with_Contact(const double rho, const int ndof, const double delta);
    
    //! Disable copy and assignment
    GenAlpha_with_Contact(const GenAlpha_with_Contact&) = delete;
    GenAlpha_with_Contact& operator=(const GenAlpha_with_Contact&) = delete;

    //! Destructor
    inline virtual ~GenAlpha_with_Contact() {}

    //! Get the evaluation time
    inline double GetNextEvaluationTime() const override
    { return GetTime() + (1.-alpha_f)*GetTimestep(); }
    
  protected:
    //! Algorithm specific matrix operator
    void ComputeAlgorithmicLHSMat(const Mat& m, const Mat& k, Mat& mat) override;

    //! Algorithm specific RHS vector
    void ComputeAlgorithmicRHSVec(const Vec& f, const Vec& kdn, const Vec& kdnp, const Vec& man, Vec& rhs) override;

    //! Compute the contact force
    double ComputeAlgorithmicContactForce(const double& r_dot_A1, const double& r_dot_A2,
					  const double& r_dot_dn, const double& r_dot_dnp) const override;
    
    
  private:

    //! Constructor
    GenAlpha_with_Contact(const double am, const double af, const int ndof, const double delta);
    
    const double alpha_f; //!< algorithmic parameter
    const double alpha_m; //!< algorithmic parameter
    const double gamma;   //!< algorithmic parameter
    const double beta;    //!< algorithmic parameter
    const double tstep;   //!< time step
  };
}
