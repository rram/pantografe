
/** \file pfe_Newmark_with_Contact.h
 * \brief Defines the class pfe::Newmark_with_Contact
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_TimeIntegrator_with_Contact.h>

namespace pfe
{
  //! \ingroup time_contact
  class Newmark_with_Contact: public TimeIntegrator_with_Contact
  {
  public:
    //! Constructor
    inline Newmark_with_Contact(const int ndof, const double delta)
      :TimeIntegrator_with_Contact(0.25, 0.5, ndof, delta),
      tstep(delta) {}

    //! Disable copy and assignment
    Newmark_with_Contact(const Newmark_with_Contact&) = delete;
    Newmark_with_Contact operator=(const Newmark_with_Contact&) = delete;

    //! Destructor
    inline ~Newmark_with_Contact() {}

    //! Get the evaluation time
    inline double GetNextEvaluationTime() const override
    { return GetTime()+GetTimestep(); }

  protected:
    //! Algorithm specific matrix operator
    void ComputeAlgorithmicLHSMat(const Mat& m, const Mat& k, Mat& mat) override;

    //! Algorithm specific RHS vector
    void ComputeAlgorithmicRHSVec(const Vec& f, const Vec& kdn, const Vec& kdnp, const Vec& man, Vec& rhs) override;

    //! Compute the contact force
    double ComputeAlgorithmicContactForce(const double& r_dot_A1, const double& r_dot_A2,
					  const double& r_dot_dn, const double& r_dot_dnp) const override;

  private:
    const double tstep;
  };
  
}
