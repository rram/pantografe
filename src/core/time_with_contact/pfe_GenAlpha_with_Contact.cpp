
/** \file pfe_GenAlpha_with_Contact.cpp
 * \brief Implements the class pfe::GenAlpha_with_Contact
 * \author Ramsharan Rangarajan
 */

#include <pfe_GenAlpha_with_Contact.h>

namespace pfe
{
  // Constructor
  GenAlpha_with_Contact::GenAlpha_with_Contact(const double am, const double af,
					       const int ndof, const double delta)
    :TimeIntegrator_with_Contact(0.25*(1.-am+af)*(1.-am+af), 0.5-am+af, ndof, delta),
     alpha_m(am),
     alpha_f(af),
     beta(0.25*(1.-am+af)*(1.-am+af)),
     gamma(0.5-am+af),
     tstep(delta)
  {
    // ensure stability
    const double tol = 1.e-4;
    assert(alpha_m<=alpha_f+tol && alpha_f<=0.5+tol);
  }
    
  
  // Constructor
  GenAlpha_with_Contact::GenAlpha_with_Contact(const double rho, const int ndof, const double delta)
    :GenAlpha_with_Contact((2.*rho-1.)/(rho+1.), rho/(1.+rho), ndof, delta)
  {}
  

  // Algorithm specific matrix operator
  void GenAlpha_with_Contact::ComputeAlgorithmicLHSMat(const Mat& m, const Mat& k, Mat& mat)
  {
    // mat = (1-alpha_m)*M + (1-alpha_f)*beta*dt^2 K
    PetscErrorCode ierr;
    ierr = MatDuplicate(m, MAT_COPY_VALUES, &mat);                              CHKERRV(ierr);
    ierr = MatScale(mat, 1.-alpha_m);                                           CHKERRV(ierr);
    ierr = MatSetOption(mat, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);            CHKERRV(ierr);
    ierr = MatAXPY(mat, (1.-alpha_f)*beta*tstep*tstep, k, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);
    
    // done
    return;
  }

  // Algorithm specific RHS vector
  void GenAlpha_with_Contact::ComputeAlgorithmicRHSVec(const Vec& f, const Vec& kdn,
						       const Vec& kdnp, const Vec& man, Vec& rhs)
  {
    // rhs = F - alpha_m*M*An -(1-alpha_f)*K*dnp - alpha_f*K*dn    
    PetscErrorCode ierr;
    ierr = VecCopy(f, rhs);                   CHKERRV(ierr);
    ierr = VecAXPY(rhs, -alpha_m, man);       CHKERRV(ierr);
    ierr = VecAXPY(rhs, -(1.-alpha_f), kdnp); CHKERRV(ierr);
    ierr = VecAXPY(rhs, -alpha_f, kdn);       CHKERRV(ierr);

    // done
    return;
  }


  // Compute the contact force
  double GenAlpha_with_Contact::
  ComputeAlgorithmicContactForce(const double& r_dot_A1, const double& r_dot_A2,
				 const double& r_dot_dn, const double& r_dot_dnp) const 
  {
    const double dr = (1.-alpha_f)*beta*tstep*tstep;
    return ((-(1.-alpha_f)*r_dot_dnp - alpha_f*r_dot_dn)/dr - r_dot_A1)/r_dot_A2;
  }
  
}
