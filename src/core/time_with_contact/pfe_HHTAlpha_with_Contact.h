
/** \file pfe_HHTAlpha_with_Contact.h
 * \brief Defines the class pfe::HHTAlpha_with_Contact
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_TimeIntegrator_with_Contact.h>

namespace pfe
{
  //! \ingroup time_contact
  class HHTAlpha_with_Contact: public TimeIntegrator_with_Contact
  {
  public:
    //! Constructor
    HHTAlpha_with_Contact(const double aleph, const int ndof, const double delta);

    //! Disable copy and assignment
    HHTAlpha_with_Contact(const HHTAlpha_with_Contact&) = delete;
    HHTAlpha_with_Contact& operator=(const HHTAlpha_with_Contact&) = delete;

    //! Destructor
    inline virtual ~HHTAlpha_with_Contact() {}

    //! Get the evaluation time
    inline double GetNextEvaluationTime() const override
    { return GetTime()+GetTimestep(); }
    
  protected:
     //! Algorithm specific matrix operator
    void ComputeAlgorithmicLHSMat(const Mat& m, const Mat& k, Mat& mat) override;

    //! Algorithm specific RHS vector
    void ComputeAlgorithmicRHSVec(const Vec& f, const Vec& kdn, const Vec& kdnp, const Vec& man, Vec& rhs) override;

    //! Compute the contact force
    double ComputeAlgorithmicContactForce(const double& r_dot_A1, const double& r_dot_A2,
					  const double& r_dot_dn, const double& r_dot_dnp) const override;

  private:
    const double alpha;   //!< algorithmic parameter
    const double gamma;   //!< algorithmic parameter
    const double beta;    //!< algorithmic parameter
    const double tstep;   //!< time step
  };

}
