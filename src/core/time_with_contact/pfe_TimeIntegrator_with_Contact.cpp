
/** \file pfe_TimeIntegrator_with_Contact.cpp
 * \brief Implements the class pfe::TimeIntegrator_with_Contact
 * \author Ramsharan Rangarajan
 */

#include <pfe_TimeIntegrator_with_Contact.h>

namespace pfe
{
  // Constructor
  TimeIntegrator_with_Contact::TimeIntegrator_with_Contact(const double BETA, const double GAMMA,
							   const int ndof, const double delta)
    :isInitialized(false),
     isSet(false),
     isDestroyed(false),
     beta(BETA),
     gamma(GAMMA),
     time(0.),
     dt(delta),
     nDof(ndof),
     state(nDof),
     contact_force(0.),
     dirichlet_dofs{} {}

     // Destuctor
     TimeIntegrator_with_Contact::~TimeIntegrator_with_Contact()
     {
       assert(isDestroyed==true);
     }

  
  // Destroy data structures
  void TimeIntegrator_with_Contact::Destroy()
  {
    assert(isDestroyed==false);
    state.Destroy();
    if(isSet)
      {
	PetscErrorCode ierr;
	ierr = MatDestroy(&M);      CHKERRV(ierr);
	ierr = MatDestroy(&K);      CHKERRV(ierr);
	ierr = MatDestroy(&LHSmat); CHKERRV(ierr);
	ierr = VecDestroy(&F);      CHKERRV(ierr);
	ierr = VecDestroy(&RHS);    CHKERRV(ierr);
	ierr = VecDestroy(&dnp);    CHKERRV(ierr);
	ierr = VecDestroy(&A1);     CHKERRV(ierr);
	ierr = VecDestroy(&A2);     CHKERRV(ierr);
	ierr = VecDestroy(&MAn);    CHKERRV(ierr);
	ierr = VecDestroy(&Kdn);    CHKERRV(ierr);
	ierr = VecDestroy(&Kdnp);   CHKERRV(ierr);
      }
    linSolver.Destroy();
    isDestroyed = true;
  }


  // Set operators
  void TimeIntegrator_with_Contact::SetOperators(const Mat& mass, const Mat& stiff,
						 const Vec& frc, const std::vector<int> dbc_dofs)
  {
    assert(isSet==false);
    
    // Copy M, K and F
    PetscErrorCode ierr;
    ierr = MatDuplicate(mass, MAT_COPY_VALUES,  &M); CHKERRV(ierr);
    ierr = MatDuplicate(stiff, MAT_COPY_VALUES, &K); CHKERRV(ierr);
    ierr = VecDuplicate(frc, &F);                    CHKERRV(ierr);
    ierr = VecCopy(frc, F);                          CHKERRV(ierr);
    dirichlet_dofs = dbc_dofs;

    // algorithmic operator
    ComputeAlgorithmicLHSMat(M, K, LHSmat);

    // Dirichlet bcs
    if(!dirichlet_dofs.empty())
      {
	const int nbcs = static_cast<int>(dirichlet_dofs.size());
	ierr = MatZeroRows(LHSmat, nbcs, dirichlet_dofs.data(), 1., PETSC_NULLPTR, PETSC_NULLPTR); CHKERRV(ierr);
      }

    // Set operators in the linear solver
    linSolver.SetOperator(LHSmat);
    
    // allocate vectors
    ierr = VecDuplicate(frc, &RHS);  CHKERRV(ierr);
    ierr = VecDuplicate(frc, &dnp);  CHKERRV(ierr);
    ierr = VecDuplicate(frc, &A1);   CHKERRV(ierr);
    ierr = VecDuplicate(frc, &A2);   CHKERRV(ierr);
    ierr = VecDuplicate(frc, &MAn);  CHKERRV(ierr);
    ierr = VecDuplicate(frc, &Kdn);  CHKERRV(ierr);
    ierr = VecDuplicate(frc, &Kdnp); CHKERRV(ierr);

    // done
    isSet       = true;
    isDestroyed = false;
    return;
  }

  
  // Initial conditions
  void TimeIntegrator_with_Contact::SetInitialConditions(const double t0,
							 const double* disp,
							 const double* vel,
							 const double* accn,
							 const double fc)
  {
    time = t0;
    assert(isDestroyed==false && isSet==true && isInitialized==false);

    // Aliases
    Vec& U = state.U;
    Vec& V = state.V;
    Vec& A = state.A;
    std::vector<int> indx(nDof);
    for(int i=0; i<nDof; ++i)
      indx[i] = i;

    // Copy
    contact_force = fc;
    PetscErrorCode ierr;
    ierr = VecSetValues(U, nDof, indx.data(), disp, INSERT_VALUES); CHKERRV(ierr);
    ierr = VecSetValues(V, nDof, indx.data(), vel,  INSERT_VALUES); CHKERRV(ierr);
    ierr = VecSetValues(A, nDof, indx.data(), accn, INSERT_VALUES); CHKERRV(ierr);
    ierr = VecAssemblyBegin(U); CHKERRV(ierr);
    ierr = VecAssemblyBegin(V); CHKERRV(ierr);
    ierr = VecAssemblyBegin(A); CHKERRV(ierr);
    ierr = VecAssemblyEnd(U);   CHKERRV(ierr);
    ierr = VecAssemblyEnd(V);   CHKERRV(ierr);
    ierr = VecAssemblyEnd(A);   CHKERRV(ierr);

    // done
    isInitialized = true;
    return;
  }



  // Advance in time
  void TimeIntegrator_with_Contact::Advance(const Vec& rvec)
  {
    assert(isSet==true && isDestroyed==false && isInitialized==true);
    const int nbcs = static_cast<int>(dirichlet_dofs.size());
    const std::vector<double> zero(nbcs, 0.);
    
    // State at time tn
    Vec& Un = state.U;
    Vec& Vn = state.V;
    Vec& An = state.A;

    // predictor displacement dnp = dn + dt*Vn + (1/2-beta)*dt^2*An
    PetscErrorCode ierr;
    ierr = VecCopy(Un, dnp);             CHKERRV(ierr);
    ierr = VecAXPY(dnp, dt, Vn);         CHKERRV(ierr);
    ierr = VecAXPY(dnp, (0.5-beta)*dt*dt, An); CHKERRV(ierr);

    // Intermediate quantitites
    ierr = VecZeroEntries(MAn);   CHKERRV(ierr);
    ierr = VecZeroEntries(Kdn);   CHKERRV(ierr);
    ierr = VecZeroEntries(Kdnp);  CHKERRV(ierr);
    ierr = MatMult(M, An, MAn);   CHKERRV(ierr);
    ierr = MatMult(K, Un, Kdn);   CHKERRV(ierr);
    ierr = MatMult(K, dnp, Kdnp); CHKERRV(ierr);

    // Algorithm specific RHS vec
    ComputeAlgorithmicRHSVec(F, Kdn, Kdnp, MAn, RHS);

    // Solve LHSmat*A1 = RHS
    if(nbcs!=0)
      {
	ierr = VecSetValues(RHS, nbcs, dirichlet_dofs.data(), zero.data(), INSERT_VALUES); CHKERRV(ierr);
	ierr = VecAssemblyBegin(RHS); CHKERRV(ierr);
	ierr = VecAssemblyEnd(RHS);   CHKERRV(ierr);
      }
    linSolver.Solve(RHS, A1);

    // solve LHSmat*A2 = rvec
    ierr = VecCopy(rvec, RHS); CHKERRV(ierr);
    if(nbcs!=0)
      {
	ierr = VecSetValues(RHS, nbcs, dirichlet_dofs.data(), zero.data(), INSERT_VALUES); CHKERRV(ierr);
	ierr = VecAssemblyBegin(RHS);                                                      CHKERRV(ierr);
	ierr = VecAssemblyEnd(RHS);                                                        CHKERRV(ierr);
      }
    linSolver.Solve(RHS, A2);
    
    // Compute the contact force using the constraint r^T d = 0
    double r_dot_A1;
    ierr = VecDot(rvec, A1, &r_dot_A1); CHKERRV(ierr);
    double r_dot_A2;
    ierr = VecDot(rvec, A2, &r_dot_A2); CHKERRV(ierr);
    double r_dot_dn;
    ierr = VecDot(rvec, Un, &r_dot_dn);   CHKERRV(ierr);
    double r_dot_dnp;
    ierr = VecDot(rvec, dnp, &r_dot_dnp); CHKERRV(ierr);
    contact_force = ComputeAlgorithmicContactForce(r_dot_A1, r_dot_A2, r_dot_dn, r_dot_dnp);
    
    // Vnp = Vn + (1-gamma)*dt*An
    ierr = VecAXPY(Vn, (1.-gamma)*dt, An);  CHKERRV(ierr);
    
    // An+1 = A1 + fc*A2
    ierr = VecCopy(A1, An);                CHKERRV(ierr);
    ierr = VecAXPY(An, contact_force, A2); CHKERRV(ierr);

    // Vn+1 = Vnp + gamma*dt*An+1
    ierr = VecAXPY(Vn, gamma*dt, An); CHKERRV(ierr);

    // dn+1 = dnp + beta*dt^2*An+1
    ierr = VecCopy(dnp, Un); CHKERRV(ierr);
    ierr = VecAXPY(Un, beta*dt*dt, An); CHKERRV(ierr);
      
    // update time
    time += dt;
    
    // done
    return;
  }

}
