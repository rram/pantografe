
/** \file pfe_TimeIntegrator_with_Contact.h
 * \brief Defines the class pfe::TimeIntegrator_with_Contact
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_DynamicState.h>
#include <pfe_LinearSolver.h>
#include <vector>
#include <petscmat.h>
#include <cassert>

namespace pfe
{
  //! \ingroup time_contact
  class TimeIntegrator_with_Contact
  {
  public:
    //! Constructor
    TimeIntegrator_with_Contact(const double BETA, const double GAMMA, const int ndof, const double delta);

    //! Destructor
    virtual ~TimeIntegrator_with_Contact();

    //! Destroy data structures
    void Destroy();

    //! Set operators
    void SetOperators(const Mat& mass, const Mat& stiffness,
		      const Vec& frc, const std::vector<int> dbcs_dofs);

    //! Set initial conditions
    void SetInitialConditions(const double t0, const double* disp,
			      const double* vel, const double* accn, const double fc);

    //! Get the evaluation time
    virtual double GetNextEvaluationTime() const = 0;

    //! Main functionality
    void Advance(const Vec& rvec);

    //! Access the solution
    inline DynamicState& GetState()
    {
      assert(isInitialized==true && isDestroyed==false);
      return state;
    }

    //! Access the contact force
    inline double GetContactForce()
    {
      assert(isInitialized==true && isDestroyed==false);
      return contact_force;
    }
    
    //! Access the time
    inline double GetTime() const
    { return time; }

    //! Access the time step
    inline double GetTimestep() const
    { return dt; }


  protected:

    //! Algorithm specific matrix operator
    virtual void ComputeAlgorithmicLHSMat(const Mat& m, const Mat& k, Mat& mat) = 0;

    //! Algorithm specific RHS vector
    virtual void ComputeAlgorithmicRHSVec(const Vec& f, const Vec& kdn, const Vec& kdnp, const Vec& man, Vec& rhs) = 0;

    //! Compute the contact force
    virtual double ComputeAlgorithmicContactForce(const double& r_dot_A1, const double& r_dot_A2,
						  const double& r_dot_dn, const double& r_dot_dnp) const = 0;
						  
  private:
    bool isInitialized; //!< have initial conditions been set
    bool isSet;         //!< have the operators been set
    bool isDestroyed;   //!< state of petsc data structures
    
    const double beta;   //!< Algorithmic parameter
    const double gamma;  //!< Algorithmic parameter
    
    const double dt;   //!< time step
    const int nDof;    //!< number of dofs, not including the contact force
    double time;       //!< current time
    std::vector<int> dirichlet_dofs; //!< dirichlet dofs
    
    DynamicState state;          //!< Current dynamic state
    double       contact_force;  //!< contact force
    Mat          M;              //!< mass
    Mat          K;              //!< stiffness
    Vec          F;              //!< force 
    Mat          LHSmat;         //!< composite matrix
    Vec          RHS;            //!< RHS vector
    Vec          A1, A2;         //!< intermediate solutions
    Vec          dnp;            //!< predictor displacement
    Vec          MAn, Kdn, Kdnp; //!< Intermediate vectors           
    LinearSolver linSolver;      //!< Linear solver
  };
  
}
