
# add library
add_library(pfe_time_contact STATIC
		     pfe_TimeIntegrator_with_Contact.cpp	
		     pfe_GenAlpha_with_Contact.cpp
		     pfe_HHTAlpha_with_Contact.cpp
		     pfe_Newmark_with_Contact.cpp )

# headers
target_include_directories(pfe_time_contact PUBLIC
  				    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  				    ${PETSC_INCLUDE_DIRS})

# link
target_link_libraries(pfe_time_contact PUBLIC pfe_time)

# Add required flags
target_compile_features(pfe_time_contact PUBLIC ${pfe_COMPILE_FEATURES})

install(FILES
		pfe_TimeIntegrator_with_Contact.h
		pfe_GenAlpha_with_Contact.h
		pfe_HHTAlpha_with_Contact.h
		pfe_Newmark_with_Contact.h
		DESTINATION ${PROJECT_NAME}/include)

