
/** \file pfe_HHTAlpha_with_Contact.cpp
 * \brief Implements the class pfe::HHTAlpha_with_Contact
 * \author Ramsharan Rangarajan
 */

#include <pfe_HHTAlpha_with_Contact.h>

namespace pfe
{
  // Constructor
  HHTAlpha_with_Contact::HHTAlpha_with_Contact(const double aleph, const int ndof, const double delta)
    :TimeIntegrator_with_Contact(0.25*(1.-aleph)*(1.-aleph), 0.5-aleph, ndof, delta),
     alpha(aleph),
     gamma(0.5-alpha),
     beta(0.25*(1.-alpha)*(1.-alpha)),
     tstep(delta) {}


  // Algorithm specific matrix operator
  void HHTAlpha_with_Contact::ComputeAlgorithmicLHSMat(const Mat& m, const Mat& k, Mat& mat)
  {
    // A = M + (1+alpha)*beta*dt^2 K
    PetscErrorCode ierr;
    ierr = MatDuplicate(m, MAT_COPY_VALUES, &mat);                                  CHKERRV(ierr);
    ierr = MatSetOption(mat, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);                CHKERRV(ierr);
    ierr = MatAXPY(mat, (1.+alpha)*beta*tstep*tstep, k, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);

    // done
    return;
  }


  // Algorithm specific RHS vector
  void HHTAlpha_with_Contact::ComputeAlgorithmicRHSVec(const Vec& f, const Vec& kdn,
						       const Vec& kdnp, const Vec& man, Vec& rhs)
  {
    // Solve rhs = F-(1+alpha)*Kdnp+alpha*Kdn
    PetscErrorCode ierr;
    ierr = VecCopy(f, rhs);                 CHKERRV(ierr);
    ierr = VecAXPY(rhs, -(1.+alpha), kdnp); CHKERRV(ierr);
    ierr = VecAXPY(rhs, alpha, kdn);        CHKERRV(ierr);

    // done
    return;
  }

  // Compute the contact force
  double HHTAlpha_with_Contact::
  ComputeAlgorithmicContactForce(const double& r_dot_A1, const double& r_dot_A2,
				 const double& r_dot_dn, const double& r_dot_dnp) const
  {
    return (-r_dot_dnp/(beta*tstep*tstep)-r_dot_A1)/r_dot_A2;
  }

}
