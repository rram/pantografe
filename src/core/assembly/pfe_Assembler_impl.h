
/** \file pfe_Assembler_impl.h
 * \brief Implements the class pfe::Assembler
 * \author Ramsharan Rangarajan
 * Last modified: September 28, 2021
 */

#ifndef PFE_ASSEMBLER_IMPL_H
#define PFE_ASSEMBLER_IMPL_H

namespace pfe
{
  // Implementation of Assembler<T>

  template<class T> void Assembler<T>::
    ElmDataStruct::Resize(const std::vector<int>& dofs)
    {  
      const int nFields = static_cast<int>(dofs.size());
      int nTotalDofs = 0;
      funcval.resize(nFields);
      dfuncval.resize(nFields);
      for(int f=0; f<nFields; ++f)
	{
	  funcval[f].resize( dofs[f] );
	  dfuncval[f].resize( dofs[f] );
	  nTotalDofs += dofs[f];
      
	  for(int a=0; a<dofs[f]; ++a)
	    {
	      dfuncval[f][a].resize( nFields );
	      for(int g=0; g<nFields; ++g)
		dfuncval[f][a][g].resize( dofs[g] );
	    }
	}
      indices.resize(nTotalDofs);
      resvals.resize(nTotalDofs);
      dresvals.resize(nTotalDofs*nTotalDofs);
      return;
    }

  // Zeroes out elemental vectors
  template<class T> void
    Assembler<T>::ElmDataStruct::ZeroRes()
    {
      for(auto& v:funcval)
	for(auto& w:v)
	  w = 0.;
    }

  // Zeros out elemental matrices
  template<class T> void
    Assembler<T>::ElmDataStruct::ZeroDRes()
    {
      for(auto& u:dfuncval)
	for(auto& v:u)
	  for(auto& w:v)
	    for(auto& x:w)
	      x = 0.;
    }


  // Constructor
  template<class T> Assembler<T>::
    Assembler(std::vector<T*>& ops,
	      const LocalToGlobalMap& l2gmap)
    :ElOps_(ops), L2GMap_(l2gmap)
  {
    assert(ElOps_.size()>0 &&
	   "Assembler::Assembler()- Operation array is empty.");

    // Use the fields/dofs from first operation for data structure allocations.
    const auto* op = ElOps_[0];
  
    // Fields
    fields_ = op->GetFields();
    const int nFields = static_cast<int>(fields_.size());

    // Dofs for each field and the total sum
    nDofsPerOp_ = 0;
    dofs_.resize(nFields);
    for(int f=0; f<nFields; ++f)
      {
	dofs_[f] = op->GetFieldDof(fields_[f]);
	nDofsPerOp_ += dofs_[f];
      }

    // Element-level data structues
    ElmDs_.Resize(dofs_);
  }


  // Main functionality. Assembles the residual.
  template<class T> void
    Assembler<T>::Assemble(const void* State, Vec& ResVec)
    {
      PetscErrorCode ierr;
  
      // Number of operations
      const int nOps = static_cast<int>(ElOps_.size());

      // Number of fields
      const int nFields = static_cast<int>(fields_.size());
  
      // Convenient aliases
      auto& funcval = ElmDs_.funcval;
      auto& indices = ElmDs_.indices;
      auto& resvals = ElmDs_.resvals;
  
      // Loop over operations
      for(int e=0; e<nOps; ++e)
	{
	  // Compute the element residual
	  ElOps_[e]->GetVal(State, &funcval);

	  // Flatten residue
	  for(int f=0, i=0; f<nFields; ++f)
	    for(int a=0; a<dofs_[f]; ++a, ++i)
	      {
		indices[i] =  L2GMap_.Map(fields_[f], a, e);
		resvals[i] = funcval[f][a];
	      }

	  // Assemble into global residue vector
	  ierr = VecSetValues(ResVec, nDofsPerOp_, &indices[0], &resvals[0], ADD_VALUES);
	  CHKERRV(ierr);
	}
  
      // Finish up assembly
      VecAssemblyBegin(ResVec);
      VecAssemblyEnd(ResVec);
  
      // Done
      return;
    }

  // Main functionality. Assembles stiffness
  template<class T> void
    Assembler<T>::Assemble(const void* State, Mat& DResMat)
    {
      PetscErrorCode ierr;

      // Number of operations
      const int nOps = static_cast<int>(ElOps_.size());
      
      // Number of fields
      const int nFields = static_cast<int>(fields_.size());
  
      // Convenient aliases
      auto& dfuncval = ElmDs_.dfuncval;
      auto& indices = ElmDs_.indices;
      auto& dresvals = ElmDs_.dresvals;
  
  
      // Loop over operations
      for(int e=0; e<nOps; ++e)
	{
	  // Compute the element residue and stiffness
	  ElOps_[e]->GetDVal(State, nullptr, &dfuncval);
	  
	  // Flatten residue and stiffness
	  for(int f=0, i=0, j=0; f<nFields; ++f)
	    for(int a=0; a<dofs_[f]; ++a, ++i)
	      {
		indices[i] = L2GMap_.Map(fields_[f], a, e);
	    
		for(int g=0; g<nFields; ++g)
		  for(int b=0; b<dofs_[g]; ++b, ++j)
		    dresvals[j] = dfuncval[f][a][g][b];
	      }

	  // Assemble stiffness into global matrix
	  ierr = MatSetValuesLocal(DResMat, nDofsPerOp_, &indices[0], nDofsPerOp_, &indices[0], &dresvals[0], ADD_VALUES); CHKERRV(ierr);
	}

      // Finish up assembly
      MatAssemblyBegin(DResMat, MAT_FINAL_ASSEMBLY);
      MatAssemblyEnd(DResMat, MAT_FINAL_ASSEMBLY);
  
      // Done
      return;
    }


  // Main functionality. Assembles residual and stiffness
  template<class T> void
    Assembler<T>::Assemble(const void* State, Vec& ResVec, Mat& DResMat)
    {
      PetscErrorCode ierr;

      // Number of operations
      const int nOps = static_cast<int>(ElOps_.size());

      // Number of fields
      const int nFields = static_cast<int>(fields_.size());
  
      // Convenient aliases
      auto& funcval = ElmDs_.funcval;
      auto& dfuncval = ElmDs_.dfuncval;
      auto& indices = ElmDs_.indices;
      auto& resvals = ElmDs_.resvals;
      auto& dresvals = ElmDs_.dresvals;
  
  
      // Loop over operations
      for(int e=0; e<nOps; ++e)
	{
	  // Compute the element residue and stiffness
	  ElOps_[e]->GetDVal(State, &funcval, &dfuncval);

	  // Flatten residue and stiffness
	  for(int f=0, i=0, j=0; f<nFields; ++f)
	    for(int a=0; a<dofs_[f]; ++a, ++i)
	      {
		resvals[i] = funcval[f][a];
		indices[i] = L2GMap_.Map(fields_[f], a, e);
	    
		for(int g=0; g<nFields; ++g)
		  for(int b=0; b<dofs_[g]; ++b, ++j)
		    dresvals[j] = dfuncval[f][a][g][b];
	      }

	  // Assemble residue into global vector
	  ierr = VecSetValues(ResVec, nDofsPerOp_, &indices[0], &resvals[0], ADD_VALUES);
	  CHKERRV(ierr);

	  // Assemble stiffness into global matrix
	  ierr = MatSetValuesLocal(DResMat, nDofsPerOp_, &indices[0], nDofsPerOp_, &indices[0], &dresvals[0], ADD_VALUES); CHKERRV(ierr);
	}

      // Finish up assembly
      VecAssemblyBegin(ResVec);
      VecAssemblyEnd(ResVec);
      MatAssemblyBegin(DResMat, MAT_FINAL_ASSEMBLY);
      MatAssemblyEnd(DResMat, MAT_FINAL_ASSEMBLY);
  
      // Done
      return;
    }


  // Count number of nonzeros
  template<class T> void
    Assembler<T>::CountNonzeros(std::vector<int>& nz) const
    {
      // We need the number of nonzeros.
      // Count relations between degrees of freedom
      // Assume that all degrees of freedom in one element are related
      const int ndof = L2GMap_.GetTotalNumDof();
      nz.resize(ndof);
      std::fill(nz.begin(), nz.end(), 0);
  
      const int nElements = static_cast<int>(L2GMap_.GetNumElements());
      std::vector<std::set<int>> dofrelations(ndof);
      int DofPerField;
      for(int e=0; e<nElements; ++e)
	{
	  // Visit each dof within this element
	  std::set<int> ElmDofs;
	  const int nFields = L2GMap_.GetNumFields(e);
	  for(int f=0; f<nFields; ++f)
	    {
	      DofPerField = L2GMap_.GetNumDofs(e,f);
	      for(int a=0; a<DofPerField; ++a)
		ElmDofs.insert( L2GMap_.Map(f,a,e) );
	    }
	
	  for(auto& itA:ElmDofs)
	    for(auto& itB:ElmDofs)
	      dofrelations[itA].insert( itB );
	}

      // Count the number of nonzeros.
      for(int i=0; i<ndof; ++i)
	nz[i] = static_cast<int>(dofrelations[i].size());
    }
  
}

#endif
