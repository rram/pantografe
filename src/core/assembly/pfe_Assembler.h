
/** \file pfe_Assembler.h
 * \brief Defines the class pfe::Assembler
 * \author Ramsharan Rangarajan
 */

#ifndef PFE_ASSEMBLER_H
#define PFE_ASSEMBLER_H

#include <pfe_ElementalOperation.h>
#include <pfe_LocalToGlobalMap.h>
#include <cassert>
#include <set>
#include <petscmat.h>
#include <petscvec.h>

namespace pfe
{
  //! \brief Assembly of finite element data structures using PETSc
  //! Assumes that each operation has identical fields/dofs.
  //! \ingroup assembly
  template<class T>
    class Assembler
    {
    public:
      //! Constructor
      //! \param[in] ops Operations for which to assemble. referenced.
      //! \param[in] l2gmap Local to global map, referenced
      Assembler(std::vector<T*>& ops, const LocalToGlobalMap& l2gmap);

      //! Destructor
      inline virtual ~Assembler() {}

      // Disable copy and assignment
      Assembler(const Assembler<T>&) = delete;
      Assembler<T>& operator=(const Assembler<T>&) = delete;
      
      //! Returns the element operations
      inline const std::vector<T*>& GetOperations() const
      { return ElOps_; }
  
      //! Return the local to global map
      inline const LocalToGlobalMap& GetLocalToGlobalMap() const
      { return L2GMap_; }

      //! Main functionality. Assembles the residual.
      //! \param[in] State Configuration at which to assemble
      //! \param[out] ResVec assembled residual vector
      void Assemble(const void* State, Vec& ResVec);

      //! Main functionality. Assemble the stiffness.
      //! \param[in] State Configuration at which to assemble
      //! \param[out] DResMat assembled stiffness matrix
      void Assemble(const void* State, Mat& DResMat);
      
      //! Main functionality. Assembles residual and stiffness
      //! \param[in] State Configuration at which to assemble
      //! \param[out] ResVec assembed residual vector
      //! \param[out] DResMat assembled stiffness matrix
      void Assemble(const void* State, Vec& ResVec, Mat& DResMat);

      //! Estimates the number of non-zeros for sparsity
      //! Assumes that all dofs within each element are related to each other.
      //! \param[out] nz Computed number of nonzeros.
      void CountNonzeros(std::vector<int>& nz) const;

    protected:

      //! Local data structures used during assembly
      struct ElmDataStruct
      {
	//! Helper method to resize vectors
	void Resize(const std::vector<int>& dofs);
	//! Zeroes out elemental vectors
	void ZeroRes();
	//! Zeros out elemental matrices
	void ZeroDRes();
	std::vector<int> indices;
	std::vector<double> resvals, dresvals;
	std::vector<std::vector<double>> funcval;
	std::vector<std::vector<std::vector<std::vector<double>>>> dfuncval;
      };
  
      std::vector<T*>& ElOps_; //!< Reference to operations vector
      const LocalToGlobalMap& L2GMap_; //!< reference to local 2 global map
      std::vector<int> fields_; //!< Fields for this set of operations
      std::vector<int> dofs_; //!< Number of dofs corresponding to each field
      int nDofsPerOp_; //!< Total number of entries per operation.
      ElmDataStruct ElmDs_; //!< Local data structures used in each operation
    };
}

#endif

// Implementation
#include <pfe_Assembler_impl.h>
