/** \file pfe_Quadrature.h
 * \brief Defines the class pfe::Quadrature
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <vector>
#include <cassert>

namespace pfe
{
  //! \ingroup element
  class Quadrature
  {
  public:
    //! @param NQ  number of quadrature points
    //! @param NC  number of coordinates for each point
    //! @param xqdat vector with coordinates of the quadrature points
    //! xqdat[a*NC+i] gives the i-th coordinate of quadrature point a
    //! @param wqdat vector with quadrature point weights
    //! wq[a] is the weight of quadrature point a
    //! This constructor sets the two sets of coordinates for the quadrature points to 
    //! be the same.
    inline Quadrature(const std::vector<double>& xqdat, 
		      const std::vector<double>& wqdat, 
		      const int NC,
		      const int NQ)
      :NumberOfMapCoordinates(NC),
      NumberOfQuadraturePoints(NQ),
      xqmap(NC*NQ),
      wq(NQ)
      {
	assert((NQ>=0 && NC>=0) && "Quadrature::Quadrature: unexpected number of quadrature points");
	for(int q=0; q<NQ; ++q)
	  {
	    for(int i=0; i<NC; ++i)
	      xqmap[q*NC+i] = xqdat[q*NC+i];
	    wq[q] = wqdat[q];
	  }
      }

    inline virtual ~Quadrature() {}

    //! Disable copy and assignment
    Quadrature(const Quadrature&) = delete;
    Quadrature& operator=(const Quadrature&) = delete;

    // Accessors/Mutators
    inline int GetNumberQuadraturePoints() const { return NumberOfQuadraturePoints; }

    //! Returns the number of map coordinates 
    inline int GetNumberOfCoordinates() const { return NumberOfMapCoordinates; }

    //! Return map coordinates of quadrature point q  
    inline const double*  GetQuadraturePoint(int q) const 
    { return &xqmap[q*NumberOfMapCoordinates]; } 

    //! Returns weight of quadrature point q
    inline double GetQuadratureWeights(int q) const { return wq[q]; } 

  private:
    std::vector<double> xqmap;
    std::vector<double> wq;
    int       NumberOfMapCoordinates;
    int       NumberOfQuadraturePoints;
  };
}

