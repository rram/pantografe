/** \file pfe_SegmentGeometry.h
 * \brief Defines the class pfe::SegmentGeometry
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_ElementGeometry.h>
#include <cmath>
#include <cassert>

namespace pfe
{
  /**
     \brief SegmentGeometry: Geometry of straight segments 
   
     A Segment is:\n
     1) A set of indices that describe the connectivity of the segment, 
     properly oriented. The coordinates
     are not stored in the element but wherever the application decides\n
     2) An affine map from a one-dimensional segment (parametric configuration) 
     with length 1 to the convex  
     hull of the two vertices. Segments embedded in two- and three-dimensional space 
     are hence easily handled. \n
   
     The parametric configuration is the segment (0,1).\n
     The parametric coordinate used is the distance to 0.
     
     \ingroup core
  */

  template<int spatial_dimension> 
    class SegmentGeometry: public ElementGeometry
    {
    public:
      //! Constructor
      inline SegmentGeometry(const int i1, const int i2)
      {
	Connectivity.push_back(i1);
	Connectivity.push_back(i2);
      }
  
      inline virtual ~SegmentGeometry(){}

      //! Disable copy and assignment
      SegmentGeometry(const SegmentGeometry&) = delete;
      SegmentGeometry<spatial_dimension>& operator=(const SegmentGeometry<spatial_dimension>&) = delete;
  
      inline int GetNumberOfVertices() const override
      { return 2; }

      inline const std::vector<int>& GetConnectivity() const override
      { return Connectivity; }

      inline int GetParametricDimension() const override
      { return 1; }

      inline int GetEmbeddingDimension() const override
      { return spatial_dimension; }

      //! @param X first parametric coordinate
      //! @param Y Output the result of the map
      void Map(const double * X, double *Y) const override;

      //! @param X first parametric coordinate
      //! @param DY Output the derivative of the map
      //! @param Jac Output the jacobian of the map
      void DMap(const double * X, double *DY, double &Jac) const override; 

      //! Set the only static member of the class GlobalCoordinateArray
      //! @param coord pointer to the array of all vertex coordinates
      inline static void SetGlobalCoordinatesArray(const std::vector<double> & coord) 
      { GlobalCoordinatesArray = &coord; }

    protected:
      //! Interface to avoid explicitly using GlobalCoordinatesArray everywhere, 
      //! since the latter is likely 
      //! to change later on
      //! @param a local node number
      //! @param i coordinate number
      virtual double  GetCoordinate(const int a, const int i) const
      { return (*GlobalCoordinatesArray)
	  [(GetConnectivity()[a]-1)*spatial_dimension+i]; }
  
    private:
      static const std::vector<double> *GlobalCoordinatesArray;  //<! Pointer to the global coordinates array
      std::vector<int> Connectivity;
  
    };

  
  // Class implementation
  template<int spatial_dimension> 
    const std::vector<double>* SegmentGeometry<spatial_dimension>::GlobalCoordinatesArray = nullptr;

  template<int spatial_dimension> 
    void SegmentGeometry<spatial_dimension>::Map(const double* X, double* Y) const
    {
      for(int i=0; i<spatial_dimension; ++i)
	Y[i] = X[0]*GetCoordinate(0,i) + (1.-X[0])*GetCoordinate(1,i);
  
      return;
    }

  template<int spatial_dimension> 
    void SegmentGeometry<spatial_dimension>::DMap(const double* X, double* DY, double& Jac) const
    {
      for(int i=0; i<spatial_dimension; ++i)
	DY[i] =  GetCoordinate(0,i) - GetCoordinate(1,i);
  
      double g11=0.;

      for(int i=0; i<spatial_dimension; ++i)
	g11 += DY[i]*DY[i];
  
      Jac=std::sqrt(g11);
  
      return;
    }

}

