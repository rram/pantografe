/** \file pfe_HermiteShape.cpp
 * \brief Implementation of the class pfe::HermiteShape
 * \author Ramsharan Rangarajan
 * Last modified: September 13, 2021
 */

#include <pfe_HermiteShape.h>
#include <cmath>
#include <cassert>

namespace pfe
{
  extern "C" void dgesv_(int *, int *, double *, int *, int *, double *, int *, int *);

  //! Constructor
  HermiteShape::HermiteShape(const double left, const double right)
    :ShapeFunction(),
     elmcoord{left, right}
  {
    // Use petsc to invert a matrix to find the coordinates
    double A[16] = {1, left, std::pow(left,2.), std::pow(left,3.),      // for value at left end
		    1, right, std::pow(right,2.), std::pow(right,3.),   // for value at right end
		    0., 1., 2.*left, 3.*std::pow(left,2.),              // for value of derivative at left
		    0., 1., 2.*right, 3.*std::pow(right,2.) };
      
    // Invert Matrix A:
    double RHS[16] = {1.,0.,0.,0., 
		      0.,1.,0.,0.,
		      0.,0.,1,0.,
		      0.,0.,0.,1.};
    int N = 4, nrhs = 4, lda = 4, ldb = 4;
    int ipiv[4];
    int info;
    dgesv_(&N, &nrhs, A, &lda, ipiv, RHS, &ldb, &info);
    assert(info>=0 && "pfe::HermiteShape: Could not invert matrix to find coefficients");
  
    // Save the coefficient matrix
    // Modify shape functions 2 and 3 based on prescribed slope
    double presval[] = {1.,1., 1., 1. };
    for(int a=0; a<4; a++)
      for(int i=0; i<4; i++)
	Coef[a][i] = RHS[4*i+a]*presval[a];
  }


  // Computes the value of requested shape function at given point.
  double HermiteShape::Val(const int a,  const double *x) const
  {
    double y = elmcoord[0]*x[0] + elmcoord[1]*(1.-x[0]); 
    double val = 0.;
    for(int i=0; i<4; i++)
      val += Coef[a][i]*std::pow(y,static_cast<double>(i));
    return val;
  }


  // Computes the derivative of the requested shape function in the requested direction at given point.
  double HermiteShape::DVal(const int a, const double *x, const int i) const
  {
    assert(i==0 && "pfe::HermiteShape::DVal- Unexpected derivative number requested");
    double y = elmcoord[0]*x[0] + elmcoord[1]*(1.-x[0]);

    // Derivative wrt cartesian coordinates
    double dval = Coef[a][1]*1.0 + Coef[a][2]*2.0*y + Coef[a][3]*3.0*y*y;

    // scaling to make derivative wrt barycentric coordinate.
    return (elmcoord[0]-elmcoord[1])*dval;   
  }


  // Computes the 2nd derivative
  double HermiteShape::D2Val(const int a, const double* x) const
  {
    double y = elmcoord[0]*x[0] + elmcoord[1]*(1.-x[0]);

    // Derivative
    double d2val = Coef[a][2]*2. + Coef[a][3]*6.*y;

    // scaling to make derivative wrt barycentric coordinate.
    return (elmcoord[0]-elmcoord[1])*(elmcoord[0]-elmcoord[1])*d2val;   
  }

  // Consistency test for Val, DVal and D2Val
  bool HermiteShape::ConsistencyTest(const double *x, const double pertTOL, const double resTOL) const
  {
    ShapeFunction::ConsistencyTest(x, pertTOL, resTOL);

    // Check interpolation of functions
    const double one = 1.;
    assert(std::abs(Val(0,&one)-1.)+std::abs(Val(1,&one)-0.)+
	   std::abs(Val(2,&one)-0.)+std::abs(Val(3,&one)-0.)<resTOL);

    const double zero = 0.;
    assert(std::abs(Val(0,&zero)-0.)+std::abs(Val(1,&zero)-1.)+
	   std::abs(Val(2,&zero)-0.)+std::abs(Val(3,&zero)-0.)<resTOL);

    // Check interpolation of derivatives
    assert(std::abs(DVal(0,&one,0)-0.)+std::abs(DVal(1,&one,0)-0.)+
	   +std::abs(DVal(3,&one,0)-0.)<resTOL);
    const double Jac = elmcoord[0]-elmcoord[1];
    
    assert(std::abs(DVal(0,&zero,0-0.))+std::abs(DVal(1,&zero,0)-0.)+
	   std::abs(DVal(2,&zero,0)-0.)<resTOL);

    assert(std::abs(DVal(2,&one,0)/Jac-1.)+std::abs(DVal(3,&zero,0)/Jac-1.)<resTOL);
    
    // Check consistency of second derivatives
    double xpert;
    double dval_plus, dval_minus;
    double d2val, num_d2val;
    for(int a=0; a<4; ++a)
      {
	d2val = D2Val(a, x);

	xpert = x[0]+pertTOL;
	dval_plus = DVal(a, &xpert, 0);
	xpert = x[0]-pertTOL;
	dval_minus = DVal(a, &xpert, 0);
	num_d2val = (dval_plus-dval_minus)/(2.*pertTOL);

	assert(std::abs(d2val-num_d2val)<resTOL);
      }
    
    return true;
  }

  
}  
