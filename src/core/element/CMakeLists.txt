
# add library
add_library(pfe_element STATIC
  pfe_LineQuadratures.cpp
  pfe_ShapeFunction.cpp
  pfe_ElementGeometry.cpp
  pfe_BasisFunctions.cpp
  pfe_HermiteShape.cpp)

# headers
target_include_directories(pfe_element PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  ${GSL_INCLUDE_DIRS})

# link
target_link_libraries(pfe_element PUBLIC ${LAPACK_LIBRARIES})

# Add required flags
target_compile_features(pfe_element PUBLIC ${pfe_COMPILE_FEATURES})

install(FILES
	pfe_BasisFunctions.h
	pfe_Element.h
	pfe_ElementGeometry.h
	pfe_H31DElement.h
	pfe_HermiteShape.h
	pfe_LineQuadratures.h
	pfe_LocalToGlobalMap.h
	pfe_OffsetLocalToGlobalMap.h
	pfe_Quadrature.h
	pfe_SegmentGeometry.h
	pfe_ShapeFunction.h
        DESTINATION ${PROJECT_NAME}/include)
