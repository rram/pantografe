/** \file pfe_BasisFunctions.cpp
 * \brief Implements the class pfe::BasisFunctions
 * \author Ramsharan Rangarajan
 * Last modified: September 12, 2021
 */


#include <pfe_BasisFunctions.h>
#include <cassert>

namespace pfe
{
  extern "C" void dgesv_(int *, int *, double *, int *, int *, double *, int *, int *);

  void BasisFunctions::CreateObject(const Quadrature* TQuadrature,
				    const ShapeFunction* TShape, 
				    const ElementGeometry* EG)
  {
    const int Na = TShape->GetNumberOfFunctions();
    const int Nq = TQuadrature->GetNumberQuadraturePoints();
    const int Nd = EG->GetEmbeddingDimension();
    int Np = EG->GetParametricDimension();

    LocalShapes.resize(Nq*Na);
    if(Np==Nd)  LocalDShapes.resize(Nq*Na*Nd);
    LocalWeights.resize(Nq);
    LocalCoordinates.resize(Nq*Nd);
  
    double *DY = new double[Nd*Np];
    double *Y  = new double[Nd];
  
    for(int q=0; q<Nq; ++q)
      {
	EG->Map(TQuadrature->GetQuadraturePoint(q), Y);
	for(int i=0; i<Nd ; i++) 
	  LocalCoordinates[q*Nd+i] = Y[i];
      
	for(int a=0; a<Na ; a++)
	  LocalShapes[q*Na+a] = TShape->Val(a, TQuadrature->GetQuadraturePoint(q));

	double Jac;
	EG->DMap(TQuadrature->GetQuadraturePoint(q), DY, Jac);

	LocalWeights[q] = (TQuadrature->GetQuadratureWeights(q))*Jac;

	// Compute derivatives of shape functions only when the element map goes
	// between spaces of the same dimension
	if(Np==Nd)
	  {
	    double *DYInv = new double[Np*Np];
	    double *DYT   = new double[Np*Np];
            
	    // Lapack
	    {
	      // Transpose DY to Fortran mode
	      for(int k=0; k<Nd; k++)
		for(int M=0; M<Np; M++)
		  {
		    DYT[k*Nd+M] = DY[M*Np+k];

		    //Right hand-side
		    DYInv[k*Nd+M] = k==M?1:0; 
		  }

	      int    *IPIV = new int[Np];
	      int    INFO;

	      // DYInv contains the transpose of the inverse
	      dgesv_(&Np, &Np, DYT, &Np, IPIV, DYInv, &Np, &INFO);
	    
	      assert( (INFO==0) && "BasisFunctions::CreateObject: Lapack could not invert matrix" );
	    
	      delete[] DYT;
	      delete[] IPIV;
	    }
	  
	    for(int a=0; a<Na; ++a)
	      for(int i=0; i<Nd; ++i) 
		{
		  LocalDShapes[q*Na*Nd+a*Nd+i] = 0;
		  for(int M=0; M<Np; ++M)
		    LocalDShapes[q*Na*Nd+a*Nd+i] += 
		      TShape->DVal(a, TQuadrature->GetQuadraturePoint(q), M)
		      *DYInv[M*Np+i];
		}
	  
	    delete[] DYInv;
	  }
      }
    delete[] DY;
    delete[] Y;
  }

}
