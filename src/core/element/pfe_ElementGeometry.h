/** \file pfe_ElementGeometry.h
 * \brief Defines the abstract class pfe::ElementGeometry
 * \author Ramsharan Rangarajan
 */


#ifndef PFE_ELEMENT_GEOMETRY_H
#define PFE_ELEMENT_GEOMETRY_H

#include <string>
#include <vector>

namespace pfe
{
  /**
     \brief ElementGeometry: Defines the geometry of the polytope over which the
     interpolation takes place

     \ingroup element
  */
  class ElementGeometry
  {
  public:
    inline ElementGeometry(){}
    inline virtual ~ElementGeometry(){}

    //! Disable copy and assignment
    ElementGeometry(const ElementGeometry&) = delete;
    ElementGeometry& operator=(const ElementGeometry&) = delete;

    virtual int GetNumberOfVertices() const = 0; 

    //!< Vertices of the polytope. 
    virtual const std::vector<int>& GetConnectivity() const = 0; 

    //! Number of dimensions in parametric configuration
    virtual int GetParametricDimension() const = 0; 

    //! Number of dimensions in the real configuration
    virtual int GetEmbeddingDimension() const = 0;

    //! Map from parametric to real configuration
    //! @param X parametric coordinates
    //! @param Y returned real coordinates
    virtual void Map(const double* X, double* Y) const = 0; 
  
    //! Derivative of map from parametric to real configuration
    //! @param X parametric coordinates.
    //! @param Jac returns absolute value of the Jacobian of the map.
    //! @param DY  returns derivative of the map. 
    //! Here DY[a*GetEmbeddingDimension()+i] 
    //! contains the derivative in the a-th direction
    //! of the i-th coordinate.
    virtual void DMap(const double* X, double* DY, double& Jac) const = 0; 

    //! Consistency test for Map and its derivative
    //! @param X parametric coordinates at which to test
    //! @param Pert size of the perturbation with which to compute numerical 
    //! derivatives (X->X+Pert)
    bool ConsistencyTest(const double* X, const double pertTOL, const double resTOL) const;
  };
  
}

#endif
