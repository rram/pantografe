
/** \file test_H31DElement.cpp
 * \brief Unit test for class pfe::H31DElement
 * \author Ramsharan Rangarajan
 * \core_elm_test Unit test for class pfe::H31DElement
 */

#include <pfe_H31DElement.h>
#include <cassert>
#include <random>
#include <iostream>

using namespace pfe;

template<int NFields>
void Test(const H31DElement<NFields>&);

int main()
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-1.,1.);
  for(int trial=0; trial<100; ++trial)
    {
      // Coordinates
      std::vector<double> gcoords{dis(gen), 2.+dis(gen)};
      SegmentGeometry<1>::SetGlobalCoordinatesArray(gcoords);
      
      // 1 field test
      H31DElement<1> elm_1(1,2);
      Test(elm_1);

      // 2 field test
      H31DElement<2> elm_2(1,2);
      Test(elm_2);

      // 3 field test
      H31DElement<3> elm_3(1,2);
      Test(elm_3);
    }
}

void N1(const double x0, const double x1, const double x,
	double& val, double& dval, double& d2val)
{
  val = -((std::pow(x - x1,2)*(2*x - 3*x0 + x1))/std::pow(x0 - x1,3));
  dval = (-2*std::pow(x - x1,2))/std::pow(x0 - x1,3) - (2*(x - x1)*(2*x - 3*x0 + x1))/std::pow(x0 - x1,3);
  d2val = (-8*(x - x1))/std::pow(x0 - x1,3) - (2*(2*x - 3*x0 + x1))/std::pow(x0 - x1,3);
}

void N2(const double x0, const double x1, const double x,
	double& val, double& dval, double& d2val)
{
  val = (std::pow(x - x0,2)*(2*x + x0 - 3*x1))/std::pow(x0 - x1,3);
  dval = (2*std::pow(x - x0,2))/std::pow(x0 - x1,3) + (2*(x - x0)*(2*x + x0 - 3*x1))/std::pow(x0 - x1,3);
  d2val = (8*(x - x0))/std::pow(x0 - x1,3) + (2*(2*x + x0 - 3*x1))/std::pow(x0 - x1,3);
}

void N3(const double x0, const double x1, const double x,
	double& val, double& dval, double& d2val)
{
  val = ((x - x0)*std::pow(x - x1,2))/std::pow(x0 - x1,2);
  dval = (2*(x - x0)*(x - x1))/std::pow(x0 - x1,2) + std::pow(x - x1,2)/std::pow(x0 - x1,2);
  d2val = (2*(x - x0))/std::pow(x0 - x1,2) + (4*(x - x1))/std::pow(x0 - x1,2);
}

void N4(const double x0, const double x1, const double x,
	double& val, double& dval, double& d2val)
{
  val = (std::pow(x - x0,2)*(x - x1))/std::pow(x0 - x1,2);
  dval = std::pow(x - x0,2)/std::pow(x0 - x1,2) + (2*(x - x0)*(x - x1))/std::pow(x0 - x1,2);
  d2val = (4*(x - x0))/std::pow(x0 - x1,2) + (2*(x - x1))/std::pow(x0 - x1,2);
}


template<int NFields>
void Test(const H31DElement<NFields>& elm)
{
  const int nDof = 4;
  assert(elm.GetNumFields()==NFields);
  for(int f=0; f<NFields; ++f)
    { assert(elm.GetNumDof(f)==nDof);
      assert(elm.GetNumDerivatives(f)==1); }
  auto& fields = elm.GetFields();
  assert(static_cast<int>(fields.size())==NFields);
  for(int f=0; f<NFields; ++f)
    assert(fields[f]==f);

  // verify array sizes
  const int nQuad = static_cast<int>(elm.GetIntegrationWeights(fields[0]).size());
  for(int f=0; f<NFields; ++f)
    {
      assert(static_cast<int>(elm.GetIntegrationPointCoordinates(f).size())==nQuad);
      assert(static_cast<int>(elm.GetShape(f).size())==nQuad*nDof);
      assert(static_cast<int>(elm.GetDShape(f).size())==nQuad*nDof);
      assert(static_cast<int>(elm.GetD2Shape(f).size())==nQuad*nDof);
    }

  // check geometry
  auto& geom = elm.GetElementGeometry();
  assert(static_cast<int>(geom.GetConnectivity().size())==2);
  assert(geom.GetConnectivity()[0]==1);
  assert(geom.GetConnectivity()[1]==2);
  double xleft, xright, lambda;
  lambda = 0.; geom.Map(&lambda, &xright);
  lambda = 1.; geom.Map(&lambda, &xleft);
  const double len = xright-xleft;
  assert(len>0.);

  // quadrature weights should sum to the length
  for(int f=0; f<NFields; ++f)
    {
      double qsum = 0.;
      const auto& qwts = elm.GetIntegrationWeights(f);
      assert(static_cast<int>(qwts.size())==nQuad);
      for(int q=0; q<nQuad; ++q)
	qsum += qwts[q];
      assert(std::abs(qsum-len)<1.e-6);
    }

  // quadrature points should lie within the element
  for(int f=0; f<NFields; ++f)
    {
      const auto& qpts = elm.GetIntegrationPointCoordinates(f);
      assert(static_cast<int>(qpts.size())==nQuad);
      for(int q=0; q<nQuad; ++q)
	{ assert(qpts[q]>=xleft && qpts[q]<=xright); }
    }


  // Verify function values and derivatives (compare with Mathematica expressions implemented above)
  std::vector<double> Na(nDof), dNa(nDof), d2Na(nDof);
  for(int f=0; f<NFields; ++f)
    {
      const auto& qpts = elm.GetIntegrationPointCoordinates(f);
      for(int q=0; q<nQuad; ++q)
	{
	  N1(xleft, xright, qpts[q], Na[0], dNa[0], d2Na[0]);
	  N2(xleft, xright, qpts[q], Na[1], dNa[1], d2Na[1]);
	  N3(xleft, xright, qpts[q], Na[2], dNa[2], d2Na[2]);
	  N4(xleft, xright, qpts[q], Na[3], dNa[3], d2Na[3]);
	  for(int a=0; a<nDof; ++a)
	    {
	      assert(std::abs(elm.GetShape(f,q,a)-Na[a])<1.e-6);
	      assert(std::abs(elm.GetDShape(f,q,a,0)-dNa[a])<1.e-6);
	      assert(std::abs(elm.GetD2Shape(f,q,a,0,0)-d2Na[a])<1.e-6);
	    }
	}
    }
  
  // done
  return;
}
    
