
/** \file test_HermiteShape.cpp
 * \brief Unit test for 1D Hermite shape functions
 * \author Ramsharan Rangarajan
 * \core_elm_test Unit test for 1D Hermite shape functions
 */

#include <pfe_HermiteShape.h>
#include <cassert>
#include <random>
#include <iostream>

using namespace pfe;

int main()
{
  // Check function values + consistency of derivatives
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(0.,1.);
  for(int trial=0; trial<100; ++trial)
    {
      double t_left = dis(gen);
      double t_right = 1.+dis(gen);
      HermiteShape Shp(t_left, t_right);
      assert(Shp.GetNumberOfFunctions()==4);
      assert(Shp.GetNumberOfVariables()==1);

      // consistency
      for(int run=0; run<100; ++run)
	{
	  const double lambda = dis(gen);
	  Shp.ConsistencyTest(&lambda, 1.e-5, 1.e-4);
	}
    }

  // done
}
	 
