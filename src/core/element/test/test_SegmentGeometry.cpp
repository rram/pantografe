
/** \file test_SegmentGeometry.cpp
 * \brief Unit test for pfe::SegmentGeometry class
 * \author Ramsharan Rangarajan
 * \core_elm_test Unit test for pfe::SegmentGeometry class
 */

#include <pfe_SegmentGeometry.h>
#include <random>

using namespace pfe;

template<int SPD>
void TestSegmentGeometry(const SegmentGeometry<SPD>& geom);

int main()
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-1., 1.);

  for(int trial=0; trial<50; ++trial)
    {
      // 1D segment
      std::vector<double> gcoord_1(2);
      for(int a=0; a<2; ++a)
	{
	  gcoord_1[0] = dis(gen);
	  gcoord_1[1] = dis(gen)+2.;
	}
      SegmentGeometry<1>::SetGlobalCoordinatesArray(gcoord_1);
      SegmentGeometry<1> seg_1(1,2);
      TestSegmentGeometry(seg_1);

      // 2D segment
      std::vector<double> gcoord_2(4);
      for(int a=0; a<2; ++a)
	{
	  gcoord_2[0] = dis(gen);
	  gcoord_2[1] = dis(gen);
	  gcoord_2[2] = 2.+dis(gen);
	  gcoord_2[3] = 2.+dis(gen);
	}
      SegmentGeometry<2>::SetGlobalCoordinatesArray(gcoord_2);
      SegmentGeometry<2> seg_2(1,2);
      TestSegmentGeometry(seg_2);

      // 3D segment
      std::vector<double> gcoord_3(6);
      for(int a=0; a<2; ++a)
	{
	  gcoord_3[0] = dis(gen);
	  gcoord_3[1] = dis(gen);
	  gcoord_3[2] = dis(gen);
	  gcoord_3[3] = 2.+dis(gen);
	  gcoord_3[4] = 2.+dis(gen);
	  gcoord_3[5] = 2.+dis(gen);
	}
      SegmentGeometry<3>::SetGlobalCoordinatesArray(gcoord_3);
      SegmentGeometry<3> seg_3(1,2);
      TestSegmentGeometry(seg_3);
    }
  
  // done
}


template<int SPD>
void TestSegmentGeometry(const SegmentGeometry<SPD>& geom)
{
  assert(geom.GetNumberOfVertices()==2);
  assert(geom.GetParametricDimension()==1);
  assert(geom.GetEmbeddingDimension()==SPD);
  assert(static_cast<int>(geom.GetConnectivity().size())==2);
  
  // Consistency tests
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(0.,1.);
  for(int trial=0; trial<100; ++trial)
    {
      const double lambda = dis(gen);
      geom.ConsistencyTest(&lambda, 1.e-5, 1.e-4);
    }

  // done
  return;
}
