
/** \file test_LineQuadratures.cpp
 * \brief Unit test for quadrature rules on the segment [0,1]
 * \author Ramsharan Rangarajan
 * \core_elm_test Unit test for line quadratures
 */


#include <pfe_LineQuadratures.h>
#include <random>

using namespace pfe;

void TestRule(const Quadrature& qrule, const int nPoints);

int main()
{
  TestRule(LineQuadratures::Line_2pt, 2); 
  TestRule(LineQuadratures::Line_3pt, 3);
  TestRule(LineQuadratures::Line_4pt, 4);
  TestRule(LineQuadratures::Line_5pt, 5);
}


void TestRule(const Quadrature& qrule, const int nPoints)
{
  assert(qrule.GetNumberQuadraturePoints()==nPoints);
  assert(qrule.GetNumberOfCoordinates()==1);
  double wsum = 0.;
  for(int q=0; q<nPoints; ++q)
    {
      const double& qpt = *qrule.GetQuadraturePoint(q);
      const double qwt = qrule.GetQuadratureWeights(q);
      assert(qpt>=0. && qpt<=1.);
      assert(qwt>0. && qwt<1.);
      wsum += qwt;
    }
  assert(std::abs(wsum-1.)<1.e-4);

  // Check integration of polynomials with random coefficients
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-1., 1.);
  std::vector<double> coefs(2*nPoints-1); // x^0, x^1, x^2, ..

  for(int trial=0; trial<50; ++trial)
    {
      for(int a=0; a<2*nPoints-1; ++a)
	coefs[a] = dis(gen);

      // Exact integral
      double ExactIntegral = 0.;
      for(int a=0; a<2*nPoints-1; ++a)
	ExactIntegral += coefs[a]/static_cast<double>(a+1);

      // Integral computed with quadrature
      double numIntegral = 0.;
      for(int q=0; q<nPoints; ++q)
	{
	  double fval = 0.;
	  double qpt = *qrule.GetQuadraturePoint(q);
	  for(int a=0; a<2*nPoints-1; ++a)
	    fval += coefs[a]*std::pow(qpt, static_cast<double>(a));
	  double qwt = qrule.GetQuadratureWeights(q);
	  numIntegral += qwt*fval;
	}

      assert(std::abs(ExactIntegral-numIntegral)<1.e-4);
    }
  
  return;
}
