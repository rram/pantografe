
/** \file pfe_H31DElement.h
 * \brief Defines the class pfe::H31DElement and pfe::H31DL2GMap
 * \author Ramsharan Rangarajan
 */


#ifndef PFE_HERMITE_31D_ELEMENT_H
#define PFE_HERMITE_31D_ELEMENT_H

#include <pfe_Element.h>
#include <pfe_SegmentGeometry.h>
#include <pfe_HermiteShape.h>
#include <pfe_LineQuadratures.h>
#include <pfe_LocalToGlobalMap.h>
#include <cassert>

namespace pfe
{
  //! \ingroup element
  template<int NFields>
    class H31DElement: public Element
    {
    public:
      //! Constructor
      //! \param[in] i1,i2 Node numbers
      H31DElement(const int i1, const int i2);

      //! Destructor
      inline virtual ~H31DElement()
      { delete SegGeom; }

      // Disable copy and assignment
      H31DElement(const H31DElement<NFields>&) = delete;
      H31DElement<NFields>& operator=(const H31DElement<NFields>&) = delete;
      
      //! Access element geometry
      const SegmentGeometry<1>& GetElementGeometry() const override
      { return *SegGeom; }

    protected:
      //! Return the position in LocalShapes where shape functions for a 
      //! given field are saved.
      //! \param fieldnum Field number
      inline virtual int GetFieldIndex(int fieldnum) const override
      { return 0; }

    private:
      SegmentGeometry<1>* SegGeom; //!< Element geoemtry
    };


  // Implement constructor
  template<int NFields> H31DElement<NFields>::
    H31DElement(const int i1, const int i2)
    :Element()
    {
      // Create the segment geometry
      SegGeom = new SegmentGeometry<1>(i1, i2);

      // Shape functions
      double endpts[2];
      double lambda;
      lambda = 1.; SegGeom->Map(&lambda, &endpts[0]); // Left end point
      lambda = 0.; SegGeom->Map(&lambda, &endpts[1]); // Right end point
      HermiteShape Shp(endpts[0], endpts[1]);

      // Append fields
      for(int i=0; i<NFields; ++i)
	AppendField(i);
	    
      // Evaluate shape functions and 1st derivatives
      const auto& qrule = LineQuadratures::Line_5pt;
      BasisFunctions ShpEval(&qrule, &Shp, SegGeom);

      // Evaluate 2nd derivatives of shape functions
      const int nQuad = qrule.GetNumberQuadraturePoints();
      const int nShapes = Shp.GetNumberOfFunctions();
      std::vector<double> d2shapes(nShapes*nQuad);
      const double Jac2 = (endpts[1]-endpts[0])*(endpts[1]-endpts[0]);
      for(int q=0; q<nQuad; ++q)
	{
	  // This quadrature point (lambda)
	  const double* qlambda = qrule.GetQuadraturePoint(q);
	  // 2nd derivatives of shape functions
	  for(int a=0; a<nShapes; ++a)
	    {
	      //d2Na/dx^2 = d^2Na/dlambda^2 /Jac^2
	      d2shapes[q*nShapes+a] = Shp.D2Val(a,qlambda)/Jac2;
	    }
	}

      // Add basis functions
      BasisFunctions BF(ShpEval.GetQuadraturePointCoordinates(),
			ShpEval.GetIntegrationWeights(),
			ShpEval.GetShapes(),
			ShpEval.GetDShapes(),
			d2shapes);
      AddBasisFunctions(BF);
      
      // done	    
    }


  
  //! Class for local to global maps for Hermite elements
  class H31DL2GMap: public LocalToGlobalMap
  {
  public:
    //! Constructor
    //! \param EA Element array
    inline H31DL2GMap(const std::vector<Element*>& EA)
      :LocalToGlobalMap(),
      ElmArray(EA),
      NFields(ElmArray[0]->GetNumFields()),
      nDof(ElmArray[0]->GetNumDof(0))
	{
	  assert(nDof==4);
	  // Count the total number of nodes
	  nNodes = 0;
	  for(auto& Elm:ElmArray)
	    {
	      const auto& conn = Elm->GetElementGeometry().GetConnectivity();
	      for(auto& n:conn)
		if(n>nNodes)
		  nNodes = n;
	    }
	  nTotalDof = NFields*2*nNodes; // 2 dofs per node
	}
    
    //! Destructor
    inline virtual ~H31DL2GMap() {}

    //! Disable copy and assignment
    H31DL2GMap(const H31DL2GMap&) = delete;
    H31DL2GMap operator=(const H31DL2GMap&) = delete;

    //! Main functionality: Map dofs 0, 1 consecutively. Then map dofs 2 and 3.
    inline int Map(int field, const int dof, const int elm) const override
    {
      assert(field>=0 && field<NFields);
      assert(dof>=0 && dof<nDof);
      if(dof==0 || dof==1)
	{
	  const int nodenum = ElmArray[elm]->GetElementGeometry().GetConnectivity()[dof]-1;
	  return NFields*nodenum+field;
	}
      else
	{
	  const int nodenum = ElmArray[elm]->GetElementGeometry().GetConnectivity()[dof-2]-1;
	  return NFields*nNodes + NFields*nodenum + field;
	}
    }

    inline int GetNumElements() const override
    { return static_cast<int>(ElmArray.size()); }

    inline int GetNumFields(const int elm) const override
    { return NFields; }
  
    inline int GetNumDofs(const int elm, const int field) const override
    { return nDof; }
  
    //! Total number of dofs
    inline int GetTotalNumDof() const override
    { return nTotalDof; }

  private:
    const std::vector<Element*>& ElmArray;
    const int NFields;
    const int nDof;
    int nNodes;
    int nTotalDof;
  };

}


#endif
