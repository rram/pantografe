/** \file pfe_ElementGeometry.cpp
 * \brief Implements the class pfe::ElementGeometry
 * \author Ramsharan Rangarajan
 * Last modified: September 12, 2021
 */

#include <pfe_ElementGeometry.h>
#include <cmath>
#include <cassert>

namespace pfe
{
  bool ElementGeometry::ConsistencyTest(const double * X, const double pertTOL, const double resTOL) const
  {
    assert(pertTOL>0. && resTOL>0.);
      
    const int pDim = GetParametricDimension();
    const int eDim = GetEmbeddingDimension();
    std::vector<double> Xpert(pDim), Yplus(eDim), Yminus(eDim), dY(pDim*eDim);
    double Jac, dYnum;

    // Computed derivaties
    DMap(X, &dY[0], Jac);

    // Numerical derivatives
    Xpert.assign(X, X+pDim);
    for(int I=0; I<pDim; ++I)
      {
	Xpert[I] += pertTOL;
	Map(&Xpert[0], &Yplus[0]);
	Xpert[I] -= 2.*pertTOL;
	Map(&Xpert[0], &Yminus[0]);
	Xpert[I] += pertTOL;

	for(int j=0; j<eDim; ++j)
	  {
	    dYnum = (Yplus[j]-Yminus[j])/(2.*pertTOL);
	    assert(std::abs(dYnum-dY[I*eDim+j])<resTOL);
	  }
      }

    return true;
  }

}
