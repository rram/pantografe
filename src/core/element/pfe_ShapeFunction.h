/** \file pfe_ShapeFunction.h
 * \brief Defines the base class for shape functions pfe::ShapeFunction
 * \author Ramsharan Rangarajan
 * Last modified: September 12, 2021
 */

#pragma once

namespace pfe
{
  /**
     \brief base class for any set of basis (or shape) functions and its
     first derivatives

     A set of basis functions permits the evaluation of any of the functions 
     in the basis at any point

     \ingroup element
  */
  class ShapeFunction
  {
  public:
    inline ShapeFunction(){}
    inline virtual ~ShapeFunction(){}

    //! Disable copy and assignment
    ShapeFunction(const ShapeFunction &) = delete;
    ShapeFunction& operator=(const ShapeFunction&) = delete;
  
    // Accessors/Mutators
    virtual int GetNumberOfFunctions() const = 0; 
    virtual int GetNumberOfVariables() const = 0; //!< Number of arguments of the functions

    //! Value of shape \f$N_a(x)\f$
    //! @param x Barycentic coordinates
    virtual double Val(const int a, const double *x) const = 0; 
  
    //! Value of \f$\frac{\partial N_a}{\partial x_i}(x)\f$
    //! @param x Barycentric coordinates
    //! @param i coordinate number 
    virtual double DVal(const int a, const double *x, const int i) const = 0; 

    //! Consistency test for Val and DVal 
    //! @param x coordinates of the point at which to test
    //! @param Pert size of the perturbation with which to compute numerical 
    //! derivatives (x->x+Pert)
    virtual bool ConsistencyTest(const double *x, const double pertTOL, const double resTOL) const;
  };
  //! @}
}


