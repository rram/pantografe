/** \file pfe_ShapeFunction.cpp
 * \brief Implements the class pfe::ShapeFunction
 * \author Ramsharan Rangarajan
 * Last modified: September 12, 2021
 */

#include <pfe_ShapeFunction.h>
#include <vector>
#include <cmath>
#include <cassert>

namespace pfe
{
  bool ShapeFunction::ConsistencyTest(const double *X, const double pertTOL, const double resTOL) const
  {
    assert(pertTOL>0. && resTOL>0.);
    const int nVars = GetNumberOfVariables();
    const int nFuncs = GetNumberOfFunctions();

    // Check consistency of each shape function
    std::vector<double> Xpert(nVars);
    Xpert.assign(X, X+nVars);
    double dval, num_dval, val_plus, val_minus;

    for(int a=0; a<nFuncs; ++a)
      {
	for(int i=0; i<nVars; ++i)
	  {
	    // derivative
	    dval = DVal(a, X, i);
	    
	    Xpert[i] += pertTOL;
	    val_plus = Val(a, &Xpert[0]);
	    Xpert[i] -= 2.*pertTOL;
	    val_minus = Val(a, &Xpert[0]);
	    Xpert[i] += pertTOL;

	    // numerical derivative
	    num_dval = (val_plus-val_minus)/(2.*pertTOL);

	    // check
	    assert(std::abs(dval-num_dval)<resTOL);
	  }
      }
    return true;
  }


}
