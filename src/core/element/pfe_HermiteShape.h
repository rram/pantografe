/** \file pfe_HermiteShape.h
 * \brief Defines the class of Hermite shape functions pfe::HermiteShape
 * \author Ramsharan Rangarajan
 */

#ifndef PFE_HERMITE_SHAPE_H
#define PFE_HERMITE_SHAPE_H

#include <pfe_ShapeFunction.h>

namespace pfe
{
  //! \brief Class for cubic Hermite polynomials on \f$[a,b]\f$.  
  /** In the name of the class Hermite31D, the 3 stands for cubic polynomials
   * and 1D for the polynomial being defined over an interval on the real line.
   * This class defines the basis functions that interpolate the values of a function
   * and its derivative at the two end points of the element.
   * \warning Note that shape functions are computed over the real element and 
   * not the parametric element.
   *
   *
   * \ingroup element
   */
  class HermiteShape: public ShapeFunction
  {
  public:
    //! Constructor
    //! \param endpts Cartesian coordinates of the end points in 1D. 
    HermiteShape(const double left, const double right);
  
    //! Destructor
    inline virtual ~HermiteShape() {}

    // Disable copy and assignment
    HermiteShape(const HermiteShape&) = delete;
    HermiteShape& operator=(const HermiteShape&) = delete;
  
    //! Returns the number of shape functions.
    inline int GetNumberOfFunctions() const override
    { return 4; }
  
    //! Returns the number of arguments of the functions- which 1.
    inline int GetNumberOfVariables() const override
    { return 1; }
  
    //! Computes the value of requested shape function at given point.
    //! \param a Shape function number. Should be between 0 and 3.
    //! \param x Barycentric coordinates of point. 
    //! \warning Be careful about shape functions scaling with the length of the segment.
    double Val(const int a,  const double *x) const override;

    //! Computes the derivative of the requested shape function in the requested direction at given point.
    //! \param a Shape function number. Should be between 0 and 3.
    //! \param x Barycentric coordinate of point in segment.
    //! \param i Direction- Should be 0.
    //! \warning Derivatives are wrt barycentric coordinates, not cartesian coordinates.
    double DVal(const int a, const double *x, const int i) const override;

    //! Computes the 2nd derivative
    //! \param a Shape function number. Should be between 0 and 3
    //! \param x Barycentric coordinates in the segment
    //! Assumes only 0 direction
    double D2Val(const int a, const double* x) const;

    //! Consistency test for Val, DVal and D2Val
    //! @param x coordinates of the point at which to test
    //! @param Pert size of the perturbation with which to compute numerical 
    //! derivatives (x->x+Pert)
    bool ConsistencyTest(const double *x, const double pertTOL, const double resTOL) const override;
    
  private:
    const double elmcoord[2];
    double Coef[4][4];
  };

}

#endif

