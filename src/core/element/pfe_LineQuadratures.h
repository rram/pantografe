
/** \file pfe_LineQuadratures.h
 * \brief Defines quadrature rules over 1D elements
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_Quadrature.h>

namespace pfe
{
  //! \ingroup element
  struct LineQuadratures
  {
    //! \brief 2-point Gauss quadrature coordinates in the segment (0,1).
    //! Barycentric coordinates used for the Gauss points.
    static Quadrature Line_2pt;
    
  
    //! \brief 3-point Gauss quadrature coordinates in the segment (0,1).
    //! Barycentric coordinates used for the Gauss points.
    //! Degree of precision is 5 (quintic polynomials are exactly integrated on the line).
    static Quadrature Line_3pt;

    //! \brief 4-point Gauss quadrature coordinates in the segment (0,1).
    //! Barycentric coordinates used for the Gauss points.
    static Quadrature Line_4pt;
    

    //! \brief 5-point Gauss quadrature coordinates in the segment (0,1).
    //! Barycentric coordinates used for the Gauss points.
    static Quadrature Line_5pt;
  };
}
