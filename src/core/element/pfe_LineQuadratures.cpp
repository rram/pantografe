
/** \file pfe_LineQuadratures.h
 * \brief Implements quadrature rules over 1D elements
 * \author Ramsharan Rangarajan
 * Last modified: September 28, 2021
 */

#include <pfe_LineQuadratures.h>

namespace pfe
{
  Quadrature LineQuadratures::Line_2pt
  (std::vector<double>{0.5 + 0.577350269/2., 0.5 - 0.577350269/2.},
   std::vector<double>{1./2.,1./2.},
   1, 2);

  Quadrature LineQuadratures::Line_3pt
  (std::vector<double>{0.887298334620742, 0.5, 0.112701665379258},
   std::vector<double>{5./18.,4./9.,5./18.},
   1, 3);
  
  Quadrature LineQuadratures::Line_4pt
  (std::vector<double>{0.93056815579, 0.66999052179, 0.33000947821, 0.06943184421},
   std::vector<double>{0.5*0.34785484513, 0.5*0.65214515486, 0.5*0.65214515486, 0.5*0.34785484513},
   1, 4);

  Quadrature LineQuadratures::Line_5pt
  (std::vector<double>{0.046910075, 0.230765345, 0.5, 0.769234655, 0.953089925},
   std::vector<double>{0.118463445, 0.239314335, 0.2844444450, 0.239314335, 0.118463445},
   1, 5);
}
