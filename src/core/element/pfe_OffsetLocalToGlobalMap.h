
/** \file pfe_OffsetLocalToGlobalMap.h
 * \brief Defines the class pfe::OffsetLocalToGlobalMap
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_LocalToGlobalMap.h>

namespace pfe
{
  //! Local to global map that incorporates a uniform offset
  //! \ingroup element
  class OffsetLocalToGlobalMap: public LocalToGlobalMap
  {
  public:
    //! Constructor
    inline OffsetLocalToGlobalMap(const LocalToGlobalMap& l2g, const int shift)
      :L2G(l2g),
      offset(shift)
      {}
	

    inline virtual ~OffsetLocalToGlobalMap() {}

    // Disable copy and assignment
    OffsetLocalToGlobalMap(const OffsetLocalToGlobalMap&) = delete;
    OffsetLocalToGlobalMap& operator=(const OffsetLocalToGlobalMap&) = delete;
						       
  
    //! local to global map
    inline int Map(const int field, const int dof, const int elm) const override
    { return offset+L2G.Map(field, dof, elm); }
  

    //! Total number of elements that can be mapped. Usually, total number of
    //! elements in the mesh.
    inline int GetNumElements() const override
    {return L2G.GetNumElements(); }

    //! Number of fields in an element mapped
    inline int GetNumFields(const int ElementMapped) const override
    { return L2G.GetNumFields(ElementMapped); }


    //! Number of dofs in an element mapped in a given field
    inline int GetNumDofs(const int ElementMapped, const int field) const override
    { return L2G.GetNumDofs(ElementMapped, field); }

    //! Total number of dof in the entire map
    inline int GetTotalNumDof() const override
    { return L2G.GetTotalNumDof(); }

  private:
    const LocalToGlobalMap &L2G;
    const int              offset;
  };

  
} // pfe::

