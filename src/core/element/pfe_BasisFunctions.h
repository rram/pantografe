/** \file pfe_BasisFunctions.h
 * \brief Defines the class pfe::BasisFunctions
 * \author Ramsharan Rangarajan
 * Last modified: September 23, 2021
 */


#ifndef PFE_BASIS_FUNCTIONS_H
#define PFE_BASIS_FUNCTIONS_H

#include <pfe_Quadrature.h>
#include <pfe_ShapeFunction.h>
#include <pfe_ElementGeometry.h>

namespace pfe
{
  /**
     BasisFunctions: Evaluation of the shape functions and derivatives at 
     the quadrature points. 

     \ingroup element
  */

  class BasisFunctions
  {
  public:
    //! \param QuadObj Quadrature object that defines integration rules on the element. 
    //! \param ShapeObj Shape object.
    //! \param EG Geometry of the element for which to evaluate shape functions.
    BasisFunctions(const Quadrature* QuadObj,
		   const ShapeFunction* ShapeObj, 
		   const ElementGeometry* EG)
      :has_d2(false)
      {  CreateObject(QuadObj, ShapeObj, EG); }

    //! Constructor with given basis functions
  BasisFunctions(const std::vector<double>& qcoords,
		 const std::vector<double>& qweights,
		 const std::vector<double>& Na,
		 const std::vector<double>& dNa)
    :has_d2(false),
      LocalCoordinates(qcoords),
      LocalWeights(qweights),
      LocalShapes(Na),
      LocalDShapes(dNa),
      LocalD2Shapes({})
	{
	  const int Nquad = static_cast<int>(LocalWeights.size());
	  const int Nbasis = GetBasisDimension();
	  const int Nderiv = GetNumberOfDerivativesPerFunction();
	  assert(static_cast<int>(LocalShapes.size())==Nbasis*Nquad);
	  assert(static_cast<int>(LocalDShapes.size())==Nbasis*Nquad*Nderiv);
	  assert(LocalCoordinates.size()%LocalWeights.size()==0);
	  assert(LocalD2Shapes.empty()==true);
	}
    
    //! Constructor with given basis functions
  BasisFunctions(const std::vector<double>& qcoords,
		 const std::vector<double>& qweights,
		 const std::vector<double>& Na,
		 const std::vector<double>& dNa,
		 const std::vector<double>& d2Na)
    :has_d2(true),
      LocalCoordinates(qcoords),
      LocalWeights(qweights),
      LocalShapes(Na),
      LocalDShapes(dNa),
      LocalD2Shapes(d2Na)
      {
	const int Nquad = static_cast<int>(LocalWeights.size());
	const int Nbasis = GetBasisDimension();
	const int Nderiv = GetNumberOfDerivativesPerFunction();
	assert(static_cast<int>(LocalShapes.size())==Nbasis*Nquad);
	assert(static_cast<int>(LocalDShapes.size())==Nbasis*Nquad*Nderiv);
	assert(static_cast<int>(LocalD2Shapes.size())==Nbasis*Nquad*Nderiv*Nderiv);
	assert(LocalCoordinates.size()%LocalWeights.size()==0);
      }
    
    inline virtual ~BasisFunctions(){}

    //! Disable copy and assignment
    BasisFunctions(const BasisFunctions&) = delete;
    BasisFunctions& operator=(const BasisFunctions&) = delete;
  
    // Accessors/Mutators:
    inline const std::vector<double>& GetShapes() const 
    { return LocalShapes; }
  
    inline const std::vector<double>& GetDShapes() const 
    { return LocalDShapes; }

    inline const std::vector<double>& GetD2Shapes() const
    { return LocalD2Shapes; }
  
    inline const std::vector<double>& GetIntegrationWeights() const 
    { return LocalWeights;}
  
    inline const std::vector<double>& GetQuadraturePointCoordinates() const 
    { return LocalCoordinates; }
  
    //! returns the number of shape functions provided
    inline int GetBasisDimension() const 
    { return LocalShapes.size()/LocalWeights.size(); }
  
    //! returns the number of directional derivative for each shape function
    inline int GetNumberOfDerivativesPerFunction() const 
    { return LocalDShapes.size()/LocalShapes.size(); }
  
    //! returns the number of  number of coordinates for each Gauss point
    inline int GetSpatialDimension() const 
    { return LocalCoordinates.size()/LocalWeights.size(); }
  
  private:
    //! Helper to compute quadrature & basis function values/derivatives
    void  CreateObject(const Quadrature* QuadObj,
		       const ShapeFunction* ShapeObj, 
		       const ElementGeometry* EG);

    const bool has_d2;
    std::vector<double> LocalShapes;
    std::vector<double> LocalDShapes;
    std::vector<double> LocalD2Shapes;
    std::vector<double> LocalWeights;
    std::vector<double> LocalCoordinates;
  };

}

#endif
