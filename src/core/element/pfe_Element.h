/** \file pfe_Element.h
 * \brief Defines the class pfe::Element
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <vector>
#include <pfe_BasisFunctions.h>

namespace pfe
{
  /**
     \brief Element: abstract base class for any element

     An Element is a convex polytope with possibly-multiple discrete
     functional spaces, one for each field, with support on it.

     An element has:\n
     1) A geometry. The connectivity of vertices that define the convex hull.\n
     2) A group of scalar functional spaces. Each functional
     space is defined by a set of basis functions with support
     on the element. Each functional space has an associated
     number of degrees of freedom to it: the components of any function
     in the space in the chosen basis.\n
   
     A field is a scalar-valued function defined over the element.\n
     Each field may have a different underlying functional space.\n
     Each field may have a different quadrature rule. Different quadrature rules
     will also be handled with different elements, or through inheritance. The 
     consistency of the quadrature rule when different fields are integrated together
     is in principle not checked by the element hierarchy.
     \n
     Clearly, isoparametric elements will not necessarily be convex, 
     but Element class can still be used.

     As a convention, fields are numbered starting from 0.

     \ingroup element
  */

  class Element
  {
  public:

    //! Default constructor
    inline Element(){}

    //! Destructor.
    //! Deletes local shape functions for this element
    inline virtual ~Element()
    {
      for(unsigned int i=0; i<LocalShapes_.size(); ++i)
	delete LocalShapes_[i];
    }

    // Disable copy and assignment
    Element(const Element&) = delete;
    Element& operator=(const Element&) = delete;
    
    //! Returns the number of fields in this element
    int GetNumFields() const
    { return Fields_.size(); }
  
    //! Returns the  fields in this element
    const std::vector<int>& GetFields() const
    { return Fields_; }
  
    //! Number of degrees of freedom of one of the fields.
    //! \param[in] field Field number
    int GetNumDof(int field) const
    { return LocalShapes_[GetFieldIndex(field)]->GetBasisDimension(); }
  
    //! Number of derivatives per shape function for one of the fields
    //! \param[in] field field number
    int GetNumDerivatives(int field) const
    { return LocalShapes_[GetFieldIndex(field)]->GetNumberOfDerivativesPerFunction(); }
  
    //!  Shape functions at quadrature points of one of the fields
    //! \param[in] field  field number
    const std::vector<double>& GetShape(int field) const
    { return LocalShapes_[GetFieldIndex(field)]->GetShapes(); }

    //!  Shape function derivatives at quadrature points of one of the fields
    //! \param[in] field field number
    const std::vector <double> &GetDShape(int field) const
    { return LocalShapes_[GetFieldIndex(field)]->GetDShapes(); }

    //! 2nd derivatives of shape functions
    virtual const std::vector<double>& GetD2Shape(int field) const
    { return LocalShapes_[GetFieldIndex(field)]->GetD2Shapes(); }

    //! Integration weights of a given field
    //! \param[in] field field number
    virtual const std::vector<double>& GetIntegrationWeights(int field) const
    { return LocalShapes_[GetFieldIndex(field)]->GetIntegrationWeights(); }

    //! Integration point coordinates of a given field
    //! \param[in] field Field number
    virtual const std::vector<double>& GetIntegrationPointCoordinates(int field) const
    { return LocalShapes_[GetFieldIndex(field)]->GetQuadraturePointCoordinates(); }

    //! Value of shape function for a field at a specific quadrature point
    //! \param[in] f field number
    //! \param[in] q quadrature point number
    //! \param[in] a Shape function number
    virtual double GetShape(const int f, const int q, const int a) const
    { return LocalShapes_[GetFieldIndex(f)]->GetShapes()[q*GetNumDof(f)+a]; }
  
    //! Value of derivative of shape function 'a' for field 'f'
    //! at quadrature point 'q' along the coordinate direction 'i'
    //! \param[in] f field number
    //! \param[in] q quadrature point number
    //! \param[in] a Shape function number
    //! \param[in] i Coordinate direction
    virtual double GetDShape(const int f, const int q, const int a, const int i) const
    { return LocalShapes_[GetFieldIndex(f)]->GetDShapes()
	[q*GetNumDof(f)*GetNumDerivatives(f)+a*GetNumDerivatives(f)+i]; }

    //! Value of 2nd derivatives
    virtual double GetD2Shape(const int f, const int q, const int a, const int i, const int j) const
    {
      int nderiv = GetNumDerivatives(f);
      return LocalShapes_[GetFieldIndex(f)]->GetD2Shapes()
	[q*GetNumDof(f)*nderiv*nderiv + a*nderiv*nderiv + nderiv*i + j];
    }
    
    //! Access to Element Geometry
    virtual const ElementGeometry& GetElementGeometry() const = 0;
  
  protected:
    //! Mapping from field to its index in this element
    //! Multiple fields can use the same basis functions.
    //! Therefore, the index need not be consistent with the member Fields_
    //! \param[in] field Global field number
    //! \return Index of given field
    virtual int GetFieldIndex(int field) const = 0;

    //! AddBasisFunctions adds a BasisFunctions pointer at the end of LocalShapes_
    //! The i-th added  BasisFunctions pointer is referenced when GetFieldShapes
    //! returns  the integer i-1
    //! \param[in] field Field number
    //! \param[in] Funcs Basis functions for this field
    inline void AddBasisFunctions(const BasisFunctions &Funcs)
    { if(Funcs.GetD2Shapes().empty())
	LocalShapes_.push_back( new BasisFunctions(Funcs.GetQuadraturePointCoordinates(),
						   Funcs.GetIntegrationWeights(),
						   Funcs.GetShapes(),
						   Funcs.GetDShapes()) );
      else
	LocalShapes_.push_back( new BasisFunctions(Funcs.GetQuadraturePointCoordinates(),
						   Funcs.GetIntegrationWeights(),
						   Funcs.GetShapes(),
						   Funcs.GetDShapes(),
						   Funcs.GetD2Shapes()) );
    }
    
    //! Appends a given field to the list of fields in this element
    //! \param[in] field Field number
    inline void AppendField(const int field)
    { Fields_.push_back(field); }
  
  private:
    //! Basis functions for this element indexed by
    //! the local field number
    std::vector<BasisFunctions*> LocalShapes_;

    //! Fields used in this element
    std::vector<int> Fields_;
  };
}
  
