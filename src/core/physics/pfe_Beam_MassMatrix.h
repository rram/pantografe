
/** \file pfe_Beam_MassMatrix.h
 * \brief Defines the class pfe::Beam_MassMatrix
 * \author Ramsharan Rangarajan
 * Last modified: April 05, 2022
 */

#ifndef PFE_BEAM_MASS_MATRIX_H
#define PFE_BEAM_MASS_MATRIX_H

#include <pfe_ElementalOperation.h>
#include <pfe_Configuration.h>

namespace pfe
{
  class Beam_MassMatrix: public DResidue
  {
  public:
    //! Constructor
    inline Beam_MassMatrix(const int elmnum, const Element* elm, const double rho)
      :DResidue(),
      ElmNum(elmnum), Elm(elm), Rho(rho), Fields({0}) {}

    //! Destructor
    inline virtual ~Beam_MassMatrix() {}

    //! Disable copy and assignment
    Beam_MassMatrix(const Beam_MassMatrix&) = delete;
    Beam_MassMatrix& operator=(const Beam_MassMatrix&) = delete;

    //! Access the element number
    inline int GetElementNumber() const
    { return ElmNum; }

    //! Access the density
    inline double GetLineDensity() const
    { return Rho; }

    //! Set the density
    inline void SetDensity(const double rho) 
    { Rho = rho; }

    //! Access the fields
    inline const std::vector<int>& GetFields() const override
    { return Fields; }

    //! Access the number of dofs in the given field
    inline int GetFieldDof(int fieldnum) const override
    { return Elm->GetNumDof(Fields[fieldnum]); }

    //! Compute the functional
    double ComputeFunctional(const void* argval) const;

    //! Residual
    inline void GetVal(const void* argval, std::vector<std::vector<double>>* funcval) const override
    { return GetDVal(argval, funcval, nullptr); }

    //! Mass matrix
    void GetDVal(const void* argval,
		 std::vector<std::vector<double>> *funcval,
		 std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const override;

    //! Consistency test
    bool ConsistencyTest(const void* argval,
			 const double pertEPS,
			 const double tolEPS) const override;

  protected:
    const int ElmNum;
    const Element* Elm;
    double Rho;
    const std::vector<int> Fields;
  };
  
}

#endif
