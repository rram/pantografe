
/** \file pfe_Standard_Consistency.h
 * \brief Defines the helper function pfe::Standard_Consistency_Tets
 * \author Ramsharan Rangarajan
 */

#ifndef PFE_STANDARD_CONSISTENCY_TEST_H
#define PFE_STANDARD_CONSISTENCY_TEST_H

#include <cmath>

namespace pfe
{
  // Consistency test
  template<class OpType>
    bool Standard_Consistency_Test(OpType& op,
				   const void* argval,
				   const double pertEPS,
				   const double tolEPS)
    {
      // access the configuration
      assert(argval!=nullptr);
      const auto& config = *static_cast<const TransverseDisplacementField*>(argval);
      const auto& L2GMap = *config.L2GMap;
      const double* wvals = config.displacement;
      const int ElmNum = op.GetElementNumber();

      // Evaluate residual and stiffness
      const int nFields = 1;
      const int field = 0;
      const int nDof = op.GetFieldDof(field);
      std::vector<std::vector<double>> res(nFields, std::vector<double>(nDof));
      std::vector<std::vector<std::vector<std::vector<double>>>> dres(nFields);
      for(auto& a:dres)
	{ a.resize(nDof);
	  for(auto& b:a)
	    { b.resize(nFields);
	      for(auto& c:b)
		c.resize(nDof); } }
      op.GetDVal(argval, &res, &dres);

      // Perturbed configurations
      const int nTotalDof = L2GMap.GetTotalNumDof();
      std::vector<double> wpert(wvals, wvals+nTotalDof);
      TransverseDisplacementField pert_config{.displacement=&wpert[0], .L2GMap=&L2GMap};

      // Consistency tests
      auto resplus = res;
      auto resminus = res;
      for(int f=0; f<nFields; ++f)
	for(int a=0; a<nDof; ++a)
	  {
	    int dofnum = L2GMap.Map(field,a,ElmNum);
	  
	    // positive perturbation
	    wpert[dofnum] += pertEPS;
	    double Eplus = op.ComputeFunctional(&pert_config);
	    op.GetVal(&pert_config, &resplus);

	    // negative perturbation
	    wpert[dofnum] -= 2.*pertEPS;
	    double Eminus = op.ComputeFunctional(&pert_config);
	    op.GetVal(&pert_config, &resminus);

	    // undo perturbations
	    wpert[dofnum] += pertEPS;

	    // Consistency of residuals
	    double resnum = (Eplus-Eminus)/(2.*pertEPS);
	    assert(std::abs(resnum-res[f][a])<tolEPS);
	  
	    // Consistency of stiffness
	    for(int g=0; g<nFields; ++g)
	      for(int b=0; b<nDof; ++b)
		{
		  double dresnum = (resplus[g][b]-resminus[g][b])/(2.*pertEPS);
		  assert(std::abs(dresnum-dres[f][a][g][b])<tolEPS);
		}
	  }

      // done
      return true;
    }
}

#endif
