
/** \file test_Beam_MassMatrix.cpp
 * \brief Unit test for class pfe::Beam_MassMatrix
 * \author Ramsharan Rangarajan
 * \core_phy_test Unit test for class pfe::Beam_MassMatrix
 */

#include <pfe_Beam_MassMatrix.h>
#include <pfe_H31DElement.h>
#include <random>
#include <algorithm>

using namespace pfe;

int main()
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> real_dis(0.,1.);
  std::uniform_int_distribution<> int_dis(1, 10);

  for(int trial=0; trial<10; ++trial)
    {
      // Create elements
      const int nElements = int_dis(gen);
      const int nNodes = nElements+1;
      std::vector<int> index(nNodes);
      for(int a=0; a<nNodes; ++a)
	index[a] = a;
      std::shuffle(index.begin(), index.end(), gen);
      std::vector<double> coordinates(nNodes);
      for(int a=0; a<nNodes; ++a)
	coordinates[index[a]] = static_cast<double>(a)+real_dis(gen);
      SegmentGeometry<1>::SetGlobalCoordinatesArray(coordinates);
      std::vector<int> connectivity(2*nElements);
      for(int e=0; e<nElements; ++e)
	{ connectivity[2*e] = index[e]+1;
	  connectivity[2*e+1] = index[e+1]+1; }
      std::vector<Element*> ElmArray(nElements);
      for(int e=0; e<nElements; ++e)
	ElmArray[e] = new H31DElement<1>(connectivity[2*e], connectivity[2*e+1]);

      // Local to global map
      H31DL2GMap L2GMap(ElmArray);

      // dof array
      const int nTotalDof = L2GMap.GetTotalNumDof();
      std::vector<double> displacements(nTotalDof);
      for(int n=0; n<nTotalDof; ++n)
	displacements[n] = real_dis(gen);

      // Configuration
      TransverseDisplacementField config{.displacement=&displacements[0], .L2GMap=&L2GMap};

      // Create operations
      std::vector<Beam_MassMatrix*> OpsArray(nElements);
      std::vector<double> Rho(nElements);
      for(int e=0; e<nElements; ++e)
	{ Rho[e] = 1.+real_dis(gen);
	  OpsArray[e] = new Beam_MassMatrix(e, ElmArray[e], Rho[e]); }

      // Test
      for(int e=0; e<nElements; ++e)
	{
	  assert(std::abs(OpsArray[e]->GetLineDensity()-Rho[e])<1.e-8);
	  assert(static_cast<int>(OpsArray[e]->GetFields().size())==1);
	  assert(OpsArray[e]->GetFields()[0]==0);
	  assert(OpsArray[e]->GetFieldDof(0)==4);
	  assert(OpsArray[e]->GetElementNumber()==e);
	  auto success = OpsArray[e]->ConsistencyTest(&config, 1.e-6, 1.e-4);
	  assert(success==true);
	}

      // clean up
      for(auto& elm:ElmArray) delete elm;
      for(auto& op:OpsArray) delete op;
    }
}
