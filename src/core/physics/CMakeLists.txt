
# add library
add_library(pfe_physics STATIC
  pfe_ElementalOperation.cpp
  pfe_Beam_BendingStiffness.cpp
  pfe_Beam_MassMatrix.cpp
  pfe_TensionForce.cpp
  pfe_GravityForce.cpp)

# headers
target_include_directories(pfe_physics PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)

# link
target_link_libraries(pfe_physics PUBLIC pfe_element pfe_petsc)

# Add required flags
target_compile_features(pfe_physics PUBLIC ${pfe_COMPILE_FEATURES})

install(FILES
	pfe_Beam_BendingStiffness.h
	pfe_Beam_MassMatrix.h
	pfe_Configuration.h
	pfe_ElementalOperation.h
	pfe_GravityForce.h
	pfe_Standard_Consistency_Test.h
	pfe_TensionForce.h
        DESTINATION ${PROJECT_NAME}/include)
