
/** \file pfe_ElementalOperation.cpp
 * \brief Implements non pure methods of pfe::ElementalOperation
 * \author Ramsharan Rangarajan
 * Last modified: September 24, 2021
 */

#include <pfe_ElementalOperation.h>

namespace pfe
{
  // Zero the residuals and derivatives
  void DResidue::
  SetZero(std::vector<std::vector<double>>* funcval,
	  std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
  {
    // Set all rows of funcval -> 0
    if(funcval!=nullptr)
      for(auto& it:(*funcval))
	std::fill(it.begin(), it.end(), 0.);
  
    // Set all rows for dfuncval -> 0
    if(dfuncval!=nullptr)
      for(auto& it1:(*dfuncval))
	for(auto& it2:it1)
	  for(auto& it3:it2)
	    std::fill(it3.begin(), it3.end(), 0.);
    return;
  }

}   
