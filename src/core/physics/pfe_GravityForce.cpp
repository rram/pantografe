
/** \file pfe_GravityForce.cpp
 * \brief Implements the class pfe::GravityForce
 * \author Ramsharan Rangarajan
 * Last modified: September 28, 2021
 */

#include <pfe_GravityForce.h>
#include <cmath>

namespace pfe
{
  // Compute the force potential
  double GravityForce::ComputeFunctional(const void* argval) const
  {
    // Initialize the output
    double Energy = 0.;

    // access the configuration
    assert(argval!=nullptr);
    auto& config = *static_cast<const TransverseDisplacementField*>(argval);
    auto& L2GMap = *config.L2GMap;
    const double* wvals = config.displacement;

    // Element details
    const int field = 0;
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());
    const int nDof = Elm->GetNumDof(field);

    // Integrate
    double w;
    for(int q=0; q<nQuad; ++q)
      {
	// w here
	w = 0.;
	for(int a=0; a<nDof; ++a)
	  w +=  wvals[L2GMap.Map(field,a,ElmNum)]*Elm->GetShape(field,q,a);

	// update functional
	Energy += Qwts[q]*ForceVal*w;
      }

    return Energy;
  }


  // Residual
  void GravityForce::GetVal(const void* argval,
			    std::vector<std::vector<double>>* funcval) const
  {
    // zero outputs
    assert(funcval!=nullptr);
    for(auto& a:*funcval)
      for(auto& b:a)
	b = 0.;

    // Element details
    const int field = 0.;
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());
    const int nDof = Elm->GetNumDof(field);

    // Integrate
    for(int q=0; q<nQuad; ++q)
      for(int a=0; a<nDof; ++a)
	(*funcval)[field][a] += Qwts[q]*ForceVal*Elm->GetShape(field,q,a);

    return;
  }


  // Consistency test
  bool GravityForce::ConsistencyTest(const void* argval,
				     const double pertEPS, 
				     const double tolEPS) const
  {
    // access the configuration
    assert(argval!=nullptr);
    const auto& config = *static_cast<const TransverseDisplacementField*>(argval);
    const auto& L2GMap = *config.L2GMap;
    const double* wvals = config.displacement;

    // Evaluate residual
    const int nFields = 1;
    const int field = 0;
    const int nDof = GetFieldDof(field);
    std::vector<std::vector<double>> res(nFields, std::vector<double>(nDof));
    GetVal(argval, &res);

    // Perturbed configuration
    const int nTotalDof = L2GMap.GetTotalNumDof();
    std::vector<double> wpert(wvals, wvals+nTotalDof);
    TransverseDisplacementField pert_config{.displacement=&wpert[0], .L2GMap=&L2GMap};

    // Consistency test
    for(int f=0; f<nFields; ++f)
      for(int a=0; a<nDof; ++a)
	{
	  int dofnum = L2GMap.Map(field,a,ElmNum);

	  // positive perturbation
	  wpert[dofnum] += pertEPS;
	  double Eplus = ComputeFunctional(&pert_config);

	  // negative perturbation
	  wpert[dofnum] -= 2.*pertEPS;
	  double Eminus = ComputeFunctional(&pert_config);

	  // undo perturbations
	  wpert[dofnum] += pertEPS;

	  // consistency
	  double resnum = (Eplus-Eminus)/(2.*pertEPS);
	  assert(std::abs(res[f][a]-resnum)<tolEPS);
	}
    
    return true;
    
  }
}

