
/** \file pfe_Beam_BendingStiffness.cpp
 * \brief Implements the class pfe::Beam_BendingStiffness
 * \author Ramsharan Rangarajan
 * Last modified: September 27, 2021
 */

#include <pfe_Beam_BendingStiffness.h>
#include <pfe_Standard_Consistency_Test.h>
#include <cmath>

namespace pfe
{
  // Compute the strain energy
  double Beam_BendingStiffness::ComputeFunctional(const void* argval) const
  {
    // initialize output
    double Energy = 0.;

    // access the configuration
    assert(argval!=nullptr);
    auto& config = *static_cast<const TransverseDisplacementField*>(argval);
    auto& L2GMap = *config.L2GMap;
    const double* wvals = config.displacement;

    // Element details
    const int field = 0;
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());
    const int nDof = Elm->GetNumDof(field);

    // Integrate
    double d2w;
    for(int q=0; q<nQuad; ++q)
      {
	// w'' here
	d2w = 0.;
	for(int a=0; a<nDof; ++a)
	  d2w += wvals[L2GMap.Map(field,a,ElmNum)]*Elm->GetD2Shape(field,q,a,0,0);

	// update functional
	Energy += 0.5*Qwts[q]*EI*d2w*d2w;
      }

    // done
    return Energy;
  }
    
  // residual and stiffness
  void Beam_BendingStiffness::GetDVal(const void* argval,
				 std::vector<std::vector<double>> *funcval,
				 std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
  {
    // zero outputs
    SetZero(funcval, dfuncval);
    
    // access the configuration
    assert(argval!=nullptr);
    const auto& config = *static_cast<const TransverseDisplacementField*>(argval);
    const auto& L2GMap = *config.L2GMap;
    const double* wvals = config.displacement;

    // Element details
    const int field = 0;
    const int nDof = Elm->GetNumDof(field);
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());

    // Integrate
    double d2w;
    for(int q=0; q<nQuad; ++q)
      {
	// Residual
	if(funcval!=nullptr)
	  {
	    // w'' at this point
	    d2w = 0.;
	    for(int a=0; a<nDof; ++a)
	      d2w += wvals[L2GMap.Map(field, a, ElmNum)]*Elm->GetD2Shape(field,q,a,0,0);
	
	    for(int a=0; a<nDof; ++a)
	      (*funcval)[field][a] += Qwts[q]*EI*d2w*Elm->GetD2Shape(field,q,a,0,0);
	  }

	// DResidue
	if(dfuncval!=nullptr)
	  for(int a=0; a<nDof; ++a)
	    for(int b=0; b<nDof; ++b)
	      (*dfuncval)[field][a][field][b] += Qwts[q]*EI*
		Elm->GetD2Shape(field,q,a,0,0)*
		Elm->GetD2Shape(field,q,b,0,0);
      }

    // done
    return;
  }

  // Consistency test
  bool Beam_BendingStiffness::ConsistencyTest(const void* argval,
					      const double pertEPS,
					      const double tolEPS) const
  { return Standard_Consistency_Test(*this, argval, pertEPS, tolEPS);  }

}
