
/** \file pfe_Beam_BendingStiffness.h
 * \brief Defines the class pfe::Beam_BendingStiffness
 * \author Ramsharan Rangarajan
 * Last modified: September 27, 2021
 */


#ifndef PFE_BENDING_STIFFNESS_H
#define PFE_BENDING_STIFFNESS_H

#include <pfe_ElementalOperation.h>
#include <pfe_Configuration.h>

namespace pfe
{
 

  class Beam_BendingStiffness: public DResidue
  {
  public:
    //! Constructor
    inline Beam_BendingStiffness(const int elmnum, const Element* elm, const double ei)
      :DResidue(),
      ElmNum(elmnum), Elm(elm), EI(ei), Fields({0}) {}

    //! Destructor, nothing to do
    inline virtual ~Beam_BendingStiffness() {}
    
    //! Disable copy and assignment
    Beam_BendingStiffness(const Beam_BendingStiffness&) = delete;
    Beam_BendingStiffness& operator=(const Beam_BendingStiffness&) = delete;
    
    //! Access the element number
    inline int GetElementNumber() const
    { return ElmNum; }
    
    //! Access bending modulus
    inline double GetBendingModulus() const
    { return EI; }

    //! Set the modulus
    inline void SetBendingModulus(const double ei)
    { EI = ei; }
    
    //! Access the fields
    inline const std::vector<int>& GetFields() const override
    { return Fields; }

    //! Access the number of dofs in the given field
    inline int GetFieldDof(int fieldnum) const override
    { return Elm->GetNumDof(Fields[fieldnum]); }

    //! Compute the strain energy
    double ComputeFunctional(const void* argval) const;
    
    //! residual
    inline void GetVal(const void* argval, std::vector<std::vector<double>>* funcval) const override
    { return GetDVal(argval, funcval, nullptr); }

    //! residual and stiffness
    void GetDVal(const void* argval,
		 std::vector<std::vector<double>> *funcval,
		 std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const override;

    //! Consistency test
    //! \param[in] argval Configuration at which to perform the consistency test
    //! \param[in] pertEPS Tolerance to use for perturbation
    //! \param[in] pertEPS Tolerance to use for consistency check
    bool ConsistencyTest(const void* argval,
			 const double pertEPS,
			 const double tolEPS) const override;

  protected:
    const int ElmNum;
    const Element* Elm;
    double EI;
    const std::vector<int> Fields;
  };
}

#endif
