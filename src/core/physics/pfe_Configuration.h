
/** \file pfe_Configuration.h
 * \brief Defines the class pfe::Configuration
 * \author Ramsharan Rangarajan
 */

#pragma once

namespace pfe
{
  //! \ingroup physics
  struct TransverseDisplacementField
  {
    const double* displacement;
    const LocalToGlobalMap* L2GMap;
  };
  
}

