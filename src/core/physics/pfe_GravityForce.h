
/** \file pfe_GravityForce.h
 * \brief Defines the class pfe::GravityForce
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_ElementalOperation.h>
#include <pfe_Configuration.h>

namespace pfe
{
  //! \ingroup physics
  class GravityForce: public Residue
  {
  public:
    //! Constructor
    inline GravityForce(const int elmnum, const Element* elm, const double fval)
      :Residue(),
      ElmNum(elmnum), Elm(elm), ForceVal(fval), Fields({0}) {}

    //! Destructor
    inline virtual ~GravityForce() {}

    //! Disable copy and assignment
    GravityForce(const GravityForce&) = delete;
    GravityForce& operator=(const GravityForce&) = delete;

    //! Access the element number
    inline int GetElementNumber() const
    { return ElmNum; }

    //! Access the force value
    inline double GetForceValue() const
    { return ForceVal; }

    //! Set the force value
    inline void SetForceValue(double fval)
    { ForceVal = fval; }
      
    //! Access the fields
    inline const std::vector<int>& GetFields() const override
    { return Fields; }

    //! Access the number of dofs in the given field
    inline int GetFieldDof(int fieldnum) const override
    { return Elm->GetNumDof(Fields[fieldnum]); }

    //! Compute the force potential
    double ComputeFunctional(const void* argval) const;

    //! residual
    void GetVal(const void* argval, std::vector<std::vector<double>>* funcval) const override;
    
    //! Consistency test
    //! \param[in] argval Configuration at which to perform the consistency test
    //! \param[in] pertEPS Tolerance to use for perturbation
    //! \param[in] pertEPS Tolerance to use for consistency check
    bool ConsistencyTest(const void* argval,
			 const double pertEPS,
			 const double tolEPS) const;

  protected:
    const int ElmNum;
    const Element* Elm;
    double ForceVal;
    const std::vector<int> Fields;
  };
}
