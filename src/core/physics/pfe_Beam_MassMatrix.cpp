
/** \file pfe_Beam_MassMatrix.cpp
 * \brief Implements the class pfe::Beam_MassMatrix
 * \author Ramsharan Rangarajan
 * Last modified: March 15, 2022
 */

#include <pfe_Beam_MassMatrix.h>
#include <pfe_Standard_Consistency_Test.h>
#include <cmath>

namespace pfe
{
  // Compute the L2 functional
  double Beam_MassMatrix::ComputeFunctional(const void* argval) const
  {
    // Initialize output
    double Energy = 0.;

    // access the configuration
    assert(argval!=nullptr);
    auto& config = *static_cast<const TransverseDisplacementField*>(argval);
    auto& L2GMap = *config.L2GMap;
    const double* wvals = config.displacement;

    // Element details
    const int field = 0;
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());
    const int nDof = Elm->GetNumDof(field);

    // Integrate
    double w = 0.;
    for(int q=0; q<nQuad; ++q)
      {
	// w here
	w = 0.;
	for(int a=0; a<nDof; ++a)
	  w += wvals[L2GMap.Map(field,a,ElmNum)]*Elm->GetShape(field,q,a);

	// update functional
	Energy += 0.5*Qwts[q]*Rho*w*w;
      }

    // done
    return Energy;
  }


  // Residual and stiffness
  void Beam_MassMatrix::GetDVal(const void* argval,
				std::vector<std::vector<double>> *funcval,
				std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
  {
    // zero outputs
    SetZero(funcval, dfuncval);

    // access the configuration
    assert(argval!=nullptr);
    const auto& config = *static_cast<const TransverseDisplacementField*>(argval);
    const auto& L2GMap = *config.L2GMap;
    const double* wvals = config.displacement;

    // Element details
    const int field = 0;
    const int nDof = Elm->GetNumDof(field);
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());

    // Integrate
    double w;
    for(int q=0; q<nQuad; ++q)
      {
	// residual
	if(funcval!=nullptr)
	  {
	    // w at this point
	    w = 0.;
	    for(int a=0; a<nDof; ++a)
	      w += wvals[L2GMap.Map(field,a,ElmNum)]*Elm->GetShape(field,q,a);

	    // element residual
	    for(int a=0; a<nDof; ++a)
	      (*funcval)[field][a] += Qwts[q]*Rho*w*Elm->GetShape(field,q,a);
	  }

	// dresidue
	if(dfuncval!=nullptr)
	  for(int a=0; a<nDof; ++a)
	    for(int b=0; b<nDof; ++b)
	      (*dfuncval)[field][a][field][b] += Qwts[q]*Rho*
		Elm->GetShape(field,q,a)*
		Elm->GetShape(field,q,b);
      }

    // done
    return;
  }

  // Consistency test
  bool Beam_MassMatrix::ConsistencyTest(const void* argval,
					const double pertEPS,
					const double tolEPS) const
  { return Standard_Consistency_Test(*this, argval, pertEPS, tolEPS); }
  
} // pfe::
    
