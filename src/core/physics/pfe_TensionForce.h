
/** \file pfe_TensionForce.h
 * \brief Defines the class pfe::TensionForce
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_ElementalOperation.h>
#include <pfe_Configuration.h>

namespace pfe
{
  //! \ingroup physics
  class TensionForce: public DResidue
  {
  public:
    //! Constructor
    inline TensionForce(const int elmnum, const Element* elm, const double tval)
      :DResidue(),
      ElmNum(elmnum), Elm(elm), Tension(tval), Fields({0}) {}

    //! Destructor
    inline virtual ~TensionForce() {}

    //! Disable copy and assignment
    TensionForce(const TensionForce&) = delete;
    TensionForce& operator=(const TensionForce&) = delete;

    //! Access the element number
    inline int GetElementNumber() const
    { return ElmNum; }
    
    //! Access the tension
    inline double GetTension() const
    { return Tension; }

    //! Set the tension
    inline void SetTension(const double tval)
    { Tension = tval; }

    //! Access the fields
    inline const std::vector<int>& GetFields() const override
    { return Fields; }

    //! Access the number of dofs in the given field
    inline int GetFieldDof(int fieldnum) const override
    { return Elm->GetNumDof(Fields[fieldnum]); }

    //! Compute the strain energy
    double ComputeFunctional(const void* argval) const;
    
    //! residual
    inline void GetVal(const void* argval, std::vector<std::vector<double>>* funcval) const override
    { return GetDVal(argval, funcval, nullptr); }

    //! residual and stiffness
    void GetDVal(const void* argval,
		 std::vector<std::vector<double>> *funcval,
		 std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const override;

    //! Consistency test
    //! \param[in] argval Configuration at which to perform the consistency test
    //! \param[in] pertEPS Tolerance to use for perturbation
    //! \param[in] pertEPS Tolerance to use for consistency check
    bool ConsistencyTest(const void* argval,
			 const double pertEPS,
			 const double tolEPS) const override;
  protected:
    const int ElmNum;
    const Element* Elm;
    double Tension;
    const std::vector<int> Fields;
  };
  
}
