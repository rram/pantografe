
/** \file pfe_TensionForce.cpp
 * \brief Implements the class pfe::TensionForce
 * \author Ramsharan Rangarajan
 * Last modified: September 27, 2021
 */

#include <pfe_TensionForce.h>
#include <pfe_Standard_Consistency_Test.h>

namespace pfe
{
  // Compute the strain energy
  double TensionForce::ComputeFunctional(const void* argval) const
  {
    // Initialize output
    double Energy = 0.;

    // access the configuration
    assert(argval!=nullptr);
    auto& config = *static_cast<const TransverseDisplacementField*>(argval);
    auto& L2GMap = *config.L2GMap;
    const double* wvals = config.displacement;

    // Element details
    const int field = 0;
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());
    const int nDof = Elm->GetNumDof(field);

    // Integrate
    double dw = 0.;
    for(int q=0; q<nQuad; ++q)
      {
	// w' here
	dw = 0.;
	for(int a=0; a<nDof; ++a)
	  dw += wvals[L2GMap.Map(field,a,ElmNum)]*Elm->GetDShape(field,q,a,0);

	// update functional
	Energy += 0.5*Qwts[q]*Tension*dw*dw;
      }
    
    return Energy;
  }
    
  // residual and stiffness
  void TensionForce::GetDVal(const void* argval,
			     std::vector<std::vector<double>> *funcval,
			     std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
  {
    // zero outputs
    SetZero(funcval, dfuncval);

    // access the configuration
    assert(argval!=nullptr);
    auto& config = *static_cast<const TransverseDisplacementField*>(argval);
    auto& L2GMap = *config.L2GMap;
    const double* wvals = config.displacement;

    // Element details
    const int field = 0;
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());
    const int nDof = Elm->GetNumDof(field);

    // Integrate
    double dw = 0.;
    for(int q=0; q<nQuad; ++q)
      {
	// Residual
	if(funcval!=nullptr)
	  {
	    // w' at this point
	    dw = 0.;
	    for(int a=0; a<nDof; ++a)
	      dw += wvals[L2GMap.Map(field,a,ElmNum)]*Elm->GetDShape(field,q,a,0);

	    for(int a=0; a<nDof; ++a)
	      (*funcval)[field][a] += Qwts[q]*Tension*dw*Elm->GetDShape(field,q,a,0);
	  }

	// DResidue
	if(dfuncval!=nullptr)
	  for(int a=0; a<nDof; ++a)
	    for(int b=0; b<nDof; ++b)
	      (*dfuncval)[field][a][field][b] += Qwts[q]*Tension*
		Elm->GetDShape(field,q,a,0)*
		Elm->GetDShape(field,q,b,0);
      }

    // done
    return;
  }

  // Consistency test
  bool TensionForce::ConsistencyTest(const void* argval,
				     const double pertEPS,
				     const double tolEPS) const
  { return Standard_Consistency_Test(*this, argval, pertEPS, tolEPS); }
  
}
