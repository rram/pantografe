
/** \file pfe_linear_Model_MovingLoad.h
 * \brief Defines the class pfe::linear::Model_MovingLoad
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_PantographParams.h>
#include <pfe_TimeIntegrator.h>
#include <pfe_PetscData.h>

// instantiations
#include <pfe_HeavyTensionedBeam.h>
#include <pfe_MultiSpan_Cable.h>
#include <pfe_linear_SingleSpan_OHE.h>
#include <pfe_linear_MultiSpan_OHE.h>

namespace pfe
{
  namespace linear
  {
    //! \ingroup models
    template<typename OHE_TYPE>
      class Model_MovingLoad
      {
      public:
     
	//! Constructor
	Model_MovingLoad(const OHE_TYPE& obj1,
			 const Pantograph_0_Params& obj2,
			 TimeIntegrator& ti,
			 const std::vector<int> dirichlet={});
      
	//! Destructor
	virtual ~Model_MovingLoad();

	//! Disable copy and assignment
	Model_MovingLoad(const Model_MovingLoad<OHE_TYPE>&) = delete;
	Model_MovingLoad<OHE_TYPE>& operator=(const Model_MovingLoad<OHE_TYPE>&) = delete;

	//! Destroy
	virtual void Destroy();
	
	//! Access the ohe system
	inline const OHE_TYPE& GetOHE() const
	{ return ohe; }

	//! Total number of dofs
	inline int GetTotalNumDof() const
	{ return nTotalDof; }

	//! Get the pantograph parameters
	inline Pantograph_0_Params GetPantographParams() const
	{ return panto_params; }
      
	//! Set the pantograph parameters
	inline void SetPantographParams(const Pantograph_0_Params& params)
	{ panto_params = params; }

	//! Set initial conditions
	void SetInitialConditions(const double* uvals, const double* vvals, const double* avals) const;
	
	//! Main functionality: static solve
	void StaticInitialize() const;

	//! Advance the state to the next time step
	void Advance();

	//! Access the state
	inline const DynamicState& GetState() const
	{ return TI.GetState(); }

	//! Access the time
	inline double GetTime() const
	{ return TI.GetTime(); }

	//! Access the load point deflection, velocity and acceleration
	std::array<double,3> GetLoadPointKinematics() const;
     
	//! Visualize
	void Write(const std::vector<std::string> ohe_fnames, const std::string panto_fname) const;
      
      private:
	//! evaluate mass, stiffness and force
	void Evaluate(const double time, Mat& M, Mat& K, Vec& F) const;
	
	bool isDestroyed;
	const OHE_TYPE            &ohe;
	Pantograph_0_Params       panto_params;
	TimeIntegrator            &TI;
	const std::vector<int>    dirichlet_dofs;
	const int                 nTotalDof;

	mutable PetscData         PD;
	mutable LinearSolver      linSolver;
      };

    // Instantiations
    using Model_Cable_MovingLoad          = Model_MovingLoad<HeavyTensionedBeam>;
    using Model_MultiSpanCable_MovingLoad = Model_MovingLoad<MultiSpan_Cable>;
    using Model_SingleSpanOHE_MovingLoad  = Model_MovingLoad<SingleSpan_OHE>;
    using Model_MultiSpanOHE_MovingLoad   = Model_MovingLoad<MultiSpan_OHE>;
  }
}

// Implementation
#include <pfe_linear_Model_MovingLoad_IMPL.h>

