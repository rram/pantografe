
/** \file test_Bowe_Mullarkey_2.cpp
 * \brief Test for the class pfe::linear::Model_MovingLoad
 * \author Ramsharan Rangarajan
 */

#include <pfe_linear_Model_MovingLoad.h>
#include <pfe_HHTAlpha.h>
#include <pfe_GenAlpha.h>
#include <iostream>
#include <fstream>
#include <filesystem>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRQ(ierr);

  // Cable parameters
  pfe::HeavyTensionedBeamParams beam_params;
  beam_params.tag    = "beam";
  beam_params.span   = 7.62;
  beam_params.EI     = 2.07*4.58*1.e6;
  beam_params.Tension = 0.0;
  beam_params.rhoA   = 46.0;
  beam_params.g      = 0.0;
  beam_params.zLevel = 0.;

  // Pantograph parameters
  pfe::Pantograph_0_Params panto_params;
  panto_params.tag = "panto";
  panto_params.xinit = 0.0;
  panto_params.speed = 50.8;
  panto_params.force = 0.;
  const double mg    = -2629.0*9.81;
  
  // Create the cable
  const int nElements = 50;
  pfe::HeavyTensionedBeam beam(beam_params, nElements);

  // clamp the right end of the beam
  const int nNodes = beam.GetNumNodes();
  const std::vector<int> dirichlet_dofs{nNodes-1, 2*nNodes-1};

  // Create time integrator
  const double timestep = 0.00125;
  pfe::GenAlpha TI(0.9, beam.GetTotalNumDof(), timestep);

  // Create an instance of the model
  pfe::linear::Model_Cable_MovingLoad model(beam, panto_params, TI, dirichlet_dofs);
  const int nTotalDof = model.GetTotalNumDof();
    
  // Static solution
  model.StaticInitialize();

  // visualize
  model.Write({"cable-0.dat"}, "load-0.dat");

  // History
  std::fstream hfile;
  hfile.open("history.dat", std::ios::out);
  assert(hfile.good());

  // # time steps
  const double tend        = beam_params.span/panto_params.speed;
  const int num_time_steps = static_cast<int>(tend/timestep)-1;

  // Set the pantograph force
  panto_params.force = mg;
  model.SetPantographParams(panto_params);
  
  // Moving pantograph
  for(int tstep=0; tstep<num_time_steps; ++tstep)
    {
      // record the tip deflection
      const int end_dof = 0;
      double end_U;
      VecGetValues(model.GetState().U, 1, &end_dof, &end_U);
      hfile << model.GetTime() << "\t" << end_U << std::endl;

      // advance to the next instant
      std::cout << "Time step " << tstep << std::endl;
      model.Advance();
      std::string tstr = std::to_string(tstep+1)+".dat";
      model.Write({"cable-"+tstr}, "load-"+tstr);
    }
  
  // Clean up
  TI.Destroy();
  model.Destroy();
  beam.Destroy();
  PetscFinalize();
}
