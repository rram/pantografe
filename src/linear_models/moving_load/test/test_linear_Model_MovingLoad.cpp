
/** \file test_linear_Model_MovingLoad.cpp
 * \brief Test for the class pfe::linear::Model_MovingLoad
 * \author Ramsharan Rangarajan, Suman Dutta
 */

#include <pfe_linear_Model_MovingLoad.h>
#include <pfe_Newmark.h>
#include <iostream>
#include <fstream>
#include <filesystem>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRQ(ierr);

  // Benchmark study parameters
  pfe::OHESpan_ParameterPack ohe_pack;
  std::fstream jfile;
  assert(std::filesystem::exists("benchmark_params.json"));
  jfile.open("benchmark_params.json", std::ios::in);
  assert(jfile.is_open());
  jfile >> ohe_pack;

  // Constant for pantograph parameters
  pfe::Pantograph_0_Params panto_params;
  panto_params.tag = "pantograph 0";
  jfile >> panto_params;
  jfile.close();

  // Create the OHE span
  const int nElements = 20;
  pfe::linear::SingleSpan_OHE ohe(0., ohe_pack, nElements);

  // Create time integrator
  const double timestep = 0.002;
  pfe::Newmark TI(ohe.GetTotalNumDof(), timestep);

  // Create an instance of the model
  pfe::linear::Model_SingleSpanOHE_MovingLoad model(ohe, panto_params, TI);
  const int nTotalDof = model.GetTotalNumDof();
    
  // Static solution
  model.StaticInitialize();

  // visualize
  model.Write({"catenary-0.dat","contact-0.dat","dropper-0.dat"}, "load-0.dat");
 
  std::fstream hfile;
  hfile.open("history.dat", std::ios::out);
  assert(hfile.good());
  hfile << "# time \t u \t v \t a "<< std::endl;
  std::array<double,3> ohe_uva   = model.GetLoadPointKinematics();
  hfile << 0.0 << "\t" << ohe_uva[0] << "\t" << ohe_uva[1] << "\t" << ohe_uva[2] << "\t" << std::endl;
    	   
  // Moving pantograph
  for(int tstep=0; tstep<275; ++tstep)
    {
      std::cout << "Time step " << tstep << std::endl;
      model.Advance();
      std::string tstr = std::to_string(tstep+1)+".dat";
      model.Write({"catenary-"+tstr, "contact-"+tstr, "dropper-"+tstr}, "load-"+tstr);
               
      ohe_uva   = model.GetLoadPointKinematics();
      hfile << model.GetTime() << "\t" << ohe_uva[0] << "\t" << ohe_uva[1] << "\t" << ohe_uva[2] << "\t" << std::endl;
    }
    
  hfile.close();
  
  // Clean up
  TI.Destroy();
  model.Destroy();
  ohe.Destroy();
  PetscFinalize();
}
