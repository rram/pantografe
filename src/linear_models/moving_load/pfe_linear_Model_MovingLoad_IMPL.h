
/** \file pfe_linear_Model_MovingLoad_IMPL.h
 * \brief Implements the templated class pfe::linear::Model_MovingLoad
 * \author Ramsharan Rangarajan
 */

#pragma once

namespace pfe
{
  namespace linear
  {
    // Constructor
    template<typename OHE_TYPE>
      Model_MovingLoad<OHE_TYPE>::Model_MovingLoad(const OHE_TYPE &obj1,
						   const Pantograph_0_Params& obj2,
						   TimeIntegrator &ti,
						   const std::vector<int> dirichlet)
      :isDestroyed(false),
      ohe(obj1),
      panto_params(obj2),
      TI(ti),
      dirichlet_dofs(dirichlet),
      nTotalDof(ohe.GetTotalNumDof())
	{
	  // sparsity of the ohe component
	  std::vector<int> nz = ohe.GetSparsity();
	
	  // Create PETSc data structure
	  PD.Initialize(nz);

	  // done
	}


    // Detsroy
    template<typename OHE_TYPE>
      void Model_MovingLoad<OHE_TYPE>::Destroy()
      {
	assert(isDestroyed==false);
	PetscErrorCode ierr;
	PetscBool      isFinalized;
	ierr = PetscFinalized(&isFinalized); CHKERRV(ierr);
	assert(isFinalized==PETSC_FALSE);

	PD.Destroy();
	linSolver.Destroy();
	isDestroyed = true;
      }
    
    // Destructor
    template<typename OHE_TYPE>
      Model_MovingLoad<OHE_TYPE>::~Model_MovingLoad()
      {
	assert(isDestroyed==true);
      }
    
    // Main functionlity: evaluate mass, stiffness and force vectors
    template<typename OHE_TYPE>
      void Model_MovingLoad<OHE_TYPE>::Evaluate(const double time, Mat &M, Mat &K, Vec &F) const
      {
	assert(isDestroyed==false);
	
	// Evaluate contribution from OHE
	ohe.Evaluate(M, K, F);

	// finish up assembly
	PetscErrorCode ierr;
	ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY);                        CHKERRV(ierr);
	ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY);                        CHKERRV(ierr);
	ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY);                        CHKERRV(ierr);
	ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY);                        CHKERRV(ierr);
	ierr = VecAssemblyBegin(F);                                            CHKERRV(ierr);
	ierr = VecAssemblyEnd(F);                                              CHKERRV(ierr);
      
	// position of the panto
	const double x = panto_params.xinit + panto_params.speed*time;
      
	// interacting dofs and shape function values
	auto dof_shp_pairs = ohe.GetActiveDofs(x);

	// Add contribution from the moving force
	const double& F0 = panto_params.force;
	for(auto& it:dof_shp_pairs)
	  {
	    const auto& row = it.first;
	    const auto& Na  = it.second;
	    const double val = F0*Na;
	    ierr = VecSetValues(F, 1, &row, &val, ADD_VALUES); CHKERRV(ierr);
	  }      
	ierr = VecAssemblyBegin(F);                        CHKERRV(ierr);
	ierr = VecAssemblyEnd(F);                          CHKERRV(ierr);

	// done
	return;
      }

  
    // Set initial conditions
    template<typename OHE_TYPE>
      void Model_MovingLoad<OHE_TYPE>::SetInitialConditions(const double* uvals, const double* vvals, const double* avals) const
      {
	assert(isDestroyed==false);
	
	// Aliases
	auto& M = PD.massMAT;
	auto& K = PD.stiffnessMAT;
	auto& F = PD.resVEC;
	auto& U = PD.solutionVEC;

	// zero
	PetscErrorCode ierr;
	ierr = MatZeroEntries(M); CHKERRV(ierr);
	ierr = MatZeroEntries(K); CHKERRV(ierr);
	ierr = VecZeroEntries(F); CHKERRV(ierr);
	ierr = VecZeroEntries(U); CHKERRV(ierr);
      
	// Preserve nonzero structure
	ierr = MatSetOption(K, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE); CHKERRV(ierr);
	ierr = MatSetOption(M, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE); CHKERRV(ierr);

	// Evaluate M, K, F.
	// M is not required
	Evaluate(0., M, K, F);
      
	// Set operators for the time integrator initialization
	TI.SetOperators(M, K, dirichlet_dofs);

	// Set initial conditions
	TI.SetInitialConditions(0., uvals, vvals, avals);

	// done
	return;
      }

  
    // Main functionality: static solve
    template<typename OHE_TYPE>
      void Model_MovingLoad<OHE_TYPE>::StaticInitialize() const
      {
	assert(isDestroyed==false);
	const int nbcs = static_cast<int>(dirichlet_dofs.size());
	const std::vector<double> dirichlet_values(nbcs, 0.);
      
	// Aliases
	auto& M = PD.massMAT;
	auto& K = PD.stiffnessMAT;
	auto& F = PD.resVEC;
	auto& U = PD.solutionVEC;

	// zero
	PetscErrorCode ierr;
	ierr = MatZeroEntries(M); CHKERRV(ierr);
	ierr = MatZeroEntries(K); CHKERRV(ierr);
	ierr = VecZeroEntries(F); CHKERRV(ierr);
	ierr = VecZeroEntries(U); CHKERRV(ierr);
      
	// Preserve nonzero structure
	ierr = MatSetOption(K, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE); CHKERRV(ierr);
	ierr = MatSetOption(M, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE); CHKERRV(ierr);

	// Evaluate M, K, F.
	// M is not required
	Evaluate(0., M, K, F);
      
	// Set operators for the time integrator initialization
	TI.SetOperators(M, K, dirichlet_dofs);

	// Set dirichlet BCs
	ierr = MatZeroRows(K, nbcs, &dirichlet_dofs[0], 1.0, PETSC_NULLPTR, PETSC_NULLPTR);    CHKERRV(ierr);
	ierr = VecSetValues(F, nbcs, &dirichlet_dofs[0], &dirichlet_values[0], INSERT_VALUES); CHKERRV(ierr);
	ierr = VecAssemblyBegin(F); CHKERRV(ierr);
	ierr = VecAssemblyEnd(F);   CHKERRV(ierr);

	// Solve
	linSolver.SetOperator(K);
	linSolver.Solve(F, U);

	// Set the static solution as the initial condition for the time integrator
	std::vector<double> zero(nTotalDof); // velocity and acceleration
	std::fill(zero.begin(), zero.end(), 0.);
	double* uvals;
	ierr = VecGetArray(U, &uvals);                        CHKERRV(ierr);
	TI.SetInitialConditions(0., uvals, zero.data(), zero.data());
	ierr = VecRestoreArray(U, &uvals);                    CHKERRV(ierr);

	// done
	return;
      }


    // Advance the solution to the next time step
    template<typename OHE_TYPE>
      void Model_MovingLoad<OHE_TYPE>::Advance()
      {
	assert(isDestroyed==false);
	
	// Aliases
	auto& M = PD.massMAT;
	auto& K = PD.stiffnessMAT;
	auto& F = PD.resVEC;
	auto& U = PD.solutionVEC;

	// Initialize to zero before assembly
	PetscErrorCode ierr;
	ierr = MatZeroEntries(M); CHKERRV(ierr);
	ierr = MatZeroEntries(K); CHKERRV(ierr);
	ierr = VecZeroEntries(F); CHKERRV(ierr);
	ierr = VecZeroEntries(U); CHKERRV(ierr);
      
	// Evaluate operators and loads
	const double time = TI.GetTime()+TI.GetTimestep();
	Evaluate(time, M, K, F);

	// Operator is unchanged- hence not setting it again in TI.
      
	// Advance in time
	TI.Advance(F);

	// done
	return;
      }


    // Access the load point deflection, velocity and acceleration
    template<typename OHE_TYPE>
      std::array<double,3> Model_MovingLoad<OHE_TYPE>::GetLoadPointKinematics() const
      {
	assert(isDestroyed==false);
	
	// current time
	const double time = TI.GetTime();

	// pantograph position
	const double x = panto_params.xinit + panto_params.speed*time;
      
	// interacting dofs and shape function values
	auto dof_shp_pairs = ohe.GetActiveDofs(x);
      
	// state
	const auto& state = TI.GetState();
	const auto& U = state.U;
	const auto& V = state.V;
	const auto& A = state.A;

	// Evaluate position, velocity and acceleration
	double val;
	std::array<double,3> uva{0.,0.,0.};
	for(auto& it:dof_shp_pairs)
	  {
	    const int& a     = it.first;
	    const double& Na = it.second;

	    VecGetValues(U, 1, &a, &val);
	    uva[0] += val*Na;

	    VecGetValues(V, 1, &a, &val);
	    uva[1] += val*Na;

	    VecGetValues(A, 1, &a, &val);
	    uva[2] += val*Na;
	  }

	// done
	return uva;
      }
      
  
    // Visualize
    template<typename OHE_TYPE>
      void Model_MovingLoad<OHE_TYPE>::Write(const std::vector<std::string> ohe_fnames, const std::string panto_fname) const
      {
	assert(isDestroyed==false);
	
	// Aliases
	const auto& state = TI.GetState();
	const Vec& U = state.U;
	const Vec& V = state.V;
	const Vec& A = state.A;

	// ohe profiles
	double *disp, *vel, *accn;
	PetscErrorCode ierr;
	ierr = VecGetArray(U, &disp);              CHKERRV(ierr);
	ierr = VecGetArray(V, &vel);               CHKERRV(ierr);
	ierr = VecGetArray(A, &accn);              CHKERRV(ierr); 
	ohe.Write(disp, vel, accn, ohe_fnames); 
	ierr = VecRestoreArray(U, &disp);          CHKERRV(ierr);
	ierr = VecRestoreArray(V, &vel);           CHKERRV(ierr);
	ierr = VecRestoreArray(A, &accn);          CHKERRV(ierr);

	// pantograph position
	const double time = TI.GetTime();
	const double x = panto_params.xinit + panto_params.speed*time;

	// Contact wire displacement here
	double wval = 0.;
	auto dof_shp_pairs = ohe.GetActiveDofs(x);
	for(auto& it:dof_shp_pairs)
	  {
	    auto& dof = it.first;
	    auto& shpval = it.second;
	    double wnode;
	    ierr = VecGetValues(U, 1, &dof, &wnode); CHKERRV(ierr);
	    wval += wnode*shpval;
	  }

	// vertical extent of the pantograph
	const double z0 = panto_params.zbase;
	const double z1 = ohe.GetSpan(0).GetDatum() + wval;

	// write to file
	std::fstream pfile;
	pfile.open(panto_fname, std::ios::out);
	assert(pfile.good());
	pfile << x << " " << z0 << std::endl
	      << x << " " << z1 << std::endl;
	pfile.close();

	// done
	return;
      }
  }
}
