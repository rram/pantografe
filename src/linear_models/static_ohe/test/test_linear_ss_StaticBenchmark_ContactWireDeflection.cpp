
/** \file test_linear_StaticBenchmark_ContactWireDeflection.cpp
 * \brief Unit test for class pfe::linear::SingleSpan_OHE 
 * \author Ramsharan Rangarajan
 */


#include <pfe_linear_SingleSpan_OHE.h>
#include <filesystem>

using OHE_TYPE = pfe::linear::SingleSpan_OHE;

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Benchmark study parameters
  pfe::OHESpan_ParameterPack pack;
  std::fstream jfile;
  assert(std::filesystem::exists("benchmark_params.json"));
  jfile.open("benchmark_params.json", std::ios::in);
  assert(jfile.is_open());
  jfile >> pack;  
  jfile.close();

  // Create a single span
  const int nElements = 20;
  OHE_TYPE ohe_span(0., pack, nElements);

  // Get the dropper nodes
  auto dropper_nodes = ohe_span.GetDropperNodes();

  std::vector<std::pair<int,double>> dropper_nodal_disp(pack.dropperSchedule.nDroppers);
  for(int d=0; d<pack.dropperSchedule.nDroppers; ++d)
    dropper_nodal_disp[d] = std::make_pair(dropper_nodes[d], pack.dropperSchedule.target_sag[d]);
  
  // Compute contact wire configuration with given sag
  auto& contact_wire = ohe_span.ContactWire();
  const int nDofs = contact_wire.GetLocalToGlobalMap().GetTotalNumDof();
  std::vector<double> displacements(nDofs);
  contact_wire.StaticSolve(dropper_nodal_disp, displacements.data());

  // Visualize
  contact_wire.Write(displacements.data(), "contact.dat");
  
  // Clean up
  ohe_span.Destroy();
  PetscFinalize();
}
