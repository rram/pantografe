
/** \file test_linear_ms_StaticBenchmark_DropperLengthAndStiffness.cpp
 * \brief Unit test for class pfe::linear::Model_Static_MultiSpan_OHE
 * \author Ramsharan Rangarajan
 */


#include <pfe_linear_Model_Static_OHE.h>
#include <filesystem>

using OHE_TYPE   = pfe::linear::MultiSpan_OHE;
using MODEL_TYPE = pfe::linear::Model_Static_MultiSpan_OHE;

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Benchmark study parameters
  pfe::OHESpan_ParameterPack pack;
  std::fstream jfile;
  assert(std::filesystem::exists("benchmark_params.json"));
  jfile.open("benchmark_params.json", std::ios::in);
  assert(jfile.is_open());
  jfile >> pack;
  jfile.close();
  
  // Create a single span
  const int nElements = 10;
  const int num_spans = 7;
  const int calc_span = 3;
  OHE_TYPE ohe(0., pack, num_spans, nElements);
  auto& ohe_span = ohe.GetSpan(calc_span);
  
  // Create an instance of the static model
  MODEL_TYPE model(ohe);

  // Adjust dropper lengths to achieve sag over 20 iterations
  model.AdjustDropperLengths(calc_span, 50);

  // Solve at the adjusted dropper lengths
  const int nTotalDof = model.GetTotalNumDof();
  std::vector<double> displacements(nTotalDof);
  model.Solve(displacements.data());

  // Compare target and achieved displacements
  std::fstream pfile;
  const int nDroppers = pack.dropperSchedule.nDroppers;
  const auto dropper_dofs = ohe.GetDropperDofs(calc_span);
  pfile.open("dropper_adjustments.dat", std::ios::out);
  assert(pfile.good());
  pfile <<"# dropper-index x-dropper, target sag, realized sag, nominal length, adjusted length" << std::endl;
  for(int d=0; d<nDroppers; ++d)
    pfile << d <<" " << pack.dropperSchedule.coordinates[d] << " "
	  << pack.dropperSchedule.target_sag[d] << " " << displacements[dropper_dofs[d].second] << " "
	  << pack.dropperSchedule.nominal_lengths[d] << " " << ohe_span.GetDroppers()[d]->GetLength() << std::endl;
  pfile.close();


  // Compute the transverse stiffness
  const auto& coordinates = ohe_span.ContactWire().GetCoordinates();
  const int nNodes = ohe_span.ContactWire().GetNumNodes();
  std::vector<double> stiffness(nNodes);
  model.ComputeTransverseStiffnessDistribution(calc_span, displacements.data(), 100., stiffness.data());
  pfile.open("stiffness.dat", std::ios::out);
  for(int a=0; a<nNodes; ++a)
    pfile << coordinates[a]<<" "<<stiffness[a]<<"\n";
  pfile.close();
  
  // Clean up
  ohe.Destroy();
  model.Destroy();
  PetscFinalize();
}
