
/** \file test_linear_StaticBenchmark_Deflection.cpp
 * \brief Unit test for class pfe::linear::Model_Static_MultiSpan_OHE
 * \author Ramsharan Rangarajan
 */

#include <pfe_linear_Model_Static_OHE.h>
#include <filesystem>

using MODEL_TYPE = pfe::linear::Model_Static_MultiSpan_OHE;
using OHE_TYPE   = pfe::linear::MultiSpan_OHE;

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Benchmark study parameters
  pfe::OHESpan_ParameterPack pack;
  std::fstream jfile;
  assert(std::filesystem::exists("benchmark_params.json"));
  jfile.open("benchmark_params.json", std::ios::in);
  assert(jfile.is_open());
  jfile >> pack;
  jfile.close();
  
  // Create a single span
  const int nElements = 10;
  const int num_spans = 3;
  const int calc_span = 1;
  OHE_TYPE ohe(0., pack, num_spans, nElements);
  
  // Create an instance of the model
  MODEL_TYPE model(ohe);
  
  // Solve
  const int nTotalDof = model.GetTotalNumDof();
  std::vector<double> displacements(nTotalDof);
  model.Solve(displacements.data());

  // Visualize
  model.Write(displacements.data(), "catenary.dat", "contact.dat", "droppers.dat");
  
  // Clean up
  model.Destroy();
  ohe.Destroy();
  PetscFinalize();
}
