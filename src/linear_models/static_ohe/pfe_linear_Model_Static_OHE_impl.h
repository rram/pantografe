
/** \file pfe_linear_Model_Static_OHE_impl.h
 * \brief Implementation of the class pfe::linear::Model_Static_OHE
 * \author Ramsharan Rangarajan
 */

#pragma once

namespace pfe
{
  namespace linear
  {
    // Constructor
    template<typename OHE_TYPE>
      Model_Static_OHE<OHE_TYPE>::Model_Static_OHE(OHE_TYPE& obj)
      :isDestroyed(false), ohe(obj)
    {
      PetscErrorCode ierr;
      PetscBool isInitialized;
      ierr = PetscInitialized(&isInitialized); CHKERRV(ierr);
      assert(isInitialized==PETSC_TRUE);
    
      // sparsity
      auto nz = ohe.GetSparsity();

      // Initialize PETSc data structures with known sparsity 
      PD.Initialize(nz);
    }

    // Destroy
    template<typename OHE_TYPE>
      void Model_Static_OHE<OHE_TYPE>::Destroy()
      {
	assert(isDestroyed==false);
	  
	// Check that PETSc has not been finalized
	PetscBool isFinalized;
	auto ierr = PetscFinalized(&isFinalized); CHKERRV(ierr);
	assert(isFinalized==PETSC_FALSE);
	PD.Destroy();
	linSolver.Destroy();
	isDestroyed = true;
      }
      
    // Destructor
    template<typename OHE_TYPE>
      Model_Static_OHE<OHE_TYPE>::~Model_Static_OHE()
      {
	assert(isDestroyed==true);
      }

    // Main functionality: solve
    template<typename OHE_TYPE>
      void Model_Static_OHE<OHE_TYPE>::Solve(double* displacements)
      {
	assert(isDestroyed==false);
	
	// Zero
	PetscErrorCode ierr;
	ierr = MatZeroEntries(PD.stiffnessMAT); CHKERRV(ierr);
	ierr = VecZeroEntries(PD.resVEC);       CHKERRV(ierr);
	ierr = VecZeroEntries(PD.solutionVEC);  CHKERRV(ierr);

	// Assemble contributions to (K, F) from the catenary cable, contact wire,
	// dropper cables and suspension springs
	ohe.Evaluate(PD.massMAT, PD.stiffnessMAT, PD.resVEC);

	// Solve
	linSolver.SetOperator(PD.stiffnessMAT);
	linSolver.Solve(PD.resVEC, PD.solutionVEC);

	// Get the solution
	double* solution;
	ierr = VecGetArray(PD.solutionVEC, &solution); CHKERRV(ierr);
	const int nTotalDof = ohe.GetTotalNumDof();
	for(int a=0; a<nTotalDof; ++a)
	  displacements[a] = solution[a];
	ierr = VecRestoreArray(PD.solutionVEC, &solution); CHKERRV(ierr);

	// done
	return;
      }


    // Main functionalty: match a prescribed value of sag
    template<typename OHE_TYPE>
      void Model_Static_OHE<OHE_TYPE>::AdjustDropperLengths(const int span_num, const int nIters)
      {
	assert(isDestroyed==false);
	
	const int num_spans = ohe.GetNumSpans();
	assert(span_num>=0 && span_num<num_spans);
	auto& ohe_span = ohe.GetSpan(span_num);
	const auto& droppers = ohe_span.GetDroppers();
	const auto& schedule = ohe_span.GetDropperArrangement();
	const int nDroppers = static_cast<int>(droppers.size());
	const auto& target_sag = schedule.target_sag;
	
	const int nTotalDof = ohe.GetTotalNumDof();
	std::vector<double> displacements(nTotalDof);
	std::vector<double> lengths(nDroppers);
	const auto dropper_dofs = ohe.GetDropperDofs(span_num);
    
	// Iterate over dropper lengths
	for(int iter=0; iter<=nIters; ++iter)
	  {    
	    // solve in the current state
	    Solve(displacements.data());

	    // Adjust dropper lengths towards achieving the target
	    for(int d=0; d<nDroppers; ++d)
	      {
		double diff = target_sag[d]-displacements[dropper_dofs[d].second];
		lengths[d] = droppers[d]->GetLength()-diff;
		assert(lengths[d]>0.);
	      }
	    
	    // Set the new dropper lengths
	    for(int s=0; s<num_spans; ++s)
	      ohe.GetSpan(s).ResetDropperLengths(lengths.data());
	  }

	// done
	return;
      }


    // Compute transverse stiffness disrtribution
    template<typename OHE_TYPE>
      void Model_Static_OHE<OHE_TYPE>::
      ComputeTransverseStiffnessDistribution(const int span_num,
					     const double* displacements,
					     const double Force,
					     double* stiffness) const
      {
	assert(isDestroyed==false);
	const int num_spans = ohe.GetNumSpans();
	assert(span_num>=0 && span_num<num_spans);
	auto& ohe_span = ohe.GetSpan(span_num);

    
	// Apply the given force at each node of the contact wire along the span
	// Measure the displacement and compute the stiffness
	const auto& contact_wire = ohe_span.ContactWire();
	const int nNodes = contact_wire.GetNumNodes();
	const int nTotalDof = ohe.GetTotalNumDof();
    
	// Loop over the loading node
	PetscErrorCode ierr;
	for(int lnode=0; lnode<nNodes; ++lnode)
	  {
	    ierr = MatZeroEntries(PD.stiffnessMAT); CHKERRV(ierr);
	    ierr = VecZeroEntries(PD.resVEC);       CHKERRV(ierr);
	    ierr = VecZeroEntries(PD.solutionVEC);  CHKERRV(ierr);
	
	    // Assemble
	    ohe.Evaluate(PD.massMAT, PD.stiffnessMAT, PD.resVEC);

	    // Add transverse force at "lnode" on the contact wire
	    const int dof = ohe.MapContactWireNodeToDof(span_num, lnode);
	    ierr = VecSetValues(PD.resVEC, 1, &dof, &Force, ADD_VALUES); CHKERRV(ierr);
	    ierr = VecAssemblyBegin(PD.resVEC);                          CHKERRV(ierr);
	    ierr = VecAssemblyEnd(PD.resVEC);                            CHKERRV(ierr);
	
	    // Solve
	    linSolver.SetOperator(PD.stiffnessMAT);
	    linSolver.Solve(PD.resVEC, PD.solutionVEC);

	    // Get the solution
	    double* solution;
	    ierr = VecGetArray(PD.solutionVEC, &solution); CHKERRV(ierr);

	    // relative displacement
	    double delta_disp = solution[dof]-displacements[dof];

	    // restore
	    ierr = VecRestoreArray(PD.solutionVEC, &solution); CHKERRV(ierr);

	    // Compute the stiffness
	    assert(std::abs(delta_disp)>1.e-9);
	    stiffness[lnode] = Force/delta_disp;
	  }
    
	// done
	return;
      }
    
  }
}
