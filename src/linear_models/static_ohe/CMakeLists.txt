
# add library
add_library(pfe_linear_model_static_ohe INTERFACE)

# headers
target_include_directories(pfe_linear_model_static_ohe INTERFACE
  				      $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
		                      $<INSTALL_INTERFACE:include>)

# link
target_link_libraries(pfe_linear_model_static_ohe INTERFACE pfe_linear_ohe)

# Add required flags
target_compile_features(pfe_linear_model_static_ohe INTERFACE ${pfe_COMPILE_FEATURES})

install(FILES
	pfe_linear_Model_Static_OHE.h
	pfe_linear_Model_Static_OHE_impl.h
	DESTINATION ${PROJECT_NAME}/include)

