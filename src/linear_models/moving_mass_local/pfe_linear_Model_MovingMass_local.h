
/** \file pfe_linear_Model_MovingMass_local.h
 * \brief Defines the class pfe::linear::Model_MovingMass_local
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_Pantograph.h>
#include <pfe_TimeIntegrator.h>
#include <pfe_PetscData.h>

// instantiations
#include <pfe_HeavyTensionedBeam.h>

namespace pfe
{
  namespace linear
  {
    //! \ingroup models
    template<typename OHE_TYPE>
      class Model_MovingMass_local
      {
      public:

	//! Constructor
	Model_MovingMass_local(const OHE_TYPE& obj1,
			       const Pantograph_1_Params& obj2,
			       TimeIntegrator& ti, 
			       const std::vector<int> dirichlet={});
	//! Destructor
	virtual ~Model_MovingMass_local();

	//! Disable copy and assignment
	Model_MovingMass_local(const Model_MovingMass_local<OHE_TYPE>&) = delete;
	Model_MovingMass_local<OHE_TYPE>& operator=(const Model_MovingMass_local<OHE_TYPE>&) = delete;

	//! Destroy
	virtual	void Destroy();
	
	//! Access the ohe system
	inline const OHE_TYPE& GetOHE() const
	{ return ohe; }

	//! Access the total number of dofs
	inline int GetTotalNumDof() const
	{ return nTotalDof; }

	//! Access the pantograph
	inline const Pantograph_1_Params& GetPantographParams() const
	{ return panto_params; }

	//! Main functionality: static solve
	void StaticInitialize() const;

	//! Set initiali conditions
	void SetInitialConditions(const double* uvals, const double* vvals, const double* avals) const;

	//! Advance to the next time step
	void Advance();

	//! Access the state
	inline const DynamicState& GetState() const
	{ return TI.GetState(); }

	//! Access the time
	inline double GetTime() const
	{ return TI.GetTime(); }

	//! Visualize
	void Write(const std::vector<std::string> ohe_fnames, const std::string panto_fname) const;

      private:
	//! Main functionality: evaluate mass, stiffness and force
	void Evaluate(const double time, Mat& M, Mat& K, Vec& F) const;

	bool isDestroyed;
	const OHE_TYPE            &ohe;
	const Pantograph_1_Params panto_params;
	TimeIntegrator            &TI;
	const std::vector<int>    dirichlet_dofs;
	const int                 nTotalDof;
      
	mutable Mat               Mb, Kb, M, K;
	mutable Vec               Ub, Fb, F;
      };

    // Instantiations
    using Model_Cable_MovingMass_local = Model_MovingMass_local<HeavyTensionedBeam>;
  }
}

// Implementation
#include <pfe_linear_Model_MovingMass_local_IMPL.h>

