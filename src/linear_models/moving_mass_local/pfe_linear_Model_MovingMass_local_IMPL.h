
/** \file pfe_linear_Model_MovingMass_local_IMPL.h
 * \brief Implements the templated class pfe::linear::Model_MovingMass_local_IMPL
 * \author Ramsharan Rangarajan
 */

#pragma once

namespace pfe
{
  namespace linear
  {
    // Constructor
    template<typename OHE_TYPE>
      Model_MovingMass_local<OHE_TYPE>::Model_MovingMass_local(const OHE_TYPE& obj1,
							       const Pantograph_1_Params& obj2,
							       TimeIntegrator& ti, 
							       const std::vector<int> dirichlet)
      :isDestroyed(false),
      ohe(obj1),
      panto_params(obj2),
      TI(ti), 
      dirichlet_dofs(dirichlet),
      nTotalDof(ohe.GetTotalNumDof())
	{
	  // sparsity of the ohe component
	  const std::vector<int> nz = ohe.GetSparsity();

	  // Mass, stiffness and force of the ohe
	  PetscErrorCode ierr;
	  ierr = MatCreateSeqAIJ(PETSC_COMM_WORLD, nTotalDof, nTotalDof, PETSC_DEFAULT, nz.data(), &Mb); CHKERRV(ierr);
	  ierr = MatCreateSeqAIJ(PETSC_COMM_WORLD, nTotalDof, nTotalDof, PETSC_DEFAULT, nz.data(), &Kb); CHKERRV(ierr);
	  ierr = MatSetOption(Mb, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE); CHKERRV(ierr);
	  ierr = MatSetOption(Kb, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE); CHKERRV(ierr);
	  ierr = VecCreateSeq(PETSC_COMM_WORLD, nTotalDof, &Ub);         CHKERRV(ierr);
	  ierr = VecCreateSeq(PETSC_COMM_WORLD, nTotalDof, &Fb);         CHKERRV(ierr);
	
	  // zero
	  ierr = MatZeroEntries(Mb); CHKERRV(ierr);
	  ierr = MatZeroEntries(Kb); CHKERRV(ierr);
	  ierr = VecZeroEntries(Ub); CHKERRV(ierr);
	  ierr = VecZeroEntries(Fb); CHKERRV(ierr);

	  // evaluate ohe data
	  ohe.Evaluate(0.0, Ub, Mb, Kb, Fb);

	  // Mass, stiffness and force for ohe + panto
	  ierr = MatDuplicate(Mb, MAT_DO_NOT_COPY_VALUES, &M);           CHKERRV(ierr);
	  ierr = MatDuplicate(Kb, MAT_DO_NOT_COPY_VALUES, &K);           CHKERRV(ierr);
	  ierr = MatSetOption(M, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
	  ierr = MatSetOption(K, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
	  ierr = MatSetOption(M, MAT_KEEP_NONZERO_PATTERN,  PETSC_TRUE); CHKERRV(ierr);
	  ierr = MatSetOption(K, MAT_KEEP_NONZERO_PATTERN,  PETSC_TRUE); CHKERRV(ierr);
	  ierr = VecCreateSeq(PETSC_COMM_WORLD, nTotalDof, &F);          CHKERRV(ierr);

	  // done
	}

    // destroy
    template<typename OHE_TYPE>
      void Model_MovingMass_local<OHE_TYPE>::Destroy()
      {
	assert(isDestroyed==false);
	PetscErrorCode ierr;
	PetscBool      isFinalized;
	ierr = PetscFinalized(&isFinalized); CHKERRV(ierr);
	assert(isFinalized==PETSC_FALSE);

	ierr = MatDestroy(&M);  CHKERRV(ierr);
	ierr = MatDestroy(&K);  CHKERRV(ierr);
	ierr = MatDestroy(&Mb); CHKERRV(ierr);
	ierr = MatDestroy(&Kb); CHKERRV(ierr);
	ierr = VecDestroy(&F);  CHKERRV(ierr);
	ierr = VecDestroy(&Fb); CHKERRV(ierr);
	ierr = VecDestroy(&Ub); CHKERRV(ierr);

	isDestroyed = true;
	return;
      }

    // destructor
    template<typename OHE_TYPE>
      Model_MovingMass_local<OHE_TYPE>::~Model_MovingMass_local()
      {
	assert(isDestroyed==true);
      }
    
  
    // Main functionality: evaluate mass, stiffness and force
    template<typename OHE_TYPE>
      void Model_MovingMass_local<OHE_TYPE>::Evaluate(const double time, Mat& Mmat, Mat& Kmat, Vec& Fvec) const
      {
	assert(isDestroyed==false);
	
	// Copy contributions from the ohe
	PetscErrorCode ierr;
	ierr = MatZeroEntries(Mmat); CHKERRV(ierr);
	ierr = MatZeroEntries(Kmat); CHKERRV(ierr);
	ierr = VecZeroEntries(Fvec); CHKERRV(ierr);
	ierr = MatCopy(Mb, Mmat, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);
	ierr = MatCopy(Kb, Kmat, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);
	ierr = VecCopy(Fb, Fvec);                            CHKERRV(ierr);

	// position of the pantograph
	const double x = panto_params.xinit + time*panto_params.speed;
      
	// active dofs
	const auto dof_shp = ohe.GetActiveDisplacementDofs(x);

	// M = Mb + m (r x r)
	// K = Kb + k (r x r)
	// F = Fb + fp r
	double val;
	for(auto& it:dof_shp)
	  {
	    const int& a     = it.first;
	    const double& Na = it.second;

	    // update force
	    val = panto_params.force*Na;
	    ierr = VecSetValues(Fvec, 1, &a, &val, ADD_VALUES); CHKERRV(ierr);

	    // update matrices
	    for(auto& jt:dof_shp)
	      {
		const int& b     = jt.first;
		const double& Nb = jt.second;
		val  = panto_params.m*Na*Nb;
		ierr = MatSetValues(Mmat, 1, &a, 1, &b, &val, ADD_VALUES); CHKERRV(ierr);
		val  = panto_params.k*Na*Nb;
		ierr = MatSetValues(Kmat, 1, &a, 1, &b, &val, ADD_VALUES); CHKERRV(ierr);
	      }
	  }
	ierr = MatAssemblyBegin(Mmat, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyEnd(Mmat,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyBegin(Kmat, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyEnd(Kmat,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = VecAssemblyBegin(Fvec);                     CHKERRV(ierr);
	ierr = VecAssemblyEnd(Fvec);                       CHKERRV(ierr);

	// done
	return;
      }

    // Static initialization
    template<typename OHE_TYPE>
      void Model_MovingMass_local<OHE_TYPE>::StaticInitialize() const
      {
	assert(isDestroyed==false);
	
	// Evaluate M, K and F
	PetscErrorCode ierr;
	Evaluate(0.0, M, K, F);

	// Set dirichlet BCs
	const int nbcs = static_cast<int>(dirichlet_dofs.size());
	if(nbcs!=0)
	  {
	    const std::vector<double> zero(nbcs, 0.0);
	    ierr = MatZeroRows(K, nbcs, &dirichlet_dofs[0], 1., PETSC_NULLPTR, PETSC_NULLPTR); CHKERRV(ierr);
	    ierr = VecSetValues(F, nbcs, &dirichlet_dofs[0], &zero[0], INSERT_VALUES);         CHKERRV(ierr);
	    ierr = VecAssemblyBegin(F); CHKERRV(ierr);
	    ierr = VecAssemblyEnd(F);   CHKERRV(ierr);
	  }
	
	// linear solver
	LinearSolver linSolver;
	linSolver.SetOperator(K);
	linSolver.Solve(F, Ub);

	// set the static solution in the time integrator
	double *uvals;
	ierr = VecGetArray(Ub, &uvals); CHKERRV(ierr);
	std::vector<double> zero(nTotalDof);      // velocity and acceleration
	std::fill(zero.begin(), zero.end(), 0.0);
	TI.SetInitialConditions(0., uvals, zero.data(), zero.data());
	ierr = VecRestoreArray(Ub, &uvals); CHKERRV(ierr);

	// done
	return;
      }

  
    // Set initial conditions
    template<typename OHE_TYPE>
      void Model_MovingMass_local<OHE_TYPE>::SetInitialConditions(const double* uvals,
								  const double* vvals,
								  const double* avals) const
      {
	assert(isDestroyed==false);
	
	TI.SetInitialConditions(0., uvals, vvals, avals);
	return;
      }

    // Advance the solution to the next time step
    template<typename OHE_TYPE>
      void Model_MovingMass_local<OHE_TYPE>::Advance()
      {
	assert(isDestroyed==false);
	
	// evaluate matrices & vectors
	const double time = TI.GetTime()+TI.GetTimestep();
	Evaluate(time, M, K, F);

	// Reset operators in the time integrator, since M & K are time-dependent
	TI.SetOperators(M, K, dirichlet_dofs);
      
	// Advance in time
	TI.Advance(F);

	// done
	return;
      }

    // Visualize
    template<typename OHE_TYPE>
      void Model_MovingMass_local<OHE_TYPE>::Write(const std::vector<std::string> ohe_fnames, const std::string panto_fname) const
      {
	assert(isDestroyed==false);
	
	// Aliases
	const auto& state = TI.GetState();
	const Vec& U = state.U;
	const Vec& V = state.V;
	const Vec& A = state.A;

	// OHE
	double *disp, *vel, *accn;
	PetscErrorCode ierr;
	ierr = VecGetArray(U, &disp);           CHKERRV(ierr);
	ierr = VecGetArray(V, &vel);            CHKERRV(ierr);
	ierr = VecGetArray(A, &accn);           CHKERRV(ierr);
	ohe.Write(disp, vel, accn, ohe_fnames);
	ierr = VecRestoreArray(A, &accn);       CHKERRV(ierr);
	ierr = VecRestoreArray(V, &vel);        CHKERRV(ierr);
	ierr = VecRestoreArray(U, &disp);       CHKERRV(ierr);

	// pantograph position
	const double time = TI.GetTime();
	const double x = panto_params.xinit + panto_params.speed*time;

	// displacement at the pantograph position
	double wval = 0.;
	const auto& dof_shp = ohe.GetActiveDofs(x);
	for(auto& it:dof_shp)
	  {
	    const auto& a  = it.first;
	    const auto& Na = it.second;
	    double wnode;
	    ierr = VecGetValues(U, 1, &a, &wnode); CHKERRV(ierr);
	    wval += wnode*Na;
	  }

	// vertical extent of the pantograph
	const double z0 = panto_params.zbase;
	const double z1 = ohe.GetSpan(0).GetDatum()+wval;

	// write to file
	std::fstream pfile;
	pfile.open(panto_fname, std::ios::out);
	assert(pfile.good());
	pfile << x << " " << z0 << std::endl
	      << x << " " << z1 << std::endl;
	pfile.close();

	// done
	return;
      }  
  }
}
