
/** \file tets_bruni.cpp
 * \brief Tests class OHE_Panto1
 * \author Ramsharan Rangarajan
 */

/* Output: 
   beam*.dat     beam configuration indexed by time step, columns: x w w-dot w-ddot 
   load*.dat     position of the load, indexed by time step
   history.dat   time history of the deflection of the free end of the beam
*/

#include <pfe_linear_Model_OHE_Panto.h>
#include <pfe_GenAlpha_with_Contact.h>
#include <pfe_HHTAlpha_with_Contact.h>
#include <pfe_Newmark_with_Contact.h>
#include <iostream>
#include <fstream>

// simulation parameter
void read_simulation_parameters(const std::string json_file, const std::string tag,
				std::string& beam_file, std::string& ohe_tag,
				std::string& panto_file, std::string& panto_tag,
				int& pantograph_type, std::string& pantograph_inertia,
				std::string& bc_type,
				int& num_spans, int& nelm,
				double& timestep, int& num_time_steps,
				std::string& out_directory);

// time stepping
template<class ModelType>
void Simulate(ModelType model, const int output_dof,
	      const double span, const double speed, const double panto_wt,
	      const std::string out_directory, const int num_time_steps);

// time stepping
template<>
void Simulate<pfe::linear::Model_Cable_Panto*>(pfe::linear::Model_Cable_Panto*  model, const int output_dof,
					       const double span, const double speed, const double panto_wt,
					       const std::string out_directory, const int num_time_steps);


int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRQ(ierr);

  // span parameters
  pfe::ContactWireParams wireParams;
  wireParams.tag     = "ohe";
  wireParams.span    = 60.0;
  wireParams.EI      = 136.0;
  wireParams.Tension = 20000.0;
  wireParams.rhoA    = 1.35;
  wireParams.g       = 0.0;
  wireParams.zLevel  = 0.;

  // Create the beam system
  const int nelm = 800;
  pfe::HeavyTensionedBeam beam(wireParams, nelm);

  // pantograph parameters
  pfe::Pantograph_1_Params panto_params;
  panto_params.tag   = "panto";
  panto_params.xinit = 0.0;
  panto_params.speed = 60.0;
  panto_params.zbase = 0.0;
  panto_params.force = 50.0;
  panto_params.m     = 3.0;
  panto_params.k     = 0.;
  panto_params.c     = 0.;

  // create pantograph
  pfe::Pantograph_1 panto(panto_params);

  // Simply supported ends
  const int nNodes = beam.GetNumNodes();
  const std::vector<int> dirichlet_dofs{0, nNodes-1};
  
  // Time integrator
  const int num_time_steps = 800;
  const double timestep = (wireParams.span/panto_params.speed)/static_cast<double>(num_time_steps);
  const double spectral_radius = 0.5;
  const double alpha = -0.05;
  const int nTotalDof = beam.GetTotalNumDof()+panto.GetTotalNumDof();
  pfe::GenAlpha_with_Contact TI(spectral_radius, nTotalDof, timestep);
  //pfe::HHTAlpha_with_Contact TI(alpha, nTotalDof, timestep);
  //pfe::Newmark_with_Contact TI(nTotalDof, timestep);
  
  // Create model
  pfe::linear::Model_Cable_Panto model(beam, panto, TI, dirichlet_dofs);
  //auto model = new pfe::Cable_Panto(*beam, panto, timestep, alpha_m, alpha_f, dirichlet_dofs);

  // Set initial conditions
  assert(nTotalDof==model.GetTotalNumDof());
  std::vector<double> zero(nTotalDof);
  std::fill(zero.begin(), zero.end(), 0.);
  model.SetInitialConditions(zero.data(), zero.data(), zero.data(), 0.);
  
  // Visualize
  const std::string out_directory = "./";
  //model.Write({out_directory+"/beam-0.dat"}, out_directory+"/load-0.dat");

  // History of the free end
  std::fstream hfile;
  hfile.open(out_directory+"/history.dat", std::ios::out);
  assert(hfile.good());
  hfile << "# time \t Fc" << std::endl;

  // Moving pantograph
  std::cout << "Time step (of " << num_time_steps << ") " << std::endl;
  for(int tstep=0; tstep<num_time_steps; ++tstep)
    {
      std::cout << tstep+1 << std::endl;
      model.Advance();
      //model.Write({out_directory+"/beam-"+std::to_string(tstep+1)+".dat"}, out_directory+"/load-"+std::to_string(tstep+1)+".dat");
      auto& U = model.GetState().U;
      const double time =  model.GetTime();
      hfile << time << "\t" << model.GetContactForce() << std::endl;
    }
  hfile.close();
  
  // Clean up
  TI.Destroy();
  beam.Destroy();
  model.Destroy();
  PetscFinalize();
}
