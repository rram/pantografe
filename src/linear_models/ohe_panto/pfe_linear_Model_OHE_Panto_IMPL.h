
/** \file pfe::Model_OHE_Panto_IMPL.h
 * \brief Implements the templated class pfe::Model_OHE_Panto
 * \author Ramsharan Rangarajan
 */

#pragma once

namespace pfe
{
  namespace linear
  {
    // Constructor
    template<typename OHE_TYPE>
      Model_OHE_Panto<OHE_TYPE>::Model_OHE_Panto(const OHE_TYPE &obj1,
						 const Pantograph& obj2,
						 TimeIntegrator_with_Contact& ti,
						 const std::vector<int> dirichlet)
      :isDestroyed(false),
      ohe(obj1),
      panto(obj2),
      TI(&ti),
      dirichlet_dofs(dirichlet),
      num_ohe_dofs(ohe.GetTotalNumDof()),
      num_panto_dofs(panto.GetTotalNumDof()),
      nTotalDof(ohe.GetTotalNumDof()+panto.GetTotalNumDof()),
      ohe_dofs(num_ohe_dofs),
      panto_dofs(num_panto_dofs),
      ohe_panto_dofs(nTotalDof)
      {
	// sparsity of ohe+panto
	std::vector<int> nz       = ohe.GetSparsity();
	std::vector<int> nz_panto = panto.GetSparsity();
	for(auto& it:nz_panto)
	  nz.push_back(it);
      
	// PETSc data structure
	PetscErrorCode ierr;
	PD.Initialize(nz);
	ierr = VecCreateSeq(PETSC_COMM_WORLD, nTotalDof, &constraintVec);            CHKERRV(ierr);
	ierr = MatSetOption(PD.massMAT,      MAT_KEEP_NONZERO_PATTERN,  PETSC_TRUE); CHKERRV(ierr);
	ierr = MatSetOption(PD.stiffnessMAT, MAT_KEEP_NONZERO_PATTERN,  PETSC_TRUE); CHKERRV(ierr);
	ierr = MatSetOption(PD.massMAT,      MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
	ierr = MatSetOption(PD.stiffnessMAT, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);

	// OHE dof indices
	for(int i=0; i<num_ohe_dofs; ++i)
	  ohe_dofs[i] = i;
      
	// panto dof indices
	for(int i=0; i<num_panto_dofs; ++i)
	  panto_dofs[i] = num_ohe_dofs+i;

	// ohe+panto dofs
	for(int i=0; i<nTotalDof; ++i)
	  ohe_panto_dofs[i] = i;

	// Index sets for OHE and panto dofs
	ierr = ISCreateGeneral(PETSC_COMM_WORLD, num_ohe_dofs,   &ohe_dofs[0],       PETSC_COPY_VALUES, &is_ohe);       CHKERRV(ierr);
	ierr = ISCreateGeneral(PETSC_COMM_WORLD, num_panto_dofs, &panto_dofs[0],     PETSC_COPY_VALUES, &is_panto);     CHKERRV(ierr);
      }

    // Destroy
    template<typename OHE_TYPE>
      void Model_OHE_Panto<OHE_TYPE>::Destroy()
      {
	assert(isDestroyed==false);
	PetscErrorCode ierr;
	PetscBool      isFinalized;
	ierr = PetscFinalized(&isFinalized); CHKERRV(ierr);
	assert(isFinalized==PETSC_FALSE);

	PD.Destroy();
	ierr = VecDestroy(&constraintVec); CHKERRV(ierr);
	ierr = ISDestroy(&is_ohe);         CHKERRV(ierr);
	ierr = ISDestroy(&is_panto);       CHKERRV(ierr);
	isDestroyed = true;
      }
      
    // Destructor
    template<typename OHE_TYPE>
      Model_OHE_Panto<OHE_TYPE>::~Model_OHE_Panto()
      {
	assert(isDestroyed==true);
      }

  
    // Compute the constraint vector at a specified time
    template<typename OHE_TYPE>
      void Model_OHE_Panto<OHE_TYPE>::EvaluateConstraintVector(const double time, Vec& rvec) const
      {
	assert(isDestroyed==false);
	
	// position of the pantograph
	const double x = panto.GetPosition(time);
      
	// active displacement dofs of the ohe
	auto ohe_dof_shp_pairs = ohe.GetActiveDofs(x);

	// interacting dof of the pantograph
	const int dof_panto = num_ohe_dofs+panto.GetCoupledDofNum();
      
	// Constraint vector
	PetscErrorCode ierr;
	ierr = VecZeroEntries(rvec); CHKERRV(ierr);
	for(auto& it:ohe_dof_shp_pairs)
	  {
	    ierr = VecSetValues(rvec, 1, &it.first, &it.second, INSERT_VALUES); CHKERRV(ierr);
	  }
	const double minus_one = -1.;
	ierr = VecSetValues(rvec, 1, &dof_panto, &minus_one, INSERT_VALUES);    CHKERRV(ierr);
	ierr = VecAssemblyBegin(rvec);                                          CHKERRV(ierr);
	ierr = VecAssemblyEnd(rvec);                                            CHKERRV(ierr);

	// done
	return;
      }

     
    // Main functionality: evaluate M, K, F
    template<typename OHE_TYPE>
      void Model_OHE_Panto<OHE_TYPE>::Evaluate(Mat &M, Mat &K, Vec &F) const
      {
	assert(isDestroyed==false);
	PetscErrorCode ierr;

	// Zero
	ierr = MatZeroEntries(M); CHKERRV(ierr);
	ierr = MatZeroEntries(K); CHKERRV(ierr);
	ierr = VecZeroEntries(F); CHKERRV(ierr);
      
	// Evaluate contribution from OHE
	Mat M_ohe, K_ohe;
	Vec F_ohe;
	ierr = MatGetLocalSubMatrix(M, is_ohe, is_ohe, &M_ohe);     CHKERRV(ierr);
	ierr = MatGetLocalSubMatrix(K, is_ohe, is_ohe, &K_ohe);     CHKERRV(ierr);
	ierr = VecGetSubVector(F, is_ohe, &F_ohe);                  CHKERRV(ierr);
	ohe.Evaluate(M_ohe, K_ohe, F_ohe); 
	ierr = VecRestoreSubVector(F, is_ohe, &F_ohe);              CHKERRV(ierr);
	ierr = MatRestoreLocalSubMatrix(K, is_ohe, is_ohe, &K_ohe); CHKERRV(ierr);
	ierr = MatRestoreLocalSubMatrix(M, is_ohe, is_ohe, &M_ohe); CHKERRV(ierr);

	// Evaluate contribution from pantograph
	Mat M_panto, K_panto;
	Vec F_panto;
	ierr = MatGetLocalSubMatrix(M, is_panto, is_panto, &M_panto);     CHKERRV(ierr);
	ierr = MatGetLocalSubMatrix(K, is_panto, is_panto, &K_panto);     CHKERRV(ierr);
	ierr = VecGetSubVector(F, is_panto, &F_panto);                    CHKERRV(ierr); 
	panto.Evaluate(M_panto, K_panto, F_panto); 
	ierr = VecRestoreSubVector(F, is_panto, &F_panto);                CHKERRV(ierr);
	ierr = MatRestoreLocalSubMatrix(K, is_panto, is_panto, &K_panto); CHKERRV(ierr);
	ierr = MatRestoreLocalSubMatrix(M, is_panto, is_panto, &M_panto); CHKERRV(ierr);

	// Finish up assembly of M/K/F
	ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = VecAssemblyBegin(F);                     CHKERRV(ierr);
	ierr = VecAssemblyEnd(F);                       CHKERRV(ierr);

	// done
	return;
      }


    // Set initial conditions
    template<typename OHE_TYPE>
      void Model_OHE_Panto<OHE_TYPE>::SetInitialConditions(const double* uvals, const double* vvals,
							   const double* avals, const double fc) const
      {
	assert(isDestroyed==false);
	
	// Aliases
	auto& M    = PD.massMAT;
	auto& K    = PD.stiffnessMAT;
	auto& F    = PD.resVEC;
	auto& U    = PD.solutionVEC;
	auto& rvec = constraintVec;

	// zero
	PetscErrorCode ierr;
	ierr = MatZeroEntries(M);    CHKERRV(ierr);
	ierr = MatZeroEntries(K);    CHKERRV(ierr);
	ierr = VecZeroEntries(F);    CHKERRV(ierr);
	ierr = VecZeroEntries(U);    CHKERRV(ierr);
	ierr = VecZeroEntries(rvec); CHKERRV(ierr);

	// M/K/F
	Evaluate(M, K, F);

	// Set operators in the time integrator
	TI->SetOperators(M, K, F, dirichlet_dofs);

	// Initial conditions
	TI->SetInitialConditions(0., uvals, vvals, avals, fc);

	// done
	return;
      }
      

    // Main functionality: static solve
    template<typename OHE_TYPE>
      void Model_OHE_Panto<OHE_TYPE>::StaticInitialize() const
      {
	assert(isDestroyed==false);
	
	// Aliases
	auto& M    = PD.massMAT;
	auto& K    = PD.stiffnessMAT;
	auto& F    = PD.resVEC;
	auto& U1   = PD.solutionVEC;
	auto& rvec = constraintVec;

	// zero
	PetscErrorCode ierr;
	ierr = MatZeroEntries(M);    CHKERRV(ierr);
	ierr = MatZeroEntries(K);    CHKERRV(ierr);
	ierr = VecZeroEntries(F);    CHKERRV(ierr);
	ierr = VecZeroEntries(U1);   CHKERRV(ierr);
	ierr = VecZeroEntries(rvec); CHKERRV(ierr);

	// M/K/F
	Evaluate(M, K, F);

	// Set operators in the time integrator
	TI->SetOperators(M, K, F, dirichlet_dofs);
      
	// Evaluate the constraint vector
	EvaluateConstraintVector(0., rvec);
      
	// Solve the system (i) KU = F + fc r & r^T U = 0
	// by solving the pair of systems K U1 = F, K U2 = r
	// and solving for fc using r^T U = 0

	// Set Dirichlet BCs in K
	const int nbcs = static_cast<int>(dirichlet_dofs.size());
	if(nbcs!=0)
	  {
	    ierr = MatZeroRows(K, nbcs, &dirichlet_dofs[0], 1.0, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRV(ierr);
	  }

	// Linear solver
	LinearSolver linSolver;
	linSolver.SetOperator(K);

	// Solve K U1 = F with Dirichlet BCs
	if(nbcs!=0)
	  {
	    const std::vector<double> dirichlet_values(nbcs, 0.);
	    ierr = VecSetValues(F, nbcs, &dirichlet_dofs[0], &dirichlet_values[0], INSERT_VALUES);   CHKERRV(ierr);
	    ierr = VecAssemblyBegin(F);                                                              CHKERRV(ierr);
	    ierr = VecAssemblyEnd(F);                                                                CHKERRV(ierr);
	  }
	linSolver.Solve(F, U1);

	// Solve K U2 = r with Dirichlet BCs
	// Note: not over-writing 'r' so that the constraint r.U = 0 can be correctly evaluated
	ierr = VecZeroEntries(F); CHKERRV(ierr);
	ierr = VecCopy(rvec, F);  CHKERRV(ierr);
	Vec U2;
	ierr = VecDuplicate(U1, &U2); CHKERRV(ierr);
	ierr = VecZeroEntries(U2);    CHKERRV(ierr);
	if(nbcs!=0)
	  {
	    const std::vector<double> dirichlet_values(nbcs, 0.);
	    ierr = VecSetValues(F, nbcs, &dirichlet_dofs[0], &dirichlet_values[0], INSERT_VALUES);   CHKERRV(ierr);
	    ierr = VecAssemblyBegin(F);                                                              CHKERRV(ierr);
	    ierr = VecAssemblyEnd(F);                                                                CHKERRV(ierr);
	  }
	linSolver.Solve(F, U2);
      
	// use r^T(U1 + fc U2) = 0
	double r_dot_U1;
	ierr = VecDot(rvec, U1, &r_dot_U1); CHKERRV(ierr);
	double r_dot_U2;
	ierr = VecDot(rvec, U2, &r_dot_U2); CHKERRV(ierr);
	double fc = -r_dot_U1/r_dot_U2;

	// U = U1 + fc U2
	ierr = VecAXPY(U1, fc, U2); CHKERRV(ierr);

	// initial conditions
	std::vector<double> uvals(nTotalDof), vvals(nTotalDof), avals(nTotalDof);
	ierr = VecGetValues(U1, nTotalDof, &ohe_panto_dofs[0], &uvals[0]); CHKERRV(ierr);
	std::fill(vvals.begin(), vvals.end(), 0.);
	std::fill(avals.begin(), avals.end(), 0.);
	TI->SetInitialConditions(0., &uvals[0], &vvals[0], &avals[0], fc);

	// Clean up
	linSolver.Destroy();
	ierr = VecDestroy(&U2); CHKERRV(ierr);
      
	// done
	return;
      }


    // Advance to the next timestep
    template<typename OHE_TYPE>
      void Model_OHE_Panto<OHE_TYPE>::Advance() const
      {
	assert(isDestroyed==false);
	
	// next time instant
	const double time = TI->GetNextEvaluationTime();
      
	// constraint vector
	auto& rvec = constraintVec;
	EvaluateConstraintVector(time, rvec);

	// advance
	TI->Advance(rvec);

	// done
	return;
      }

    // Access the contact point deflection, velocity and acceleration
    template<typename OHE_TYPE>
      std::array<double,3> Model_OHE_Panto<OHE_TYPE>::GetOHEContactPointKinematics() const
      {
	assert(isDestroyed==false);
	
	// current time
	const double time = TI->GetTime();

	// pantograph position
	const double x = panto.GetPosition(time);

	// state
	const auto& state = TI->GetState();
	const auto& U = state.U;
	const auto& V = state.V;
	const auto& A = state.A;

	// Active dofs
	auto dof_shp_pairs = ohe.GetActiveDofs(x);
      
	// Evaluate position, velocity and acceleration
	double val;
	std::array<double,3> uva{0.,0.,0.};
	for(auto& it:dof_shp_pairs)
	  {
	    const int& a     = it.first;
	    const double& Na = it.second;

	    VecGetValues(U, 1, &a, &val);
	    uva[0] += val*Na;

	    VecGetValues(V, 1, &a, &val);
	    uva[1] += val*Na;

	    VecGetValues(A, 1, &a, &val);
	    uva[2] += val*Na;
	  }

	// done
	return uva;
      }

    // Access the deflection, velocity and acceleration of the pantograph at the contact point
    template<typename OHE_TYPE>
      std::array<double,3> Model_OHE_Panto<OHE_TYPE>::GetPantographContactPointKinematics() const
      {
	assert(isDestroyed==false);
	
	// Pantograph local dof coupled to the OHE
	const int coupled_dof = panto.GetCoupledDofNum();
      
	// indices of pantograph dofs
	const int* indices;
	ISGetIndices(is_panto, &indices);
      
	// Global coupled dof #
	const int indx = indices[coupled_dof];
	ISRestoreIndices(is_panto, &indices);

	// Get values
	std::array<double, 3> uva{0.,0.,0.};
	auto& state = TI->GetState();
	VecGetValues(state.U, 1, &indx, &uva[0]);
	VecGetValues(state.V, 1, &indx, &uva[1]);
	VecGetValues(state.A, 1, &indx, &uva[2]);
      
	return uva;
      }
     
  
     
    // Visualize
    template<typename OHE_TYPE>
      void Model_OHE_Panto<OHE_TYPE>::Write(const std::vector<std::string> ohe_fnames, const std::string panto_fname) const
      {
	// Aliases
	const auto& state = TI->GetState();
	const Vec& U = state.U;
	const Vec& V = state.V;
	const Vec& A = state.A;

	// ohe profiles
	PetscErrorCode ierr;
	Vec U_ohe, V_ohe, A_ohe;
	ierr = VecGetSubVector(U, is_ohe, &U_ohe);     CHKERRV(ierr);
	ierr = VecGetSubVector(V, is_ohe, &V_ohe);     CHKERRV(ierr);
	ierr = VecGetSubVector(A, is_ohe, &A_ohe);     CHKERRV(ierr);
	double *disp, *vel, *accn;
	ierr = VecGetArray(U_ohe, &disp);              CHKERRV(ierr);
	ierr = VecGetArray(V_ohe, &vel);               CHKERRV(ierr);
	ierr = VecGetArray(A_ohe, &accn);              CHKERRV(ierr);
	ohe.Write(disp, vel, accn, ohe_fnames);
	ierr = VecRestoreArray(A_ohe, &accn);          CHKERRV(ierr);
	ierr = VecRestoreArray(V_ohe, &vel);           CHKERRV(ierr);
	ierr = VecRestoreArray(U_ohe, &disp);          CHKERRV(ierr);
	ierr = VecRestoreSubVector(U, is_ohe, &U_ohe); CHKERRV(ierr);
	ierr = VecRestoreSubVector(V, is_ohe, &V_ohe); CHKERRV(ierr);
	ierr = VecRestoreSubVector(A, is_ohe, &A_ohe); CHKERRV(ierr);

	// pantograph position
	const double time = TI->GetTime();
	const double x = panto.GetPosition(time);

	// pantograph displacement at the contact point
	Vec U_panto;
	ierr = VecGetSubVector(U, is_panto, &U_panto);         CHKERRV(ierr);
	const int coupled_indx = panto.GetCoupledDofNum();
	double wval;
	ierr = VecGetValues(U_panto, 1, &coupled_indx, &wval); CHKERRV(ierr);
	ierr = VecRestoreSubVector(U, is_panto, &U_panto);     CHKERRV(ierr);
						       
	// vertical extent of the pantograph
	const double z0 = panto.GetBaseHeight();
	const double z1 = ohe.GetSpan(0).GetDatum() + wval;

	// write to file
	std::fstream pfile;
	pfile.open(panto_fname, std::ios::out);
	assert(pfile.good());
	pfile << x << " " << z0 << std::endl
	      << x << " " << z1 << std::endl;
	pfile.close();


	// done
	return;
      }
  
  }      
}
