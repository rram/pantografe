
/** \file pfe::linear::Model_OHE_Panto.h
 * \brief Defines the class pfe::linear::Model_OHE_Panto
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_Pantograph.h>
#include <pfe_TimeIntegrator_with_Contact.h>
#include <pfe_PetscData.h>
#include <array>

// instantiations
#include <pfe_HeavyTensionedBeam.h>
#include <pfe_linear_SingleSpan_OHE.h>
#include <pfe_linear_MultiSpan_OHE.h>

namespace pfe
{
  namespace linear
  {
    //! \ingroup models
    template<typename OHE_TYPE>
      class Model_OHE_Panto
      {
      public:

	//! Constructor
	Model_OHE_Panto(const OHE_TYPE& obj1,
			const Pantograph& obj2,
			TimeIntegrator_with_Contact& ti, 
			const std::vector<int> dirichlet={});
     
	//! Destructor
	virtual ~Model_OHE_Panto();

	//! Disable copy and assignment
	Model_OHE_Panto(const Model_OHE_Panto<OHE_TYPE>&) = delete;
	Model_OHE_Panto<OHE_TYPE>& operator=(const Model_OHE_Panto<OHE_TYPE>&) = delete;

	//! Destroy
	virtual void Destroy();
	
	//! Access the OHE
	inline const OHE_TYPE& GetOHE() const
	{ return ohe; }

	//! Access the pantograph
	inline const Pantograph& GetPantograph() const
	{ return panto; }

	//! Total number of dofs
	inline int GetTotalNumDof() const
	{ return nTotalDof; }

	//! Set initial conditions
	void SetInitialConditions(const double* uvals, const double* vvals, const double* avals, const double fc) const;
      
	//! Main functionality: static initialization
	void StaticInitialize() const;

	//! Advance to the next timestep
	void Advance() const;

	//! Access the state
	inline const DynamicState& GetState() const
	{ return TI->GetState(); }

	//! Access the contact force
	inline double GetContactForce() const
	{ return TI->GetContactForce(); }

	//! Access the time
	inline double GetTime() const
	{ return TI->GetTime(); }

	//! Access the contact point deflection, velocity and acceleration
	std::array<double,3> GetOHEContactPointKinematics() const;

	//! Access the deflection, velocity and acceleration of the pantograph at the contact point
	std::array<double,3> GetPantographContactPointKinematics() const;
     
	//! Visualize
	void Write(const std::vector<std::string> ohe_fnames, const std::string panto_fname) const;

      private:
	//! Evaluate M, K, F
	void Evaluate(Mat& M, Mat& K, Vec& F) const;
     
	//! Compute the constraint vector at a specified time
	void EvaluateConstraintVector(const double time, Vec& rvec) const;

	bool isDestroyed;
	const OHE_TYPE               &ohe;
	const Pantograph             &panto;
	TimeIntegrator_with_Contact  *TI;
	const int                    nTotalDof;
	const std::vector<int>       dirichlet_dofs;

	const int        num_ohe_dofs;
	const int        num_panto_dofs;
	std::vector<int> ohe_dofs;
	std::vector<int> panto_dofs;
	std::vector<int> ohe_panto_dofs;
	IS is_ohe;
	IS is_panto;

	mutable PetscData      PD;
	mutable Vec            constraintVec;
      };

 
    // Instantiations
    using Model_Cable_Panto         = Model_OHE_Panto<HeavyTensionedBeam>;
    using Model_SingleSpanOHE_Panto = Model_OHE_Panto<linear::SingleSpan_OHE>;
    using Model_MultiSpanOHE_Panto  = Model_OHE_Panto<linear::MultiSpan_OHE>;
 
  }
}


// Implementation
#include <pfe_linear_Model_OHE_Panto_IMPL.h>
