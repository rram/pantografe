
/** \file pfe_nonlinear_TimeIntegrator_impl.h
 * \brief Implements the class pfe::nonlinear::TimeIntegrator
 * \author Ramsharan Rangarajan
 */

#pragma once

namespace pfe
{
  namespace nonlinear
  {
    // Constructor
    template<typename ModelType>
      TimeIntegrator<ModelType>::TimeIntegrator(const double BETA, const double GAMMA, const int ndof, const double delta)
      :isInitialized(false),
      isDestroyed(false),
      nTotalDof(ndof),
      beta(BETA),
      gamma(GAMMA),
      dt(delta),
      nDof(ndof),
      time(0.),
      curr_state(nDof),
      next_state(nDof),
      uvec(nDof),
      dof_indices(nDof) {
      
	for(int i=0; i<nDof; ++i)
	  dof_indices[i] = i;
      }

    // Destructor
    template<typename ModelType>
      TimeIntegrator<ModelType>::~TimeIntegrator()
      {
	assert(isDestroyed==true);
      }

    // Destroy data structures
    template<typename ModelType>
      void TimeIntegrator<ModelType>::Destroy()
      {
	assert(isDestroyed==false);
	curr_state.Destroy();
	next_state.Destroy();
	snes_solver.Destroy();
	isDestroyed = true;
      }

    // residual function
    template<typename ModelType>
      PetscErrorCode TimeIntegrator<ModelType>::Residual_Func(SNES snes, Vec solVec, Vec resVec, void* usr)
      {
	PetscErrorCode ierr;

	// access the context
	SNES_Context *ctx;
	ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
	assert(ctx!=nullptr);
	assert(ctx->prev_state!=nullptr);
	assert(ctx->TI!=nullptr);
	auto& prev_state = *(ctx->prev_state);
	auto& TI         = *(ctx->TI);

	// compute the residual
	TI.ComputeResidue(prev_state, solVec, resVec);

	// done
	return 0;
      }


    // jacobian function
    template<typename ModelType>
      PetscErrorCode TimeIntegrator<ModelType>::Jacobian_Func(SNES snes, Vec solVec, Mat kMat, Mat pMat, void* usr)
      {
	PetscErrorCode ierr;

	// access the context
	SNES_Context *ctx;
	ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
	assert(ctx!=nullptr);
	assert(ctx->prev_state!=nullptr);
	assert(ctx->TI!=nullptr);
	auto& prev_state = *(ctx->prev_state);
	auto& TI         = *(ctx->TI);

	// compute the jacobian
	TI.ComputeJacobian(prev_state, solVec, kMat);

	// done
	return 0;
      }


    
    // Set initial conditions
    template<typename ModelType>
      void TimeIntegrator<ModelType>::Initialize(ModelType& M, const std::vector<int>& nnz, const double t0, 
						 const double* disp, const double* vel, const double* accn)
      {
	assert(isDestroyed==false && isInitialized==false);
	time  = t0;
	model = &M;
	assert(static_cast<int>(nnz.size())==nDof);

	// initialize solver
	snes_solver.Initialize(nnz, Residual_Func, Jacobian_Func);
      
	// Initial conditions
	// Aliases
	Vec& U = curr_state.U;
	Vec& V = curr_state.V;
	Vec& A = curr_state.A;
	std::vector<int> indx(nDof);
	for(int i=0; i<nDof; ++i)
	  indx[i] = i;

	// Copy
	PetscErrorCode ierr;
	ierr = VecSetValues(U, nDof, indx.data(), disp, INSERT_VALUES); CHKERRV(ierr);
	ierr = VecSetValues(V, nDof, indx.data(), vel,  INSERT_VALUES); CHKERRV(ierr);
	ierr = VecSetValues(A, nDof, indx.data(), accn, INSERT_VALUES); CHKERRV(ierr);
	ierr = VecAssemblyBegin(U); CHKERRV(ierr);
	ierr = VecAssemblyBegin(V); CHKERRV(ierr);
	ierr = VecAssemblyBegin(A); CHKERRV(ierr);
	ierr = VecAssemblyEnd(U);   CHKERRV(ierr);
	ierr = VecAssemblyEnd(V);   CHKERRV(ierr);
	ierr = VecAssemblyEnd(A);   CHKERRV(ierr);

	// done
	isInitialized = true;
	return;
      }

     
    // Main functionality
    template<typename ModelType>
      void TimeIntegrator<ModelType>::Advance()
      {
	// Context
	SNES_Context ctx;
	ctx.prev_state = &curr_state;
	ctx.TI         = this;

	// Solve for Un+1, with initial guess = Un
	PetscErrorCode ierr;
	ierr = VecGetValues(curr_state.U, nDof, dof_indices.data(), uvec.data()); CHKERRV(ierr);
	snes_solver.Solve(uvec.data(), &ctx, true);
      
     
	// aliases
	auto& Un = curr_state.U;
	auto& Vn = curr_state.V;
	auto& An = curr_state.A;
	auto& Unp1 = next_state.U;
	auto& Vnp1 = next_state.V;
	auto& Anp1 = next_state.A;

	// Update the state
	// Un+1 <- uvec
	ierr = VecSetValues(Unp1, nDof, dof_indices.data(), uvec.data(), INSERT_VALUES); CHKERRV(ierr);
      
	// An+1 = (Un+1 - Un -dt*Vn -(1/2-beta)*dt^2*An)/(beta*dt^2)
	ierr = VecCopy(Unp1, Anp1);                 CHKERRV(ierr);
	ierr = VecAXPY(Anp1, -1., Un);              CHKERRV(ierr);
	ierr = VecAXPY(Anp1, -dt, Vn);              CHKERRV(ierr);
	ierr = VecAXPY(Anp1, (beta-0.5)*dt*dt, An); CHKERRV(ierr);
	ierr = VecScale(Anp1, 1./(beta*dt*dt));     CHKERRV(ierr);
      
	// Vn+1 = Vn + dt*(1-gamma)*An + dt*gamma*An+1
	ierr = VecCopy(Vn, Vnp1);                CHKERRV(ierr);
	ierr = VecAXPY(Vnp1, dt*(1.-gamma), An); CHKERRV(ierr);
	ierr = VecAXPY(Vnp1, dt*gamma, Anp1);    CHKERRV(ierr);

	// curr_state <- next_state
	ierr = VecCopy(Unp1, Un); CHKERRV(ierr);
	ierr = VecCopy(Vnp1, Vn); CHKERRV(ierr);
	ierr = VecCopy(Anp1, An); CHKERRV(ierr);

	// done
	time += dt;
	return;
      }
	
  }
}
