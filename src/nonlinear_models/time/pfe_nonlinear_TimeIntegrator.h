
/** \file pfe_nonlinear_TimeIntegrator.h
 * \brief Defines the class pfe::nonlinear::TimeIntegrator
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_DynamicState.h>
#include <pfe_SNES_Solver.h>
#include <cassert>

namespace pfe
{
  namespace nonlinear
  {
    template<typename ModelType>
      class TimeIntegrator
      {
      public:
	//! Constructor
	TimeIntegrator(const double BETA, const double GAMMA, const int ndof, const double delta);
      
	//! Destructor
	virtual ~TimeIntegrator();
      	
	//! Disable copy and assignment
	TimeIntegrator(const TimeIntegrator&) = delete;
	TimeIntegrator& operator=(const TimeIntegrator&) = delete;
      
	//! Destroy data structures
	virtual void Destroy();
      
	//! Set initial conditions and initialize data structures
	virtual void Initialize(ModelType& M, const std::vector<int>& nnz, const double t0, 
				const double* disp, const double* vel, const double* accn);

	//! Returns if the integrator has been initialized
	bool IsInitialized() const
	{ return isInitialized; }

	//! Provides access to the model
	inline ModelType& GetModel()
	{ return *model; }
	
	//! Main functionality: advance the time step
	void Advance();
      
	//! Access the solution
	inline DynamicState& GetState()
	{
	  assert(isInitialized==true && isDestroyed==false);
	  return curr_state;
	}

	//! Access # dofs
	inline int GetTotalNumDof() const
	{ return nTotalDof; }
	  
	//! Access the time
	inline double GetTime() const
	{ return time; }

	//! Access the time step
	inline double GetTimestep() const
	{ return dt; }

	//! Compute the residual, to be implemented by derived classes
	virtual void ComputeResidue(const DynamicState& prev_state, Vec& Unp1, Vec& resVec) = 0;

	//! Compute the Jacobian, to be implemented by derived classes
	virtual void ComputeJacobian(const DynamicState& prev_state, Vec& Unp1, Mat& kMat) = 0;
      
      private:
	struct SNES_Context
	{
	  const DynamicState        *prev_state; // previous state
	  TimeIntegrator<ModelType> *TI;         // instance of the time integrator
	};

	// residual function
	static PetscErrorCode Residual_Func(SNES snes, Vec solVec, Vec resVec, void* usr);

	// jacobian function
	static PetscErrorCode Jacobian_Func(SNES snes, Vec solVec, Mat kMat, Mat pMat, void* usr);
	
	bool isInitialized;
	bool isDestroyed;

	const int nTotalDof;
	const double beta;
	const double gamma;

	const double dt;
	const int    nDof;
	double       time;

	ModelType           *model;
	DynamicState        curr_state, next_state;
	SNESSolver          snes_solver;
	std::vector<double> uvec;
	std::vector<int>    dof_indices;
      };
  
  }
}

#include <pfe_nonlinear_TimeIntegrator_impl.h>
