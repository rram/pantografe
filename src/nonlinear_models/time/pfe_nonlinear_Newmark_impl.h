
/** \file pfe_nonlinear_Newmark_impl.h
 * \brief Implements the class pfe::nonlinear::Newmark
 * \author Ramsharan Rangarajan
 */

#pragma once

namespace pfe
{
  namespace nonlinear
  {

    // Set initial conditions and initialize data structures
    template<typename ModelType>
      void Newmark<ModelType>::Initialize(ModelType& M, const std::vector<int>& nnz, const double t0, 
					  const double* disp, const double* vel, const double* accn)
      {
	TimeIntegrator<ModelType>::Initialize(M, nnz, t0, disp, vel, accn);
	PD.Initialize(nnz);
	PetscErrorCode ierr;
	ierr = MatDuplicate(PD.stiffnessMAT, MAT_DO_NOT_COPY_VALUES, &C); CHKERRV(ierr);
	ierr = MatAssemblyBegin(C, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyEnd(C, MAT_FINAL_ASSEMBLY);   CHKERRV(ierr);
	ierr = VecDuplicate(PD.resVEC, &Vnp1); CHKERRV(ierr);
      }

    //! Destroy data structures
    template<typename ModelType>
      void Newmark<ModelType>::Destroy() 
      {
	PD.Destroy();
	PetscErrorCode ierr;
	ierr = MatDestroy(&C);    CHKERRV(ierr);
	ierr = VecDestroy(&Vnp1); CHKERRV(ierr);
	TimeIntegrator<ModelType>::Destroy();
      }

	
    // Compute the residual
    template<typename ModelType>
      void Newmark<ModelType>::ComputeResidue(const DynamicState& prev_state, Vec& Unp1, Vec& resVec)
      {
	assert(this->IsInitialized()==true);
	auto& model = this->GetModel();

	// evaluate at next time instant
	const double dt   = this->GetTimestep();
	const double time = this->GetTime()+dt;
	auto& M = PD.massMAT;
	auto& K = PD.stiffnessMAT;
	model.Evaluate(time, Unp1, M, K, C, resVec);

	// An+1 = (Un+1-Un-dt*Vn-(1/2-beta)*dt^2*An)/(beta*dt^2)
	PetscErrorCode ierr;
	auto& Anp1     = PD.resVEC;
	const auto& Un = prev_state.U;
	const auto& Vn = prev_state.V;
	const auto& An = prev_state.A;
	ierr = VecCopy(Unp1, Anp1);             CHKERRV(ierr);
	ierr = VecAXPY(Anp1, -1., Un);          CHKERRV(ierr);
	ierr = VecAXPY(Anp1, -dt, Vn);          CHKERRV(ierr);
	ierr = VecAXPY(Anp1, -0.25*dt*dt, An);  CHKERRV(ierr);
	ierr = VecScale(Anp1, 1./(0.25*dt*dt)); CHKERRV(ierr);

	// Vn+1 = Vn + dt*(1-gamma)*An + dt*gamma*An+1
	ierr = VecCopy(Vn, Vnp1);           CHKERRV(ierr);
	ierr = VecAXPY(Vnp1, 0.5*dt, An);   CHKERRV(ierr);
	ierr = VecAXPY(Vnp1, 0.5*dt, Anp1); CHKERRV(ierr);
	
	// R = R(Un+1) + C Vn+1 + M An+1
	ierr = MatMultAdd(C, Vnp1, resVec, resVec); CHKERRV(ierr);
	ierr = MatMultAdd(M, Anp1, resVec, resVec); CHKERRV(ierr);
	
	// done
	return;
      }

    
    // Compute the Jacobian
    template<typename ModelType>
      void Newmark<ModelType>::ComputeJacobian(const DynamicState& prev_state, Vec& Unp1, Mat& LHS)
      {
	assert(this->IsInitialized()==true);
	auto& model = this->GetModel();
	
	// evaluate at the next time instant
	const double dt   = this->GetTimestep();
	const double time = this->GetTime()+dt;
	auto& M = PD.massMAT;
	auto& R = PD.resVEC;
	model.Evaluate(time, Unp1, M, LHS, C, R); // LHS = K

	// LHS = Lin(MAn+1 + CVn+1 + R(u)) = 4M/dt^2 + 2C/dt + K
	PetscErrorCode ierr;
	ierr = MatAXPY(LHS, 4./(dt*dt), M, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);
	ierr = MatAXPY(LHS, 2./dt, C, DIFFERENT_NONZERO_PATTERN);      CHKERRV(ierr);
	
	// done
	return;  
      }
	
  }
}
