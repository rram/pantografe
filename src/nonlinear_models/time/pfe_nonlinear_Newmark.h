
/** \file pfe_nonlinear_Newmark.h
 * \brief Defines the class pfe::nonlinear::Newmark
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_nonlinear_TimeIntegrator.h>
#include <pfe_PetscData.h>

namespace pfe
{
  namespace nonlinear
  {
    template<typename ModelType>
      class Newmark: public TimeIntegrator<ModelType>
      {
      public:
	//! Constructor
	inline Newmark(const int ndof, const double delta)
	  :TimeIntegrator<ModelType>(0.25, 0.5, ndof, delta)
	  {}

	//! Destructor
	inline virtual ~Newmark() {}
	  
	//! Disable copy and assignment
	Newmark(const Newmark&) = delete;
	Newmark operator=(const Newmark&) = delete;

	//! Set initial conditions and initialize data structures
	void Initialize(ModelType& M, const std::vector<int>& nnz, const double t0, 
			const double* disp, const double* vel, const double* accn) override;
	
	//! Destroy data structures
	void Destroy() override;
	
	//! residual for computing Un+1
	void ComputeResidue(const DynamicState& prev_state, Vec& Unp1, Vec& resVec) override;

	//! Jacobian for computing Un+1
	void ComputeJacobian(const DynamicState& prev_state, Vec& Unp1, Mat& kMat) override;

      private:
	mutable PetscData PD;
	mutable Mat C;
	mutable Vec Vnp1;
      };
  }
}


#include <pfe_nonlinear_Newmark_impl.h>
