
/** \file test_nonlinear_Newmark.cpp
 * \brief Defines the class pfe::nonlinear::Newmark
 * \author Ramsharan Rangarajan
 */

#include <pfe_nonlinear_Newmark.h>
#include <fstream>
#include <iostream>

// model for a linear system
class Model
{
public:
  void Evaluate(const double time, const Vec& U, Mat& M, Mat& K, Mat& C, Vec& R);
};

void Xsol(const double t, double& x, double& xdot, double& xddot);
void Ysol(const double t, double& x, double& xdot, double& xddot);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Model
  Model model;

  // Time integrator
  double time = 0.;
  const double dt = 0.1;
  pfe::nonlinear::Newmark<Model> TI(2, dt);
  const std::vector<int> nnz{2,2};

  // Initialize
  double x, xdot, xddot;
  double y, ydot, yddot;
  Xsol(0., x, xdot, xddot);
  Ysol(0., y, ydot, yddot);
  const double U0[] = {x, y};
  const double V0[] = {xdot, ydot};
  const double A0[] = {xddot, yddot};
  TI.Initialize(model, nnz, time, U0, V0, A0);

  // Aliases
  auto& state = TI.GetState();
  auto& U = state.U;
  auto& V = state.V;
  auto& A = state.A;

  std::fstream pfile;
  pfile.open("sol.dat", std::ios::out);
  pfile << "#time U0 U1 V0 V1 A0 A1";
  while(time<10.)
    {
      time = TI.GetTime();
      std::cout << "Time: " << time << std::endl;
	  
      // Print state to file
      double *u, *v, *a;
      PetscErrorCode ierr;
      ierr = VecGetArray(U, &u);     CHKERRQ(ierr);
      ierr = VecGetArray(V, &v);     CHKERRQ(ierr);
      ierr = VecGetArray(A, &a);     CHKERRQ(ierr);
      pfile <<"\n"<<time<<" "<<u[0]<<" "<<u[1]<<" "<<v[0]<<" "<<v[1]<<" "<<a[0]<<" "<<a[1];
      ierr = VecRestoreArray(U, &u); CHKERRQ(ierr);
      ierr = VecRestoreArray(V, &v); CHKERRQ(ierr);
      ierr = VecRestoreArray(A, &a); CHKERRQ(ierr);

      // Advance to the next time step
      TI.Advance();
    }
  pfile.close();
  
  // Exact solution
  pfile.open("exsol.dat", std::ios::out);
  pfile << "#time U0 U1 V0 V1 A0 A1";
  const int N = 200;
  for(int i=0; i<=N; ++i)
    {
      double t = 10.*static_cast<int>(i)/static_cast<double>(N);
      double x, xdot, xddot;
      double y, ydot, yddot;
      Xsol(t, x, xdot, xddot);
      Ysol(t, y, ydot, yddot);
      pfile <<"\n"<<t << " "<<x<<" "<<y<<" "<<xdot<<" "<<ydot<<" "<<xddot<<" "<<yddot;
    }
  pfile.close();

  // clean up
  TI.Destroy();
  PetscFinalize();
}

void Xsol(const double t, double& x, double& xdot, double& xddot)
{
  x = (315*std::cos(t/3.))/1496. - (3*std::cos(std::sqrt(2)*t))/17. - (3*std::cos(std::sqrt(5)*t))/88. + 
    (16*std::sin(t/2.))/133. - (std::sqrt(2)*std::sin(std::sqrt(2)*t))/21. + (2*std::sin(std::sqrt(5)*t))/(57.*std::sqrt(5));

  xdot = (8*std::cos(t/2.))/133. - (2*std::cos(std::sqrt(2)*t))/21. + (2*std::cos(std::sqrt(5)*t))/57. - 
    (105*std::sin(t/3.))/1496. + (3*std::sqrt(2)*std::sin(std::sqrt(2)*t))/17. + 
    (3*std::sqrt(5)*std::sin(std::sqrt(5)*t))/88.;

  xddot = (-35*std::cos(t/3.))/1496. + (6*std::cos(std::sqrt(2)*t))/17. + 
    (15*std::cos(std::sqrt(5)*t))/88. - (2*
					 (6*std::sin(t/2.) - 19*std::sqrt(2)*std::sin(std::sqrt(2)*t) + 7*std::sqrt(5)*std::sin(std::sqrt(5)*t)))/
    399.;
  
  return;
}

void Ysol(const double t, double& y, double& ydot, double& yddot)
{
  y = (81*std::cos(t/3.))/748. - (3*std::cos(std::sqrt(2)*t))/17. + (3*std::cos(std::sqrt(5)*t))/44. + 
    (44*std::sin(t/2.))/133. - (std::sqrt(2)*std::sin(std::sqrt(2)*t))/21. - 
    (4*std::sin(std::sqrt(5)*t))/(57.*std::sqrt(5));

  ydot = (22*std::cos(t/2.))/133. - (2*std::cos(std::sqrt(2)*t))/21. - (4*std::cos(std::sqrt(5)*t))/57. - 
    (3*(9*std::sin(t/3.) - 44*std::sqrt(2)*std::sin(std::sqrt(2)*t) + 17*std::sqrt(5)*std::sin(std::sqrt(5)*t)))/
    748.;

  yddot = (-9*std::cos(t/3.))/748. + (6*std::cos(std::sqrt(2)*t))/17. - (15*std::cos(std::sqrt(5)*t))/44. + 
    (-33*std::sin(t/2.) + 38*std::sqrt(2)*std::sin(std::sqrt(2)*t) + 28*std::sqrt(5)*std::sin(std::sqrt(5)*t))/
    399.;

  return;
}
 

// model for a linear system
void Model::Evaluate(const double time, const Vec& U, Mat& M, Mat& K, Mat& C, Vec& R)
{
  const double mass[2][2]  = {{2.,0.},{0.,1.}};
  const double stiff[2][2] = {{6., -2.,},{-2., 4.}};

  // M and K
  PetscErrorCode ierr;
  ierr = MatZeroEntries(M); CHKERRV(ierr);
  ierr = MatZeroEntries(K); CHKERRV(ierr);
  ierr = MatZeroEntries(C); CHKERRV(ierr);
  ierr = VecZeroEntries(R); CHKERRV(ierr);
  for(int i=0; i<2; ++i)
    for(int j=0; j<2; ++j)
      {
	ierr = MatSetValues(M, 1, &i, 1, &j, &mass[i][j], INSERT_VALUES);  CHKERRV(ierr);
	ierr = MatSetValues(K, 1, &i, 1, &j, &stiff[i][j], INSERT_VALUES); CHKERRV(ierr);
      }
  ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
  ierr = MatAssemblyEnd(M, MAT_FINAL_ASSEMBLY);   CHKERRV(ierr);
  ierr = MatAssemblyBegin(K,MAT_FINAL_ASSEMBLY);  CHKERRV(ierr);
  ierr = MatAssemblyEnd(K, MAT_FINAL_ASSEMBLY);   CHKERRV(ierr);
  ierr = MatAssemblyBegin(C,MAT_FINAL_ASSEMBLY);  CHKERRV(ierr);
  ierr = MatAssemblyEnd(C, MAT_FINAL_ASSEMBLY);   CHKERRV(ierr);
  

  // R = KU-F
  double* uvals;
  ierr = VecGetArray(U, &uvals); CHKERRV(ierr);
  double rvec[2] = {0.,0.};
  for(int i=0; i<2; ++i)
    for(int j=0; j<2; ++j)
      rvec[i] += stiff[i][j]*uvals[j];
  rvec[0] -= std::cos(time/3.);
  rvec[1] -= std::sin(time/2.);
  ierr = VecRestoreArray(U, &uvals); CHKERRV(ierr);
  int indx[] = {0,1};
  ierr = VecSetValues(R, 2, indx, rvec, INSERT_VALUES); CHKERRV(ierr);

  // done
  return;
}
