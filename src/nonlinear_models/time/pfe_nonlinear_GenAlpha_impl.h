
/** \file pfe_nonlinear_GenAlpha_impl.h
 * \brief Implements the class pfe::nonlinear::GenAlpha
 * \author Ramsharan Rangarajan
 */

#pragma once

namespace pfe
{
  namespace nonlinear
  {
    // Constructor
    template<typename ModelType>
      GenAlpha<ModelType>::GenAlpha(const double am, const double af, const int ndof, const double delta)
      :TimeIntegrator<ModelType>(0.25*(1.-am+af)*(1.-am+af), // beta
				 0.5-am+af,                  // gamma
				 ndof, delta),
      alpha_m(am), alpha_f(af),
      beta(0.25*(1.-am+af)*(1.-am+af)),
      gamma(0.5-am+af)
	{
	  // checks on ranges of parameters
	  assert(alpha_m<=alpha_f+1.e-6 && alpha_f<=0.5+1.e-6);
	  assert(beta>=0.25+0.5*(alpha_f-alpha_m)-1.e-6);
	}
	
    
    // delegating constructor
    template<typename ModelType>
      GenAlpha<ModelType>::GenAlpha(const double rho, const int ndof, const double delta)
      :GenAlpha<ModelType>((2.*rho-1.)/(rho+1.), // alpha_m
			   rho/(rho+1.),         // alpha_f
			   ndof, delta) {}
      

    // Set initial conditions and initialize data structures
    template<typename ModelType>
      void GenAlpha<ModelType>::
      Initialize(ModelType& M, const std::vector<int>& nnz, const double t0, 
		 const double* disp, const double* vel, const double* accn)
      {
	TimeIntegrator<ModelType>::Initialize(M, nnz, t0, disp, vel, accn);
	PD.Initialize(nnz);
	PetscErrorCode ierr = MatDuplicate(PD.stiffnessMAT, MAT_DO_NOT_COPY_VALUES, &C); CHKERRV(ierr);
	ierr = MatAssemblyBegin(C, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyEnd(C, MAT_FINAL_ASSEMBLY);   CHKERRV(ierr);
	ierr = VecDuplicate(PD.resVEC, &Vnxt);          CHKERRV(ierr);
      }

    // Destroy data structures
    template<typename ModelType>
      void GenAlpha<ModelType>::Destroy()
      {
	PD.Destroy();
	PetscErrorCode ierr;
	ierr = MatDestroy(&C);    CHKERRV(ierr);
	ierr = VecDestroy(&Vnxt); CHKERRV(ierr);
	TimeIntegrator<ModelType>::Destroy();
      }
	
    // residual
    template<typename ModelType>
      void GenAlpha<ModelType>::ComputeResidue(const DynamicState& prev_state, Vec& Unp1, Vec& resVec)
      {
	assert(this->IsInitialized()==true);
	auto& model = this->GetModel();

	// aliases
	auto& Un   = prev_state.U;
	auto& Vn   = prev_state.V;
	auto& An   = prev_state.A;
	auto& Unxt = PD.solutionVEC;
	auto& Anxt = PD.resVEC;
	
	// next evaluation instant
	const double tn   = this->GetTime();
	const double dt   = this->GetTimestep();
	const double tnp1 = tn+dt;
	const double tnxt = (1.-alpha_f)*tnp1 + alpha_f*tn;

	// static residual at the next evaluation instant
	// U(n+1-alpha_f) = (1-alpha_f) Un+1 + alpha_f Un
	PetscErrorCode ierr;
	ierr = VecCopy(Unp1, Unxt);               CHKERRV(ierr);
	ierr = VecScale(Unxt, 1.-alpha_f);        CHKERRV(ierr);
	ierr = VecAXPY(Unxt, alpha_f, Un);        CHKERRV(ierr);
	model.Evaluate(tnxt, Unxt, PD.massMAT, PD.stiffnessMAT, C, resVec); 
	
	// An+1 = (Un+1-Un-dt*Vn-(1/2-beta)*dt^2*An)/(beta*dt^2)
	ierr = VecCopy(Unp1, Anxt);                 CHKERRV(ierr);
	ierr = VecAXPY(Anxt, -1., Un);              CHKERRV(ierr);
	ierr = VecAXPY(Anxt, -dt, Vn);              CHKERRV(ierr);
	ierr = VecAXPY(Anxt, (beta-0.5)*dt*dt, An); CHKERRV(ierr);
	ierr = VecScale(Anxt, 1./(beta*dt*dt));     CHKERRV(ierr);

	// Vn+1 = Vn + dt((1-gamma) An  + gamma An+1)
	ierr = VecCopy(Vn, Vnxt);                  CHKERRV(ierr);
	ierr = VecAXPY(Vnxt, dt*(1.-gamma), An);   CHKERRV(ierr);
	ierr = VecAXPY(Vnxt, dt*gamma,      Anxt); CHKERRV(ierr);
	
	// A(n+1-a_m) = (1-alpha_m) An+1 + alpha_m An
	ierr = VecScale(Anxt, 1.-alpha_m); CHKERRV(ierr);
	ierr = VecAXPY(Anxt, alpha_m, An); CHKERRV(ierr);

	// V(n+1-a_f) = (1-a_f) Vn+1 + a_f V_n
	ierr = VecScale(Vnxt, 1.-alpha_f); CHKERRV(ierr);
	ierr = VecAXPY(Vnxt, alpha_f, Vn); CHKERRV(ierr);
	
	// R = R(Unxt) + M A(n+1-a_m) + C V(n+1-a_f)
	ierr = MatMultAdd(PD.massMAT, Anxt, resVec, resVec); CHKERRV(ierr);
	ierr = MatMultAdd(C, Vnxt, resVec, resVec);          CHKERRV(ierr);

	// done
	return;
      }


    template<typename ModelType>
      void GenAlpha<ModelType>::ComputeJacobian(const DynamicState& prev_state, Vec& Unp1, Mat& LHS)
      {
	assert(this->IsInitialized()==true);
	auto& model = this->GetModel();

	// aliases
	auto& Un   = prev_state.U;
	auto& Vn   = prev_state.V;
	auto& An   = prev_state.A;
	auto& Unxt = PD.solutionVEC;
	auto& M    = PD.massMAT;
	
	// next evaluation instant
	const double tn   = this->GetTime();
	const double dt   = this->GetTimestep();
	const double tnp1 = tn+dt;
	const double tnxt = (1.-alpha_f)*tnp1 + alpha_f*tn;

	// U(n+1-alpha_f) = (1-alpha_f)*Un+1 + alpha_f Un
	PetscErrorCode ierr; 
	ierr = VecCopy(Unp1, Unxt);        CHKERRV(ierr);
	ierr = VecScale(Unxt, 1.-alpha_f); CHKERRV(ierr);
	ierr = VecAXPY(Unxt, alpha_f, Un); CHKERRV(ierr);

	// mass, stiffness, damping and residual
	model.Evaluate(tnxt, Unxt, M, LHS, C, PD.resVEC);

	// LHS = (1-alpha_f)K + (1-alpha_m)/(beta dt^2) M + gamma(1-a_f)/(beta dt) C
	ierr = MatScale(LHS, 1.-alpha_f); CHKERRV(ierr);
	ierr = MatAXPY(LHS, (1.-alpha_m)/(beta*dt*dt), M, DIFFERENT_NONZERO_PATTERN);    CHKERRV(ierr);
	ierr = MatAXPY(LHS, (1.-alpha_f)*gamma/(beta*dt), C, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);

	// done
	return;
      }

     
  }
}
