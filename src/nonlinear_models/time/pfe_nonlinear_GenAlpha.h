
/** \file pfe_nonlinear_GenAlpha.h
 * \brief Defines the class pfe::nonlinear::GenAlpha
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_nonlinear_TimeIntegrator.h>
#include <pfe_PetscData.h>
#include <pfe_SNES_Solver.h>

namespace pfe
{
  namespace nonlinear
  {
    template<typename ModelType>
      class GenAlpha: public TimeIntegrator<ModelType>
      {
      public:
	//! Constructor
	GenAlpha(const double am, const double af, const int ndof, const double delta);

	//! Constructor
	GenAlpha(const double r, const int ndof, const double delta);

	//! Disable copy and assignment
	GenAlpha(const GenAlpha&) = delete;
	GenAlpha operator=(const GenAlpha&) = delete;

	//! Set initial conditions and initialize data structures
	void Initialize(ModelType& M, const std::vector<int>& nnz, const double t0, 
			const double* disp, const double* vel, const double* accn) override;

	//! Destroy data structures
	void Destroy() override;
	
	//! residual for computing Un+1
	void ComputeResidue(const DynamicState& prev_state, Vec& Unp1, Vec& resVec) override;

	//! Jacobian for computing Un+1
	void ComputeJacobian(const DynamicState& prev_state, Vec& Unp1, Mat& kMat) override;

      private:
	const double alpha_m, alpha_f;
	const double beta, gamma;
	mutable PetscData PD;
	mutable Mat C;
	mutable Vec Vnxt;
      };
  }
}


// implementation
#include <pfe_nonlinear_GenAlpha_impl.h>
