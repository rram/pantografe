
/** \file pfe_nonlinear_Model_MovingLoad.h
 * \brief Defines the class pfe::nonlinear::Model_MovingLoad
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_PantographParams.h>
#include <pfe_nonlinear_TimeIntegrator.h>
#include <pfe_PetscData.h>
#include <pfe_OHEDampingParams.h>

// instantiations
#include <pfe_nonlinear_SingleSpan_OHE.h>
#include <pfe_nonlinear_MultiSpan_OHE.h>

namespace pfe
{
  namespace nonlinear
  {
    //! \ingroup models
    template<typename OHE_TYPE>
      class Model_MovingLoad
      {
      public:
     
	//! Constructor
	Model_MovingLoad(const OHE_TYPE& obj1,
			 const OHEDampingParams& damping,
			 const Pantograph_0_Params& obj2,
			 nonlinear::TimeIntegrator<Model_MovingLoad<OHE_TYPE>>& ti);
      
	//! Destructor
	virtual ~Model_MovingLoad();

	//! Destroy
	void Destroy();
	
	//! Disable copy and assignment
	Model_MovingLoad(const Model_MovingLoad<OHE_TYPE>&) = delete;
	Model_MovingLoad<OHE_TYPE>& operator=(const Model_MovingLoad<OHE_TYPE>&) = delete;

	//! Access the ohe system
	inline const OHE_TYPE& GetOHE() const
	{ return ohe; }

	//! Total number of dofs
	inline int GetTotalNumDof() const
	{ return nTotalDof; }

	//! Get the pantograph parameters
	inline Pantograph_0_Params GetPantographParams() const
	{ return panto_params; }
      
	//! Set the pantograph parameters
	inline void SetPantographParams(const Pantograph_0_Params& params)
	{ panto_params = params; }
      
	//! Main functionality: static solve
	void StaticInitialize();
	
	//! evaluate mass, stiffness and residual
	void Evaluate(const double time, const Vec& U, Mat& M, Mat& K, Vec& R) const;
	void Evaluate(const double time, const Vec& U, Mat& M, Mat& K, Mat& C, Vec& R) const;
	
	//! Advance the state to the next time step
	inline void Advance()
	{ TI.Advance(); }

	//! Access the state
	inline const DynamicState& GetState() const
	{ return TI.GetState(); }

	//! Access the time
	inline double GetTime() const
	{ return TI.GetTime(); }

	//! Access the load point deflection, velocity and acceleration
	std::array<double,3> GetLoadPointKinematics() const;
     
	//! Visualize
	void Write(const std::vector<std::string> ohe_fnames, const std::string panto_fname) const;
      
      private:
	struct StaticInitialize_SNES_Context
	{
	  Model_MovingLoad *model;
	  PetscData        *PD;
	  double           time;
	};
	
	static PetscErrorCode StaticInitialize_Residue_Func(SNES snes,  Vec solVec, Vec resVec, void* usr);
	static PetscErrorCode StaticInitialize_Jacobian_Func(SNES snes, Vec solVec, Mat kMat, Mat pMat, void* usr);
	  
	const OHE_TYPE                                        &ohe;
	const OHEDampingParams                                ohe_damping_params;
	Pantograph_0_Params                                   panto_params;
	nonlinear::TimeIntegrator<Model_MovingLoad<OHE_TYPE>> &TI;
	const int                                             nTotalDof;
	bool isInitialized;
	bool isDestroyed;
	Mat dampingMat;
      };

    // Instantiations
    using Model_SingleSpanOHE_MovingLoad  = Model_MovingLoad<SingleSpan_OHE>;
    using Model_MultiSpanOHE_MovingLoad   = Model_MovingLoad<MultiSpan_OHE>;
  }
}

// Implementation
#include <pfe_nonlinear_Model_MovingLoad_IMPL.h>

