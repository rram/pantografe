
/** \file test_nonlinear_ms_Model_MovingLoad.cpp
 * \brief Test for the class pfe::nonlinear::Model_MovingLoad<MultiSpan_OHE>
 * \author Ramsharan Rangarajan, Suman Dutta
 */

#include <pfe_nonlinear_Model_MovingLoad.h>
#include <pfe_nonlinear_Newmark.h>
#include <pfe_nonlinear_GenAlpha.h>
#include <iostream>
#include <fstream>
#include <filesystem>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRQ(ierr);

  // Benchmark study parameters
  pfe::OHESpan_ParameterPack ohe_pack;
  std::fstream jfile;
  assert(std::filesystem::exists("benchmark_params.json"));
  jfile.open("benchmark_params.json", std::ios::in);
  assert(jfile.is_open());
  jfile >> ohe_pack;

  // Constant for pantograph parameters
  pfe::Pantograph_0_Params panto_params;
  panto_params.tag = "pantograph 0";
  jfile >> panto_params;

  // OHE damping parameters
  pfe::OHEDampingParams ohe_damping{.tag="ohe damping"};
  jfile >> ohe_damping;
  
  jfile.close();
  panto_params.xinit = 6.0;
  panto_params.force = 169.;
  
  using OHEType    = pfe::nonlinear::MultiSpan_OHE;
  using MODEL_TYPE = pfe::nonlinear::Model_MovingLoad<OHEType>;
  
  // Create the OHE 
  const int nElements = 10;
  const int num_spans = 3;
  OHEType ohe(0., ohe_pack, num_spans, nElements, 1.e5);

  // Create time integrator
  const double timestep = 0.002;
  //pfe::nonlinear::Newmark<MODEL_TYPE> TI(ohe->GetTotalNumDof(), timestep);
  pfe::nonlinear::GenAlpha<MODEL_TYPE> TI(0.5, ohe.GetTotalNumDof(), timestep);

  // Create an instance of the model
  MODEL_TYPE model(ohe, ohe_damping, panto_params, TI);
  const int nTotalDof = model.GetTotalNumDof();
    
  // Static solution
  model.StaticInitialize();

  // visualize
  model.Write({"catenary-0.dat","contact-0.dat","dropper-0.dat"}, "load-0.dat");

  std::fstream hfile;
  hfile.open("history.dat", std::ios::out);
  assert(hfile.good());
  hfile << "# time \t u \t v \t a "<< std::endl;
  std::array<double,3> ohe_uva   = model.GetLoadPointKinematics();
  hfile << 0.0 << "\t" << ohe_uva[0] << "\t" << ohe_uva[1] << "\t" << ohe_uva[2] << "\t" << std::endl;
    	

  // Moving pantograph
  for(int tstep=0; tstep<894; ++tstep)
    {
      std::cout << "Time step " << tstep << std::endl;
      model.Advance();

      /*std::string tstr = std::to_string(tstep+1)+".dat";
      model.Write({"catenary-"+tstr, "contact-"+tstr, "dropper-"+tstr}, "load-"+tstr);*/
      
      ohe_uva   = model.GetLoadPointKinematics();
      hfile << model.GetTime() << "\t" << ohe_uva[0] << "\t" << ohe_uva[1] << "\t" << ohe_uva[2] << "\t" << std::endl;
    }
  
  hfile.close();
      
  // Clean up
  TI.Destroy();
  model.Destroy();
  ohe.Destroy();
  PetscFinalize();
}
