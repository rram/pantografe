
# add library
add_library(pfe_nonlinear_model_moving_load INTERFACE)

# headers
target_include_directories(pfe_nonlinear_model_moving_load INTERFACE
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<INSTALL_INTERFACE:include>)

# link
target_link_libraries(pfe_nonlinear_model_moving_load INTERFACE pfe_ohe_damping pfe_nonlinear_time pfe_component pfe_core)

# Add required flags
target_compile_features(pfe_nonlinear_model_moving_load INTERFACE ${pfe_COMPILE_FEATURES})

install(FILES
	pfe_nonlinear_Model_MovingLoad.h
	pfe_nonlinear_Model_MovingLoad_IMPL.h
	DESTINATION ${PROJECT_NAME}/include)

