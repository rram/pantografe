
/** \file pfe_nonlinear_Model_MovingLoad_IMPL.h
 * \brief Implements the templated class pfe::nonlinear::Model_MovingLoad
 * \author Ramsharan Rangarajan
 */

#pragma once

namespace pfe
{
  namespace nonlinear
  {
    // Constructor
    template<typename OHE_TYPE>
      Model_MovingLoad<OHE_TYPE>::Model_MovingLoad(const OHE_TYPE &obj1,
						   const OHEDampingParams& damping,
						   const Pantograph_0_Params& obj2,
						   nonlinear::TimeIntegrator<Model_MovingLoad<OHE_TYPE>> &ti)
      :ohe(obj1),
      ohe_damping_params(damping),
      panto_params(obj2),
      TI(ti),
      nTotalDof(ohe.GetTotalNumDof()),
      isInitialized(false),
      isDestroyed(false)
	{
	  assert(nTotalDof==TI.GetTotalNumDof());
	}
	  

    // Destructor
    template<typename OHE_TYPE>
      Model_MovingLoad<OHE_TYPE>::~Model_MovingLoad()
      {
	assert(isDestroyed==true);
      }


    // Destroy
    template<typename OHE_TYPE>
      void Model_MovingLoad<OHE_TYPE>::Destroy()
      {
	assert(isDestroyed==false);
	PetscErrorCode ierr = MatDestroy(&dampingMat); CHKERRV(ierr);
	isDestroyed = true;
      }
      
    // Main functionlity: evaluate mass/stiffness matrices, and the residual vector
    template<typename OHE_TYPE>
      void Model_MovingLoad<OHE_TYPE>::Evaluate(const double time, const Vec& U, Mat &M, Mat &K, Vec &R) const
      {
	// zero
	PetscErrorCode ierr;
	ierr = MatZeroEntries(M); CHKERRV(ierr);
	ierr = MatZeroEntries(K); CHKERRV(ierr);
	ierr = VecZeroEntries(R); CHKERRV(ierr);
	
	// Evaluate contribution from OHE
	ohe.Evaluate(U, M, K, R);

	// add contribution from the pantograph
	
	// position of the panto
	const double x = panto_params.xinit + panto_params.speed*time;
      
	// interacting dofs and shape function values
	auto dof_shp_pairs = ohe.GetActiveDofs(x);

	// Add contribution from the moving force
	const double& F0 = panto_params.force;
	for(auto& it:dof_shp_pairs)
	  {
	    const auto& row = it.first;
	    const auto& Na  = it.second;
	    const double val = -F0*Na;
	    ierr = VecSetValues(R, 1, &row, &val, ADD_VALUES); CHKERRV(ierr);
	  }      
	ierr = VecAssemblyBegin(R); CHKERRV(ierr);
	ierr = VecAssemblyEnd(R);   CHKERRV(ierr);

	// done
	return;
      }

    
    // Main functionlity: evaluate mass/stiffness/damping matrices, and the residual vector
    template<typename OHE_TYPE>
      void Model_MovingLoad<OHE_TYPE>::
      Evaluate(const double time, const Vec& U, Mat& M, Mat& K, Mat& C, Vec& R) const
    {
      // Evaluate M,K,R
      Evaluate(time, U, M, K, R);

      // Copy C <- dampingMat
      PetscErrorCode ierr;
      ierr = MatZeroEntries(C); CHKERRV(ierr);
      ierr = MatCopy(dampingMat, C, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);
      return;
    }

    
    template<typename OHE_TYPE>
      PetscErrorCode Model_MovingLoad<OHE_TYPE>::StaticInitialize_Residue_Func(SNES snes,  Vec solVec, Vec resVec, void* usr)
    {
      PetscErrorCode ierr;
      StaticInitialize_SNES_Context *ctx;
      ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
      assert(ctx!=nullptr);
      assert(ctx->model!=nullptr && ctx->PD!=nullptr);
      ctx->model->Evaluate(ctx->time, solVec, ctx->PD->massMAT, ctx->PD->stiffnessMAT, resVec);
      return 0;
    }

    template<typename OHE_TYPE>
    PetscErrorCode Model_MovingLoad<OHE_TYPE>::StaticInitialize_Jacobian_Func(SNES snes, Vec solVec, Mat kMat, Mat pMat, void* usr)
    {
      PetscErrorCode ierr;
      StaticInitialize_SNES_Context *ctx;
      ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
      assert(ctx!=nullptr);
      assert(ctx->model!=nullptr && ctx->PD!=nullptr);
      ctx->model->Evaluate(ctx->time, solVec, ctx->PD->massMAT, kMat, ctx->PD->resVEC); 
      return 0;
    }
					     
      
    // Main functionality: static solve
    template<typename OHE_TYPE>
      void Model_MovingLoad<OHE_TYPE>::StaticInitialize()
      {
	assert(isInitialized==false && isDestroyed==false);
	
	// Petsc data structures
	const std::vector<int> nnz = ohe.GetSparsity();
	PetscData PD;
	PD.Initialize(nnz);

	// Solver
	SNESSolver snes_solver;
	snes_solver.Initialize(nnz, StaticInitialize_Residue_Func, StaticInitialize_Jacobian_Func);

	// Solve
	StaticInitialize_SNES_Context ctx;
	ctx.model = this;
	ctx.PD    = &PD;
	ctx.time  = 0.;
	std::vector<double> disp(nTotalDof);
	std::fill(disp.begin(), disp.end(), 0.);
	snes_solver.Solve(disp.data(), &ctx, true);

	// initialize the time integrator
	std::vector<double> zero(nTotalDof); // velocity and acceleration
	std::fill(zero.begin(), zero.end(), 0.);
	TI.Initialize(*this, nnz, 0.0, disp.data(), zero.data(), zero.data());

	// assemble M and K at the static deflected state
	PetscErrorCode ierr;
	ierr = MatZeroEntries(PD.massMAT);      CHKERRV(ierr);
	ierr = MatZeroEntries(PD.stiffnessMAT); CHKERRV(ierr);
	ierr = VecZeroEntries(PD.resVEC);       CHKERRV(ierr);
	Evaluate(0., TI.GetState().U, PD.massMAT, PD.stiffnessMAT, PD.resVEC); // load of the pantograph -> res vec, only and does not matter

	// damping matrix: alpha*M + beta*K
	ierr = MatDuplicate(PD.stiffnessMAT, MAT_COPY_VALUES, &dampingMat); CHKERRV(ierr);
	ierr = MatScale(dampingMat, ohe_damping_params.beta);               CHKERRV(ierr);
	ierr = MatAXPY(dampingMat, ohe_damping_params.alpha, PD.massMAT, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);
		
	// done
	snes_solver.Destroy();
	PD.Destroy();
	isInitialized = true;
	return;
      }


    // Access the load point deflection, velocity and acceleration
    template<typename OHE_TYPE>
      std::array<double,3> Model_MovingLoad<OHE_TYPE>::GetLoadPointKinematics() const
      {
	assert(isInitialized==true && isDestroyed==false);
	
	// current time
	const double time = TI.GetTime();

	// pantograph position
	const double x = panto_params.xinit + panto_params.speed*time;
      
	// interacting dofs and shape function values
	auto dof_shp_pairs = ohe.GetActiveDofs(x);
      
	// state
	const auto& state = TI.GetState();
	const auto& U = state.U;
	const auto& V = state.V;
	const auto& A = state.A;

	// Evaluate position, velocity and acceleration
	double val;
	std::array<double,3> uva{0.,0.,0.};
	for(auto& it:dof_shp_pairs)
	  {
	    const int& a     = it.first;
	    const double& Na = it.second;

	    VecGetValues(U, 1, &a, &val);
	    uva[0] += val*Na;

	    VecGetValues(V, 1, &a, &val);
	    uva[1] += val*Na;

	    VecGetValues(A, 1, &a, &val);
	    uva[2] += val*Na;
	  }

	// done
	return uva;
      }
      
  
    // Visualize
    template<typename OHE_TYPE>
      void Model_MovingLoad<OHE_TYPE>::Write(const std::vector<std::string> ohe_fnames, const std::string panto_fname) const
      {
	assert(isInitialized==true && isDestroyed==false);
	
	// Aliases
	const auto& state = TI.GetState();
	const Vec& U = state.U;
	const Vec& V = state.V;
	const Vec& A = state.A;

	// ohe profiles
	double *disp, *vel, *accn;
	PetscErrorCode ierr;
	ierr = VecGetArray(U, &disp);              CHKERRV(ierr);
	ierr = VecGetArray(V, &vel);               CHKERRV(ierr);
	ierr = VecGetArray(A, &accn);              CHKERRV(ierr); 
	ohe.Write(disp, vel, accn, ohe_fnames); 
	ierr = VecRestoreArray(U, &disp);          CHKERRV(ierr);
	ierr = VecRestoreArray(V, &vel);           CHKERRV(ierr);
	ierr = VecRestoreArray(A, &accn);          CHKERRV(ierr);

	// pantograph position
	const double time = TI.GetTime();
	const double x = panto_params.xinit + panto_params.speed*time;

	// Contact wire displacement here
	double wval = 0.;
	auto dof_shp_pairs = ohe.GetActiveDofs(x);
	for(auto& it:dof_shp_pairs)
	  {
	    auto& dof = it.first;
	    auto& shpval = it.second;
	    double wnode;
	    ierr = VecGetValues(U, 1, &dof, &wnode); CHKERRV(ierr);
	    wval += wnode*shpval;
	  }

	// vertical extent of the pantograph
	const double z0 = panto_params.zbase;
	const double z1 = ohe.GetSpan(0).GetDatum() + wval;

	// write to file
	std::fstream pfile;
	pfile.open(panto_fname, std::ios::out);
	assert(pfile.good());
	pfile << x << " " << z0 << std::endl
	      << x << " " << z1 << std::endl;
	pfile.close();

	// done
	return;
      }
  }
}
