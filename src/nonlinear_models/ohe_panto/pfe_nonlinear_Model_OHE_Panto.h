
/** \file pfe::nonlinear::Model_OHE_Panto.h
 * \brief Defines the class pfe::nonlinear::Model_OHE_Panto
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_Pantograph.h>
#include <pfe_nonlinear_TimeIntegrator.h>
#include <pfe_ContactSpring.h>
#include <pfe_OHEDampingParams.h>

// instantiations
#include <pfe_nonlinear_SingleSpan_OHE.h>
#include <pfe_nonlinear_MultiSpan_OHE.h>

namespace pfe
{
  namespace nonlinear
  {
    template<typename OHE_TYPE>
      class Model_OHE_Panto
      {
      public:
	//! Constructor
	Model_OHE_Panto(const OHE_TYPE& obj1,
			const OHEDampingParams& damping,
			const Pantograph& obj2,
			const ContactSpring& obj3,
			TimeIntegrator<Model_OHE_Panto<OHE_TYPE>>& ti);

	//! Destructor
	virtual ~Model_OHE_Panto();
	
	//! Disable copy and assignment
	Model_OHE_Panto(const Model_OHE_Panto&) = delete;
	Model_OHE_Panto operator=(const Model_OHE_Panto&) = delete;

	//! Destroy instance
	void Destroy();
	  
	//! Access the ohe
	inline const OHE_TYPE& GetOHE() const
	{ return ohe; }

	//! Access the pantograph
	inline const Pantograph& GetPantograph() const
	{ return panto; }

	//! Total #dofs
	inline int GetTotalNumDof() const
	{ return nTotalDof; }

	//! Evaluate
	void Evaluate(const double time, const Vec& U, Mat& M, Mat& K, Vec& R) const;
	void Evaluate(const double time, const Vec& U, Mat& M, Mat& K, Mat& C, Vec& R) const;

	//! evaluate the contact constraint at the current instant
	double GetContactGap() const;
	
	//! Static initialization
	void StaticInitialize();

	//! Advance to the next time step
	void Advance();

	//! Access the state
	inline const DynamicState& GetState() const
	{ return TI.GetState(); }

	//! Current time
	inline double GetTime() const
	{ return TI.GetTime(); }

	//! Access the contact force
	double GetContactForce() const;
	  
	//! Access the contact point deflection, velocity and acceleration
	std::array<double,3> GetOHEContactPointKinematics() const;

	//! Access the deflection, velocity and acceleration of the pantograph contact point
	std::array<double,3> GetPantographContactPointKinematics() const;

	//! Visualize
	void Write(const std::vector<std::string> ohe_fnames, const std::string panto_fname) const;

      private:

	struct SNES_Context
	{
	  const Model_OHE_Panto* model;
	  PetscData *PD;
	};

	static PetscErrorCode Static_Residual_Func(SNES snes, Vec solVec, Vec resVec, void *usr);
	static PetscErrorCode Static_Jacobian_Func(SNES snes, Vec solVec, Mat kMat, Mat pMat, void* usr);
													
	
	//! Compute the constraint at a given time
	std::vector<std::pair<int,double>> GetContactConstraint(const double time) const;

	//! Compute the damping matrix
	//! Should be called after the time integrator has been initialized
	void ComputeDampingMatrix(PetscData& PD);
	
	bool isInitialized;
	bool isDestroyed;
	const OHE_TYPE    &ohe;
	const OHEDampingParams ohe_damping_params;
	const Pantograph  &panto;
	TimeIntegrator<Model_OHE_Panto<OHE_TYPE>> &TI;
	const ContactSpring &contact_spring;
	Mat dampingMat;

	const int num_ohe_dofs;
	const int num_panto_dofs;
	const int nTotalDof;
	
	std::vector<int> ohe_dofs;
	std::vector<int> panto_dofs;
	IS is_ohe;
	IS is_panto;
      };

    // instantiations
    using Model_SingleSpan_OHE_Panto = Model_OHE_Panto<SingleSpan_OHE>;
    using Model_MultiSpan_OHE_Panto  = Model_OHE_Panto<MultiSpan_OHE>;
  }
}

// implementation
#include <pfe_nonlinear_Model_OHE_Panto_impl.h>
