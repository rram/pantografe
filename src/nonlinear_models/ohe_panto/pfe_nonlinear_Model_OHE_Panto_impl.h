
/** \file pfe::Model_OHE_Panto_IMPL.h
 * \brief Implements the templated class pfe::Model_OHE_Panto
 * \author Ramsharan Rangarajan
 */


#pragma once

namespace pfe
{
  namespace nonlinear
  {
    // Constructor
    template<typename OHE_TYPE>
      Model_OHE_Panto<OHE_TYPE>::Model_OHE_Panto(const OHE_TYPE& obj1,
						 const OHEDampingParams& damping,
						 const Pantograph& obj2,
						 const ContactSpring& obj3,
						 TimeIntegrator<Model_OHE_Panto<OHE_TYPE>>& ti)
      :isInitialized(false), isDestroyed(false),
      ohe(obj1), ohe_damping_params(damping),
      panto(obj2), contact_spring(obj3), TI(ti),
      num_ohe_dofs(ohe.GetTotalNumDof()),
      num_panto_dofs(panto.GetTotalNumDof()),
      nTotalDof(num_ohe_dofs+num_panto_dofs),
      ohe_dofs(num_ohe_dofs),
      panto_dofs(num_panto_dofs)
      {
	// sparsity of ohe+panto
	std::vector<int> nz       = ohe.GetSparsity();
	std::vector<int> nz_panto = panto.GetSparsity();
	for(auto& it:nz_panto)
	  nz.push_back(it);

	// PETSc data structure
	PetscErrorCode ierr;

	// OHE dof indices
	for(int i=0; i<num_ohe_dofs; ++i)
	  ohe_dofs[i] = i;

	// panto dof indices
	for(int i=0; i<num_panto_dofs; ++i)
	  panto_dofs[i] = num_ohe_dofs+i;

	// Index sets for OHE and panto dofs
	ierr = ISCreateGeneral(PETSC_COMM_WORLD, num_ohe_dofs,   &ohe_dofs[0],   PETSC_COPY_VALUES, &is_ohe);   CHKERRV(ierr);
	ierr = ISCreateGeneral(PETSC_COMM_WORLD, num_panto_dofs, &panto_dofs[0], PETSC_COPY_VALUES, &is_panto); CHKERRV(ierr);
      }


    // Destroy
    template<typename OHE_TYPE>
      void Model_OHE_Panto<OHE_TYPE>::Destroy()
      {
	assert(isDestroyed==false);
	PetscErrorCode ierr;
	PetscBool      isFinalized;
	ierr = PetscFinalized(&isFinalized); CHKERRV(ierr);
	assert(isFinalized==PETSC_FALSE);
	
	ierr = ISDestroy(&is_ohe);      CHKERRV(ierr);
	ierr = ISDestroy(&is_panto);    CHKERRV(ierr);
	ierr = MatDestroy(&dampingMat); CHKERRV(ierr);
	
	// done
	isDestroyed = true;
      }
    
      // Destructor
    template<typename OHE_TYPE>
      Model_OHE_Panto<OHE_TYPE>::~Model_OHE_Panto()
      {
	assert(isDestroyed==true);
      }

    
    // Compute the constraint at a given time
    template<typename OHE_TYPE>
      std::vector<std::pair<int,double>>
      Model_OHE_Panto<OHE_TYPE>::GetContactConstraint(const double time) const
    {
      // position of the pantograph
      const double x = panto.GetPosition(time);

      // active dofs of the ohe (std::array, not std::vector)
      auto ohe_dof_shp_pairs = ohe.GetActiveDofs(x);

      // interactive dof of the pantograph
      const int panto_dof = num_ohe_dofs+panto.GetCoupledDofNum();

      // constraint
      std::vector<std::pair<int,double>> constraint{};
      for(auto& it:ohe_dof_shp_pairs)
	constraint.push_back({it.first,it.second});
      constraint.push_back({panto_dof,-1.});

      // done
      return std::move(constraint);
    }

    
    // evaluate the contact constraint at the current instant
    template<typename OHE_TYPE>
      double Model_OHE_Panto<OHE_TYPE>::GetContactGap() const
      {
	assert(isInitialized==true && isDestroyed==false);
	auto constraint = GetContactConstraint(TI.GetTime());
	const int nterms = static_cast<int>(constraint.size());
	const Vec& U = TI.GetState().U;
	double* uvals;
	PetscErrorCode ierr;
	ierr = VecGetArray(U, &uvals); CHKERRQ(ierr);
	double gap = 0.;
	for(int i=0; i<nterms; ++i)
	  gap += uvals[constraint[i].first]*constraint[i].second;
	ierr = VecRestoreArray(U, &uvals); CHKERRQ(ierr);
	return gap;
      }


    // Evaluate matrices and residual
    template<typename OHE_TYPE>
      void Model_OHE_Panto<OHE_TYPE>::Evaluate(const double time, const Vec& U, Mat& M, Mat& K, Vec& R) const
      {
	assert(isDestroyed==false);

	// set options for matrices
	PetscErrorCode ierr;
	ierr = MatSetOption(M, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);  CHKERRV(ierr);
	ierr = MatSetOption(K, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);  CHKERRV(ierr);
	ierr = MatSetOption(M, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
	ierr = MatSetOption(K, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
	
	// zero
	ierr = MatZeroEntries(M); CHKERRV(ierr);
	ierr = MatZeroEntries(K); CHKERRV(ierr);
	ierr = VecZeroEntries(R); CHKERRV(ierr);

	// Contribution from ohe
	Mat M_ohe, K_ohe;
	Vec U_ohe, R_ohe;
	ierr = MatGetLocalSubMatrix(M, is_ohe, is_ohe, &M_ohe);     CHKERRV(ierr);
	ierr = MatGetLocalSubMatrix(K, is_ohe, is_ohe, &K_ohe);     CHKERRV(ierr);
	ierr = VecGetSubVector(U, is_ohe, &U_ohe);                  CHKERRV(ierr);
	ierr = VecGetSubVector(R, is_ohe, &R_ohe);                  CHKERRV(ierr);
	ohe.Evaluate(U_ohe, M_ohe, K_ohe, R_ohe);
	ierr = VecRestoreSubVector(R, is_ohe, &R_ohe);              CHKERRV(ierr);
	ierr = VecRestoreSubVector(U, is_ohe, &U_ohe);              CHKERRV(ierr);
	ierr = MatRestoreLocalSubMatrix(K, is_ohe, is_ohe, &K_ohe); CHKERRV(ierr);
	ierr = MatRestoreLocalSubMatrix(M, is_ohe, is_ohe, &M_ohe); CHKERRV(ierr);

	// Evaluate contribution from the pantograph
	Mat M_panto, K_panto;
	Vec U_panto, R_panto;
	ierr = MatGetLocalSubMatrix(M, is_panto, is_panto, &M_panto);     CHKERRV(ierr);
	ierr = MatGetLocalSubMatrix(K, is_panto, is_panto, &K_panto);     CHKERRV(ierr);
	ierr = VecGetSubVector(U, is_panto, &U_panto);                    CHKERRV(ierr);
	ierr = VecGetSubVector(R, is_panto, &R_panto);                    CHKERRV(ierr);
	panto.Evaluate(U_panto, M_panto, K_panto, R_panto);
	ierr = VecRestoreSubVector(R, is_panto, &R_panto);                CHKERRV(ierr);
	ierr = VecRestoreSubVector(U, is_panto, &U_panto);                CHKERRV(ierr);
	ierr = MatRestoreLocalSubMatrix(K, is_panto, is_panto, &K_panto); CHKERRV(ierr);
	ierr = MatRestoreLocalSubMatrix(M, is_panto, is_panto, &M_panto); CHKERRV(ierr);

	
	// Add contribution from the penalty spring
	// contact constraint
	auto constraint = GetContactConstraint(time);
	contact_spring.Evaluate(U, constraint, K, R);

	// Finish up assembly
	ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = VecAssemblyBegin(R); CHKERRV(ierr);
	ierr = VecAssemblyEnd(R);   CHKERRV(ierr);

	// done
	return;
      }


    // Evaluate matrices and residual
    template<typename OHE_TYPE>
      void Model_OHE_Panto<OHE_TYPE>::Evaluate(const double time, const Vec& U, Mat& M, Mat& K, Mat &C, Vec& R) const
      {
	// evaluate M, K, R
	Evaluate(time, U, M, K, R);

	// copy the damping matrix
	PetscErrorCode ierr;
	ierr = MatZeroEntries(C); CHKERRV(ierr);
	ierr = MatCopy(dampingMat, C, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);
	return;
      }
    
    
    // Residual for static initialization
    template<typename OHE_TYPE>
      PetscErrorCode Model_OHE_Panto<OHE_TYPE>::
      Static_Residual_Func(SNES snes, Vec solVec, Vec resVec, void *usr)
      {
	// access the model and petsc data
	PetscErrorCode ierr;
	SNES_Context *ctx;
	ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
	assert(ctx!=nullptr);
	assert(ctx->PD!=nullptr);
	assert(ctx->model!=nullptr);

	// compute the residual
	ierr = MatZeroEntries(ctx->PD->massMAT);      CHKERRQ(ierr);
	ierr = MatZeroEntries(ctx->PD->stiffnessMAT); CHKERRQ(ierr);
	ierr = VecZeroEntries(resVec);                CHKERRQ(ierr);
	ctx->model->Evaluate(0., solVec, ctx->PD->massMAT, ctx->PD->stiffnessMAT, resVec);

	// done
	return 0;
      }

    // Jacobian for static initialization
    template<typename OHE_TYPE>
      PetscErrorCode Model_OHE_Panto<OHE_TYPE>::
      Static_Jacobian_Func(SNES snes, Vec solVec, Mat kMat, Mat pMat, void* usr)
      {
	// access the model and petsc data
	PetscErrorCode ierr;
	SNES_Context *ctx;
	ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
	assert(ctx!=nullptr);
	assert(ctx->PD!=nullptr);
	assert(ctx->model!=nullptr);

	// compute the Jacobian
	ierr = MatZeroEntries(ctx->PD->massMAT); CHKERRQ(ierr);
	ierr = MatZeroEntries(kMat);             CHKERRQ(ierr);
	ierr = VecZeroEntries(ctx->PD->resVEC);  CHKERRQ(ierr);
	ctx->model->Evaluate(0., solVec, ctx->PD->massMAT, kMat, ctx->PD->resVEC); 

	// done
	return 0;
      }
	
    
    // Static initialization
    template<typename OHE_TYPE>
      void Model_OHE_Panto<OHE_TYPE>::StaticInitialize() 
      {
	assert(isInitialized==false && isDestroyed==false);
	
	// sparsity of ohe+panto
	std::vector<int> nz       = ohe.GetSparsity();
	std::vector<int> nz_panto = panto.GetSparsity();
	for(auto& it:nz_panto)
	  nz.push_back(it);

	// snes solver
	SNESSolver snes_solver;
	snes_solver.Initialize(nz, Static_Residual_Func, Static_Jacobian_Func);

	// Petsc data
	PetscData PD;
	PD.Initialize(nz);
	PetscErrorCode ierr;
	ierr = MatSetOption(PD.massMAT,      MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);  CHKERRV(ierr);
	ierr = MatSetOption(PD.stiffnessMAT, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);  CHKERRV(ierr);
	ierr = MatSetOption(PD.massMAT,      MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
	ierr = MatSetOption(PD.stiffnessMAT, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);

	// prepare snes solver
	SNES_Context ctx;
	ctx.model = this;
	ctx.PD    = &PD;

	// solve
	std::vector<double> disp(nTotalDof,0.);
	snes_solver.Solve(disp.data(), &ctx, true);

	// set initial conditions in the time integrator
	std::vector<double> zero(nTotalDof,0.);
	TI.Initialize(*this, nz, 0.0, disp.data(), zero.data(), zero.data());

	// Compute the damping matrix
	ComputeDampingMatrix(PD);
	
	// done
	snes_solver.Destroy();
	PD.Destroy();
	isInitialized = true;
	return;
      }


    // Advance to the next time step
    template<typename OHE_TYPE>
      void Model_OHE_Panto<OHE_TYPE>::Advance()
      {
	assert(isInitialized==true && isDestroyed==false);
	TI.Advance();
      }

    
    // Compute the damping matrix
    template<typename OHE_TYPE>
      void Model_OHE_Panto<OHE_TYPE>::ComputeDampingMatrix(PetscData& PD)
      {
	auto& M = PD.massMAT;
	auto& K = PD.stiffnessMAT;
	auto& C = dampingMat;
	auto& U = TI.GetState().U;
	auto& R = PD.resVEC;

	// allocate the damping matrix
	PetscErrorCode ierr;
	ierr = MatDuplicate(K, MAT_DO_NOT_COPY_VALUES, &C); CHKERRV(ierr);
	
	// set options for matrices
	ierr = MatSetOption(M, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);  CHKERRV(ierr);
	ierr = MatSetOption(K, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);  CHKERRV(ierr);
	ierr = MatSetOption(C, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);  CHKERRV(ierr);
	ierr = MatSetOption(M, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
	ierr = MatSetOption(K, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
	ierr = MatSetOption(C, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
	
	// zero
	ierr = MatZeroEntries(M); CHKERRV(ierr);
	ierr = MatZeroEntries(K); CHKERRV(ierr);
	ierr = MatZeroEntries(C); CHKERRV(ierr);
	ierr = VecZeroEntries(R); CHKERRV(ierr);

	// Contribution from ohe
	Mat M_ohe, K_ohe;
	Vec U_ohe, R_ohe;
	ierr = MatGetLocalSubMatrix(M, is_ohe, is_ohe, &M_ohe);     CHKERRV(ierr);
	ierr = MatGetLocalSubMatrix(K, is_ohe, is_ohe, &K_ohe);     CHKERRV(ierr);
	ierr = VecGetSubVector(U, is_ohe, &U_ohe);                  CHKERRV(ierr);
	ierr = VecGetSubVector(R, is_ohe, &R_ohe);                  CHKERRV(ierr);
	ohe.Evaluate(U_ohe, M_ohe, K_ohe, R_ohe); 
	ierr = VecRestoreSubVector(R, is_ohe, &R_ohe);              CHKERRV(ierr);
	ierr = VecRestoreSubVector(U, is_ohe, &U_ohe);              CHKERRV(ierr);
	ierr = MatRestoreLocalSubMatrix(K, is_ohe, is_ohe, &K_ohe); CHKERRV(ierr);
	ierr = MatRestoreLocalSubMatrix(M, is_ohe, is_ohe, &M_ohe); CHKERRV(ierr);

	// Compute damping matrix for the OHE: C = alpha M + beta K
	ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr); 
	ierr = MatAXPY(C, ohe_damping_params.alpha, M, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);
	ierr = MatAXPY(C, ohe_damping_params.beta,  K, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);
	ierr = MatSetOption(M, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);  CHKERRV(ierr);
	ierr = MatSetOption(K, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);  CHKERRV(ierr);
	ierr = MatSetOption(C, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);  CHKERRV(ierr);
	

	// Evaluate contribution from the pantograph
	Mat M_panto, K_panto, C_panto;
	Vec U_panto, R_panto;
	ierr = MatGetLocalSubMatrix(M, is_panto, is_panto, &M_panto);     CHKERRV(ierr);
	ierr = MatGetLocalSubMatrix(K, is_panto, is_panto, &K_panto);     CHKERRV(ierr);
	ierr = MatGetLocalSubMatrix(C, is_panto, is_panto, &C_panto);     CHKERRV(ierr);
	ierr = VecGetSubVector(U, is_panto, &U_panto);                    CHKERRV(ierr);
	ierr = VecGetSubVector(R, is_panto, &R_panto);                    CHKERRV(ierr); 
	panto.Evaluate(U_panto, M_panto, K_panto, C_panto, R_panto);
	ierr = VecRestoreSubVector(R, is_panto, &R_panto);                CHKERRV(ierr);
	ierr = VecRestoreSubVector(U, is_panto, &U_panto);                CHKERRV(ierr);
	ierr = MatRestoreLocalSubMatrix(K, is_panto, is_panto, &K_panto); CHKERRV(ierr);
	ierr = MatRestoreLocalSubMatrix(M, is_panto, is_panto, &M_panto); CHKERRV(ierr);
	ierr = MatRestoreLocalSubMatrix(C, is_panto, is_panto, &C_panto); CHKERRV(ierr);

	// Finish up assembly
	ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyBegin(C, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = MatAssemblyEnd(C,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
	ierr = VecAssemblyBegin(R); CHKERRV(ierr);
	ierr = VecAssemblyEnd(R);   CHKERRV(ierr);

	// done
	return;
      }
	

	
    // Access the contact point deflection, velocity and acceleration
    template<typename OHE_TYPE>
      std::array<double,3> Model_OHE_Panto<OHE_TYPE>::GetOHEContactPointKinematics() const
      {
	assert(isInitialized==true && isDestroyed==false);
		// current time
	const double time = TI.GetTime();

	// pantograph position
	const double x = panto.GetPosition(time);
      
	// interacting dofs and shape function values
	auto dof_shp_pairs = ohe.GetActiveDofs(x);
      
	// state
	const auto& state = TI.GetState();
	const auto& U = state.U;
	const auto& V = state.V;
	const auto& A = state.A;

	// Evaluate position, velocity and acceleration
	double val;
	std::array<double,3> uva{0.,0.,0.};
	for(auto& it:dof_shp_pairs)
	  {
	    const int& a     = it.first;
	    const double& Na = it.second;

	    VecGetValues(U, 1, &a, &val);
	    uva[0] += val*Na;

	    VecGetValues(V, 1, &a, &val);
	    uva[1] += val*Na;

	    VecGetValues(A, 1, &a, &val);
	    uva[2] += val*Na;
	  }

	// done
	return uva;
      }


    // Access the deflection, velocity and acceleration of the pantograph contact point
    template<typename OHE_TYPE>
      std::array<double,3> Model_OHE_Panto<OHE_TYPE>::GetPantographContactPointKinematics() const
      {
	assert(isInitialized==true && isDestroyed==false);
	
	// Pantograph local dof coupled to the OHE
	const int coupled_dof = panto.GetCoupledDofNum();
      
	// indices of pantograph dofs
	const int* indices;
	ISGetIndices(is_panto, &indices);
      
	// Global coupled dof #
	const int indx = indices[coupled_dof];
	ISRestoreIndices(is_panto, &indices);

	// Get values
	std::array<double, 3> uva{0.,0.,0.};
	auto& state = TI.GetState();
	VecGetValues(state.U, 1, &indx, &uva[0]);
	VecGetValues(state.V, 1, &indx, &uva[1]);
	VecGetValues(state.A, 1, &indx, &uva[2]);
      
	return uva;
      }
      

    // Access the contact force
    template<typename OHE_TYPE>
      double Model_OHE_Panto<OHE_TYPE>::GetContactForce() const
      {
	assert(isInitialized==true && isDestroyed==false);

	// spring force
	return contact_spring.ComputeContactForce(TI.GetState().U, GetContactConstraint(TI.GetTime()));
      }
    
    
    // Visualize
    template<typename OHE_TYPE>
      void Model_OHE_Panto<OHE_TYPE>::Write(const std::vector<std::string> ohe_fnames,
					    const std::string panto_fname) const
      {
	assert(isInitialized==true && isDestroyed==false);
	
	// Aliases
	const auto& state = TI.GetState();
	const Vec& U = state.U;
	const Vec& V = state.V;
	const Vec& A = state.A;

	// ohe profiles
	PetscErrorCode ierr;
	Vec U_ohe, V_ohe, A_ohe;
	ierr = VecGetSubVector(U, is_ohe, &U_ohe); CHKERRV(ierr);
	ierr = VecGetSubVector(V, is_ohe, &V_ohe); CHKERRV(ierr);
	ierr = VecGetSubVector(A, is_ohe, &A_ohe); CHKERRV(ierr);
	double* disp, *vel, *accn;
	ierr = VecGetArray(U_ohe, &disp);          CHKERRV(ierr);
	ierr = VecGetArray(V_ohe, &vel);           CHKERRV(ierr);
	ierr = VecGetArray(A_ohe, &accn);          CHKERRV(ierr);
	ohe.Write(disp, vel, accn, ohe_fnames);
	ierr = VecRestoreSubVector(U, is_ohe, &U_ohe); CHKERRV(ierr);
	ierr = VecRestoreSubVector(V, is_ohe, &V_ohe); CHKERRV(ierr);
	ierr = VecRestoreSubVector(A, is_ohe, &A_ohe); CHKERRV(ierr);
	
	// pantograph position
	const double time = TI.GetTime();
	const double x = panto.GetPosition(time);

	// pantograph head position
	Vec U_panto;
	ierr = VecGetSubVector(U, is_panto, &U_panto); CHKERRV(ierr);
	const int coupled_indx = panto.GetCoupledDofNum();
	double wval;
	ierr = VecGetValues(U_panto, 1, &coupled_indx, &wval); CHKERRV(ierr);
	ierr = VecRestoreSubVector(U, is_panto, &U_panto);     CHKERRV(ierr);

	// vertical extent of the pantograph
	const double z0 = panto.GetBaseHeight();
	const double z1 = ohe.GetSpan(0).GetDatum() + wval;

	// write to file
	std::fstream pfile;
	pfile.open(panto_fname, std::ios::out);
	assert(pfile.good());
	pfile << x << " " << z0 << std::endl
	      << x << " " << z1 << std::endl;
	pfile.close();

	// done
	return;
      }
	
  }
}
