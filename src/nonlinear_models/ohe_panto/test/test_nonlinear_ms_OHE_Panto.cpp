
/** \file test_nonlinear_ms_OHE_Panto.cpp
 * \brief Unit test for class pfe::nonlinear::Model_MultiSpan_OHE_Panto
 * \author Ramsharan Rangarajan
 */

#include <pfe_nonlinear_Model_OHE_Panto.h>
#include <pfe_nonlinear_GenAlpha.h>
#include <pfe_json.hpp>
#include <iostream>

// simulation parameter
void read_simulation_parameters(const std::string sim_file, const std::string sim_id,
				std::string& params_file,
				int& panto_type, std::string& panto_tag,
				int& num_spans, int& nelm,
				std::string& time_integrator_type,
				double& time_integrator_param,
				double& timestep,
				int& num_time_steps,
				std::string& out_directory);

// aliases
using OHE_TYPE   = pfe::nonlinear::MultiSpan_OHE;
using PANTO_TYPE = pfe::Pantograph_1;
using MODEL_TYPE = pfe::nonlinear::Model_MultiSpan_OHE_Panto;
  
int main(int argc, char** argv)
{  
  // start clock
  auto start = std::chrono::high_resolution_clock::now();
  
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRQ(ierr);

  // read simulation parameters
  std::string params_file;            // json file with ohe+pantograph data
  std::string outdir;                 // output directory
  int         num_spans;              // number of spans
  int         nelm;                   // number of elements between successive droppers
  int         panto_type;             // pantograph type = 1 or 3
  std::string panto_tag;              // pantograph tag in the parameter file
  std::string time_integrator_type;   // time integrator type
  double      rho;                    // parameter to be used with the time integrator
  double      timestep;               // time step
  int         num_time_steps;         // number of time steps
  read_simulation_parameters("sim_options.json",  "ms", 
			     params_file,
			     panto_type, panto_tag, num_spans, nelm,
			     time_integrator_type, rho,
			     timestep, num_time_steps, outdir);
  outdir += "/";
  
  // ohe + pantograph parametetes
  std::fstream jfile;
  jfile.open(params_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());

  pfe::OHESpan_ParameterPack ohe_params;
  jfile >> ohe_params;

  pfe::OHEDampingParams ohe_damping_params{.tag="ohe damping"};
  jfile >> ohe_damping_params;
  
  pfe::Pantograph_1_Params panto_1_params{.tag=panto_tag};
  jfile >> panto_1_params;

  // Create OHE + pantograph
  OHE_TYPE ohe(0.0, ohe_params, num_spans, nelm, 1.e5);
  PANTO_TYPE panto(panto_1_params);

  // Contact spring
  pfe::LinearContactSpring contact_spring(0.5e5);

  // time integrator
  const int nTotalDof = ohe.GetTotalNumDof()+panto.GetTotalNumDof();
  pfe::nonlinear::GenAlpha<MODEL_TYPE> TI(rho, nTotalDof, timestep);

  // create the model
  MODEL_TYPE model(ohe, ohe_damping_params, panto, contact_spring, TI);

  // static initialization
  model.StaticInitialize();
  model.Write({outdir+"cat-0.dat", outdir+"con-0.dat", outdir+"drop-0.dat"}, outdir+"panto-0.dat");
  std::cout<< "Contact gap: " << model.GetContactGap() << std::endl;
 
  // History at the contact point
  std::fstream hfile;
  hfile.open(outdir+"history.dat", std::ios::out);
  assert(hfile.good());
  hfile << "# time \t contact force \t ohe (u, v, a) \t pantograph (u, v, a) "<< std::endl;
  std::array<double,3> ohe_uva   = model.GetOHEContactPointKinematics();
  std::array<double,3> panto_uva = model.GetPantographContactPointKinematics();
  hfile << 0.0 << "\t" << model.GetContactForce() << "\t"
	<< ohe_uva[0] << "\t" << ohe_uva[1] << "\t" << ohe_uva[2] << "\t"
    	<< panto_uva[0] << "\t" << panto_uva[1] << "\t" << panto_uva[2] << std::endl;

  // time stepping
  std::cout << "Time step (of " << num_time_steps << ") " << std::endl;
  for(int tstep=1;  tstep<=num_time_steps; ++tstep)
    {
      std::cout << tstep << std::endl;
      model.Advance();

      /*const std::string str = std::to_string(tstep)+".dat";
	model.Write({outdir+"cat-"+str, outdir+"con-"+str, outdir+"drop-"+str}, outdir+"panto-"+str);*/
      
            
      const double time =  model.GetTime();
      ohe_uva   = model.GetOHEContactPointKinematics();
      panto_uva = model.GetPantographContactPointKinematics();
      hfile << model.GetTime() << "\t" << model.GetContactForce() << "\t"
	    << ohe_uva[0]       << "\t" << ohe_uva[1]   << "\t" << ohe_uva[2] << "\t"
	    << panto_uva[0]     << "\t" << panto_uva[1] << "\t" << panto_uva[2] << std::endl;
    }
  
  // end clock
  auto elapsed = std::chrono::high_resolution_clock::now() - start;
  long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
  int seconds = floor(microseconds*1.0e-6);
  int minutes = floor(seconds/60.0);
  seconds = seconds % 60;
  //std::cout << "Time taken : " << microseconds*1.0e-6 << "s" << std::endl;
  std::cout << "Time taken : " << minutes << "m " << seconds << "s" << std::endl;
  
  // clean up
  ohe.Destroy();
  model.Destroy();
  TI.Destroy();
  PetscFinalize();
}


// simulation parameter
void read_simulation_parameters(const std::string sim_file, const std::string sim_id,
				std::string& params_file,
				int& panto_type, std::string& panto_tag, int& num_spans, int& nelm,
				std::string& time_integrator_type, double& time_integrator_param,
				double& timestep, int& num_time_steps,
				std::string& out_directory)
{
  assert(std::filesystem::path(sim_file).extension()==".json");
  std::fstream jfile;
  jfile.open(sim_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());

  // get the data
  auto J = nlohmann::json::parse(jfile);
  jfile.close();

  // read options
  auto it = J.find(sim_id);
  assert(it!=J.end());
  auto& j = *it;
  assert(j.contains("parameters file")    && "Input file missing params file tag");
  assert(j.contains("num spans")          && "Input file missing num spans");
  assert(j.contains("pantograph type")    && "Input file missing pantograph type");
  assert(j.contains("pantograph tag")     && "Input file missing pantograph tag");
  assert(j.contains("num elements")       && "Input file missing num elements tag");
  assert(j.contains("time integrator")    && "Input file missing time integrator tag");
  assert(j.contains("time step")          && "Input file missing time step tag");
  assert(j.contains("num time steps")     && "Input file missing num time steps tag");
  assert(j.contains("output directory")   && "Input file missing output directory tag");
  
  j["parameters file"].get_to(params_file);
  j["num spans"].get_to(num_spans);
  j["pantograph type"].get_to(panto_type);
  j["pantograph tag"].get_to(panto_tag);
  j["num elements"].get_to(nelm);
  j["time integrator"].get_to(time_integrator_type);
  j["time step"].get_to(timestep); 
  j["num time steps"].get_to(num_time_steps);
  j["output directory"].get_to(out_directory);

  // list of tags
  std::set<std::string> tags{"parameters file", "num spans", "pantograph type", "pantograph tag",
      "num elements", "time integrator", "time step", "num time steps", "output directory", "_comment"};
  
  // time integrator parameter
  if(time_integrator_type=="generalized alpha")
    {
      assert(j.contains("spectral radius") && "Input file missing spectral radius tag");
      j["spectral radius"].get_to(time_integrator_param);
      tags.insert("spectral radius");
    }
  else
    {
      assert(false && "Expected time integrator = generalized alpha");
    }

  // checks on inputs
  assert(params_file.empty()==false && std::filesystem::exists(params_file));
  assert(nelm>0 && timestep>0 && num_time_steps>0 && num_spans>=1);
  assert((panto_type==1 || panto_type==3) && "Expected pantograph type to be 1 or 3");
  
  // create the output directory if it does not exist, erase .dat files it does
  if(std::filesystem::exists(out_directory)==true)
    {
      std::cout << "Output directory " << out_directory << " exists. Erasing dat files" << std::endl;
      auto it_dir = std::filesystem::directory_iterator(out_directory);
      for(auto& it:it_dir)
	if(it.path().extension()==".dat")
	  {
	    auto flag = std::filesystem::remove(it.path());
	    assert(flag==true && "Could not erase file");
	  }
    }
  else
    {
      std::cout << "Creating output directory \"" << out_directory << "\"" << std::endl;
      auto flag  = std::filesystem::create_directory(out_directory);
      assert(flag==true && "Could not create output directory");
    }

  // Check consistency of tags
  for(auto jt:j.items())
    if(tags.find(jt.key())==tags.end())
      {
	std::cout << "Unexpected tag " << jt.key() << " found in input file " << std::endl;
	assert(false);
      }

  // done
  return;
}
