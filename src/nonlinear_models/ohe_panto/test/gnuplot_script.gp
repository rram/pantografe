set term jpeg
set ylabel "Displacement [m]"
set xlabel "Time [s]"
set autoscale xfix
set output "ohe-movingload_u2.jpg"
plot "output/history.dat" u 1:6 w l title "PantograFE"

set term jpeg
set ylabel "Velocity [m/s]"
set xlabel "Time [s]"
set autoscale xfix
set output "ohe-movingload_v2.jpg"
plot "output/history.dat" u 1:7 w l title "PantograFE"

set term jpeg
set ylabel "Acceleration [m/s^2]"
set xlabel "Time [s]"
set autoscale xfix
set yrange [-35:30]
set output "ohe-movingload_a2.jpg"
plot "output/history.dat" u 1:8 w l title "PantograFE"

set term jpeg
set ylabel "Contact Force [N]"
set xlabel "Time [s]"
set autoscale xfix
set yrange [-100:500]
set output "ohe-movingload_cfn2.jpg"
plot "output/history.dat" u 1:2 w l title "PantograFE"

