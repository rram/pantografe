#! /bin/bash

set -e

g++ -o stat.out ../../../../../applications/scripts/stat01.cpp
./stat.out 8 output/history.dat 6 Displacement 7 Velocity 8 Acceleration 2 ContactForce
rm stat.out

rm -rf plots/
gnuplot "gnuplot_script.gp"
mkdir plots
mv *.jpg plots/.
mv statistics.dat plots/.
