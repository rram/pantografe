
# add library
add_library(pfe_ohe_damping STATIC
		 pfe_OHEDampingParams.cpp)

# headers
target_include_directories(pfe_ohe_damping PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<INSTALL_INTERFACE:include>)

# link
target_link_libraries(pfe_ohe_damping PUBLIC pfe_ext)

# Add required flags
target_compile_features(pfe_ohe_damping PUBLIC ${pfe_COMPILE_FEATURES})

install(FILES
	pfe_OHEDampingParams.h
	DESTINATION ${PROJECT_NAME}/include)
