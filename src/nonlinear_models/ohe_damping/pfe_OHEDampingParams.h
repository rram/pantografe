
/** \file pfe_OHEDampingParams.h
 * \brief Defines the struct pfe::OHEDampingParams
 * \author Ramsharan Rangarajan
 */


#pragma once

#include <fstream>
#include <string>

namespace pfe
{
  struct OHEDampingParams
  {
    std::string tag = "ohe damping";
    double alpha = 0.;
    double beta = 0.;

    friend std::ostream& operator << (std::ostream& out, const OHEDampingParams& params);
    friend std::istream& operator >> (std::istream& in,  OHEDampingParams& params);
  };
}
