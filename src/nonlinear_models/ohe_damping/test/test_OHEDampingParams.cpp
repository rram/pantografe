
/** \file test_PantographParams.cpp
 * \brief Unit test for read/write of parameters using the json format
 * \author Ramsharan Rangarajan
 * \comp_test Unit test for read/write of parameters using the json format
 */


#include <pfe_OHEDampingParams.h>
#include <random>
#include <cassert>

void Generate(pfe::OHEDampingParams& params);

void Compare(const pfe::OHEDampingParams& A, pfe::OHEDampingParams& B, 
	     const double tol);


int main()
{
  std::fstream jfile;
  jfile.open("datasheet.json", std::ios::out);
  
  // Write 
  jfile << "{";
  // damping parameters
  pfe::OHEDampingParams params;
  params.tag = "ohe damping";
  Generate(params);
  jfile << params;
  jfile << "}";
  jfile.close();

  // Read & compare
  jfile.open("datasheet.json", std::ios::in);

  pfe::OHEDampingParams params_copy;
  params_copy.tag = "ohe damping";
  jfile >> params_copy;
  jfile.close();
  Compare(params, params_copy, 1.e-6);
}


void Generate(pfe::OHEDampingParams& params)
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(1.0, 100.0);
  params.alpha = dis(gen);
  params.beta = dis(gen);
  assert(params.tag=="ohe damping");
  return;
}


void Compare(const pfe::OHEDampingParams& A, pfe::OHEDampingParams& B,
	     const double tol)
{
  assert(A.tag.empty()==false && B.tag.empty()==false);
  assert(A.tag==B.tag);
  assert(std::abs(A.alpha-B.alpha)<tol);
  assert(std::abs(A.beta-B.beta)<tol);
  
  return;
}
