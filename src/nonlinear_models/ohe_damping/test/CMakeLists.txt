
# Project
project(ohe-damping-unit-tests)

# Identical for all targets
foreach(TARGET test_OHEDampingParams)
  
  # Add this target
  add_executable(${TARGET}  ${TARGET}.cpp)
  
  # Link
  target_link_libraries(${TARGET} PUBLIC pfe_ohe_damping)

  # choose appropriate compiler flags
  target_compile_features(${TARGET} PUBLIC ${pfe_COMPILE_FEATURES})
  
  add_test(NAME ${TARGET} COMMAND ${TARGET})

endforeach()
