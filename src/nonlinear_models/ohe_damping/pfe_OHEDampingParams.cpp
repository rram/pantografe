
/** \file pfe_OHEDamping.cpp
 * \brief Implements i/o for the struct pfe::OHEDampingParams
 * \author Ramsharan Rangarajan
 */


#include <pfe_OHEDampingParams.h>
#include <pfe_json.hpp>
#include <cassert>
#include <iomanip>


namespace pfe
{
  std::ostream& operator << (std::ostream& out, const OHEDampingParams& params)
  {
    assert(params.tag.empty()==false);

    nlohmann::json j;
    auto& J = j[params.tag];
    J["alpha"] = params.alpha;
    J["beta"]  = params.beta;

    out << std::endl << std::endl
	<< "\"" << params.tag << "\"" << " : " << std::setw(8) << j[params.tag];
    return out;
  }
  
  std::istream& operator >> (std::istream& in, OHEDampingParams& params)
  {
    assert(params.tag.empty()==false);

    // rewind to the start of the file
    in.seekg(0, std::ios::beg);

    // parse
    auto J = nlohmann::json::parse(in);
    auto it = J.find(params.tag);
    assert(it!=J.end());
    auto& j = *it;

    // check fields
    assert(j.empty()==false);
    assert(j.contains("alpha") && j.contains("beta"));

    // read
    j["alpha"].get_to(params.alpha);
    j["beta"].get_to(params.beta);

    return in;
  }

}
