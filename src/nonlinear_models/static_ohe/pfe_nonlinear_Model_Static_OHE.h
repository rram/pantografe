
/** \file pfe_nonlinear_Model_Static_OHE.h
 * \brief Defines the class pfe::nonlinear::Model_Static_OHE to simulate static behavior of an OHE
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_nonlinear_SingleSpan_OHE.h>
#include <pfe_nonlinear_MultiSpan_OHE.h>
#include <pfe_SNES_Solver.h>

namespace pfe
{
  namespace nonlinear
  {
    //!< Class to simulate static behavior of an single span OHE
    //! \ingroup models
    template<typename OHE_TYPE>
      class Model_Static_OHE
      {
      public:
	//! Constructor
	Model_Static_OHE(OHE_TYPE& ohe_obj);

	//! Destructor
	virtual ~Model_Static_OHE();

	//! Disable copy and assignment
	Model_Static_OHE(const Model_Static_OHE&) = delete;
	Model_Static_OHE operator=(const Model_Static_OHE&) = delete;

	//! Destroy data structures
	void Destroy();
	
	//! Access the span
	inline const OHE_TYPE& GetOHE() const
	{ return ohe; }
    
	//! number of dofs
	inline int GetTotalNumDof() const
	{ return ohe.GetTotalNumDof(); }
    
	//! Main functionality: static solve
	void Solve(double* displacements);

	//! Main functionality: adjust dropper lengths to match sag
	void AdjustDropperLengths(const int span_num, const int nIters);

	//! Compute transverse stiffness disrtribution
	void ComputeTransverseStiffnessDistribution(const int span_num, const double* displacements,
						    const double Force, double* stiffness) const;

	//! Visualize the configuration
	inline void Write(const double* displacements,
			  const std::string catenary_fname,
			  const std::string contact_fname,
			  const std::string dropper_fname) const
	{ ohe.Write(displacements, catenary_fname, contact_fname, dropper_fname); }

      private:

	struct SNES_Context
	{
	  OHE_TYPE  *ohe;
	  PetscData *petsc_data;
	  double    force;
	  int       dofnum;
	};
      
	static PetscErrorCode Residual_Func(SNES snes, Vec solVec, Vec resVec, void* usr);
	static PetscErrorCode Jacobian_Func(SNES snes, Vec solVec, Mat kMat, Mat pMat, void* usr);

	bool isDestroyed;
	OHE_TYPE& ohe;                   //!< ohe
	mutable SNESSolver snes_solver;  //!< Helper for nonlinear solves
	mutable PetscData  petsc_data;   //!< Helper data for snes assembly
      };

    using Model_Static_SingleSpan_OHE = Model_Static_OHE<SingleSpan_OHE>;
    using Model_Static_MultiSpan_OHE  = Model_Static_OHE<MultiSpan_OHE>;
  }
}

#include <pfe_nonlinear_Model_Static_OHE_impl.h>
