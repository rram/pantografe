// Sriramajayam

#include <pfe_Simulate.h>

int main(int argc, char** argv) {

  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRQ(ierr);

  // read commandline options
  std::string sim_file = "input_nonlinear_moving_load.json";
  std::string sim_tag = "demo";

  // start clock
  auto start = std::chrono::high_resolution_clock::now();
  
  pfe::run(sim_file, sim_tag);
  PetscFinalize();
  
  auto elapsed = std::chrono::high_resolution_clock::now() - start;
  long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
  int seconds = floor(microseconds*1.0e-6);
  int minutes = floor(seconds/60.0);
  seconds = seconds % 60;
  //std::cout << "Time taken : " << microseconds*1.0e-6 << "s" << std::endl;
  std::cout << "Time taken : " << minutes << "m " << seconds << "s" << std::endl;
  
}
