
#include <pfe_SimParams.h>
#include <set>

namespace pfe
{
  void Dynamic_SimParams::create_template(const std::string sim_file, const std::string sim_tag)
  {
    assert(std::filesystem::path(sim_file).extension()==".json" && "Expected simulation options file to have .json extension");

    nlohmann::json J;
    auto& j = J[sim_tag];

    j["simulation type"]  = "nonlinear dynamic"; 
    j["parameters file"]  = "parameters.json";
    j["num spans"]        = 5;
    j["pantograph type"]  = 3;
    j["pantograph tag"]   = "panto_3";
    j["contact type"]     = "intermittent";
    j["num elements"]     = 6;
    j["time integrator"]  = "generalized alpha";
    j["spectral radius"]  = 0.1;
    j["time step"]        = 0.005;
    j["num time steps"]   = 100;
    j["output directory"] = "output";

    std::fstream jfile;
    jfile.open(sim_file, std::ios::out);
    assert((jfile.good() && jfile.is_open()) && "Could not open simulation options file");
    jfile << J;
    jfile.close();
    return;
  }

  // dynamic simulation parameters
  Dynamic_SimParams::Dynamic_SimParams(const std::string sim_file, const std::string sim_id, bool clear_outputdir)
  {
    assert(std::filesystem::path(sim_file).extension()==".json");
    std::fstream jfile;
    jfile.open(sim_file, std::ios::in);
    assert(jfile.good() && jfile.is_open());

    // get the data
    auto J = nlohmann::json::parse(jfile);
    jfile.close();

    // read options
    auto it = J.find(sim_id);
    assert(it!=J.end());
    auto& j = *it;
    assert(j.contains("simulation type")    && "Input file missing simulation type tag");
    assert(j.contains("parameters file")    && "Input file missing params file tag");
    assert(j.contains("num spans")          && "Input file missing num spans");
    assert(j.contains("pantograph type")    && "Input file missing pantograph type");
    assert(j.contains("pantograph tag")     && "Input file missing pantograph tag");
    assert(j.contains("contact type")       && "Input file missing contact type");
    assert(j.contains("num elements")       && "Input file missing num elements tag");
    assert(j.contains("time integrator")    && "Input file missing time integrator tag");
    assert(j.contains("time step")          && "Input file missing time step tag");
    assert(j.contains("num time steps")     && "Input file missing num time steps tag");
    assert(j.contains("output directory")   && "Input file missing output directory tag");

    j["simulation type"].get_to(simulation_type);
    j["parameters file"].get_to(params_file);
    j["num spans"].get_to(num_spans);
    j["pantograph type"].get_to(panto_type);
    j["pantograph tag"].get_to(panto_tag);
    j["contact type"].get_to(contact_type);
    j["num elements"].get_to(nelm);
    j["time integrator"].get_to(time_integrator_type);
    j["time step"].get_to(timestep); 
    j["num time steps"].get_to(num_time_steps);
    j["output directory"].get_to(outdir);

    // list of tags
    std::set<std::string> tags{"simulation type", "parameters file", "num spans", "pantograph type", "pantograph tag", "contact type",
	"num elements", "time integrator", "time step", "num time steps", "output directory", "_comment"};
  
    // time integrator parameter
    if(time_integrator_type=="generalized alpha")
      {
	assert(j.contains("spectral radius") && "Input file missing spectral radius tag");
	j["spectral radius"].get_to(time_integrator_param);
	tags.insert("spectral radius");
      }
    else if(time_integrator_type=="hht alpha")
      {
	assert(j.contains("alpha") && "Input file missing alpha tag");
	j["alpha"].get_to(time_integrator_param);
	tags.insert("alpha");
      }

    // checks on inputs
    assert(params_file.empty()==false && std::filesystem::exists(params_file));
    assert(nelm>0 && timestep>0 && num_time_steps>0 && num_spans>=1);
    assert((panto_type==0 || panto_type==1 || panto_type==3) && "Expected pantograph type to be 0, 1 or 3");
    assert((contact_type=="persistent" || contact_type=="intermittent") && "Expected contact type to be persistent or intermittent");
    if(simulation_type=="linear")
      assert(contact_type=="persistent" && "Linear simulation more only supports persistent contact type");
    if(panto_type==0)
      assert(contact_type=="persistent" && "Moving load simulations only supports persistent contact type");
	
  
    // create the output directory if it does not exist, erase .dat files it does
    if(std::filesystem::exists(outdir)==true)
      {
	std::cout << "Output directory " << outdir << " exists. " << std::endl;
	if(clear_outputdir==true)
	  {
	    std::cout << "Erasing dat files" << std::endl;
	    auto it_dir = std::filesystem::directory_iterator(outdir);
	    for(auto& it:it_dir)
	      if(it.path().extension()==".dat")
		{
		  auto flag = std::filesystem::remove(it.path());
		  assert(flag==true && "Could not erase file");
		}
	  }
      }
    else
      {
	std::cout << "Creating output directory \"" << outdir << "\"" << std::endl;
	auto flag  = std::filesystem::create_directory(outdir);
	assert(flag==true && "Could not create output directory");
      }

    // Check consistency of tags
    for(auto jt:j.items())
      if(tags.find(jt.key())==tags.end())
	{
	  std::cout << "Unexpected tag " << jt.key() << " found in input file " << std::endl;
	  assert(false);
	}

    // done
  }

}
