// Sriramajayam

#pragma once

#include <pfe_json.hpp>
#include <iostream>
#include <fstream>

namespace pfe
{
  // static simulation parameters
  struct Static_SimParams {
    std::string simulation_type;
    std::string params_file;
    std::string outdir;
    int         nelm;
    int         num_length_iters;
    double      force_val;
    Static_SimParams(const std::string sim_file, const std::string sim_tag, bool clear_output_dir=true);
    static void create_template(const std::string sim_file, const std::string sim_tag);
  };

  // dynamic simulation parameters
  struct Dynamic_SimParams {
    std::string simulation_type;        // simulation type
    std::string params_file;            // json file with ohe+pantograph data
    std::string outdir;                 // output directory
    int         num_spans;              // number of spans
    int         nelm;                   // number of elements between successive droppers
    int         panto_type;             // pantograph type = 1 or 3
    std::string panto_tag;              // pantograph tag in the parameter file
    std::string contact_type;           // persistent or intermittent contact
    std::string time_integrator_type;   // time integrator type
    double      time_integrator_param;  // parameter to be used with the time integrator
    double      timestep;               // time step
    int         num_time_steps;         // number of time steps
    Dynamic_SimParams(const std::string sim_file, const std::string sim_id, bool clear_output_dir=true);
    static void create_template(const std::string sim_file, const std::string sim_tag);
  };

}
