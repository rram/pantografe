
#include <pfe_SimParams.h>
#include <set>

namespace pfe
{
  void Static_SimParams::create_template(const std::string sim_file, const std::string sim_tag)
  {
    assert(std::filesystem::path(sim_file).extension()==".json" && "Expected simulation options file to have .json extension");

    nlohmann::json J;
    auto& j = J[sim_tag];
    j["simulation type"] = "linear static";
    j["parameters file"] = "parameters.json";
    j["num elements"]    = 4;
    j["output directory"] = "output";
    j["length adjustment iterations"] = 5;
    j["force"] = 100.0;

    std::fstream jfile;
    jfile.open(sim_file, std::ios::out);
    assert((jfile.good() && jfile.is_open()) && "Could not open simulation options file");
    jfile << J;
    jfile.close();
    return;
  }

  // static simulation parameters
  Static_SimParams::Static_SimParams(const std::string sim_file, const std::string sim_tag, bool clear_outputdir)
  {
    assert(std::filesystem::path(sim_file).extension()==".json" &&
	   "Expected simulation options file to have .json extension");
    std::fstream jfile;
    jfile.open(sim_file, std::ios::in);
    assert((jfile.good() && jfile.is_open()) && "Could not open simulation options file");

    // get the data
    auto J = nlohmann::json::parse(jfile);
    jfile.close();

    // read options
    auto it = J.find(sim_tag);
    assert(it!=J.end() && "Could not find simulation id in input file");
    auto& j = *it;
    assert(j.contains("simulation type")              && "Input file missing simulation type tag");
    assert(j.contains("parameters file")              && "Input file missing parameters file tag");
    assert(j.contains("num elements")                 && "Input file missing num elements tag");
    assert(j.contains("output directory")             && "Input file missing output directory tag");
    assert(j.contains("length adjustment iterations") && "Input file missing length adjustment iterations tag");
    assert(j.contains("force")                        && "Input file missing force tag");

    j["simulation type"].get_to(simulation_type);
    j["parameters file"].get_to(params_file);
    j["num elements"].get_to(nelm);
    j["output directory"].get_to(outdir);
    j["length adjustment iterations"].get_to(num_length_iters);
    j["force"].get_to(force_val);

    assert((params_file.empty()==false && std::filesystem::exists(params_file)) &&
	   "Cannot find ohe file specified in input file");
    assert(nelm>0 && "Expected positive number of elements in input file");
    assert(num_length_iters>=0 && "Unexpected number of length adjustment iterations specified in input file");

    // create the output directory if it does not exist, erase .dat files it does
    if(std::filesystem::exists(outdir)==true)
      {
	std::cout << "Output directory " << outdir << " exists. " << std::endl;
	if(clear_outputdir==true)
	  {
	    std::cout << "Erasing dat files" << std::endl;
	    auto it_dir = std::filesystem::directory_iterator(outdir);
	    for(auto& it:it_dir)
	      if(it.path().extension()==".dat")
		{
		  auto flag = std::filesystem::remove(it.path());
		  assert(flag==true && "Could not erase file");
		}
	  }
      }
    else
      {
	std::cout << "Creating output directory \"" << outdir << "\"" << std::endl;
	auto flag  = std::filesystem::create_directory(outdir);
	assert(flag==true && "Could not create output directory");
      }

    // should not have any other tags
    std::set<std::string> tags{"simulation type", "parameters file", "num elements", "output directory", "length adjustment iterations", "force", "_comment"};
    for(auto jt:j.items())
      if(tags.find(jt.key())==tags.end())
	{
	  std::cout << "Unexpected tag " << jt.key() << " found in input file " << std::endl;
	  assert(false);
	}
  }

}
  
  
