// Sriramajayam

#include <pfe_Simulate.h>

namespace pfe
{
  namespace linear {
  
    // run the simulation
    void simulate_ohe_moving_load(const std::string sim_file, const std::string sim_id)
    {

      // read simulation parameters
      pfe::Dynamic_SimParams sp(sim_file,  sim_id);
    
      // Expect the moving load case
      assert(sp.simulation_type=="linear dynamic" && sp.panto_type==0);
    
      std::fstream jfile;
      jfile.open(sp.params_file, std::ios::in);
      assert(jfile.good() && jfile.is_open());
    
      // ohe + moving load parameters
      pfe::OHESpan_ParameterPack ohe_pack;
      jfile >> ohe_pack;
      pfe::Pantograph_0_Params panto_0_params{.tag=sp.panto_tag};
      jfile >> panto_0_params;
      jfile.close();
    
      // Create the OHE span
      pfe::linear::MultiSpan_OHE ohe(0., ohe_pack, sp.num_spans, sp.nelm);
    
      // Time integrator
      const int nTotalDof = ohe.GetTotalNumDof();
      std::unique_ptr<pfe::TimeIntegrator> TI;
      if(sp.time_integrator_type=="generalized alpha")
	TI = std::make_unique<pfe::GenAlpha>(sp.time_integrator_param, nTotalDof, sp.timestep);
      else if(sp.time_integrator_type=="hht alpha")
	TI = std::make_unique<pfe::HHTAlpha>(sp.time_integrator_param, nTotalDof, sp.timestep);
      else if(sp.time_integrator_type=="newmark")
	TI = std::make_unique<pfe::Newmark>(nTotalDof, sp.timestep);
      else
	assert(false && "Expected time integrator to be one of 'generalized alpha', 'hht alpha' or 'newmark'");
    
      // Create an instance of the model and simulate
      pfe::linear::Model_MultiSpanOHE_MovingLoad model(ohe, panto_0_params, *TI);

      // Static solution
      model.StaticInitialize();
    
      // visualize
      //model.Write({out_directory+"/catenary-ref.dat", out_directory+"/contact-ref.dat", out_directory+"/dropper-ref.dat"},  out_directory+"/load-ref.dat");
      model.Write({sp.outdir+"/cat-0.dat", sp.outdir+"/con-0.dat", sp.outdir+"/drop-0.dat"}, sp.outdir+"/load-0.dat");
    
      // Load point history
      std::fstream hfile;
      hfile.open(sp.outdir+"/history.dat", std::ios::out);
      assert(hfile.good());
      hfile << "#Time, U, V, A " << std::endl;
    
      // Time stepping
      std::cout << "Time step (of " << sp.num_time_steps << "): " << std::endl;
      for(int tstep=0; tstep<sp.num_time_steps; ++tstep)
	{
	  std::cout << tstep+1 << std::endl;
      
	  model.Advance();
	  std::string tstr = std::to_string(tstep+1)+".dat";
	  model.Write({sp.outdir+"/cat-"+tstr, sp.outdir+"/con-"+tstr, sp.outdir+"/drop-"+tstr}, sp.outdir+"/load-"+tstr);

	  // Load point kinematics
	  auto uva = model.GetLoadPointKinematics();
	  hfile << model.GetTime() << " " << uva[0] << " " << uva[1] << " " << uva[2] << std::endl;
	}
      hfile.close();
      
      TI->Destroy();
      ohe.Destroy();
      model.Destroy();
      return;
    }

  }
}
