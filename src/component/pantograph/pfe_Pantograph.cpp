
/** \file pfe_Pantograph.cpp
 * \brief Implements the class pfe::Pantograph
 * \author Ramsharan Rangarajan
 */

#include <pfe_Pantograph.h>
#include <fstream>
#include <iostream>
#include <cassert>

namespace pfe
{
  // Write pantograph configuration to file
  void Pantograph::Write(const double* disp, const double* vel, const double* accn,
			 const std::string fname) const
  {
    std::fstream pfile;
    pfile.open(fname, std::ios::app|std::ios::out);
    assert(pfile.good());
    const int ndof = GetTotalNumDof();
    for(int a=0; a<ndof; ++a)
      pfile << disp[a] << "\t" << vel[a] << "\t" << accn[a] << "\t";
    pfile << std::endl;
    pfile.close();

    // done
    return;
  }
}
