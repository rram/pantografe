
/** \file pfe_Pantograph_1.cpp
 * \brief Implements the class pfe::Pantograph_1
 * \author Ramsharan Rangarajan
 */

#include <pfe_Pantograph.h>
#include <cassert>

namespace pfe
{
  // Constructor
  Pantograph_3::Pantograph_3(const Pantograph_3_Params& p)
      :params(p) {

    // diagonal mass: [m1 0 0; 0 m2 0; 0 0 m3]
    Mvec[0] = params.m1;
    Mvec[1] = params.m2;
    Mvec[2] = params.m3;

    // stiffness
     // E : 1/2 k1 u1^2 + 1/2 k2 (u2-u1)^2 + 1/2 k3 (u3-u2)^2
    //  K :
    // [k1+k2  -k2     0]
    // [-k2    k2+k3  -k3]
    // [0      -k3     k3]
    Kmat[0][0] = params.k1+params.k2;
    Kmat[1][1] = params.k2+params.k3;
    Kmat[2][2] = params.k3;
    Kmat[0][1] = -params.k2;
    Kmat[1][0] = -params.k2;
    Kmat[1][2] = -params.k3;
    Kmat[2][1] = -params.k3;
    Kmat[0][2] = 0.;
    Kmat[2][0] = 0.;

    // Damping: similar structure as the stiffness matrix
    Cmat[0][0] = params.c1+params.c2;
    Cmat[1][1] = params.c2+params.c3;
    Cmat[2][2] = params.c3;
    Cmat[0][1] = -params.c2;
    Cmat[1][0] = -params.c2;
    Cmat[1][2] = -params.c3;
    Cmat[2][1] = -params.c3;
    Cmat[0][2] = 0.;
    Cmat[2][0] = 0.;
    
    // force: [F 0 0]
    Fvec[0] = params.force;
    Fvec[1] = 0.;
    Fvec[2] = 0.;
    
    // indices
    indx[0] = 0;
    indx[1] = 1;
    indx[2] = 2;
  }
  
  // Assemble contributions to the mass, stiffness and force vector
  void Pantograph_3::Evaluate(Mat& M, Mat& K, Vec& F) const
  {
    // Sanity checks
    PetscErrorCode ierr;
    int nrows, ncols;
    ierr = MatGetSize(M, &nrows, &ncols); CHKERRV(ierr);
    assert(nrows==3 && ncols==3);
    ierr = MatGetSize(K, &nrows, &ncols); CHKERRV(ierr);
    assert(nrows==3 && ncols==3);

    for(int i=0; i<3; ++i)
      {
	for(int j=0; j<3; ++j)
	  {
	    ierr = MatSetValuesLocal(K, 1, &indx[i], 1, &indx[j], &Kmat[i][j], ADD_VALUES); CHKERRV(ierr);
	  }
	ierr = MatSetValuesLocal(M, 1, &indx[i], 1, &indx[i], &Mvec[i], ADD_VALUES); CHKERRV(ierr);
	ierr = VecSetValuesLocal(F, 1, &indx[i], &Fvec[i], ADD_VALUES);              CHKERRV(ierr);
      }

    // finish assembly
    ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = VecAssemblyBegin(F);                     CHKERRV(ierr);
    ierr = VecAssemblyEnd(F);                       CHKERRV(ierr);

    // done
    return;
  }


  void Pantograph_3::Evaluate(const Vec& U, Mat& M, Mat& K, Vec& R) const
  {
    // Sanity checks
    PetscErrorCode ierr;
    int nrows, ncols;
    ierr = MatGetSize(M, &nrows, &ncols); CHKERRV(ierr);
    assert(nrows==3 && ncols==3);
    ierr = MatGetSize(K, &nrows, &ncols); CHKERRV(ierr);
    assert(nrows==3 && ncols==3);

    double uvals[3];
    ierr = VecGetValues(U, 3, indx, uvals); CHKERRV(ierr);

    for(int i=0; i<3; ++i)
      {
	for(int j=0; j<3; ++j)
	  {
	    ierr = MatSetValuesLocal(K, 1, &indx[i], 1, &indx[j], &Kmat[i][j], ADD_VALUES); CHKERRV(ierr);
	  }
	
	ierr = MatSetValuesLocal(M, 1, &indx[i], 1, &indx[i], &Mvec[i], ADD_VALUES); CHKERRV(ierr);

	// R += KU-F
	double val = Kmat[i][0]*uvals[0] + Kmat[i][1]*uvals[1] + Kmat[i][2]*uvals[2] - Fvec[i];
	ierr = VecSetValuesLocal(R, 1, &indx[i], &val, ADD_VALUES); CHKERRV(ierr);
      }

     // finish assembly
    ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = VecAssemblyBegin(R);                     CHKERRV(ierr);
    ierr = VecAssemblyEnd(R);                       CHKERRV(ierr);
  }


  void Pantograph_3::Evaluate(const Vec& U, Mat& M, Mat& K, Mat& C, Vec& R) const
  {
    // Assemble M, K, R
    Evaluate(U, M, K, R);

    // Assemble C
    PetscErrorCode ierr;
    int nrows, ncols;
    ierr = MatGetSize(C, &nrows, &ncols); CHKERRV(ierr);
    assert(nrows==3 && ncols==3);

    // set values
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	{
	  ierr = MatSetValuesLocal(C, 1, &indx[i], 1, &indx[j], &Cmat[i][j], ADD_VALUES); CHKERRV(ierr);
	}

    // finish assembly
    ierr = MatAssemblyBegin(C, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(C,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    return;
  }
}
