
/** \file pfe_PantographParams.cpp
 * \brief Implements the struct pfe::PantographParams
 * \author Ramsharan Rangarajan
 * Last modified: July 29, 2022
 */

#include <pfe_PantographParams.h>
#include <pfe_json.hpp>
#include <cassert>
#include <iomanip>

namespace pfe
{
  // Pantograph_0_Params
  //------------------------
  
  // write to a json file
  std::ostream& operator << (std::ostream& out, const Pantograph_0_Params& params)
  {
    assert(params.tag.empty()==false);

    nlohmann::json j;
    auto& J = j[params.tag];
    J["xinit"] = params.xinit;
    J["zbase"] = params.zbase;
    J["speed"] = params.speed;
    J["force"] = params.force;
    
    out << std::endl << std::endl 
	<< "\"" << params.tag << "\"" << " : " << std::setw(8) << j[params.tag];
    return out;
  }

  // read from a json file
  std::istream& operator >> (std::istream &in, Pantograph_0_Params& params)
  {
    assert(params.tag.empty()==false);

    // rewind to the start of the file
    in.seekg(0, std::ios::beg);

    // parse
    auto J = nlohmann::json::parse(in);
    auto it = J.find(params.tag);
    assert(it!=J.end());
    auto& j = *it;

    // check fields
    assert(j.empty()==false);
    assert(j.contains("xinit") && j.contains("zbase") && j.contains("speed") && j.contains("force"));

    // read
    j["xinit"].get_to(params.xinit);
    j["zbase"].get_to(params.zbase);
    j["speed"].get_to(params.speed);
    j["force"].get_to(params.force);

    return in;
  }


  
  // Pantograph_1_Params
  //------------------------
  // write to a json file
  std::ostream& operator << (std::ostream& out, const Pantograph_1_Params& params)
  {
    assert(params.tag.empty()==false);

    nlohmann::json j;
    auto& J = j[params.tag];
    J["xinit"] = params.xinit;
    J["speed"] = params.speed;
    J["zbase"] = params.zbase;
    J["force"] = params.force;
    J["m"]     = params.m;
    J["k"]     = params.k;
    J["c"]     = params.c;
    
    out << std::endl << std::endl 
	<< "\"" << params.tag << "\"" << " : " << std::setw(8) << j[params.tag];
    return out;
  }

  // read from a json file
  std::istream& operator >> (std::istream &in, Pantograph_1_Params& params)
  {
    assert(params.tag.empty()==false);

    // rewind to the start of the file
    in.seekg(0, std::ios::beg);

    // parse
    auto J = nlohmann::json::parse(in);
    auto it = J.find(params.tag);
    assert(it!=J.end());
    auto& j = *it;
    
    // check fields
    assert(j.empty()==false);
    assert(j.contains("xinit") && j.contains("speed") && j.contains("zbase") && j.contains("force") &&
	   j.contains("m") && j.contains("k") && j.contains("c"));

    // read
    j["xinit"].get_to(params.xinit);
    j["speed"].get_to(params.speed);
    j["zbase"].get_to(params.zbase);
    j["force"].get_to(params.force);
    j["m"].get_to(params.m);
    j["k"].get_to(params.k);
    j["c"].get_to(params.c);

    return in;
  }



  // Pantograph_3_Params
  //------------------------
  
  // write to a json file
  std::ostream& operator << (std::ostream& out, const Pantograph_3_Params& params)
  {
    assert(params.tag.empty()==false);

    nlohmann::json j;
    auto& J = j[params.tag];

    J["xinit"] = params.xinit;
    J["zbase"] = params.zbase;
    J["force"] = params.force;
    J["speed"] = params.speed;
    
    // masses
    J["m1"] = params.m1;
    J["m2"] = params.m2;
    J["m3"] = params.m3;

    // stiffness
    J["k1"] = params.k1;
    J["k2"] = params.k2;
    J["k3"] = params.k3;

    // damping
    J["c1"] = params.c1;
    J["c2"] = params.c2;
    J["c3"] = params.c3;

    out << std::endl << std::endl
	<< "\"" << params.tag << "\"" << " : " << std::setw(8) << J;
    return out;
  }

  
  // read from a json file
  std::istream& operator >> (std::istream &in, Pantograph_3_Params& params)
  {
    assert(params.tag.empty()==false);

    // rewind to the start of the file
    in.seekg(0, std::ios::beg);

    // parse
    auto J = nlohmann::json::parse(in);
    auto it = J.find(params.tag);
    assert(it!=J.end());
    auto &j = *it;
    
    // check fields
    assert(j.empty()==false);
    assert(j.contains("xinit") && j.contains("zbase") &&
	   j.contains("speed") && j.contains("force") && 
	   j.contains("m1") && j.contains("m2") && j.contains("m3") &&
	   j.contains("k1") && j.contains("k2") && j.contains("k3") &&
	   j.contains("c1") && j.contains("c2") && j.contains("c3") );

    // read
    j["xinit"].get_to(params.xinit);
    j["zbase"].get_to(params.zbase);
    j["force"].get_to(params.force);
    j["speed"].get_to(params.speed);
    j["m1"].get_to(params.m1);
    j["m2"].get_to(params.m2);
    j["m3"].get_to(params.m3);
    j["k1"].get_to(params.k1);
    j["k2"].get_to(params.k2);
    j["k3"].get_to(params.k3);
    j["c1"].get_to(params.c1);
    j["c2"].get_to(params.c2);
    j["c3"].get_to(params.c3);

    return in;
  }

} // pfe::
