
/** \file pfe_Pantograph_1.cpp
 * \brief Implements the class pfe::Pantograph_1
 * \author Ramsharan Rangarajan
 */

#include <pfe_Pantograph.h>
#include <cassert>

namespace pfe
{
  // Assemble contributions to the mass, stiffness and force vector
  void Pantograph_1::Evaluate(Mat& M, Mat& K, Vec& F) const
  {
    // Sanity checks
    PetscErrorCode ierr;
    int nrows, ncols;
    ierr = MatGetSize(M, &nrows, &ncols); CHKERRV(ierr);
    assert(nrows==1 && ncols==1);
    ierr = MatGetSize(K, &nrows, &ncols); CHKERRV(ierr);
    assert(nrows==1 && ncols==1);
    
    // m, k, F
    const int indx = 0;
    ierr = MatSetValuesLocal(M, 1, &indx, 1, &indx, &params.m, ADD_VALUES); CHKERRV(ierr);
    ierr = MatSetValuesLocal(K, 1, &indx, 1, &indx, &params.k, ADD_VALUES); CHKERRV(ierr);
    ierr = VecSetValuesLocal(F, 1, &indx, &params.force, ADD_VALUES);       CHKERRV(ierr);
    ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY);                         CHKERRV(ierr);
    ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY);                         CHKERRV(ierr);
    ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY);                         CHKERRV(ierr);
    ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY);                         CHKERRV(ierr);
    ierr = VecAssemblyBegin(F);                                             CHKERRV(ierr);
    ierr = VecAssemblyEnd(F);                                               CHKERRV(ierr);

    // done
    return;
  }

  void Pantograph_1::Evaluate(const Vec& U, Mat& M, Mat& K, Vec& R) const
  {
    // Sanity checks
    PetscErrorCode ierr;
    int nrows, ncols;
    ierr = MatGetSize(M, &nrows, &ncols); CHKERRV(ierr);
    assert(nrows==1 && ncols==1);
    ierr = MatGetSize(K, &nrows, &ncols); CHKERRV(ierr);
    assert(nrows==1 && ncols==1);

    const int indx = 0;
    
    // M
    ierr = MatSetValuesLocal(M, 1, &indx, 1, &indx, &params.m, ADD_VALUES); CHKERRV(ierr);
    ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY);                         CHKERRV(ierr);
    ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY);                         CHKERRV(ierr);

    // K
    ierr = MatSetValuesLocal(K, 1, &indx, 1, &indx, &params.k, ADD_VALUES); CHKERRV(ierr);
    ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY);                         CHKERRV(ierr);
    ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY);                         CHKERRV(ierr);

    // R += KU-F
    double val;
    ierr = VecGetValues(U, 1, &indx, &val); CHKERRV(ierr);
    val *= params.k;
    val -= params.force;
    ierr = VecSetValuesLocal(R, 1, &indx, &val, ADD_VALUES); CHKERRV(ierr);
    ierr = VecAssemblyBegin(R); CHKERRV(ierr);
    ierr = VecAssemblyEnd(R);    CHKERRV(ierr);
    
  }


  void Pantograph_1::Evaluate(const Vec& U, Mat& M, Mat& K, Mat& C, Vec& R) const
  {
    // compute M, K, R
    Evaluate(U, M, K, R);

    // compute damping matrix
    PetscErrorCode ierr;
    int nrows, ncols;
    ierr = MatGetSize(C, &nrows, &ncols); CHKERRV(ierr);
    assert(nrows==1 && ncols==1);
    const int indx = 0;
    ierr = MatSetValuesLocal(C, 1, &indx, 1, &indx, &params.c, ADD_VALUES); CHKERRV(ierr);
    ierr = MatAssemblyBegin(C, MAT_FINAL_ASSEMBLY);                         CHKERRV(ierr);
    ierr = MatAssemblyEnd(C,   MAT_FINAL_ASSEMBLY);                         CHKERRV(ierr);

    return;
  }
}
