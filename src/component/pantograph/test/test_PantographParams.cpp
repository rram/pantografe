
/** \file test_PantographParams.cpp
 * \brief Unit test for read/write of parameters using the json format
 * \author Ramsharan Rangarajan
 * \comp_test Unit test for read/write of parameters using the json format
 */


#include <pfe_PantographParams.h>
#include <random>
#include <cassert>

void Generate(pfe::Pantograph_0_Params& params);

void Generate(pfe::Pantograph_1_Params& params);

void Generate(pfe::Pantograph_3_Params& params);

void Compare(const pfe::Pantograph_0_Params& A,
	     const pfe::Pantograph_0_Params& B,
	     const double tol);

void Compare(const pfe::Pantograph_1_Params& A,
	     const pfe::Pantograph_1_Params& B,
	     const double tol);

void Compare(const pfe::Pantograph_3_Params& A,
	     const pfe::Pantograph_3_Params& B,
	     const double tol);

int main()
{
  std::fstream jfile;
  jfile.open((char*)"datasheet.json", std::ios::out);
  
  // Write 
  jfile << "{";
  // delta pantograph
  pfe::Pantograph_0_Params deltaPantoParams;
  deltaPantoParams.tag = "delta pantograph";
  Generate(deltaPantoParams);
  jfile << deltaPantoParams << ",";
  
  // simple pantograph
  pfe::Pantograph_1_Params simplePantoParams;
  simplePantoParams.tag = "simple pantograph";
  Generate(simplePantoParams);
  jfile << simplePantoParams << ",";
  
  // pantograph
  pfe::Pantograph_3_Params pantoParams;
  pantoParams.tag = "pantograph";
  Generate(pantoParams);
  jfile << pantoParams;
  jfile << "}";
  jfile.close();

  // Read & compare
  jfile.open((char*)"datasheet.json", std::ios::in);

  pfe::Pantograph_0_Params delta_panto_copy;
  delta_panto_copy.tag = "delta pantograph";
  jfile >> delta_panto_copy;
  Compare(deltaPantoParams, delta_panto_copy, 1.e-6);
  
  pfe::Pantograph_1_Params simple_panto_copy;
  simple_panto_copy.tag = "simple pantograph";
  jfile >> simple_panto_copy;
  Compare(simplePantoParams, simple_panto_copy, 1.e-6);
  
  pfe::Pantograph_3_Params panto_copy;
  panto_copy.tag = "pantograph";
  jfile >> panto_copy;
  Compare(pantoParams, panto_copy, 1.e-6);
  
  jfile.close();
}



void Generate(pfe::Pantograph_0_Params& params)
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(1.0, 100.0);
  params.xinit = dis(gen);
  params.zbase = dis(gen);
  params.speed = dis(gen);
  params.force = dis(gen);
  return;
}



void Generate(pfe::Pantograph_1_Params& params)
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(1.0, 100.0);
  params.xinit = dis(gen);
  params.speed = dis(gen);
  params.zbase = dis(gen);
  params.force = dis(gen);
  params.m = dis(gen);
  params.k = dis(gen);
  params.c = dis(gen);
  return;
}


void Generate(pfe::Pantograph_3_Params& params)
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(1.0, 100.0);

  params.xinit = dis(gen);
  params.speed = dis(gen);

  params.m1 = dis(gen);
  params.m2 = dis(gen);
  params.m3 = dis(gen);
  
  params.k1 = dis(gen);
  params.k2 = dis(gen);
  params.k3 = dis(gen);

  params.c1 = dis(gen);
  params.c2 = dis(gen);
  params.c3 = dis(gen);

  return;
}



void Compare(const pfe::Pantograph_0_Params& A,
	     const pfe::Pantograph_0_Params& B,
	     const double tol)
{
  assert(A.tag.empty()==false && B.tag.empty()==false);
  assert(A.tag==B.tag);
  assert(std::abs(A.xinit-B.xinit)<tol);
  assert(std::abs(A.zbase-B.zbase)<tol);
  assert(std::abs(A.speed-B.speed)<tol);
  assert(std::abs(A.force-B.force)<tol);
  
  return;
}


void Compare(const pfe::Pantograph_1_Params& A,
	     const pfe::Pantograph_1_Params& B,
	     const double tol)
{
  assert(A.tag.empty()==false && B.tag.empty()==false);
  assert(A.tag==B.tag);
  assert(std::abs(A.xinit-B.xinit)<tol);
  assert(std::abs(A.speed-B.speed)<tol);
  assert(std::abs(A.zbase-B.zbase)<tol);
  assert(std::abs(A.force-B.force)<tol);
  assert(std::abs(A.m-B.m)<tol);
  assert(std::abs(A.k-B.k)<tol);
  assert(std::abs(A.c-B.c)<tol);
  
  return;
}


void Compare(const pfe::Pantograph_3_Params& A,
	     const pfe::Pantograph_3_Params& B,
	     const double tol)
{
  assert(A.tag.empty()==false && B.tag.empty()==false);
  assert(A.tag==B.tag);

  assert(std::abs(A.xinit-B.xinit)<tol);
  assert(std::abs(A.speed-B.speed)<tol);
  
  assert(std::abs(A.m1-B.m1)<tol);
  assert(std::abs(A.m2-B.m2)<tol);
  assert(std::abs(A.m3-B.m3)<tol);
  
  assert(std::abs(A.k1-B.k1)<tol);
  assert(std::abs(A.k2-B.k2)<tol);
  assert(std::abs(A.k3-B.k3)<tol);

  assert(std::abs(A.c1-B.c1)<tol);
  assert(std::abs(A.c2-B.c2)<tol);
  assert(std::abs(A.c3-B.c3)<tol);

  return;
}


