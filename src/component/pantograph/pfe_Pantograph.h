
/** \file pfe_Pantograph.h
 * \brief Defines the class pfe::Pantograph
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_PantographParams.h>
#include <petscvec.h>
#include <petscmat.h>
#include <vector>
#include <array>

namespace pfe
{
  //! Abstract base class for a pantograph
  //! \ingroup components
  class Pantograph
  {
  public:
    //! Constructor
    inline Pantograph() {}

    //! Destructor
    inline virtual ~Pantograph() {}

    // Disable copy and assignment
    Pantograph(const Pantograph&) = delete;

    //! position of the panto
    virtual double GetPosition(const double time) const = 0;

    //! carriage height of the panto
    virtual double GetBaseHeight() const = 0;
    
    //! Number of dofs for the pantograph
    virtual int GetTotalNumDof() const = 0;

    //! Returns the sparsity
    virtual std::vector<int> GetSparsity() const = 0;

    //! Returns the local # of the dof coupled to the OHE
    virtual int GetCoupledDofNum() const = 0;
    
    //! Assemble contributions to the mass, stiffness, force vector
    //! Matrices should have size equal to numdofs
    virtual void Evaluate(Mat& M, Mat& K, Vec& F) const = 0;
    virtual void Evaluate(const Vec& U, Mat& M, Mat& K, Vec& R) const = 0;
    virtual void Evaluate(const Vec& U, Mat& M, Mat& K, Mat& C, Vec& R) const = 0;

    //! Write configuration to file
    void Write(const double* disp, const double* vel,
	       const double* accn, const std::string fname) const;
  };

  
  //! 1-dof pantograph
  //! \ingroup components
  class Pantograph_1: public Pantograph
  {
  public:
    //! Constructor
    inline Pantograph_1(const Pantograph_1_Params& p)
      :params(p) {}
    
    //! Destructor
    inline virtual ~Pantograph_1() {}

    //! Disable copy and assignment
    Pantograph_1(const Pantograph_1&) = delete;
    Pantograph_1 operator=(const Pantograph_1&) = delete;

    //! position of the panto
    inline double GetPosition(const double time) const override
    { return params.xinit + time*params.speed; }

    //! carriage height of the panto
    inline double GetBaseHeight() const override
    { return params.zbase; }
    
    //! Number of dofs
    inline virtual int GetTotalNumDof() const override
    { return 1; }

    //! Returns the sparsity
    inline std::vector<int> GetSparsity() const override
    { return {1}; }

    //! Returns the local # of the dof coupled to the OHE
    inline int GetCoupledDofNum() const override
    { return 0; }
    
    //! Assemble contributions to the mass, stiffness and force vector
    virtual void Evaluate(Mat& M, Mat& K, Vec& F) const override;

    //! Assemble contributions to the mass, stiffness, damping and residual vector
    virtual void Evaluate(const Vec& U, Mat& M, Mat& K, Vec& R) const override;
    virtual void Evaluate(const Vec& U, Mat& M, Mat& K, Mat& C, Vec& R) const override;

    //! Reset the pantograph parameters
    inline void SetPantographParams(const Pantograph_1_Params& p)
    { params = p; }
    
  private:
    Pantograph_1_Params params;
  };
  

  //! 3-dof pantograph
  //! \ingroup components
  class Pantograph_3: public Pantograph
  {
  public:
    //! Constructor
    Pantograph_3(const Pantograph_3_Params& p);

    //! Destructor
    inline virtual ~Pantograph_3() {}

    //! Disable copy and assignment
    Pantograph_3(const Pantograph_3&) = delete;
    Pantograph_3 operator=(const Pantograph_3&) = delete;

    //! position of the panto
    inline double GetPosition(const double time) const override
    { return params.xinit + time*params.speed; }

    //! carriage height of the panto
    inline double GetBaseHeight() const override
    { return params.zbase; }
    
    //! Number of dofs
    inline virtual int GetTotalNumDof() const override
    { return 3; }

     //! Returns the sparsity
    inline std::vector<int> GetSparsity() const override
    { return {3,3,3}; }

    //! Returns the local # of the dof coupled to the OHE
    // carriage -> m1 -> m2 -> m3 -> ohe
    inline int GetCoupledDofNum() const override
    { return 2; }
    
    //! Assemble contributions to the mass, stiffness and force vector
    virtual void Evaluate(Mat& M, Mat& K, Vec& F) const override;
    virtual void Evaluate(const Vec& U, Mat& M, Mat& K, Vec& R) const override;
    virtual void Evaluate(const Vec& U, Mat& M, Mat& K, Mat& C, Vec& R) const override;
    
  private:
    const Pantograph_3_Params params;
    double Mvec[3];
    double Kmat[3][3];
    double Cmat[3][3];
    double Fvec[3];
    int    indx[3];
  };
  
 
}


