
/** \file pfe_PantographParams.h
 * \brief Defines the struct pfe::PantographParams
 * \author Ramsharan Rangarajan
 * Last modified: July 29, 2022
 */

#pragma once

#include <fstream>

namespace pfe
{
  struct Pantograph_0_Params
  {
    std::string tag;
    double xinit, zbase, speed, force;

    friend std::ostream& operator << (std::ostream& out, const Pantograph_0_Params& params);
    friend std::istream& operator >> (std::istream& in,  Pantograph_0_Params& params);
  };
  
  struct Pantograph_1_Params
  {
    std::string tag;
    double xinit, speed, zbase, force;
    double m, k, c;
    
    friend std::ostream& operator << (std::ostream& out, const Pantograph_1_Params& params);
    friend std::istream& operator >> (std::istream& in, Pantograph_1_Params& params);
  };
  
  struct Pantograph_3_Params
  {
    std::string tag;
    double xinit;
    double zbase;
    double force;
    double speed;
    double m1, k1, c1;
    double m2, k2, c2;
    double m3, k3, c3;

    friend std::ostream& operator << (std::ostream& out, const Pantograph_3_Params& params);

    friend std::istream& operator >> (std::istream& in, Pantograph_3_Params& params);
  };
}
