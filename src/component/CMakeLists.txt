add_subdirectory(suspension_spring)
add_subdirectory(contact_spring)
add_subdirectory(beam)
add_subdirectory(dropper)
add_subdirectory(pantograph)
add_subdirectory(ohe)

add_library(pfe_component INTERFACE)
target_link_libraries(pfe_component INTERFACE
   		              pfe_linear_ohe
			      pfe_nonlinear_ohe
			      pfe_pantograph
    			      pfe_ohe_base
			      pfe_dropper
			      pfe_susp_spring
			      pfe_contact_spring
			      pfe_beam)
