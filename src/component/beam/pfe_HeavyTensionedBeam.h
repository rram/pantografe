
/** \file pfe_HeavyTensionedBeam.h
 * \brief Defines the class pfe::HeavyTensionedBeam
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_HeavyTensionedBeamParams.h>
#include <pfe_Beam_BendingStiffness.h>
#include <pfe_Beam_MassMatrix.h>
#include <pfe_TensionForce.h>
#include <pfe_GravityForce.h>
#include <pfe_SuspensionSpring.h>
#include <pfe_H31DElement.h>
#include <pfe_Assembler.h>
#include <pfe_PetscData.h>
#include <pfe_LinearSolver.h>
#include <array>

namespace pfe
{
  //! Class for a heavy pre-tensioned
  //! \ingroup components
  class HeavyTensionedBeam
  {
  public:

    //! Constructor
    HeavyTensionedBeam(const HeavyTensionedBeamParams& beam_params, const std::vector<double>& coords);

    //! Constructor
    HeavyTensionedBeam(const HeavyTensionedBeamParams& beam_params, const int nelm);
    
    //! Destructor
    virtual ~HeavyTensionedBeam();

    //! Disable copy and assignment
    HeavyTensionedBeam(const HeavyTensionedBeam&) = delete;
    HeavyTensionedBeam& operator=(const HeavyTensionedBeam&) = delete;

    //! Destroy
    virtual void Destroy();
    
    //! Add a suspension spring
    void AddSuspensionSpring(const SuspensionSpringParams& spring_params, const int dofnum);

    //! Get the nonzero data structure
    virtual std::vector<int> GetSparsity() const;

    //! returns this span
    const HeavyTensionedBeam& GetSpan(const int span_num) const
    { return *this; }

    //! returns the datum level
    inline double GetDatum() const
    { return params.zLevel; }
      
    //! Evaluate the residual, stiffness and mass matrix
    //! Assumes that the sparsity patterns have been set
    virtual void Evaluate(Mat& M, Mat& K, Vec& F) const;
    inline virtual void Evaluate(const double time, const Vec& U, Mat &M, Mat&K, Vec& F) const
    { Evaluate(M, K, F); }
    
    //! Main functionality, computes static equilibrium
    void StaticSolve(const std::vector<std::pair<int,double>>& nodalDisp, double* sol) const;
    
    //! Access local to global map
    inline const LocalToGlobalMap& GetLocalToGlobalMap() const
    { assert(isDestroyed==false);
      return *L2GMap; }

    //! Get the number of dofs
    inline int GetTotalNumDof() const
    { assert(isDestroyed==false);
      return L2GMap->GetTotalNumDof(); }

    //! Access coordinates
    inline const std::vector<double>& GetCoordinates() const
    { return coordinates; }
    
    //! Access parameters
    inline const HeavyTensionedBeamParams& GetParameters() const
    { return params; }

    //! Access element array
    inline const std::vector<Element*>& GetElementArray() const
    { assert(isDestroyed==false);
      return ElmArray; }

    //! Access the number of nodes
    inline int GetNumNodes() const
    { return nNodes; }

    //! Get the active displacement dofs and shape function values at a given point along the span
    std::array<std::pair<int,double>,2> GetActiveDisplacementDofs(const double x) const;

    //! Get the active dofs and shape functions at a given point along the span
    std::array<std::pair<int,double>,4> GetActiveDofs(const double x) const;

    //! Dofs at the start/end of the span
    std::pair<int,int> GetStartDofs() const;
    std::pair<int,int> GetEndDofs() const;
    
    //! Write to file
    void Write(const double* displacements, const std::string filename) const;

    void Write(const double* disp, const double* vel, const double* accn, const std::string filename) const;

    inline void Write(const double* disp, const double* vel, const double* accn, const std::vector<std::string> filename) const
    {
      assert(static_cast<int>(filename.size())==1);
      Write(disp, vel, accn, filename[0]);
    }
    
  private:

    //! Create the object
    void Create();

    bool isDestroyed;
    const HeavyTensionedBeamParams params;
    const int                      nElements;
    const int                      nNodes;
    std::vector<double>            coordinates;
    std::vector<HermiteShape*>     shapeFuncs;
    std::vector<Element*>          ElmArray;
    LocalToGlobalMap              *L2GMap;
    
    std::vector<Beam_BendingStiffness*> EIOpsArray;
    std::vector<Beam_MassMatrix*>       MassOpsArray;
    std::vector<TensionForce*>          TOpsArray;
    std::vector<GravityForce*>          gOpsArray;
    
    Assembler<Beam_BendingStiffness>   *Asm_EI;
    Assembler<Beam_MassMatrix>         *Asm_M;
    Assembler<TensionForce>            *Asm_T;
    Assembler<GravityForce>            *Asm_g;
    
    std::vector<SuspensionSpring*> suspension_springs; //!< Suspension springs
    std::vector<double> zeroVec;
  };

  using SingleSpan_Cable = HeavyTensionedBeam;
}

