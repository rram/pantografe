
/** \file pfe_HeavyTensionedBeamParams.h
 * \brief Implements the struct pfe::HeavyTensionedBeamParams
 * \author Ramsharan Rangarajan
 */

#include <pfe_HeavyTensionedBeamParams.h>
#include <pfe_json.hpp>

namespace pfe
{
  // write parameters to json file
  std::ostream& operator << (std::ostream &out, const HeavyTensionedBeamParams& params)
  {
    assert(params.tag.empty()==false);

    nlohmann::json j;
    j[params.tag]["span"] = params.span;
    j[params.tag]["EI"] = params.EI;
    j[params.tag]["tension"] = params.Tension;
    j[params.tag]["line density"] = params.rhoA;
    j[params.tag]["gravity"] = params.g;
    j[params.tag]["height"] = params.zLevel;
    out << std::endl << std::endl <<"\""<<params.tag<<"\""<<": " << std::setw(8)<<j[params.tag];
    return out;
  }


  // read parameters from json file
  std::istream& operator >> (std::istream &in, HeavyTensionedBeamParams &params)
  {
    assert(params.tag.empty()==false);
    
    // rewind the file
    in.seekg(0, std::ios::beg);

    // parse
    auto J = nlohmann::json::parse(in);

    // read option
    auto it = J.find(params.tag);
    assert(it!=J.end());
    auto& j = *it;

    // check fields
    assert(j.empty()==false);
    assert(j.contains("span"));
    assert(j.contains("EI"));
    assert(j.contains("tension"));
    assert(j.contains("line density"));
    assert(j.contains("gravity"));
    assert(j.contains("height"));

    // read. use get_to() to enforce type
    j["line density"].get_to(params.rhoA);
    j["gravity"].get_to(params.g);
    j["span"].get_to(params.span);
    j["EI"].get_to(params.EI);
    j["tension"].get_to(params.Tension);
    j["height"].get_to(params.zLevel);

    return in;
  }
  
}
