
/** \file pfe_HeavyTensionedBeamParams.h
 * \brief Defines the struct pfe::HeavyTensionedBeamParams
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <string>
#include <fstream>

namespace pfe
{
  //! \ingroup components
  struct HeavyTensionedBeamParams
  {
    std::string tag;
    double span;
    double EI;
    double Tension;
    double rhoA;
    double g; 
    double zLevel;

    // print to json file
    friend std::ostream& operator << (std::ostream &out, const HeavyTensionedBeamParams& params);

    // read from json file
    friend std::istream& operator >> (std::istream& in, HeavyTensionedBeamParams& params);
  
  };

  using ContactWireParams = HeavyTensionedBeamParams;
  using CatenaryWireParams = HeavyTensionedBeamParams;

}
