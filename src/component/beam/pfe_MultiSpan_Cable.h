/** \file pfe_MultiSpan_Cable.h
 * \brief Defines the class pfe::MultiSpan_Cable encapsulating multiple span cable
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_HeavyTensionedBeam.h>

namespace pfe
{
  //! Multiple cable spans
  //! \ingroup components
  class MultiSpan_Cable
  {
  public:
    //! Constructor
    MultiSpan_Cable(const double x0,
		    const HeavyTensionedBeamParams& cable_params,
		    const SuspensionSpringParams&   spring_params,
		    const int nspans, const int nelm);

    //! Destructor
    virtual ~MultiSpan_Cable();

    //! Disable copy and assignment
    MultiSpan_Cable(const MultiSpan_Cable&) = delete;
    MultiSpan_Cable operator=(const MultiSpan_Cable&) = delete;

    //! Destroy
    void Destroy();
      
    //! Access a specified span
    inline const HeavyTensionedBeam& GetSpan(const int num) const
    {
      assert(isDestroyed==false);
      assert(num>=0 && num<num_spans);
      return *cable_spans[num];
    }
    
    //! Access the total number of dofs
    inline int GetTotalNumDof() const
    { return nTotalDof; }

    //! Access the number of spans
    inline int GetNumSpans() const
    { return num_spans; }

    //! Main functionality: evaluate mass, stiffness and force
    void Evaluate(Mat& M, Mat& K, Vec& F) const;

    //! Get the active dosplacement dofs and shape functions at a given point along the span
    std::array<std::pair<int,double>,2> GetActiveDisplacementDofs(const double x) const;

    //! Get the active dofs and shape functions at a given point along the span
    std::array<std::pair<int,double>,4> GetActiveDofs(const double x) const;

    //! Number of nonzero's per row
    std::vector<int> GetSparsity() const;

    //! Visualize the configuration
    void Write(const double* displacements, const std::string filename) const;

    void Write(const double* disp, const double* vel, const double* accn, const std::string filename) const;

    inline void Write(const double* disp, const double* vel, const double* accn, const std::vector<std::string> filename) const
    {
      assert(static_cast<int>(filename.size())==1);
      Write(disp, vel, accn, filename[0]);
    }

  private:
    bool isDestroyed;
    const double xinit;                             //! start of the first span
    const int    num_spans;                         //! number of spans
    const double span_length;                       //! length of each span
    std::vector<HeavyTensionedBeam*> cable_spans;   //! vector of cable spans
    std::vector<IS>                  cable_IS;      //! vector of index sets for each cable
    std::vector<std::vector<int>>    span_dof_maps; //! span-wise local to global dof map
    int                              nTotalDof;     //! total number of dofs
  };

}
