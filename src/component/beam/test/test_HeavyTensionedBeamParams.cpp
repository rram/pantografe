
/** \file test_HeavyTensionedBeamParams.cpp
 * \brief Unit test for read/write of parameters using the json format
 * \author Ramsharan Rangarajan
 * \comp_test Unit test for read/write of parameters using the json format
 
 */

#include <pfe_HeavyTensionedBeamParams.h>
#include <random>
#include <cassert>

void Generate(pfe::HeavyTensionedBeamParams& params);


void Compare(const pfe::HeavyTensionedBeamParams& A,
	     const pfe::HeavyTensionedBeamParams& B,
	     const double tol);

int main()
{
  std::fstream jfile;
  jfile.open((char*)"datasheet.json", std::ios::out);
  jfile << "{" << std::endl;
  // Write 
  // Catenary cable
  pfe::HeavyTensionedBeamParams catenary_params;
  catenary_params.tag = "catenary cable";
  Generate(catenary_params);
  jfile << catenary_params << "," << std::endl;

  // contact cable
  pfe::HeavyTensionedBeamParams contact_params;
  contact_params.tag = "contact wire";
  Generate(contact_params);
  jfile << contact_params << std::endl;
  jfile << "}" << std::endl;
  jfile.close();

  // Read & compare
  jfile.open((char*)"datasheet.json", std::ios::in);
    
  pfe::HeavyTensionedBeamParams copy_params;
  copy_params.tag = "contact wire";
  jfile >> copy_params;
  Compare(contact_params, copy_params, 1.e-6);

  copy_params.tag = "catenary cable";
  jfile >> copy_params;
  Compare(catenary_params, copy_params, 1.e-6);
 
  jfile.close();
}


void Generate(pfe::HeavyTensionedBeamParams& params)
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(1.0, 100.0);
  params.span = dis(gen);
  params.EI = dis(gen);
  params.Tension = dis(gen);
  params.rhoA = dis(gen);
  params.g = dis(gen);
  params.zLevel = dis(gen);
  return;
}


void Compare(const pfe::HeavyTensionedBeamParams& A,
	     const pfe::HeavyTensionedBeamParams& B,
	     const double tol)
{
  assert(A.tag==B.tag);
  assert(std::abs(A.span-B.span)<tol);
  assert(std::abs(A.zLevel-B.zLevel)<tol);
  assert(std::abs(A.EI-B.EI)<tol);
  assert(std::abs(A.Tension-B.Tension)<tol);
  assert(std::abs(A.zLevel-B.zLevel)<tol);
  assert(std::abs(A.rhoA-B.rhoA)<tol);
  assert(std::abs(A.g-B.g)<tol);
  return;
}
