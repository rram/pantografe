
/** \file test_HeavyTensionedBeam.cpp
 * \brief Unit test for the component pfe::HeavyTensionedBeam
 * \author Ramsharan Rangarajan
 * \comp_test Unit test for the component pfe::HeavyTensionedBeam
 */

#include <pfe_HeavyTensionedBeam.h>
#include <fstream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Create contact wire model
  pfe::ContactWireParams params{.span=55., .EI=195., .Tension=22000., .rhoA=1.35, .g=9.81, .zLevel=1.};
  const int nElements = 100;
  pfe::HeavyTensionedBeam cwire(params, nElements); 

  // Add a suspension spring at the left end
  pfe::SuspensionSpringParams spring_params{.stiffness=2000.,.mass=0.94, .g=9.81};
  cwire.AddSuspensionSpring(spring_params, 0);
  
  // displacement field
  const int nTotalDofs = cwire.GetLocalToGlobalMap().GetTotalNumDof();
  std::vector<double> displacements(nTotalDofs);
  
  // Dirichlet bc at the right end
  const int nNodes = nElements+1;
  std::vector<std::pair<int,double>> dirichlet_bcs{};
  dirichlet_bcs.push_back( std::make_pair(nNodes-1, 0.) );
  
  // Solve
  cwire.StaticSolve(dirichlet_bcs, displacements.data());

  // Print solution to file
  cwire.Write(displacements.data(), "sol.dat");
  
  // Clean up
  cwire.Destroy();
  PetscFinalize();
}
