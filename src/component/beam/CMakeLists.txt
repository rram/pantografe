
# add library
add_library(pfe_beam STATIC
  pfe_HeavyTensionedBeamParams.cpp
  pfe_HeavyTensionedBeam.cpp
  pfe_MultiSpan_Cable.cpp)

# headers
target_include_directories(pfe_beam PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)

# link
target_link_libraries(pfe_beam PUBLIC pfe_susp_spring)

# Add required flags
target_compile_features(pfe_beam PUBLIC ${pfe_COMPILE_FEATURES})

install(FILES
	pfe_HeavyTensionedBeam.h
	pfe_HeavyTensionedBeamParams.h
	pfe_MultiSpan_Cable.h
        DESTINATION ${PROJECT_NAME}/include)
	  
