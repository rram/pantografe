
/** \file pfe_HeavyTensionedBeam.cpp
 * \brief Implements the class pfe::HeavyTensionedBeam
 * \author Ramsharan Rangarajan
 */

#include <pfe_HeavyTensionedBeam.h>
#include <cassert>
#include <fstream>
#include <iostream>

namespace pfe
{
  // Constructor
  HeavyTensionedBeam::HeavyTensionedBeam(const HeavyTensionedBeamParams& beam_params,
					 const std::vector<double>& coords)
    :isDestroyed(false),
     params(beam_params),
     nElements(coords.size()-1),
     nNodes(nElements+1),
     coordinates(coords),
     suspension_springs({})
  {
    // Sanity checks 
    assert(std::abs(1.-(coords.back()-coords.front())/beam_params.span)<1.e-4);
    for(int n=1; n<nNodes; ++n)
      assert(coords[n]>coords[n-1]);
    
    // Create
    Create();
  }
   
  // Constructor
  HeavyTensionedBeam::HeavyTensionedBeam(const HeavyTensionedBeamParams& beam_params,
					 const int nelm)
    :isDestroyed(false),
     params(beam_params),
     nElements(nelm),
     nNodes(nElements+1),
     coordinates(nNodes),
     suspension_springs({})
  {
    // Set coordinates
    const double dx = params.span/static_cast<double>(nElements);
    for(int a=0; a<nNodes; ++a)
      coordinates[a] = static_cast<double>(a)*dx;
    
    // Create
    Create();
  }

  // Create
  void HeavyTensionedBeam::Create()
  {
    // Set coordinates
    SegmentGeometry<1>::SetGlobalCoordinatesArray(coordinates);

    // Create shape functions
    shapeFuncs.resize(nElements);
    for(int e=0; e<nElements; ++e)
      shapeFuncs[e] = new HermiteShape(coordinates[e], coordinates[e+1]);
    
    // Create elements
    assert(nElements>0);
    ElmArray.resize(nElements);
    for(int e=0; e<nElements; ++e)
      ElmArray[e] = new H31DElement<1>(e+1, e+2);

    // Local to global map
    L2GMap = new H31DL2GMap(ElmArray);

    // Operations
    EIOpsArray.resize(nElements);
    MassOpsArray.resize(nElements);
    TOpsArray.resize(nElements);
    gOpsArray.resize(nElements);
    for(int e=0; e<nElements; ++e)
      {
	EIOpsArray[e] = new Beam_BendingStiffness(e, ElmArray[e], params.EI);
	MassOpsArray[e] = new Beam_MassMatrix(e, ElmArray[e], params.rhoA);
	TOpsArray[e]  = new TensionForce(e, ElmArray[e], params.Tension);
	gOpsArray[e]  = new GravityForce(e, ElmArray[e], -params.rhoA*params.g);
      }

    // Assemblers
    Asm_EI = new Assembler<Beam_BendingStiffness>(EIOpsArray, *L2GMap);
    Asm_M  = new Assembler<Beam_MassMatrix>(MassOpsArray, *L2GMap); 
    Asm_T = new Assembler<TensionForce>(TOpsArray, *L2GMap);
    Asm_g = new Assembler<GravityForce>(gOpsArray, *L2GMap);

    // zero vector
    zeroVec.resize(L2GMap->GetTotalNumDof());
    std::fill(zeroVec.begin(), zeroVec.end(), 0.);
    
    // done
    return;
  }

  // Detsroy
  void HeavyTensionedBeam::Destroy()
  {
    assert(isDestroyed==false);
    delete L2GMap;
    for(auto& s:shapeFuncs)    delete s;
    for(auto& e:ElmArray)      delete e;
    for(auto& op:EIOpsArray)   delete op;
    for(auto& op:MassOpsArray) delete op;
    for(auto& op:TOpsArray)    delete op;
    for(auto& op:gOpsArray)    delete op;
    delete Asm_EI;
    delete Asm_M;
    delete Asm_T;
    delete Asm_g;
    for(auto& s:suspension_springs) delete s;
    isDestroyed = true;
  }
  
  // Destructor
  HeavyTensionedBeam::~HeavyTensionedBeam()
  {      
    assert(isDestroyed==true);
  }

  
  // Add suspension springs
  void HeavyTensionedBeam::
  AddSuspensionSpring(const SuspensionSpringParams& spring_params, const int dofnum)
  {
    assert(isDestroyed==false);
    const int ndof = L2GMap->GetTotalNumDof();
    assert(dofnum>=0 && dofnum<ndof);
    suspension_springs.push_back( new SuspensionSpring(spring_params, dofnum) );
  }

  // Evaluate the residual and stiffness
  void HeavyTensionedBeam::Evaluate(Mat& M, Mat& K, Vec& F) const
  {
    assert(isDestroyed==false);
    
    // Configuration at which to assemble
    TransverseDisplacementField config;
    config.L2GMap = L2GMap;
    config.displacement = &zeroVec[0];

    // Assemble mass matrix
    if(M!=nullptr)
      Asm_M->Assemble(&config, M);
    
    // Assemble stiffness
    if(K!=nullptr)
      {
	Asm_EI->Assemble(&config, K);
	Asm_T->Assemble(&config, K);
      }

    // Assemble residual
    if(F!=nullptr)
      Asm_g->Assemble(&config, F);

    // Add contributions for suspension springs
    for(auto& spring:suspension_springs)
      spring->Evaluate(M, K, F);

    // done
    return;
  }
  
  
  // Get the nonzero data structure
  std::vector<int> HeavyTensionedBeam::GetSparsity() const
  {
    assert(isDestroyed==false);
    std::vector<int> nnz{};
    Asm_EI->CountNonzeros(nnz);
    return std::move(nnz);
  }
    
    
  // Main functionality: solve
  void HeavyTensionedBeam::StaticSolve(const std::vector<std::pair<int,double>>& nodalDisp, double* sol) const
  {
    assert(isDestroyed==false);
      
    // Create PETSc data structure
    PetscData PD;
    const int nTotalDof = L2GMap->GetTotalNumDof();
    std::vector<int> nz{};
    Asm_EI->CountNonzeros(nz);
    PD.Initialize(nz);
    
    // Create linear sovler
    LinearSolver linSolver;
    
    // Assemble the stiffness and residual
    PetscErrorCode ierr;
    ierr = MatZeroEntries(PD.stiffnessMAT); CHKERRV(ierr);
    ierr = VecZeroEntries(PD.resVEC);       CHKERRV(ierr);
    Evaluate(PD.massMAT, PD.stiffnessMAT, PD.resVEC);
    
    // Set dirichlet bcs in the matrix-vector system
    std::vector<int> boundary{};
    std::vector<double> bvalues{};
    for(auto& nd:nodalDisp)
      {
	boundary.push_back( nd.first );
	bvalues.push_back( nd.second );
      }
    PD.SetDirichletBCs(boundary, bvalues);

    // Solve
    linSolver.SetOperator(PD.stiffnessMAT);
    linSolver.Solve(PD.resVEC, PD.solutionVEC);

    // Get the solution
    double* solution;
    ierr = VecGetArray(PD.solutionVEC, &solution); CHKERRV(ierr);
    for(int a=0; a<nTotalDof; ++a)
      sol[a] = solution[a];
    ierr = VecRestoreArray(PD.solutionVEC, &solution); CHKERRV(ierr);

    // clean up
    PD.Destroy();
    linSolver.Destroy();
    
    // done
    return;
  }


  // Get the active displacement dofs and shape function values at a given point along the span
  std::array<std::pair<int,double>,2> HeavyTensionedBeam::GetActiveDisplacementDofs(const double x) const
  {
    assert(isDestroyed==false);
      
    // TODO: use a better algorithm for locating x in an interval
    // =========================================================
    
    // locate the element, using 0.1% of h as the tolerance 
    int elm = -1;
    double htol;
    for(int e=0; e<nElements && elm==-1; ++e)
      {
	htol = 1.e-3*(coordinates[e+1]-coordinates[e]);
	if(x>=coordinates[e]-htol && x<=coordinates[e+1]+htol)
	  elm = e;
      }
    assert(elm!=-1);

    // barycentric coordinate & shape function
    double lambda   = (coordinates[elm+1]-x)/(coordinates[elm+1]-coordinates[elm]);
    const auto& shp = *shapeFuncs[elm];
    
    return {std::make_pair(L2GMap->Map(0,0,elm),   shp.Val(0,&lambda)),
	std::make_pair(L2GMap->Map(0,1,elm), shp.Val(1,&lambda))};
  }



  // Get the active dofs and shape functions at a given point along the span
  std::array<std::pair<int,double>,4> HeavyTensionedBeam::GetActiveDofs(const double x) const
  {
    assert(isDestroyed==false);
      
    // TODO: use a better algorithm for locating x in an interval
    // =========================================================
    
    // locate the element
    int elm = -1;
    double htol;
    for(int e=0; e<nElements && elm==-1; ++e)
      {
	htol = 1.e-3*(coordinates[e+1]-coordinates[e]);
	if(x>=coordinates[e]-htol && x<=coordinates[e+1]+htol)
	  elm = e;
      }
    assert(elm!=-1);

    // barycentric coordinate & shape function
    double lambda   = (coordinates[elm+1]-x)/(coordinates[elm+1]-coordinates[elm]);
    const auto& shp = *shapeFuncs[elm];
    
    return {std::make_pair(L2GMap->Map(0,0,elm),   shp.Val(0,&lambda)),
	std::make_pair(L2GMap->Map(0,1,elm), shp.Val(1,&lambda)),
	std::make_pair(L2GMap->Map(0,2,elm), shp.Val(2,&lambda)),
	std::make_pair(L2GMap->Map(0,3,elm), shp.Val(3,&lambda))};
  }

  //! Dofs at the start/end of the span
  std::pair<int,int> HeavyTensionedBeam::GetStartDofs() const
  {
    assert(isDestroyed==false);
    return {L2GMap->Map(0,0,0), L2GMap->Map(0,2,0)};  // disp and derivative dofs at the left end
  }
  
  std::pair<int,int> HeavyTensionedBeam::GetEndDofs() const
  {
    assert(isDestroyed==false);
    return {L2GMap->Map(0,1,nElements-1), L2GMap->Map(0,3,nElements-1)}; // disp and derivative dofs at the right end
  }

  
  // Write to file
  void HeavyTensionedBeam::Write(const double* displacements, const std::string filename) const
  {
    assert(isDestroyed==false);
    
    std::fstream pfile;
    pfile.open(filename.c_str(), std::ios::app|std::ios::out);
    assert(pfile.good());
    for(int n=0; n<nNodes; ++n)
      pfile << coordinates[n] <<" "<< params.zLevel + displacements[n] << std::endl;
    pfile.close();
    return;
  }
  
  void HeavyTensionedBeam::
  Write(const double* disp, const double* vel, const double* accn, const std::string filename) const
  {
    assert(isDestroyed==false);
    
    std::fstream pfile;
    pfile.open(filename.c_str(), std::ios::app|std::ios::out);
    assert(pfile.good());
    for(int n=0; n<nNodes; ++n)
      pfile << coordinates[n] <<" "<< params.zLevel + disp[n] << " " << vel[n] <<" "<< accn[n] << std::endl;
    pfile.close();
    return;
  }
}
