
/** \file pfe_MultiSpan_Cable.cpp
 * \brief Implements the class pfe::MultiSpan_Cable
 * \author Ramsharan Rangarajan
 * Last modified: February 24, 2023
 */

#include <pfe_MultiSpan_Cable.h>
#include <fstream>
#include <map>

namespace pfe
{
  // Constructor
  MultiSpan_Cable::MultiSpan_Cable(const double x0,
				   const HeavyTensionedBeamParams& cable_params,
				   const SuspensionSpringParams&   spring_params,
				   const int nspans, const int nelms_per_span)
    :isDestroyed(false),
     xinit(x0),
     num_spans(nspans),
     span_length(cable_params.span),
     cable_spans({}),
     cable_IS({}),
     span_dof_maps({})
  {
    PetscErrorCode ierr;
    assert(num_spans>=1);

    // Create cable spans
    const int nNodes_per_span = nelms_per_span+1;
    std::vector<double> coordinates(nNodes_per_span);
    const double dx = span_length/static_cast<double>(nelms_per_span);
    for(int a=0; a<nNodes_per_span; ++a)
      coordinates[a] = xinit+static_cast<double>(a)*dx;

    SuspensionSpringParams half_spring = spring_params;
    half_spring.stiffness = 0.5*spring_params.stiffness;
    
    for(int s=0; s<num_spans; ++s)
      {
	// create this span
	cable_spans.push_back( new HeavyTensionedBeam(cable_params, coordinates) );

	// add a suspension spring at the left end
	if(s==0)
	  cable_spans[s]->AddSuspensionSpring(spring_params, 0);
	else
	  cable_spans[s]->AddSuspensionSpring(half_spring, 0);
	
	// add a suspension spring at the right end
	if(s==num_spans-1)
	  cable_spans[s]->AddSuspensionSpring(spring_params, nNodes_per_span-1);
	else
	  cable_spans[s]->AddSuspensionSpring(half_spring, nNodes_per_span-1);

	// coordinates for the next span
	for(auto& x:coordinates)
	  x += span_length;
      }


    // Enumerate dofs span-wise, while noting common dofs
    const int num_dofs_per_span = cable_spans[0]->GetTotalNumDof();
    span_dof_maps.resize(num_spans, std::vector<int>(num_dofs_per_span));
    int dof_start = 0;
    for(int s=0; s<num_spans; ++s)
      {
	// set the dofs for this span without noting common dofs
	const int dof_start = s*num_dofs_per_span;
	for(int j=0; j<num_dofs_per_span; ++j)
	  span_dof_maps[s][j] = j+dof_start;

	// note common dofs: end dofs of left span = start dofs of right span
	if(s>0)
	  {
	    // end dofs of left span
	    auto left_dofs = cable_spans[s-1]->GetEndDofs();

	    // start dofs of this span
	    auto right_dofs = cable_spans[s]->GetStartDofs();

	    // Set start dofs of right span = end dofs of left span
	    span_dof_maps[s][right_dofs.first] = span_dof_maps[s-1][left_dofs.first];
	  }
      }

    // Set of unique dofs
    std::set<int> dof_set{};
    for(auto& it:span_dof_maps)
      for(auto& jt:it)
	dof_set.insert(jt);

    // Serialize dofs
    std::map<int,int> old_2_new_num{};
    int dof_count = 0;
    for(auto& it:dof_set)
      old_2_new_num[it] = dof_count++;
    nTotalDof = dof_count;

    // Renumber dofs
    for(int s=0; s<num_spans; ++s)
      for(int j=0; j<num_dofs_per_span; ++j)
	{
	  int old_num = span_dof_maps[s][j];
	  auto it = old_2_new_num.find(old_num);
	  assert(it!=old_2_new_num.end());
	  span_dof_maps[s][j] = it->second;
	}

    // Create index sets
    cable_IS.resize(num_spans);
    for(int s=0; s<num_spans; ++s)
      {
	ierr = ISCreateGeneral(PETSC_COMM_WORLD, num_dofs_per_span, span_dof_maps[s].data(), PETSC_COPY_VALUES, &cable_IS[s]); CHKERRV(ierr);
      }

    // done
  }


  // Destroy
  void MultiSpan_Cable::Destroy()
  {
    assert(isDestroyed==false);
    for(auto& it:cable_spans)
      {
	it->Destroy();
	delete it;
      }

    PetscErrorCode ierr;
    for(auto& is:cable_IS)
      {
	ierr = ISDestroy(&is); CHKERRV(ierr);
      }
    isDestroyed = true;
  }

  
  // Destructor
  MultiSpan_Cable::~MultiSpan_Cable()
  {
    assert(isDestroyed==true);
  }

  
  // Main functionality: evaluate contribution to mass, stiffness & force
  void MultiSpan_Cable::Evaluate(Mat& M, Mat& K, Vec& F) const
  {
    assert(isDestroyed==false);
    
    PetscErrorCode ierr;

    // permit new nonzeros
    ierr = MatSetOption(M, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
    ierr = MatSetOption(K, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);

    // Assemble contributions span-by-span
    Mat M_span;
    Mat K_span;
    Vec F_span;
    
    for(int s=0; s<num_spans; ++s)
      {
	// extract sib-matrices/sub-vectors for this span
	const auto& is = cable_IS[s];
	ierr = MatGetLocalSubMatrix(M, is, is, &M_span); CHKERRV(ierr);
	ierr = MatGetLocalSubMatrix(K, is, is, &K_span); CHKERRV(ierr);
	ierr = VecGetSubVector(F, is, &F_span);          CHKERRV(ierr);
	
	// assemble contributions from this span
	cable_spans[s]->Evaluate(M_span, K_span, F_span);

	// restore
	ierr = VecRestoreSubVector(F, is, &F_span);          CHKERRV(ierr);
	ierr = MatRestoreLocalSubMatrix(K, is, is, &K_span); CHKERRV(ierr);
	ierr = MatRestoreLocalSubMatrix(M, is, is, &M_span); CHKERRV(ierr);
      }

    // Complete assembly
    ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = VecAssemblyBegin(F);                     CHKERRV(ierr); 
    ierr = VecAssemblyEnd(F);                       CHKERRV(ierr);

    // done
    return;
  }

  
  // Get the active displacement dofs and shape functions at a given point along the span
  std::array<std::pair<int,double>,2> MultiSpan_Cable::GetActiveDisplacementDofs(const double x) const
  {
    assert(isDestroyed==false);
    
    // locate the span containing x
    const int span_num = static_cast<int>((x-xinit)/span_length);
    assert(span_num>=0 && span_num<num_spans);
    assert(x>=xinit+span_num*span_length && x<=xinit+(span_num+1)*span_length);

    // get the local dof# and shape function values in this span
    auto dof_shp_pairs = cable_spans[span_num]->GetActiveDisplacementDofs(x);

    // map local dofs -> global dofs
    for(int i=0; i<2; ++i)
      dof_shp_pairs[i].first = span_dof_maps[span_num][dof_shp_pairs[i].first];

    // done
    return dof_shp_pairs;
  }

  // Get the active dofs and shape functions at a point along the span
  std::array<std::pair<int,double>,4> MultiSpan_Cable::GetActiveDofs(const double x) const
  {
    assert(isDestroyed==false);
    
    // locate the span containing x
    const int span_num = static_cast<int>((x-xinit)/span_length);
    assert(span_num>=0 && span_num<num_spans);
    assert(x>=xinit+span_num*span_length && x<=xinit+(span_num+1)*span_length);

    // get the local dof# and the shape function values in this span
    auto dof_shp_pairs = cable_spans[span_num]->GetActiveDofs(x);

    // map local dofs -> global dofs
    for(int i=0; i<4; ++i)
      dof_shp_pairs[i].first = span_dof_maps[span_num][dof_shp_pairs[i].first];

    // done
    return dof_shp_pairs;
  }


  // Visualize the configuration
  void MultiSpan_Cable::Write(const double* displacements,
			      const std::string filename) const
  {
    assert(isDestroyed==false);
    
    const int num_dofs_per_span = cable_spans[0]->GetTotalNumDof();

    // write span-by-span
    std::vector<double> span_dofs(num_dofs_per_span);
    for(int s=0; s<num_spans; ++s)
      {
	const auto& l2g = span_dof_maps[s];

	// dof values for this span
	for(int a=0; a<num_dofs_per_span; ++a)
	  span_dofs[a] = displacements[l2g[a]];

	// write this span to file
	cable_spans[s]->Write(span_dofs.data(), filename);
      }

    // done
    return;
  }


  void MultiSpan_Cable::Write(const double* disp, const double* vel, const double* accn,
			      const std::string filename) const
  {
    assert(isDestroyed==false);
    
    const int num_dofs_per_span = cable_spans[0]->GetTotalNumDof();

    // write span-by-span
    std::vector<double> span_disp(num_dofs_per_span);
    std::vector<double> span_vel(num_dofs_per_span);
    std::vector<double> span_accn(num_dofs_per_span);
    for(int s=0; s<num_spans; ++s)
      {
	const auto& l2g = span_dof_maps[s];
	
	// data for this span
	for(int a=0; a<num_dofs_per_span; ++a)
	  {
	    span_disp[a] = disp[l2g[a]];
	    span_vel[a]  = vel[l2g[a]];
	    span_accn[a] = accn[l2g[a]];
	  }

	// write this span
	cable_spans[s]->Write(span_disp.data(), span_vel.data(), span_accn.data(), filename);
      }

    // done
    return;
  }

  // Sparsity
  std::vector<int> MultiSpan_Cable::GetSparsity() const
  {
    assert(isDestroyed==false);
    
    std::vector<int> sparsity(nTotalDof);
    std::fill(sparsity.begin(), sparsity.end(), 0);

    // Update sparsity span-by-span
    const int num_dofs_per_span = cable_spans[0]->GetTotalNumDof();
    for(int s=0; s<num_spans; ++s)
      {
	// sparsity of this span
	const auto span_sparsity = cable_spans[s]->GetSparsity();
	assert(static_cast<int>(span_sparsity.size())==num_dofs_per_span);

	// update with global indices
	const auto& l2g = span_dof_maps[s];
	for(int a=0; a<num_dofs_per_span; ++a)
	  sparsity[l2g[a]] += span_sparsity[a];
      }

    // done
    return std::move(sparsity);
  }
   
}
