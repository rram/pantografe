
/** \file pfe_SuspensionSpring.h
 * \brief Defines the class pfe::SuspensionSpring
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_SuspensionSpringParams.h>
#include <petscvec.h>
#include <petscmat.h>

namespace pfe
{
  //! Class defining a suspension spring
  //! \ingroup components
  class SuspensionSpring
  {
  public:
    inline SuspensionSpring(const SuspensionSpringParams& sp,
			    const int dofnum)
      :params(sp),
      dofNum(dofnum) {}

    //! Destructor
    inline virtual ~SuspensionSpring() {}

    //! Disable copy and assignment
    SuspensionSpring(const SuspensionSpring&) = delete;
    SuspensionSpring operator=(const SuspensionSpring&) = delete;

    //! Assemble contributions to the mass, stiffness and force vector
    void Evaluate(Mat& M, Mat& K, Vec& F);

  protected:
    const SuspensionSpringParams params;
    const int dofNum;
  };
}

