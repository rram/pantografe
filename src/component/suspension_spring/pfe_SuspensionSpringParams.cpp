
/** \file pfe_SuspensionSpringParams.cpp
 * \brief Implements the struct pfe::SuspensionSpringParams
 * \author Ramsharan Rangarajan
 */

#include <pfe_SuspensionSpringParams.h>
#include <pfe_json.hpp>
#include <cassert>
#include <iomanip>

namespace pfe
{
  // write to a json file
  std::ostream& operator << (std::ostream &out, const SuspensionSpringParams& params)
  {
    assert(params.tag.empty()==false);

    nlohmann::json j;
    j[params.tag]["stiffness"] = params.stiffness;
    j[params.tag]["mass"]      = params.mass;
    j[params.tag]["gravity"]   = params.g;

    out << std::endl << std::endl << "\"" << params.tag << "\"" << " : " << std::setw(8) << j[params.tag];
									    
    return out;
  }

  // read from a json file
  std::istream& operator >> (std::istream &in, SuspensionSpringParams& params)
  {
    assert(params.tag.empty()==false);

    // rewind the file
    in.seekg(0, std::ios::beg);

    // parse
    auto J = nlohmann::json::parse(in);
    auto it = J.find(params.tag);
    assert(it!=J.end());
    auto& j = *it;
    
    // check fields
    assert(j.empty()==false);
    assert(j.contains("stiffness"));
    assert(j.contains("mass"));
    assert(j.contains("gravity"));

    // read. use get_to() to enforce type
    j["stiffness"].get_to(params.stiffness);
    j["mass"].get_to(params.mass);
    j["gravity"].get_to(params.g);
    
    return in;
  }
}
