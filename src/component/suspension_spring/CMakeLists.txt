
# add library
add_library(pfe_susp_spring STATIC
  pfe_SuspensionSpringParams.cpp
  pfe_SuspensionSpring.cpp)

# headers
target_include_directories(pfe_susp_spring PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)

# link
target_link_libraries(pfe_susp_spring PUBLIC pfe_assembly pfe_ext)

# Add required flags
target_compile_features(pfe_susp_spring PUBLIC ${pfe_COMPILE_FEATURES})

install(FILES
	pfe_SuspensionSpring.h
	pfe_SuspensionSpringParams.h
        DESTINATION ${PROJECT_NAME}/include)
	  
