
/** \file pfe_SuspensionSpringParams.h
 * \brief Defines the struct pfe::SuspensionSpringParams
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <fstream>

namespace pfe
{
  //! \ingroup components
  struct SuspensionSpringParams
  {
    std::string tag;
    double stiffness;
    double mass;
    double g;

    friend std::ostream& operator << (std::ostream &out, const SuspensionSpringParams& params);

    friend std::istream& operator >> (std::istream &in, SuspensionSpringParams& params);
  };
    
}
