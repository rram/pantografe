
/** \file pfe_SuspensionSpring.cpp
 * \brief Implements the class pfe::SuspensionSpring
 * \author Ramsharan Rangarajan
 */

#include <pfe_SuspensionSpring.h>

namespace pfe
{
  // Assemble contributions to the stiffness and force vector
  void SuspensionSpring::Evaluate(Mat &M, Mat& K, Vec& F)
  {
    PetscErrorCode ierr;
    
    // Update mass
    ierr = MatSetValuesLocal(M, 1, &dofNum, 1, &dofNum, &params.mass, ADD_VALUES); CHKERRV(ierr);
    ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    
    // Update stiffness
    // Functional: 1/2 K (w_dof)^2
    ierr = MatSetValuesLocal(K, 1, &dofNum, 1, &dofNum, &params.stiffness, ADD_VALUES); CHKERRV(ierr);
    ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
  
    // Update force
    double val = -params.mass*params.g;
    ierr = VecSetValues(F, 1, &dofNum, &val, ADD_VALUES); CHKERRV(ierr);
    ierr = VecAssemblyBegin(F); CHKERRV(ierr);
    ierr = VecAssemblyEnd(F);   CHKERRV(ierr);
    
    // done
    return;
  }

}
