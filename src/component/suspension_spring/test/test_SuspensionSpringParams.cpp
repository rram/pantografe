
/** \file test_IntercableDropperParams.cpp
 * \brief Unit test for read/write of parameters using the json format
 * \author Ramsharan Rangarajan
 * \comp_test Unit test for read/write of parameters using the json format
 */


#include <pfe_SuspensionSpringParams.h>
#include <random>
#include <cassert>

void Generate(pfe::SuspensionSpringParams& params);

void Compare(const pfe::SuspensionSpringParams& A,
	     const pfe::SuspensionSpringParams& B,
	     const double tol);

int main()
{
  std::fstream jfile;
  jfile.open((char*)"datasheet.json", std::ios::out);
  jfile << "{" << std::endl;
  // Write 
  // suspension springs
  pfe::SuspensionSpringParams springParams;
  springParams.tag = "suspension spring";
  Generate(springParams);
  jfile << springParams;
  jfile <<"}" << std::endl;

  jfile.close();

  // Read & compare
  jfile.open((char*)"datasheet.json", std::ios::in);

  pfe::SuspensionSpringParams spring_copy;
  spring_copy.tag = "suspension spring";
  jfile >> spring_copy;
  Compare(springParams, spring_copy, 1.e-6);
  
  jfile.close();
}




void Generate(pfe::SuspensionSpringParams& params)
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(1.0, 100.0);
  params.stiffness = dis(gen);
  params.mass = dis(gen);
  params.g = dis(gen);
  return;
}


void Compare(const pfe::SuspensionSpringParams& A,
	     const pfe::SuspensionSpringParams& B,
	     const double tol)
{
  assert(A.tag.empty()==false && B.tag.empty()==false);
  assert(std::abs(A.stiffness-B.stiffness)<tol);
  assert(std::abs(A.mass-B.mass)<tol);
  assert(std::abs(A.g-B.g)<tol);
  return;
}
