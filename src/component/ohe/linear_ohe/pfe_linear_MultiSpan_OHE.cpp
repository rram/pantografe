
/** \file pfe_linear_MultiSpan_OHE.cpp
 * \brief Implements the class pfe::linear::MultiSpan_OHE 
 * \author Ramsharan Rangarajan
 */


#include <pfe_linear_MultiSpan_OHE.h>

namespace pfe
{
  namespace linear
  {
    // Main functionlity: evaluate contribution to mass, stiffness and force vectors
    void MultiSpan_OHE::Evaluate(Mat& M, Mat& K, Vec& F) const
    {
      assert(isDestroyed==false);
      PetscErrorCode ierr;

      // Permit new nonzeros
      ierr = MatSetOption(M, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
      ierr = MatSetOption(K, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
    
      // Assemble contributions 1-span at a time
      Mat M_span;
      Mat K_span;
      Vec F_span;

      for(int i=0; i<num_spans; ++i)
	{
	  // extract sub-matrices/sub-vectors for this span
	  const auto& is = ohe_IS[i];
	  ierr = MatGetLocalSubMatrix(M, is, is, &M_span); CHKERRV(ierr);
	  ierr = MatGetLocalSubMatrix(K, is, is, &K_span); CHKERRV(ierr);
	  ierr = VecGetSubVector(F, is, &F_span);          CHKERRV(ierr);

	  // assemble contributions from this span
	  ohe_spans[i]->Evaluate(M_span, K_span, F_span);

	  // restore
	  ierr = VecRestoreSubVector(F, is, &F_span);          CHKERRV(ierr);
	  ierr = MatRestoreLocalSubMatrix(K, is, is, &K_span); CHKERRV(ierr);
	  ierr = MatRestoreLocalSubMatrix(M, is, is, &M_span); CHKERRV(ierr);
	}

      // Complete assembly
      ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
      ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
      ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
      ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
      ierr = VecAssemblyBegin(F);                     CHKERRV(ierr);
      ierr = VecAssemblyEnd(F);                       CHKERRV(ierr);

      // done
      return;
    }

  }
}
