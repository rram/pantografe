
/** \file pfe_linear_MultiSpan_OHE.h
 * \brief Defines the class pfe::linear::MultiSpan_OHE encapsulating multiple OHE spans with linear droppers
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_MultiSpan_OHE_base.h>
#include <pfe_linear_SingleSpan_OHE.h>

namespace pfe
{
  namespace linear
  {
    class MultiSpan_OHE: public MultiSpan_OHE_base<SingleSpan_OHE>
    {
    public:
      //! Constructor
      inline MultiSpan_OHE(const double x0,  const OHESpan_ParameterPack& pack,
			   const int nspans, const int nelm_per_interval)
	:MultiSpan_OHE_base<SingleSpan_OHE>(x0, pack, nspans, nelm_per_interval) {}

      //! Destructor
      inline virtual ~MultiSpan_OHE() {
	assert(isDestroyed==true); }

      //! Disable copy and assignment
      MultiSpan_OHE(const MultiSpan_OHE&) = delete;
      MultiSpan_OHE operator=(const MultiSpan_OHE&) = delete;

      // Main functionlity: evaluate contribution to mass, stiffness and force vectors
      void Evaluate(Mat& M, Mat& K, Vec& F) const;
    };
  }
}
