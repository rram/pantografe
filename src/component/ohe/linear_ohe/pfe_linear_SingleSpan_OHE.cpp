
/** \file pfe_linear_SingleSpan_OHE.cpp
 * \brief Implements the class pfe::linear::SingleSpan_OHE
 * \author Ramsharan Rangarajan
 */

#include <pfe_linear_SingleSpan_OHE.h>

namespace pfe
{
  namespace linear
  {
    // Constructor that allows for different spring stiffnesses at the left and right ends of the spans
    // of the catenary cable and contact wire
    SingleSpan_OHE::SingleSpan_OHE(const double x0,
				   const CatenaryWireParams&      catenaryParams,
				   const ContactWireParams&       wireParams,
				   const DropperParams&           dropperParams,
				   const DropperArrangement&      dropper_schedule,
				   const SuspensionSpringParams& left_catenary_spring_params,
				   const SuspensionSpringParams& right_catenary_spring_params,
				   const SuspensionSpringParams& left_contact_spring_params,
				   const SuspensionSpringParams& right_contact_spring_params,
				   const int nelm)
    :SingleSpan_OHE_base(x0, catenaryParams, wireParams, dropperParams, dropper_schedule,
			 left_catenary_spring_params, right_catenary_spring_params, 
			 left_contact_spring_params, right_contact_spring_params, 
			 nelm)
    {
      // create droppers
      const int& nDroppers = dropper_schedule.nDroppers;
      droppers.resize(nDroppers);
      for(int d=0; d<nDroppers; ++d)
	{
	  auto top_bot_dofs = GetDropperDofPair(d);
	  droppers[d] = new LinearDropper(dropperParams, dropper_schedule.nominal_lengths[d],
					  dropper_schedule.zEncumbrance, top_bot_dofs.first, top_bot_dofs.second);
	}
    }
    
    // Constructor
    SingleSpan_OHE::SingleSpan_OHE(const double x0, const OHESpan_ParameterPack& pack, const int nelm)
      :SingleSpan_OHE(x0, pack.catenaryParams, pack.wireParams, pack.dropperParams, pack.dropperSchedule,
		      pack.catenary_spring_params, pack.catenary_spring_params, // left=right
		      pack.contact_spring_params, pack.contact_spring_params,   // left=right
		      nelm) {}

    // Destroy
    void SingleSpan_OHE::Destroy()
    {
      assert(isDestroyed==false);
      SingleSpan_OHE_base::Destroy();
      for(auto& d:droppers)
	delete d;
      assert(isDestroyed==true);
    }
    
    // Destructor
    SingleSpan_OHE::~SingleSpan_OHE()
    {
      assert(isDestroyed==true);
    }
  
    // Reset dropper lengths
    void SingleSpan_OHE::ResetDropperLengths(const double* lengths)
    {
      assert(isDestroyed==false);
      const int nDroppers = static_cast<int>(droppers.size());
      for(int d=0; d<nDroppers; ++d)
	{
	  assert(lengths[d]>0.);
	  droppers[d]->SetLength(lengths[d]);
	}
      return;
    }

  
    // Main functionality: evaluate contribution to mass, stiffness, and force vectors
    void SingleSpan_OHE::Evaluate(Mat& M, Mat& K, Vec& F) const
    {
      assert(isDestroyed==false);
      
      // Contribution from cables
      SingleSpan_OHE_base::Evaluate(M,K,F);
      
      // Permit new nonzeros
      PetscErrorCode ierr;
      ierr = MatSetOption(M, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
      ierr = MatSetOption(K, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
    
      // Contribution from droppers
      for(auto& d:droppers)
	d->Evaluate(M, K, F);

      // done
      return;
    }

  }
}
