
/** \file pfe_linear_SingleSpan_OHE.h
 * \brief Defines the class pfe::linear::SingleSpan_OHE encapsulating a single span of the OHE with linear droppers
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_SingleSpan_OHE_base.h>
#include <pfe_LinearDropper.h>

namespace pfe
{
  namespace linear
  {
    class SingleSpan_OHE: public SingleSpan_OHE_base
    {
    public:
      //! Constructor
      SingleSpan_OHE(const double x0, const OHESpan_ParameterPack& pack, const int nelm);

      //! Constructor that allows for different spring stiffnesses at the left and right ends of the spans
      //! of the catenary cable and contact wire
      SingleSpan_OHE(const double x0,
		     const CatenaryWireParams&      catenaryParams,
		     const ContactWireParams&       wireParams,
		     const DropperParams&           dropperParams,
		     const DropperArrangement&      dropper_schedule,
		     const SuspensionSpringParams& left_catenary_spring_params,
		     const SuspensionSpringParams& right_catenary_spring_params,
		     const SuspensionSpringParams& left_contact_spring_params,
		     const SuspensionSpringParams& right_contact_spring_params,
		     const int nelm);
    
      //! Destructor
      virtual ~SingleSpan_OHE();

      // Disable copy and assignment
      SingleSpan_OHE(const SingleSpan_OHE&) = delete;
      SingleSpan_OHE operator=(const SingleSpan_OHE&) = delete;

      //! Destroy
      void Destroy() override;
      
      //! Access this span
      inline virtual const SingleSpan_OHE& GetSpan() const
      { assert(isDestroyed==false);
	return *this; }

      //! Access this span
      inline const SingleSpan_OHE& GetSpan(const int s) const
      { assert(isDestroyed==false);
	return *this; }

      //! Access this span
      inline SingleSpan_OHE& GetSpan(const int s)
      { assert(isDestroyed==false);
	return *this; }

      //! Access droppers
      inline const std::vector<LinearDropper*>& GetDroppers() const
      { assert(isDestroyed==false);
	return droppers; }

      //! Main functionality: evaluate contribution to mass, stiffness, and force vectors
      virtual void Evaluate(Mat& M, Mat& K, Vec& F) const override;

      //! Reset dropper lengths
      void ResetDropperLengths(const double* lengths);

    private:
      std::vector<LinearDropper*> droppers;
    };
  
  }
}
