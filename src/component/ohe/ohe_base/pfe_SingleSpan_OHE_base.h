
/** \file pfe_SingleSpan_OHE_base.h
 * \brief Defines the class pfe::SingleSpan_OHE_base encapsulating a single span of the OHE but not including droppers
 * \author Ramsharan Rangarajan
 */


#pragma once

#include <pfe_HeavyTensionedBeam.h>
#include <pfe_SuspensionSpring.h>
#include <pfe_DropperParams.h>

namespace pfe
{
  //! Pack of parameters
  //! \ingroup components
  struct OHESpan_ParameterPack
  {
    CatenaryWireParams      catenaryParams;
    ContactWireParams       wireParams;
    DropperParams           dropperParams;
    DropperArrangement      dropperSchedule;
    SuspensionSpringParams  catenary_spring_params;
    SuspensionSpringParams  contact_spring_params;

    // read from json file
    friend std::istream& operator >> (std::istream& in, OHESpan_ParameterPack& params);
  };
  
  //! One span of the OHE
  //! \ingroup components
  class SingleSpan_OHE_base
  {
  public:
    //! Constructor
    SingleSpan_OHE_base(const double x0, const OHESpan_ParameterPack& pack, const int nelm);

    //! Constructor that allows for different spring stiffnesses at the left and right ends of the spans
    //! of the catenary cable and contact wire
    SingleSpan_OHE_base(const double x0,
			const CatenaryWireParams&      catenaryParams,
			const ContactWireParams&       wireParams,
			const DropperParams&           dropperParams,
			const DropperArrangement&      dropper_schedule,
			const SuspensionSpringParams& left_catenary_spring_params,
			const SuspensionSpringParams& right_catenary_spring_params,
			const SuspensionSpringParams& left_contact_spring_params,
			const SuspensionSpringParams& right_contact_spring_params,
			const int nelm);
	    
    //! Destructor
    virtual ~SingleSpan_OHE_base();

    // Disable copy and assignment
    SingleSpan_OHE_base(const SingleSpan_OHE_base&) = delete;
    SingleSpan_OHE_base operator=(const SingleSpan_OHE_base&) = delete;

    //! Destroy
    virtual void Destroy();
    
    //! Access number of spans
    inline int GetNumSpans() const
    { return 1; }
   
    //! Access the total number of dofs
    inline int GetTotalNumDof() const 
    { return nTotalDof; }
    
    //! Access the dofs of droppers
    std::vector<std::pair<int,int>> GetDropperDofs(const int span_num) const;
    
    //! Access the dofs of a given dropper
    std::pair<int,int> GetDropperDofPair(const int d) const;
    
    //! Map contact wire node to global dof number
    inline int MapContactWireNodeToDof(const int span_num, const int node) const
    { assert(span_num==0);
      return node + catenary_L2GMap->GetTotalNumDof(); }

    //! Get the active displacement dofs and shape function values at a given point along the span
    std::array<std::pair<int,double>,2> GetActiveDisplacementDofs(const double x) const;

    //! Get the active dofs and shape function values at a given point along the span
    std::array<std::pair<int,double>,4> GetActiveDofs(const double x) const;
    
    //! Number of nonzero per row
    std::vector<int> GetSparsity() const;

    //! Main functionality: evaluate contribution to mass, stiffness, and force vectors
    virtual void Evaluate(Mat& M, Mat& K, Vec& F) const;
    
    //! Access the catenary cable component
    inline const HeavyTensionedBeam& CatenaryCable() const 
    { return *catenary_cable; }

    //! Access the contact wire component
    inline const HeavyTensionedBeam& ContactWire() const 
    { return *contact_wire; }

    //! Access the dropper arrangement
    inline const DropperArrangement& GetDropperArrangement() const 
    { return dropperSchedule; }

    //! Get the datum level of the contact wire
    inline const double GetDatum() const 
    { return contact_wire->GetDatum(); }
	
    // Access the list of dropper nodes (matching nodes on the contact and catenary)
    std::vector<int> GetDropperNodes() const;
    
    //! Get the catenary cable dofs at the start and end of the span
    std::pair<int,int> GetCatenaryCableStartDofs() const;
    std::pair<int,int> GetCatenaryCableEndDofs() const;

    //! Get the contact wire dofs at the start and end of the span
    std::pair<int,int> GetContactWireStartDofs() const;
    std::pair<int,int> GetContactWireEndDofs() const;

    //! Get the list of contact wire dofs
    std::set<int> GetContactWireDofs() const;

    //! Visualize the configuration
    virtual void Write(const double* displacements,
		       const std::string catenary_fname,
		       const std::string contact_fname,
		       const std::string dropper_fname) const;

    virtual void Write(const double* disp, const double* vel, const double* accn,
		       const std::string catenary_fname,
		       const std::string contact_fname,
		       const std::string dropper_fname) const;

    inline virtual void Write(const double* disp, const double* vel, const double* accn,
			      const std::vector<std::string> ohe_fnames) const 
    {
      assert(static_cast<int>(ohe_fnames.size())==3);
      Write(disp, vel, accn, ohe_fnames[0], ohe_fnames[1], ohe_fnames[2]);
    }
    
  protected:
    bool isDestroyed;
    const int nElements_per_Interval;         //!< Number of elements per interval
    const int nElements;                      //!< Number of elements
    const int nNodes;                         //!< Number of nodes
    const DropperArrangement dropperSchedule; //!< Dropper arrangement
    HeavyTensionedBeam* catenary_cable;       //!< Catenary cable
    HeavyTensionedBeam* contact_wire;         //!< Contact wire
    
    LocalToGlobalMap* catenary_L2GMap;        //!< local to global map for the catenary cable
    LocalToGlobalMap* contact_L2GMap;         //!< local to global map for the contact wire
    int nTotalDof;                            //!< total number of dofs

    IS IS_catenary_cable;                     //!< (local) index set for catenary cable dofs
    IS IS_contact_wire;                       //!< (local) index set for contact wire dofs
  };
}
