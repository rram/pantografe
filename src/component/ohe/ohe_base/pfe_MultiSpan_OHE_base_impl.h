
/** \file pfe_MultiSpan_OHE_base_impl.h
 * \brief Implements the class pfe::MultiSpan_OHE_base
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <fstream>

namespace pfe
{
  // Constructor
  template<typename OHEtype>
    MultiSpan_OHE_base<OHEtype>::MultiSpan_OHE_base(const double x0,  const OHESpan_ParameterPack& pack,
						    const int nspans, const int nelms_per_interval)
    :isDestroyed(false),
    xinit(x0),
    num_spans(nspans),
    span_length(pack.wireParams.span),
    ohe_spans({}),
    ohe_IS({}),
    contact_wire_dof_maps({})
      {
	PetscErrorCode ierr;
	assert(num_spans>=1);
    
	// Create OHE spans
	double xstart = x0;
	SuspensionSpringParams left_catenary_spring_params  = pack.catenary_spring_params;
	SuspensionSpringParams right_catenary_spring_params = pack.catenary_spring_params;
	SuspensionSpringParams left_contact_spring_params   = pack.contact_spring_params;
	SuspensionSpringParams right_contact_spring_params  = pack.contact_spring_params;
	for(int i=0; i<num_spans; ++i)
	  {
	    if(i==0) // left ends has full spring stiffness & mass
	      {
		left_catenary_spring_params.stiffness = pack.catenary_spring_params.stiffness;
		left_catenary_spring_params.mass      = pack.catenary_spring_params.mass;
		left_contact_spring_params.stiffness  = pack.contact_spring_params.stiffness;
		left_contact_spring_params.mass       = pack.contact_spring_params.mass;
	      }
	    else // left end has half spring stiffness & mass
	      {
		left_catenary_spring_params.stiffness = 0.5*pack.catenary_spring_params.stiffness;
		left_catenary_spring_params.mass      = 0.5*pack.catenary_spring_params.mass;
		left_contact_spring_params.stiffness  = 0.5*pack.contact_spring_params.stiffness;
		left_contact_spring_params.mass       = 0.5*pack.contact_spring_params.mass;
	      }

	    if(i==num_spans-1) // right end has full spring stiffness & mass
	      {
		right_catenary_spring_params.stiffness = pack.catenary_spring_params.stiffness;
		right_catenary_spring_params.mass      = pack.catenary_spring_params.mass;
		right_contact_spring_params.stiffness  = pack.contact_spring_params.stiffness;
		right_contact_spring_params.mass       = pack.contact_spring_params.mass;
	      }
	    else // right end has half spring stiffness & mass
	      {
		right_catenary_spring_params.stiffness = 0.5*pack.catenary_spring_params.stiffness;
		right_catenary_spring_params.mass      = 0.5*pack.catenary_spring_params.mass;
		right_contact_spring_params.stiffness  = 0.5*pack.contact_spring_params.stiffness;
		right_contact_spring_params.mass       = 0.5*pack.contact_spring_params.mass;
	      }

	    ohe_spans.push_back(new OHEtype(xstart,
					    pack.catenaryParams,
					    pack.wireParams,
					    pack.dropperParams,
					    pack.dropperSchedule,
					    left_catenary_spring_params, right_catenary_spring_params,
					    left_contact_spring_params,  right_contact_spring_params,
					    nelms_per_interval));
	    xstart += span_length;
	  }


	// Number dofs span-wise, while noting common dofs
	const int num_dofs_per_span = ohe_spans[0]->GetTotalNumDof();
	std::vector<std::vector<int>> span_dofs(num_spans, std::vector<int>(num_dofs_per_span));
	int dof_start = 0;
	for(int i=0; i<num_spans; ++i)
	  {
	    // set the dofs for this span without noting common dofs
	    for(int j=0; j<num_dofs_per_span; ++j)
	      span_dofs[i][j] = j+dof_start;
	
	    const std::set<int> span_contact_dofs = ohe_spans[0]->GetContactWireDofs(); // contact wire dofs, in the local snap's numbering
	    std::map<int, int> contact_dof_map{};
	    for(auto& it:span_contact_dofs)
	      contact_dof_map.insert({it, it+dof_start});
	
	    // note common dofs: end dofs of left span = start dofs of right span
	    if(i>0)
	      {
		// end dofs of left span
		auto left_catenary_dofs = ohe_spans[i-1]->GetCatenaryCableEndDofs();
		auto left_contact_dofs  = ohe_spans[i-1]->GetContactWireEndDofs();

		// start dofs of this span
		auto right_catenary_dofs = ohe_spans[i]->GetCatenaryCableStartDofs();
		auto right_contact_dofs  = ohe_spans[i]->GetContactWireStartDofs();

		// Set start dofs of right span = end dofs of left span

		// displacement continuity at the catenary
		span_dofs[i][right_catenary_dofs.first]  = span_dofs[i-1][left_catenary_dofs.first];
	 
		// displacement continuity at the contact wire
		span_dofs[i][right_contact_dofs.first]  = span_dofs[i-1][left_contact_dofs.first];

		// update the common dof in the contact wire dof map
		auto it = contact_dof_map.find(right_contact_dofs.first);
		assert(it!=contact_dof_map.end());
		it->second = span_dofs[i-1][left_contact_dofs.first];

		// Derivative continuity at the catenary and contact wires
		span_dofs[i][right_catenary_dofs.second] = span_dofs[i-1][left_catenary_dofs.second];
		span_dofs[i][right_contact_dofs.second]  = span_dofs[i-1][left_contact_dofs.second];
		auto jt = contact_dof_map.find(right_contact_dofs.second);
		assert(jt!=contact_dof_map.end());
		jt->second = span_dofs[i-1][left_contact_dofs.second];
	      }

	    // update the map of contact wire dofs for this span
	    contact_wire_dof_maps.push_back(contact_dof_map);
	
	    // start dof for the next span
	    dof_start += num_dofs_per_span;
	  }

	// Set of unique dofs
	std::set<int> dof_set{};
	for(auto& it:span_dofs)
	  for(auto& jt:it)
	    dof_set.insert(jt);

	// Serialize dofs
	std::map<int,int> old_2_new_num{};
	int dof_count = 0;
	for(auto& it:dof_set)
	  old_2_new_num[it] = dof_count++;
	nTotalDof = dof_count;

	// Renumber dofs
	for(int i=0; i<num_spans; ++i)
	  for(int j=0; j<num_dofs_per_span; ++j)
	    {
	      int old_num = span_dofs[i][j];
	      auto it = old_2_new_num.find(old_num);
	      assert(it!=old_2_new_num.end());
	      span_dofs[i][j] = it->second;
	    }

	// Note the update dof #s in the contact wire map
	for(int i=0; i<num_spans; ++i)
	  for(auto& it:contact_wire_dof_maps[i])
	    {
	      int old_num = it.second;
	      auto jt = old_2_new_num.find(old_num);
	      assert(jt!=old_2_new_num.end());
	      it.second = jt->second;
	    }
    
	// Create index sets
	ohe_IS.resize(num_spans);
	for(int i=0; i<num_spans; ++i)
	  {
	    ierr = ISCreateGeneral(PETSC_COMM_WORLD, num_dofs_per_span, span_dofs[i].data(), PETSC_COPY_VALUES, &ohe_IS[i]); CHKERRV(ierr);
	  }

	// done
      }


  // Destroy
  template<typename OHEtype>
    void MultiSpan_OHE_base<OHEtype>::Destroy()
    {
      assert(isDestroyed==false);
      for(auto& x:ohe_spans)
	{
	  x->Destroy();
	  delete x;
	}

      PetscErrorCode ierr;
      for(auto& is:ohe_IS)
	{
	  ierr = ISDestroy(&is); CHKERRV(ierr);
	}
      isDestroyed = true;
    }

  
  // Destructor
  template<typename OHEtype>
    MultiSpan_OHE_base<OHEtype>::~MultiSpan_OHE_base()
    {
      assert(isDestroyed==true);
    }

  
  // Get the active displacement dofs and shape function values at a given point along the span
  template<typename OHEtype>
    std::array<std::pair<int,double>,2>
    MultiSpan_OHE_base<OHEtype>::GetActiveDisplacementDofs(const double x) const
    {
      assert(isDestroyed==false);
      
      // locate the span containing 'x'
      const int span_num = static_cast<int>((x-xinit)/span_length);
      assert(span_num>=0 && span_num<num_spans);
      assert(x>=xinit+span_num*span_length && x<=xinit+(span_num+1)*span_length);
    
      // get the local dof# and shape function values within this span
      auto dof_shp_pairs = ohe_spans[span_num]->GetActiveDisplacementDofs(x);

      // map local dof# to global dof#
      for(int i=0; i<2; ++i)
	{
	  int local_dof = dof_shp_pairs[i].first;
	  auto it = contact_wire_dof_maps[span_num].find(local_dof);
	  assert(it!=contact_wire_dof_maps[i].end());
	  dof_shp_pairs[i].first = it->second;
	}
    
      return dof_shp_pairs;
    }

  
  // Get the active dofs and shape function values at a given point along the span
  template<typename OHEtype>
    std::array<std::pair<int,double>,4>
    MultiSpan_OHE_base<OHEtype>::GetActiveDofs(const double x) const
    {
      assert(isDestroyed==false);
      
      // locate the span containing 'x'
      const int span_num = static_cast<int>((x-xinit)/span_length);
      assert(span_num>=0 && span_num<num_spans);
      assert(x>=xinit+span_num*span_length && x<=xinit+(span_num+1)*span_length);

      // get the local dofs and shape function values within this span
      auto dof_shp_pairs = ohe_spans[span_num]->GetActiveDofs(x);

      // map local dof# to global dof#
      for(int i=0; i<4; ++i)
	{
	  int local_dof = dof_shp_pairs[i].first;
	  auto it = contact_wire_dof_maps[span_num].find(local_dof);
	  assert(it!=contact_wire_dof_maps[i].end());
	  dof_shp_pairs[i].first = it->second;
	}

      // done
      return dof_shp_pairs;
    }

  
  // Visualize the configuration
  template<typename OHEtype>
    void MultiSpan_OHE_base<OHEtype>::Write(const double* displacements,
					    const std::string catenary_fname,
					    const std::string contact_fname,
					    const std::string dropper_fname) const
    {
      assert(isDestroyed==false);
      
      PetscErrorCode ierr;
      const int num_dofs_per_span = ohe_spans[0]->GetTotalNumDof();
      
      // write span-by-span
      const int* indices;
      int num_indices;
      std::vector<double> span_dofs(num_dofs_per_span);
      for(int i=0; i<num_spans; ++i)
	{
	  const auto& is = ohe_IS[i];
	
	  // dof indices for this span
	  ierr = ISGetSize(is, &num_indices);     CHKERRV(ierr);
	  assert(num_indices==num_dofs_per_span);
	  ierr = ISGetIndices(is, &indices);      CHKERRV(ierr);

	  // copy dofs for this span
	  for(int j=0; j<num_indices; ++j)
	    span_dofs[j] = displacements[indices[j]];

	  // write this span
	  ohe_spans[i]->Write(span_dofs.data(), catenary_fname, contact_fname, dropper_fname);

	  // restore
	  ierr = ISRestoreIndices(is, &indices); CHKERRV(ierr);
	}

      // done
      return;
    }


  // Visualize the configuration
  template<typename OHEtype>
    void MultiSpan_OHE_base<OHEtype>::Write(const double* disp, const double* vel, const double* accn,
					    const std::string catenary_fname,
					    const std::string contact_fname,
					    const std::string dropper_fname) const
    {
      assert(isDestroyed==false);
      
      PetscErrorCode ierr;
      const int num_dofs_per_span = ohe_spans[0]->GetTotalNumDof();
      
      // write span-by-span
      const int* indices;
      int num_indices;
      std::vector<double> span_disp(num_dofs_per_span);
      std::vector<double> span_vel(num_dofs_per_span);
      std::vector<double> span_accn(num_dofs_per_span);
      for(int i=0; i<num_spans; ++i)
	{
	  const auto& is = ohe_IS[i];
	
	  // dof indices for this span
	  ierr = ISGetSize(is, &num_indices);     CHKERRV(ierr);
	  assert(num_indices==num_dofs_per_span);
	  ierr = ISGetIndices(is, &indices);      CHKERRV(ierr);

	  // copy dofs for this span
	  for(int j=0; j<num_indices; ++j)
	    {
	      span_disp[j] = disp[indices[j]];
	      span_vel[j]  = vel[indices[j]];
	      span_accn[j] = accn[indices[j]];
	    }

	  // write this span
	  ohe_spans[i]->Write(span_disp.data(), span_vel.data(), span_accn.data(),
			      catenary_fname, contact_fname, dropper_fname);

	  // restore
	  ierr = ISRestoreIndices(is, &indices); CHKERRV(ierr);
	}

      // done
      return;
    }


  // Number of nonzero's per row
  template<typename OHEtype>
    std::vector<int> MultiSpan_OHE_base<OHEtype>::GetSparsity() const
    {
      assert(isDestroyed==false);
      
      PetscErrorCode ierr;
      std::vector<int> sparsity(nTotalDof);
      std::fill(sparsity.begin(), sparsity.end(), 0);
    
      // Update sparsity span-by-span
      const int num_dofs_per_span = ohe_spans[0]->GetTotalNumDof();
      const int* indices;
      for(int i=0; i<num_spans; ++i)
	{
	  // sparsity of this span
	  const auto span_sparsity = ohe_spans[i]->GetSparsity();
	  assert(static_cast<int>(span_sparsity.size())==num_dofs_per_span);

	  // update with global indices
	  const auto& is = ohe_IS[i];
	  ierr = ISGetIndices(is, &indices);     //CHKERRV(ierr);
	  for(int j=0; j<num_dofs_per_span; ++j)
	    sparsity[indices[j]] += span_sparsity[j];
	  ierr = ISRestoreIndices(is, &indices); //CHKERRV(ierr);
	}

      // done
      return std::move(sparsity);
    }


  // Access dropper dofs for a given span
  template<typename OHE_TYPE>
    std::vector<std::pair<int,int>>
    MultiSpan_OHE_base<OHE_TYPE>::GetDropperDofs(const int span_num) const
  {
    assert(isDestroyed==false);
    assert(span_num>=0 && span_num<num_spans);

    const int nDroppers = ohe_spans[span_num]->GetDropperArrangement().nDroppers;
    std::vector<std::pair<int,int>> dropper_dofs(nDroppers);
    const int* dof_indices;
    ISGetIndices(ohe_IS[span_num], &dof_indices);
    for(int d=0; d<nDroppers; ++d)
      {
	// span-wise local dof numbers
	auto dof_pair = ohe_spans[span_num]->GetDropperDofPair(d);

	// global dof numbers
	dropper_dofs[d].first  = dof_indices[dof_pair.first];
	dropper_dofs[d].second = dof_indices[dof_pair.second];
      }
    ISRestoreIndices(ohe_IS[span_num], &dof_indices);

    // done
    return dropper_dofs;
  }


  // Map contact wire node to global dof number
  template<typename OHE_TYPE>
    int MultiSpan_OHE_base<OHE_TYPE>::
    MapContactWireNodeToDof(const int span_num, const int node) const
    {
      assert(isDestroyed==false);
      assert(span_num>=0 && span_num<num_spans);
      int local_dofnum = ohe_spans[span_num]->MapContactWireNodeToDof(0, node);
      auto it = contact_wire_dof_maps[span_num].find(local_dofnum);
      assert(it!=contact_wire_dof_maps[span_num].end());
      return it->second;
    }
  
}
