
/** \file pfe_MultiSpan_OHE_base.h
 * \brief Defines the class pfe::MultiSpan_OHE_base encapsulating multiple span OHE
 * \author Ramsharan Rangarajan
 */


#pragma once

#include <pfe_SingleSpan_OHE_base.h>
#include <map>

namespace pfe
{
  //! Multiple OHE spans
  //! \ingroup components
  template<typename OHEtype>
    class MultiSpan_OHE_base
    {
    public:
      //! Constructor
      MultiSpan_OHE_base(const double x0,  const OHESpan_ParameterPack& pack,
			 const int nspans, const int nelm_per_interval);

      //! Destructor
      virtual ~MultiSpan_OHE_base();

      //! Disable copy and assignment
      MultiSpan_OHE_base(const MultiSpan_OHE_base&) = delete;
      MultiSpan_OHE_base operator=(const MultiSpan_OHE_base&) = delete;

      // Destroy
      virtual void Destroy();
      
      //! Access the number of spans
      inline int GetNumSpans() const
      { return num_spans; }
      
      //! Access a specified OHE span
      inline OHEtype& GetSpan(const int num)
      {
	assert(num>=0 && num<num_spans);
	return *ohe_spans[num];
      }
      
       //! Access a specified OHE span
      inline const OHEtype& GetSpan(const int num) const
      {
	assert(num>=0 && num<num_spans);
	return *ohe_spans[num];
      }

      //! Access the total number of dofs
      inline int GetTotalNumDof() const
      { return nTotalDof; }

      //! Access dropper dofs for a given span
      std::vector<std::pair<int,int>> GetDropperDofs(const int span_num) const;

      //! Map contact wire node to global dof number
      int MapContactWireNodeToDof(const int span_num, const int node) const;
      
      //! Get the active displacement dofs and shape function values at a given point along the span
      std::array<std::pair<int,double>,2> GetActiveDisplacementDofs(const double x) const;

      //! Get the active dofs and shape function values at a given point along the span
      std::array<std::pair<int,double>,4> GetActiveDofs(const double x) const;
    
      // Number of nonzero's per row
      std::vector<int> GetSparsity() const;
    
      // Visualize the configuration
      void Write(const double* displacements,
		 const std::string catenary_fname,
		 const std::string contact_fname,
		 const std::string dropper_fname) const;

      void Write(const double* disp, const double* vel, const double* accn,
		 const std::string catenary_fname,
		 const std::string contact_fname,
		 const std::string dropper_fname) const;

      inline void Write(const double* disp, const double* vel, const double* accn,
			const std::vector<std::string> fnames) const
      {
	assert(static_cast<int>(fnames.size())==3);
	Write(disp, vel, accn, fnames[0], fnames[1], fnames[2]);
      }
      
    protected:
      bool isDestroyed;                       
      const double                 xinit;       //! start of the first span
      const int                    num_spans;   //! number of spans
      const double                 span_length; //! length of each span
      std::vector<OHEtype*>        ohe_spans;   //! vector of OHE spans
      std::vector<IS>              ohe_IS;      //! corresponding vector of index sets
      int                          nTotalDof;   //! total number of dofs
    
      std::vector<std::map<int,int>> contact_wire_dof_maps; //!< span-wise local to global map for contact wire dofs
    };
}


#include <pfe_MultiSpan_OHE_base_impl.h>
