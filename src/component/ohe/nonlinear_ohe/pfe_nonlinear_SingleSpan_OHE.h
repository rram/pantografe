
/** \file pfe_nonlinear_SingleSpan_OHE.h
 * \brief Defines the class pfe::nonlinear::SingleSpan_OHE encapsulating a single span of the OHE with nonlinear droppers
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_SingleSpan_OHE_base.h>
#include <pfe_NonlinearDropper.h>

namespace pfe
{
  namespace nonlinear
  {
    class SingleSpan_OHE: public SingleSpan_OHE_base
    {
    public:
      //! Constructor
      SingleSpan_OHE(const double x0, const OHESpan_ParameterPack& pack,
		     const double dropper_alpha, const int nelm);

      //! Constructor that allows for different spring stiffnesses at the left and right ends of the spans
      //! of the catenary cable and contact wire and delayed setting of the slack parameter
      SingleSpan_OHE(const double x0,
		     const CatenaryWireParams&      catenaryParams,
		     const ContactWireParams&       wireParams,
		     const DropperParams&           dropperParams,
		     const DropperArrangement&      dropper_schedule,
		     const SuspensionSpringParams& left_catenary_spring_params,
		     const SuspensionSpringParams& right_catenary_spring_params,
		     const SuspensionSpringParams& left_contact_spring_params,
		     const SuspensionSpringParams& right_contact_spring_params,
		     const int nelm);

      //! Destructor
      virtual ~SingleSpan_OHE();

      //! Destroy
      virtual void Destroy() override;
      
      // Disable copy and assignment
      SingleSpan_OHE(const SingleSpan_OHE&) = delete;
      SingleSpan_OHE operator=(const SingleSpan_OHE&) = delete;
    
      //! Access this span
      inline virtual const SingleSpan_OHE& GetSpan(const int s) const
      { assert(isDestroyed==false);
	return *this; }

      //! Access this span
      inline virtual SingleSpan_OHE& GetSpan(const int s)
      { assert(isDestroyed==false);
	return *this; }
      
      //! Access droppers
      inline const std::vector<NonlinearDropper*>& GetDroppers() const
      { assert(isDestroyed==false);
	return droppers; }

      //! Set slack parameter
      inline void SetSlackParameter(const double alpha)
      {
	assert(isDestroyed==false);
	for(auto d:droppers)
	  d->SetSlackParameter(alpha);
	is_SlackParam_Set = true;
      }
      
      //! Main functionality: evaluate contribution to mass, stiffness and the residual vector
      void Evaluate(const Vec& U, Mat& M, Mat& K, Vec& Res) const;
    
      //! Reset dropper lengths
      void ResetDropperLengths(const double* lengths);

      //! Visualize the configuration
      void Write(const double* displacements,
		 const std::string catenary_fname,
		 const std::string contact_fname,
		 const std::string dropper_fname) const override;

      void Write(const double* disp, const double* vel, const double* accn,
		 const std::string catenary_fname,
		 const std::string contact_fname,
		 const std::string dropper_fname) const override;

      inline  void Write(const double* disp, const double* vel, const double* accn,
			 const std::vector<std::string> ohe_fnames) const override
      {
	assert(static_cast<int>(ohe_fnames.size())==3);
	Write(disp, vel, accn, ohe_fnames[0], ohe_fnames[1], ohe_fnames[2]);
      }
      
    private:
      bool is_SlackParam_Set;
      std::vector<NonlinearDropper*> droppers;
      mutable PetscData cablePD;
    };
  
  }
}
