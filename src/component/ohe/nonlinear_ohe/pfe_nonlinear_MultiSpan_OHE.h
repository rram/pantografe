
/** \file pfe_nonlinear_MultiSpan_OHE.h
 * \brief Defines the class pfe::nonlinear::MultiSpan_OHE encapsulating multiple OHE spans with nonlinear droppers
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_MultiSpan_OHE_base.h>
#include <pfe_nonlinear_SingleSpan_OHE.h>

namespace pfe
{
  namespace nonlinear
  {
    class MultiSpan_OHE: public MultiSpan_OHE_base<SingleSpan_OHE>
    {
    public:
      //! Constructor
      MultiSpan_OHE(const double x0,  const OHESpan_ParameterPack& pack,
		    const int nspans, const int nelm_per_interval, const double slack_param);
      
      //! Destructor
      inline virtual ~MultiSpan_OHE() {
	assert(isDestroyed==true); }

      //! Disable copy and assignment
      MultiSpan_OHE(const MultiSpan_OHE&) = delete;
      MultiSpan_OHE operator=(const MultiSpan_OHE&) = delete;

      // Main functionlity: evaluate contribution to mass, stiffness and force vectors
      void Evaluate(const Vec& U, Mat& M, Mat& K, Vec& R) const;
    };
	
  }
}
