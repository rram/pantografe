
/** \file pfe_nonlinear_MultiSpan_OHE.cpp
 * \brief Implements the class pfe::nonlinear::MultiSpan_OHE 
 * \author Ramsharan Rangarajan
 */


#include <pfe_nonlinear_MultiSpan_OHE.h>

namespace pfe
{
  namespace nonlinear
  {
    // Constructor
    MultiSpan_OHE::MultiSpan_OHE(const double x0,  const OHESpan_ParameterPack& pack,
				 const int nspans, const int nelm_per_interval,
				 const double slack_param)
      :MultiSpan_OHE_base<SingleSpan_OHE>(x0, pack, nspans, nelm_per_interval)
    {
      // set the slack parameter in droppers of all span
      for(int s=0; s<num_spans; ++s)
	ohe_spans[s]->SetSlackParameter(slack_param);
    }
    
    
    // Main functionlity: evaluate contribution to mass, stiffness and force vectors
    void MultiSpan_OHE::Evaluate(const Vec& U, Mat& M, Mat& K, Vec& R) const
    {
      assert(isDestroyed==false);
      
      PetscErrorCode ierr;

      // Permit new nonzeros
      ierr = MatSetOption(M, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
      ierr = MatSetOption(K, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
    
      // Assemble contributions 1-span at a time
      Vec U_span;
      Mat M_span;
      Mat K_span;
      Vec R_span;

      for(int i=0; i<num_spans; ++i)
	{
	  // extract sub-matrices/sub-vectors for this span
	  const auto& is = ohe_IS[i];

	  ierr = VecGetSubVector(U, is, &U_span);          CHKERRV(ierr);
	  ierr = MatGetLocalSubMatrix(M, is, is, &M_span); CHKERRV(ierr);
	  ierr = MatGetLocalSubMatrix(K, is, is, &K_span); CHKERRV(ierr);
	  ierr = VecGetSubVector(R, is, &R_span);          CHKERRV(ierr);

	  // assemble contributions from this span
	  ohe_spans[i]->Evaluate(U_span, M_span, K_span, R_span);

	  // restore
	  ierr = VecRestoreSubVector(R, is, &R_span);          CHKERRV(ierr);
	  ierr = MatRestoreLocalSubMatrix(K, is, is, &K_span); CHKERRV(ierr);
	  ierr = MatRestoreLocalSubMatrix(M, is, is, &M_span); CHKERRV(ierr);
	  ierr = VecRestoreSubVector(U, is, &U_span);          CHKERRV(ierr);
	}

      // Complete assembly
      ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
      ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
      ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
      ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
      ierr = VecAssemblyBegin(R);                     CHKERRV(ierr);
      ierr = VecAssemblyEnd(R);                       CHKERRV(ierr);

      // done
      return;
    }

    
  }
}
