
/** \file pfe_nonlinear_SingleSpan_OHE.cpp
 * \brief Implements the class pfe::nonlinear::SingleSpan_OHE
 * \author Ramsharan Rangarajan
 */

#include <pfe_nonlinear_SingleSpan_OHE.h>

namespace pfe
{
  namespace nonlinear
  {
    // Constructor
    SingleSpan_OHE::SingleSpan_OHE(const double x0,
				   const OHESpan_ParameterPack& pack,
				   const double dropper_alpha,
				   const int nelm)
      :SingleSpan_OHE(x0, pack.catenaryParams, pack.wireParams, pack.dropperParams, pack.dropperSchedule,
		      pack.catenary_spring_params, pack.catenary_spring_params, 
		      pack.contact_spring_params, pack.contact_spring_params,
		      nelm)
    {
      SetSlackParameter(dropper_alpha);
      is_SlackParam_Set = true;
    }
    
    // Constructor
    SingleSpan_OHE::SingleSpan_OHE(const double x0,
				   const CatenaryWireParams&      catenaryParams,
				   const ContactWireParams&       wireParams,
				   const DropperParams&           dropperParams,
				   const DropperArrangement&      dropper_schedule,
				   const SuspensionSpringParams& left_catenary_spring_params,
				   const SuspensionSpringParams& right_catenary_spring_params,
				   const SuspensionSpringParams& left_contact_spring_params,
				   const SuspensionSpringParams& right_contact_spring_params,
				   const int nelm)
    :SingleSpan_OHE_base(x0, catenaryParams, wireParams, dropperParams, dropper_schedule,
			 left_catenary_spring_params, right_catenary_spring_params, 
			 left_contact_spring_params, right_contact_spring_params,
			 nelm),
      is_SlackParam_Set(false)
    {
      // pre-compute cable stiffness (no dropper contribution)
      PetscErrorCode ierr;
      Mat Mc;
      const int ndof = GetTotalNumDof();
      auto nz = GetSparsity();
      cablePD.Initialize(nz);
      ierr = VecZeroEntries(cablePD.resVEC); CHKERRV(ierr);
      ierr = MatZeroEntries(cablePD.stiffnessMAT); CHKERRV(ierr);
      ierr = MatZeroEntries(cablePD.massMAT); CHKERRV(ierr);
      SingleSpan_OHE_base::Evaluate(cablePD.massMAT, cablePD.stiffnessMAT, cablePD.resVEC); 
      
      // create droppers
      const int& nDroppers = dropper_schedule.nDroppers;
      droppers.resize(nDroppers);
      for(int d=0; d<nDroppers; ++d)
	{
	  auto top_bot_dofs = GetDropperDofPair(d);
	  droppers[d] = new NonlinearDropper(dropperParams, dropperSchedule.nominal_lengths[d], 
					     dropper_schedule.zEncumbrance, top_bot_dofs.first, top_bot_dofs.second);
	}
    }

    // Destroy
    void SingleSpan_OHE::Destroy()
    {
      assert(isDestroyed==false);
      SingleSpan_OHE_base::Destroy();
      cablePD.Destroy();
      for(auto& d:droppers)
	delete d;
      assert(isDestroyed==true);
    }
    
    // Destructor
    SingleSpan_OHE::~SingleSpan_OHE()
    {
      assert(isDestroyed==true);
    }
  
    // Reset dropper lengths
    void SingleSpan_OHE::ResetDropperLengths(const double* lengths)
    {
      assert(isDestroyed==false);
      const int nDroppers = static_cast<int>(droppers.size());
      for(int d=0; d<nDroppers; ++d)
	{
	  assert(lengths[d]>0.);
	  droppers[d]->SetLength(lengths[d]);
	}
      return;
    }

  
    // Main functionality: evaluate contribution to mass, stiffness, and residal vectors
    void SingleSpan_OHE::Evaluate(const Vec& U, Mat& M, Mat& K, Vec& Res) const
    {
      assert(isDestroyed==false);
      assert(is_SlackParam_Set==true);
      
      // Recompute contribution from cables since matrix/vector operations
      // are no supported by mat = localref
      PetscErrorCode ierr;
      auto& F  = cablePD.resVEC;
      auto& Kc = cablePD.stiffnessMAT;
      ierr = VecZeroEntries(F); CHKERRV(ierr);
      SingleSpan_OHE_base::Evaluate(M, K, F);

      // R += Kc U-F
      ierr = VecScale(F, -1); CHKERRV(ierr);
      ierr = MatMultAdd(Kc, U, F, F); CHKERRV(ierr);
      ierr = VecAXPY(Res, 1., F);      CHKERRV(ierr);

      // Permit new nonzeros in M and K
      ierr = MatSetOption(M, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
      ierr = MatSetOption(K, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);

      // Add contribution from droppers
      double* uarray;
      ierr = VecGetArray(U, &uarray);     CHKERRV(ierr);
      for(auto& d:droppers)
	d->Evaluate(uarray, M, K, Res);
      ierr = VecRestoreArray(U, &uarray); CHKERRV(ierr);
    
      // done
      return;
    }
  

    // Visualize the configuration
    void SingleSpan_OHE::Write(const double* displacements,
			       const std::string catenary_fname,
			       const std::string contact_fname,
			       const std::string dropper_fname) const
    {
      // catenary cable
      catenary_cable->Write(displacements, catenary_fname);

      // contact wire
      const int nCatenaryCableDofs = catenary_L2GMap->GetTotalNumDof();
      contact_wire->Write(displacements+nCatenaryCableDofs, contact_fname);

      // droppers
      const auto& coordinates = catenary_cable->GetCoordinates();
      const auto dropperNodes = GetDropperNodes();
      const int nDroppers = static_cast<int>(dropperNodes.size());
      const auto& catenary_params = catenary_cable->GetParameters();
      const auto& contact_params  = contact_wire->GetParameters();
      std::fstream pfile;
      pfile.open(dropper_fname.c_str(), std::ios::app|std::ios::out);
      assert(pfile.good());
      for(int n=0; n<nDroppers; ++n)
	{
	  const int& node = dropperNodes[n];
	  bool is_slack = droppers[n]->IsSlack(displacements); // true if slack, false otherwise
	  pfile << coordinates[node] <<" "<< catenary_params.zLevel + displacements[node] << " " << static_cast<int>(!is_slack) // not slack = 1, slack = 0
		<< std::endl
	    << coordinates[node] <<" "<< contact_params.zLevel + displacements[nCatenaryCableDofs+node] << " " << static_cast<int>(!is_slack)
		<< std::endl
		<< std::endl;
	}
      pfile.close();

      return;
    }


    // proxy for displacement, velocity and acceleration
    void SingleSpan_OHE::Write(const double* disp, const double* vel, const double* accn,
			       const std::string catenary_fname,
			       const std::string contact_fname,
			       const std::string dropper_fname) const
    {
      // catenary cable
      catenary_cable->Write(disp, vel, accn, catenary_fname);

      // contact wire
      const int nCatenaryCableDofs = catenary_L2GMap->GetTotalNumDof();
      contact_wire->Write(disp+nCatenaryCableDofs, vel+nCatenaryCableDofs, accn+nCatenaryCableDofs, contact_fname);

      // droppers: write only field 1
      const auto& coordinates = catenary_cable->GetCoordinates();
      const auto dropperNodes = GetDropperNodes();
      const int nDroppers = static_cast<int>(dropperNodes.size());
      const auto& catenary_params = catenary_cable->GetParameters();
      const auto& contact_params  = contact_wire->GetParameters();
      std::fstream pfile;
      pfile.open(dropper_fname.c_str(), std::ios::app|std::ios::out);
      assert(pfile.good());
      for(int n=0; n<nDroppers; ++n)
	{
	  const int& node = dropperNodes[n];
	  bool is_slack = droppers[n]->IsSlack(disp); // true if slack, false otherwise
	  pfile << coordinates[node] <<" "<< catenary_params.zLevel + disp[node] << " " << static_cast<int>(!is_slack)
		<< std::endl
	    << coordinates[node] <<" "<< contact_params.zLevel + disp[nCatenaryCableDofs+node] << " " << static_cast<int>(!is_slack)
		<< std::endl
		<< std::endl;
	}
      pfile.close();
    }

  }
}
