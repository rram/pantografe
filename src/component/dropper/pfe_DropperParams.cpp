
/** \file pfe_DropperParams.h
 * \brief Implements the struct pfe::DropperParams
 * \author Ramsharan Rangarajan
 */

#include <pfe_DropperParams.h>
#include <pfe_json.hpp>

namespace pfe
{
  // write parameters to json file
  std::ostream& operator << (std::ostream &out, const DropperParams& params)
  {
    assert(params.tag.empty()==false);
    
    nlohmann::json j;
    j[params.tag]["EA"] = params.EA;
    j[params.tag]["line density"] = params.rhoA;
    j[params.tag]["catenary cable clamp mass"] = params.Mcm;
    j[params.tag]["contact wire clamp mass"] = params.Mcc;
    j[params.tag]["gravity"] = params.g;

    out << std::endl << std::endl << "\"" << params.tag << "\"" << " : " << std::setw(8)<<j[params.tag];
    return out;
  }

  // read parameters from json file
  std::istream& operator >> (std::istream &in, DropperParams &params)
  {
    assert(params.tag.empty()==false);
    
    // rewind the file
    in.seekg(0, std::ios::beg);

    // parse
    auto J = nlohmann::json::parse(in);
    auto it = J.find(params.tag);
    assert(it!=J.end());
    auto& j = *it;

    // checks
    assert(j.empty()==false);
    assert(j.contains("EA"));
    assert(j.contains("line density"));
    assert(j.contains("gravity"));
    assert(j.contains("catenary cable clamp mass"));
    assert(j.contains("contact wire clamp mass"));
    

    // read. use get_to() to enforce type
    j["EA"].get_to(params.EA);
    j["line density"].get_to(params.rhoA);
    j["catenary cable clamp mass"].get_to(params.Mcm);
    j["contact wire clamp mass"].get_to(params.Mcc);
    j["gravity"].get_to(params.g);

    return in;
  }

  // Verify dropper arrangement
  void DropperArrangement::Validate() const
  {
    assert(tag.empty()==false);
    assert(nDroppers>=1);
    assert(zEncumbrance>0.);
    assert(static_cast<int>(nominal_lengths.size())==nDroppers);
    assert(static_cast<int>(coordinates.size())==nDroppers);
    assert(static_cast<int>(target_sag.size())==nDroppers);
    for(int d=0; d<nDroppers; ++d)
      {
	assert(nominal_lengths[d]>0.);
	if(d!=0)
	  assert(coordinates[d]>coordinates[d-1]);
      }
    return;
  }

  // print dropper arrangement to json file
  std::ostream& operator << (std::ostream &out, const DropperArrangement& params)
  {
    params.Validate();

    nlohmann::json j;
    j[params.tag]["number of droppers"] = params.nDroppers;
    j[params.tag]["encumbrance"] = params.zEncumbrance;
    j[params.tag]["dropper positions"] = params.coordinates;
    j[params.tag]["target sag"] = params.target_sag;
    j[params.tag]["nominal lengths"] = params.nominal_lengths;

    out << std::endl << std::endl << "\"" << params.tag << "\"" << " : " << std::setw(8) << j[params.tag];
    return out;
  }

  // read dropper arrangement from json file
  std::istream& operator >> (std::istream &in, DropperArrangement& params)
  {
    assert(params.tag.empty()==false);

    // rewind the file
    in.seekg(0, std::ios::beg);

    // parse
    auto J = nlohmann::json::parse(in);
    auto it = J.find(params.tag);
    assert(it!=J.end());
    auto& j = *it;
    
    // checks
    assert(j.empty()==false);
    assert(j.contains("number of droppers"));
    assert(j.contains("encumbrance"));
    assert(j.contains("dropper positions"));
    assert(j.contains("target sag"));
    assert(j.contains("nominal lengths"));

    // read. use get_to to enforce type
    j["number of droppers"].get_to(params.nDroppers);
    j["encumbrance"].get_to(params.zEncumbrance);
    j["dropper positions"].get_to(params.coordinates);
    j["target sag"].get_to(params.target_sag);
    j["nominal lengths"].get_to(params.nominal_lengths);

    // check data read
    params.Validate();
    
    return in;
  }
  
}
