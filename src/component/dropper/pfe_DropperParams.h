
/** \file pfe_DropperParams.h
 * \brief Defines the struct pfe::DropperParams
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <string>
#include <fstream>
#include <vector>

namespace pfe
{
  //! Struct defining parameters of an inter-cable dropper
  //! \ingroup components
  struct DropperParams
  {
    std::string tag;
    double EA;
    double rhoA; // Mass per unit length of the dropper
    double Mcm; // Clamp weight on the messenger wire 
    double Mcc; // Clamp weight on the contact wire
    double g;  // Acceleration due to gravity

    // print to json file
    friend std::ostream& operator << (std::ostream &out, const DropperParams& params);

    // read from json file
    friend std::istream& operator >> (std::istream &in, DropperParams& params);
  };


  //! Struct defining dropper arrangement
  struct DropperArrangement
  {
    std::string tag; 
    int nDroppers;
    double zEncumbrance; // z(catenary) - z(contact wire)
    std::vector<double> nominal_lengths;
    std::vector<double> coordinates;
    std::vector<double> target_sag;
    void Validate() const;
    
    // print to json file
    friend std::ostream& operator << (std::ostream &out, const DropperArrangement& params);

    // read from json file
    friend std::istream& operator >> (std::istream &in, DropperArrangement& params);
  };
}
