
/** \file test_DropperParams.cpp
 * \brief Unit test for read/write of parameters using the json format
 * \author Ramsharan Rangarajan
 * \comp_test Unit test for read/write of parameters using the json format
 */


#include <pfe_DropperParams.h>
#include <random>
#include <cassert>


void Generate(pfe::DropperParams& params);

void Generate(pfe::DropperArrangement& params);

void Compare(const pfe::DropperParams& A,
	     const pfe::DropperParams& B,
	     const double tol);

void Compare(const pfe::DropperArrangement& A,
	     const pfe::DropperArrangement& B,
	     const double tol);

int main()
{
  std::fstream jfile;
  jfile.open((char*)"datasheet.json", std::ios::out);
  jfile << "{" << std::endl;
  // Write 
  // droppers
  pfe::DropperParams dropper_params;
  dropper_params.tag = "droppers";
  Generate(dropper_params);
  jfile << dropper_params << "," ;

  // dropper arrangement
  pfe::DropperArrangement dropper_schedule;
  dropper_schedule.tag = "dropper schedule";
  Generate(dropper_schedule);
  jfile << dropper_schedule;
  jfile << "}" << std::endl;
  jfile.close();

  // Read & compare
  jfile.open((char*)"datasheet.json", std::ios::in);

  pfe::DropperArrangement schedule_copy;
  schedule_copy.tag = "dropper schedule";
  jfile >> schedule_copy;
  Compare(dropper_schedule, schedule_copy, 1.e-6);
    
  pfe::DropperParams dropper_copy;
  dropper_copy.tag = "droppers";
  jfile >> dropper_copy;
  Compare(dropper_params, dropper_copy, 1.e-6);
  
  jfile.close();
}


void Generate(pfe::DropperParams& params)
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(1.0, 100.0);
  params.EA = dis(gen);
  params.rhoA = dis(gen);
  params.Mcm = dis(gen);
  params.Mcc = dis(gen);
  params.g = dis(gen);
  return;
}


void Generate(pfe::DropperArrangement& params)
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(1.0, 100.0);
  params.nDroppers = 1+static_cast<int>(dis(gen));
  params.zEncumbrance = dis(gen);
  params.nominal_lengths.resize(params.nDroppers);
  params.coordinates.resize(params.nDroppers);
  params.target_sag.resize(params.nDroppers);
  for(int d=0; d<params.nDroppers; ++d)
    {
      params.nominal_lengths[d] = dis(gen);
      params.target_sag[d] = dis(gen);
      params.coordinates[d] = dis(gen);
      if(d>0)
	params.coordinates[d] += params.coordinates[d-1];
    }
  return;
}


void Compare(const pfe::DropperParams& A,
	     const pfe::DropperParams& B,
	     const double tol)
{
  assert(A.tag==B.tag);
  assert(std::abs(A.EA-B.EA)<tol);
  assert(std::abs(A.rhoA-B.rhoA)<tol);
  assert(std::abs(A.Mcm-B.Mcm)<tol);
  assert(std::abs(A.Mcc-B.Mcc)<tol);
  assert(std::abs(A.g-B.g)<tol);
  return;
}


void Compare(const pfe::DropperArrangement& A,
	     const pfe::DropperArrangement& B,
	     const double tol)
{
  A.Validate();
  B.Validate();
  assert(A.tag==B.tag);
  assert(A.nDroppers==B.nDroppers);
  assert(std::abs(A.zEncumbrance-B.zEncumbrance)<tol);
  for(int d=0; d<A.nDroppers; ++d)
    {
      assert(std::abs(A.target_sag[d]-B.target_sag[d])<tol);
      assert(std::abs(A.nominal_lengths[d]-B.nominal_lengths[d])<tol);
      assert(std::abs(A.coordinates[d]-B.coordinates[d])<tol);
    }
  return;
}
