
/** \file pfe_LinearDropper.h
 * \brief Defines the class pfe::LinearDropper
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_Dropper_base.h>

namespace pfe
{
  //namespace linear
  //  {
  //! Class defining a dropper between a pair of structures
  //! \ingroup components
  class LinearDropper: public Dropper_base
  {
  public:
    //! Constructor
    inline LinearDropper(const DropperParams& dp,
			 const double len,
			 const double encumbrance,
			 const int dofTop, const int dofBot)
      :Dropper_base(dp, len, encumbrance, dofTop, dofBot) {}
	
    //! Destructor
    inline virtual ~LinearDropper() {}

    //! Disable copy and assignment
    LinearDropper(const LinearDropper&) = delete;
    LinearDropper& operator=(const LinearDropper&) = delete;
    
    //! Assemble contributions to the mass, stiffness and force vector
    void Evaluate(Mat& M, Mat& K, Vec& F);
  };
  
  //}
}
