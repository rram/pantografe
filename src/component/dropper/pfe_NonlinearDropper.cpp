
/** \file pfe_NonlinearDropper.cpp
 * \brief Implements the class pfe::NonlinearDropper
 * \author Ramsharan Rangarajan
 */

#include <pfe_NonlinearDropper.h>
#include <cassert>

namespace pfe
{
  // Evaluate contributions to the mass, stiffness and residual
  void NonlinearDropper::Evaluate(const double* dofvalues, Mat& M, Mat &K, Vec& R)
  {
    assert(is_SlackParam_Set==true);
    PetscErrorCode ierr;
    
    // Add intertia of
    // (i) clamp + 1/2 mass of dropper to the attachment point on the messenger wire
    // (ii) clamp + 1/2 mass of dropper to the attachement point on the contact wire
    // This contribution is identical to that of a linear dropper
    double mass;
    mass = params.Mcm + 0.5*params.rhoA*length;
    ierr = MatSetValuesLocal(M, 1, &top_dofNum, 1, &top_dofNum, &mass, ADD_VALUES); CHKERRV(ierr);
    mass = params.Mcc + 0.5*params.rhoA*length;
    ierr = MatSetValuesLocal(M, 1, &bot_dofNum, 1, &bot_dofNum, &mass, ADD_VALUES); CHKERRV(ierr);
    ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);

    // u = ztop+wtop - (zbot+wbot) - Length
    //   = (zc-Length) + (wtop-wbot)
    // Elastic energy: (1/2) k phi(u) u^2; 
    // k = EA/L, phi(u) = 1/2 (1+tanh(alpha u))
    // gravity: rhoA L (wtop+wbot)/2
    // elastic residual: k phi(u) u [1; -1], ignoring derivative of phi(u)
    // gravity residual: (rhoA L/2)[1; 1]
    const double& wtop = dofvalues[top_dofNum];
    const double& wbot = dofvalues[bot_dofNum];
    const double u     = (zEncumbrance-length) + (wtop-wbot);
    const double tanh  = std::tanh(alpha*u);
    const double phi   = 0.5*(1.+tanh);
    const double Kspr  = (params.EA/length)*phi;

    double value;
    // top: du/dw_t = 1
    value = Kspr*u*1.0 + 0.5*params.rhoA*params.g*length + params.Mcm*params.g;
    ierr = VecSetValuesLocal(R, 1, &top_dofNum, &value, ADD_VALUES); CHKERRV(ierr);
    // bot: du/dw_b = -1 
    value = Kspr*u*(-1.0) + 0.5*params.rhoA*params.g*length + params.Mcc*params.g;
    ierr = VecSetValuesLocal(R, 1, &bot_dofNum, &value, ADD_VALUES); CHKERRV(ierr);
    // finish residual assembly
    ierr = VecAssemblyBegin(R); CHKERRV(ierr);
    ierr = VecAssemblyEnd(R);   CHKERRV(ierr);

    // stiffness -> linearization of the residual
    const double dtanh = alpha*(1.-tanh*tanh);
    const double dphi  = 0.5*dtanh;
    const double dKspr = (params.EA/length)*dphi;  // dKspr/du
    value = dKspr*u + Kspr;
    ierr  = MatSetValuesLocal(K, 1, &top_dofNum, 1, &top_dofNum, &value, ADD_VALUES); CHKERRV(ierr); // Ktt
    ierr  = MatSetValuesLocal(K, 1, &bot_dofNum, 1, &bot_dofNum, &value, ADD_VALUES); CHKERRV(ierr); // Kbb
    value *= -1.;
    ierr  = MatSetValuesLocal(K, 1, &top_dofNum, 1, &bot_dofNum, &value, ADD_VALUES); CHKERRV(ierr); // Ktb
    ierr  = MatSetValuesLocal(K, 1, &bot_dofNum, 1, &top_dofNum, &value, ADD_VALUES); CHKERRV(ierr); // Kbt
    // finish assembly of K
    ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);

    // done
    return;
  }


  // Returns whether the dropper is slack
  bool NonlinearDropper::IsSlack(const double* dofvalues) const
  {
    // u = ztop+wtop - (zbot+wbot) - Length
    //   = (zc-Length) + (wtop-wbot)
    const double& wtop = dofvalues[top_dofNum];
    const double& wbot = dofvalues[bot_dofNum];
    const double u     = (zEncumbrance-length) + (wtop-wbot);
    return (u>0.) ? false : true;
  }
  
}
