
/** \file pfe_NonlinearDropper.h
 * \brief Defines the class pfe::NonlinearDropper
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_Dropper_base.h>

namespace pfe
{
  //! Class defining a dropper between a pair of structures
  //! \ingroup components
  class NonlinearDropper: public Dropper_base
  {
  public:
    //! Constructor
    inline NonlinearDropper(const DropperParams& dp,
			    const double len,
			    const double slack_param,
			    const double encumbrance,
			    const int dofTop, const int dofBot)
      :Dropper_base(dp, len, encumbrance, dofTop, dofBot),
      alpha(slack_param),
      is_SlackParam_Set(true) {}

    //! Constructor
    inline NonlinearDropper(const DropperParams& dp,
			    const double len,
			    const double encumbrance,
			    const int dofTop, const int dofBot)
      :Dropper_base(dp, len, encumbrance, dofTop, dofBot),
      alpha(0.), is_SlackParam_Set(false) {}
    
    //! Destructor
    inline virtual ~NonlinearDropper() {}

    //! Disable copy and assignment
    NonlinearDropper(const NonlinearDropper&) = delete;
    NonlinearDropper& operator=(const NonlinearDropper&) = delete;

    //! Set the slack parameter
    inline void SetSlackParameter(const double val)
    {
      alpha = val;
      is_SlackParam_Set = true;
    }

    //! Returns whether the dropper is slack
    bool IsSlack(const double* dofvalues) const;
    
    //! Assemble contributions to the mass, stiffness and residue vector
    void Evaluate(const double* dofvalues, Mat& M, Mat& K, Vec& R);

  protected:
    bool is_SlackParam_Set;
    double alpha;
  };

}
