
/** \file pfe_Dropper_base.h
 * \brief Defines the class pfe::Dropper_base
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_DropperParams.h>
#include <petscvec.h>
#include <petscmat.h>

namespace pfe
{
  //! Base class defining a dropper between a pair of cables
  class Dropper_base
  {
  public:
    //! Constructor
    inline Dropper_base(const DropperParams& dp,
			const double len,
			const double encumbrance,
			const int dofTop, const int dofBot)
      :params(dp),
      length(len),
      zEncumbrance(encumbrance),
      top_dofNum(dofTop),
      bot_dofNum(dofBot) {}

    //! Destructor
    inline virtual ~Dropper_base() {}

    //! Disable copy and assignment
    Dropper_base(const Dropper_base&) = delete;
    Dropper_base& operator=(const Dropper_base&) = delete;

    //! Access length
    inline double GetLength() const
    { return length; }
    
    //! Set the length of this dropper
    inline void SetLength(const double len)
    { length = len; }
    
  protected:
    const DropperParams params;
    double length;
    const double zEncumbrance;
    const int top_dofNum;
    const int bot_dofNum;
  };

}
