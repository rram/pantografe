
/** \file pfe_LinearDropper.cpp
 * \brief Implements the class pfe::LinearDropper
 * \author Ramsharan Rangarajan
 */

#include <pfe_LinearDropper.h>

namespace pfe
{
  // Evaluate contributions to the force and stiffness
  void LinearDropper::Evaluate(Mat& M, Mat& K, Vec& F)
  {
    PetscErrorCode ierr;

    // Add intertia of
    // (i) clamp + 1/2 mass of dropper to the attachment point on the messenger wire
    // (ii) clamp + 1/2 mass of dropper to the attachement point on the contact wire
    double mass;
    mass = params.Mcm + 0.5*params.rhoA*length;
    ierr = MatSetValuesLocal(M, 1, &top_dofNum, 1, &top_dofNum, &mass, ADD_VALUES); CHKERRV(ierr);
    mass = params.Mcc + 0.5*params.rhoA*length;
    ierr = MatSetValuesLocal(M, 1, &bot_dofNum, 1, &bot_dofNum, &mass, ADD_VALUES); CHKERRV(ierr);
    ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);

    
    // Spring stiffness contributions
    // Functional: 1/2 k (ztop+wtop - (zbot+wbot) - Length)^2
    //           = 1/2 k (ztop-zbot + wtop-wbot   - Length)^2
    //           = 1/2 k (zEncumbrance + wtop-wbot - Length)^2
    //           = 1/2 k (wbot-wtop + Length - zEncumbrance)^2
    // k = EA/L
    const double Kspr = params.EA/length;

    // Update forces
    double value = Kspr*(length-zEncumbrance);
    ierr = VecSetValuesLocal(F, 1, &top_dofNum, &value, ADD_VALUES); CHKERRV(ierr);
    value *= -1.;
    ierr = VecSetValuesLocal(F, 1, &bot_dofNum, &value, ADD_VALUES); CHKERRV(ierr);

    // Add contributions from weight of the dropper
    // Functional: (rhoA*g x L) x wtop
    value = -params.rhoA*params.g*length;
    ierr = VecSetValuesLocal(F, 1, &top_dofNum, &value, ADD_VALUES); CHKERRV(ierr);

    // Add contributions from weights of the clamps at the top & bottom
    value = -params.Mcm*params.g;
    ierr = VecSetValuesLocal(F, 1, &top_dofNum, &value, ADD_VALUES); CHKERRV(ierr);
    value = -params.Mcc*params.g;
    ierr = VecSetValuesLocal(F, 1, &bot_dofNum, &value, ADD_VALUES); CHKERRV(ierr);

    // finish force assembly
    ierr = VecAssemblyBegin(F); CHKERRV(ierr);
    ierr = VecAssemblyEnd(F);   CHKERRV(ierr);
  
    // Update stiffness: only contributions from spring stiffness
    value = Kspr;
    ierr = MatSetValuesLocal(K, 1, &top_dofNum, 1, &top_dofNum, &value, ADD_VALUES); CHKERRV(ierr); // K_11
    ierr = MatSetValuesLocal(K, 1, &bot_dofNum, 1, &bot_dofNum, &value, ADD_VALUES); CHKERRV(ierr); // K_22
    value *= -1.;
    ierr = MatSetValuesLocal(K, 1, &top_dofNum, 1, &bot_dofNum, &value, ADD_VALUES); CHKERRV(ierr); // K_12
    ierr = MatSetValuesLocal(K, 1, &bot_dofNum, 1, &top_dofNum, &value, ADD_VALUES); CHKERRV(ierr); // K_21
    ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);

    // done
    return;
  }
    
}
