
/** \file pfe_ContactSpring.h
 * \brief Defines the classes pfe::ContactSpring,  pfe::LinearContactSpring and pfe::NonlinearContactSpring
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <petscvec.h>
#include <petscmat.h>
#include <vector>

namespace pfe
{
  //! Interface for linear and nonlinear contact springs
  class ContactSpring
  {
  public:
    //! Constructor
    ContactSpring() = default;

    //! Destructor
    virtual ~ContactSpring() = default;

    // disable copy and assignment
    ContactSpring(const ContactSpring&) = delete;
    ContactSpring& operator=(const ContactSpring&) = delete;

    //! Assemble contributions to the stiffness
    virtual void Evaluate(const Vec& U,  const std::vector<std::pair<int,double>>& constraints, Mat& K, Vec& R) const = 0;

    //! Compute the contact force at a given configuration
    virtual double ComputeContactForce(const Vec& U, const std::vector<std::pair<int,double>>& constraints) const = 0;
  };


  //! Linear contact spring
  class LinearContactSpring: public ContactSpring
  {
  public:
    //! Constructor
    inline LinearContactSpring(const double k)
      :ContactSpring(), stiffness(k) {}      

    //! Destructor
    virtual ~LinearContactSpring() = default;

    //! Disable copy and assignment
    LinearContactSpring(const LinearContactSpring&) = delete;
    LinearContactSpring operator=(const LinearContactSpring&) = delete;

    //! Assemble contributions to the stiffness
    void Evaluate(const Vec& U, const std::vector<std::pair<int,double>>& constraints,
		  Mat& K, Vec& R) const;

    //! Compute the contact force at a given configuration
    double ComputeContactForce(const Vec& U, const std::vector<std::pair<int,double>>& constraints) const;

  private:
    const double stiffness;
  };


  //! Nonlinear contact spring
  class NonlinearContactSpring: public ContactSpring
  {
  public:
    //! Constructor
    inline NonlinearContactSpring(const double k, const double alpha)
      :ContactSpring(), stiffness(k), slack_factor(alpha) {}

    //! Destructor
    virtual ~NonlinearContactSpring() = default;

    // Disable copy and assignment
    NonlinearContactSpring(const NonlinearContactSpring&) = delete;
    NonlinearContactSpring operator=(const NonlinearContactSpring&) = delete;

    //! Assemble contributions to the stiffness and residual
    void Evaluate(const Vec& U, const std::vector<std::pair<int,double>>& constraints,
		  Mat &K, Vec &R) const;

    //! Compute the contact force at the given configuration
    double ComputeContactForce(const Vec& U, const std::vector<std::pair<int,double>>& constraints) const;

  private:
    const double stiffness;
    const double slack_factor;
  };
}
