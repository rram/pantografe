
/** \file pfe_LinearContactSpring.cpp
 * \brief Implements the class pfe::LinearContactSpring
 * \author Ramsharan Rangarajan
 */

#include <pfe_ContactSpring.h>

namespace pfe
{
  // Assemble contributions to the stiffness
  void LinearContactSpring::Evaluate(const Vec& U, const std::vector<std::pair<int,double>>& constraint, Mat& K, Vec& R) const
  {
    // Access dof values
    const int nterms = static_cast<int>(constraint.size());
    std::vector<int> indx(nterms);
    for(int i=0; i<nterms; ++i)
      indx[i] = constraint[i].first;
    std::vector<double> uvals(nterms);
    PetscErrorCode ierr;
    ierr = VecGetValues(U, nterms, &indx[0], &uvals[0]); CHKERRV(ierr);

    // constraint value
    double cval = 0.;
    for(int i=0; i<nterms; ++i)
      cval += constraint[i].second*uvals[i];
    
    // update residual
    for(int i=0; i<nterms; ++i)
      {
	double val = stiffness*cval*constraint[i].second;
	ierr = VecSetValues(R, 1, &constraint[i].first, &val, ADD_VALUES); CHKERRV(ierr);
      }
    ierr = VecAssemblyBegin(R); CHKERRV(ierr);
    ierr = VecAssemblyEnd(R);   CHKERRV(ierr);
    
    // update stiffness
    for(int i=0; i<nterms; ++i)
      for(int j=0; j<nterms; ++j)
	{
	  double val = constraint[i].second*constraint[j].second;
	  ierr = MatSetValues(K, 1, &constraint[i].first, 1, &constraint[j].first, &val, ADD_VALUES); CHKERRV(ierr);
	}
    ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(K, MAT_FINAL_ASSEMBLY);   CHKERRV(ierr);

    // done
    return;
  }


  // Compute the contact force at a given configuration
  double LinearContactSpring::ComputeContactForce(const Vec& U, const std::vector<std::pair<int,double>>& constraint) const
  {
    // Access dof values
    const int nterms = static_cast<int>(constraint.size());
    std::vector<int> indx(nterms);
    for(int i=0; i<nterms; ++i)
      indx[i] = constraint[i].first;
    std::vector<double> uvals(nterms);
    PetscErrorCode ierr;
    ierr = VecGetValues(U, nterms, &indx[0], &uvals[0]); CHKERRQ(ierr);

    // constraint value
    double cval = 0.;
    for(int i=0; i<nterms; ++i)
      cval -= constraint[i].second*uvals[i];

    return cval*stiffness;
  }
}
