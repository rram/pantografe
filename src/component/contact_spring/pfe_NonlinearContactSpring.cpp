
/** \file pfe_NonlinearContactSpring.cpp
 * \brief Implements the class pfe::NonlinearContactSpring
 * \author Ramsharan Rangarajan
 */

#include <pfe_ContactSpring.h>

namespace pfe
{
  // Assemble contributions to the residual and stiffness
  void NonlinearContactSpring::Evaluate(const Vec& U, const std::vector<std::pair<int,double>>& constraint,
					Mat& K, Vec& R) const
  {
    // Access dof values
    const int nterms = static_cast<int>(constraint.size());
    std::vector<int> indx(nterms);
    for(int i=0; i<nterms; ++i)
      indx[i] = constraint[i].first;
    std::vector<double> uvals(nterms);
    PetscErrorCode ierr;
    ierr = VecGetValues(U, nterms, &indx[0], &uvals[0]); CHKERRV(ierr);

    // constraint value
    double cval = 0.;
    for(int i=0; i<nterms; ++i)
      cval += constraint[i].second*uvals[i];

    // slack factor
    const double tanh = std::tanh(slack_factor*cval);
    const double phi  = 0.5*(1.-tanh);
    const double Kc   = stiffness*phi;

    // update residual
    double value;
    for(int i=0; i<nterms; ++i)
      {
	value = Kc*cval*constraint[i].second;
	ierr = VecSetValuesLocal(R, 1, &constraint[i].first, &value, ADD_VALUES); CHKERRV(ierr);
      }
    ierr = VecAssemblyBegin(R); CHKERRV(ierr);
    ierr = VecAssemblyEnd(R);   CHKERRV(ierr);

    // stiffness
    const double dtanh = slack_factor*(1.-tanh*tanh);
    const double dphi  = -0.5*dtanh;
    const double dKc   = stiffness*dphi;  // dKc/dc
    for(int i=0; i<nterms; ++i)
      {
	const int& row   = constraint[i].first;
	const double& Na = constraint[i].second;
	for(int j=0; j<nterms; ++j)
	  {
	    const int& col   = constraint[j].first;
	    const double& Nb =  constraint[j].second;
	    value = dKc*cval*Na*Nb + Kc*Na*Nb;
	    ierr = MatSetValuesLocal(K, 1, &row, 1, &col, &value, ADD_VALUES); CHKERRV(ierr);
	  }
      }
    ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(K, MAT_FINAL_ASSEMBLY);   CHKERRV(ierr);

    // done
    return;
  }

  
  // Compute the contact force at a given configuration
  double NonlinearContactSpring::ComputeContactForce(const Vec& U,
						     const std::vector<std::pair<int,double>>& constraint) const
  {
    // Access dof values
    const int nterms = static_cast<int>(constraint.size());
    std::vector<int> indx(nterms);
    for(int i=0; i<nterms; ++i)
      indx[i] = constraint[i].first;
    std::vector<double> uvals(nterms);
    PetscErrorCode ierr;
    ierr = VecGetValues(U, nterms, &indx[0], &uvals[0]); CHKERRQ(ierr);

    // constraint value
    double cval = 0.;
    for(int i=0; i<nterms; ++i)
      cval += constraint[i].second*uvals[i];

    // spring stiffness with slack factor
    const double tanh = std::tanh(slack_factor*cval);
    const double phi  = 0.5*(1.-tanh);
    return -stiffness*phi*cval;
  }
}

