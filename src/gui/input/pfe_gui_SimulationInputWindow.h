
#pragma once
#include <pfe_gui_ParametersInputWindow.h>
#include <gtkmm.h>
#include <string>

namespace pfe
{
  namespace gui
  {
    class SimulationInputWindow: public Gtk::Window
    {
    public:
      //! Constructor
      SimulationInputWindow();

      //! Destructor
      virtual ~SimulationInputWindow() = default;

      // Disable copy and assignment
      SimulationInputWindow(const SimulationInputWindow&) = delete;
      SimulationInputWindow operator=(const SimulationInputWindow&) = delete;
    
    ParametersInputWindow *parametersInputWindow;
    std::string sim_filename="";
    
    protected:

      void on_choose_file_clicked();
      void on_save_button_clicked();
      void on_paraminput_changed();
      void on_create_new_clicked();
      void on_simulation_type_changed();
      void on_pantograph_type_changed();
      void on_contact_type_changed();
      void on_adjust_iterations_value_changed();
      void on_timeint_changed();
      void get_default_values();
      void writeJson(std::string fileName);
      bool is_real_number(std::string);
      bool is_integer(std::string);

    // Member widgets
    Gtk::ComboBoxText *simulation_type_combo;
    Gtk::Entry *uplift_force_entry, *tstep_entry, *specradius_entry, *alphaval_entry;
    Gtk::ComboBoxText *pantograph_type_combo;
    Gtk::ComboBoxText *contact_type_combo, *paraminput_combo, *timeint_type_combo;
    Gtk::Entry *nelem_entry, *outdir_entry;
    Gtk::Button *choose_file_button, *create_new_button;
    Gtk::Label *nspan_label, *pantograph_type_label, *contact_type_label, *adjust_iterations_label, *uplift_force_label, *tstep_label, *timeint_type_label, *specradius_label, *alphaval_label, *err_label;
    Gtk::Button *submit_button;
    Gtk::Separator *separator;
    Gtk::SpinButton *nspan_spinButton, *adjust_iterations_spinButton;
    
    // default values
    int nelem_value, adjust_iterations_value, uplift_force_value, nspan_value;
    double tstep_value, alphaval_value, specradius_value;

    };
    
  }
}
