
# Project
project(gui-input-tests)

# Identical for all targets
foreach(TARGET test_InputWindow)
  
  # Add this target
  add_executable(${TARGET}  ${TARGET}.cpp)
  
  # Link
  target_link_libraries(${TARGET} PUBLIC pfe_gui_input)

  # choose appropriate compiler flags
  target_compile_features(${TARGET} PUBLIC ${pfe_COMPILE_FEATURES})
  
  add_test(NAME ${TARGET} COMMAND ${TARGET})

endforeach()
