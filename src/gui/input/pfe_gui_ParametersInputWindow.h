
#pragma once

//#include <pfe_SimParams.h>
#include <pfe_json.hpp>
#include <gtkmm.h>
#include <string>

namespace pfe
{
  namespace gui
  {
    class ParametersInputWindow: public Gtk::Window
    {
    public:
      //! Constructor
      ParametersInputWindow();

      //! Destructor
      virtual ~ParametersInputWindow() = default;

      // Disable copy and assignment
      ParametersInputWindow(const ParametersInputWindow&) = delete;
      ParametersInputWindow operator=(const ParametersInputWindow&) = delete;
      
      void set_pantograph_type(std::string);
      void set_simulation_type(std::string);
      void set_contact_type(std::string);
      void set_do_adjustment(bool);
      void readJson(std::string);
      int get_nsteps(int, double);
      
      bool writeSuccess=false;
      std::string output_filename;

    protected:

      void on_submit_clicked();
      void on_pantograph_type_changed();
      void on_contact_type_changed();
      void on_simulation_type_changed();
      void show_dropper_schedule(int);
      void on_dropper_ndrop_changed();
      void get_default_values();
      void writeJson(std::string);
      std::string d2s(double);
      bool is_real_number(std::string);

    // Member widgets
    Gtk::Box *panto_hbox, *nl_hbox, *damp_hbox;
    Gtk::Frame *panto1_frame, *panto3_frame;
    Gtk::Label *err_label;
    Gtk::Entry *catenary_EI_entry, *catenary_height_entry, *catenary_ld_entry, *catenary_span_entry, *catenary_tension_entry, *catenary_gravity_entry;
    Gtk::Entry *contact_EI_entry, *contact_height_entry, *contact_ld_entry, *contact_span_entry, *contact_tension_entry, *contact_gravity_entry;
    Gtk::Entry *dropper_EA_entry, *dropper_ld_entry, *dropper_uclm_entry, *dropper_lclm_entry, *dropper_gravity_entry, *dropper_encumbrance_entry;
    Gtk::Entry *MWSS_stiff_entry, *MWSS_mass_entry, *MWSS_gravity_entry, *CWSS_stiff_entry, *CWSS_mass_entry, *CWSS_gravity_entry; 
    Gtk::Entry *panto0_xinit_entry, *panto0_zbase_entry, *panto0_speed_entry, *panto0_force_entry, *panto1_m_entry, *panto1_c_entry, *panto1_k_entry, *panto3_m1_entry, *panto3_c1_entry, *panto3_k1_entry, *panto3_m2_entry, *panto3_c2_entry, *panto3_k2_entry, *panto3_m3_entry, *panto3_c3_entry, *panto3_k3_entry;
    Gtk::Entry *nl_dropslack_entry, *nl_contstiff_entry, *nl_contslack_entry, *damping_alpha_entry, *damping_beta_entry;
    Gtk::Grid *grid, *ds_grid1, *ds_grid2, *nl_grid2, *nl_grid3;
    Gtk::ComboBoxText *dropper_ndrop_combo;
    std::vector<int> ndrop_values = {3, 4, 5, 6, 7, 8, 9, 10, 11};
    std::vector<Gtk::Entry*> dpos_entry, noml_entry, tsag_entry;
    Gtk::Button *submit_button;
    Gtk::Separator *panto_separator, *nl_separator, *damp_separator;
    int nprev;
    bool do_adjustment=true;

    // default values
    int dropper_ndrop_value;
    
    double catenary_EI_value, catenary_height_value, catenary_ld_value, catenary_span_value, catenary_tension_value, catenary_gravity_value;
    double contact_EI_value, contact_height_value, contact_ld_value, contact_span_value, contact_tension_value, contact_gravity_value;
    double dropper_EA_value, dropper_ld_value, dropper_uclm_value, dropper_lclm_value, dropper_gravity_value, dropper_encumbrance_value;
    double MWSS_stiff_value, MWSS_mass_value, MWSS_gravity_value, CWSS_stiff_value, CWSS_mass_value, CWSS_gravity_value; 
    double panto0_xinit_value, panto0_zbase_value, panto0_speed_value, panto0_force_value, panto1_xinit_value, panto1_zbase_value, panto1_speed_value, panto1_force_value, panto3_xinit_value, panto3_zbase_value, panto3_speed_value, panto3_force_value, panto1_m_value, panto1_c_value, panto1_k_value, panto3_m1_value, panto3_c1_value, panto3_k1_value, panto3_m2_value, panto3_c2_value, panto3_k2_value, panto3_m3_value, panto3_c3_value, panto3_k3_value;
    double nl_dropslack_value, nl_contstiff_value, nl_contslack_value, damping_alpha_value, damping_beta_value;
    std::vector<double> dpos_value, noml_value, tsag_value;
    std::string pantograph_type, simulation_type, contact_type;

    };
    
  }
}
