#include <pfe_gui_ParametersInputWindow.h>
#include <pfe_gui_SimulationInputWindow.h>
#include <pfe_gui_preprocess_window.h>

#include <iostream>
namespace pfe
{
  namespace gui
  {
    void run_preprocess_window()
    {
      auto app = Gtk::Application::create("com.example.inputWindow");

      // Create the windows
      pfe::gui::SimulationInputWindow simulationInputWindow;
      pfe::gui::ParametersInputWindow parametersInputWindow;

      simulationInputWindow.parametersInputWindow = &parametersInputWindow;
    
      // Run the application
      app->run(simulationInputWindow);


      if (simulationInputWindow.sim_filename!="")
      {
          std::string command="";
          std::string answer;
          std::cout << "Do you want to run the code? (yes/no): ";
          std::cin >> answer;
          if (answer == "y" || answer == "Y" || answer == "yes" || answer == "Yes" || answer == "YES"){
              command+="./pantografe -i ";
              command+=simulationInputWindow.sim_filename;
              command+=" -s demo";
          }
          //std::cout << command << std::endl;
          std::system(command.c_str());
      } 
      
      // done
      return;
    }
    
  }
}
