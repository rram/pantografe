
#include <pfe_gui_ParametersInputWindow.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>

namespace pfe
{
  namespace gui
  {
    // Constructor
    ParametersInputWindow::ParametersInputWindow()
    {

        get_default_values();

        set_title(" Parameters ");
        set_default_size(500, 300);
        set_position(Gtk::WIN_POS_CENTER);
        move(20,20);
        
        // Window icon
        std::ifstream file("logo.JPG");
        if (file.good()){
            Glib::RefPtr<Gdk::Pixbuf> icon_win = Gdk::Pixbuf::create_from_file("logo.JPG");
            set_icon(icon_win);
        }

        // Create a vertical box container
        auto outer_vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 10));
        add(*outer_vbox);
        // Create a horizontal box container
        auto hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10));

        int row, col;

        //catenary cable details

        grid = Gtk::manage(new Gtk::Grid());
        grid->set_row_spacing(10);
        grid->set_column_spacing(10);
        
        row=0;
        col=0;
        
        auto label = Gtk::manage(new Gtk::Label("EI [N m^2]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        catenary_EI_entry = Gtk::manage(new Gtk::Entry());
        catenary_EI_entry->set_hexpand(true);
        catenary_EI_entry->set_text(d2s(catenary_EI_value));
        grid->attach(*catenary_EI_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Height [m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        catenary_height_entry = Gtk::manage(new Gtk::Entry());
        catenary_height_entry->set_hexpand(true);
        catenary_height_entry->set_text(d2s(catenary_height_value));
        grid->attach(*catenary_height_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Line Density [kg/m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        catenary_ld_entry = Gtk::manage(new Gtk::Entry());
        catenary_ld_entry->set_hexpand(true);
        catenary_ld_entry->set_text(d2s(catenary_ld_value));
        grid->attach(*catenary_ld_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Span : ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        catenary_span_entry = Gtk::manage(new Gtk::Entry());
        catenary_span_entry->set_hexpand(true);
        catenary_span_entry->set_text(d2s(catenary_span_value));
        grid->attach(*catenary_span_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Tension [N]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        catenary_tension_entry = Gtk::manage(new Gtk::Entry());
        catenary_tension_entry->set_hexpand(true);
        catenary_tension_entry->set_text(d2s(catenary_tension_value));
        grid->attach(*catenary_tension_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Gravity [m/s^2]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        catenary_gravity_entry = Gtk::manage(new Gtk::Entry());
        catenary_gravity_entry->set_hexpand(true);
        catenary_gravity_entry->set_text(d2s(catenary_gravity_value));
        grid->attach(*catenary_gravity_entry, col+1, row, 1, 1);
        row=row+1;

        auto frame = Gtk::manage(new Gtk::Frame());
        frame->add(*grid);
        frame->set_label("Catenary Cable");
        hbox->pack_start(*frame, Gtk::PACK_SHRINK);
        
        // Create a separator line
        auto separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_VERTICAL));
        hbox->pack_start(*separator, Gtk::PACK_SHRINK);
          
        // contact cable details

        grid = Gtk::manage(new Gtk::Grid());
        grid->set_row_spacing(10);
        grid->set_column_spacing(10);
        
        row=0;
        col=0;
        
        label = Gtk::manage(new Gtk::Label("EI [N m^2]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        contact_EI_entry = Gtk::manage(new Gtk::Entry());
        contact_EI_entry->set_hexpand(true);
        contact_EI_entry->set_text(d2s(contact_EI_value));
        grid->attach(*contact_EI_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Height [m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        contact_height_entry = Gtk::manage(new Gtk::Entry());
        contact_height_entry->set_hexpand(true);
        contact_height_entry->set_text(d2s(contact_height_value));
        grid->attach(*contact_height_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Line Density [kg/m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        contact_ld_entry = Gtk::manage(new Gtk::Entry());
        contact_ld_entry->set_hexpand(true);
        contact_ld_entry->set_text(d2s(contact_ld_value));
        grid->attach(*contact_ld_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Span : ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        contact_span_entry = Gtk::manage(new Gtk::Entry());
        contact_span_entry->set_hexpand(true);
        contact_span_entry->set_text(d2s(contact_span_value));
        grid->attach(*contact_span_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Tension [N]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        contact_tension_entry = Gtk::manage(new Gtk::Entry());
        contact_tension_entry->set_hexpand(true);
        contact_tension_entry->set_text(d2s(contact_tension_value));
        grid->attach(*contact_tension_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Gravity [m/s^2]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        contact_gravity_entry = Gtk::manage(new Gtk::Entry());
        contact_gravity_entry->set_hexpand(true);
        contact_gravity_entry->set_text(d2s(contact_gravity_value));
        grid->attach(*contact_gravity_entry, col+1, row, 1, 1);
        row=row+1;

        frame = Gtk::manage(new Gtk::Frame());
        frame->add(*grid);
        frame->set_label("Contact Wire");
        hbox->pack_start(*frame, Gtk::PACK_SHRINK);
        
        // Create a separator line
        separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_VERTICAL));
        hbox->pack_start(*separator, Gtk::PACK_SHRINK);
          
        // Dropper details

        grid = Gtk::manage(new Gtk::Grid());
        grid->set_row_spacing(10);
        grid->set_column_spacing(10);
        
        row=0;
        col=0;
        
        label = Gtk::manage(new Gtk::Label("EA : [N]", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        dropper_EA_entry = Gtk::manage(new Gtk::Entry());
        dropper_EA_entry->set_hexpand(true);
        dropper_EA_entry->set_text(d2s(dropper_EA_value));
        grid->attach(*dropper_EA_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Line Density [kg/m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        dropper_ld_entry = Gtk::manage(new Gtk::Entry());
        dropper_ld_entry->set_hexpand(true);
        dropper_ld_entry->set_text(d2s(dropper_ld_value));
        grid->attach(*dropper_ld_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Upper Clamp Mass : [kg]", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        dropper_uclm_entry = Gtk::manage(new Gtk::Entry());
        dropper_uclm_entry->set_hexpand(true);
        dropper_uclm_entry->set_text(d2s(dropper_uclm_value));
        grid->attach(*dropper_uclm_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Lower Clamp Mass : [kg]", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        dropper_lclm_entry = Gtk::manage(new Gtk::Entry());
        dropper_lclm_entry->set_hexpand(true);
        dropper_lclm_entry->set_text(d2s(dropper_lclm_value));
        grid->attach(*dropper_lclm_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Gravity : [m/s^2]", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        dropper_gravity_entry = Gtk::manage(new Gtk::Entry());
        dropper_gravity_entry->set_hexpand(true);
        dropper_gravity_entry->set_text(d2s(dropper_gravity_value));
        grid->attach(*dropper_gravity_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("No. of droppers: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        
        //ndrop_values = {5, 6, 7, 8, 9, 10, 11};
        dropper_ndrop_combo = Gtk::manage(new Gtk::ComboBoxText());
        for(int i=0; i<ndrop_values.size(); i++){
            dropper_ndrop_combo->append(std::to_string(ndrop_values[i]));
        }
        dropper_ndrop_combo->set_active(dropper_ndrop_value);
        dropper_ndrop_combo->signal_changed().connect(sigc::mem_fun(*this, &ParametersInputWindow::on_dropper_ndrop_changed));
        grid->attach(*dropper_ndrop_combo, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Encumbrance [m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        dropper_encumbrance_entry = Gtk::manage(new Gtk::Entry());
        dropper_encumbrance_entry->set_hexpand(true);
        dropper_encumbrance_entry->set_text(d2s(dropper_encumbrance_value));
        grid->attach(*dropper_encumbrance_entry, col+1, row, 1, 1);
        row=row+1;

        frame = Gtk::manage(new Gtk::Frame());
        frame->add(*grid);
        frame->set_label("Droppers");
        hbox->pack_start(*frame, Gtk::PACK_SHRINK); 

        // Create a separator line
        separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_VERTICAL));
        hbox->pack_start(*separator, Gtk::PACK_SHRINK);
          
        //----------------------------------------------------------------

        auto spring_vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 15));

        //MWSS details

        grid = Gtk::manage(new Gtk::Grid());
        grid->set_row_spacing(10);
        grid->set_column_spacing(10);
        
        row=0;
        col=0;
        
        label = Gtk::manage(new Gtk::Label("Stiffness [N/m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        MWSS_stiff_entry = Gtk::manage(new Gtk::Entry());
        MWSS_stiff_entry->set_hexpand(true);
        MWSS_stiff_entry->set_text(d2s(MWSS_stiff_value));
        grid->attach(*MWSS_stiff_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Mass [kg]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        MWSS_mass_entry = Gtk::manage(new Gtk::Entry());
        MWSS_mass_entry->set_hexpand(true);
        MWSS_mass_entry->set_text(d2s(MWSS_mass_value));
        grid->attach(*MWSS_mass_entry, col+1, row, 1, 1);
        row=row+1;

        label = Gtk::manage(new Gtk::Label("Gravity [m/s^2]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        MWSS_gravity_entry = Gtk::manage(new Gtk::Entry());
        MWSS_gravity_entry->set_hexpand(true);
        MWSS_gravity_entry->set_text(d2s(MWSS_gravity_value));
        grid->attach(*MWSS_gravity_entry, col+1, row, 1, 1);
        row=row+1;
        
        
        frame = Gtk::manage(new Gtk::Frame());
        frame->add(*grid);
        frame->set_label("Catenary cable suspension spring");
        spring_vbox->pack_start(*frame, Gtk::PACK_SHRINK);
        
        // Create a separator line
        separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
        spring_vbox->pack_start(*separator, Gtk::PACK_SHRINK);
                
        //CWSS details

        grid = Gtk::manage(new Gtk::Grid());
        grid->set_row_spacing(10);
        grid->set_column_spacing(10);
        
        row=0;
        col=0;
        
        label = Gtk::manage(new Gtk::Label("Stiffness [N/m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        CWSS_stiff_entry = Gtk::manage(new Gtk::Entry());
        CWSS_stiff_entry->set_hexpand(true);
        CWSS_stiff_entry->set_text(d2s(CWSS_stiff_value));
        grid->attach(*CWSS_stiff_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Mass [kg]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        CWSS_mass_entry = Gtk::manage(new Gtk::Entry());
        CWSS_mass_entry->set_hexpand(true);
        CWSS_mass_entry->set_text(d2s(CWSS_mass_value));
        grid->attach(*CWSS_mass_entry, col+1, row, 1, 1);
        row=row+1;

        label = Gtk::manage(new Gtk::Label("Gravity [m/s^2]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        CWSS_gravity_entry = Gtk::manage(new Gtk::Entry());
        CWSS_gravity_entry->set_hexpand(true);
        CWSS_gravity_entry->set_text(d2s(CWSS_gravity_value));
        grid->attach(*CWSS_gravity_entry, col+1, row, 1, 1);
        row=row+1;

        frame = Gtk::manage(new Gtk::Frame());
        frame->add(*grid);
        frame->set_label("Contact Wire suspension spring");
        spring_vbox->pack_start(*frame, Gtk::PACK_SHRINK);
        
        hbox->pack_start(*spring_vbox, Gtk::PACK_SHRINK); 
        
        outer_vbox->pack_start(*hbox, Gtk::PACK_SHRINK);
        
        // Create a separator line
        separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
        outer_vbox->pack_start(*separator, Gtk::PACK_SHRINK);
        
        //----------------------------------------------------------------

        
        hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10));
        auto dropper_vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 15));
        
        ds_grid1 = Gtk::manage(new Gtk::Grid());
        ds_grid1->set_row_spacing(10);
        ds_grid1->set_column_spacing(10);
        
        ds_grid2 = Gtk::manage(new Gtk::Grid());
        ds_grid2->set_row_spacing(10);
        ds_grid2->set_column_spacing(10);
        
        label = Gtk::manage(new Gtk::Label("Dropper Positions [m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        ds_grid1->attach(*label, 0, 0, 1, 1);
        
        label = Gtk::manage(new Gtk::Label("Nominal Lengths [m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        ds_grid1->attach(*label, 0, 1, 1, 1);               
        
        label = Gtk::manage(new Gtk::Label("\t\t  Target Sag [m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        ds_grid2->attach(*label, 0, 0, 1, 1);               
             
        dropper_vbox->pack_start(*ds_grid1, Gtk::PACK_SHRINK); 
        dropper_vbox->pack_start(*ds_grid2, Gtk::PACK_SHRINK);      
             
        frame = Gtk::manage(new Gtk::Frame());
        frame->add(*dropper_vbox);
        frame->set_label("Dropper Schedule");
           
        hbox->pack_start(*frame, Gtk::PACK_SHRINK);
        
        outer_vbox->pack_start(*hbox, Gtk::PACK_SHRINK);

        dpos_entry.resize(ndrop_values[ndrop_values.size()-1], nullptr);
        noml_entry.resize(ndrop_values[ndrop_values.size()-1], nullptr);
        tsag_entry.resize(ndrop_values[ndrop_values.size()-1], nullptr);
        nprev=0;
        show_dropper_schedule(ndrop_values[dropper_ndrop_value]);
        for (int i=0;i<ndrop_values[dropper_ndrop_value];i++){
            dpos_entry[i]->set_text(d2s(dpos_value[i]));
            noml_entry[i]->set_text(d2s(noml_value[i]));
            tsag_entry[i]->set_text(d2s(tsag_value[i]));
        }

        //----------------------------------------------------------------
        
        damp_separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
        outer_vbox->pack_start(*damp_separator, Gtk::PACK_SHRINK);
        
        damp_hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10));
        
        hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 20));
        
        grid = Gtk::manage(new Gtk::Grid());
        grid->set_row_spacing(10);
        grid->set_column_spacing(5);
        row=0;
        col=0;
        label = Gtk::manage(new Gtk::Label("alpha [1/s]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        damping_alpha_entry = Gtk::manage(new Gtk::Entry());
        damping_alpha_entry->set_hexpand(true);
        damping_alpha_entry->set_text(d2s(damping_alpha_value));
        grid->attach(*damping_alpha_entry, col+1, row, 1, 1);
        hbox->pack_start(*grid, Gtk::PACK_SHRINK);
        
        grid = Gtk::manage(new Gtk::Grid());
        grid->set_row_spacing(10);
        grid->set_column_spacing(5);
        row=0;
        col=0;
        label = Gtk::manage(new Gtk::Label("beta [s]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        damping_beta_entry = Gtk::manage(new Gtk::Entry());
        damping_beta_entry->set_hexpand(true);
        damping_beta_entry->set_text(d2s(damping_beta_value));
        grid->attach(*damping_beta_entry, col+1, row, 1, 1);
        hbox->pack_start(*grid, Gtk::PACK_SHRINK);
        
        frame = Gtk::manage(new Gtk::Frame());
        frame->add(*hbox);
        frame->set_label("OHE damping");
        
        damp_hbox->pack_start(*frame, Gtk::PACK_SHRINK);
        
        outer_vbox->pack_start(*damp_hbox, Gtk::PACK_SHRINK);
                        
        //------------------------------------------------------------------

        // Create a separator line
        nl_separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
        outer_vbox->pack_start(*nl_separator, Gtk::PACK_SHRINK);
        
        nl_hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10));
        
        hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 20));
        
        grid = Gtk::manage(new Gtk::Grid());
        grid->set_row_spacing(10);
        grid->set_column_spacing(5);
        row=0;
        col=0;
        label = Gtk::manage(new Gtk::Label("Dropper Slack Factor [1/m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        nl_dropslack_entry = Gtk::manage(new Gtk::Entry());
        nl_dropslack_entry->set_hexpand(true);
        nl_dropslack_entry->set_text(d2s(nl_dropslack_value));
        grid->attach(*nl_dropslack_entry, col+1, row, 1, 1);
        hbox->pack_start(*grid, Gtk::PACK_SHRINK);
        
        nl_grid2 = Gtk::manage(new Gtk::Grid());
        nl_grid2->set_row_spacing(10);
        nl_grid2->set_column_spacing(5);
        row=0;
        col=0;
        label = Gtk::manage(new Gtk::Label("Contact Spring Stiffness [N/m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        nl_grid2->attach(*label, col, row, 1, 1);
        nl_contstiff_entry = Gtk::manage(new Gtk::Entry());
        nl_contstiff_entry->set_hexpand(true);
        nl_contstiff_entry->set_text(d2s(nl_contstiff_value));
        nl_grid2->attach(*nl_contstiff_entry, col+1, row, 1, 1);
        hbox->pack_start(*nl_grid2, Gtk::PACK_SHRINK);
        
        nl_grid3 = Gtk::manage(new Gtk::Grid());
        nl_grid3->set_row_spacing(10);
        nl_grid3->set_column_spacing(5);
        row=0;
        col=0;
        label = Gtk::manage(new Gtk::Label("Contact Spring Slack Factor [1/m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        nl_grid3->attach(*label, col, row, 1, 1);
        nl_contslack_entry = Gtk::manage(new Gtk::Entry());
        nl_contslack_entry->set_hexpand(true);
        nl_contslack_entry->set_text(d2s(nl_contslack_value));
        nl_grid3->attach(*nl_contslack_entry, col+1, row, 1, 1);
        row=row+1;
        hbox->pack_start(*nl_grid3, Gtk::PACK_SHRINK);
        
        frame = Gtk::manage(new Gtk::Frame());
        frame->add(*hbox);
        frame->set_label("Nonlinear Parameters");
        
        nl_hbox->pack_start(*frame, Gtk::PACK_SHRINK);
        
        outer_vbox->pack_start(*nl_hbox, Gtk::PACK_SHRINK);
                
        //----------------------------------------------------------------
          
        // Create a separator line
        panto_separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
        outer_vbox->pack_start(*panto_separator, Gtk::PACK_SHRINK);

        // Create a horizontal box container
        panto_hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10));
        
        // pantograph 0

        grid = Gtk::manage(new Gtk::Grid());
        grid->set_row_spacing(10);
        grid->set_column_spacing(10);
        
        row=0;
        col=0;
        
        label = Gtk::manage(new Gtk::Label("Initial Position [m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        panto0_xinit_entry = Gtk::manage(new Gtk::Entry());
        panto0_xinit_entry->set_hexpand(true);
        panto0_xinit_entry->set_text(d2s(panto0_xinit_value));
        grid->attach(*panto0_xinit_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("z - base [m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        panto0_zbase_entry = Gtk::manage(new Gtk::Entry());
        panto0_zbase_entry->set_hexpand(true);
        panto0_zbase_entry->set_text(d2s(panto0_zbase_value));
        grid->attach(*panto0_zbase_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Speed [m/s]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        panto0_speed_entry = Gtk::manage(new Gtk::Entry());
        panto0_speed_entry->set_hexpand(true);
        panto0_speed_entry->set_text(d2s(panto0_speed_value));
        grid->attach(*panto0_speed_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("Uplift Force [N]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        panto0_force_entry = Gtk::manage(new Gtk::Entry());
        panto0_force_entry->set_hexpand(true);
        panto0_force_entry->set_text(d2s(panto0_force_value));
        grid->attach(*panto0_force_entry, col+1, row, 1, 1);
        row=row+1;

        frame = Gtk::manage(new Gtk::Frame());
        frame->add(*grid);
        
        panto_hbox->pack_start(*frame, Gtk::PACK_SHRINK);
        
        // Panto 1
        
        grid = Gtk::manage(new Gtk::Grid());
        grid->set_row_spacing(10);
        grid->set_column_spacing(10);
        
        row=0;
        col=0;
        
        label = Gtk::manage(new Gtk::Label(" ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        label = Gtk::manage(new Gtk::Label(" ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col+1, row, 1, 1);
        row=row+1;
                
        label = Gtk::manage(new Gtk::Label("m [kg]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        panto1_m_entry = Gtk::manage(new Gtk::Entry());
        panto1_m_entry->set_hexpand(true);
        panto1_m_entry->set_text(d2s(panto1_m_value));
        grid->attach(*panto1_m_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("c [kg/s]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        panto1_c_entry = Gtk::manage(new Gtk::Entry());
        panto1_c_entry->set_hexpand(true);
        panto1_c_entry->set_text(d2s(panto1_c_value));
        grid->attach(*panto1_c_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("k [N/m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        panto1_k_entry = Gtk::manage(new Gtk::Entry());
        panto1_k_entry->set_hexpand(true);
        panto1_k_entry->set_text(d2s(panto1_k_value));
        grid->attach(*panto1_k_entry, col+1, row, 1, 1);
        row=row+1;
        
        panto1_frame = Gtk::manage(new Gtk::Frame());
        panto1_frame->add(*grid);
        
        panto_hbox->pack_start(*panto1_frame, Gtk::PACK_SHRINK);
        
        
        // Panto 3
        
        grid = Gtk::manage(new Gtk::Grid());
        grid->set_row_spacing(10);
        grid->set_column_spacing(10);
        
        row=0;
        col=0;
        
        label = Gtk::manage(new Gtk::Label(" ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        label = Gtk::manage(new Gtk::Label(" ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col+1, row, 1, 1);
        row=row+1;
                       
        label = Gtk::manage(new Gtk::Label("m1 [kg]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        panto3_m1_entry = Gtk::manage(new Gtk::Entry());
        panto3_m1_entry->set_hexpand(true);
        panto3_m1_entry->set_text(d2s(panto3_m1_value));
        grid->attach(*panto3_m1_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("c1 [kg/s]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        panto3_c1_entry = Gtk::manage(new Gtk::Entry());
        panto3_c1_entry->set_hexpand(true);
        panto3_c1_entry->set_text(d2s(panto3_c1_value));
        grid->attach(*panto3_c1_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("k1 [N/m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        panto3_k1_entry = Gtk::manage(new Gtk::Entry());
        panto3_k1_entry->set_hexpand(true);
        panto3_k1_entry->set_text(d2s(panto3_k1_value));
        grid->attach(*panto3_k1_entry, col+1, row, 1, 1);
        row=row+1;
                
        row=0;
        col=2;
        
        label = Gtk::manage(new Gtk::Label(" ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        label = Gtk::manage(new Gtk::Label(" ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col+1, row, 1, 1);
        row=row+1;
                       
        label = Gtk::manage(new Gtk::Label("m2 [kg]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        panto3_m2_entry = Gtk::manage(new Gtk::Entry());
        panto3_m2_entry->set_hexpand(true);
        panto3_m2_entry->set_text(d2s(panto3_m2_value));
        grid->attach(*panto3_m2_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("c2 [kg/s]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        panto3_c2_entry = Gtk::manage(new Gtk::Entry());
        panto3_c2_entry->set_hexpand(true);
        panto3_c2_entry->set_text(d2s(panto3_c2_value));
        grid->attach(*panto3_c2_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("k2 [N/m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        panto3_k2_entry = Gtk::manage(new Gtk::Entry());
        panto3_k2_entry->set_hexpand(true);
        panto3_k2_entry->set_text(d2s(panto3_k2_value));
        grid->attach(*panto3_k2_entry, col+1, row, 1, 1);
        row=row+1;
                
        row=0;
        col=4;
        
        label = Gtk::manage(new Gtk::Label(" ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        label = Gtk::manage(new Gtk::Label(" ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col+1, row, 1, 1);
        row=row+1;
                       
        label = Gtk::manage(new Gtk::Label("m3 [kg]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        panto3_m3_entry = Gtk::manage(new Gtk::Entry());
        panto3_m3_entry->set_hexpand(true);
        panto3_m3_entry->set_text(d2s(panto3_m3_value));
        grid->attach(*panto3_m3_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("c3 [kg/s]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        panto3_c3_entry = Gtk::manage(new Gtk::Entry());
        panto3_c3_entry->set_hexpand(true);
        panto3_c3_entry->set_text(d2s(panto3_c3_value));
        grid->attach(*panto3_c3_entry, col+1, row, 1, 1);
        row=row+1;
        
        label = Gtk::manage(new Gtk::Label("k3 [N/m]: ", Gtk::ALIGN_END));
        label->set_halign(Gtk::ALIGN_END);
        grid->attach(*label, col, row, 1, 1);
        panto3_k3_entry = Gtk::manage(new Gtk::Entry());
        panto3_k3_entry->set_hexpand(true);
        panto3_k3_entry->set_text(d2s(panto3_k3_value));
        grid->attach(*panto3_k3_entry, col+1, row, 1, 1);
        row=row+1;
        
        panto3_frame = Gtk::manage(new Gtk::Frame());
        panto3_frame->add(*grid);
        frame->set_label("Pantograph Parameters");
        
        panto_hbox->pack_start(*panto3_frame, Gtk::PACK_SHRINK);
        
        
        outer_vbox->pack_start(*panto_hbox, Gtk::PACK_SHRINK);
            
        // Add a button to submit the data
        submit_button = Gtk::manage(new Gtk::Button("save parameters"));
        submit_button->signal_clicked().connect(sigc::mem_fun(*this, &ParametersInputWindow::on_submit_clicked));
        outer_vbox->pack_start(*submit_button, Gtk::PACK_SHRINK);

        // Create a label for error message
        err_label = Gtk::manage(new Gtk::Label("", Gtk::ALIGN_START));
        err_label->override_color (Gdk::RGBA("red"), Gtk::STATE_FLAG_NORMAL);
        outer_vbox->pack_start(*err_label, Gtk::PACK_SHRINK);
 
        outer_vbox->set_margin_start(10);
        outer_vbox->set_margin_end(10);
        outer_vbox->set_margin_top(10);
        outer_vbox->set_margin_bottom(10);
      
        // Show all widgets
        show_all_children();
        panto1_frame->set_visible(false);
        panto3_frame->set_visible(false);
        panto_hbox->set_visible(false);
        panto_separator->set_visible(false);
        damp_hbox->set_visible(false);
        damp_separator->set_visible(false);
        nl_hbox->set_visible(false);
        nl_separator->set_visible(false);
        err_label->set_visible(false);
        on_contact_type_changed();
        
        set_resizable(false);
        resize(1,1);
    }


    void ParametersInputWindow::set_pantograph_type(std::string message) {
        //std::cout << "Received Message: " << message << std::endl;
        //output_buffer = output_textbox->get_buffer().operator->();
        //output_buffer->insert_at_cursor("Pantograph type changed in Simulation Options\n");
        //adj->set_value(adj->get_upper() - adj->get_page_size());
        pantograph_type=message;
        on_pantograph_type_changed();
    }
    
    void ParametersInputWindow::set_simulation_type(std::string message) {
        //std::cout << "Received Message: " << message << std::endl;
        //output_buffer = output_textbox->get_buffer().operator->();
        //output_buffer->insert_at_cursor("Simulation type changed in Simulation Options\n");
        //adj->set_value(adj->get_upper() - adj->get_page_size());
        simulation_type=message;
        on_simulation_type_changed();
    }
    
    void ParametersInputWindow::set_contact_type(std::string message) {
        //std::cout << "Received Message: " << message << std::endl;
        //output_buffer = output_textbox->get_buffer().operator->();
        //output_buffer->insert_at_cursor("Contact type changed in Simulation Options\n");
        //adj->set_value(adj->get_upper() - adj->get_page_size());
        contact_type=message;
        on_contact_type_changed();
    }
    
    void ParametersInputWindow::set_do_adjustment(bool flag){
        do_adjustment=flag;
        on_simulation_type_changed();
    }
    
    void ParametersInputWindow::readJson(std::string input_filename){

       std::fstream jfile;
       jfile.open(input_filename, std::ios::in);
       assert(jfile.good());
       auto J = nlohmann::json::parse(jfile);
       jfile.close();
       assert(J.empty()==false);

       // Loop through each item in the JSON object
       for (auto& [itemName, itemValues] : J.items()) {

           if(itemName=="catenary cable"){
               for (auto& [key, value] : itemValues.items()) {
                   if(key=="EI") {if (value.is_number()) catenary_EI_value=value; else catenary_EI_value=0.0;}
                   if(key=="height") {if (value.is_number()) catenary_height_value=value; else catenary_height_value=0.0;}
                   if(key=="line density") {if (value.is_number()) catenary_ld_value=value; else catenary_ld_value=0.0;}
                   if(key=="span") {if (value.is_number()) catenary_span_value=value; else catenary_span_value=0.0;}
                   if(key=="tension") {if (value.is_number()) catenary_tension_value=value; else catenary_tension_value=0.0;}
                   if(key=="gravity") {if (value.is_number()) catenary_gravity_value=value; else catenary_gravity_value=0.0;}
                }
           }else if(itemName=="contact wire"){
               for (auto& [key, value] : itemValues.items()) {
                   if(key=="EI") {if (value.is_number()) contact_EI_value=value; else contact_EI_value=0.0;}
                   if(key=="height") {if (value.is_number()) contact_height_value=value; else contact_height_value=0.0;}
                   if(key=="line density") {if (value.is_number()) contact_ld_value=value; else contact_ld_value=0.0;}
                   if(key=="span") {if (value.is_number()) contact_span_value=value; else contact_span_value=0.0;}
                   if(key=="tension") {if (value.is_number()) contact_tension_value=value; else contact_tension_value=0.0;}
                   if(key=="gravity") {if (value.is_number()) contact_gravity_value=value; else contact_gravity_value=0.0;}
               }
           }else if(itemName=="droppers"){
               for (auto& [key, value] : itemValues.items()) {
                   if(key=="EA") {if (value.is_number()) dropper_EA_value=value; else dropper_EA_value=0.0;}
                   if(key=="line density") {if (value.is_number()) dropper_ld_value=value; else dropper_ld_value=0.0;}
                   if(key=="catenary cable clamp mass") {if (value.is_number()) dropper_uclm_value=value; else dropper_uclm_value=0.0;}
                   if(key=="contact wire clamp mass") {if (value.is_number()) dropper_lclm_value=value; else dropper_lclm_value=0.0;}
                   if(key=="gravity") {if (value.is_number()) dropper_gravity_value=value; else dropper_gravity_value=0.0;}
               }
           }else if(itemName=="catenary cable suspension spring"){
               for (auto& [key, value] : itemValues.items()) {
                   if(key=="stiffness") {if (value.is_number()) MWSS_stiff_value=value; else MWSS_stiff_value=0.0;}
                   if(key=="mass") {if (value.is_number()) MWSS_mass_value=value; else MWSS_mass_value=0.0;}
                   if(key=="gravity") {if (value.is_number()) MWSS_gravity_value=value; else MWSS_gravity_value=0.0;}
               }
           }else if(itemName=="contact wire suspension spring"){
               for (auto& [key, value] : itemValues.items()) {
                   if(key=="stiffness") {if (value.is_number()) CWSS_stiff_value=value; else CWSS_stiff_value=0.0;}
                   if(key=="mass") {if (value.is_number()) CWSS_mass_value=value; else CWSS_mass_value=0.0;}
                   if(key=="gravity") {if (value.is_number()) CWSS_gravity_value=value; else CWSS_gravity_value=0.0;}
               }
           }else if(itemName=="nonlinear parameters"){
               for (auto& [key, value] : itemValues.items()) {
                   if(key=="dropper slack factor") {if (value.is_number()) nl_dropslack_value=value; else nl_dropslack_value=0.0;}
                   if(key=="contact spring stiffness") {if (value.is_number()) nl_contstiff_value=value; else nl_contstiff_value=0.0;}
                   if(key=="contact spring slack factor") {if (value.is_number()) nl_contslack_value=value; else nl_contslack_value=0.0;}
               }
           }else if(itemName=="ohe damping"){
               for (auto& [key, value] : itemValues.items()) {
                   if(key=="alpha") {if (value.is_number()) damping_alpha_value=value; else damping_alpha_value=0.0;}
                   if(key=="beta") {if (value.is_number()) damping_alpha_value=value; else damping_alpha_value=0.0;}
               }
           }else if(itemName=="dropper schedule"){
               for (auto& [key, value] : itemValues.items()) {
                   if(key=="encumbrance") {if (value.is_number()) dropper_encumbrance_value=value; else dropper_encumbrance_value=0.0;}
                   if(key=="number of droppers") {
                       for (dropper_ndrop_value=0;dropper_ndrop_value<ndrop_values.size();dropper_ndrop_value++)
                       {
                           if(ndrop_values[dropper_ndrop_value]==value) break;
                       }
                   }
               }
               dpos_value.resize(ndrop_values[dropper_ndrop_value], 0.0);
               noml_value.resize(ndrop_values[dropper_ndrop_value], 0.0);
               tsag_value.resize(ndrop_values[dropper_ndrop_value], 0.0);
               for (auto& [key, value] : itemValues.items()) {
                   int i=0;
                   if (value.is_array()) {
                       for (const auto& arrayValue : value) {
                           if(key=="dropper positions") {if (arrayValue.is_number()) dpos_value[i++]=arrayValue;}
                           if(key=="nominal lengths") {if (arrayValue.is_number()) noml_value[i++]=arrayValue;}
                           if(key=="target sag") {if (arrayValue.is_number()) tsag_value[i++]=arrayValue;}
                       }
                   }
               }
           }else if(itemName=="pantograph 0"){
               for (auto& [key, value] : itemValues.items()) {
                   if(key=="xinit") {if (value.is_number()) panto0_xinit_value=value; else panto0_xinit_value=0.0;}
                   if(key=="zbase") {if (value.is_number()) panto0_zbase_value=value; else panto0_zbase_value=0.0;}
                   if(key=="speed") {if (value.is_number()) panto0_speed_value=value; else panto0_speed_value=0.0;}
                   if(key=="force") {if (value.is_number()) panto0_force_value=value; else panto0_force_value=0.0;}
               }
           }else if(itemName=="pantograph 1"){
               for (auto& [key, value] : itemValues.items()) {
                   if(key=="xinit") {if (value.is_number()) panto1_xinit_value=value; else panto1_xinit_value=0.0;}
                   if(key=="zbase") {if (value.is_number()) panto1_zbase_value=value; else panto1_zbase_value=0.0;}
                   if(key=="speed") {if (value.is_number()) panto1_speed_value=value; else panto1_speed_value=0.0;}
                   if(key=="force") {if (value.is_number()) panto1_force_value=value; else panto1_force_value=0.0;}
                   if(key=="m") {if (value.is_number()) panto1_m_value=value; else panto1_m_value=0.0;}
                   if(key=="c") {if (value.is_number()) panto1_c_value=value; else panto1_c_value=0.0;}
                   if(key=="k") {if (value.is_number()) panto1_k_value=value; else panto1_k_value=0.0;}
               }
           }else if(itemName=="pantograph 3"){
               for (auto& [key, value] : itemValues.items()) {
                   if(key=="xinit") {if (value.is_number()) panto3_xinit_value=value; else panto3_xinit_value=0.0;}
                   if(key=="zbase") {if (value.is_number()) panto3_zbase_value=value; else panto3_zbase_value=0.0;}
                   if(key=="speed") {if (value.is_number()) panto3_speed_value=value; else panto3_speed_value=0.0;}
                   if(key=="force") {if (value.is_number()) panto3_force_value=value; else panto3_force_value=0.0;}
                   if(key=="m1") {if (value.is_number()) panto3_m1_value=value; else panto3_m1_value=0.0;}
                   if(key=="c1") {if (value.is_number()) panto3_c1_value=value; else panto3_c1_value=0.0;}
                   if(key=="k1") {if (value.is_number()) panto3_k1_value=value; else panto3_k1_value=0.0;}
                   if(key=="m2") {if (value.is_number()) panto3_m2_value=value; else panto3_m2_value=0.0;}
                   if(key=="c2") {if (value.is_number()) panto3_c2_value=value; else panto3_c2_value=0.0;}
                   if(key=="k2") {if (value.is_number()) panto3_k2_value=value; else panto3_k2_value=0.0;}
                   if(key=="m3") {if (value.is_number()) panto3_m3_value=value; else panto3_m3_value=0.0;}
                   if(key=="c3") {if (value.is_number()) panto3_c3_value=value; else panto3_c3_value=0.0;}
                   if(key=="k3") {if (value.is_number()) panto3_k3_value=value; else panto3_k3_value=0.0;}
               }
           }
       }
        
        //set all the values
        
        catenary_EI_entry->set_text(d2s(catenary_EI_value));
        catenary_height_entry->set_text(d2s(catenary_height_value));
        catenary_ld_entry->set_text(d2s(catenary_ld_value));
        catenary_span_entry->set_text(d2s(catenary_span_value));
        catenary_tension_entry->set_text(d2s(catenary_tension_value));
        catenary_gravity_entry->set_text(d2s(catenary_gravity_value));
        
        
        contact_EI_entry->set_text(d2s(contact_EI_value));
        contact_height_entry->set_text(d2s(contact_height_value));
        contact_ld_entry->set_text(d2s(contact_ld_value));
        contact_span_entry->set_text(d2s(contact_span_value));
        contact_tension_entry->set_text(d2s(contact_tension_value));
        contact_gravity_entry->set_text(d2s(contact_gravity_value));
        
        
        dropper_EA_entry->set_text(d2s(dropper_EA_value));
        dropper_ld_entry->set_text(d2s(dropper_ld_value));
        dropper_uclm_entry->set_text(d2s(dropper_uclm_value));
        dropper_lclm_entry->set_text(d2s(dropper_lclm_value));
        dropper_gravity_entry->set_text(d2s(dropper_gravity_value));
        
        dropper_ndrop_combo->set_active(dropper_ndrop_value);
        dropper_encumbrance_entry->set_text(d2s(dropper_encumbrance_value));
        
        on_dropper_ndrop_changed();
        for(int i=0; i<std::stoi(dropper_ndrop_combo->get_active_text()); i++){
            dpos_entry[i]->set_text(d2s(dpos_value[i]));
            noml_entry[i]->set_text(d2s(noml_value[i]));
            tsag_entry[i]->set_text(d2s(tsag_value[i]));
        }
        
        MWSS_stiff_entry->set_text(d2s(MWSS_stiff_value));
        MWSS_mass_entry->set_text(d2s(MWSS_mass_value));
        MWSS_gravity_entry->set_text(d2s(MWSS_gravity_value));
              
        CWSS_stiff_entry->set_text(d2s(CWSS_stiff_value));
        CWSS_mass_entry->set_text(d2s(CWSS_mass_value));
        CWSS_gravity_entry->set_text(d2s(CWSS_gravity_value));
        
        
        nl_dropslack_entry->set_text(d2s(nl_dropslack_value));
        nl_contstiff_entry->set_text(d2s(nl_contstiff_value));
        nl_contslack_entry->set_text(d2s(nl_contslack_value));
        
        if (pantograph_type=="pantograph 0"){
            panto0_xinit_entry->set_text(d2s(panto0_xinit_value));
            panto0_zbase_entry->set_text(d2s(panto0_zbase_value));
            panto0_speed_entry->set_text(d2s(panto0_speed_value));
            panto0_force_entry->set_text(d2s(panto0_force_value));
        }else if(pantograph_type=="pantograph 1"){
            panto0_xinit_entry->set_text(d2s(panto1_xinit_value));
            panto0_zbase_entry->set_text(d2s(panto1_zbase_value));
            panto0_speed_entry->set_text(d2s(panto1_speed_value));
            panto0_force_entry->set_text(d2s(panto1_force_value));
        }
        else{
            panto0_xinit_entry->set_text(d2s(panto3_xinit_value));
            panto0_zbase_entry->set_text(d2s(panto3_zbase_value));
            panto0_speed_entry->set_text(d2s(panto3_speed_value));
            panto0_force_entry->set_text(d2s(panto3_force_value));
        }
        panto1_m_entry->set_text(d2s(panto1_m_value));
        panto1_c_entry->set_text(d2s(panto1_c_value));
        panto1_k_entry->set_text(d2s(panto1_k_value));
        
        panto3_m1_entry->set_text(d2s(panto3_m1_value));
        panto3_c1_entry->set_text(d2s(panto3_c1_value));
        panto3_k1_entry->set_text(d2s(panto3_k1_value));
        panto3_m2_entry->set_text(d2s(panto3_m2_value));
        panto3_c2_entry->set_text(d2s(panto3_c2_value));
        panto3_k2_entry->set_text(d2s(panto3_k2_value));
        panto3_m3_entry->set_text(d2s(panto3_m3_value));
        panto3_c3_entry->set_text(d2s(panto3_c3_value));
        panto3_k3_entry->set_text(d2s(panto3_k3_value));
  
    }
    
    
    // Callback function for the submit button click
    void ParametersInputWindow::on_submit_clicked() {
        double temp;
        
        if(!(is_real_number(catenary_EI_entry->get_text())&&is_real_number(catenary_height_entry->get_text())&&is_real_number(catenary_ld_entry->get_text())&&is_real_number(catenary_span_entry->get_text())&&is_real_number(catenary_tension_entry->get_text())&&is_real_number(catenary_gravity_entry->get_text())))
        {
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"catenary cable\"");
            return;
        }
        if(std::stod(catenary_EI_entry->get_text())<0.0 || std::stod(catenary_height_entry->get_text())<0.0 || std::stod(catenary_ld_entry->get_text())<0.0 || std::stod(catenary_span_entry->get_text())<0.0 || std::stod(catenary_tension_entry->get_text())<0.0 || std::stod(catenary_gravity_entry->get_text())<0.0){
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"catenary cable\"");
            return;
        }
        
        if(!(is_real_number(contact_EI_entry->get_text())&&is_real_number(contact_height_entry->get_text())&&is_real_number(contact_ld_entry->get_text())&&is_real_number(contact_span_entry->get_text())&&is_real_number(contact_tension_entry->get_text())&&is_real_number(contact_gravity_entry->get_text())))
        {
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"contact cable\"");
            return;
        }
        if(std::stod(contact_EI_entry->get_text())<0.0 || std::stod(contact_height_entry->get_text())<0.0 || std::stod(contact_ld_entry->get_text())<0.0 || std::stod(contact_span_entry->get_text())<0.0 || std::stod(contact_tension_entry->get_text())<0.0 || std::stod(contact_gravity_entry->get_text())<0.0){
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"contact cable\"");
            return;
        }

        if(!(is_real_number(dropper_EA_entry->get_text())&&is_real_number(dropper_ld_entry->get_text())&&is_real_number(dropper_uclm_entry->get_text())&&is_real_number(dropper_lclm_entry->get_text())&&is_real_number(dropper_gravity_entry->get_text())&&is_real_number(dropper_encumbrance_entry->get_text())))
        {
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"droppers\"");
            return;  
        }
        if(std::stod(dropper_EA_entry->get_text())<0.0 || std::stod(dropper_ld_entry->get_text())<0.0 || std::stod(dropper_uclm_entry->get_text())<0.0 || std::stod(dropper_lclm_entry->get_text())<0.0 || std::stod(dropper_gravity_entry->get_text())<0.0 || std::stod(dropper_encumbrance_entry->get_text())<0.0){
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"droppers\"");
            return;
        }

        if(!(is_real_number(MWSS_stiff_entry->get_text())&&is_real_number(MWSS_mass_entry->get_text())&&is_real_number(MWSS_gravity_entry->get_text())))
        {
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"catenary cable suspension spring\"");
            return;  
        }
        if(std::stod(MWSS_stiff_entry->get_text())<0.0 || std::stod(MWSS_mass_entry->get_text())<0.0 || std::stod(MWSS_gravity_entry->get_text())<0.0){
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"catenary cable suspension spring\"");
            return;
        }
        
        if(!(is_real_number(CWSS_stiff_entry->get_text())&&is_real_number(CWSS_mass_entry->get_text())&&is_real_number(CWSS_gravity_entry->get_text())))
        {
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"contact wire suspension spring\"");
            return;  
        }
        if(std::stod(CWSS_stiff_entry->get_text())<0.0 || std::stod(CWSS_mass_entry->get_text())<0.0 || std::stod(CWSS_gravity_entry->get_text())<0.0){
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"contact wire suspension spring\"");
            return;
        }

        bool flag=false;
        for (int i=0;i<std::stoi(dropper_ndrop_combo->get_active_text());i++){
            if ((is_real_number(dpos_entry[i]->get_text())&&is_real_number(noml_entry[i]->get_text())&&is_real_number(tsag_entry[i]->get_text())))
            {
                if(std::stod(dpos_entry[i]->get_text())<0.0||std::stod(dpos_entry[i]->get_text())>std::stod(contact_span_entry->get_text())||std::stod(noml_entry[i]->get_text())<0.0) flag=true;
            }
            else flag=true;
        }
        if(flag){
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"dropper schedule\"");
            return;
        }

        if(!(is_real_number(nl_dropslack_entry->get_text())&&is_real_number(nl_contstiff_entry->get_text())&&is_real_number(nl_contslack_entry->get_text())))
        {
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"nonlinear parameters\"");
            return;  
        }
        if(std::stod(nl_dropslack_entry->get_text())<0.0 || std::stod(nl_contstiff_entry->get_text())<0.0 || std::stod(nl_contslack_entry->get_text())<0.0){
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"nonlinear parameters\"");
            return;
        }
        
        if(!(is_real_number(damping_alpha_entry->get_text())&&is_real_number(damping_alpha_entry->get_text())))
        {
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"OHE damping parameters\"");
            return;  
        }
        if(std::stod(damping_alpha_entry->get_text())<0.0 || std::stod(damping_beta_entry->get_text())<0.0 ){
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"nonlinear parameters\"");
            return;
        }

        if(!(is_real_number(panto0_xinit_entry->get_text())&&is_real_number(panto0_zbase_entry->get_text())&&is_real_number(panto0_speed_entry->get_text())&&is_real_number(panto0_force_entry->get_text())&&is_real_number(panto1_m_entry->get_text())&&is_real_number(panto1_c_entry->get_text())&&is_real_number(panto1_k_entry->get_text())&&is_real_number(panto3_m1_entry->get_text())&&is_real_number(panto3_c1_entry->get_text())&&is_real_number(panto3_k1_entry->get_text())&&is_real_number(panto3_m2_entry->get_text())&&is_real_number(panto3_c2_entry->get_text())&&is_real_number(panto3_k2_entry->get_text())&&is_real_number(panto3_m3_entry->get_text())&&is_real_number(panto3_c3_entry->get_text())&&is_real_number(panto3_k3_entry->get_text())))
        {
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"pantograph parameters\"");
            return;  
        }
        if(std::stod(panto0_xinit_entry->get_text())<0.0 || std::stod(panto0_speed_entry->get_text())<0.0 || std::stod(panto0_force_entry->get_text())<0.0 || std::stod(panto1_m_entry->get_text())<0.0 || std::stod(panto1_c_entry->get_text())<0.0 || std::stod(panto1_k_entry->get_text())<0.0 || std::stod(panto3_m1_entry->get_text())<0.0 || std::stod(panto3_c1_entry->get_text())<0.0 || std::stod(panto3_k1_entry->get_text())<0.0 || std::stod(panto3_m2_entry->get_text())<0.0 || std::stod(panto3_c2_entry->get_text())<0.0 || std::stod(panto3_k2_entry->get_text())<0.0 || std::stod(panto3_m3_entry->get_text())<0.0 || std::stod(panto3_c3_entry->get_text())<0.0 || std::stod(panto3_k3_entry->get_text())<0.0){
            err_label->set_visible(true);
            err_label->set_text("INCORRECT INPUT : check \"pantograph parameters\"");
            return;
        }

        Gtk::FileChooserDialog dialog("Save parameters", Gtk::FILE_CHOOSER_ACTION_SAVE);
        dialog.set_transient_for(*this);
        dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
        dialog.add_button("_Save", Gtk::RESPONSE_OK);
        dialog.set_current_name("output.json");
        auto filter_txt = Gtk::FileFilter::create();
        filter_txt->set_name("json files (*.json)");
        filter_txt->add_pattern("*.json");
        dialog.add_filter(filter_txt);

        // Show the dialog and wait for user response
        int result = dialog.run();

        if (result == Gtk::RESPONSE_OK){
            std::cout << " Parameters entered " << std::endl;
            output_filename=dialog.get_filename();
            writeJson(output_filename);
            err_label->set_visible(false);
            resize(1,1);
            hide();
        }  
    }

    void ParametersInputWindow::on_pantograph_type_changed() {
        if (pantograph_type == "pantograph 1") {
            panto1_frame->set_visible(true);
            panto3_frame->set_visible(false);
            nl_grid2->set_visible(true);
            nl_grid3->set_visible(true);
            on_contact_type_changed();
            resize(1,1);
        } else if(pantograph_type == "pantograph 3"){
            panto1_frame->set_visible(false);
            panto3_frame->set_visible(true);
            nl_grid2->set_visible(true);
            nl_grid3->set_visible(true);
            on_contact_type_changed();
            resize(1,1);
        } else {
            panto1_frame->set_visible(false);
            panto3_frame->set_visible(false);
            nl_grid2->set_visible(false);
            nl_grid3->set_visible(false);
            resize(1,1);
        }
    }
    
    void ParametersInputWindow::on_contact_type_changed() {
        if (contact_type == "persistent") {
            nl_grid3->set_visible(false);
            resize(1,1);
        } else {
            nl_grid3->set_visible(true);
            resize(1,1);
        }
    }
    
    void ParametersInputWindow::on_simulation_type_changed() {
        if (simulation_type == "Linear Static") {
            panto_hbox->set_visible(false);
            panto_separator->set_visible(false);
            damp_hbox->set_visible(false);
            damp_separator->set_visible(false);
            nl_hbox->set_visible(false);
            nl_separator->set_visible(false);
            ds_grid2->set_visible(do_adjustment);
            resize(1,1);
        } else if(simulation_type == "Nonlinear Static"){
            panto_hbox->set_visible(false);
            panto_separator->set_visible(false);
            damp_hbox->set_visible(false);
            damp_separator->set_visible(false);
            nl_hbox->set_visible(true);
            nl_separator->set_visible(true);
            nl_grid2->set_visible(false);
            nl_grid3->set_visible(false);
            ds_grid2->set_visible(do_adjustment);
            resize(1,1);
        } else if(simulation_type == "Linear Dynamic"){
            panto_hbox->set_visible(true);
            panto_separator->set_visible(true);
            damp_hbox->set_visible(false);
            damp_separator->set_visible(false);
            nl_hbox->set_visible(false);
            nl_separator->set_visible(false);
            on_pantograph_type_changed();
            ds_grid2->set_visible(false);
            resize(1,1);
        } else {
            panto_hbox->set_visible(true);
            panto_separator->set_visible(true);
            damp_hbox->set_visible(true);
            damp_separator->set_visible(true);
            nl_hbox->set_visible(true);
            nl_separator->set_visible(true);
            nl_grid2->set_visible(true);
            nl_grid3->set_visible(true);
            ds_grid2->set_visible(false);
            on_pantograph_type_changed();
            resize(1,1);
        }
    }
   
    void ParametersInputWindow::show_dropper_schedule(int nshow){

        int entryWidth=8;
        if (nshow<nprev){
            for(int i=nshow;i<nprev;i++){
                delete dpos_entry[i];
                delete noml_entry[i];
                ds_grid1->remove_column(nshow+1);
                delete tsag_entry[i];
                ds_grid2->remove_column(nshow+1);
            }
            nprev=nshow;
        } else if (nshow > nprev){
            for(int i=nprev;i<nshow;i++){
                dpos_entry[i] = Gtk::manage(new Gtk::Entry());
                dpos_entry[i]->set_hexpand(true);
                dpos_entry[i]->set_width_chars(entryWidth);
                ds_grid1->attach(*dpos_entry[i], i+1, 0, 1, 1);
                
                noml_entry[i] = Gtk::manage(new Gtk::Entry());
                noml_entry[i]->set_hexpand(true);
                noml_entry[i]->set_width_chars(entryWidth);
                ds_grid1->attach(*noml_entry[i], i+1, 1, 1, 1);
                
                tsag_entry[i] = Gtk::manage(new Gtk::Entry());
                tsag_entry[i]->set_hexpand(true);
                tsag_entry[i]->set_width_chars(entryWidth);
                ds_grid2->attach(*tsag_entry[i], i+1, 0, 1, 1);
            }
            nprev=nshow;
        }
    }
    
    
    // Callback function for ComboBox change
    void ParametersInputWindow::on_dropper_ndrop_changed() {
        
        show_dropper_schedule(std::stoi(dropper_ndrop_combo->get_active_text()));
        for(int i=0; i<std::stoi(dropper_ndrop_combo->get_active_text()); i++){
            dpos_entry[i]->set_text("0.000");
            noml_entry[i]->set_text(dropper_encumbrance_entry->get_text());
            tsag_entry[i]->set_text("0.000");
        }
        show_all_children();
        on_simulation_type_changed();
        err_label->set_visible(false);
        resize(1,1);
    }
    
    void ParametersInputWindow::get_default_values(){
        
        dropper_ndrop_value=6;
        
        catenary_EI_value=131.7;
        catenary_height_value=1.2; 
        catenary_ld_value=1.08;
        catenary_span_value=55.0;
        catenary_tension_value=16000.0;
        catenary_gravity_value=9.81;
    
        contact_EI_value=195.0;
        contact_height_value=0.0;
        contact_ld_value=1.35;
        contact_span_value=55.0;
        contact_tension_value=22000.0;
        contact_gravity_value=9.81;
        
        dropper_EA_value=200000.0;
        dropper_ld_value=0.117;
        dropper_uclm_value=0.195;
        dropper_lclm_value=0.165;
        dropper_gravity_value=9.81;
        dropper_encumbrance_value=1.2;
        
        MWSS_stiff_value=500000.0;
        MWSS_mass_value=0.0;
        MWSS_gravity_value=9.81;
        
        CWSS_stiff_value=2672.0;
        CWSS_mass_value=0.94;
        CWSS_gravity_value=9.81;
        
        panto0_xinit_value=panto1_xinit_value=panto3_xinit_value=6.0;
        panto0_zbase_value=panto1_zbase_value=panto3_zbase_value=-0.15;
        panto0_speed_value=panto1_speed_value=panto3_speed_value=88.89;
        panto0_force_value=panto1_force_value=panto3_force_value=169.0;
        panto1_m_value=7.5;
        panto1_c_value=0.0;
        panto1_k_value=150.0;
        panto3_m1_value=6.0;
        panto3_c1_value=0.0;
        panto3_k1_value=160.0;
        panto3_m2_value=9.0;
        panto3_c2_value=0.0;
        panto3_k2_value=15500.0;
        panto3_m3_value=7.5;
        panto3_c3_value=0.0;
        panto3_k3_value=7000.0;
        
        nl_dropslack_value=1.0e5;
        nl_contstiff_value=50000;
        nl_contslack_value=1.0e5;
        
        damping_alpha_value=0.125;
        damping_beta_value=1.0e-4;
        
        dpos_value={4.5, 10.25, 16.0, 21.75, 27.5, 33.25, 39.0, 44.75, 50.5};
        noml_value={1.017, 0.896, 0.810, 0.758, 0.741, 0.758, 0.810, 0.896, 1.017};
        tsag_value={-0.0, -0.024, -0.041, -0.052, -0.055, -0.052, -0.041, -0.024, -0.0};
        
        pantograph_type = "pantograph 0";
        simulation_type = "Linear Static";
        contact_type = "persistent";
    }
    
    std::string ParametersInputWindow::d2s(double d){
    
        std::ostringstream ss;
        ss << std::fixed << std::setprecision(3) << d;
        return ss.str();
    }

    void ParametersInputWindow::writeJson(std::string fileName){

        // Output file name
        //const std::string fileName = "output.json";

        // Write JSON data to a file
        std::ofstream outputFile(fileName);
        if (outputFile.is_open()) {
        
            outputFile << "{";

            outputFile << "\n\t\"catenary cable\": {" << "\n";
            outputFile << "\t\t\"EI\": " << catenary_EI_entry->get_text() << ",\n";
            outputFile << "\t\t\"height\": " << catenary_height_entry->get_text() << ",\n";
            outputFile << "\t\t\"line density\": " << catenary_ld_entry->get_text() << ",\n";
            outputFile << "\t\t\"span\": " << catenary_span_entry->get_text() << ",\n";
            outputFile << "\t\t\"tension\": " << catenary_tension_entry->get_text() << ",\n";
            outputFile << "\t\t\"gravity\": " << catenary_gravity_entry->get_text() << "\n\t},\n";
            
            outputFile << "\n\t\"contact wire\": {" << "\n";
            outputFile << "\t\t\"EI\": " << contact_EI_entry->get_text() << ",\n";
            outputFile << "\t\t\"height\": " << contact_height_entry->get_text() << ",\n";
            outputFile << "\t\t\"line density\": " << contact_ld_entry->get_text() << ",\n";
            outputFile << "\t\t\"span\": " << contact_span_entry->get_text() << ",\n";
            outputFile << "\t\t\"tension\": " << contact_tension_entry->get_text() << ",\n";
            outputFile << "\t\t\"gravity\": " << contact_gravity_entry->get_text() << "\n\t},\n";
            
            outputFile << "\n\t\"droppers\": {" << "\n";
            outputFile << "\t\t\"EA\": " << dropper_EA_entry->get_text() << ",\n";
            outputFile << "\t\t\"line density\": " << dropper_ld_entry->get_text() << ",\n";
            outputFile << "\t\t\"catenary cable clamp mass\": " << dropper_uclm_entry->get_text() << ",\n";
            outputFile << "\t\t\"contact wire clamp mass\": " << dropper_lclm_entry->get_text() << ",\n";
            outputFile << "\t\t\"gravity\": " << dropper_gravity_entry->get_text() << "\n\t},\n";
            
            outputFile << "\n\t\"dropper schedule\": {" << "\n";
            outputFile << "\t\t\"dropper positions\": [";
            for(int i=0;i<std::stoi(dropper_ndrop_combo->get_active_text());i++){
                if(i>0) outputFile << ", ";
                outputFile << dpos_entry[i]->get_text();
            }
            outputFile << "],\n";
            outputFile << "\t\t\"encumbrance\": " << dropper_encumbrance_entry->get_text() << ",\n";
            outputFile << "\t\t\"nominal lengths\": [";
            for(int i=0;i<std::stoi(dropper_ndrop_combo->get_active_text());i++){
                if(i>0) outputFile << ", ";
                outputFile << noml_entry[i]->get_text();
            }
            outputFile << "],\n";
            outputFile << "\t\t\"number of droppers\": " << dropper_ndrop_combo->get_active_text() << ",\n";
            outputFile << "\t\t\"target sag\": [";
            for(int i=0;i<std::stoi(dropper_ndrop_combo->get_active_text());i++){
                if(i>0) outputFile << ", ";
                outputFile << tsag_entry[i]->get_text();
            }
            outputFile << "]\n\t},\n";
            
            outputFile << "\n\t\"contact wire suspension spring\": {" << "\n";
            outputFile << "\t\t\"stiffness\": " << CWSS_stiff_entry->get_text() << ",\n";
            outputFile << "\t\t\"mass\": " << CWSS_mass_entry->get_text() << ",\n";
            outputFile << "\t\t\"gravity\": " << CWSS_gravity_entry->get_text() << "\n\t},\n";
            
            outputFile << "\n\t\"catenary cable suspension spring\": {" << "\n";
            outputFile << "\t\t\"stiffness\": " << MWSS_stiff_entry->get_text() << ",\n";
            outputFile << "\t\t\"mass\": " << MWSS_mass_entry->get_text() << ",\n";
            outputFile << "\t\t\"gravity\": " << MWSS_gravity_entry->get_text() << "\n\t},\n";
            
            outputFile << "\n\t\"nonlinear parameters\": {" << "\n";
            outputFile << "\t\t\"dropper slack factor\": " << nl_dropslack_entry->get_text() << ",\n";
            outputFile << "\t\t\"contact spring stiffness\": " << nl_contstiff_entry->get_text() << ",\n";
            outputFile << "\t\t\"contact spring slack factor\": " << nl_contslack_entry->get_text() << "\n\t},\n";
            
            outputFile << "\n\t\"ohe damping\": {" << "\n";
            outputFile << "\t\t\"alpha\": " << damping_alpha_entry->get_text() << ",\n";
            outputFile << "\t\t\"beta\": " << damping_beta_entry->get_text() << "\n\t},\n";
            
            outputFile << "\n\t\"pantograph 0\": {" << "\n";
            outputFile << "\t\t\"xinit\": " << panto0_xinit_entry->get_text() << ",\n";
            outputFile << "\t\t\"zbase\": " << panto0_zbase_entry->get_text() << ",\n";
            outputFile << "\t\t\"speed\": " << panto0_speed_entry->get_text() << ",\n";
            outputFile << "\t\t\"force\": " << panto0_force_entry->get_text() << "\n\t},\n";
                        
            outputFile << "\n\t\"pantograph 1\": {" << "\n";
            outputFile << "\t\t\"xinit\": " << panto0_xinit_entry->get_text() << ",\n";
            outputFile << "\t\t\"zbase\": " << panto0_zbase_entry->get_text() << ",\n";
            outputFile << "\t\t\"speed\": " << panto0_speed_entry->get_text() << ",\n";
            outputFile << "\t\t\"force\": " << panto0_force_entry->get_text() << ",\n";
            outputFile << "\t\t\"m\": " << panto1_m_entry->get_text() << ",\n";
            outputFile << "\t\t\"k\": " << panto1_k_entry->get_text() << ",\n";
            outputFile << "\t\t\"c\": " << panto1_c_entry->get_text() << "\n\t},\n";
                        
            outputFile << "\n\t\"pantograph 3\": {" << "\n";
            outputFile << "\t\t\"xinit\": " << panto0_xinit_entry->get_text() << ",\n";
            outputFile << "\t\t\"zbase\": " << panto0_zbase_entry->get_text() << ",\n";
            outputFile << "\t\t\"speed\": " << panto0_speed_entry->get_text() << ",\n";
            outputFile << "\t\t\"force\": " << panto0_force_entry->get_text() << ",\n";
            outputFile << "\t\t\"m1\": " << panto3_m1_entry->get_text() << ",\n";
            outputFile << "\t\t\"k1\": " << panto3_k1_entry->get_text() << ",\n";
            outputFile << "\t\t\"c1\": " << panto3_c1_entry->get_text() << ",\n";
            outputFile << "\t\t\"m2\": " << panto3_m2_entry->get_text() << ",\n";
            outputFile << "\t\t\"k2\": " << panto3_k2_entry->get_text() << ",\n";
            outputFile << "\t\t\"c2\": " << panto3_c2_entry->get_text() << ",\n";
            outputFile << "\t\t\"m3\": " << panto3_m3_entry->get_text() << ",\n";
            outputFile << "\t\t\"k3\": " << panto3_k3_entry->get_text() << ",\n";
            outputFile << "\t\t\"c3\": " << panto3_c3_entry->get_text() << "\n\t}\n";
                                                                                
            outputFile << "}";
    
            outputFile.close();
            std::cout << "JSON data has been written to " << fileName << std::endl;
            writeSuccess=true;
        } else {
            std::cerr << "Error opening the file: " << fileName << std::endl;
        }
    }
    
    int ParametersInputWindow::get_nsteps(int nspan, double timestep){
       return std::floor((nspan*std::stod(contact_span_entry->get_text())-std::stod(panto0_xinit_entry->get_text()))/(std::stod(panto0_speed_entry->get_text())*timestep))-1;
    }
    
    bool ParametersInputWindow::is_real_number(std::string str) {
        std::istringstream iss(str);
        double value;
        return (iss >> value) && iss.eof();
    }

  }
}
