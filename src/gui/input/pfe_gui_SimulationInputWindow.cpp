
#include <pfe_gui_SimulationInputWindow.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <regex>

namespace pfe
{
  namespace gui
  {
    // Constructor
    SimulationInputWindow::SimulationInputWindow()
    {
        get_default_values();

        set_title(" Simulation Options ");
        set_default_size(300, 300);
        set_position(Gtk::WIN_POS_CENTER);
        move(0, 0);

        // Window icon
        std::ifstream file("logo.JPG");
        if (file.good()){
            Glib::RefPtr<Gdk::Pixbuf> icon_win = Gdk::Pixbuf::create_from_file("logo.JPG");
            set_icon(icon_win);
        }

        // Create a vertical box container
        auto vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 10));
        add(*vbox);

        // Create a label
        Gtk::Label *label = Gtk::manage(new Gtk::Label("Select Simulation Type:", Gtk::ALIGN_START));
        vbox->pack_start(*label, Gtk::PACK_SHRINK);

        // Create a dropdown box
        simulation_type_combo = Gtk::manage(new Gtk::ComboBoxText());
        simulation_type_combo->append("Linear Static");
        simulation_type_combo->append("Linear Dynamic");
        simulation_type_combo->append("Nonlinear Static");
        simulation_type_combo->append("Nonlinear Dynamic");

        // Set the default selection
        simulation_type_combo->set_active(0);

        // Add the combo box to the main vbox
        simulation_type_combo->signal_changed().connect(sigc::mem_fun(*this, &SimulationInputWindow::on_simulation_type_changed));
        vbox->pack_start(*simulation_type_combo, Gtk::PACK_SHRINK);

        // Create a separator line
        separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
        vbox->pack_start(*separator, Gtk::PACK_SHRINK);

        // Create a label
        nspan_label = Gtk::manage(new Gtk::Label("Number of Spans:", Gtk::ALIGN_START));
        vbox->pack_start(*nspan_label, Gtk::PACK_SHRINK);
        nspan_spinButton= Gtk::manage(new Gtk::SpinButton());
        nspan_spinButton->set_range(1, 20); // Range
        nspan_spinButton->set_increments(1, 1); // Increment by 1
        nspan_spinButton->set_numeric(true);
        nspan_spinButton->set_value(nspan_value);
        vbox->pack_start(*nspan_spinButton, Gtk::PACK_SHRINK);

        // Create a label
        pantograph_type_label = Gtk::manage(new Gtk::Label("Select Pantograph Type:", Gtk::ALIGN_START));
        vbox->pack_start(*pantograph_type_label, Gtk::PACK_SHRINK);

        // Create a dropdown box for panto type
        pantograph_type_combo = Gtk::manage(new Gtk::ComboBoxText());
        pantograph_type_combo->append("pantograph 0");
        pantograph_type_combo->append("pantograph 1");
        pantograph_type_combo->append("pantograph 3");
        pantograph_type_combo->set_active(0);
        pantograph_type_combo->signal_changed().connect(sigc::mem_fun(*this, &SimulationInputWindow::on_pantograph_type_changed));
        vbox->pack_start(*pantograph_type_combo, Gtk::PACK_SHRINK);
        
        // Create a dropdown for contact type
        contact_type_label = Gtk::manage(new Gtk::Label("Select Contact Type:", Gtk::ALIGN_START));
        vbox->pack_start(*contact_type_label, Gtk::PACK_SHRINK);
        contact_type_combo = Gtk::manage(new Gtk::ComboBoxText());
        contact_type_combo->append("persistent");
        contact_type_combo->append("intermittent");
        contact_type_combo->set_active(0);
        contact_type_combo->signal_changed().connect(sigc::mem_fun(*this, &SimulationInputWindow::on_contact_type_changed));
        vbox->pack_start(*contact_type_combo, Gtk::PACK_SHRINK);

        // Create a label
        label = Gtk::manage(new Gtk::Label("Number of elements between droppers:", Gtk::ALIGN_START));
        vbox->pack_start(*label, Gtk::PACK_SHRINK);
        // Create an entry field
        nelem_entry = Gtk::manage(new Gtk::Entry());
        nelem_entry->set_placeholder_text("enter an integer");
        nelem_entry->set_text(std::to_string(nelem_value));
        vbox->pack_start(*nelem_entry, Gtk::PACK_SHRINK);

        adjust_iterations_label = Gtk::manage(new Gtk::Label("Length adjustment iterations :", Gtk::ALIGN_START));
        vbox->pack_start(*adjust_iterations_label, Gtk::PACK_SHRINK);
        // spin button
        adjust_iterations_spinButton= Gtk::manage(new Gtk::SpinButton());
        adjust_iterations_spinButton->set_range(0, 10); // Range
        adjust_iterations_spinButton->set_increments(1, 1); // Increment by 1
        adjust_iterations_spinButton->set_numeric(true);
        adjust_iterations_spinButton->set_value(adjust_iterations_value);
        vbox->pack_start(*adjust_iterations_spinButton, Gtk::PACK_SHRINK);
        // Connect the signal handler to handle the value change in SpinButton
        adjust_iterations_spinButton->signal_value_changed().connect(sigc::mem_fun(*this, &SimulationInputWindow::on_adjust_iterations_value_changed));

        // Create a label
        uplift_force_label = Gtk::manage(new Gtk::Label("Uplift force [N]:", Gtk::ALIGN_START));
        vbox->pack_start(*uplift_force_label, Gtk::PACK_SHRINK);
        // Create an entry field
        uplift_force_entry = Gtk::manage(new Gtk::Entry());
        uplift_force_entry->set_placeholder_text("enter a positive real number");
        uplift_force_entry->set_text(std::to_string(uplift_force_value));
        vbox->pack_start(*uplift_force_entry, Gtk::PACK_SHRINK);

        // Create a label
        tstep_label = Gtk::manage(new Gtk::Label("Time-step [s]:", Gtk::ALIGN_START));
        vbox->pack_start(*tstep_label, Gtk::PACK_SHRINK);
        // Create an entry field
        tstep_entry = Gtk::manage(new Gtk::Entry());
        tstep_entry->set_placeholder_text("enter a real number");
        tstep_entry->set_text(std::to_string(tstep_value));
        vbox->pack_start(*tstep_entry, Gtk::PACK_SHRINK);

        // Create a label
        timeint_type_label = Gtk::manage(new Gtk::Label("Select Time-Integrator:", Gtk::ALIGN_START));
        vbox->pack_start(*timeint_type_label, Gtk::PACK_SHRINK);
        // Create a dropdown box
        timeint_type_combo = Gtk::manage(new Gtk::ComboBoxText());
        timeint_type_combo->append("generalized alpha");
        timeint_type_combo->append("hht alpha");
        timeint_type_combo->append("newmark");

        // Set the default selection
        timeint_type_combo->set_active(0);

        timeint_type_combo->signal_changed().connect(sigc::mem_fun(*this, &SimulationInputWindow::on_timeint_changed));
        vbox->pack_start(*timeint_type_combo, Gtk::PACK_SHRINK);

        // Create a label for spectral radius
        specradius_label = Gtk::manage(new Gtk::Label("Spectral Radius :", Gtk::ALIGN_START));
        vbox->pack_start(*specradius_label, Gtk::PACK_SHRINK);
        // Create an Entry for spectral radius
        specradius_entry = Gtk::manage(new Gtk::Entry());
        specradius_entry->set_placeholder_text("Enter a number between 0 and 1");
        specradius_entry->set_text(std::to_string(specradius_value));
        vbox->pack_start(*specradius_entry, Gtk::PACK_SHRINK);

        // Create a label for spectral radius
        alphaval_label = Gtk::manage(new Gtk::Label("Alpha :", Gtk::ALIGN_START));
        vbox->pack_start(*alphaval_label, Gtk::PACK_SHRINK);
        // Create an Entry for alpha value
        alphaval_entry = Gtk::manage(new Gtk::Entry());
        alphaval_entry->set_placeholder_text("Enter  a number between 0 and -.33");
        alphaval_entry->set_text(std::to_string(alphaval_value));
        vbox->pack_start(*alphaval_entry, Gtk::PACK_SHRINK);
        
        // Create a label
        label = Gtk::manage(new Gtk::Label("Output directory name:", Gtk::ALIGN_START));
        vbox->pack_start(*label, Gtk::PACK_SHRINK);
        // Create an entry field
        outdir_entry = Gtk::manage(new Gtk::Entry());
        outdir_entry->set_text("output");
        vbox->pack_start(*outdir_entry, Gtk::PACK_SHRINK);
        
        //-----------------------------------------------------------------------
        
        // Create a separator line
        separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
        vbox->pack_start(*separator, Gtk::PACK_SHRINK);
        
        // Create a label
        label = Gtk::manage(new Gtk::Label("Input Parameters:", Gtk::ALIGN_START));
        vbox->pack_start(*label, Gtk::PACK_SHRINK);

        // Create a dropdown box
        paraminput_combo = Gtk::manage(new Gtk::ComboBoxText());
        paraminput_combo->append("Load from file");
        paraminput_combo->append("Create new");

        // Set the default selection
        paraminput_combo->set_active(0);

        // Add the combo box to the main vbox
        paraminput_combo->signal_changed().connect(sigc::mem_fun(*this, &SimulationInputWindow::on_paraminput_changed));
        vbox->pack_start(*paraminput_combo, Gtk::PACK_SHRINK);

        // Create a button to open the file chooser
        choose_file_button = Gtk::manage(new Gtk::Button("Select a .json file"));
        choose_file_button->signal_clicked().connect(sigc::mem_fun(*this, &SimulationInputWindow::on_choose_file_clicked));
        vbox->pack_start(*choose_file_button, Gtk::PACK_SHRINK);

        // Create a button to open the file chooser
        create_new_button = Gtk::manage(new Gtk::Button("Enter Parameters"));
        create_new_button->signal_clicked().connect(sigc::mem_fun(*this, &SimulationInputWindow::on_create_new_clicked));
        vbox->pack_start(*create_new_button, Gtk::PACK_SHRINK);
        //---------------------------------------------------------------------
        
        // Create a separator line
        separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
        vbox->pack_start(*separator, Gtk::PACK_SHRINK);

        // Add a button to submit the selected simulation type
        submit_button = Gtk::manage(new Gtk::Button("save"));
        submit_button->signal_clicked().connect(sigc::mem_fun(*this, &SimulationInputWindow::on_save_button_clicked));
        vbox->pack_start(*submit_button, Gtk::PACK_SHRINK);
        submit_button->set_sensitive(false);
        
        // Create a label for error message
        err_label = Gtk::manage(new Gtk::Label("", Gtk::ALIGN_START));
        err_label->override_color (Gdk::RGBA("red"), Gtk::STATE_FLAG_NORMAL);
        vbox->pack_start(*err_label, Gtk::PACK_SHRINK);

        vbox->set_margin_start(10);
        vbox->set_margin_end(10);
        vbox->set_margin_top(10);
        vbox->set_margin_bottom(10);
        
        // Show all widgets
        show_all_children();
        create_new_button->set_visible(false);
        nspan_label->set_visible(false);
        //nspan_entry->set_visible(false);
        nspan_spinButton->set_visible(false);
        pantograph_type_label->set_visible(false);
        pantograph_type_combo->set_visible(false);
        contact_type_label->set_visible(false);
        contact_type_combo->set_visible(false);
        tstep_label->set_visible(false);
        tstep_entry->set_visible(false);
        timeint_type_label->set_visible(false);
        timeint_type_combo->set_visible(false);
        specradius_label->set_visible(false);
        specradius_entry->set_visible(false);
        alphaval_label->set_visible(false);
        alphaval_entry->set_visible(false);
        err_label->set_visible(false);
        
        set_resizable(false);
    }


    // Callback function for the "Choose File" button click
    void SimulationInputWindow::on_choose_file_clicked() {
    
        // Create a file chooser dialog
        Gtk::FileChooserDialog dialog("Please choose a .json file",
                                      Gtk::FILE_CHOOSER_ACTION_OPEN);
        dialog.set_transient_for(*this);

        // Add buttons to the dialog
        dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
        dialog.add_button("_Open", Gtk::RESPONSE_OK);

        // Filter for .json files
        auto filter_json = Gtk::FileFilter::create();
        filter_json->set_name("json files (*.json)");
        filter_json->add_pattern("*.json");
        dialog.add_filter(filter_json);

        // Run the dialog
        int result = dialog.run();

        // Check the result
        bool flag=false;
        std::string input_filename;
        switch (result) {
        case Gtk::RESPONSE_OK:
            input_filename = dialog.get_filename();
            flag=true;
            break;
        case Gtk::RESPONSE_CANCEL:
            std::cout << "File selection canceled." << std::endl;
            break;
        default:
            std::cerr << "Unexpected button clicked." << std::endl;
            break;
        }
        
        if (flag){
            parametersInputWindow->set_pantograph_type(pantograph_type_combo->get_active_text());
            parametersInputWindow->readJson(dialog.get_filename());
            parametersInputWindow->show();
            submit_button->set_sensitive(true);
        } 
    }
    
    void SimulationInputWindow::on_save_button_clicked() {
        
        double tempd;
        int    tempi;

        const std::string dirname=outdir_entry->get_text();
        std::regex pattern("^[a-zA-Z0-9_\\-\\.]+$");
        if (!std::regex_match(dirname, pattern))
        {
            err_label->set_visible(true);
            err_label->set_text("check output directory");
            return;
        }
        
        if(!is_integer(nelem_entry->get_text()))
        {
            err_label->set_visible(true);
            err_label->set_text("check number of elements");
            return;
        }
        tempi=std::stoi(nelem_entry->get_text());
        if (tempi<=0)
        {
            err_label->set_visible(true);
            err_label->set_text("check number of elements");
            return;
        }
        
        if(!is_real_number(uplift_force_entry->get_text()))
        {
            err_label->set_visible(true);
            err_label->set_text("check uplift force");
            return;
        }
        tempd=std::stod(uplift_force_entry->get_text());
        if (tempd<0.0)
        {
            err_label->set_visible(true);
            err_label->set_text("uplift force must be positive");
            return;
        }    
        
        if(!is_real_number(tstep_entry->get_text()))
        {
            err_label->set_visible(true);
            err_label->set_text("check time-step");
            return;
        }
        tempd=std::stod(tstep_entry->get_text());
        if (tempd<=0.0)
        {
            err_label->set_visible(true);
            err_label->set_text("time-step must be positive");
            return;
        }    
        
        if(!is_real_number(specradius_entry->get_text()))
        {
            err_label->set_visible(true);
            err_label->set_text("check spectral radius");
            return;
        }
        tempd=std::stod(specradius_entry->get_text());
        if (tempd<0.0||tempd>1.0)
        {
            err_label->set_visible(true);
            err_label->set_text("incorrect spectral radius");
            return;
        }    
        
        if(!is_real_number(alphaval_entry->get_text()))
        {
            err_label->set_visible(true);
            err_label->set_text("check alpha");
            return;
        }
        tempd=std::stod(alphaval_entry->get_text());
        if (tempd<-0.33||tempd>0.0)
        {
            err_label->set_visible(true);
            err_label->set_text("incorrect alpha");
            return;
        }    

        if (parametersInputWindow->writeSuccess){
            Gtk::FileChooserDialog dialog("Save As", Gtk::FILE_CHOOSER_ACTION_SAVE);
            dialog.set_transient_for(*this);
            dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
            dialog.add_button("_Save", Gtk::RESPONSE_OK);
            dialog.set_current_name("sims.json");
            auto filter_txt = Gtk::FileFilter::create();
            filter_txt->set_name("json files (*.json)");
            filter_txt->add_pattern("*.json");
            dialog.add_filter(filter_txt);

            // Show the dialog and wait for user response
            int result = dialog.run();

            if (result == Gtk::RESPONSE_OK){
                std::cout << " Simulation options entered " << std::endl;
                writeJson(dialog.get_filename());
                sim_filename=dialog.get_filename();
                hide();
            }
        }
        else{
            err_label->set_visible(true);
            err_label->set_text("parameters not saved");
        }
    }
    
    // Callback function for paraminput ComboBox change
    void SimulationInputWindow::on_paraminput_changed() {
        // Enable/disable and show/hide the appropriate entry field based on the selected paraminput
        if (paraminput_combo->get_active_text() == "Load from file") {
            parametersInputWindow->hide();
            create_new_button->set_visible(false);
            choose_file_button->set_visible(true);
            create_new_button->set_sensitive(false);
            choose_file_button->set_sensitive(true);
        } else {
            create_new_button->set_visible(true);
            choose_file_button->set_visible(false);
            create_new_button->set_sensitive(true);
            choose_file_button->set_sensitive(false);
        }
    }
    
    
    // Callback function for the "Create New" button click
    void SimulationInputWindow::on_create_new_clicked() {
        parametersInputWindow->show();
        submit_button->set_sensitive(true);    
    }
    
    
    // Callback function for Sinmulation type ComboBox change
    void SimulationInputWindow::on_simulation_type_changed() {
        // Enable/disable and show/hide the appropriate entry field based on the selected input
        if (simulation_type_combo->get_active_text() == "Linear Static") {
            nspan_label->set_visible(false);
            nspan_spinButton->set_visible(false);
            pantograph_type_label->set_visible(false);
            pantograph_type_combo->set_visible(false);
            contact_type_label->set_visible(false);
            contact_type_combo->set_visible(false);
            tstep_label->set_visible(false);
            tstep_entry->set_visible(false);
            timeint_type_label->set_visible(false);
            timeint_type_combo->set_visible(false);
            specradius_label->set_visible(false);
            specradius_entry->set_visible(false);
            alphaval_label->set_visible(false);
            alphaval_entry->set_visible(false);
            
            adjust_iterations_label->set_visible(true);
            adjust_iterations_spinButton->set_visible(true);
            uplift_force_label->set_visible(true);
            uplift_force_entry->set_visible(true);
            
            parametersInputWindow->set_simulation_type(simulation_type_combo->get_active_text());
            resize(1, 1);
            //submit_button->set_sensitive(false);
        } else if (simulation_type_combo->get_active_text() == "Nonlinear Static") {
            nspan_label->set_visible(false);
            nspan_spinButton->set_visible(false);
            pantograph_type_label->set_visible(false);
            pantograph_type_combo->set_visible(false);
            contact_type_label->set_visible(false);
            contact_type_combo->set_visible(false);
            tstep_label->set_visible(false);
            tstep_entry->set_visible(false);
            timeint_type_label->set_visible(false);
            timeint_type_combo->set_visible(false);
            specradius_label->set_visible(false);
            specradius_entry->set_visible(false);
            alphaval_label->set_visible(false);
            alphaval_entry->set_visible(false);
            
            adjust_iterations_label->set_visible(true);
            adjust_iterations_spinButton->set_visible(true);
            uplift_force_label->set_visible(true);
            uplift_force_entry->set_visible(true);
            
            parametersInputWindow->set_simulation_type(simulation_type_combo->get_active_text());
            resize(1, 1);
            //submit_button->set_sensitive(false);
        } else if (simulation_type_combo->get_active_text() == "Linear Dynamic") {
            nspan_label->set_visible(true);
            nspan_spinButton->set_visible(true);
            pantograph_type_label->set_visible(true);
            pantograph_type_combo->set_visible(true);
            contact_type_label->set_visible(false);
            contact_type_combo->set_visible(false);
            tstep_label->set_visible(true);
            tstep_entry->set_visible(true);
            timeint_type_label->set_visible(true);
            timeint_type_combo->set_visible(true);
            specradius_label->set_visible(true);
            specradius_entry->set_visible(true);
            alphaval_label->set_visible(true);
            alphaval_entry->set_visible(true);
            
            adjust_iterations_label->set_visible(false);
            adjust_iterations_spinButton->set_visible(false);
            uplift_force_label->set_visible(false);
            uplift_force_entry->set_visible(false);
            
            on_timeint_changed();
            parametersInputWindow->set_simulation_type(simulation_type_combo->get_active_text());
            resize(1, 1);
            //submit_button->set_sensitive(false);
        } else {
            nspan_label->set_visible(true);
            nspan_spinButton->set_visible(true);
            pantograph_type_label->set_visible(true);
            pantograph_type_combo->set_visible(true);
            if (pantograph_type_combo->get_active_text() == "pantograph 0"){
                contact_type_label->set_visible(false);
                contact_type_combo->set_visible(false);
            }else{
                contact_type_label->set_visible(true);
                contact_type_combo->set_visible(true);
            }
            tstep_label->set_visible(true);
            tstep_entry->set_visible(true);
            timeint_type_label->set_visible(true);
            timeint_type_combo->set_visible(true);
            specradius_label->set_visible(true);
            specradius_entry->set_visible(true);
            alphaval_label->set_visible(true);
            alphaval_entry->set_visible(true);
            
            adjust_iterations_label->set_visible(false);
            adjust_iterations_spinButton->set_visible(false);
            uplift_force_label->set_visible(false);
            uplift_force_entry->set_visible(false);
            
            on_timeint_changed();
            parametersInputWindow->set_simulation_type(simulation_type_combo->get_active_text());
            resize(1, 1);
            //submit_button->set_sensitive(false);
        }
    }
    
    
    // Callback function for ComboBox change
    void SimulationInputWindow::on_pantograph_type_changed()
    {   
        if (pantograph_type_combo->get_active_text() == "pantograph 0"){
            contact_type_label->set_visible(false);
            contact_type_combo->set_visible(false);
            resize(1, 1);
        }else{
            contact_type_label->set_visible(true);
            contact_type_combo->set_visible(true);
            on_simulation_type_changed();
            resize(1, 1);
        }
        parametersInputWindow->set_pantograph_type(pantograph_type_combo->get_active_text());
    }
        
    void SimulationInputWindow::on_contact_type_changed()
    {   
        parametersInputWindow->set_contact_type(contact_type_combo->get_active_text());
    }
    
    void SimulationInputWindow::on_adjust_iterations_value_changed(){
        if(adjust_iterations_spinButton->get_value_as_int()==0)
            parametersInputWindow->set_do_adjustment(false);
        else
            parametersInputWindow->set_do_adjustment(true);
    }
    
    void SimulationInputWindow::on_timeint_changed() {
        // Enable/disable and show/hide the appropriate entry field based on the selected timeint
        if (timeint_type_combo->get_active_text() == "generalized alpha") {
            specradius_entry->set_sensitive(true);
            specradius_entry->set_visible(true);
            alphaval_entry->set_sensitive(false);
            alphaval_entry->set_visible(false);
                        
            specradius_label->set_sensitive(true);
            specradius_label->set_visible(true);
            alphaval_label->set_sensitive(false);
            alphaval_label->set_visible(false);
        } else if(timeint_type_combo->get_active_text() == "hht alpha"){
            specradius_entry->set_sensitive(false);
            specradius_entry->set_visible(false);
            alphaval_entry->set_sensitive(true);
            alphaval_entry->set_visible(true);
            
            specradius_label->set_sensitive(false);
            specradius_label->set_visible(false);
            alphaval_label->set_sensitive(true);
            alphaval_label->set_visible(true);
        } else {
            specradius_entry->set_sensitive(false);
            specradius_entry->set_visible(false);
            alphaval_entry->set_sensitive(false);
            alphaval_entry->set_visible(false);
            
            specradius_label->set_sensitive(false);
            specradius_label->set_visible(false);
            alphaval_label->set_sensitive(false);
            alphaval_label->set_visible(false);
            
            resize(1, 1);
        }
    }
    
    void SimulationInputWindow::get_default_values(){
        
        nelem_value=20;
        adjust_iterations_value=5;
        uplift_force_value=100;
        nspan_value=10;
        tstep_value=0.0015;
        alphaval_value=-.1;
        specradius_value=0.5;
        
    }
    
    void SimulationInputWindow::writeJson(std::string fileName){

        // Write JSON data to a file
        std::ofstream outputFile(fileName);
        if (outputFile.is_open()) {
        
            outputFile << "{";
            outputFile << "\n\t\"demo\": {" << "\n";

            if (simulation_type_combo->get_active_text() == "Linear Static") {
                outputFile << "\t\t\"simulation type\": " << "\"linear static\"" << ",\n";
                outputFile << "\t\t\"parameters file\": " << "\""<< parametersInputWindow->output_filename <<"\"" << ",\n";
                outputFile << "\t\t\"num elements\": " << nelem_entry->get_text() << ",\n";
                outputFile << "\t\t\"length adjustment iterations\": " << adjust_iterations_spinButton->get_value_as_int() << ",\n";
                outputFile << "\t\t\"force\": " << uplift_force_entry->get_text() << ",\n";
            }else if(simulation_type_combo->get_active_text() == "Nonlinear Static"){
                outputFile << "\t\t\"simulation type\": " << "\"nonlinear static\"" << ",\n";
                outputFile << "\t\t\"parameters file\": " << "\""<< parametersInputWindow->output_filename <<"\"" << ",\n";
                outputFile << "\t\t\"num elements\": " << nelem_entry->get_text() << ",\n";
                outputFile << "\t\t\"length adjustment iterations\": " << adjust_iterations_spinButton->get_value_as_int() << ",\n";
                outputFile << "\t\t\"force\": " << uplift_force_entry->get_text() << ",\n";
            }else if(simulation_type_combo->get_active_text() == "Linear Dynamic"){
                outputFile << "\t\t\"simulation type\": " << "\"linear dynamic\"" << ",\n";
                outputFile << "\t\t\"parameters file\": " << "\""<< parametersInputWindow->output_filename <<"\"" << ",\n";
                outputFile << "\t\t\"num spans\": " << nspan_spinButton->get_value_as_int() << ",\n";
                outputFile << "\t\t\"pantograph type\": " << (pantograph_type_combo->get_active_text()).substr(11,1) << ",\n";
                outputFile << "\t\t\"pantograph tag\": \"" << pantograph_type_combo->get_active_text() << "\",\n";                
                outputFile << "\t\t\"contact type\": \"persistent\",\n";
                outputFile << "\t\t\"num elements\": " << nelem_entry->get_text() << ",\n";
                outputFile << "\t\t\"time integrator\": \"" << timeint_type_combo->get_active_text() << "\",\n";
                if(timeint_type_combo->get_active_text()=="generalized alpha"){
                    outputFile << "\t\t\"spectral radius\": " << specradius_entry->get_text() << ",\n";
                }else if (timeint_type_combo->get_active_text()=="hht alpha"){
                    outputFile << "\t\t\"alpha\": " << alphaval_entry->get_text() << ",\n";
                }
                outputFile << "\t\t\"time step\": " << tstep_entry->get_text() << ",\n";
                outputFile << "\t\t\"num time steps\": " << parametersInputWindow->get_nsteps(nspan_spinButton->get_value_as_int(),std::stod(tstep_entry->get_text())) << ",\n";
            }else{
                outputFile << "\t\t\"simulation type\": " << "\"nonlinear dynamic\"" << ",\n";
                outputFile << "\t\t\"parameters file\": " << "\""<< parametersInputWindow->output_filename <<"\"" << ",\n";
                outputFile << "\t\t\"num spans\": " << nspan_spinButton->get_value_as_int() << ",\n";
                outputFile << "\t\t\"pantograph type\": " << (pantograph_type_combo->get_active_text()).substr(11,1) << ",\n";
                outputFile << "\t\t\"pantograph tag\": \"" << pantograph_type_combo->get_active_text() << "\",\n";
                if(pantograph_type_combo->get_active_text()=="pantograph 0"){
                    outputFile << "\t\t\"contact type\": \"persistent\",\n";
                }else{
                    outputFile << "\t\t\"contact type\": \"" << contact_type_combo->get_active_text() << "\",\n";
                }
                outputFile << "\t\t\"num elements\": " << nelem_entry->get_text() << ",\n";
                outputFile << "\t\t\"time integrator\": \"" << timeint_type_combo->get_active_text() << "\",\n";
                if(timeint_type_combo->get_active_text()=="generalized alpha"){
                    outputFile << "\t\t\"spectral radius\": " << specradius_entry->get_text() << ",\n";
                }else if (timeint_type_combo->get_active_text()=="hht alpha"){
                    outputFile << "\t\t\"alpha\": " << alphaval_entry->get_text() << ",\n";
                }
                outputFile << "\t\t\"time step\": " << tstep_entry->get_text() << ",\n";
                outputFile << "\t\t\"num time steps\": " << parametersInputWindow->get_nsteps(nspan_spinButton->get_value_as_int(),std::stod(tstep_entry->get_text())) << ",\n";
            } 
            outputFile << "\t\t\"output directory\": \"" << outdir_entry->get_text() << "\"\n\t}\n";
                                          
            outputFile << "}";
    
            outputFile.close();
            std::cout << "JSON data has been written to " << fileName << std::endl;
        } else {
            std::cerr << "Error opening the file: " << fileName << std::endl;
        }
        
    }
    bool SimulationInputWindow::is_real_number(std::string str) {
        std::istringstream iss(str);
        double value;
        return (iss >> value) && iss.eof();
    }
    bool SimulationInputWindow::is_integer(std::string str) {
        std::istringstream iss(str);
        int value;
        return (iss >> value) && iss.eof();
    }

  }
}
