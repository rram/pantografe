
#pragma once

#include <pfe_gui_DynamicOutputWindow.h>

namespace pfe
{
  namespace gui
  {
    class MovingLoadOutputWindow: public DynamicOutputWindow
    {
    public:
      //! Constructor
      MovingLoadOutputWindow(std::string analysis_jsonfile, std::string analysis_tag);

      //! Destructor
      virtual ~MovingLoadOutputWindow() = default;

      // Disable copy and assignment
      MovingLoadOutputWindow(const MovingLoadOutputWindow&) = delete;
      MovingLoadOutputWindow operator=(const MovingLoadOutputWindow&) = delete;

    private:
      void printStatistics() override;
      void on_history_type_changed() override; 
    };
    
  }
}
