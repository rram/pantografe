
#include <pfe_gui_StaticOutputWindow.h>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iomanip>

namespace pfe
{
  namespace gui
  {
    // Constructor
    StaticOutputWindow::StaticOutputWindow(std::string analysis_jsonfile, std::string analysis_tag)
      :simParams(analysis_jsonfile, analysis_tag, false) // dont erase contents of output folder
    {
      set_title("Static Analysis of OHE");
      set_default_size(500, 300);
      set_position(Gtk::WIN_POS_CENTER);
      move(100,100);

      // Create an outer hbox
      auto hbox = new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10);
      Gtk::manage(hbox);
      add(*hbox);
      hbox->set_margin_start(10);
      hbox->set_margin_end(10);
      hbox->set_margin_top(10);
      hbox->set_margin_bottom(10);
      
      // left_vbox packed to the left of hbox
      left_vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 10));
      left_vbox->set_size_request(250, -1);
      hbox->pack_start(*left_vbox, Gtk::PACK_SHRINK);

      // left_vbox contains:
      // (i)   dropdown menu to choose statistics/plot
      // (ii)  plot_vbox containing plot type and axis limits
      // (iii) button to display
      // (iv)  button to print statistics

      // (i)
      // Create a dropdown box to choose statistics or plot
      auto label = Gtk::manage(new Gtk::Label("Select Output Type:", Gtk::ALIGN_START));
      left_vbox->pack_start(*label, Gtk::PACK_SHRINK);
      output_type_combo = Gtk::manage(new Gtk::ComboBoxText());
      left_vbox->pack_start(*output_type_combo, Gtk::PACK_SHRINK);
      output_type_combo->append("Statistics");
      output_type_combo->append("Plots");
      output_type_combo->set_active(1); // default is statistics
      output_type_combo->signal_changed().connect(sigc::mem_fun(*this, &StaticOutputWindow::on_output_type_changed));

      // (ii)
      // Create plot_vbox containing a dropdown for data to plot and the axis limits
      plot_vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 10));
      auto separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
      plot_vbox->pack_start(*separator, Gtk::PACK_SHRINK);

      // Create a dropdown box to choose data to plot
      // change history_type_combo -> field_type
      label = Gtk::manage(new Gtk::Label("Select Plot Type:", Gtk::ALIGN_START));
      plot_vbox->pack_start(*label, Gtk::PACK_SHRINK);
      history_type_combo = Gtk::manage(new Gtk::ComboBoxText());
      plot_vbox->pack_start(*history_type_combo, Gtk::PACK_SHRINK);
      history_type_combo->append("displacement");
      history_type_combo->append("stiffness");
      history_type_combo->set_active(0);
      history_type_combo->signal_changed().connect(sigc::mem_fun(*this, &StaticOutputWindow::on_history_type_changed));

      // axis labels
      label = Gtk::manage(new Gtk::Label("x-axis limits:", Gtk::ALIGN_START));
      plot_vbox->pack_start(*label, Gtk::PACK_SHRINK);
      auto axis_hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10));
      plot_vbox->pack_start(*axis_hbox, Gtk::PACK_SHRINK);

      x1_entry = Gtk::manage(new Gtk::Entry());
      x1_entry->set_width_chars(8);
      x1_entry->set_text("*");
      x1_entry->signal_changed().connect(sigc::mem_fun(*this, &StaticOutputWindow::on_x1_x2_entry_changed));
      axis_hbox->pack_start(*x1_entry, Gtk::PACK_SHRINK);
      x2_entry = Gtk::manage(new Gtk::Entry());
      x2_entry->set_width_chars(8);
      x2_entry->set_text("*");
      x2_entry->signal_changed().connect(sigc::mem_fun(*this, &StaticOutputWindow::on_x1_x2_entry_changed));
      axis_hbox->pack_start(*x2_entry, Gtk::PACK_SHRINK);
      auto axis_reset_button = Gtk::manage(new Gtk::Button("auto"));
      axis_reset_button->signal_clicked().connect(sigc::mem_fun(*this, &StaticOutputWindow::reset_xaxis));
      axis_hbox->pack_end(*axis_reset_button, Gtk::PACK_SHRINK);
        
      label = Gtk::manage(new Gtk::Label("y-axis limits:", Gtk::ALIGN_START));
      plot_vbox->pack_start(*label, Gtk::PACK_SHRINK);
      axis_hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10));
      plot_vbox->pack_start(*axis_hbox, Gtk::PACK_SHRINK);

      y1_entry = Gtk::manage(new Gtk::Entry());
      y1_entry->set_width_chars(8);
      y1_entry->set_text("*");
      y1_entry->signal_changed().connect(sigc::mem_fun(*this, &StaticOutputWindow::on_y1_y2_entry_changed));
      axis_hbox->pack_start(*y1_entry, Gtk::PACK_SHRINK);
      y2_entry = Gtk::manage(new Gtk::Entry());
      y2_entry->set_width_chars(8);
      y2_entry->set_text("*");
      y2_entry->signal_changed().connect(sigc::mem_fun(*this, &StaticOutputWindow::on_y1_y2_entry_changed));

      axis_hbox->pack_start(*y2_entry, Gtk::PACK_SHRINK);
      axis_reset_button = Gtk::manage(new Gtk::Button("auto"));
      axis_reset_button->signal_clicked().connect(sigc::mem_fun(*this, &StaticOutputWindow::reset_yaxis));
      axis_hbox->pack_end(*axis_reset_button, Gtk::PACK_SHRINK);

      separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL)); 
      plot_vbox->pack_start(*separator, Gtk::PACK_SHRINK); 
      left_vbox->pack_start(*plot_vbox, Gtk::PACK_SHRINK); 

      // (iii)
      // create a button to display a plot
      plot_button = Gtk::manage(new Gtk::Button("plot"));
      plot_button->signal_clicked().connect(sigc::mem_fun(*this, &StaticOutputWindow::display_plot));
      left_vbox->pack_end(*plot_button, Gtk::PACK_SHRINK);

      // (iv)
      // create a button to print statistics
      stat_print_button = Gtk::manage(new Gtk::Button("compute"));
      stat_print_button->signal_clicked().connect(sigc::mem_fun(*this, &StaticOutputWindow::printStatistics));
      left_vbox->pack_end(*stat_print_button, Gtk::PACK_SHRINK);
          
      //-------------------------------------------------------------------------------------

      separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_VERTICAL));
      hbox->pack_start(*separator, Gtk::PACK_SHRINK);
        
      //-------------------------------------------------------------------------------------

      // To the right of the left_vbox, is either a stats frame or a plot frame

      // Create the statistics frame
      statistics_frame = Gtk::manage(new Gtk::Frame());
      statistics_frame->set_label("Statistics Output :");
      hbox->pack_start(*statistics_frame, Gtk::PACK_SHRINK);

      // statistics frame contains a stat_vbox
      // stat_vbox contains:
      // a scroll window, which contains an output text box
      // a save button to write stats to file

      // Create stat_vbox and attach it to the frame
      stat_vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 10));
      statistics_frame->add(*stat_vbox);
      
      // Create a scrolled window for the output box and pack it inside stat_vbox
      auto scrolled_window = Gtk::manage(new Gtk::ScrolledWindow());
      scrolled_window->set_min_content_height(490);
      scrolled_window->set_min_content_width(660);
      stat_vbox->pack_start(*scrolled_window, Gtk::PACK_EXPAND_WIDGET);

      // Create a text box inside the scrolled window to display stats
      stat_textbox = Gtk::manage(new Gtk::TextView());
      stat_textbox->set_editable(false);
      stat_textbox->set_cursor_visible(false);
      stat_textbox->set_wrap_mode(Gtk::WRAP_WORD_CHAR);
      scrolled_window->add(*stat_textbox);

      separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
      stat_vbox->pack_start(*separator, Gtk::PACK_SHRINK);

      // save button to write stats to file
      stat_save_button = Gtk::manage(new Gtk::Button("save"));
      stat_save_button->signal_clicked().connect(sigc::mem_fun(*this, &StaticOutputWindow::on_stats_save_button_clicked));
      stat_vbox->pack_end(*stat_save_button, Gtk::PACK_SHRINK);
      stat_save_button->set_sensitive(false);

      //------------------------------------------------------------------------------------
      
      // Create a frame for plotting
      plot_frame = Gtk::manage(new Gtk::Frame());
      plot_frame->set_label("Image Output");
      hbox->pack_start(*plot_frame, Gtk::PACK_SHRINK);

      // plot_frame contains a vbox
      auto plot_frame_vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 10));
      plot_frame->add(*plot_frame_vbox);
      
      // plot_frame_vbox contains
      // (i) image output
      // (ii) button to save

      // (i)
      // Create an image widget
      image = Gtk::manage(new Gtk::Image());
      image->set_size_request(660, 490);
      plot_frame_vbox->pack_start(*image, Gtk::PACK_SHRINK);

      separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
      plot_frame_vbox->pack_start(*separator, Gtk::PACK_SHRINK);
      
      // Create a save button
      plot_save_button = Gtk::manage(new Gtk::Button("save"));
      plot_save_button->signal_clicked().connect(sigc::mem_fun(*this, &StaticOutputWindow::saveplot));
      plot_frame_vbox->pack_end(*plot_save_button, Gtk::PACK_SHRINK);
      plot_save_button->set_sensitive(false);
                
      //------------------------------------------------------------------------------------      
      gplot.has_xrange = false;
      gplot.has_yrange = false;
      
      // Show all widgets
      show_all_children();
      on_output_type_changed();
      on_history_type_changed();
      disable_axis_manipulation();
      
      set_resizable(false);
      resize(1,1);
    }

    // Handle dropdown choice of statistics or plotting output
    void StaticOutputWindow::on_output_type_changed()
    {
      if (output_type_combo->get_active_text() == "Statistics")
	{
	  plot_frame->set_visible(false);
	  plot_vbox->set_visible(false);
	  plot_button->set_visible(false);
	  
	  statistics_frame->set_visible(true);
	  stat_print_button->set_visible(true);
	}
      else
	{
	  plot_frame->set_visible(true);
	  plot_vbox->set_visible(true);
	  plot_button->set_visible(true);

	  statistics_frame->set_visible(false);
	  stat_print_button->set_visible(false);
	}

      //resize(1,1);
      return;
    }



    void StaticOutputWindow::printStatistics()
    {
      // lambda to read the column of a file
      auto read_2ndcolumn = [](std::string filename)
	{
	  std::vector<double> col2{};
	  assert(std::filesystem::exists(filename));
	  std::fstream in;
	  in.open(filename, std::ios::in);
	  assert(in.good());
	  double val;
	  in >> val; // col 1
	  while(in.good())
	    {
	      in >> val; // col 2
	      col2.push_back(val);
	      in >> val; // col
	      if(!in.good()) {
		   in.close();
	         return col2;
		}
	    }
	  in.close();
	  return col2;
	};
	
	// lambda to read the nth column of a file
	auto read_jthcolumn = [](std::string filename, const int numcols, const int j)
	{
	  std::vector<double> coln{};
	  assert(std::filesystem::exists(filename));
	  std::fstream in;
	  in.open(filename, std::ios::in);
	  assert(in.good());
	  std::vector<double> row(numcols);
	  // ignore the first comment line
	  std::string line1;
	  std::getline(in, line1);
	  while(true)
	    {
	      for(int k=0; k<numcols; ++k){
		    in >> row[k];
		    if(!in.good()) {
		      in.close();
	            return coln;
		    }
		}
	      coln.push_back(row[j]);
	    }
	   return coln;
	};

      
      // read contact.dat containing x vs w, get min and max values of w
      std::vector<double> wvals = read_2ndcolumn(simParams.outdir + "/contact.dat");
      double max_w = *std::max_element(wvals.begin(), wvals.end());
      double min_w = *std::min_element(wvals.begin(), wvals.end());
	
      // read stiffness.dat containing x vs K. save the vector of K
      std::vector<double> kvals = read_2ndcolumn(simParams.outdir + "/stiffness.dat");
      double max_K = *std::max_element(kvals.begin(), kvals.end());
      double min_K = *std::min_element(kvals.begin(), kvals.end());
      double mean_K = std::accumulate(kvals.begin(), kvals.end(), 0.)/static_cast<double>(kvals.size());
      double sq_sum_K = std::inner_product(kvals.begin(), kvals.end(), kvals.begin(), 0.0);
      double stdev_K = std::sqrt(sq_sum_K/kvals.size() - mean_K*mean_K);

      // read droppers.dat containing dropper coordinates in deformed configuration
      std::vector<double> dvals = read_2ndcolumn(simParams.outdir + "/droppers.dat");

      //reading dropper EA from parameters file
      std::fstream jfile;
      jfile.open(simParams.params_file, std::ios::in);
      assert(jfile.good());
      auto J = nlohmann::json::parse(jfile);
      jfile.close();
      assert(J.empty()==false);
      
      double EA_dropper=0.0;
      {auto it = J.find("droppers");
      assert(it!=J.end());
      auto& j = *it;
      assert(j.empty()==false);
      assert(j.contains("EA"));
      j["EA"].get_to(EA_dropper);}

      // flush to the buffer
      std::stringstream ss;
      ss << std::endl
	 << "Displacement" << " [mm]: " << std::endl
	 << "Minimum: " << min_w*1000 << std::endl
	 << "Maximum: " << max_w*1000 << std::endl
	 << "---------------------" << std::endl
	 << std::endl
	 << "Stiffness" << " [N/m]:\n"
	 << "Mean: " << mean_K << std::endl
	 << "Standard Deviation: " << stdev_K << std::endl
	 << "Minimum: " << min_K << std::endl
	 << "Maximum: " << max_K << std::endl
	 << "---------------------" << std::endl;
      
      if(simParams.num_length_iters>0)
      {
         ss << "\nDropper  -  Adjusted Length [m] - Dropper Force [N] - M.W. sag [m] - C.W. sag [m]\n";
         std::vector<double> newlengths = read_jthcolumn(simParams.outdir+"/dropper_adjustments.dat", 6, 5);
         for (int j=0;j<newlengths.size();j++)
         {
            ss << "\t" << j+1 << std::setprecision(5) << std::setw(30) << newlengths[j] << std::setprecision(4) << std::setw(30) << ((dvals[j*2] - dvals[j*2+1])-newlengths[j])*EA_dropper/newlengths[j] << std::setprecision(4) << std::setw(30) << dvals[j*2] << std::setprecision(4) << std::setw(25) << dvals[j*2+1] << std::endl;
         }
      }
      else
      {
         std::vector<double> dlengths;
         int ndroppers=0.0;
         {auto it = J.find("dropper schedule");
         assert(it!=J.end());
         auto& j = *it;
         assert(j.empty()==false);
         assert(j.contains("nominal lengths"));
         j["nominal lengths"].get_to(dlengths);
         assert(j.contains("number of droppers"));
         j["number of droppers"].get_to(ndroppers);}
      
         ss << "\nDropper - Dropper Force [N] - M.W. sag [m] - C.W. sag [m]\n";
         for (int j=0;j<ndroppers;j++)
         {
            ss << "\t" << j+1 << std::setprecision(4) << std::setw(30) << ((dvals[j*2] - dvals[j*2+1])-dlengths[j])*EA_dropper/dlengths[j] << std::setprecision(4) << std::setw(30) <<  dvals[j*2] << std::setprecision(4) << std::setw(25) << dvals[j*2+1]  << std::endl;
         }
      }
      stat_textbox->get_buffer()->insert_at_cursor(ss.str());

      stat_save_button->set_sensitive(true);
      stat_print_button->set_sensitive(false);
    }


    void StaticOutputWindow::on_stats_save_button_clicked() {
    
      Gtk::FileChooserDialog dialog("Save As", Gtk::FILE_CHOOSER_ACTION_SAVE);
      dialog.set_transient_for(*this);
      dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
      dialog.add_button("_Save", Gtk::RESPONSE_OK);
      dialog.set_current_name("statistics.txt");
      auto filter_txt = Gtk::FileFilter::create();
      filter_txt->set_name("txt files (*.txt)");
      filter_txt->add_pattern("*.txt");
      dialog.add_filter(filter_txt);

      // Show the dialog and wait for user response
      int result = dialog.run();

      if (result == Gtk::RESPONSE_OK){
        
	// Get the buffer from the text view
	auto buffer = stat_textbox->get_buffer();

	// Get the start and end iterators of the buffer
	Gtk::TextIter start = buffer->begin();
	Gtk::TextIter end = buffer->end();

	// Get the text content of the buffer
	std::string text_content = buffer->get_text(start, end);

	// Specify the file path where you want to save the content
	std::string file_path = dialog.get_filename();

	// Open the file for writing
	std::ofstream outfile(file_path);
	assert(outfile.is_open());
	outfile << text_content;
	outfile.close();
      } 
    }
    

   
    void StaticOutputWindow::on_history_type_changed()
    {
      if(history_type_combo->get_active_text() == "displacement")
	{
	  gplot.data_filename = simParams.outdir+"/contact.dat";
	  gplot.img_filename  = "image.tmp";
	  gplot.xlabel = "Distance [m]"; 
	  gplot.ylabel = "Displacement [m]";  
	}
      else
	{
	  gplot.data_filename = simParams.outdir+"/stiffness.dat";
	  gplot.img_filename  = "image.tmp";
	  gplot.xlabel = "Distance [m]"; 
	  gplot.ylabel = "Stiffness [N/m]";  
	}

      gplot.col1 = 1;
      gplot.col2 = 2;
      gplot.has_xrange = false;
      gplot.has_yrange = false;
	  
      reset_xaxis();  
      reset_yaxis();
      disable_axis_manipulation();
    }


    void StaticOutputWindow::display_plot()
    {
      // run gnuplot command
      std::system(gplot.getCommand().c_str());

      // Load the image
      auto pixbuf = Gdk::Pixbuf::create_from_file(gplot.img_filename);
      assert(pixbuf);
      image->set(pixbuf);
      image->show();
      plot_save_button->set_sensitive(true);
      enable_axis_manipulation();
    }
    
    
    void StaticOutputWindow::reset_xaxis()
    {
      x1_entry->set_text("");
      x2_entry->set_text("");
      gplot.has_xrange = false;
    }
    
    void StaticOutputWindow::reset_yaxis()
    {
      y1_entry->set_text("");
      y2_entry->set_text("");
      gplot.has_yrange = false;
    }

    void StaticOutputWindow::saveplot()
    {
      Gtk::FileChooserDialog dialog("Save image", Gtk::FILE_CHOOSER_ACTION_SAVE);
      dialog.set_transient_for(*this);
      dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
      dialog.add_button("_Save", Gtk::RESPONSE_OK);
      dialog.set_current_name("plot.jpg");
      auto file_filter = Gtk::FileFilter::create();
      file_filter->set_name("jpg/csv files");
      file_filter->add_pattern("*.jpg");
      file_filter->add_pattern("*.csv");
      dialog.add_filter(file_filter);

      // Show the dialog and wait for user response
      int result = dialog.run();
      if(result!=Gtk::RESPONSE_OK)
	return;

      // get the filename
      const std::string filename = dialog.get_filename();
      const std::string extn = std::filesystem::path(filename).extension();

      if(extn==".jpg") // save an image
	{
	  auto myplot = gplot;
	  myplot.img_filename = filename;
	  std::system(myplot.getCommand().c_str());
	}
      else if(extn==".csv")  // or the data
	{
	  // flush gplot.col1 and gplot.col2 of the input file into output
	  // eg: awk '{print $2 "," $3}' file1.dat > file2.csv
	  std::string sys_command = "awk '{print $" + std::to_string(gplot.col1) + " \",\" $"+ std::to_string(gplot.col2) + "}' " + gplot.data_filename + " > " + filename;
	  std::cout << "Executing command " << sys_command << std::endl;
	  std::system(sys_command.c_str());
	}
      
      return;
    } 
    
    
    void StaticOutputWindow::disable_axis_manipulation()
    {
      x1_entry->set_sensitive(false);
      x2_entry->set_sensitive(false);
      y1_entry->set_sensitive(false);
      y2_entry->set_sensitive(false);
    }
        
    void StaticOutputWindow::enable_axis_manipulation()
    {
      x1_entry->set_sensitive(true);
      x2_entry->set_sensitive(true);
      y1_entry->set_sensitive(true);
      y2_entry->set_sensitive(true);
    }
    
     
    void StaticOutputWindow::on_x1_x2_entry_changed(){

	if(!is_real_number(x1_entry->get_text())||!is_real_number(x2_entry->get_text()))
	{
	  gplot.has_xrange = false;
	  return;
	}
      
	double x1 = std::stod(x1_entry->get_text());
	double x2 = std::stod(x2_entry->get_text());

      // invalid range
      if(x1>x2)
      { 
        gplot.has_xrange = false;
	  return;
	}
      
      gplot.has_xrange = true;
      gplot.xlo = x1;
      gplot.xhi = x2;
    }

    
    void StaticOutputWindow::on_y1_y2_entry_changed()
    {	
      if(!is_real_number(y1_entry->get_text())||!is_real_number(y2_entry->get_text()))
	{
	  gplot.has_yrange = false;
	  return;
	}

	double y1 = std::stod(y1_entry->get_text());
	double y2 = std::stod(y2_entry->get_text());

      // invalid range
      if(y1>y2)
      { 
        gplot.has_yrange = false;
	  return;
	}
      
      gplot.has_yrange = true;
      gplot.ylo = y1;
      gplot.yhi = y2;
    }
      

    std::string GnuLinePlot::getCommand()
    {
      std::string gnu_command{};
      gnu_command  = "gnuplot -persist <<-EOFMarker\n";
      gnu_command += "set term jpeg\nunset key\n";
      gnu_command += "set output \"" + img_filename + "\"\n";
      gnu_command += "set xlabel \"" + xlabel + "\"\n";
      gnu_command += "set ylabel \"" + ylabel + "\"\n";
      gnu_command += "set grid\n";
    
      if(has_xrange)
	gnu_command += "set xrange [" + std::to_string(xlo) + ":" + std::to_string(xhi) + "]\n";
      else
	gnu_command += "set autoscale xfix\n";

      if(has_yrange)
	gnu_command += "set yrange [" + std::to_string(ylo) + ":" + std::to_string(yhi) + "]\n";
      else
	gnu_command += "set autoscale yfix\n";
    
      gnu_command +=
	"plot \"" + data_filename + "\"" +
	" u " + std::to_string(col1) + ":" + std::to_string(col2) + " w l lw 2";

      //std::cout << "Gnu command: " << gnu_command << std::endl;
      return gnu_command;
    }
    
    std::string GnuLinePlot::getCommandF()
    {
      std::string gnu_command{};
      gnu_command  = "gnuplot -persist <<-EOFMarker\n";
      gnu_command += "set term jpeg\nset key box top left\n";
      gnu_command += "set output \"" + img_filename + "\"\n";
      gnu_command += "set xlabel \"" + xlabel + "\"\n";
      gnu_command += "set ylabel \"" + ylabel + "\"\n";
      gnu_command += "set grid\n";
    
      if(has_xrange)
	gnu_command += "set xrange [" + std::to_string(xlo) + ":" + std::to_string(xhi) + "]\n";
      else
	gnu_command += "set autoscale xfix\n";

      if(has_yrange)
	gnu_command += "set yrange [" + std::to_string(ylo) + ":" + std::to_string(yhi) + "]\n";
      else
	gnu_command += "set autoscale yfix\n";
    
      gnu_command +=
	"plot \"" + data_filename + "\"" +
	" u " + std::to_string(col1) + ":" + std::to_string(col2) + " w l lw 0.5 title \"Unfiltered\", \""+
	data_filename + ".filtered\"" +
	" u " + std::to_string(col1) + ":" + std::to_string(col2) + " w l lw 2 title \"Filtered\"";

      //std::cout << "Gnu command: " << gnu_command << std::endl;
      return gnu_command;
    }
    
    bool StaticOutputWindow::is_real_number(std::string str) {
        std::istringstream iss(str);
        double value;
        return (iss >> value) && iss.eof();
    }

  }
}
