
#include <pfe_gui_DynamicOutputWindow.h>
#include <fstream>
#include <numeric>
#include <thread>
#include <fftw3.h>
#include <sstream>
#include <algorithm>

namespace pfe
{
  namespace gui
  {
    // Constructor
    DynamicOutputWindow::DynamicOutputWindow(std::string analysis_jsonfile, std::string analysis_tag)
      :simParams(analysis_jsonfile, analysis_tag, false) // don't erase contents of the output folder
    {
      set_title("Moving Pantograph on OHE");
      set_default_size(500, 300);
      set_position(Gtk::WIN_POS_CENTER);
      move(100,100);
      
      //get number of timesteps executed by user (0 to nsteps-1)
      nsteps=0;
      std::ifstream inputFile(simParams.outdir+"/history.dat");
      assert(inputFile.is_open());
      std::string line;
      while (std::getline(inputFile, line)) nsteps++;
      inputFile.close();
      nsteps=nsteps-1; // 1st line is comment
      //std::cout << nsteps << std::endl;
      numcols=8; //number of columns in history file

      // Create an outer hbox
      auto hbox = new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10);
      Gtk::manage(hbox);
      add(*hbox);
      hbox->set_margin_start(10);
      hbox->set_margin_end(10);
      hbox->set_margin_top(10);
      hbox->set_margin_bottom(10);
      
      // left_vbox packed to the left of hbox
      left_vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 10));
      left_vbox->set_size_request(253, -1);
      hbox->pack_start(*left_vbox, Gtk::PACK_SHRINK);

      // left_vbox contains:
      // (i)   dropdown menu to choose statistics/plot
      // (ii)  plot_vbox containing plot type and axis limits
      // (iii) button to display
      // (iv)  button to print statistics

      // (i)
      // Create a dropdown box to choose statistics or plot
      auto label = Gtk::manage(new Gtk::Label("Select Output Type:", Gtk::ALIGN_START));
      left_vbox->pack_start(*label, Gtk::PACK_SHRINK);
      output_type_combo = Gtk::manage(new Gtk::ComboBoxText());
      left_vbox->pack_start(*output_type_combo, Gtk::PACK_SHRINK);
      output_type_combo->append("Statistics");
      output_type_combo->append("Plots");
      output_type_combo->append("Animation");
      output_type_combo->set_active(0); // default is statistics
      output_type_combo->signal_changed().connect(sigc::mem_fun(*this, &DynamicOutputWindow::on_output_type_changed));

      // (ii)
      // Create plot_vbox containing a dropdown for data to plot and the axis limits
      plot_vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 10));
      auto separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
      plot_vbox->pack_start(*separator, Gtk::PACK_SHRINK);

      // Create a dropdown box to choose data to plot
      // change history_type_combo -> field_type
      label = Gtk::manage(new Gtk::Label("Select Plot Type:", Gtk::ALIGN_START));
      plot_vbox->pack_start(*label, Gtk::PACK_SHRINK);
      history_type_combo = Gtk::manage(new Gtk::ComboBoxText());
      plot_vbox->pack_start(*history_type_combo, Gtk::PACK_SHRINK);
      history_type_combo->append("displacement vs time");
      history_type_combo->append("velocity vs time");
      history_type_combo->append("acceleration vs time");
      history_type_combo->append("contact force vs time"); 
      history_type_combo->append("displacement spectrum");
      history_type_combo->append("velocity spectrum");
      history_type_combo->append("acceleration spectrum");
      history_type_combo->append("contact force spectrum");
      history_type_combo->set_active(0);
      history_type_combo->signal_changed().connect(sigc::mem_fun(*this, &DynamicOutputWindow::on_history_type_changed));

      // axis labels
      label = Gtk::manage(new Gtk::Label("x-axis limits:", Gtk::ALIGN_START));
      plot_vbox->pack_start(*label, Gtk::PACK_SHRINK);
      auto axis_hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10));
      plot_vbox->pack_start(*axis_hbox, Gtk::PACK_SHRINK);

      x1_entry = Gtk::manage(new Gtk::Entry());
      x1_entry->set_width_chars(8);
      x1_entry->signal_changed().connect(sigc::mem_fun(*this, &DynamicOutputWindow::on_x1_x2_entry_changed));
      axis_hbox->pack_start(*x1_entry, Gtk::PACK_SHRINK);
      x2_entry = Gtk::manage(new Gtk::Entry());
      x2_entry->set_width_chars(8);
      x2_entry->signal_changed().connect(sigc::mem_fun(*this, &DynamicOutputWindow::on_x1_x2_entry_changed));
      axis_hbox->pack_start(*x2_entry, Gtk::PACK_SHRINK);
      auto axis_reset_button = Gtk::manage(new Gtk::Button("auto"));
      axis_reset_button->signal_clicked().connect(sigc::mem_fun(*this, &DynamicOutputWindow::reset_xaxis));
      axis_hbox->pack_end(*axis_reset_button, Gtk::PACK_SHRINK);
        
      label = Gtk::manage(new Gtk::Label("y-axis limits:", Gtk::ALIGN_START));
      plot_vbox->pack_start(*label, Gtk::PACK_SHRINK);
      axis_hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10));
      plot_vbox->pack_start(*axis_hbox, Gtk::PACK_SHRINK);

      y1_entry = Gtk::manage(new Gtk::Entry());
      y1_entry->set_width_chars(8);
      y1_entry->signal_changed().connect(sigc::mem_fun(*this, &DynamicOutputWindow::on_y1_y2_entry_changed));
      axis_hbox->pack_start(*y1_entry, Gtk::PACK_SHRINK);
      y2_entry = Gtk::manage(new Gtk::Entry());
      y2_entry->set_width_chars(8);
      y2_entry->signal_changed().connect(sigc::mem_fun(*this, &DynamicOutputWindow::on_y1_y2_entry_changed));

      axis_hbox->pack_start(*y2_entry, Gtk::PACK_SHRINK);
      axis_reset_button = Gtk::manage(new Gtk::Button("auto"));
      axis_reset_button->signal_clicked().connect(sigc::mem_fun(*this, &DynamicOutputWindow::reset_yaxis));
      axis_hbox->pack_end(*axis_reset_button, Gtk::PACK_SHRINK);

      left_vbox->pack_start(*plot_vbox, Gtk::PACK_SHRINK); 
      
      // stat vbox with options for statistics
      stat_Lvbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 10));
      left_vbox->pack_start(*stat_Lvbox, Gtk::PACK_SHRINK);
      separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
      stat_Lvbox->pack_start(*separator, Gtk::PACK_SHRINK);
      label = Gtk::manage(new Gtk::Label("span limits:", Gtk::ALIGN_START));
      stat_Lvbox->pack_start(*label, Gtk::PACK_SHRINK);
      stat_hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10));
      stat_Lvbox->pack_start(*stat_hbox, Gtk::PACK_SHRINK);
      stat_span_entry1 = Gtk::manage(new Gtk::Entry());
      stat_span_entry1->set_width_chars(8);
      stat_span_entry1->set_text("1");
      stat_hbox->pack_start(*stat_span_entry1, Gtk::PACK_SHRINK);
      stat_span_entry2 = Gtk::manage(new Gtk::Entry());
      stat_span_entry2->set_width_chars(8);
      stat_span_entry2->set_text(std::to_string(simParams.num_spans));
      stat_hbox->pack_start(*stat_span_entry2, Gtk::PACK_SHRINK);
      stat_spanReset_button = Gtk::manage(new Gtk::Button("auto"));
      stat_spanReset_button->signal_clicked().connect(sigc::mem_fun(*this, &DynamicOutputWindow::on_statSpanResetButton_pressed));
      stat_hbox->pack_end(*stat_spanReset_button, Gtk::PACK_SHRINK);

      // (iii) filter box for filter options
      filter_vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 10));
      left_vbox->pack_start(*filter_vbox, Gtk::PACK_SHRINK);
      separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL)); 
      filter_vbox->pack_start(*separator, Gtk::PACK_SHRINK); 
      check_button = Gtk::manage(new Gtk::CheckButton("Enable Filter"));
      check_button->signal_clicked().connect(sigc::mem_fun(*this, &DynamicOutputWindow::on_check_button_clicked));
      filter_vbox->pack_start(*check_button, Gtk::PACK_SHRINK); 
      filter_hbox1 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10));
      filter_vbox->pack_start(*filter_hbox1, Gtk::PACK_SHRINK);
      label = Gtk::manage(new Gtk::Label("Min frequency [Hz]:", Gtk::ALIGN_START));
      filter_hbox1->pack_start(*label, Gtk::PACK_SHRINK);
      min_frequency = Gtk::manage(new Gtk::Entry());
      min_frequency->set_width_chars(8);
      min_frequency->set_text("0");
      filter_hbox1->pack_end(*min_frequency, Gtk::PACK_SHRINK);
      filter_hbox2 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10));
      filter_vbox->pack_start(*filter_hbox2, Gtk::PACK_SHRINK);
      label = Gtk::manage(new Gtk::Label("Max frequency [Hz]:", Gtk::ALIGN_START));
      filter_hbox2->pack_start(*label, Gtk::PACK_SHRINK);
      max_frequency = Gtk::manage(new Gtk::Entry());
      max_frequency->set_width_chars(8);
      max_frequency->set_text("20");
      filter_hbox2->pack_end(*max_frequency, Gtk::PACK_SHRINK);

      // (iv) animation box for animation options
      animation_vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 10));
      left_vbox->pack_start(*animation_vbox, Gtk::PACK_SHRINK);
      separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
      animation_vbox->pack_start(*separator, Gtk::PACK_SHRINK);
      label = Gtk::manage(new Gtk::Label("span limits:", Gtk::ALIGN_START));
      animation_vbox->pack_start(*label, Gtk::PACK_SHRINK);
      axis_hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10));
      animation_vbox->pack_start(*axis_hbox, Gtk::PACK_SHRINK);
      start_span_entry = Gtk::manage(new Gtk::Entry());
      start_span_entry->set_width_chars(8);
      start_span_entry->set_text("1");
      axis_hbox->pack_start(*start_span_entry, Gtk::PACK_SHRINK);
      end_span_entry = Gtk::manage(new Gtk::Entry());
      end_span_entry->set_width_chars(8);
      end_span_entry->set_text(std::to_string(simParams.num_spans));
      axis_hbox->pack_start(*end_span_entry, Gtk::PACK_SHRINK);
      axis_reset_button = Gtk::manage(new Gtk::Button("auto"));
      axis_reset_button->signal_clicked().connect(sigc::mem_fun(*this, &DynamicOutputWindow::reset_span_limits));
      axis_hbox->pack_end(*axis_reset_button, Gtk::PACK_SHRINK);

      label = Gtk::manage(new Gtk::Label("y-axis limits:", Gtk::ALIGN_START));
      animation_vbox->pack_start(*label, Gtk::PACK_SHRINK);
      axis_hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10));
      animation_vbox->pack_start(*axis_hbox, Gtk::PACK_SHRINK);
      animation_y1 = Gtk::manage(new Gtk::Entry());
      animation_y1->set_width_chars(8);
      animation_y1->set_text("-0.1");
      animation_y1->set_sensitive(false);
      axis_hbox->pack_start(*animation_y1, Gtk::PACK_SHRINK);
      animation_y2 = Gtk::manage(new Gtk::Entry());
      animation_y2->set_width_chars(8);
      animation_y2->set_text("0.1");
      animation_y2->set_sensitive(false);
      axis_hbox->pack_start(*animation_y2, Gtk::PACK_SHRINK);
      axis_reset_button = Gtk::manage(new Gtk::Button("auto"));
      axis_reset_button->signal_clicked().connect(sigc::mem_fun(*this, &DynamicOutputWindow::reset_animation_yaxis));
      axis_hbox->pack_end(*axis_reset_button, Gtk::PACK_SHRINK);

      generate_button = Gtk::manage(new Gtk::Button("generate"));
      generate_button->signal_clicked().connect(sigc::mem_fun(*this, &DynamicOutputWindow::generate_frames));
      animation_vbox->pack_start(*generate_button, Gtk::PACK_SHRINK);
      progressBar = Gtk::manage(new Gtk::ProgressBar());
      animation_vbox->pack_start(*progressBar, Gtk::PACK_SHRINK);            

      // (v)
      // create a button to display a plot
      plot_button = Gtk::manage(new Gtk::Button("plot"));
      plot_button->signal_clicked().connect(sigc::mem_fun(*this, &DynamicOutputWindow::display_plot));
      left_vbox->pack_end(*plot_button, Gtk::PACK_SHRINK);

      // (vi)
      // create a button to print statistics
      stat_print_button = Gtk::manage(new Gtk::Button("compute"));
      stat_print_button->signal_clicked().connect(sigc::mem_fun(*this, &DynamicOutputWindow::printStatistics));
      left_vbox->pack_end(*stat_print_button, Gtk::PACK_SHRINK);

      // (vii) Create a Gtk::ToggleButton for play/pause
      playButton = Gtk::manage(new Gtk::ToggleButton("Play"));
      playButton->signal_clicked().connect(sigc::mem_fun(*this, &DynamicOutputWindow::on_toggle_button_clicked));
      left_vbox->pack_end(*playButton, Gtk::PACK_SHRINK);
      playButton->set_sensitive(false);
                  
      //-------------------------------------------------------------------------------------

      separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_VERTICAL));
      hbox->pack_start(*separator, Gtk::PACK_SHRINK);
        
      //-------------------------------------------------------------------------------------

      // To the right of the left_vbox, is either a stats frame or a plot frame

      // Create the statistics frame
      statistics_frame = Gtk::manage(new Gtk::Frame());
      statistics_frame->set_label("Statistics Output :");
      hbox->pack_start(*statistics_frame, Gtk::PACK_SHRINK);

      // statistics frame contains a stat_vbox
      // stat_vbox contains:
      // a scroll window, which contains an output text box
      // a save button to write stats to file

      // Create stat_vbox and attach it to the frame
      stat_vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 10));
      statistics_frame->add(*stat_vbox);
      
      // Create a scrolled window for the output box and pack it inside stat_vbox
      auto scrolled_window = Gtk::manage(new Gtk::ScrolledWindow());
      scrolled_window->set_min_content_height(490);
      scrolled_window->set_min_content_width(660);
      stat_vbox->pack_start(*scrolled_window, Gtk::PACK_EXPAND_WIDGET);

      // Create a text box inside the scrolled window to display stats
      stat_textbox = Gtk::manage(new Gtk::TextView());
      stat_textbox->set_editable(false);
      stat_textbox->set_cursor_visible(false);
      stat_textbox->set_wrap_mode(Gtk::WRAP_WORD_CHAR);
      scrolled_window->add(*stat_textbox);

      separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
      stat_vbox->pack_start(*separator, Gtk::PACK_SHRINK);

      // save button to write stats to file
      auto stat_hbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10));
      stat_vbox->pack_end(*stat_hbox, Gtk::PACK_SHRINK);
      stat_save_button = Gtk::manage(new Gtk::Button("save"));
      stat_save_button->signal_clicked().connect(sigc::mem_fun(*this, &DynamicOutputWindow::on_stats_save_button_clicked));
      stat_hbox->pack_end(*stat_save_button, Gtk::PACK_SHRINK);
      stat_save_button->set_sensitive(false);

      //------------------------------------------------------------------------------------
      
      // Create a frame for plotting
      plot_frame = Gtk::manage(new Gtk::Frame());
      plot_frame->set_label("Image Output");
      hbox->pack_start(*plot_frame, Gtk::PACK_SHRINK);

      // plot_frame contains a vbox
      auto plot_frame_vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 10));
      plot_frame->add(*plot_frame_vbox);
      
      // plot_frame_vbox contains
      // (i) image output
      // (ii) button to save

      // (i)
      // Create an image widget
      image = Gtk::manage(new Gtk::Image());
      image->set_size_request(660, 490);
      plot_frame_vbox->pack_start(*image, Gtk::PACK_SHRINK);

      separator = Gtk::manage(new Gtk::Separator(Gtk::ORIENTATION_HORIZONTAL));
      plot_frame_vbox->pack_start(*separator, Gtk::PACK_SHRINK);
      
      // Create a save button
      plot_save_button = Gtk::manage(new Gtk::Button("save"));
      plot_save_button->signal_clicked().connect(sigc::mem_fun(*this, &DynamicOutputWindow::saveplot));
      plot_frame_vbox->pack_end(*plot_save_button, Gtk::PACK_SHRINK);
      plot_save_button->set_sensitive(false);
                
      //------------------------------------------------------------------------------------      
      
      // Create a frame for animation
      animation_frame = Gtk::manage(new Gtk::Frame());
      animation_frame->set_label("Animation Output");
      hbox->pack_start(*animation_frame, Gtk::PACK_SHRINK);
              
      // Create a vertical box container for animation
      auto animation_frame_vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 10));
      animation_frame->add(*animation_frame_vbox);
      
      slide = Gtk::manage(new Gtk::Image());
      slide->set_size_request(660, 490);
      animation_frame_vbox->pack_start(*slide, Gtk::PACK_SHRINK);

      // Create a Gtk::Scale widget for the slider
      animation_slider.signal_value_changed().connect(sigc::mem_fun(*this, &DynamicOutputWindow::on_animation_slider_value_changed));
      animation_frame_vbox->pack_start(animation_slider, Gtk::PACK_SHRINK);
      animation_slider.set_sensitive(false);

      auto animation_hbox=Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 10));
      animation_frame_vbox->pack_end(*animation_hbox, Gtk::PACK_SHRINK);
      
      animation_save_button = Gtk::manage(new Gtk::Button("save"));
      animation_save_button->signal_clicked().connect(sigc::mem_fun(*this, &DynamicOutputWindow::on_animation_save_button_clicked));
      animation_hbox->pack_end(*animation_save_button, Gtk::PACK_SHRINK);
      animation_save_button->set_sensitive(false);

      //------------------------------------------------------------------------------------      
    
      // Show all widgets
      show_all_children();
      on_output_type_changed();
      on_history_type_changed();
      disable_axis_manipulation();
      on_check_button_clicked();
      progressBar->set_visible(false);
      
      set_resizable(false);
      resize(1,1);

      //reading speed, xinit and span from parameters file
      std::fstream jfile;
      jfile.open(simParams.params_file, std::ios::in);
      assert(jfile.good());
      auto J = nlohmann::json::parse(jfile);
      jfile.close();
      assert(J.empty()==false);
      
      {auto it = J.find(simParams.panto_tag);
      assert(it!=J.end());
      auto& j = *it;
      assert(j.empty()==false);
      assert(j.contains("xinit") && j.contains("speed"));
      j["xinit"].get_to(panto_xinit);
      j["speed"].get_to(panto_speed);}
      
      {auto it = J.find("contact wire");
      assert(it!=J.end());
      auto& j = *it;
      assert(j.empty()==false);
      assert(j.contains("span"));
      j["span"].get_to(span_length);}
      
      //std::cout << panto_xinit << "\t" << panto_speed << "\t" << span_length << std::endl;

    }

    // Handle dropdown choice of statistics or plotting output
    void DynamicOutputWindow::on_output_type_changed()
    {
      if (output_type_combo->get_active_text() == "Statistics")
	{
	  plot_frame->set_visible(false);
	  plot_vbox->set_visible(false);
	  plot_button->set_visible(false);
	  
	  statistics_frame->set_visible(true);
	  stat_Lvbox->set_visible(true);
	  stat_print_button->set_visible(true);
	  filter_vbox->set_visible(true);
	  check_button->set_active(false);
	  on_check_button_clicked();
	  
	  animation_frame->set_visible(false);
	  animation_vbox->set_visible(false);
	  playButton->set_visible(false);
	}
      else if(output_type_combo->get_active_text() == "Plots")
	{
	  plot_frame->set_visible(true);
	  plot_vbox->set_visible(true);
	  plot_button->set_visible(true);

	  statistics_frame->set_visible(false);
	  stat_Lvbox->set_visible(false);
	  stat_print_button->set_visible(false);
	  filter_vbox->set_visible(true);
	  check_button->set_active(false);
	  on_check_button_clicked();
	  
	  animation_frame->set_visible(false);
	  animation_vbox->set_visible(false);
	  playButton->set_visible(false);
	}
	else
	{
	  plot_frame->set_visible(false);
	  plot_vbox->set_visible(false);
	  plot_button->set_visible(false);
	  
	  statistics_frame->set_visible(false);
	  stat_Lvbox->set_visible(false);
	  stat_print_button->set_visible(false);
	  filter_vbox->set_visible(false);
	  
	  animation_frame->set_visible(true);
	  animation_vbox->set_visible(true);
	  playButton->set_visible(true);
	}
      resize(1,1);
      return;
    }



    void DynamicOutputWindow::printStatistics()
    {
    
      stat_textbox->get_buffer()->set_text("");
      
      // lambda to read the column of a file
      auto read_jthcolumn = [](std::string filename, const int numcols, const int j)
	{
	  std::vector<double> coln{};
	  assert(std::filesystem::exists(filename));
	  std::fstream in;
	  in.open(filename, std::ios::in);
	  assert(in.good());
	  std::vector<double> row(numcols);
	  // ignore the first comment line
	  std::string line1;
	  std::getline(in, line1);
	  while(in.good())
	    {
	      for(int k=0; k<numcols; ++k){
		    in >> row[k];
		    if(!in.good()) {
		      in.close();
	            return coln;
		    }
		}
	      coln.push_back(row[j]);
	    }
	  in.close();
	  return coln;
	};

      // format: t, F, u-ohe, v-ohe, a-ohe, u-p, v-p, a-p
      
      // read displacement, velocity, accn, contact force of the pantograph
      std::vector<double> F_contact = read_jthcolumn(simParams.outdir+"/history.dat", 8, 1);
      std::vector<double> u_panto   = read_jthcolumn(simParams.outdir+"/history.dat", 8, 5);
      std::vector<double> v_panto   = read_jthcolumn(simParams.outdir+"/history.dat", 8, 6);
      std::vector<double> a_panto   = read_jthcolumn(simParams.outdir+"/history.dat", 8, 7);
      
      std::vector<double> time_vec = read_jthcolumn(simParams.outdir+"/history.dat", 8, 0);
      //Folloing section trims output from the begining and end
      if(!is_integer(stat_span_entry1->get_text())||!is_integer(stat_span_entry2->get_text())){
          show_error_dialog("Span limits must be valid integers");
          playButton->set_sensitive(false);
          return;
      }
      int start_span=std::stoi(stat_span_entry1->get_text());
      int end_span=std::stoi(stat_span_entry2->get_text());
      if(start_span>end_span)
      {
          show_error_dialog("Start span cannot be greater than End span");
          return;
      }
      if(start_span<1) 
      {
          show_error_dialog("Start span cannot be less than 1");
          return;
      }
      if(end_span>simParams.num_spans) 
      {
          show_error_dialog("End span cannot be greater than "+std::to_string(simParams.num_spans));
          return;
      }
      int row;
      for(int i=0; i<time_vec.size(); ++i)
      {
          row=i;
          if (time_vec[i]*panto_speed+panto_xinit>=(start_span-1)*span_length) break;
      }
      time_vec.erase(time_vec.begin() + 0, time_vec.begin() + row);
      F_contact.erase(F_contact.begin() + 0, F_contact.begin() + row);
      u_panto.erase(u_panto.begin() + 0, u_panto.begin() + row);
      v_panto.erase(v_panto.begin() + 0, v_panto.begin() + row);
      a_panto.erase(a_panto.begin() + 0, a_panto.begin() + row);
      for(int i=0; i<time_vec.size(); ++i)
      {
          row=i+1;
          if (time_vec[i]*panto_speed+panto_xinit>end_span*span_length) break;
      }
      time_vec.erase(time_vec.begin() + row, time_vec.end());
      F_contact.erase(F_contact.begin() + row, F_contact.end());
      u_panto.erase(u_panto.begin() + row, u_panto.end());
      v_panto.erase(v_panto.begin() + row, v_panto.end());
      a_panto.erase(a_panto.begin() + row, a_panto.end());
      //std::cout << "Starting point : " << time_vec[0]*panto_speed+panto_xinit << "; Ending point : " << time_vec[time_vec.size()-1]*panto_speed+panto_xinit << "; Size of the vector is : "  << time_vec.size() << std::endl;
      std::stringstream ss1;
      ss1 << "Span Range : " << start_span << " to " << end_span << "\n" ;
      stat_textbox->get_buffer()->insert_at_cursor(ss1.str());
      //------------------------------------------------
            
      if (check_button->get_active())
      {   
          if(!is_real_number(max_frequency->get_text()))
          {
            show_error_dialog("Maximum frequency must be a positive number");
            return;
          }
          if(!is_real_number(min_frequency->get_text()))
          {
            show_error_dialog("Minimum frequency must be a positive number");
            return;
          }
          double fmax= std::stod(max_frequency->get_text());
          double fmin= std::stod(min_frequency->get_text());
          if (fmax<=fmin)
          {
            show_error_dialog("Max.frequency must be greater than Min. frequency");
            return;
          }
          if (fmin<0.0)
          {
            show_error_dialog("Minimum frequency must NOT be negative");
            return;
          }
       
          int NUM_ROWS = fftAmp.size();
          assert(NUM_ROWS>0);
          int NUM_COLS = fftAmp[0].size();

          for (int i=0;i<F_contact.size();i++)
          {
              F_contact[i]=0.0;
              u_panto[i]=0.0;
              v_panto[i]=0.0;
              a_panto[i]=0.0;
              double time=time_vec[i];
              for (int j=0;j<NUM_ROWS;++j)
              {
                  double frequency = fftAmp[j][0];
                  if (frequency>fmax) break;
                  if (frequency>=fmin){
                      F_contact[i] += fftAmp[j][2-1]*cos(2.0*M_PI*fftAng[j][0]*time+fftAng[j][2-1]);
                      u_panto[i] += fftAmp[j][6-1]*cos(2.0*M_PI*fftAng[j][0]*time+fftAng[j][6-1]);
                      v_panto[i] += fftAmp[j][7-1]*cos(2.0*M_PI*fftAng[j][0]*time+fftAng[j][7-1]);
                      a_panto[i] += fftAmp[j][8-1]*cos(2.0*M_PI*fftAng[j][0]*time+fftAng[j][8-1]);
                      //std::cout << j << " frequency : " << frequency << std::endl;
                  }
              }
          }
          
          std::stringstream ss;
          ss << "Filtered Statistics\nFrequency Range : " << min_frequency->get_text() << " to " << max_frequency->get_text() << " Hz\n --------------------- \n" ;
          stat_textbox->get_buffer()->insert_at_cursor(ss.str());
      }
      else {
        std::stringstream ss;
        ss << "Un-filtered Statistics\n --------------------- \n" ;
        stat_textbox->get_buffer()->insert_at_cursor(ss.str());
      }

      std::vector<std::vector<double>*> fields{&F_contact, &u_panto, &v_panto, &a_panto};
      std::vector<std::string> field_titles = {"Contact force [N]", "Displacement [m]", "Velocity [m/s]", "Acceleration [m/s^2]"};
      std::stringstream ss;
      for(int fnum=0; fnum<4; ++fnum)
	{
	  const auto& fvals = *fields[fnum];
	  double max = *std::max_element(fvals.begin(), fvals.end());
	  double min = *std::min_element(fvals.begin(), fvals.end());
	  double mean = std::accumulate(fvals.begin(), fvals.end(), 0.)/static_cast<double>(fvals.size());
	  double sq_sum = std::inner_product(fvals.begin(), fvals.end(), fvals.begin(), 0.0);
	  double stdev = std::sqrt(sq_sum/fvals.size() - mean*mean);

	  // flush to the buffer
	  ss << std::endl
	     << field_titles[fnum] << " :\n"
	     << "Mean: " << mean << std::endl
	     << "Standard Deviation: " << stdev << std::endl
	     << "Minimum: " << min << std::endl
	     << "Maximum: " << max << std::endl
	     << "Range: " << (max-min) << std::endl
	     << "---------------------" << std::endl;
	}
	
      // calculating percentage contact loss
      if( (check_button->get_active()==false) || std::stod(min_frequency->get_text())<1.0e-12){
          int count = std::count_if(F_contact.begin(), F_contact.end(), [](double num) { return num <= 0.0; });
          ss << "\nCurrent Collection Quality : " << 100.0*(1.0-static_cast<double>(count)/F_contact.size()) << " %" << std::endl;
          //std::cout << count << "/" << F_contact.size() << " = " << count/static_cast<double>(F_contact.size()) << std::endl;
      }
      
      stat_textbox->get_buffer()->insert_at_cursor(ss.str());
      
      stat_save_button->set_sensitive(true);
    }
    

    void DynamicOutputWindow::on_check_button_clicked() {
      filter_hbox1->set_visible(check_button->get_active()&&(output_type_combo->get_active_text() == "Statistics"));
      filter_hbox2->set_visible(check_button->get_active());
    }

    void DynamicOutputWindow::on_stats_save_button_clicked() {
    
      Gtk::FileChooserDialog dialog("Save As", Gtk::FILE_CHOOSER_ACTION_SAVE);
      dialog.set_transient_for(*this);
      dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
      dialog.add_button("_Save", Gtk::RESPONSE_OK);
      dialog.set_current_name("statistics.txt");
      auto filter_txt = Gtk::FileFilter::create();
      filter_txt->set_name("txt files (*.txt)");
      filter_txt->add_pattern("*.txt");
      dialog.add_filter(filter_txt);

      // Show the dialog and wait for user response
      int result = dialog.run();

      if (result == Gtk::RESPONSE_OK){
        
	// Get the buffer from the text view
	auto buffer = stat_textbox->get_buffer();

	// Get the start and end iterators of the buffer
	Gtk::TextIter start = buffer->begin();
	Gtk::TextIter end = buffer->end();

	// Get the text content of the buffer
	std::string text_content = buffer->get_text(start, end);

	// Specify the file path where you want to save the content
	std::string file_path = dialog.get_filename();

	// Open the file for writing
	std::ofstream outfile(file_path);
	assert(outfile.is_open());
	outfile << text_content;
	outfile.close();
      } 
    }
    


   
    void DynamicOutputWindow::on_history_type_changed()
    {
      if(history_type_combo->get_active_text() == "displacement vs time")
	{
	  gplot.data_filename = simParams.outdir+"/history.dat";
	  gplot.col1 = 1;
	  gplot.col2 = 6;
	  gplot.img_filename  = "image.tmp";
	  gplot.xlabel = "Time [s]"; 
	  gplot.ylabel = "Displacement [m]";
	  filter_vbox->set_visible(true);
	}
      else if(history_type_combo->get_active_text() == "velocity vs time")
	{
	  gplot.data_filename = simParams.outdir+"/history.dat";
	  gplot.col1 = 1;
	  gplot.col2 = 7;
	  gplot.img_filename  = "image.tmp";
	  gplot.xlabel = "Time [s]"; 
	  gplot.ylabel = "Velocity [m/s]"; 
	  filter_vbox->set_visible(true);
	}
      else if(history_type_combo->get_active_text() == "acceleration vs time")
	{
	  gplot.data_filename = simParams.outdir+"/history.dat";
	  gplot.col1 = 1;
	  gplot.col2 = 8;
	  gplot.img_filename  = "image.tmp";
	  gplot.xlabel = "Time [s]"; 
	  gplot.ylabel = "Acceleration [m/s^2]";  
	  filter_vbox->set_visible(true);
	}
      else if(history_type_combo->get_active_text() == "contact force vs time")
	{
	  gplot.data_filename = simParams.outdir+"/history.dat";
	  gplot.col1 = 1;
	  gplot.col2 = 2;
	  gplot.img_filename  = "image.tmp";
	  gplot.xlabel = "Time [s]"; 
	  gplot.ylabel = "Contact Force [N]";  
	  filter_vbox->set_visible(true);
	}
      else if(history_type_combo->get_active_text() == "contact force spectrum")
	{
	  gplot.data_filename = simParams.outdir+"/fft_amplitude";
	  gplot.col1 = 1;
	  gplot.col2 = 2;
	  gplot.img_filename  = "image.tmp";
	  gplot.xlabel = "Frequency [Hz]"; 
	  gplot.ylabel = "Contact force amplitude [N]";
	  check_button->set_active(false);
	  filter_vbox->set_visible(false);
	}
      else if(history_type_combo->get_active_text() == "displacement spectrum")
	{
	  gplot.data_filename = simParams.outdir+"/fft_amplitude";
	  gplot.col1 = 1;
	  gplot.col2 = 6;
	  gplot.img_filename  = "image.tmp";
	  gplot.xlabel = "Frequency [Hz]"; 
	  gplot.ylabel = "Displacement amplitude [m]";  
	  check_button->set_active(false);
	  filter_vbox->set_visible(false);
	}
      else if(history_type_combo->get_active_text() == "velocity spectrum")
	{
	  gplot.data_filename = simParams.outdir+"/fft_amplitude";
	  gplot.col1 = 1;
	  gplot.col2 = 7;
	  gplot.img_filename  = "image.tmp";
	  gplot.xlabel = "Frequency [Hz]"; 
	  gplot.ylabel = "Velocity amplitude [m/s]";  
	  check_button->set_active(false);
	  filter_vbox->set_visible(false);
	}
      else if(history_type_combo->get_active_text() == "acceleration spectrum")
	{
	  gplot.data_filename = simParams.outdir+"/fft_amplitude";
	  gplot.col1 = 1;
	  gplot.col2 = 8;
	  gplot.img_filename  = "image.tmp";
	  gplot.xlabel = "Frequency [Hz]"; 
	  gplot.ylabel = "Acceleration amplitude [m/s^2]";  
	  check_button->set_active(false);
	  filter_vbox->set_visible(false);
	}

      gplot.has_xrange = false;
      gplot.has_yrange = false;
	  
      reset_xaxis();  
      reset_yaxis();
      disable_axis_manipulation();
	on_check_button_clicked();
    }


    void DynamicOutputWindow::display_plot()
    {
      if (check_button->get_active())
      {   
          if(!is_real_number(max_frequency->get_text()))
          {
            show_error_dialog("Cut-off frequency must be a positive number");
            return;
          }
          double fmax= std::stod(max_frequency->get_text());
          if (fmax<=0.0)
          {
            show_error_dialog("Cut-off frequency must be a positive number");
            return;
          }
          
          filter_history(fmax);
          
          // run gnuplot command
          std::system(gplot.getCommandF().c_str());
      }
      else
          // run gnuplot command
          std::system(gplot.getCommand().c_str());
      
      // Load the image
      auto pixbuf = Gdk::Pixbuf::create_from_file(gplot.img_filename);
      assert(pixbuf);
      image->set(pixbuf);
      image->show();
      plot_save_button->set_sensitive(true);
      enable_axis_manipulation();
    }
    
    
    void DynamicOutputWindow::reset_xaxis()
    {
      x1_entry->set_text("");
      x2_entry->set_text("");
      gplot.has_xrange = false;
    }
    
    void DynamicOutputWindow::reset_yaxis()
    {
      y1_entry->set_text("");
      y2_entry->set_text("");
      gplot.has_yrange = false;
    }
    
    void DynamicOutputWindow::reset_span_limits()
    {
      start_span_entry->set_text("1");
      end_span_entry->set_text(std::to_string(simParams.num_spans));
    }
    void DynamicOutputWindow::reset_animation_yaxis()
    {
      animation_y1->set_text("-0.1");
      animation_y2->set_text("0.1");
    }
    void DynamicOutputWindow::on_statSpanResetButton_pressed()
    {
      stat_span_entry1->set_text("1");
      stat_span_entry2->set_text(std::to_string(simParams.num_spans));
    }

    void DynamicOutputWindow::saveplot()
          {
      Gtk::FileChooserDialog dialog("Save image", Gtk::FILE_CHOOSER_ACTION_SAVE);
      dialog.set_transient_for(*this);
      dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
      dialog.add_button("_Save", Gtk::RESPONSE_OK);
      dialog.set_current_name("plot.jpg");
      auto file_filter = Gtk::FileFilter::create();
      file_filter->set_name("jpg/csv files");
      file_filter->add_pattern("*.jpg");
      file_filter->add_pattern("*.csv");
      dialog.add_filter(file_filter);

      // Show the dialog and wait for user response
      int result = dialog.run();
      if(result!=Gtk::RESPONSE_OK)
	return;

      // get the filename
      const std::string filename = dialog.get_filename();
      const std::string extn = std::filesystem::path(filename).extension();

      if(extn==".jpg") // save an image
	{
	  auto myplot = gplot;
	  myplot.img_filename = filename;
	  if(check_button->get_active()) myplot.data_filename = myplot.data_filename+".filtered";
	  //std::cout << "myplot :" << myplot.data_filename << std::endl;
	  //std::cout << "gplot :" << gplot.data_filename << std::endl;
	  std::system(myplot.getCommand().c_str());
	}
      else if(extn==".csv")  // or the data
	{
	  // flush gplot.col1 and gplot.col2 of the input file into output
	  // eg: awk '{print $2 "," $3}' file1.dat > file2.csv
	  
	  auto myplot = gplot;
	  if(check_button->get_active()) myplot.data_filename = myplot.data_filename+".filtered";
	  std::string sys_command = "awk '{print $" + std::to_string(myplot.col1) + " \",\" $"+ std::to_string(myplot.col2) + "}' " + myplot.data_filename + " > " + filename;
	  //std::cout << "Executing command " << sys_command << std::endl;
	  std::system(sys_command.c_str());
	}
      
      return;	
    } 
    
    
    void DynamicOutputWindow::disable_axis_manipulation()
    {
      x1_entry->set_sensitive(false);
      x2_entry->set_sensitive(false);
      y1_entry->set_sensitive(false);
      y2_entry->set_sensitive(false);
    }
        
    void DynamicOutputWindow::enable_axis_manipulation()
    {
      x1_entry->set_sensitive(true);
      x2_entry->set_sensitive(true);
      y1_entry->set_sensitive(true);
      y2_entry->set_sensitive(true);
    }
    
     
    void DynamicOutputWindow::on_x1_x2_entry_changed(){

	if(!is_real_number(x1_entry->get_text())||!is_real_number(x2_entry->get_text()))
	{
	  gplot.has_xrange = false;
	  return;
	}
      
	double x1 = std::stod(x1_entry->get_text());
	double x2 = std::stod(x2_entry->get_text());

      // invalid range
      if(x1>x2)
      { 
        gplot.has_xrange = false;
	  return;
	}
      
      gplot.has_xrange = true;
      gplot.xlo = x1;
      gplot.xhi = x2;
    }

    
    void DynamicOutputWindow::on_y1_y2_entry_changed()
    {	
      if(!is_real_number(y1_entry->get_text())||!is_real_number(y2_entry->get_text()))
	{
	  gplot.has_yrange = false;
	  return;
	}

	double y1 = std::stod(y1_entry->get_text());
	double y2 = std::stod(y2_entry->get_text());

      // invalid range
      if(y1>y2)
      { 
        gplot.has_yrange = false;
	  return;
	}
      
      gplot.has_yrange = true;
      gplot.ylo = y1;
      gplot.yhi = y2;
    }

    void DynamicOutputWindow::filter_history(double fmax)
    {
       int NUM_ROWS = fftAmp.size();
       assert(NUM_ROWS>0);
       int NUM_COLS = fftAmp[0].size();

       double filteredArray[nsteps][NUM_COLS];
       for (int i=0;i<nsteps;i++)
       {
           double time=i*simParams.timestep;
           filteredArray[i][0] = time;
           
           for (int col=1;col<NUM_COLS;col++){
               filteredArray[i][col]=0.0;
               
               for (int j=0;j<NUM_ROWS;j++)
               {
                   double frequency = fftAng[j][0];
                   if (frequency>fmax) break;
                   filteredArray[i][col] += fftAmp[j][col]*cos(2.0*M_PI*frequency*time+fftAng[j][col]);
               }
           }
       }
       
       std::ofstream outputFile(simParams.outdir+"/history.dat.filtered");
       assert(outputFile.is_open());
       for (int i = 0; i < nsteps; ++i) {
           for (int j = 0; j < NUM_COLS; ++j) {
               outputFile << filteredArray[i][j] << "\t";
           }
           outputFile << std::endl;
       }
       outputFile.close();
          
    }

    void DynamicOutputWindow::generate_frames(){

        playButton->set_active(false);
        
        if(!is_integer(start_span_entry->get_text())||!is_integer(end_span_entry->get_text())){
            show_error_dialog("Span limits must be valid integers");
            playButton->set_sensitive(false);
            return;
        }
        
        int start_span=std::stoi(start_span_entry->get_text());
        int end_span=std::stoi(end_span_entry->get_text());
        
        if(start_span>end_span)
        {
            show_error_dialog("Start span cannot be greater than End span");
            playButton->set_sensitive(false);
            return;
        }
        if(start_span<1) 
        {
            show_error_dialog("Start span cannot be less than 1");
            playButton->set_sensitive(false);
            return;
        }
        if(end_span>simParams.num_spans) 
        {
            show_error_dialog("End span cannot be greater than "+std::to_string(simParams.num_spans));
            playButton->set_sensitive(false);
            return;
        }
        
        if(!is_real_number(animation_y1->get_text())||!is_real_number(animation_y2->get_text()))
	  {
            show_error_dialog("Invalid y-axis limits");
            playButton->set_sensitive(false);
            return;
	  }
	  double y1 = std::stod(animation_y1->get_text());
	  double y2 = std::stod(animation_y2->get_text());
	  if(y1>=y2)
	  {
            show_error_dialog("y-axis limits not in order");
            playButton->set_sensitive(false);
            return;
	  }
        
        xstart=span_length*(start_span-1);
        xend=span_length*(end_span);
        nstart=std::ceil((xstart-panto_xinit)/(panto_speed*simParams.timestep));
        nend=std::floor((xend-panto_xinit)/(panto_speed*simParams.timestep));
        if (nstart<0) nstart=0;
        if (nend>=nsteps) nend=nsteps-1;
        //std::cout << "start frame : " << nstart << " from x : " << xstart << std::endl;
        //std::cout << "  end frame : " << nend << " from x : " << xend << std::endl;

        progressBar->set_fraction(0.0);
        progressBar->set_visible(true);

        std::system("mkdir -p frames");
        std::system("rm -f frames/*");

        int num_threads=std::thread::hardware_concurrency();
        std::cout << "Number of threads being used : " << num_threads << std::endl;
        
        int frames_per_thread = (nend - nstart + 1) / num_threads;
    
        std::vector<std::thread> threads;  
        for (int i = 0; i < num_threads; i++) {
            int start = nstart + i * frames_per_thread;
            int end = (i == num_threads - 1) ? nend : start + frames_per_thread - 1;
            threads.emplace_back(&DynamicOutputWindow::frame_thread, this, i,start,end);
        }
        for (int i = 0; i < num_threads; i++) {
            threads[i].join();
        }

        animation_slider.set_range(nstart, nend);
        animation_slider.set_value(nstart);
        progressBar->set_fraction(0.0);
        progressBar->set_visible(false);
        playButton->set_sensitive(true);
        animation_y1->set_sensitive(true);
        animation_y2->set_sensitive(true);
        
    }
    
    /*void DynamicOutputWindow::frame_thread(int thread_id,int start, int end){
        for (int i=start; i<=end; ++i){
            //std::cout << " Thread " << num_thread << " displaying " << i << std::endl;
            if (thread_id==0) progressBar->set_fraction(static_cast<double>(i-start)/(end-start));
            
            std::stringstream confile;
            std::stringstream catfile;
            std::stringstream drofile;
            std::stringstream drofile2;                                                  // dropper slack
            std::stringstream panfile;
            std::stringstream outfile;
            std::string gnucommand;
        
            confile.str(std::string());
            confile << simParams.outdir << "/con-" << i << ".dat";
            catfile.str(std::string());
            catfile << simParams.outdir << "/../output-beam/con-" << i << ".dat";
            drofile.str(std::string());
            drofile << simParams.outdir << "/drop-" << i << ".dat";
            drofile2.str(std::string());                                                 // dropper slack
            drofile2 << simParams.outdir << "/drop-" << i << ".dats";                    // dropper slack
            panfile.str(std::string());
            if (numcols==8){
                panfile << simParams.outdir << "/panto-" << i << ".dat";
            }
            else if (numcols==4){
                panfile << simParams.outdir << "/load-" << i << ".dat";
            }
            outfile.str(std::string());
            outfile << "frames/frame" << std::setw(5) << std::setfill('0') << i << ".png";
        
            gnucommand  = "gnuplot -persist <<-EOFMarker\nset term png\nset xlabel \"";
            gnucommand += "X [m]";
            gnucommand += "\"\nset ylabel \"";
            gnucommand += "V [m/s]";
            gnucommand += "\"\n";
            gnucommand += "set xrange [";
            gnucommand += std::to_string(xstart);
            gnucommand += ":";
            gnucommand += std::to_string(xend);
            gnucommand += "]\n";
            //gnucommand += "set yrange [-0.1:0.1]";
            gnucommand += "set yrange [";
            gnucommand += animation_y1->get_text();
            gnucommand += ":";
            gnucommand += animation_y2->get_text();
            gnucommand += "]\n";
            //gnucommand += "set key off\n";
            gnucommand += "set output \"";
            gnucommand += outfile.str();
            gnucommand += "\"\n";
            gnucommand += "plot \"";
            gnucommand += confile.str();
            gnucommand += "\" u 1:3 w l lw 2 title \"cable\", \"";
            gnucommand += catfile.str();
            gnucommand += "\" u 1:3 w l lw 2 title \"beam\", ";
            gnucommand += "'-' w l lw 2 notitle \n ";
            gnucommand += std::to_string(i*0.0015*88.89+6.0);
            gnucommand += " ";
            gnucommand += animation_y1->get_text();
            gnucommand += "\n ";
            gnucommand += std::to_string(i*0.0015*88.89+6.0);
            gnucommand += " ";
            gnucommand += animation_y2->get_text();
            //gnucommand += panfile.str();
            //gnucommand += "\" u 1:2 w l lw 3";

            std::system(gnucommand.c_str());
        }        
    }*/
    
    void DynamicOutputWindow::frame_thread(int thread_id,int start, int end){
        for (int i=start; i<=end; ++i){
            //std::cout << " Thread " << num_thread << " displaying " << i << std::endl;
            if (thread_id==0) progressBar->set_fraction(static_cast<double>(i-start)/(end-start));
            
            std::stringstream confile;
            std::stringstream catfile;
            std::stringstream drofile;
            std::stringstream panfile;
            std::stringstream outfile;
            std::string gnucommand;
        
            confile.str(std::string());
            confile << simParams.outdir << "/con-" << i << ".dat";
            catfile.str(std::string());
            catfile << simParams.outdir << "/cat-" << i << ".dat";
            drofile.str(std::string());
            drofile << simParams.outdir << "/drop-" << i << ".dat";
            panfile.str(std::string());
            if (numcols==8){
                panfile << simParams.outdir << "/panto-" << i << ".dat";
            }
            else if (numcols==4){
                panfile << simParams.outdir << "/load-" << i << ".dat";
            }
            outfile.str(std::string());
            outfile << "frames/frame" << std::setw(5) << std::setfill('0') << i << ".png";
        
            gnucommand  = "gnuplot -persist <<-EOFMarker\nset term png\nset xlabel \"";
            gnucommand += "X [m]";
            gnucommand += "\"\nset ylabel \"";
            gnucommand += "Y [m]";
            gnucommand += "\"\n";
            gnucommand += "set xrange [";
            gnucommand += std::to_string(xstart);
            gnucommand += ":";
            gnucommand += std::to_string(xend);
            gnucommand += "]\n";
            //gnucommand += "set yrange [-0.1:0.1]";
            gnucommand += "set yrange [";
            gnucommand += animation_y1->get_text();
            gnucommand += ":";
            gnucommand += animation_y2->get_text();
            gnucommand += "]";
            gnucommand += "\nset key off\n";
            gnucommand += "set output \"";
            gnucommand += outfile.str();
            gnucommand += "\"\n";
            gnucommand += "plot \"";
            gnucommand += confile.str();
            gnucommand += "\" u ";
            gnucommand += "1:2";
            gnucommand += " w l lw 2, \"";
            gnucommand += catfile.str();
            gnucommand += "\" u 1:2 w l lw 2, \"";
            gnucommand += drofile.str();
            gnucommand += "\" u 1:2 w lp lw 2 linecolor rgb \"#00FFFF\", \"";
            gnucommand += panfile.str();
            gnucommand += "\" u 1:2 w lp lw 2, \"";
	      gnucommand += drofile.str();                                                                        // new slack
	      gnucommand += "\" u (column(1)*column(3)):(column(2)*column(3)) w l lw 2 linecolor rgb \"#CC3300\"";// new slack 

            std::system(gnucommand.c_str());
        }        
    }

    void DynamicOutputWindow::on_animation_slider_value_changed() {
        int frameIndex = static_cast<int>(animation_slider.get_value());
        std::stringstream outfile;
        outfile << "frames/frame" << std::setw(5) << std::setfill('0') << frameIndex << ".png";
        slide->set(Gdk::Pixbuf::create_from_file(outfile.str()));
    }
    
    void DynamicOutputWindow::on_toggle_button_clicked() {
        if (playButton->get_active()) {
            animation_slider.set_sensitive(false);
            animation_save_button->set_sensitive(false);
            playButton->set_label("Pause");
            playAnimation();
        } else {
            animation_slider.set_sensitive(true);
            playButton->set_label("Play");
            animation_save_button->set_sensitive(true);
        }
    }
    
    void DynamicOutputWindow::playAnimation() {
        int interval = 10; // Decrease this value to increse the speed
        
        while (playButton->get_active() && this->is_visible()){
            
            int i = static_cast<int>(animation_slider.get_value());
            std::this_thread::sleep_for(std::chrono::milliseconds(interval));

            // Call the existing function to update the displayed image
            on_animation_slider_value_changed();

            // Allow the main loop to handle events
            while (Gtk::Main::events_pending())
                Gtk::Main::iteration();
                
            i++;
            std::stringstream framefile;
            framefile.str(std::string());
            framefile << "frames/frame" << std::setw(5) << std::setfill('0') << i << ".png";
            if (i>nend || !std::filesystem::exists(framefile.str())) i=nstart;
            animation_slider.set_value(i);
        }
    }

    void DynamicOutputWindow::on_animation_save_button_clicked()
    {
        Gtk::FileChooserDialog dialog("Save As", Gtk::FILE_CHOOSER_ACTION_SAVE);
        dialog.set_transient_for(*this);
        dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
        dialog.add_button("_Save", Gtk::RESPONSE_OK);
        dialog.set_current_name("animation.mp4");
        auto filter_mp4 = Gtk::FileFilter::create();
        filter_mp4->set_name("mp4 files (*.mp4)");
        filter_mp4->add_pattern("*.mp4");
        dialog.add_filter(filter_mp4);

        int result = dialog.run();

        if (result == Gtk::RESPONSE_OK){
        
            std::string savecommand = "ffmpeg -framerate 30 -start_number "+std::to_string(nstart);
            savecommand += " -i frames/frame%05d.png -c:v libx264 -pix_fmt yuv420p "+dialog.get_filename();
            savecommand += " -y";
            std::system(savecommand.c_str());
        }
    }

    void DynamicOutputWindow::signalProcess() {

        int NUM_COLS=numcols;
        std::string filename=simParams.outdir+"/history.dat";
        int NUM_ROWS = 0;
        std::vector<std::vector<double>> dataArray;
        std::string line;

        std::ifstream inputFile(filename);
        if (!inputFile) {
            std::cerr << "Error opening the file." << std::endl;
            return;
        }

        while (std::getline(inputFile, line)) {
            if (line[0] != '#') {
                std::istringstream ss(line);
                double value;
                std::vector<double> rowValues;

                for (int col = 0; ss >> value && col < NUM_COLS; ++col) {
                    rowValues.push_back(value);
                }

                if (rowValues.size() == NUM_COLS) {
                    dataArray.push_back(rowValues);
                    NUM_ROWS++;
                } else {
                    std::cout << "Warning: Skipping a line due to incorrect number of columns.\n";
                }
            }
        }
        inputFile.close();
        if (NUM_ROWS % 2 == 1) NUM_ROWS=NUM_ROWS-1;

        const double DELTAT = dataArray[1][0]-dataArray[0][0]; // Assuming constant interval for simplicity

        fftAmp.resize(NUM_ROWS/2, std::vector<double>(NUM_COLS, 0.0));
        fftAng.resize(NUM_ROWS/2, std::vector<double>(NUM_COLS, 0.0));

        // Allocate memory for FFTW input and output arrays
        fftw_complex* fftInput = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * NUM_ROWS);
        fftw_complex* fftOutput = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * NUM_ROWS);

        // Create a FFTW plan
        fftw_plan plan = fftw_plan_dft_1d(NUM_ROWS, fftInput, fftOutput, FFTW_FORWARD, FFTW_ESTIMATE);

        for(int col=2; col<=NUM_COLS; col++){

            // Populate the input array with real values
            for (int i = 0; i < NUM_ROWS; ++i) {
                fftInput[i][0] = dataArray[i][col-1];
                fftInput[i][1] = 0.0; // Imaginary part is set to 0
            }

            // Execute the FFT plan
            fftw_execute(plan);

        
            for (int i = 0; i < NUM_ROWS/2; ++i) {
                
                if (col==2){
                    fftAmp[i][0]=fftAng[i][0]= i / (DELTAT * NUM_ROWS); //first column = frequency
                    fftAng[i][0]=fftAng[i][0]= i / (DELTAT * NUM_ROWS);
                    }
                    
                double amp1 = 2.0 / NUM_ROWS * sqrt(fftOutput[i][0] * fftOutput[i][0] + fftOutput[i][1] * fftOutput[i][1]);
                double amp2 = 2.0 / NUM_ROWS * sqrt(fftOutput[NUM_ROWS-i][0] * fftOutput[NUM_ROWS-i][0] + fftOutput[NUM_ROWS-i][1] * fftOutput[NUM_ROWS-i][1]);
                double ang1 = atan2(fftOutput[i][1],fftOutput[i][0]);
                double ang2 = atan2(fftOutput[NUM_ROWS-i][1],fftOutput[NUM_ROWS-i][0]);
                
                if (i==0){
                    fftAmp[0][col-1]=0.5*amp1;
                    fftAng[0][col-1]=ang1;
                }else{
                    fftAmp[i][col-1]=0.5*(amp1+amp2);
                    fftAng[i][col-1]=0.5*(ang1-ang2);
                }
            }
        }
        
        // file to be used for plotting using gnuplot
        filename = simParams.outdir+"/fft_amplitude";
        std::ofstream outputFile(filename);
        if (!outputFile.is_open()) {
            std::cerr << "Error opening output fft_amplitude file: " << filename << std::endl;
            return;
        }
        for (int row=0; row<NUM_ROWS/2; row++){
            for (int col=0; col<NUM_COLS; col++)
                outputFile << fftAmp[row][col] << "\t";
            outputFile << std::endl;
        }
        outputFile.close();

    }
    
    
    bool DynamicOutputWindow::is_real_number(std::string str) {
        std::istringstream iss(str);
        double value;
        return (iss >> value) && iss.eof();
    }
    bool DynamicOutputWindow::is_integer(std::string str) {
        std::istringstream iss(str);
        int value;
        return (iss >> value) && iss.eof();
    }
    void DynamicOutputWindow::show_error_dialog(std::string message) {
        Gtk::MessageDialog dialog(*this, message, false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK);
        dialog.set_title("Error");
        dialog.set_position(Gtk::WindowPosition::WIN_POS_CENTER_ON_PARENT);
        dialog.run();
    }
    
  }
}
