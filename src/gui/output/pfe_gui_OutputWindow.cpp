
#include <pfe_gui_OutputWindow.h>
#include <fstream>
#include <filesystem>
#include <iostream>

namespace pfe
{
  namespace gui
  {
    // Constructor
    OutputWindow::OutputWindow()
      :analysis_jsonfile(""),
       analysis_tag(""),
       all_analysis_tags({})
    {
      set_title("Post-process pantograFE results");
      set_default_size(400, 100);
      set_position(Gtk::WIN_POS_CENTER);
      move(100,100);

      // Create the outer box
      auto outer_vbox = new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 10);
      Gtk::manage(outer_vbox);
      outer_vbox->set_margin_start(10);
      outer_vbox->set_margin_end(10);
      outer_vbox->set_margin_top(10);
      outer_vbox->set_margin_bottom(10);
      add(*outer_vbox);

      // outer box contains the file load button and
      fileload_button = new Gtk::Button("Select analysis JSON file");
      Gtk::manage(fileload_button);
      outer_vbox->pack_start(*fileload_button, Gtk::PACK_SHRINK);

      // link the file load button to an event handler
      fileload_button->signal_clicked().connect(sigc::mem_fun(*this, &OutputWindow::onFileLoadButtonClicked));

      // outer box shows a label to choose the analysis tag
      analysis_choice_label = Gtk::manage(new Gtk::Label("Choose analysis tag"));
      outer_vbox->pack_start(*analysis_choice_label);
      
      // outer box contains a dropdown for choosing the analysis tag
      analysis_choice = Gtk::manage(new Gtk::ComboBoxText());
      outer_vbox->pack_start(*analysis_choice, Gtk::PACK_SHRINK);
      analysis_choice->signal_changed().connect(sigc::mem_fun(*this, &OutputWindow::onAnalysisTagClicked));
      
      // outer box contains the proceed button
      proceed_button = new Gtk::Button("Proceed");
      Gtk::manage(proceed_button);
      outer_vbox->pack_end(*proceed_button, Gtk::PACK_SHRINK);
      proceed_button->set_sensitive(false);
      proceed_button->signal_clicked().connect(sigc::mem_fun(*this, &OutputWindow::onProceedButtonClicked));

      // window properties
      show_all_children();
      analysis_choice_label->set_sensitive(false);
      analysis_choice->set_sensitive(false);
      set_resizable(false);
      resize(1,1);
    }

    
    void OutputWindow::onFileLoadButtonClicked()
    {
      // reset: disable analysis tag display and the proceed button
      analysis_choice_label->set_sensitive(false); 
      analysis_choice->set_sensitive(false);
      
      // get the user's file choice
      Gtk::FileChooserDialog dialog("Select a .json file", Gtk::FILE_CHOOSER_ACTION_OPEN);
      dialog.set_transient_for(*this);
      dialog.set_current_folder("./");
      dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
      dialog.add_button("_Open", Gtk::RESPONSE_OK);

      // restrict choices to json files
      auto filter_json = Gtk::FileFilter::create();
      filter_json->set_name("json files (*.json)");
      filter_json->add_pattern("*.json");
      dialog.add_filter(filter_json);
      auto result = dialog.run();
      if(result!=Gtk::RESPONSE_OK)
	return;
      analysis_jsonfile = dialog.get_filename(); 
      fileload_button->set_label(std::string(std::filesystem::path(analysis_jsonfile).filename()));
      
      // analysis tag
      std::fstream jfile;
      jfile.open(analysis_jsonfile, std::ios::in);
      assert(jfile.good());
      auto J = nlohmann::json::parse(jfile);
      jfile.close();
      assert(J.empty()==false);

      // read analysis tags
      all_analysis_tags.clear();
      for(auto it:J.items())
	all_analysis_tags.push_back(it.key());
      
      // activate tag choice
      analysis_choice_label->set_sensitive(true); 
      analysis_choice->set_sensitive(true);

      // display analysis choices
      analysis_choice->remove_all();
      for(auto& s:all_analysis_tags)
	analysis_choice->append(s);
	
	proceed_button->set_sensitive(false);
    }
    

    void OutputWindow::onAnalysisTagClicked()
    {
      // get the user's choice
      analysis_tag = analysis_choice->get_active_text();

      // activate the proceed button
      proceed_button->set_sensitive(true);
    }
    
    void OutputWindow::onProceedButtonClicked() {
      hide();
      return;
    }
    
    bool OutputWindow::on_delete_event(GdkEventAny* event) {
      analysis_jsonfile = "";
      hide();
      return true;
    }
    
  }
}
