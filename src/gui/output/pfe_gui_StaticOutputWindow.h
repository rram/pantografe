
#pragma once

#include <pfe_SimParams.h>
#include <pfe_json.hpp>
#include <gtkmm.h>
#include <string>

namespace pfe
{
  namespace gui
  {
    struct GnuLinePlot {
      std::string data_filename;
      std::string img_filename;
      std::string xlabel, ylabel;
      int col1, col2;
      bool has_xrange, has_yrange;
      double xlo, xhi;
      double ylo, yhi;

      std::string getCommand();
      std::string getCommandF();
    };

    
    class StaticOutputWindow: public Gtk::Window
    {
    public:
      //! Constructor
      StaticOutputWindow(std::string analysis_jsonfile, std::string analysis_tag);

      //! Destructor
      virtual ~StaticOutputWindow() = default;

      // Disable copy and assignment
      StaticOutputWindow(const StaticOutputWindow&) = delete;
      StaticOutputWindow operator=(const StaticOutputWindow&) = delete;

    private:

      void on_output_type_changed();
      void printStatistics();
      void on_stats_save_button_clicked();
      void on_history_type_changed();
      void display_plot();
      void disable_axis_manipulation();
      void enable_axis_manipulation();
      void reset_xaxis();
      void reset_yaxis();
      void saveplot();
      void on_x1_x2_entry_changed();
      void on_y1_y2_entry_changed();
      
      bool is_real_number(std::string);
      
      Static_SimParams simParams;
      Gtk::Box *left_vbox,  *plot_vbox, *stat_vbox;
      Gtk::Button *stat_save_button, *plot_save_button;
      Gtk::ComboBoxText *output_type_combo, *history_type_combo;
      Gtk::Entry *x1_entry, *x2_entry, *y1_entry, *y2_entry;
      Gtk::Button *plot_button, *stat_print_button;
      Gtk::TextView *stat_textbox;
      Gtk::Frame *statistics_frame, *plot_frame;
      Gtk::Image *image;
      GnuLinePlot gplot;
    };
    
  }
}
