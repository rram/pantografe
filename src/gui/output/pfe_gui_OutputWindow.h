
#pragma once

#include <gtkmm.h>
#include <pfe_json.hpp>

namespace pfe
{
  namespace gui
  {
    class OutputWindow: public Gtk::Window
    {
    public:
      //! Constructor
      OutputWindow();

      //! Destructor
      virtual ~OutputWindow() = default;

      // Disable copy and assignment
      OutputWindow(const OutputWindow&) = delete;
      OutputWindow operator=(const OutputWindow&) = delete;

      // access the filename read
      inline std::string GetAnalysisFile() const
      { return analysis_jsonfile; }

      // access the simulation tag
      inline std::string GetAnalysisTag() const
      { return analysis_tag; }
	
      
    private:
      std::string analysis_jsonfile;
      std::string analysis_tag;
      std::vector<std::string> all_analysis_tags;
      Gtk::Button *fileload_button, *proceed_button;
      Gtk::Label  *analysis_choice_label;
      Gtk::ComboBoxText *analysis_choice;

      void onFileLoadButtonClicked();
      void onProceedButtonClicked();
      void onAnalysisTagClicked();
      virtual bool on_delete_event(GdkEventAny* event);
    };
    
  
  }
}
  
