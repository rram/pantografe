
#pragma once

#include <pfe_gui_StaticOutputWindow.h>

namespace pfe
{
  namespace gui
  {
    class DynamicOutputWindow: public Gtk::Window
    {
    public:
      //! Constructor
      DynamicOutputWindow(std::string analysis_jsonfile, std::string analysis_tag);

      //! Destructor
      virtual ~DynamicOutputWindow() = default;

      // Disable copy and assignment
      DynamicOutputWindow(const DynamicOutputWindow&) = delete;
      DynamicOutputWindow operator=(const DynamicOutputWindow&) = delete;
      
      void signalProcess();

    protected:

      virtual void printStatistics();
      virtual void on_history_type_changed();
      void on_output_type_changed();
      void on_stats_save_button_clicked();
      void display_plot();
      void disable_axis_manipulation();
      void enable_axis_manipulation();
      void reset_xaxis();
      void reset_yaxis();
      void saveplot();
      void on_x1_x2_entry_changed();
      void on_y1_y2_entry_changed();
      void on_check_button_clicked();
      void filter_history(double);
      void generate_frames();
      void on_animation_slider_value_changed();
      void on_toggle_button_clicked();
      void playAnimation();
      void on_animation_save_button_clicked();
      void frame_thread(int,int,int);
      
      bool is_real_number(std::string);
      bool is_integer(std::string);
      void show_error_dialog(std::string);
      void reset_span_limits();
      void reset_animation_yaxis();
      void on_statSpanResetButton_pressed();
      
      Dynamic_SimParams simParams;
      Gtk::Box *left_vbox,  *plot_vbox, *stat_vbox, *filter_vbox, *filter_hbox1, *filter_hbox2, *animation_vbox, *stat_Lvbox, *stat_hbox;
      Gtk::Button *stat_save_button, *plot_save_button, *animation_save_button;
      Gtk::ComboBoxText *output_type_combo, *history_type_combo;
      Gtk::Entry *x1_entry, *x2_entry, *y1_entry, *y2_entry, *max_frequency, *min_frequency, *animation_y1, *animation_y2, *start_span_entry, *end_span_entry, *stat_span_entry1, *stat_span_entry2;
      Gtk::Button *plot_button, *stat_print_button, *generate_button, *stat_spanReset_button;
      Gtk::TextView *stat_textbox;
      Gtk::Frame *statistics_frame, *plot_frame, *animation_frame;
      Gtk::Image *image, *slide;
      GnuLinePlot gplot;
      Gtk::CheckButton *check_button;
      Gtk::Scale animation_slider;
      Gtk::ToggleButton *playButton;
      Gtk::ProgressBar *progressBar;
      int nsteps, nstart, nend;
      int numcols;
      double xstart, xend, panto_xinit, panto_speed, span_length;
      
      std::vector<std::vector<double>> fftAmp;
      std::vector<std::vector<double>> fftAng;
      
    };
    
  }
}
