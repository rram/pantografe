
#include <pfe_gui_MovingLoadOutputWindow.h>
#include <fstream>
#include <numeric>

namespace pfe
{
  namespace gui
  {
    // Constructor
    MovingLoadOutputWindow::MovingLoadOutputWindow(std::string analysis_jsonfile, std::string analysis_tag)
      :DynamicOutputWindow(analysis_jsonfile, analysis_tag)
    {
      // reset the title
      set_title("Moving Load on OHE");
      
      numcols=4; //number of columns in history file
      
      // Modify the dropdown created in the DynamicOutputWindow
      // include only disp, vel and accn. remove contact force
      history_type_combo->remove_all();
      history_type_combo->append("displacement vs time");
      history_type_combo->append("velocity vs time");
      history_type_combo->append("acceleration vs time");
      history_type_combo->append("displacement spectrum");
      history_type_combo->append("velocity spectrum");
      history_type_combo->append("acceleration spectrum");
      history_type_combo->set_active(0);
      history_type_combo->signal_changed().connect(sigc::mem_fun(*this, &MovingLoadOutputWindow::on_history_type_changed));

    }

    
    void MovingLoadOutputWindow::printStatistics()
    {
    
      stat_textbox->get_buffer()->set_text("");
      
      // lambda to read the column of a file
      auto read_jthcolumn = [](std::string filename, const int numcols, const int j)
	{
	  std::vector<double> coln{};
	  assert(std::filesystem::exists(filename));
	  std::fstream in;
	  in.open(filename, std::ios::in);
	  assert(in.good());
	  std::vector<double> row(numcols);
	  // ignore the first comment line
	  std::string line1;
	  std::getline(in, line1);
	  while(in.good())
	    {
	      for(int k=0; k<numcols; ++k)
		in >> row[k];
	      coln.push_back(row[j]);
	    }
	  in.close();
	  return coln;
	};

      // format: t, u-ohe, v-ohe, a-ohe
      
      // read displacement, velocity, accn, contact force of the pantograph
      std::vector<double> u_panto   = read_jthcolumn(simParams.outdir+"/history.dat", 4, 1);
      std::vector<double> v_panto   = read_jthcolumn(simParams.outdir+"/history.dat", 4, 2);
      std::vector<double> a_panto   = read_jthcolumn(simParams.outdir+"/history.dat", 4, 3);
      
      if (check_button->get_active())
      {   
          double fmax;
          try{fmax = std::stod(max_frequency->get_text());}
          catch (const std::invalid_argument& e)
	    {
	       return;
	    }
          assert(fmax>0.0);
          
          int NUM_COLS = numcols;
          int NUM_ROWS = 0;
          std::vector<std::vector<double>> amplitude;
          std::vector<std::vector<double>> phase;
          std::string line;

          std::ifstream ampFile(simParams.outdir+"/fft_amplitude");
          if (!ampFile) {
              std::cerr << "Error opening the file." << std::endl;
              return;
          }

          while (std::getline(ampFile, line)) {
              if (line[0] != '#') {
                  std::istringstream ss(line);
                  double value;
                  std::vector<double> rowValues;

                  for (int col = 0; ss >> value && col < NUM_COLS; ++col) {
                      rowValues.push_back(value);
                  }

                  if (rowValues.size() == NUM_COLS) {
                      amplitude.push_back(rowValues);
                      NUM_ROWS++;
                  } else {
                      std::cout << "Warning: Skipping a line due to incorrect number of columns.\n";
                  }
              }
          }
          ampFile.close();
          
          NUM_ROWS = 0;
          std::ifstream phsFile(simParams.outdir+"/fft_phase");
          if (!phsFile) {
              std::cerr << "Error opening the file." << std::endl;
              return;
          }

          while (std::getline(phsFile, line)) {
              if (line[0] != '#') {
                  std::istringstream ss(line);
                  double value;
                  std::vector<double> rowValues;

                  for (int col = 0; ss >> value && col < NUM_COLS; ++col) {
                      rowValues.push_back(value);
                  }

                  if (rowValues.size() == NUM_COLS) {
                      phase.push_back(rowValues);
                      NUM_ROWS++;
                  } else {
                      std::cout << "Warning: Skipping a line due to incorrect number of columns.\n";
                  }
              }
          }
          phsFile.close();
          
          for (int i=0;i<u_panto.size();i++)
          {
              u_panto[i]=0.0;
              v_panto[i]=0.0;
              a_panto[i]=0.0;
              double time=i*simParams.timestep;
              for (int j=0;j<NUM_ROWS;j++)
              {
                  double frequency = amplitude[j][0];
                  if (frequency>fmax) break;
                  u_panto[i] += amplitude[j][2-1]*cos(2.0*M_PI*phase[j][0]*time+phase[j][2-1]);
                  v_panto[i] += amplitude[j][3-1]*cos(2.0*M_PI*phase[j][0]*time+phase[j][3-1]);
                  a_panto[i] += amplitude[j][4-1]*cos(2.0*M_PI*phase[j][0]*time+phase[j][4-1]);
              }
          }
          
          std::stringstream ss;
          ss << "Filtered Statistics\n cut-off frequency: " << max_frequency->get_text() << " Hz\n --------------------- \n" ;
          stat_textbox->get_buffer()->insert_at_cursor(ss.str());
      }
      else {
        std::stringstream ss;
        ss << "Un-filtered Statistics\n --------------------- \n" ;
        stat_textbox->get_buffer()->insert_at_cursor(ss.str());
      }

      std::vector<std::vector<double>*> fields{&u_panto, &v_panto, &a_panto};
      std::vector<std::string> field_titles = {"Displacement [m]", "Velocity [m/s]", "Acceleration [m/s^2]"};
      std::stringstream ss;
      for(int fnum=0; fnum<3; ++fnum)
	{
	  const auto& fvals = *fields[fnum];
	  double max = *std::max_element(fvals.begin(), fvals.end());
	  double min = *std::min_element(fvals.begin(), fvals.end());
	  double mean = std::accumulate(fvals.begin(), fvals.end(), 0.)/static_cast<double>(fvals.size());
	  double sq_sum = std::inner_product(fvals.begin(), fvals.end(), fvals.begin(), 0.0);
	  double stdev = std::sqrt(sq_sum/fvals.size() - mean*mean);

	  // flush to the buffer
	  ss << std::endl
	     << field_titles[fnum] << " :\n"
	     << "Mean: " << mean << std::endl
	     << "Standard Deviation: " << stdev << std::endl
	     << "Minimum: " << min << std::endl
	     << "Maximum: " << max << std::endl
	     << "---------------------" << std::endl;
	}
      stat_textbox->get_buffer()->insert_at_cursor(ss.str());

      stat_save_button->set_sensitive(true);
    }    

   
    void MovingLoadOutputWindow::on_history_type_changed()
    {
      if(history_type_combo->get_active_text() == "displacement vs time")
	{
	  gplot.data_filename = simParams.outdir+"/history.dat";
	  gplot.col1 = 1;
	  gplot.col2 = 2;
	  gplot.img_filename  = "image.tmp";
	  gplot.xlabel = "Time [s]"; 
	  gplot.ylabel = "Displacement [m]";  
	  filter_vbox->set_visible(true);
	}
      else if(history_type_combo->get_active_text() == "velocity vs time")
	{
	  gplot.data_filename = simParams.outdir+"/history.dat";
	  gplot.col1 = 1;
	  gplot.col2 = 3;
	  gplot.img_filename  = "image.tmp";
	  gplot.xlabel = "Time [s]"; 
	  gplot.ylabel = "Velocity [m/s]";  
	  filter_vbox->set_visible(true);  
	}
      else if(history_type_combo->get_active_text() == "acceleration vs time")
	{
	  gplot.data_filename = simParams.outdir+"/history.dat";
	  gplot.col1 = 1;
	  gplot.col2 = 4;
	  gplot.img_filename  = "image.tmp";
	  gplot.xlabel = "Time [s]"; 
	  gplot.ylabel = "Acceleration [m/s^2]";  
	  filter_vbox->set_visible(true);  
	}
      else if(history_type_combo->get_active_text() == "displacement spectrum")
	{
	  gplot.data_filename = simParams.outdir+"/fft_amplitude";
	  gplot.col1 = 1;
	  gplot.col2 = 2;
	  gplot.img_filename  = "image.tmp";
	  gplot.xlabel = "Frequency [Hz]"; 
	  gplot.ylabel = "Displacement amplitude [m]";  
	  check_button->set_active(false);
	  filter_vbox->set_visible(false);
	}
      else if(history_type_combo->get_active_text() == "velocity spectrum")
	{
	  gplot.data_filename = simParams.outdir+"/fft_amplitude";
	  gplot.col1 = 1;
	  gplot.col2 = 3;
	  gplot.img_filename  = "image.tmp";
	  gplot.xlabel = "Frequency [Hz]"; 
	  gplot.ylabel = "Velocity amplitude [m/s]";  
	  check_button->set_active(false);
	  filter_vbox->set_visible(false);
	}
      else if(history_type_combo->get_active_text() == "acceleration spectrum")
	{
	  gplot.data_filename = simParams.outdir+"/fft_amplitude";
	  gplot.col1 = 1;
	  gplot.col2 = 4;
	  gplot.img_filename  = "image.tmp";
	  gplot.xlabel = "Frequency [Hz]"; 
	  gplot.ylabel = "Acceleration amplitude [m/s^2]";  
	  check_button->set_active(false);
	  filter_vbox->set_visible(false);
	}

      gplot.has_xrange = false;
      gplot.has_yrange = false;
	  
      reset_xaxis();  
      reset_yaxis();
      disable_axis_manipulation();
    }
  }
}
