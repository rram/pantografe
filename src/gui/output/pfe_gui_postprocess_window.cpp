
#include <pfe_gui_OutputWindow.h>
#include <pfe_gui_StaticOutputWindow.h>
#include <pfe_gui_DynamicOutputWindow.h>
#include <pfe_gui_MovingLoadOutputWindow.h>
#include <pfe_gui_postprocess_window.h>

namespace pfe
{
  namespace gui
  {
    void run_postprocess_window()
    {
      // get the analysis file and tag
      std::string analysis_file, analysis_tag;
      {
	auto gtk_app = Gtk::Application::create("com.example.outputWindow");
	pfe::gui::OutputWindow window;
	gtk_app->run(window);
	analysis_file = window.GetAnalysisFile();
	analysis_tag  = window.GetAnalysisTag();
      }

      // read the simulation type
      std::string sim_type;
      std::fstream jfile;
      jfile.open(analysis_file, std::ios::in);
      assert(jfile.good());
      auto J = nlohmann::json::parse(jfile);
      jfile.close();
      assert(J.contains(analysis_tag)==true);
      auto& j = J[analysis_tag];
      assert(j.contains("simulation type"));
      j["simulation type"].get_to(sim_type);
      
      std::string output_directory;
      assert(j.contains("output directory"));
      j["output directory"].get_to(output_directory);

      // read the pantograph type if available
      int panto_type;
      if(j.contains("pantograph type"))
	{
	  j["pantograph type"].get_to(panto_type);
	  assert(panto_type==0 || panto_type==1 || panto_type==3);
	}
      
      auto gtk_app = Gtk::Application::create("com.example.outputWindow");
      
      if(sim_type=="linear static" || sim_type=="nonlinear static")
	{
	  pfe::gui::StaticOutputWindow window(analysis_file, analysis_tag);
	  gtk_app->run(window);
	}
      else if(panto_type==0)
      {
	  pfe::gui::MovingLoadOutputWindow window(analysis_file, analysis_tag);
	  window.signalProcess();
	  gtk_app->run(window);
	}
      else
	{
	  pfe::gui::DynamicOutputWindow window(analysis_file, analysis_tag);
	  window.signalProcess();
	  gtk_app->run(window);
	}
	
	//clean up temporary files
	std::string ss;
	ss = "rm -rf frames image.tmp ";
	ss = ss + output_directory;
	ss = ss + "/history.dat.filtered ";
	ss = ss + output_directory;
	ss = ss + "/fft_amplitude";
	//std::cout << ss << std::endl;
	std::system(ss.c_str());
	
      // done
      return;
    }

  }
}
