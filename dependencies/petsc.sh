#! /bin/bash

#usage: ./petsc_script.sh "Destination/Directory"

# exit on the first error
set -e

# destination directory is required
if [ $# -eq 0 ]; then
    >&2 echo "Requires destination directory as argument"
    exit 0
fi

# check that destination directory exists
if [ ! -d "$1" ]; then
    echo -e "Destination directory does not exist"
    exit 0
fi

# clone into the destination directory
petsc_dir="$1/petsc"
echo -e "Cloning petsc to "$petsc_dir
git clone -b release https://gitlab.com/petsc/petsc.git $petsc_dir


# configure petsc
echo -e "Configuring petsc.. may take a few minutes"
export PETSC_DIR=$petsc_dir
export PETSC_ARCH="arch-darwin-c-opt"
cd $PETSC_DIR
./config/configure.py --with-debugging=0 --with-x=0 --with-mpi=0 --with-openmp=0 --with-shared-libraries=0 --with-single-library=1 CXXOPTFLAGS="-O3 -std=c++11" COPTFLAGS=-O3 --with-fc=0 --download-f2cblaslapack

# compile libraries
echo -e "Compiling petsc.. will take a few minutes"
make -j all

# run checks
echo -e "Running checks..."
make -j check

# set environment variables
echo -e "Appending environment variables PETSC_DIR and PETSC_ARCH to terminal startup script"
echo "export PETSC_DIR=$PETSC_DIR" >> ~/.zshrc
echo "export PETSC_ARCH=$PETSC_ARCH" >> ~/.zshrc
echo 'export PKG_CONFIG_PATH=${PKG_CONFIG_PATH}:${PETSC_DIR}/${PETSC_ARCH}/lib/pkgconfig/' >> ~/.zshrc
source ~/.zshrc

# done
echo -e "Finished setting up petsc"

