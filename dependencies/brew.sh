#! /bin/bash

# Check if Homebrew is installed
if command -v brew &> /dev/null ; then
    echo "Homebrew found"
else
    echo "Homebrew is not installed. Please install and rerun this script"
    exit 0
fi


# function to check if a package exists. If not, install it using homebrew
function check_and_install() {
    echo ""
    echo -n "Checking for $1"
    which -s $1
    if [[ $? != 0 ]] ; then
	echo "...not found. Installing with homebrew"
	brew install $1
    else
    echo "...found"
    fi
}

# git
check_and_install "git"

# cmake
check_and_install "cmake"

# pkg-config
check_and_install "pkg-config"

# gnuplot
check_and_install "gnuplot"

# gtkmm
check_and_install "gtkmm3"

# glib
check_and_install "glib"

#fftw
check_and_install "fftw"

# doxygen
#check_and_install "doxygen"

# graphviz for dot, required by doxygen
#check_and_install "graphviz"
