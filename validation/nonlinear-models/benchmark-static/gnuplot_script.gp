set term jpeg
set key center top box
set ylabel "Vertical Displacenment [m]"
set xlabel "Distance [m]"
set output "benchmark_static.jpg"
set xrange [0:55]
plot "output-s1/contact.dat" u 1:2 w l title "PantograFE", \
"../../benchmark-static/benchmark-data/abaqus_static.rpt" u 1:2 w l title "ABAQUS", \
"../../benchmark-static/benchmark-data/comsol_nldrp.txt" u 1:2 w l title "COMSOL"

set term jpeg
set key center top box
set ylabel "Stiffness [N/m]"
set xlabel "Distance [m]"
set xrange [0:55]
set output "benchmark_NL_stiff_100.jpg"
plot "../../benchmark-static/benchmark-data/spops100.csv" u 1:2 w lp title "SPOPS", \
"../../benchmark-static/benchmark-data/pcrun100.csv" u 1:2 w lp title "PCRUN", \
"output-s1/stiffness.dat" u 1:2 w l lw 2 title "PantograFE", \
"../../benchmark-static/benchmark-data/abaqus_1sp100.txt" u 1:2 w l lw 2 title "ABAQUS", \
"../../benchmark-static/benchmark-data/comsol_nldrp.txt" u 1:3 w l lw 2 title "COMSOL"


set term jpeg
set key center top box
set ylabel "Stiffness [N/m]"
set xlabel "Distance [m]"
set xrange [0:55]
set output "benchmark_NL_stiff_200.jpg"
plot "../../benchmark-static/benchmark-data/spops200.csv" u 1:2 w lp title "SPOPS", \
"../../benchmark-static/benchmark-data/pcrun200.csv" u 1:2 w lp title "PCRUN", \
"output-s2/stiffness.dat" u 1:2 w l lw 2 title "PantograFE", \
"../../benchmark-static/benchmark-data/abaqus_1sp200.txt" u 1:2 w l lw 2 title "ABAQUS", \
"../../benchmark-static/benchmark-data/comsol_nldrp.txt" u 1:4 w l lw 2 title "COMSOL"
