
set term jpeg
set xlabel "Distance [s]"
set ylabel "Displacement [m]"
set xrange [0:55]
set yrange [*:*]
set key box top center
set output "1span_load_uplift.jpg"
plot "output/contact-0.dat" u 1:2 w l lw 2 title "PantograFE", "../../moving-load/data/abaqus_uplift.rpt" u 1:2 w l lw 2 title "ABAQUS", "../../moving-load/data/comsol_uplift.txt" u 1:2 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Displacement [m]"
set xrange [0:0.5512]
set autoscale yfix
set key box top left
set output "1span_load_u2.jpg"
plot "output/history.dat" u 1:2 w l lw 2 title "PantograFE", "../../moving-load/data/comsol_history.txt" u 1:2 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Velocity [m/s]"
set xrange [0:0.5512]
set autoscale yfix
set key box bottom left
set output "1span_load_v2.jpg"
plot "output/history.dat" u 1:3 w l lw 2 title "PantograFE", "../../moving-load/data/comsol_history.txt" u 1:3 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Acceleration [m/s^2]"
set xrange [0:0.5512]
set yrange [*:*]
set key box top right
set output "1span_load_a2.jpg"
plot "output/history.dat" u 1:4 w l lw 2 title "PantograFE", "../../moving-load/data/comsol_history.txt" u 1:4 w l lw 2 title "COMSOL"

