
/** \file nonlinear_moving_load.cpp
 * \brief Validation tests for class pfe::nonlinear::Model_MovingLoad<MultiSpan_OHE>
 * \author Ramsharan Rangarajan, Suman Dutta
 * \validation \ref moving_load_1nl : Validation of moving load on a single span OHE 
 */

#include <pfe_nonlinear_Model_MovingLoad.h>
#include <pfe_nonlinear_Newmark.h>
#include <pfe_nonlinear_GenAlpha.h>
#include <pfe_json.hpp>
#include <pfe_CLI11.hpp>
#include <iostream>
#include <fstream>
#include <filesystem>

// simulation parameter
void read_simulation_parameters(const std::string sim_file, const std::string sim_id,
				std::string& params_file,
				std::string& panto_tag,
				int& num_spans, int& nelm,
				std::string& time_integrator_type,
				double& time_integrator_param,
				double& timestep,
				int& num_time_steps,
				std::string& out_directory);


int main(int argc, char** argv)
{
  // start clock
  auto start = std::chrono::high_resolution_clock::now();
  
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRQ(ierr);

  // read commandline options
  std::string sim_file;
  std::string sim_id;
  CLI::App app;
  app.add_option("-i", sim_file, "json file with simulation parameters")->required()->check(CLI::ExistingFile);
  app.add_option("-s", sim_id, "simulation is in input file")->required();
  CLI11_PARSE(app, argc, argv);

  // read simulation parameters
  std::string params_file;            // json file with ohe+pantograph data
  std::string outdir;                 // output directory
  int         num_spans;              // number of spans
  int         nelm;                   // number of elements between successive droppers
  std::string panto_tag;              // pantograph tag in the parameter file
  std::string time_integrator_type;   // time integrator type
  double      rho;                    // parameter to be used with the time integrator
  double      timestep;               // time step
  int         num_time_steps;         // number of time steps
  read_simulation_parameters(sim_file, sim_id,
			     params_file,
			     panto_tag, num_spans, nelm,
			     time_integrator_type, rho,
			     timestep, num_time_steps, outdir);
  outdir += "/";
  
  // ohe + pantograph parameters
  std::fstream jfile;
  jfile.open(params_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());

  pfe::OHESpan_ParameterPack ohe_params;
  jfile >> ohe_params;

  pfe::Pantograph_0_Params panto_0_params{.tag=panto_tag};
  jfile >> panto_0_params;
  
  // read nonlinear parameters
  double dropper_slack_factor;
  jfile.seekg(0);
  auto J = nlohmann::json::parse(jfile);
  jfile.close();
  {  
    auto it = J.find("nonlinear parameters");
    assert(it!=J.end());
    (*it)["dropper slack factor"].get_to(dropper_slack_factor);
  }

  // aliases
  using OHE_TYPE = pfe::nonlinear::MultiSpan_OHE;
  using MODEL_TYPE = pfe::nonlinear::Model_MovingLoad<OHE_TYPE>;

  // Create OHE
  OHE_TYPE ohe(0.0, ohe_params, num_spans, nelm, dropper_slack_factor);

  // time integrator
  const int nTotalDof = ohe.GetTotalNumDof();
  std::unique_ptr<pfe::nonlinear::TimeIntegrator<MODEL_TYPE>> TI;
  if(time_integrator_type=="newmark")
    TI = std::make_unique<pfe::nonlinear::Newmark<MODEL_TYPE>>(nTotalDof, timestep);
  else
    TI = std::make_unique<pfe::nonlinear::GenAlpha<MODEL_TYPE>>(rho, nTotalDof, timestep);

  // Create an instance of the model
  MODEL_TYPE model(ohe, panto_0_params, *TI);
    
  // Static solution
  model.StaticInitialize();
  model.Write({outdir+"catenary-0.dat",outdir+"contact-0.dat",outdir+"dropper-0.dat"}, outdir+"load-0.dat");

  std::fstream hfile;
  hfile.open(outdir+"history.dat", std::ios::out);
  assert(hfile.good());
  hfile << "# time \t u \t v \t a "<< std::endl;
  std::array<double,3> ohe_uva   = model.GetLoadPointKinematics();
  hfile << 0.0 << "\t" << ohe_uva[0] << "\t" << ohe_uva[1] << "\t" << ohe_uva[2] << "\t" << std::endl;
    	
  // Moving the load
  for(int tstep=0; tstep<num_time_steps; ++tstep)
    {
      std::cout << "Time step " << tstep << std::endl;
      model.Advance();

      /*std::string tstr = std::to_string(tstep+1)+".dat";
      model.Write({"catenary-"+tstr, "contact-"+tstr, "dropper-"+tstr}, "load-"+tstr);*/
      
      ohe_uva   = model.GetLoadPointKinematics();
      hfile << model.GetTime() << "\t" << ohe_uva[0] << "\t" << ohe_uva[1] << "\t" << ohe_uva[2] << "\t" << std::endl;
    }
  hfile.close();

  // end clock
  auto elapsed = std::chrono::high_resolution_clock::now() - start;
  long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
  int seconds = floor(microseconds*1.0e-6);
  int minutes = floor(seconds/60.0);
  seconds = seconds % 60;
  //std::cout << "Time taken : " << microseconds*1.0e-6 << "s" << std::endl;
  std::cout << "Time taken : " << minutes << "m " << seconds << "s" << std::endl;
  
  // Clean up
  TI->Destroy();
  ohe.Destroy();
  model.Destroy();
  PetscFinalize();
}


// simulation parameters
void read_simulation_parameters(const std::string sim_file, const std::string sim_id,
				std::string& params_file,
				std::string& panto_tag, int& num_spans, int& nelm,
				std::string& time_integrator_type, double& time_integrator_param,
				double& timestep, int& num_time_steps,
				std::string& out_directory)
{
  assert(std::filesystem::path(sim_file).extension()==".json");
  std::fstream jfile;
  jfile.open(sim_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());

  // get the data
  auto J = nlohmann::json::parse(jfile);
  jfile.close();

  // read options
  auto it = J.find(sim_id);
  assert(it!=J.end());
  auto& j = *it;
  assert(j.contains("parameters file")    && "Input file missing params file tag");
  assert(j.contains("num spans")          && "Input file missing num spans");
  assert(j.contains("pantograph tag")     && "Input file missing pantograph tag");
  assert(j.contains("num elements")       && "Input file missing num elements tag");
  assert(j.contains("time integrator")    && "Input file missing time integrator tag");
  assert(j.contains("time step")          && "Input file missing time step tag");
  assert(j.contains("num time steps")     && "Input file missing num time steps tag");
  assert(j.contains("output directory")   && "Input file missing output directory tag");
  
  j["parameters file"].get_to(params_file);
  j["num spans"].get_to(num_spans);
  j["pantograph tag"].get_to(panto_tag);
  j["num elements"].get_to(nelm);
  j["time integrator"].get_to(time_integrator_type);
  j["time step"].get_to(timestep); 
  j["num time steps"].get_to(num_time_steps);
  j["output directory"].get_to(out_directory);

  // list of tags
  std::set<std::string> tags{"parameters file", "num spans", "pantograph tag",
      "num elements", "time integrator", "time step", "num time steps", "output directory", "_comment"};
  
  // time integrator parameter
  assert(time_integrator_type=="newmark" || time_integrator_type=="generalized alpha");
  if(time_integrator_type=="generalized alpha")
    {
      assert(j.contains("spectral radius") && "Input file missing spectral radius tag");
      j["spectral radius"].get_to(time_integrator_param);
      tags.insert("spectral radius");
    }
  
  // checks on inputs
  assert(params_file.empty()==false && std::filesystem::exists(params_file));
  assert(nelm>0 && timestep>0 && num_time_steps>0 && num_spans>=1);
  
  // create the output directory if it does not exist, erase .dat files it does
  if(std::filesystem::exists(out_directory)==true)
    {
      std::cout << "Output directory " << out_directory << " exists. Erasing dat files" << std::endl;
      auto it_dir = std::filesystem::directory_iterator(out_directory);
      for(auto& it:it_dir)
	if(it.path().extension()==".dat")
	  {
	    auto flag = std::filesystem::remove(it.path());
	    assert(flag==true && "Could not erase file");
	  }
    }
  else
    {
      std::cout << "Creating output directory \"" << out_directory << "\"" << std::endl;
      auto flag  = std::filesystem::create_directory(out_directory);
      assert(flag==true && "Could not create output directory");
    }

  // Check consistency of tags
  for(auto jt:j.items())
    if(tags.find(jt.key())==tags.end())
      {
	std::cout << "Unexpected tag " << jt.key() << " found in input file " << std::endl;
	assert(false);
      }

  // done
  return;
}
