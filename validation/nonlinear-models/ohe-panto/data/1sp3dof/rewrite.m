clc 
clear all
close all

% filename='abaqus_u2.rpt';
% %filename='abaqus_v2.rpt';
% %filename='abaqus_a2.rpt';
% %input('Enter the path to file: ');
% flag=1;%input('Is this file a history file: ');

filename='abaqus_fc.txt';
flag=0;

A1=readmatrix(filename,"FileType","text");

if (flag==1) 
    A1(:,1)=A1(:,1)-1;
end
A1=A1(A1(:,1)>=0,:);

fileID = fopen(filename,'w');
fmt=' ';
for i=1:size(A1,2)
    fmt=strcat([fmt,' %12.5E ']);
end
fmt=strcat([fmt,' \n']);
fprintf(fileID,fmt,A1');
fclose(fileID);
disp('file re-written successfully')
