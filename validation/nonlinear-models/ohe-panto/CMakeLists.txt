
# Copy json files
file(GLOB JSON_FILES "${CMAKE_CURRENT_SOURCE_DIR}/*.json")
file(COPY ${JSON_FILES} DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
file(COPY "gnuplot_script.gp" DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

foreach(TARGET nonlinear_ohe_panto)

  # add executable
  add_executable(${TARGET} ${TARGET}.cpp)

  # include headers
  target_include_directories(${TARGET} PUBLIC ${PFE_INCLUDE_DIRS})

  # link directories
  target_link_directories(${TARGET} PUBLIC ${PFE_LIBRARY_DIRS})
  target_link_libraries(${TARGET} PUBLIC ${PFE_LIBRARIES})
  
  # choose appropriate flags
  target_compile_features(${TARGET} PUBLIC ${COMPILE_FEATURES})

endforeach()
