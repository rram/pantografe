
set term jpeg
set xlabel "Time [s]"
set ylabel "Displacement [m]"
set xrange [0:0.5512]
set autoscale yfix
set key box top left
set output "pen_1span_1dof_u2.jpg"
plot "output11/history.dat" u 1:6 w l lw 2 title "PantograFE", "../../ohe-panto/data/1sp1dof/abaqus_u2.rpt" u 1:2 w l lw 2 title "ABAQUS", "../../ohe-panto/data/1sp1dof/comsol_history.txt" u 1:6 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Velocity [m/s]"
set xrange [0:0.5512]
set autoscale yfix
set key box bottom left
set output "pen_1span_1dof_v2.jpg"
plot "output11/history.dat" u 1:7 w l lw 2 title "PantograFE", "../../ohe-panto/data/1sp1dof/abaqus_v2.rpt" u 1:2 w l lw 2 title "ABAQUS", "../../ohe-panto/data/1sp1dof/comsol_history.txt" u 1:7 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Acceleration [m/s^2]"
set xrange [0:0.5512]
set yrange [-35:30]
set key box bottom left
set output "pen_1span_1dof_a2.jpg"
plot "output11/history.dat" u 1:8 w l lw 2 title "PantograFE", "../../ohe-panto/data/1sp1dof/abaqus_a2.rpt" u 1:2 w l lw 2 title "ABAQUS", "../../ohe-panto/data/1sp1dof/comsol_history.txt" u 1:8 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Contact force [N]"
set xrange [0:0.5512]
set yrange [-10:*]
set key box bottom left
set output "pen_1span_1dof_fc.jpg"
plot "output11/history.dat" u 1:2 w l lw 2 title "PantograFE", "../../ohe-panto/data/1sp1dof/abaqus_fc.txt" u 1:2 w l lw 2 title "ABAQUS", "../../ohe-panto/data/1sp1dof/comsol_history.txt" u 1:2 w l lw 2 title "COMSOL"



set term jpeg
set xlabel "Time [s]"
set ylabel "Displacement [m]"
set xrange [0:0.5512]
set yrange [*:*]
set key box top left
set output "pen_1span_3dof_u2.jpg"
plot "output13/history.dat" u 1:6 w l lw 2 title "PantograFE", "../../ohe-panto/data/1sp3dof/abaqus_u2.rpt" u 1:2 w l lw 2 title "ABAQUS", "../../ohe-panto/data/1sp3dof/comsol_history.txt" u 1:6 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Velocity [m/s]"
set xrange [0:0.5512]
set autoscale yfix
set key box bottom left
set output "pen_1span_3dof_v2.jpg"
plot "output13/history.dat" u 1:7 w l lw 2 title "PantograFE", "../../ohe-panto/data/1sp3dof/abaqus_v2.rpt" u 1:2 w l lw 2 title "ABAQUS", "../../ohe-panto/data/1sp3dof/comsol_history.txt" u 1:7 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Acceleration [m/s^2]"
set xrange [0:0.5512]
set yrange [-35:30]
set key box bottom left
set output "pen_1span_3dof_a2.jpg"
plot "output13/history.dat" u 1:8 w l lw 2 title "PantograFE", "../../ohe-panto/data/1sp3dof/abaqus_a2.rpt" u 1:2 w l lw 2 title "ABAQUS", "../../ohe-panto/data/1sp3dof/comsol_history.txt" u 1:8 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Contact force [N]"
set xrange [0:0.5512]
set yrange [-10:*]
set key box bottom left
set output "pen_1span_3dof_fc.jpg"
plot "output13/history.dat" u 1:2 w l lw 2 title "PantograFE", "../../ohe-panto/data/1sp3dof/abaqus_fc.txt" u 1:2 w l lw 2 title "ABAQUS", "../../ohe-panto/data/1sp3dof/comsol_history.txt" u 1:2 w l lw 2 title "COMSOL"




set term jpeg
set xlabel "Time [s]"
set ylabel "Displacement [m]"
set xrange [0:1.788]
set yrange [*:*]
set key box top left
set output "pen_3span_3dof_u2.jpg"
plot "output33/history.dat" u 1:6 w l lw 2 title "PantograFE", "../../ohe-panto/data/3sp3dof/abaqus_u2.rpt" u 1:2 w l lw 2 title "ABAQUS", "../../ohe-panto/data/3sp3dof/comsol_history.txt" u 1:6 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Velocity [m/s]"
set xrange [0:1.788]
set autoscale yfix
set key box top left
set output "pen_3span_3dof_v2.jpg"
plot "output33/history.dat" u 1:7 w l lw 2 title "PantograFE", "../../ohe-panto/data/3sp3dof/abaqus_v2.rpt" u 1:2 w l lw 2 title "ABAQUS", "../../ohe-panto/data/3sp3dof/comsol_history.txt" u 1:7 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Acceleration [m/s^2]"
set xrange [0:1.788]
set yrange [-35:30]
set key box bottom left
set output "pen_3span_3dof_a2.jpg"
plot "output33/history.dat" u 1:8 w l lw 2 title "PantograFE", "../../ohe-panto/data/3sp3dof/abaqus_a2.rpt" u 1:2 w l lw 2 title "ABAQUS", "../../ohe-panto/data/3sp3dof/comsol_history.txt" u 1:8 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Contact force [N]"
set xrange [0:1.788]
set yrange [-10:*]
set key box top left
set output "pen_3span_3dof_fc.jpg"
plot "output33/history.dat" u 1:2 w l lw 2 title "PantograFE", "../../ohe-panto/data/3sp3dof/abaqus_fc.txt" u 1:2 w l lw 2 title "ABAQUS", "../../ohe-panto/data/3sp3dof/comsol_history.txt" u 1:2 w l lw 2 title "COMSOL"




set term jpeg
set xlabel "Time [s]"
set ylabel "Displacement [m]"
set xrange [0:0.5512]
set yrange [*:*]
set key box bottom left
set output "int_1span_3dof_u2.jpg"
plot "output13i/history.dat" u 1:6 w l lw 2 title "PantograFE", "../../ohe-panto/data/1sp3dofi/comsol_history.txt" u 1:6 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Velocity [m/s]"
set xrange [0:0.5512]
set autoscale yfix
set key box top left
set output "int_1span_3dof_v2.jpg"
plot "output13i/history.dat" u 1:7 w l lw 2 title "PantograFE", "../../ohe-panto/data/1sp3dofi/comsol_history.txt" u 1:7 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Acceleration [m/s^2]"
set xrange [0:0.5512]
set yrange [-35:30]
set key box bottom left
set output "int_1span_3dof_a2.jpg"
plot "output13i/history.dat" u 1:8 w l lw 2 title "PantograFE", "../../ohe-panto/data/1sp3dofi/comsol_history.txt" u 1:8 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Contact force [N]"
set xrange [0:0.5512]
set yrange [-10:*]
set key box top left
set output "int_1span_3dof_fc.jpg"
plot "output13i/history.dat" u 1:2 w l lw 2 title "PantograFE", "../../ohe-panto/data/1sp3dofi/comsol_history.txt" u 1:2 w l lw 2 title "COMSOL"




set term jpeg
set xlabel "Time [s]"
set ylabel "Displacement [m]"
set xrange [0:1.788]
set yrange [*:*]
set key box top left
set output "int_3span_3dof_u2.jpg"
plot "output33i/history.dat" u 1:6 w l lw 2 title "PantograFE", "../../ohe-panto/data/3sp3dofi/abaqus_u2.rpt" u 1:2 w l lw 2 title "ABAQUS", "../../ohe-panto/data/3sp3dofi/comsol_history.txt" u 1:6 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Velocity [m/s]"
set xrange [0:1.788]
set autoscale yfix
set key box top left
set output "int_3span_3dof_v2.jpg"
plot "output33i/history.dat" u 1:7 w l lw 2 title "PantograFE", "../../ohe-panto/data/3sp3dofi/abaqus_v2.rpt" u 1:2 w l lw 2 title "ABAQUS", "../../ohe-panto/data/3sp3dofi/comsol_history.txt" u 1:7 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Acceleration [m/s^2]"
set xrange [0:1.788]
set yrange [*:*]
set key box bottom left
set output "int_3span_3dof_a2.jpg"
plot "output33i/history.dat" u 1:8 w l lw 2 title "PantograFE", "../../ohe-panto/data/3sp3dofi/abaqus_a2.rpt" u 1:2 w l lw 2 title "ABAQUS", "../../ohe-panto/data/3sp3dofi/comsol_history.txt" u 1:8 w l lw 2 title "COMSOL"

set term jpeg
set xlabel "Time [s]"
set ylabel "Contact force [N]"
set xrange [0:1.788]
set yrange [-10:*]
set key box top left
set output "int_3span_3dof_fc.jpg"
plot "output33i/history.dat" u 1:2 w l lw 2 title "PantograFE", "../../ohe-panto/data/3sp3dofi/abaqus_fc.txt" u 1:2 w l lw 2 title "ABAQUS", "../../ohe-panto/data/3sp3dofi/comsol_history.txt" u 1:2 w l lw 2 title "COMSOL"


