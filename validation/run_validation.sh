#! /bin/bash

begc='\033[0;35m'
endc='\033[0m'
set -e

echo -e "\n${begc}Proceeding to validate the linear models${endc}"
cd linear-models
mkdir -p build
cd build
cmake ../
make -j
mkdir -p validation_images
rm -f validation_images/*

# bowe-mullarkey
echo -e "Running validation case: ${begc}bowe-mullarkey${endc}"
cd bowe-mullarkey
./bowe_mullarkey -i sim_options.json -s fig6b > /dev/null
./bowe_mullarkey -i sim_options.json -s fig5a > /dev/null
gnuplot "gnuplot_script.gp"
echo -e "Saving results to validation_images"
mv *.jpg ../validation_images/.
cd ../

# collina-bruni
echo -e "Running validation case: ${begc}collina-bruni${endc}"
cd collina-bruni
./collina_bruni -i sim_options.json -s cb2 > /dev/null
gnuplot "gnuplot_script.gp"
echo -e "Saving results to validation_images"
mv *.jpg ../validation_images/.
cd ../

# biggs
#echo -e "\n"
echo -e "Running validation case: ${begc}biggs${endc}"
cd biggs
./biggs -i sim_options.json -s fig8p2 > /dev/null
gnuplot "gnuplot_script.gp"
echo -e "Saving results to validation_images"
mv *.jpg ../validation_images/.
cd ../

# static benchmark
echo -e "Running validation case: ${begc}benchmark-static${endc}"
cd benchmark-static/
echo -e "Running without registration mass"
./linear_benchmark_static -i sim_options.json -s noregs100 > /dev/null
echo -e "Running with registration mass"
./linear_benchmark_static -i sim_options.json -s static100 > /dev/null
./linear_benchmark_static -i sim_options.json -s static200 > /dev/null
gnuplot "gnuplot_script.gp"
echo -e "Saving results to validation_images"
mv *.jpg ../validation_images/.
cd ../

# ohe panto
echo -e "Running validation case: ${begc}ohe-panto${endc}"
cd ohe-panto
echo -e "1. moving load case"
./ohe_panto -i sim_options.json -s hht1bload > /dev/null
echo -e "2. moving mass case"
./ohe_panto -i sim_options.json -s hht1bmass > /dev/null
echo -e "3. moving sdof case"
./ohe_panto -i sim_options.json -s hht1bsdof > /dev/null
echo -e "4. moving 3dof case"
./ohe_panto -i sim_options.json -s hht3dof > /dev/null
echo -e "5. 5-span case (wait)"
./ohe_panto -i sim_options.json -s hht3_5snrg > /dev/null
echo -e "Saving results to validation_images"
gnuplot "gnuplot_script.gp"
mv *.jpg ../validation_images/.
cd ../

cd ../../

#---------------------------------------------------------------------
echo -e "\n${begc}Proceeding to validate the non-linear models${endc}"
pwd
cd nonlinear-models/
mkdir -p build
cd build
cmake ../
make -j
mkdir -p validation_images
rm -f validation_images/*

# static benchmark
echo -e "Running validation case: ${begc}benchmark-static${endc}"
cd benchmark-static/
./nonlinear_benchmark_static -i sim_options.json -s static100 > /dev/null
./nonlinear_benchmark_static -i sim_options.json -s static200 > /dev/null
gnuplot "gnuplot_script.gp"
echo -e "Saving results to validation_images"
mv *.jpg ../validation_images/.
cd ../

# ohe moving-load
echo -e "Running validation case: ${begc}non-linear ohe-moving-load${endc}"
cd moving-load
./nonlinear_moving_load -i sim_options.json -s gen1spload > /dev/null
echo -e "Saving results to validation_images"
gnuplot "gnuplot_script.gp"
mv *.jpg ../validation_images/.
cd ../


# ohe panto
echo -e "Running validation case: ${begc}non-linear ohe-panto${endc}"
cd ohe-panto
echo -e "1. 1 span OHE 1 dof panto"
./nonlinear_ohe_panto -i sim_options.json -s gen1sp1dof > /dev/null
echo -e "2. 1 span OHE 3 dof panto"
./nonlinear_ohe_panto -i sim_options.json -s gen1sp3dof > /dev/null
echo -e "3. 3 span OHE 3 dof panto"
./nonlinear_ohe_panto -i sim_options.json -s gen3sp3dof > /dev/null
echo -e "3. 1 span OHE 3 dof panto : intermittent contact"
./nonlinear_ohe_panto -i sim_options.json -s int1sp3dof > /dev/null
echo -e "4. 3 span OHE 3 dof panto : intermittent contact"
./nonlinear_ohe_panto -i sim_options.json -s int3sp3dof > /dev/null
echo -e "Saving results to validation_images"
gnuplot "gnuplot_script.gp"
mv *.jpg ../validation_images/.
cd ../

cd ../../

