#! /bin/bash

begc='\033[0;35m'
endc='\033[0m'
set -e

#---------------------------------------------------------------------
echo -e "\n${begc}Proceeding to validate the non-linear models${endc}"
pwd
cd nonlinear-models/
mkdir -p build
cd build
cmake ../
make -j
mkdir -p validation_images
rm -f validation_images/*

# static benchmark
echo -e "Running validation case: ${begc}benchmark-static${endc}"
cd benchmark-static/
#./nonlinear_benchmark_static -i sim_options.json -s static100 > /dev/null
#./nonlinear_benchmark_static -i sim_options.json -s static200 > /dev/null
gnuplot "gnuplot_script.gp"
echo -e "Saving results to validation_images"
mv *.jpg ../validation_images/.
cd ../

# ohe moving-load
echo -e "Running validation case: ${begc}non-linear ohe-moving-load${endc}"
cd moving-load
#./nonlinear_moving_load -i sim_options.json -s gen1spload > /dev/null
echo -e "Saving results to validation_images"
gnuplot "gnuplot_script.gp"
mv *.jpg ../validation_images/.
cd ../


# ohe panto
echo -e "Running validation case: ${begc}non-linear ohe-panto${endc}"
cd ohe-panto
#echo -e "1. 1 span OHE 1 dof panto"
#./nonlinear_ohe_panto -i sim_options.json -s gen1sp1dof > /dev/null
#echo -e "2. 1 span OHE 3 dof panto"
#./nonlinear_ohe_panto -i sim_options.json -s gen1sp3dof > /dev/null
#echo -e "3. 3 span OHE 3 dof panto"
#./nonlinear_ohe_panto -i sim_options.json -s gen3sp3dof > /dev/null
#echo -e "3. 1 span OHE 3 dof panto : intermittent contact"
#./nonlinear_ohe_panto -i sim_options.json -s int1sp3dof > /dev/null
#echo -e "4. 3 span OHE 3 dof panto : intermittent contact"
#./nonlinear_ohe_panto -i sim_options.json -s int3sp3dof > /dev/null
echo -e "Saving results to validation_images"
gnuplot "gnuplot_script.gp"
mv *.jpg ../validation_images/.
cd ../

cd ../../

