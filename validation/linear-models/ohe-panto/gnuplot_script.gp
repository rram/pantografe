set term jpeg
set output "ohe-movingload_u2.jpg"
plot "output-load1b/history.dat" u 1:3 w l, "../../ohe-panto/data/pantografe.load" u 1:3 w l

set term jpeg
set yrange [-50:250]
set output "ohe-movingmass_fc.jpg"
plot "output-mass1b/history.dat" u 1:2 w l, "../../ohe-panto/data/pantografe.mass" u 1:2 w l

set term jpeg
set yrange [-50:250]
set output "ohe-movingsdof_fc.jpg"
plot "output-sdof1b/history.dat" u 1:2 w l, "../../ohe-panto/data/pantografe.sdof" u 1:2 w l, "../../ohe-panto/data/abaqus_sdof_fc.txt" u 1:2 w l, "../../ohe-panto/data/comsol_sdof.txt" u 1:2 w l

set term jpeg
set yrange [-50:250]
set output "ohe-moving3dof_fc.jpg"
plot "output-3dof/history.dat" u 1:2 w l, "../../ohe-panto/data/pantografe.3dof" u 1:2 w l, "../../ohe-panto/data/abaqus_3dof_fc.txt" u 1:2 w l, "../../ohe-panto/data/comsol_3dof.txt" u 1:2 w l

set term jpeg
set title "5-span validation against previous version" 
set yrange [-50:250]
set xrange [0:5.38]
set output "ohe-5span_fc.jpg"
plot "output_5span/history.dat" u 1:2 w l, "../../ohe-panto/data/pantografe.5span" u 1:2 w l 


