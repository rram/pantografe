
cmake_minimum_required(VERSION 3.12)
  
project(validation  DESCRIPTION "Validation of Linear-model of pantograFE")
 
# find dependencies
find_package(pantograFE REQUIRED)  

list(APPEND COMPILE_FEATURES cxx_std_17) 

add_subdirectory(benchmark-static)
add_subdirectory(biggs)
add_subdirectory(bowe-mullarkey)
add_subdirectory(collina-bruni)
add_subdirectory(ohe-panto)
