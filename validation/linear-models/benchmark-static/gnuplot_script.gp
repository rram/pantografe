set term jpeg
set output "benchmark_static.jpg"
plot "output-n1/contact.dat" u 1:2 w l, "../../benchmark-static/benchmark-data/abaqus.txt" u 1:2 w l,  "../../../linear-models/benchmark-static/benchmark-data/comsol.txt" u 1:2 w l

set term jpeg
set output "benchmark_stiff_100.jpg"
plot "output-n1/stiffness.dat" u 1:2 w l, "../../benchmark-static/benchmark-data/abaqus100.txt" u 1:2 w p,  "../../../linear-models/benchmark-static/benchmark-data/comsol100.txt" u 1:2 w p

set term jpeg
set key center top box
set title "Stiffness of the OHE for applied load of 100 N" 
set ylabel "Stiffness [N/m]"
set xlabel "Distance [m]"
set xrange [0:55]
set output "benchmark_RA_stiff_100.jpg"
plot "output-s1/stiffness.dat" u 1:2 w l title "PantograFE", \
"../../benchmark-static/benchmark-data/abaqus_stiffness_RA.txt" u 1:2 w p title "ABAQUS", \
"../../benchmark-static/benchmark-data/comsol_static_RA.txt" u 1:3 w l title "COMSOL", \
"../../benchmark-static/benchmark-data/pcrun100.csv" u 1:2 w l title "PCRUN", \
"../../benchmark-static/benchmark-data/spops100.csv" u 1:2 w l title "SPOPS"

set term jpeg
set key center top box
set title "Stiffness of the OHE for applied load of 200 N" 
set ylabel "Stiffness [N/m]"
set xlabel "Distance [m]"
set xrange [0:55]
set output "benchmark_RA_stiff_200.jpg"
plot "output-s2/stiffness.dat" u 1:2 w l title "PantograFE", \
"../../benchmark-static/benchmark-data/abaqus_stiffness_RA.txt" u 1:3 w p title "ABAQUS", \
"../../benchmark-static/benchmark-data/comsol_static_RA.txt" u 1:4 w l title "COMSOL", \
"../../benchmark-static/benchmark-data/spops200.csv" u 1:2 w l title "SPOPS", \
"../../benchmark-static/benchmark-data/pcrun200.csv" u 1:2 w l title "PCRUN", \
"../../benchmark-static/benchmark-data/oscar200.csv" u 1:2 w l title "OSCAR"

