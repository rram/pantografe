
/** \file linear_benchmark_static.cpp
 * \brief Model test to verify prediction of static deflection in a single span
 * \author Ramsharan Rangarajan
 * \validation \ref ohestat : Validation of static deflection of a single span OHE
 */

/* Input:
   -i input json file with simulation options
   -s simulation id in the input file
*/

#include <pfe_linear_Model_Static_OHE.h>
#include <pfe_CLI11.hpp>
#include <pfe_json.hpp>


// simulation parameters
void read_simulation_parameters(const std::string json_file, const std::string tag,
				std::string& ohe_file, std::string& out_directory,
				int& nelm, int& num_length_iters, double& force_val);
  


int main(int argc, char** argv)
{
  using MODEL_TYPE = pfe::linear::Model_Static_SingleSpan_OHE;
  using OHE_TYPE   = pfe::linear::SingleSpan_OHE;
  
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRQ(ierr);

  // read commandline options
  std::string sim_file;
  std::string sim_id;
  CLI::App app;
  app.add_option("-i", sim_file, "json file with simulation parameters")->required()->check(CLI::ExistingFile);
  app.add_option("-s", sim_id, "simulation is in input file")->required();
  CLI11_PARSE(app, argc, argv);

  // read simulation parameters
  std::string ohe_file;
  std::string out_directory;
  int         nelm;
  int         num_length_iters;
  double      force_val;
  read_simulation_parameters(sim_file, sim_id, ohe_file, out_directory, nelm, num_length_iters, force_val);

  // OHE parameters
  pfe::OHESpan_ParameterPack ohe_pack;
  std::fstream jfile;
  jfile.open(ohe_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());
  jfile >> ohe_pack;
  jfile.close();

  // Create the OHE span
  OHE_TYPE ohe(0.0, ohe_pack, nelm);

  // Create an instance of the model
  MODEL_TYPE model(ohe);

  // Compute the static deflection of the OHE (unadjusted dropper lengths)
  const int nTotalDof = model.GetTotalNumDof();
  std::vector<double> displacements(nTotalDof);
  model.Solve(displacements.data());
  
  // Dropper length adjustment to match target sag if requested
  if(num_length_iters>0)
    {
      // Access the sag at the dropper nodes with unadjusted dropper lengths
      const auto dropper_dofs = ohe.GetDropperDofs(0);
      std::vector<double> orig_dropper_sag(ohe_pack.dropperSchedule.nDroppers);
      for(int d=0; d<ohe_pack.dropperSchedule.nDroppers; ++d)
	orig_dropper_sag[d] = displacements[dropper_dofs[d].second];

      // iterative dropper length adjustment
      std::cout << std::endl << "Performing dropper length adjustment " << std::endl;
      model.AdjustDropperLengths(0, num_length_iters);
      model.Solve(displacements.data());
  
      // write adjusted length & sag to file
      jfile.open(out_directory+"/dropper_adjustments.dat", std::ios::out);
      assert(jfile.good());
      jfile <<"#1.dropper# 2.x_dropper 3.L_nominal 4.L_adjusted 5.target_sag 6.unadjusted_sag 7.adjusted_sag" << std::endl;
      for(int d=0; d<ohe_pack.dropperSchedule.nDroppers; ++d)
	jfile << d << " " << ohe_pack.dropperSchedule.coordinates[d] << " "                                           // 1, 2
	      << ohe_pack.dropperSchedule.nominal_lengths[d] << " " << ohe.GetDroppers()[d]->GetLength() << " "      // 3, 4
	      << ohe_pack.dropperSchedule.target_sag[d] << " "                                                        // 5
	      << orig_dropper_sag[d] << " " << displacements[dropper_dofs[d].second] << std::endl;                    // 6, 7
      jfile.close();
    }

  // visualize deflection
  model.Write(displacements.data(), out_directory+"/catenary.dat", out_directory+"/contact.dat", out_directory+"/droppers.dat");
  
  // Compute the stiffness distribution
  const int nNodes = ohe.ContactWire().GetNumNodes();
  const auto& coordinates = ohe.ContactWire().GetCoordinates();
  std::vector<double> stiffness(nNodes);
  model.ComputeTransverseStiffnessDistribution(0, displacements.data(), force_val, stiffness.data());
  jfile.open(out_directory+"/stiffness.dat", std::ios::out);
  for(int a=0; a<nNodes; ++a)
    jfile << coordinates[a] << " " << stiffness[a] << std::endl;
  jfile.close();
  
  // clean up
  model.Destroy();
  ohe.Destroy();
  PetscFinalize();
}


// simulation parameters
void read_simulation_parameters(const std::string json_file, const std::string tag,
				std::string& ohe_file, std::string& out_directory,
				int& nelm, int& num_length_iters, double& force_val)
{
  assert(std::filesystem::path(json_file).extension()==".json" &&
	 "Expected simulation options file to have .json extension");
  std::fstream jfile;
  jfile.open(json_file, std::ios::in);
  assert((jfile.good() && jfile.is_open()) && "Could not open simulation options file");

  // get the data
  auto J = nlohmann::json::parse(jfile);
  jfile.close();

  // read options
  auto it = J.find(tag);
  assert(it!=J.end() && "Could not find simulation id in input file");
  auto& j = *it;
  assert(j.contains("ohe file")                     && "Input file missing \"ohe file\" tag");
  assert(j.contains("num elements")                 && "Input file missing \"num elements\" tag");
  assert(j.contains("output directory")             && "Input file missing \"output directory\" tag");
  assert(j.contains("length adjustment iterations") && "Input file missing \"length adjustment iterations\" tag");
  assert(j.contains("force")                        && "Input file missing \"force\" tag");
  
  j["ohe file"].get_to(ohe_file);
  j["num elements"].get_to(nelm);
  j["output directory"].get_to(out_directory);
  j["length adjustment iterations"].get_to(num_length_iters);
  j["force"].get_to(force_val);

  assert((ohe_file.empty()==false && std::filesystem::exists(ohe_file)) &&
	 "Cannot find ohe file specified in input file");
  assert(nelm>0 && "Expected positive number of \"num elements\" in input file");
  assert(num_length_iters>=0 && "Unexpected number of \"length adjustment iterations\" specified in input file");

  // create the output directory if it does not exist, erase .dat files it does
  if(std::filesystem::exists(out_directory)==true)
    {
      std::cout << "Output directory " << out_directory << " exists. Erasing dat files" << std::endl;
      auto it_dir = std::filesystem::directory_iterator(out_directory);
      for(auto& it:it_dir)
	if(it.path().extension()==".dat")
	  {
	    auto flag = std::filesystem::remove(it.path());
	    assert(flag==true && "Could not erase file");
	  }
    }
  else
    {
      std::cout << "Creating output directory \"" << out_directory << "\"" << std::endl;
      auto flag  = std::filesystem::create_directory(out_directory);
      assert(flag==true && "Could not create output directory");
    }

  // should not have any other tags
  std::set<std::string> tags{"ohe file", "num elements", "output directory", "length adjustment iterations", "force", "_comment"};
  for(auto jt:j.items())
    if(tags.find(jt.key())==tags.end())
      {
	std::cout << "Unexpected tag " << jt.key() << " found in input file " << std::endl;
	assert(false);
      }
}
