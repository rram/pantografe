
/** \file Tests for model-1 using a single dof pantograph
 * \brief Reproduces results from Bowe & Mullarkey, and Collina & Bruni
 * \author Ramsharan Rangarajan
 * \validation Validation with the example of Collina & Bruni
 */

/* Input:
   -i input file in json format with simulation options
   -s simulation id
*/

/* Output: 
   beam*.dat     beam configuration indexed by time step, columns: x w w-dot w-ddot 
   load*.dat     position of the load, indexed by time step
   history.dat   time history of the deflection of the free end of the beam
*/

#include <pfe_linear_Model_OHE_Panto.h>
#include <pfe_GenAlpha_with_Contact.h>
#include <pfe_HHTAlpha_with_Contact.h>
#include <pfe_Newmark_with_Contact.h>
#include <pfe_CLI11.hpp>
#include <pfe_json.hpp>
#include <iostream>
#include <fstream>

// simulation parameter
void read_simulation_parameters(const std::string json_file, const std::string tag,
				std::string& beam_file, std::string& ohe_tag,
				std::string& panto_file, std::string& panto_tag,
				std::string& bc_type, int& nelm,
				std::string& time_integrator_type, double& time_integrator_param,
				double& timestep, int& num_time_steps,
				std::string& out_directory);

// time stepping
void Simulate(pfe::linear::Model_Cable_Panto& model, const std::string out_directory, const int num_time_steps);


int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRQ(ierr);

  // read command line options
  std::string sim_file;          // simulation options
  std::string sim_id;            // simulation id
  CLI::App app;
  app.add_option("-i", sim_file, "json file with simulation parameters")->required()->check(CLI::ExistingFile);
  app.add_option("-s", sim_id, "simulation id")->required();
  CLI11_PARSE(app, argc, argv);

  // read simulation parameters
  std::string beam_file;              // json file with beam data
  std::string ohe_tag;                // tag for the ohe
  std::string panto_file;             // json file with panto data
  std::string panto_tag;              // tag for the pantograph
  std::string out_directory;          // output directory
  std::string bc_type;                // boundary condition type: fixed-free or free-fixed
  int         nelm;                   // number of elements for one beam span
  std::string time_integrator_type;   // time integrator type
  double      time_integrator_param;  // parameter to be used with the time integrator
  double      timestep;               // time step
  int         num_time_steps;         // number of time steps
  read_simulation_parameters(sim_file,  sim_id,
			     beam_file, ohe_tag, panto_file, panto_tag, 
			     bc_type,   nelm,
			     time_integrator_type, time_integrator_param,
			     timestep, num_time_steps, out_directory);
  
  // beam parameters
  pfe::ContactWireParams wireParams{.tag=ohe_tag};
  std::fstream jfile;
  jfile.open(beam_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());
  jfile >> wireParams;
  jfile.close();

  // Create the beam system 
  pfe::HeavyTensionedBeam beam(wireParams, nelm);

  // pantograph
  pfe::Pantograph_1_Params panto_params{.tag=panto_tag};
  jfile.open(panto_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());
  jfile >> panto_params;
  jfile.close();
  pfe::Pantograph_1 panto(panto_params);

  // Time integrator
  const int nTotalDof = beam.GetTotalNumDof()+panto.GetTotalNumDof();
  std::unique_ptr<pfe::TimeIntegrator_with_Contact> TI;
  if(time_integrator_type=="generalized alpha")
    TI = std::make_unique<pfe::GenAlpha_with_Contact>(time_integrator_param, nTotalDof, timestep);
  else if(time_integrator_type=="hht alpha")
    TI = std::make_unique<pfe::HHTAlpha_with_Contact>(time_integrator_param, nTotalDof, timestep);
  else if(time_integrator_type=="newmark")
    TI = std::make_unique<pfe::Newmark_with_Contact>(nTotalDof, timestep);
  else
    {
      assert(false && "Expected time integrator to be one of 'generalized alpha', 'hht alpha' or 'newmark'"); 
    }
  
  // boundary conditions on the beam
  std::vector<int> dirichlet_dofs{};
  const int nNodes = beam.GetNumNodes();
  if(bc_type=="fixed-free")
    dirichlet_dofs = {0, nNodes};
  else if(bc_type=="fixed-fixed")
    dirichlet_dofs = {0, nNodes-1};
  else
    {
      assert(false && "Expected boundary conditions to be of type either 'fixed-free' or 'free-fixed'");
    }

  // create the model
  pfe::linear::Model_Cable_Panto model(beam, panto, *TI, dirichlet_dofs);

  // simulate
  Simulate(model, out_directory, num_time_steps);

  // clean up
  TI->Destroy();
  model.Destroy();
  beam.Destroy();
  PetscFinalize();
}


// time stepping
void Simulate(pfe::linear::Model_Cable_Panto& model, const std::string out_directory, const int num_time_steps)
{
  const int nTotalDof = model.GetTotalNumDof();

  // Set initial conditions
  std::vector<double> zero(nTotalDof);
  std::fill(zero.begin(), zero.end(), 0.);
  model.SetInitialConditions(zero.data(), zero.data(), zero.data(), 0.);
  
  // Visualize
  //model.Write({out_directory+"/beam-0.dat"}, out_directory+"/load-0.dat");

  // History of the contact force
  std::fstream hfile;
  hfile.open(out_directory+"/history.dat", std::ios::out);
  assert(hfile.good());
  hfile << "# time \t contact force" << std::endl;

  // Moving pantograph
  std::cout << "Time step (of " << num_time_steps << ") " << std::endl;
  for(int tstep=0; tstep<num_time_steps; ++tstep)
    {
      std::cout << tstep+1 << std::endl;
      model.Advance();

      //model.Write({out_directory+"/beam-"+std::to_string(tstep+1)+".dat"}, out_directory+"/load-"+std::to_string(tstep+1)+".dat");

      const double time =  model.GetTime();
      hfile << model.GetTime() << "\t" << model.GetContactForce() << std::endl;
    }
  hfile.close();
  
  // done
  return;
  
}



// simulation parameter
void read_simulation_parameters(const std::string json_file, const std::string tag,
				std::string& beam_file, std::string& ohe_tag,
				std::string& panto_file, std::string& panto_tag,
				std::string& bc_type, int& nelm,
				std::string& time_integrator_type, double& time_integrator_param,
				double& timestep, int& num_time_steps,
				std::string& out_directory)
{
  assert(std::filesystem::path(json_file).extension()==".json");
  std::fstream jfile;
  jfile.open(json_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());

  // get the data
  auto J = nlohmann::json::parse(jfile);
  jfile.close();

  // read options
  auto it = J.find(tag);
  assert(it!=J.end());
  auto& j = *it;
  assert(j.contains("ohe file")           && "Input file missing cable file tag");
  assert(j.contains("ohe tag")            && "Input file missing ohe tag");
  assert(j.contains("panto file")         && "Input file missing panto file tag");
  assert(j.contains("panto tag")          && "Input file missing pantograph tag");
  assert(j.contains("boundary condition") && "Input file missing boundary condition tag");
  assert(j.contains("num elements")       && "Input file missing num elements tag");
  assert(j.contains("time integrator")    && "Input file missing time integrator tag");
  assert(j.contains("time step")          && "Input file missing time step tag");
  assert(j.contains("num time steps")     && "Input file missing num time steps tag");
  assert(j.contains("output directory")   && "Input file missing output directory tag");
  
  j["ohe file"].get_to(beam_file);
  j["ohe tag"].get_to(ohe_tag);
  j["panto file"].get_to(panto_file);
  j["panto tag"].get_to(panto_tag);
  j["boundary condition"].get_to(bc_type);
  j["num elements"].get_to(nelm);
  j["time integrator"].get_to(time_integrator_type);
  j["time step"].get_to(timestep); 
  j["num time steps"].get_to(num_time_steps);
  j["output directory"].get_to(out_directory);

  // list of tags
  std::set<std::string> tags{"ohe file", "ohe tag", "panto file", "panto tag",
      "boundary condition", "num elements", "time integrator", "time step", "num time steps",
      "output directory", "_comment"};
  
  // time integrator parameter
  if(time_integrator_type=="generalized alpha")
    {
      assert(j.contains("spectral radius") && "Input file missing spectral radius tag");
      j["spectral radius"].get_to(time_integrator_param);
      tags.insert("spectral radius");
    }
  else if(time_integrator_type=="hht alpha")
    {
      assert(j.contains("alpha") && "Input file missing alpha tag");
      j["alpha"].get_to(time_integrator_param);
      tags.insert("alpha");
    }

  // checks on inputs
  assert(beam_file.empty()==false && std::filesystem::exists(beam_file));
  assert(panto_file.empty()==false && std::filesystem::exists(panto_file));
  assert(nelm>0 && timestep>0 && num_time_steps>0);
  
  // create the output directory if it does not exist, erase .dat files it does
  if(std::filesystem::exists(out_directory)==true)
    {
      std::cout << "Output directory " << out_directory << " exists. Erasing dat files" << std::endl;
      auto it_dir = std::filesystem::directory_iterator(out_directory);
      for(auto& it:it_dir)
	if(it.path().extension()==".dat")
	  {
	    auto flag = std::filesystem::remove(it.path());
	    assert(flag==true && "Could not erase file");
	  }
    }
  else
    {
      std::cout << "Creating output directory \"" << out_directory << "\"" << std::endl;
      auto flag  = std::filesystem::create_directory(out_directory);
      assert(flag==true && "Could not create output directory");
    }

  // Check consistency of tags
  for(auto jt:j.items())
    if(tags.find(jt.key())==tags.end())
      {
	std::cout << "Unexpected tag " << jt.key() << " found in input file " << std::endl;
	assert(false);
      }

  // done
  return;
}
