set term jpeg
set title "Vertical displacement of cantilever end point" 
set ylabel "Displacement [m]"
set xlabel "Time [s]"
set output "bowe-mullarkey_5a.jpg"
set xrange [0:0.15]
plot "output-fig5a/history.dat" u 1:3 w l title "PantograFE", \
"../../bowe-mullarkey/data/abaqus5a.txt" u 1:2 w l title "ABAQUS",  \
"../../bowe-mullarkey/data/comsol5a.txt" u 1:2 w l title "COMSOL"

set term jpeg
set title "Vertical displacement of cantilever end point" 
set ylabel "Displacement [m]"
set xlabel "Time [s]"
set xrange [0:0.15]
set output "bowe-mullarkey_6b.jpg"
plot "output-fig6b/history.dat" u 1:3 w l title "PantograFE", \
"../../bowe-mullarkey/data/abaqus6b.txt" u 1:2 w l title "ABAQUS", \
"../../bowe-mullarkey/data/comsol6b.txt" u 1:2 w l title "COMSOL"

set term jpeg
set title "Contact force between mass and cantilever" 
set ylabel "Contact Force [N]"
set xlabel "Time [s]"
set xrange [0:0.15]
set output "bowe-mullarkey_7.jpg"
plot "output-fig6b/history.dat" u 1:4 w l  title "PantograFE", \
"../../bowe-mullarkey/data/abaqus7.txt" u 1:2 w l  title "ABAQUS", \
"../../bowe-mullarkey/data/comsol6b.txt" u 1:3 w l title "COMSOL"
