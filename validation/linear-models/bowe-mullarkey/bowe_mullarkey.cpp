
/** \file bowe_mullarkey.cpp
 * \brief Examples from Bowe & Mullarkey
 * wheel: intertia may or may not be included, gravity effect included
 * beam:  inertia included, no gravity effect
 * beam bcs: fixed-free or free-fixed
 * monitor the deflection of the free end
 * \author Ramsharan Rangarajan
 *
 * \validation \ref cantload : Validation of moving load on a cantilever beam
 * \validation \ref cantmass : Validation of moving mass+load on a cantilever beam 
 * \validation \ref ssbeam : Validation of static deflection due to self-weight in a simply supported beam 
 * \validation \ref ssbmovms : Validation of moving mass+load on a simply supported beam  
 *
 */

/* Input:
   -i input file in json format with simulation options
   -s simulation id
*/

/* Output: 
   beam*.dat     beam configuration indexed by time step, columns: x w w-dot w-ddot 
   load*.dat     position of the load, indexed by time step
   history.dat   time history of the deflection of the free end of the beam
*/

#include <pfe_linear_Model_MovingLoad.h>
#include <pfe_linear_Model_MovingMass_local.h>
#include <pfe_linear_Model_OHE_Panto.h>

#include <pfe_Newmark.h>
#include <pfe_GenAlpha.h>
#include <pfe_HHTAlpha.h>

#include <pfe_Newmark_with_Contact.h> 
#include <pfe_GenAlpha_with_Contact.h>
#include <pfe_HHTAlpha_with_Contact.h>

#include <pfe_CLI11.hpp>
#include <pfe_json.hpp>
#include <iostream>
#include <fstream>

// simulation parameter
void read_simulation_parameters(const std::string json_file, const std::string tag,
				std::string& beam_file, std::string& ohe_tag,
				std::string& panto_file, std::string& panto_tag,
				int& pantograph_type, std::string& pantograph_inertia,
				std::string& bc_type,
				std::string& time_integrator_type, double& time_integrator_param,
				int& num_spans, int& nelm,
				double& timestep, int& num_time_steps,
				std::string& out_directory);

// time stepping
template<class ModelType>
void Simulate(ModelType& model, const int output_dof,
	      const double span, const double speed, const double panto_wt,
	      const std::string out_directory, const int num_time_steps);

// time stepping
template<>
void Simulate<pfe::linear::Model_Cable_Panto>(pfe::linear::Model_Cable_Panto&  model, const int output_dof,
					      const double span, const double speed, const double panto_wt,
					      const std::string out_directory, const int num_time_steps);


int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRQ(ierr);

  // read command line options
  std::string sim_file;          // simulation options
  std::string sim_id;            // simulation id
  CLI::App app;
  app.add_option("-i", sim_file, "json file with simulation parameters")->required()->check(CLI::ExistingFile);
  app.add_option("-s", sim_id, "simulation id")->required();
  CLI11_PARSE(app, argc, argv);

  // read simulation parameters
  std::string beam_file;      // json file with beam data
  std::string ohe_tag;         // tag for the ohe
  std::string panto_file;      // json file with panto data
  std::string panto_tag;       // tag for the pantograph
  std::string out_directory;   // output directory
  int         pantograph_type; // type of pantograph
  std::string pantograph_inertia; // interia type for the pantograph
  std::string bc_type;         // boundary condition type: fixed-free or free-fixed
  std::string time_integrator_type;   // time integrator type
  double      time_integrator_param;  // parameter required for the time integrator
  int         num_spans;       // number of spans to consider
  int         nelm;            // number of elements for one beam span
  double      timestep;        // time step
  int         num_time_steps;  // number of time steps
  read_simulation_parameters(sim_file, sim_id,
			     beam_file, ohe_tag, panto_file, panto_tag, 
			     pantograph_type, pantograph_inertia, bc_type,
			     time_integrator_type, time_integrator_param,
			     num_spans, nelm, timestep, num_time_steps, out_directory);
  assert((bc_type=="fixed-free" || bc_type=="free-fixed") && "Unexpected boundary condition");
  assert((pantograph_type==0 || pantograph_type==1) && "Expected pantograph type to be 0");
  assert((pantograph_inertia=="none" || pantograph_inertia=="local" || pantograph_inertia=="full") && "Unexpected pantograph inertia model provided");
  assert(num_spans==1 && "Expected 1 span");

  // beam parameters
  pfe::ContactWireParams wireParams{.tag=ohe_tag};
  std::fstream jfile;
  jfile.open(beam_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());
  jfile >> wireParams;
  jfile.close();

  // Create the beam system 
  pfe::HeavyTensionedBeam beam(wireParams, nelm);

  // bcs and dofs to monitor
  const int nNodes = beam.GetNumNodes();
  std::vector<int> dirichlet_dofs{};
  int output_dof = -1;
  if(bc_type=="fixed-free")
    {
      dirichlet_dofs = {0, nNodes};
      output_dof = nNodes-1;
    }
  else if(bc_type=="free-fixed")
    {
      dirichlet_dofs = {nNodes-1, 2*nNodes-1};
      output_dof = 0;
    }
	      
  // pantograph parameters
  if(pantograph_type==0)
    {
      assert(pantograph_inertia=="none" && "Expected pantograph inertia to be set to none");
      pfe::Pantograph_0_Params panto_0_params{.tag=panto_tag};
      jfile.open(panto_file, std::ios::in);
      assert(jfile.good() && jfile.is_open());
      jfile >> panto_0_params;
      jfile.close();

      std::unique_ptr<pfe::TimeIntegrator> TI;
      if(time_integrator_type=="newmark")
	TI = std::make_unique<pfe::Newmark>(beam.GetTotalNumDof(), timestep);
      else if(time_integrator_type=="generalized alpha")
	TI = std::make_unique<pfe::GenAlpha>(time_integrator_param, beam.GetTotalNumDof(), timestep);
      else if(time_integrator_type=="hht alpha")
	TI = std::make_unique<pfe::HHTAlpha>(time_integrator_param, beam.GetTotalNumDof(), timestep);
      else
	assert(false && "Unexpected time integrator type");
      
      pfe::linear::Model_Cable_MovingLoad model(beam, panto_0_params, *TI, dirichlet_dofs);
      Simulate(model, output_dof, wireParams.span, panto_0_params.speed, panto_0_params.force, out_directory, num_time_steps);

      TI->Destroy();
      model.Destroy();
    }
  else if(pantograph_inertia=="local")
    {
      pfe::Pantograph_1_Params panto_1_params{.tag=panto_tag};
      jfile.open(panto_file, std::ios::in);
      assert(jfile.good() && jfile.is_open());
      jfile >> panto_1_params;
      jfile.close();

      std::unique_ptr<pfe::TimeIntegrator> TI;
      if(time_integrator_type=="newmark")
	TI = std::make_unique<pfe::Newmark>(beam.GetTotalNumDof(), timestep);
      else if(time_integrator_type=="generalized alpha")
	TI = std::make_unique<pfe::GenAlpha>(time_integrator_param, beam.GetTotalNumDof(), timestep);
      else if(time_integrator_type=="hht alpha")
	TI = std::make_unique<pfe::HHTAlpha>(time_integrator_param, beam.GetTotalNumDof(), timestep);
      else
	assert(false && "Unexpected time integrator type");
      
      pfe::linear::Model_Cable_MovingMass_local model(beam, panto_1_params, *TI, dirichlet_dofs);
      Simulate(model, output_dof, wireParams.span, panto_1_params.speed, panto_1_params.force, out_directory, num_time_steps);

      TI->Destroy();
      model.Destroy();
    }
  else
    {
      pfe::Pantograph_1_Params panto_1_params{.tag=panto_tag};
      jfile.open(panto_file, std::ios::in);
      assert(jfile.good() && jfile.is_open());
      jfile >> panto_1_params;
      jfile.close();
      pfe::Pantograph_1 panto_1(panto_1_params);
      
      std::unique_ptr<pfe::TimeIntegrator_with_Contact> TI;
      if(time_integrator_type=="newmark")
	TI = std::make_unique<pfe::Newmark_with_Contact>(beam.GetTotalNumDof()+panto_1.GetTotalNumDof(), timestep);
      else if(time_integrator_type=="generalized alpha")
	TI = std::make_unique<pfe::GenAlpha_with_Contact>(time_integrator_param, beam.GetTotalNumDof()+panto_1.GetTotalNumDof(), timestep);
      else if(time_integrator_type=="hht alpha")
	TI = std::make_unique<pfe::HHTAlpha_with_Contact>(time_integrator_param, beam.GetTotalNumDof()+panto_1.GetTotalNumDof(), timestep);
      else
	assert(false && "Unexpected time integrator type");

      pfe::linear::Model_Cable_Panto model(beam, panto_1, *TI, dirichlet_dofs);
      Simulate(model, output_dof, wireParams.span, panto_1_params.speed, panto_1_params.force, out_directory, num_time_steps);

      TI->Destroy();
      model.Destroy();
    }
  
  // Clean up
  beam.Destroy();
  PetscFinalize();
}


// time stepping
template<class ModelType>
void Simulate(ModelType& model, const int output_dof, const double span, const double speed, const double panto_wt,
	      const std::string out_directory, const int num_time_steps)
{
  const int nTotalDof = model.GetTotalNumDof();

  // Set initial conditions
  std::vector<double> zero(nTotalDof);
  std::fill(zero.begin(), zero.end(), 0.);
  model.SetInitialConditions(zero.data(), zero.data(), zero.data());
  
  // Visualize
  //model.Write({out_directory+"/beam-ref.dat"}, out_directory+"/load-ref.dat");
  model.Write({out_directory+"/beam-0.dat"}, out_directory+"/load-0.dat");

  // History of the free end
  std::fstream hfile;
  hfile.open(out_directory+"/history.dat", std::ios::out);
  assert(hfile.good());
  hfile << "# time \t time*speed/span \t u" << std::endl;
  // Moving pantograph
  std::cout << "Time step (of " << num_time_steps << ") " << std::endl;
  for(int tstep=0; tstep<num_time_steps; ++tstep)
    {
      std::cout << tstep+1 << std::endl;
      model.Advance();
      model.Write({out_directory+"/beam-"+std::to_string(tstep+1)+".dat"}, out_directory+"/load-"+std::to_string(tstep+1)+".dat");
      auto& U = model.GetState().U;
      double uval;
      VecGetValues(U, 1, &output_dof, &uval);
      const double time =  model.GetTime();
      hfile << time << "\t" << time*speed/span << "\t" << uval << std::endl;
    }
  hfile.close();
  
  // done
  return;
  
}


// time stepping for OHE_Panto<> model
template<>
void Simulate<pfe::linear::Model_Cable_Panto>(pfe::linear::Model_Cable_Panto& model, const int output_dof,
					       const double span, const double speed, const double panto_wt,
					       const std::string out_directory, const int num_time_steps)
{
  const int nTotalDof = model.GetTotalNumDof();

  // Set initial conditions
  std::vector<double> zero(nTotalDof);
  std::fill(zero.begin(), zero.end(), 0.);
  model.SetInitialConditions(zero.data(), zero.data(), zero.data(), 0.);
  
  // Visualize
  //model.Write({out_directory+"/beam-ref.dat"}, out_directory+"/load-ref.dat");
  model.Write({out_directory+"/beam-0.dat"}, out_directory+"/load-0.dat");

  // History of the free end
  std::fstream hfile;
  hfile.open(out_directory+"/history.dat", std::ios::out);
  assert(hfile.good());
  hfile << "# time \t time*speed/span \t u \t Fc/weight" << std::endl;
  // Moving pantograph
  std::cout << "Time step (of " << num_time_steps << ") " << std::endl;
  for(int tstep=0; tstep<num_time_steps; ++tstep)
    {
      std::cout << tstep+1 << std::endl;
      model.Advance();
      model.Write({out_directory+"/beam-"+std::to_string(tstep+1)+".dat"}, out_directory+"/load-"+std::to_string(tstep+1)+".dat");
      auto& U = model.GetState().U;
      auto& V = model.GetState().V;
      double uval, vval;
      VecGetValues(U, 1, &output_dof, &uval);
      VecGetValues(V, 1, &output_dof, &vval);
      const double time =  model.GetTime();
      hfile << time << "\t" << time*speed/span << "\t" << uval << "\t" << model.GetContactForce()/panto_wt << std::endl;
    }
  hfile.close();
  
  // done
  return;

}


// simulation parameter
void read_simulation_parameters(const std::string json_file, const std::string tag,
				std::string& beam_file, std::string& ohe_tag,
				std::string& panto_file, std::string& panto_tag,
				int& pantograph_type, std::string& pantograph_inertia,
				std::string& bc_type,
				std::string& time_integrator_type, double& time_integrator_param,
				int& num_spans, int& nelm,
				double& timestep, int& num_time_steps,
				std::string& out_directory)
{
  assert(std::filesystem::path(json_file).extension()==".json");
  std::fstream jfile;
  jfile.open(json_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());

  // get the data
  auto J = nlohmann::json::parse(jfile);
  jfile.close();

  // read options
  auto it = J.find(tag);
  assert(it!=J.end());
  auto& j = *it;
  assert(j.contains("ohe file")           && "Input file missing cable file tag");
  assert(j.contains("ohe tag")            && "Input file missing ohe tag");
  assert(j.contains("panto file")         && "Input file missing panto file tag");
  assert(j.contains("panto tag")          && "Input file missing pantograph tag");
  assert(j.contains("panto type")         && "Input file missing pantograph type tag");
  assert(j.contains("panto inertia")      && "Input file missing pantograph inertia tag");
  assert(j.contains("boundary condition") && "Input file missing boundary condition tag");
  assert(j.contains("time integrator")    && "Input file missing time integrator tag");
  assert(j.contains("num spans")          && "Input file missing num spans type tag");
  assert(j.contains("num elements")       && "Input file missing num elements tag");
  assert(j.contains("time step")          && "Input file missing time step tag");
  assert(j.contains("num time steps")     && "Input file missing num time steps tag");
  assert(j.contains("output directory")   && "Input file missing output directory tag");
  
  j["ohe file"].get_to(beam_file);
  j["ohe tag"].get_to(ohe_tag);
  j["panto file"].get_to(panto_file);
  j["panto tag"].get_to(panto_tag);
  j["panto type"].get_to(pantograph_type);
  j["panto inertia"].get_to(pantograph_inertia);
  j["boundary condition"].get_to(bc_type);
  j["time integrator"].get_to(time_integrator_type);
  j["num spans"].get_to(num_spans);
  j["num elements"].get_to(nelm); 
  j["time step"].get_to(timestep); 
  j["num time steps"].get_to(num_time_steps);
  j["output directory"].get_to(out_directory);

  // list of tags
  std::set<std::string> tags{"ohe file", "ohe tag", "panto file", "panto tag",
      "panto type", "panto inertia", "boundary condition", "time integrator",
      "num spans", "num elements", "time step", "num time steps",
      "output directory", "_comment"};

  // time integrator parameters
  if(time_integrator_type=="generalized alpha")
    {
      assert(j.contains("spectral radius") && "Input file missing spectral radius tag");
      j["spectral radius"].get_to(time_integrator_param);
      tags.insert("spectral radius");
    }
  else if(time_integrator_type=="hht alpha")
    {
      assert(j.contains("alpha") && "Input file missing alpha tag");
      j["alpha"].get_to(time_integrator_param);
      tags.insert("alpha");
    }

  // check inputs
  assert(beam_file.empty()==false && std::filesystem::exists(beam_file));
  assert(panto_file.empty()==false && std::filesystem::exists(panto_file));
  assert(num_spans>0 && nelm>0 && timestep>0 && num_time_steps>0);
  assert(pantograph_type==0 || pantograph_type==1);

  // create the output directory if it does not exist, erase .dat files it does
  if(std::filesystem::exists(out_directory)==true)
    {
      std::cout << "Output directory " << out_directory << " exists. Erasing dat files" << std::endl;
      auto it_dir = std::filesystem::directory_iterator(out_directory);
      for(auto& it:it_dir)
	if(it.path().extension()==".dat")
	  {
	    auto flag = std::filesystem::remove(it.path());
	    assert(flag==true && "Could not erase file");
	  }
    }
  else
    {
      std::cout << "Creating output directory \"" << out_directory << "\"" << std::endl;
      auto flag  = std::filesystem::create_directory(out_directory);
      assert(flag==true && "Could not create output directory");
    }

  for(auto jt:j.items())
    if(tags.find(jt.key())==tags.end())
      {
	std::cout << "Unexpected tag " << jt.key() << " found in input file " << std::endl;
	assert(false);
      }

  // done
  return;
}
