
cmake_minimum_required(VERSION 3.12)
  
project(validation  DESCRIPTION "Validation examples for pantograFE")
 
# find dependencies
find_package(pantograFE REQUIRED)  

list(APPEND COMPILE_FEATURES cxx_std_17) 

add_subdirectory(linear-models)
add_subdirectory(nonlinear-models)
