# pantograFE 

This README provides a quick description and install instructions for pantograFE.  

### What does pantograFE do?
* Simulates dynamic mechanical interactions in OHE-Pantograph systems
 
### Dependencies
* Required: [Git](https://github.com)
* Required: [PETSc](https://github.com/petsc/petsc)
* Required: [LAPACK](http://www.netlib.org/lapack/)
* Required: [CMake](https://cmake.org) version 3.24 or later 
* Required: C++ compiler with support for C++11 features, e.g.,  [g++](https://gcc.gnu.org) or [Clang](https://clang.llvm.org)
* Required: [pkg-config](https://gitlab.freedesktop.org/pkg-config/pkg-config)
* Required: [fftw](https://www.fftw.org)  
* Required: [gtkmm](https://gtkmm.org/en/index.html) for GUI
* Required: [glib](https://docs.gtk.org/glib/) for GUI
* Required: [Gnuplot](http://www.gnuplotting.org) for visualization
<!--* Optional: [Sourcetree](https://www.sourcetreeapp.com) as a git front end -->
<!--* Optional for documentation: [Doxygen](https://doxygen.nl) and [graphviz](https://graphviz.org)-->

### Getting pantograFE
Clone the repository. From the directory in which to install `pantograFE`:
  ```sh
  git clone -b develop git@bitbucket.org:rram/pantografe.git
  ```

### Install dependencies (macOS)
* Install [homebrew](https://brew.sh)
```sh
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

* From the cloned pantograFE folder:
```sh
cd dependencies/
chmod a+x brew.sh
chmod a+x petsc.sh
```
* These scripts need to be run just once- not each time pantograFE is cloned/pulled.
  
#### `brew.sh` ####
  - Usage: `./brew.sh`.  Or `sudo ./brew.sh` (some packages require permissions)
  - The script does the following:
	  - Uses the package manager `homebrew` 
	  - Checks if the following packages are installed: `git, cmake, pkg-config, gnuplot, gtkmm-3, glib and fftw`. If not,
		installs them using `homebrew`.
  - The script checks for each package before installing them. Hence
    it is OK to rerun the script. Re-running *should* have no effect.
	
#### `petsc.sh`
  - Usage: `sudo ./petsc.sh /usr/local` to install `petsc` in `/usr/local`. Or `./petsc.sh destination_folder` to install in a custom location
   - The script does the following:
	 - Clones `petsc` from its github repository
	 - Configures `petsc`. The set of recommended command line options are specified in the script
	 - Compiles `petsc` libraries
	 - Runs test cases to check the installation
	 - Sets the environment variables `PETSC_DIR` and `PETSC_ARCH` in
     `~/.zshrc`
	 - Appends the location of the `petsc` package configuration file to
     `PKG_CONFIG_PATH` 
 - This script should be run just once. Re-running it will *reinstall*
   `petsc` and is *not* recommended.

  
### Installing pantograFE
- From the `pantograFE` directory:
 ```sh
 mkdir build
 cd build
 cmake ../ -DPFE_BUILD_TESTS=ON -DCMAKE_INSTALL_PREFIX=/usr/local
 make -j
 ctest -j
 sudo make install
 make docs
 ```

**Build options:**

	- build unit tests: `-DPFE_BUILD_TESTS` *Recommended*: `-DPFE_BUILD_TESTS=ON`
	- install location: `-DCMAKE_INSTALL_PREFIX` *Recommended*: `-DCMAKE_INSTALL_PREFIX=/usr/local/`

### Updating `pantograFE`
From the `pantograFE` directory: 
**Update:**
  ```sh
  git pull
  make -j
  sudo make install
  ```

**Reset** (e.g., in case of conflicts. *Will override all changes*): 
  ```sh
  git reset --hard HEAD
  make -j
  sudo make install
  ```
  
**Inspect differences** at any time: `git diff`

### Building applications
From the `pantograFE` home directory:
```sh
cd apps
mkdir build
cd build
cmake ../ 
make -j
```


### Install dependencies on Ubuntu
* On Ubuntu
	- CMake: `sudo apt install -y cmake`
	- Lapack: `sudo apt install -y liblapack-dev`
	- pkg-config: `sudo apt install -y pkg-config`
	- gnuplot: `sudo apt install -y gnuplot`
	- fftw: `sudo apt install -y libfftw3-dev`
	- gtkmm: `sudo apt install -y libgtkmm-3.0-dev`
	- glib: `sudo apt install -y libglib2.0-dev`
	
#### Installing PETSc
- Clone the [repository](https://github.com/petsc/petsc)
- From the home directory of `petsc`, configure:	
  ```sh
  ./config/configure.py --with-debugging=0 --with-x=0 --with-mpi=0
--with-shared-libraries=0 --with-single-library=1 CXXOPTFLAGS="-O3
-std=c++11" COPTFLAGS=-O3 --with-fc=0 --download-f2cblaslapack
   ```
- Compile libraries: `make all`
- Run checks: `make check`
- Append `PETSC_DIR` and `PETSC_ARCH` variables to `.zshrc` (use
`.bashrc` on Linux): 
	```sh
	echo 'export PETSC_DIR=/path/to/petsc/dir' >> ~/.zshrc
	echo 'export PETSC_ARCH=architecture-name' >> ~/.zshrc
	```
- Help `pkg-config` locate the `petsc` configuration file 
	```sh
	echo 'export
	PKG_CONFIG_PATH=${PKG_CONFIG_PATH}:${PETSC_DIR}/${PETSC_ARCH}/lib/pkgconfig/'>> ~/.zshrc
	```

### Who do I talk to?

* Ram (rram@iisc.ac.in)

