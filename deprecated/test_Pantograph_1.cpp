
/** \file test_SimplePantograph.cpp
 * \brief Unit test for the component pfe::SimplePantograph
 * \author Ramsharan Rangarajan
 * Last modified: August 01, 2022
 */

#include <pfe_Pantograph.h>
#include <pfe_PetscData.h>
#include <random>
#include <iostream>
#include <cassert>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  PetscErrorCode ierr;
  
  // parameters
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> param_dis(1., 10.);
  pfe::Pantograph_1_Params panto_params{.tag="simple pantograph",
      .xinit=param_dis(gen), .speed=param_dis(gen),
      .m=param_dis(gen), .k=param_dis(gen), .c=param_dis(gen)};

  // create simple pantograph
  pfe::Pantograph_1 panto(panto_params);

  // problem size
  const int nTotalDofs = panto.GetTotalNumDof();
  std::vector<int> nnz = panto.GetSparsity();
  
  // Petsc data structure
  pfe::PetscData PD;
  PD.Initialize(nnz);
  auto& U = PD.solutionVEC;
  std::vector<int> indx(nTotalDofs);
  for(int i=0; i<nTotalDofs; ++i)
    indx[i] = i;
    
  // generate random dof values
  std::uniform_real_distribution<> val_dis(-1.,1.);
  std::vector<double> dofValues(nTotalDofs);
  for(int i=0; i<nTotalDofs; ++i)
    dofValues[i] = val_dis(gen);

  // Evaluate mass/stiffness/force
  const double time = 0.;
  ierr = VecSetValues(U, indx.size(), indx.data(), dofValues.data(), INSERT_VALUES); CHKERRQ(ierr);
  panto.Evaluate(time, U, PD.massMAT, PD.stiffnessMAT, PD.resVEC);

  // perturbed states
  Vec resPlus, resMinus;
  ierr = VecDuplicate(PD.resVEC, &resPlus);  CHKERRQ(ierr);
  ierr = VecSetFromOptions(resPlus);         CHKERRQ(ierr);
  ierr = VecDuplicate(PD.resVEC, &resMinus); CHKERRQ(ierr);
  ierr = VecSetFromOptions(resMinus);        CHKERRQ(ierr);
  Mat Mmat, Kmat;
  ierr = MatDuplicate(PD.massMAT, MAT_SHARE_NONZERO_PATTERN, &Mmat);      CHKERRQ(ierr);
  ierr = MatSetFromOptions(Mmat);                                         CHKERRQ(ierr);
  ierr = MatDuplicate(PD.stiffnessMAT, MAT_SHARE_NONZERO_PATTERN, &Kmat); CHKERRQ(ierr);
  ierr = MatSetFromOptions(Kmat);                                         CHKERRQ(ierr);
    
  // consistency test for force and stiffness
  const double EPS = 1.e-5;
  for(int a=0; a<nTotalDofs; ++a)
    {
      ierr = MatZeroEntries(Mmat);     CHKERRQ(ierr);
      ierr = MatZeroEntries(Kmat);     CHKERRQ(ierr);
      ierr = VecZeroEntries(resPlus);  CHKERRQ(ierr);
      ierr = VecZeroEntries(resMinus); CHKERRQ(ierr);
      
      dofValues[a] += EPS;
      double fplus = panto.ComputeStrainEnergy(time, dofValues.data());
      ierr = VecSetValues(U, indx.size(), indx.data(), dofValues.data(), INSERT_VALUES); CHKERRQ(ierr);
      panto.Evaluate(time, U, Mmat, Kmat, resPlus); 

      dofValues[a] -= 2.*EPS;
      double fminus = panto.ComputeStrainEnergy(time, dofValues.data());
      ierr = VecSetValues(U, indx.size(), indx.data(), dofValues.data(), INSERT_VALUES); CHKERRQ(ierr);
      panto.Evaluate(time, U, Mmat, Kmat, resMinus); 

      // undo increments
      dofValues[a] += EPS;

      // consistency of force
      double num_res = (fplus-fminus)/(2.*EPS);
      double resval;
      ierr = VecGetValues(PD.resVEC, 1, &a, &resval); CHKERRQ(ierr);
      assert(std::abs(resval-num_res)<10.*EPS);
      
      // consistency of stiffness
      for(int b=0; b<nTotalDofs; ++b)
	{
	  double rplus, rminus;
	  ierr = VecGetValues(resPlus,  1, &b, &rplus);  CHKERRQ(ierr);
	  ierr = VecGetValues(resMinus, 1, &b, &rminus); CHKERRQ(ierr);
	  double num_kval = (rplus-rminus)/(2.*EPS);
	  double kval;
	  ierr = MatGetValues(PD.stiffnessMAT, 1, &b, 1, &a, &kval); CHKERRQ(ierr);
	  assert(std::abs(num_kval-kval)<10.*EPS);
	}
    }

  ierr = VecDestroy(&resPlus);  CHKERRQ(ierr);
  ierr = VecDestroy(&resMinus); CHKERRQ(ierr);
  ierr = MatDestroy(&Mmat);     CHKERRQ(ierr);
  ierr = MatDestroy(&Kmat);     CHKERRQ(ierr);
  PD.Destroy();
  PetscFinalize();
}
