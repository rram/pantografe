
/** \file pfe_MovingPointLoad.h
 * \brief Defines the class pfe::MovingPointLoad
 * \author Ramsharan Rangarajan
 * Last modified: July 18, 2022
 */

#ifndef PFE_MOVING_POINT_LOAD_H
#define PFE_MOVING_POINT_LOAD_H

#include <pfe_HeavyTensionedBeam.h>

namespace pfe
{
  class MovingPointLoad
  {
  public:
    //! Constructor
    MovingPointLoad(const HeavyTensionedBeam& bm,
		    const double f0, 
		    const double x0,
		    const double c0);

    //! Destructor
    virtual ~MovingPointLoad();

    //! Disable copy and assignment
    MovingPointLoad(const MovingPointLoad&) = delete;
    MovingPointLoad operator=(const MovingPointLoad&) = delete;

    //! Main functionality: evaluate the location and magnitude of force at a prescribed time
    void GetForce(const double time, double& x, double& F) const;

    //! Main functionality: append contribution from the forcing to the load vector
    void Evaluate(const double time, Vec& F);

  private:
    const HeavyTensionedBeam& beam; //!< structure on which the load is imposed
    const double F0;                //!< Magnitude of the point load
    const double X0;                //!< location of the load at t = 0
    const double C0;                //!< Prescribed speed of the loading
    const LocalToGlobalMap& L2GMap; //!< local to global map
    std::vector<std::pair<double,double>> elmCoords; //!< Element coordinates
    std::vector<HermiteShape*> ShpFuncs;              //!< Element shape functions
  };
}

#endif
