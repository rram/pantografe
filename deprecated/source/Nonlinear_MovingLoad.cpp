// Sriramajayam

#include "Simulate.h"

namespace NM {

  // run the simulation
  void run_moving_load(const std::string sim_file, const std::string sim_id) {

    // read simulation parameters
    pfe::Dynamic_SimParams sp(sim_file,  sim_id);
    sp.outdir += "/";
    
    // Expect moving load case
    assert(sp.simulation_type=="nonlinear dynamic" && sp.panto_type==0);

    std::fstream jfile;
    jfile.open(sp.params_file, std::ios::in);
    assert(jfile.good() && jfile.is_open());
    
    // ohe + pantograph parameters
    pfe::OHESpan_ParameterPack ohe_pack;
    jfile >> ohe_pack;
    pfe::Pantograph_0_Params panto_0_params{.tag=sp.panto_tag};
    jfile >> panto_0_params;

    // read dropper slack parameter
    double dropper_slack_factor;
    jfile.seekg(0);
    auto J = nlohmann::json::parse(jfile);
    jfile.close();
    auto it = J.find("nonlinear parameters");
    assert(it!=J.end() && "Could not find nonlinear parameters in input file");
    auto& j = *it;
    j["dropper slack factor"].get_to(dropper_slack_factor);
    jfile.close();

    using OHEType    = pfe::nonlinear::MultiSpan_OHE;
    using MODEL_TYPE = pfe::nonlinear::Model_MovingLoad<OHEType>;
  
    // Create the OHE 
    OHEType ohe(0., ohe_pack, sp.num_spans, sp.nelm, dropper_slack_factor);

    // Create time integrator
    pfe::nonlinear::GenAlpha<MODEL_TYPE> TI(sp.time_integrator_param, ohe.GetTotalNumDof(), sp.timestep);

    // Create an instance of the model
    MODEL_TYPE model(ohe, panto_0_params, TI);
    const int nTotalDof = model.GetTotalNumDof();
    
    // Static solution
    model.StaticInitialize();
    model.Write({sp.outdir+"catenary-0.dat", sp.outdir+"contact-0.dat", sp.outdir+"dropper-0.dat"}, sp.outdir+"load-0.dat");

    // history
    std::fstream hfile;
    hfile.open(sp.outdir+"history.csv", std::ios::out);
    assert(hfile.good());
    hfile << "#Time, u, v, a "<< std::endl;
    std::array<double,3> ohe_uva   = model.GetLoadPointKinematics();
    hfile << 0.0 << ", " << ohe_uva[0] << ", " << ohe_uva[1] << ", " << ohe_uva[2] << std::endl;
    
    // time stepping
    for(int tstep=0; tstep<sp.num_time_steps; ++tstep)
      {
	std::cout << "Time step " << tstep << std::endl;
	model.Advance();

	std::string tstr = std::to_string(tstep+1)+".dat";
	model.Write({sp.outdir+"catenary-"+tstr, sp.outdir+"contact-"+tstr, sp.outdir+"dropper-"+tstr}, sp.outdir+"load-"+tstr);
	ohe_uva   = model.GetLoadPointKinematics();
	hfile << model.GetTime() << ", " << ohe_uva[0] << ", " << ohe_uva[1] << ", " << ohe_uva[2] << std::endl;
      }
    hfile.close();
      
    // Clean up
    TI.Destroy();
    ohe.Destroy();
    model.Destroy();
    return;
  }

}
