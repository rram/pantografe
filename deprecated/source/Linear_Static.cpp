// Sriramajayam

#include "Simulate.h"

namespace LM {

  // run a static simulation
  void run_static(const std::string sim_file, const std::string sim_id) {

    // read simulation parameters
    pfe::Static_SimParams sp(sim_file, sim_id);
    assert(sp.simulation_type=="linear static");
    sp.outdir += "/";
    
    // OHE parameters
    pfe::OHESpan_ParameterPack ohe_pack;
    std::fstream jfile;
    jfile.open(sp.params_file, std::ios::in);
    assert(jfile.good() && jfile.is_open());
    jfile >> ohe_pack;
    jfile.close();

    // Create the OHE span
    pfe::linear::SingleSpan_OHE ohe(0.0, ohe_pack, sp.nelm);

    // Create an instance of the model
    pfe::linear::Model_Static_OHE model(ohe);
  
    // Compute the static deflection of the OHE (unadjusted dropper lengths)
    const int nTotalDof = model.GetTotalNumDof();
    std::vector<double> displacements(nTotalDof);
    model.Solve(displacements.data());
  
    // Dropper length adjustment to match target sag if requested
    if(sp.num_length_iters>0)
      {
	// Access the sag at the dropper nodes with unadjusted dropper lengths
	const auto dropper_dofs = ohe.GetDropperDofs(0);
	std::vector<double> orig_dropper_sag(ohe_pack.dropperSchedule.nDroppers);
	for(int d=0; d<ohe_pack.dropperSchedule.nDroppers; ++d)
	  orig_dropper_sag[d] = displacements[dropper_dofs[d].second];

	// iterative dropper length adjustment
	std::cout << std::endl << "Performing dropper length adjustment " << std::endl;
	model.AdjustDropperLengths(0, sp.num_length_iters);
	model.Solve(displacements.data());
  
	// write adjusted length & sag to file
	jfile.open(sp.outdir+"/dropper_adjustments.csv", std::ios::out);
	assert(jfile.good());
	jfile <<"#dropper#, x_dropper, L_nominal,  L_adjusted, target_sag, unadjusted_sag, adjusted_sag" << std::endl;
	for(int d=0; d<ohe_pack.dropperSchedule.nDroppers; ++d)
	  jfile << d << ", " << ohe_pack.dropperSchedule.coordinates[d] << ", "                                           // 1, 2
		<< ohe_pack.dropperSchedule.nominal_lengths[d] << ", " << ohe.GetDroppers()[d]->GetLength() << ", "      // 3, 4
		<< ohe_pack.dropperSchedule.target_sag[d] << ", "                                                        // 5
		<< orig_dropper_sag[d] << ", " << displacements[dropper_dofs[d].second] << std::endl;                    // 6, 7
	jfile.close();
      }

    // visualize deflection
    model.Write(displacements.data(), sp.outdir+"/catenary.dat", sp.outdir+"/contact.dat", sp.outdir+"/droppers.dat");
  
    // Compute the stiffness distribution
    const int nNodes = ohe.ContactWire().GetNumNodes();
    const auto& coordinates = ohe.ContactWire().GetCoordinates();
    std::vector<double> stiffness(nNodes);
    model.ComputeTransverseStiffnessDistribution(0, displacements.data(), sp.force_val, stiffness.data());
    jfile.open(sp.outdir+"/stiffness.dat", std::ios::out);
    for(int a=0; a<nNodes; ++a)
      jfile << coordinates[a] << " " << stiffness[a] << std::endl;
    jfile.close();
  
    // clean up
    ohe.Destroy();
    model.Destroy();
    return;
  }
}
