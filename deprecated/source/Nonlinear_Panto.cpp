// Sriramajayam

#include "Simulate.h"

namespace NM {

  // run the simulation
  void run_panto(const std::string sim_file, const std::string sim_id) {

    // read simulation parameters
    pfe::Dynamic_SimParams sp(sim_file,  sim_id);
    sp.outdir += "/";
    
    // Expect pantograph case
    assert(sp.simulation_type=="nonlinear dynamic");
    assert(sp.panto_type==1 || sp.panto_type==3);

    std::fstream jfile;
    jfile.open(sp.params_file, std::ios::in);
    assert(jfile.good() && jfile.is_open());
    
    // ohe + pantograph parameters
    pfe::OHESpan_ParameterPack ohe_pack;
    jfile >> ohe_pack;
    pfe::Pantograph_1_Params panto_1_params{.tag=sp.panto_tag};
    pfe::Pantograph_3_Params panto_3_params{.tag=sp.panto_tag};
    if(sp.panto_type==1)
      jfile >> panto_1_params;
    else
      jfile >> panto_3_params;

    // read nonlinear parameters
    double dropper_slack_factor;
    double contact_spring_stiffness;
    double contact_spring_slack_factor;
    jfile.seekg(0);
    auto J = nlohmann::json::parse(jfile);
    jfile.close();
    auto it = J.find("nonlinear parameters");
    assert(it!=J.end() && "Could not find nonlinear parameters in input file");
    auto& j = *it;
    j["dropper slack factor"].get_to(dropper_slack_factor);
    j["contact spring stiffness"].get_to(contact_spring_stiffness);
    j["contact spring slack factor"].get_to(contact_spring_slack_factor);
    jfile.close();

    // aliases
    using OHE_TYPE   = pfe::nonlinear::MultiSpan_OHE;
    using MODEL_TYPE = pfe::nonlinear::Model_MultiSpan_OHE_Panto;

    // Create OHE + pantograph
    OHE_TYPE ohe(0.0, ohe_pack, sp.num_spans, sp.nelm, dropper_slack_factor);
    std::unique_ptr<pfe::Pantograph> panto;
    if(sp.panto_type==1)
      panto = std::make_unique<pfe::Pantograph_1>(panto_1_params);
    else
      panto = std::make_unique<pfe::Pantograph_3>(panto_3_params);

    // Contact spring
    std::unique_ptr<pfe::ContactSpring> contact_spring;
    if(sp.contact_type=="persistent")
      contact_spring = std::make_unique<pfe::LinearContactSpring>(contact_spring_stiffness);
    else
      contact_spring = std::make_unique<pfe::NonlinearContactSpring>(contact_spring_stiffness, contact_spring_slack_factor);

    // time integrator
    const int nTotalDof = ohe.GetTotalNumDof()+panto->GetTotalNumDof();
    std::unique_ptr<pfe::nonlinear::TimeIntegrator<MODEL_TYPE>> TI;
    if(sp.time_integrator_type=="generalized alpha")
      TI = std::make_unique<pfe::nonlinear::GenAlpha<MODEL_TYPE>>(sp.time_integrator_param, nTotalDof, sp.timestep);
    else if(sp.time_integrator_type=="newmark")
      TI = std::make_unique<pfe::nonlinear::Newmark<MODEL_TYPE>>(nTotalDof, sp.timestep);
    else
      assert(false && "Unexpected time integrator requested");

    // create the model
    MODEL_TYPE model(ohe, *panto, *contact_spring, *TI);

    // static initialization
    model.StaticInitialize();
    model.Write({sp.outdir+"cat-0.dat", sp.outdir+"con-0.dat", sp.outdir+"drop-0.dat"}, sp.outdir+"panto-0.dat");
    std::cout<< "Contact gap: " << model.GetContactGap() << std::endl;
 
    // History at the contact point
    std::fstream hfile;
    hfile.open(sp.outdir+"history.dot", std::ios::out);
    assert(hfile.good());
    hfile << "#Time, contact force, U_ohe, V_ohe, A_ohe, U_panto, V_panto, A_panto "<< std::endl;
    std::array<double,3> ohe_uva   = model.GetOHEContactPointKinematics();
    std::array<double,3> panto_uva = model.GetPantographContactPointKinematics();
    hfile << 0.0 << " " << model.GetContactForce() << " "
	  << ohe_uva[0] << " " << ohe_uva[1] << " " << ohe_uva[2] << " "
	  << panto_uva[0] << " " << panto_uva[1] << " " << panto_uva[2] << std::endl;

    // time stepping
    std::cout << "Time step (of " << sp.num_time_steps << ") " << std::endl;
    for(int tstep=1;  tstep<=sp.num_time_steps; ++tstep)
      {
	std::cout << tstep << std::endl;
	model.Advance();
	const std::string str = std::to_string(tstep)+".dat";
	model.Write({sp.outdir+"cat-"+str, sp.outdir+"con-"+str, sp.outdir+"drop-"+str}, sp.outdir+"panto-"+str);
      
	const double time =  model.GetTime();
	ohe_uva   = model.GetOHEContactPointKinematics();
	panto_uva = model.GetPantographContactPointKinematics();
	hfile << model.GetTime() << " " << model.GetContactForce() << " "
	      << ohe_uva[0]       << " " << ohe_uva[1]   << " " << ohe_uva[2] << " "
	      << panto_uva[0]     << " " << panto_uva[1] << " " << panto_uva[2] << std::endl;
      }
    hfile.close();

    // clean up
    ohe.Destroy();
    TI->Destroy();
    model.Destroy();
    return;
  }
}

