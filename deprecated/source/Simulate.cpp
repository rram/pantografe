// Sriramajayam

#include "Simulate.h"

void run(const std::string sim_file, const std::string sim_id) {

  // check the simulation type and pantograph type
  std::string simulation_type;
  int panto_type;
  
  assert(std::filesystem::path(sim_file).extension()==".json" && "Expected simulation options file to have .json extension");
  std::fstream jfile;
  jfile.open(sim_file, std::ios::in);
  assert((jfile.good() && jfile.is_open()) && "Could not open simulation options file");
  auto J = nlohmann::json::parse(jfile);
  jfile.close();
  
  auto it = J.find(sim_id);
  assert(it!=J.end() && "Could not find simulation type in input file");
  auto& j = *it;

  // simulation type
  j["simulation type"].get_to(simulation_type);
  assert(simulation_type=="linear static" || simulation_type=="linear dynamic" ||
	 simulation_type=="nonlinear static" || simulation_type=="nonlinear dynamic");

  // static simulation
  if(simulation_type=="linear static")
    LM::run_static(sim_file, sim_id);
  else if(simulation_type=="nonlinear static")
    NM::run_static(sim_file, sim_id);
  else
    {
      // pantograph type
      j["pantograph type"].get_to(panto_type);
      assert(panto_type==0 || panto_type==1 || panto_type==3);

      // dynamic simulation
      if(simulation_type=="linear dynamic") {
	if(panto_type==0)
	  LM::run_moving_load(sim_file, sim_id);
	else
	  LM::run_panto(sim_file, sim_id);
      }
      else if(simulation_type=="nonlinear dynamic"){
	if(panto_type==0)
	  NM::run_moving_load(sim_file, sim_id);
	else
	  NM::run_panto(sim_file, sim_id);
      }
    }

  return;
}
