// Sriramajayam

#include "Simulate.h"

namespace LM {
  
  // run the simulation
  void run_panto(const std::string sim_file, const std::string sim_id) {

    // read simulation parameters
    pfe::Dynamic_SimParams sp(sim_file,  sim_id);
    
    // Expect OHE-Panto case
    assert(sp.simulation_type=="linear dynamic");
    assert(sp.panto_type==1 || sp.panto_type==3); 

    std::fstream jfile;
    jfile.open(sp.params_file, std::ios::in);
    assert(jfile.good() && jfile.is_open());

    // ohe + panto parameters
    pfe::OHESpan_ParameterPack ohe_pack;
    jfile >> ohe_pack;
    pfe::Pantograph_1_Params panto_1_params{.tag=sp.panto_tag};
    pfe::Pantograph_3_Params panto_3_params{.tag=sp.panto_tag};
    if(sp.panto_type==1)
      jfile >> panto_1_params;
    else if(sp.panto_type==3)
      jfile >> panto_3_params;
    jfile.close();
    
    // Create OHE + pantograph
    pfe::linear::MultiSpan_OHE ohe(0.0, ohe_pack, sp.num_spans, sp.nelm);
    std::unique_ptr<pfe::Pantograph> panto;
    if(sp.panto_type==1)
      panto = std::make_unique<pfe::Pantograph_1>(panto_1_params);
    else if(sp.panto_type==3)
      panto = std::make_unique<pfe::Pantograph_3>(panto_3_params);

    // Time integrator
    const int nTotalDof = ohe.GetTotalNumDof()+panto->GetTotalNumDof();
    std::unique_ptr<pfe::TimeIntegrator_with_Contact> TI;
    if(sp.time_integrator_type=="generalized alpha")
      TI = std::make_unique<pfe::GenAlpha_with_Contact>(sp.time_integrator_param, nTotalDof, sp.timestep);
    else if(sp.time_integrator_type=="hht alpha")
      TI = std::make_unique<pfe::HHTAlpha_with_Contact>(sp.time_integrator_param, nTotalDof, sp.timestep);
    else if(sp.time_integrator_type=="newmark")
      TI = std::make_unique<pfe::Newmark_with_Contact>(nTotalDof, sp.timestep);
    else
      {
	assert(false && "Expected time integrator to be one of 'generalized alpha', 'hht alpha' or 'newmark'"); 
      }
  
    // create the model
    pfe::linear::Model_MultiSpanOHE_Panto model(ohe, *panto, *TI, {});

    // Static initialization
    model.StaticInitialize();
  
    // Visualize
    model.Write({sp.outdir+"/cat-0.dat", sp.outdir+"/con-0.dat", sp.outdir+"/dro-0.dat" }, sp.outdir+"/panto-0.dat");

    // History at the contact point
    std::fstream hfile;
    hfile.open(sp.outdir+"/history.csv", std::ios::out);
    assert(hfile.good());
    hfile << "#Time, contact force, U_ohe, V_ohe, A_ohe, U_panto, V_panto, A_panto"<< std::endl;
    std::array<double,3> ohe_uva   = model.GetOHEContactPointKinematics();
    std::array<double,3> panto_uva = model.GetPantographContactPointKinematics();
    hfile << 0.0 << ", " << model.GetContactForce() << ", "
	  << ohe_uva[0] << ", " << ohe_uva[1] << ", " << ohe_uva[2] << ", "
	  << panto_uva[0] << ", " << panto_uva[1] << ", " << panto_uva[2] << std::endl;

    // Moving pantograph
    std::cout << "Time step (of " << sp.num_time_steps << ") " << std::endl;
    for(int tstep=0; tstep<sp.num_time_steps; ++tstep)
      {
	std::cout << tstep+1 << std::endl;
	model.Advance();

	const std::string str = std::to_string(tstep+1)+".dat";
	model.Write({sp.outdir+"/cat-"+str, sp.outdir+"/con-"+str, sp.outdir+"/dro-"+str}, sp.outdir+"/panto-"+str);
      
	const double time =  model.GetTime();
	ohe_uva   = model.GetOHEContactPointKinematics();
	panto_uva = model.GetPantographContactPointKinematics();
	hfile << model.GetTime() << ", " << model.GetContactForce() << ", "
	      << ohe_uva[0]       << ", " << ohe_uva[1]   << ", " << ohe_uva[2] << ", "
	      << panto_uva[0]     << ", " << panto_uva[1] << ", " << panto_uva[2] << std::endl;
      }
    hfile.close();
  
    TI->Destroy();
    ohe.Destroy();
    model.Destroy();
    return;  
  }

}
