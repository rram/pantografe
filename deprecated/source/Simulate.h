// Sriramajayam

#pragma once

#include <pfe_SimParams.h>

// linear, static
#include <pfe_linear_Model_Static_OHE.h>

// linear, moving load
#include <pfe_linear_Model_MovingLoad.h>
#include <pfe_GenAlpha.h>
#include <pfe_HHTAlpha.h>
#include <pfe_Newmark.h>

// linear, OHE-panto
#include <pfe_linear_Model_OHE_Panto.h>
#include <pfe_GenAlpha_with_Contact.h>
#include <pfe_HHTAlpha_with_Contact.h>
#include <pfe_Newmark_with_Contact.h>

// nonlinear, static
#include <pfe_nonlinear_Model_Static_OHE.h>

// nonlinear, moving load
#include <pfe_nonlinear_Model_MovingLoad.h>

// nonlinear, OHE-panto
#include <pfe_nonlinear_Model_OHE_Panto.h>
#include <pfe_nonlinear_Newmark.h>
#include <pfe_nonlinear_GenAlpha.h>

// json
#include <pfe_json.hpp>

void run(const std::string sim_file, const std::string sim_id);
    
// run a linear simulation
namespace LM {

  // run the simulation
  void run_static(const std::string sim_file, const std::string sim_id);
  
  // run the simulation
  void run_moving_load(const std::string sim_file, const std::string sim_id);

  // run the simulation
  void run_panto(const std::string sim_file, const std::string sim_id);    
}

// run a nonlinear simulation
namespace NM {

  // run the simulation
  void run_static(const std::string sim_file, const std::string sim_id);
  
  // run the simulation
  void run_moving_load(const std::string sim_file, const std::string sim_id);

  // run the simulation
  void run_panto(const std::string sim_file, const std::string sim_id);
}

