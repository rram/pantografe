// Sriramajayam

#include "Simulate.h"

namespace NM {

  // run a static simulation
  void run_static(const std::string sim_file, const std::string sim_id) {

    // read simulation parameters
    pfe::Static_SimParams sp(sim_file, sim_id);
    sp.outdir += "/";
    assert(sp.simulation_type=="nonlinear static");
    
    // OHE parameters
    pfe::OHESpan_ParameterPack ohe_pack;
    std::fstream jfile;
    jfile.open(sp.params_file, std::ios::in);
    assert(jfile.good() && jfile.is_open());
    jfile >> ohe_pack;
    
    // read dropper slack parameter
    double dropper_slack_factor;
    jfile.seekg(0);
    auto J = nlohmann::json::parse(jfile);
    jfile.close();
    auto it = J.find("nonlinear parameters");
    assert(it!=J.end() && "Could not find nonlinear parameters in input file");
    auto& j = *it;
    j["dropper slack factor"].get_to(dropper_slack_factor);
    jfile.close();

    // aliases
    using OHE_TYPE   = pfe::nonlinear::SingleSpan_OHE;
    using MODEL_TYPE = pfe::nonlinear::Model_Static_SingleSpan_OHE;

    // Create a single span
    OHE_TYPE ohe_span(0., ohe_pack, dropper_slack_factor, sp.nelm);
    
    // Create an instance of the static model
    MODEL_TYPE model(ohe_span);
    
    // Adjust dropper lengths to achieve sag over specfic # iterations
    model.AdjustDropperLengths(0, sp.num_length_iters);

    // Solve at the adjusted dropper lengths
    const int nTotalDof = model.GetTotalNumDof();
    std::vector<double> displacements(nTotalDof);
    model.Solve(displacements.data()); 
    model.Write(displacements.data(), sp.outdir+"catenary.dat", sp.outdir+"contact.dat", sp.outdir+"droppers.dat");

    // Compare target and achieved displacements
    std::fstream pfile;
    const int nDroppers = ohe_pack.dropperSchedule.nDroppers;
    const auto dropper_dofs = ohe_span.GetDropperDofs(0);
    pfile.open(sp.outdir+"dropper-sag.csv", std::ios::out);
    assert(pfile.good());
    pfile <<"#dropper-index, x-dropper, target sag, realized sag, nominal length, adjusted length" << std::endl;
    for(int d=0; d<nDroppers; ++d)
      pfile << d <<", " << ohe_pack.dropperSchedule.coordinates[d] << ", "
	    << ohe_pack.dropperSchedule.target_sag[d] << ", " << displacements[dropper_dofs[d].second] << ", "
	    << ohe_pack.dropperSchedule.nominal_lengths[d] << ", " << ohe_span.GetDroppers()[d]->GetLength() << std::endl;
    pfile.close();
  
    // Compute the transverse stiffness
    const auto& coordinates = ohe_span.ContactWire().GetCoordinates();
    const int nNodes = ohe_span.ContactWire().GetNumNodes();
    std::vector<double> stiffness(nNodes);
    std::cout << "Computing transverse stiffness: " << std::endl;
    model.ComputeTransverseStiffnessDistribution(0, displacements.data(), 100., stiffness.data());
    pfile.open(sp.outdir+"stiffness.csv", std::ios::out);
    pfile << "#X, stiffness" << std::endl;
    for(int a=0; a<nNodes; ++a)
      pfile << coordinates[a] << ", " << stiffness[a] << std::endl;
    pfile.close();
  
    // clean up
    ohe_span.Destroy();
    model.Destroy();
    return;
  }
}
    

