
/** \file StaticBenchmark_ContactWireDeflection.cpp
 * \brief Model test to verify prediction of static deflection in a single span
 * \author Ramsharan Rangarajan
 * Last modified: December 14, 2022
 */

// Input file   : benchmark_params.json containing model parameters.
// Output files : contact.dat containing deflected state the contact wire
// Comparison   : benchmark_contact.data


#include <pfe_OHESpan.h>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Benchmark study parameters
  pfe::OHESpan_ParameterPack ohe_pack;
  std::fstream jfile;
  jfile.open("benchmark_params.json", std::ios::in);
  assert(jfile.is_open());
  jfile >> ohe_pack;
  jfile.close();

  // Create a single span
  const int nElements = 20;
  auto ohe_span = new pfe::OHESpan(0., ohe_pack, nElements);

  // Get the dropper nodes
  auto dropper_nodes = ohe_span->GetDropperNodes();

  std::vector<std::pair<int,double>> dropper_nodal_disp(ohe_pack.dropperSchedule.nDroppers);
  for(int d=0; d<ohe_pack.dropperSchedule.nDroppers; ++d)
    dropper_nodal_disp[d] = std::make_pair(dropper_nodes[d], ohe_pack.dropperSchedule.target_sag[d]);
  
  // Compute contact wire configuration with given sag
  auto& contact_wire = ohe_span->ContactWire();
  const int nDofs = contact_wire.GetLocalToGlobalMap().GetTotalNumDof();
  std::vector<double> displacements(nDofs);
  contact_wire.StaticSolve(dropper_nodal_disp, displacements.data());

  // Visualize
  contact_wire.Write(displacements.data(), "contact.dat");
  
  // Clean up
  delete ohe_span;
  PetscFinalize();
}
