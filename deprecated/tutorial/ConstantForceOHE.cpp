
/** \file ConstantForceOHE.h
 * \brief Benchmark example testing a moving point load on an OHE
 * \author Ramsharan Rangarajan
 * Last modified: December 14, 2022
 */

/* Input:
   --cable json_file
   --panto json_file
   --sim   json_file
   -d      output directory
*/

/* Output: 
   catenary*.dat catenary cable configuration indexed by time step, columns: x w w-dot w-ddot 
   contact*.dat  contact wire configuration indexed by time step,   columns: x w w-dot w-ddot
   dropper*.dat  dropper cable configurations indexed by time step, columns: x w w-dot w-ddot
   load*.dat     pantograph configuration, indexed by time step,    columns: x w
*/

#include <pfe_OHE_Panto0.h>
#include <CLI/CLI.hpp>
#include <nlohmann/json.hpp>
#include <iostream>
#include <fstream>

// simulation parameter
void read_simulation_parameters(const std::string json_file, const std::string tag,
				int& nelm, double& timestep, int& num_time_steps);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL); CHKERRQ(ierr);

  // read command line options
  std::string ohe_file;        // ohe parameters
  std::string panto_file;      // panto parameters
  std::string sim_file;        // simulation options
  std::string out_directory;   // output directory
  CLI::App app;
  app.add_option("--ohe",   ohe_file,      "json file with ohe parameters")       ->required()->check(CLI::ExistingFile);
  app.add_option("--panto", panto_file,    "json file with pantograph parameters")->required()->check(CLI::ExistingFile);
  app.add_option("--sim",   sim_file,      "json file with simulation parameters")->required()->check(CLI::ExistingFile);
  app.add_option("-d",      out_directory, "output directory")->default_val("./") ->check(CLI::ExistingDirectory);
  CLI11_PARSE(app, argc, argv);

  // read simulation parameters 
  int         nelm;            // number of elements between successive droppers
  double      timestep;        // time step
  int         num_time_steps;  // number of time steps
  read_simulation_parameters(sim_file, "simulation parameters", nelm, timestep, num_time_steps);
  
  // OHE parameters
  pfe::OHESpan_ParameterPack ohe_pack;
  std::fstream jfile;
  jfile.open(ohe_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());
  jfile >> ohe_pack;
  jfile.close();

  // pantograph parameters
  pfe::Pantograph_0_Params panto_params{.tag="pantograph 0"};
  jfile.open(panto_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());
  jfile >> panto_params;
  jfile.close();
  
  // Create the OHE span
  auto ohe = new pfe::OHESpan(0., ohe_pack, nelm);
  
  // Create an instance of the model
  auto model = new pfe::OHE_Panto0(*ohe, panto_params, timestep);
  const int nTotalDof = model->GetTotalNumDof();
    
  // Static solution
  const double tstart = 0.;
  model->StaticInitialize(tstart);

  // visualize
  model->Write({out_directory+"/catenary-0.dat", out_directory+"/contact-0.dat", out_directory+"/dropper-0.dat"},  out_directory+"/load-0.dat");

  // Moving pantograph
  std::cout << "Time step (of " << num_time_steps << "): " << std::endl;
  for(int tstep=0; tstep<num_time_steps; ++tstep)
    {
      std::cout << tstep+1 << std::endl;
      
      model->Advance();
      std::string tstr = std::to_string(tstep+1)+".dat";
      model->Write({out_directory+"/catenary-"+tstr, out_directory+"/contact-"+tstr, out_directory+"/dropper-"+tstr}, out_directory+"/load-"+tstr);
    }
  
  // Clean up
  delete model;
  delete ohe;
  PetscFinalize();
}


// simulation parameter
void read_simulation_parameters(const std::string json_file, const std::string tag,
				int& nelm, double& timestep, int& num_time_steps)
{
  assert(std::filesystem::path(json_file).extension()==".json");
  std::fstream jfile;
  jfile.open(json_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());

  // get the data
  auto J = nlohmann::json::parse(jfile);
  jfile.close();

  // read options
  auto it = J.find(tag);
  assert(it!=J.end());
  auto& j = *it;
  j["num elements"].get_to(nelm); 
  j["time step"].get_to(timestep); 
  j["num time steps"].get_to(num_time_steps);

  assert(nelm>0 && timestep>0 && num_time_steps>0);

  // done
  return;
}
