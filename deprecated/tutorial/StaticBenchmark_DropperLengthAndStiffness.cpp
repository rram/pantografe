
/** \file StaticBenchmark_DropperLengthAndStiffness.cpp
 * \brief Model test to verify prediction of static deflection in a single span
 * \author Ramsharan Rangarajan
 * Last modified: December 14, 2022
 */

// Input file   : benchmark_params.json containing model parameters.
// Output files : dropper-sag.dat and stiffness.dat
// Comparison   : benchmark_sag.data and benchmark-Frc100.data


#include <pfe_Static_OHE.h>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Benchmark study parameters
  pfe::OHESpan_ParameterPack ohe_pack;
  std::fstream jfile;
  jfile.open("benchmark_params.json", std::ios::in);
  assert(jfile.is_open());
  jfile >> ohe_pack;
  jfile.close();

  // Create a single span
  const int nElements = 20;
  auto ohe_span = new pfe::OHESpan(0., ohe_pack, nElements);

  // Create an instance of the static model
  auto model = new pfe::Static_OHE(*ohe_span);

  // Adjust dropper lengths to achieve sag over 20 iterations
  model->AdjustDropperLengths(20);

  // Solve at the adjusted dropper lengths
  const int nTotalDof = model->GetTotalNumDof();
  std::vector<double> displacements(nTotalDof);
  model->Solve(displacements.data());

  // Access the achieved sag at the dropper nodes
  std::vector<double> dropper_catenary_disp(ohe_pack.dropperSchedule.nDroppers);
  std::vector<double> dropper_contact_disp(ohe_pack.dropperSchedule.nDroppers);
  ohe_span->GetDropperDisplacements(displacements.data(),
				    dropper_catenary_disp.data(), dropper_contact_disp.data());

  // Compare target and achieved displacements
  std::fstream pfile;
  pfile.open((char*)"dropper-sag.dat", std::ios::out);
  assert(pfile.good());
  pfile <<"# dropper-index x-dropper, target sag, realized sag, nominal length, adjusted length, catenary sag";
  for(int d=0; d<ohe_pack.dropperSchedule.nDroppers; ++d)
    pfile<<"\n"<<d<<" "<<ohe_pack.dropperSchedule.coordinates[d]<<" "
	 <<ohe_pack.dropperSchedule.target_sag[d]<<" "<<dropper_contact_disp[d]<<" "
	 <<ohe_pack.dropperSchedule.nominal_lengths[d]<<" "<<ohe_span->GetDroppers()[d]->GetLength()<<" "
	 <<dropper_catenary_disp[d];
  pfile.close();


  // Compute the transverse stiffness
  const auto& coordinates = ohe_span->ContactWire().GetCoordinates();
  const int nNodes = ohe_span->ContactWire().GetNumNodes();
  std::vector<double> stiffness(nNodes);
  model->ComputeTransverseStiffnessDistribution(displacements.data(), 100., stiffness.data());
  pfile.open((char*)"stiffness.dat", std::ios::out);
  for(int a=0; a<nNodes; ++a)
    pfile << coordinates[a]<<" "<<stiffness[a]<<"\n";
  pfile.close();
  
  // Clean up
  delete ohe_span;
  delete model;
  PetscFinalize();
}
