
/** \file ConstantForceCable.cpp
 * \brief Benchmark example testing a moving point load on an overhead cable system
 * \author Ramsharan Rangarajan
 * Last modified: Feb 26, 2023
 */

/* Input:
   --cable json_file
   --panto json_file
   --sim   json_file
   -d      output directory
*/

/* Output: 
   cable*.dat cable configuration indexed by time step, columns: x w w-dot w-ddot 
   load*.dat" load location indexed by time step,       columns: x w
*/

#include <pfe_Cable_Panto0.h>
#include <CLI/CLI.hpp>
#include <nlohmann/json.hpp>
#include <iostream>
#include <fstream>

// simulation parameter
void read_simulation_parameters(const std::string json_file, const std::string tag,
				int& nelm, double& timestep, int& num_time_steps);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL); CHKERRQ(ierr);

  // read command line options
  std::string cable_file;        // cable parameters
  std::string panto_file;      // panto parameters
  std::string sim_file;        // simulation options
  std::string out_directory;   // output directory
  CLI::App app;
  app.add_option("--cable", cable_file,    "json file with ohe parameters")       ->required()->check(CLI::ExistingFile);
  app.add_option("--panto", panto_file,    "json file with pantograph parameters")->required()->check(CLI::ExistingFile);
  app.add_option("--sim",   sim_file,      "json file with simulation parameters")->required()->check(CLI::ExistingFile);
  app.add_option("-d",      out_directory, "output directory")->default_val("./") ->check(CLI::ExistingDirectory);
  CLI11_PARSE(app, argc, argv);

  // read simulation parameters 
  int         nelm;            // number of elements between successive droppers
  double      timestep;        // time step
  int         num_time_steps;  // number of time steps
  read_simulation_parameters(sim_file, "simulation parameters", nelm, timestep, num_time_steps);

  // cable parameters
  pfe::ContactWireParams wireParams{.tag="cable"};
  std::fstream jfile;
  jfile.open(cable_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());
  jfile >> wireParams;
  
  // cable suspension spring
  pfe::SuspensionSpringParams spring_params{.tag="suspension spring"};
  jfile >> spring_params;
  jfile.close();

  // Create the contact wire
  auto cable = new pfe::HeavyTensionedBeam(wireParams, nelm);
  const int nNodes = cable->GetNumNodes();
  cable->AddSuspensionSpring(spring_params, 0);
  cable->AddSuspensionSpring(spring_params, nNodes-1);

  // pantograph parameters
  pfe::Pantograph_0_Params panto_params{.tag="pantograph 0"};
  jfile.open(panto_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());
  jfile >> panto_params;
  jfile.close();

  // Create an instance of the model
  auto model = new pfe::Cable_Panto0(*cable, panto_params, timestep);
  const int nTotalDof = model->GetTotalNumDof();

  // Static initialization
  const double tstart = 0.;
  model->StaticInitialize(tstart);

  // Visualize
  model->Write({out_directory+"/cable-0.dat"}, out_directory+"/load-0.dat");

  // Moving pantograph
  std::cout << "Time step (of " << num_time_steps << ") " << std::endl;
  for(int tstep=0; tstep<num_time_steps; ++tstep)
    {
      std::cout << tstep+1 << std::endl;
      model->Advance();
      model->Write({out_directory+"/cable-"+std::to_string(tstep+1)+".dat"},
		   out_directory+"/load-"+std::to_string(tstep+1)+".dat");
    }
     
  // Clean up
  delete cable;
  delete model;
  PetscFinalize();
}


// simulation parameter
void read_simulation_parameters(const std::string json_file, const std::string tag,
				int& nelm, double& timestep, int& num_time_steps)
{
  assert(std::filesystem::path(json_file).extension()==".json");
  std::fstream jfile;
  jfile.open(json_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());

  // get the data
  auto J = nlohmann::json::parse(jfile);
  jfile.close();

  // read options
  auto it = J.find(tag);
  assert(it!=J.end());
  auto& j = *it;
  j["num elements"].get_to(nelm); 
  j["time step"].get_to(timestep); 
  j["num time steps"].get_to(num_time_steps);

  assert(nelm>0 && timestep>0 && num_time_steps>0);

  // done
  return;
}
