
/** \file StaticBenchmark_Deflection.cpp
 * \brief Model test to verify prediction of static deflection in a single span
 * \author Ramsharan Rangarajan
 * Last modified: December 14, 2022
 */

// Input file   : benchmark_params.json containing model parameters.
// Output files : catenary.dat, contact.dat, droppers.dat containing deflected states of catenary, conact and dropper cables
// Comparison   : none

#include <pfe_Static_OHE.h>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Benchmark study parameters
  pfe::OHESpan_ParameterPack ohe_pack;
  std::fstream jfile;
  jfile.open("benchmark_params.json", std::ios::in);
  assert(jfile.is_open());
  jfile >> ohe_pack;
  jfile.close();

  // Create a single span
  const int nElements = 20;
  auto ohe_span = new pfe::OHESpan(0., ohe_pack, nElements);

  // Create an instance of the model
  auto model = new pfe::Static_OHE(*ohe_span);

  // Solve
  const int nTotalDof = model->GetTotalNumDof();
  std::vector<double> displacements(nTotalDof);
  model->Solve(displacements.data());

  // Visualize
  model->Write(displacements.data(), "catenary.dat", "contact.dat", "droppers.dat");
  
  // Clean up
  delete model;
  delete ohe_span;
  PetscFinalize();
}
