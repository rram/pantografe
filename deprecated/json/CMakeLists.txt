
# add library
add_library(pfe_json INTERFACE)

# headers
target_include_directories(pfe_json INTERFACE
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)

# Add required flags
target_compile_features(pfe_json INTERFACE ${pfe_COMPILE_FEATURES})

install(FILES
  json.hpp
  DESTINATION ${PROJECT_NAME}/include)
