
/** \file pfe_CatenarySupportedContactWire_statice.cpp
 * \brief Implements the class pfe::CatenarySupportedContactWire
 * \author Ramsharan Rangarajan
 * Last modified: August 19, 2022
 */

#include <pfe_CatenarySupportedContactWire.h>

namespace pfe
{
  // Main functionality: solve
  void CatenarySupportedContactWire::StaticSolve(const double time,
						 const std::vector<std::pair<int,double>>& catenary_bcs,
						 const std::vector<std::pair<int,double>>& contact_bcs,
						 double* displacements)
  {
    assert(isSetup==true);
    
    // Zero
    PetscErrorCode ierr;
    ierr = MatZeroEntries(PD.stiffnessMAT); CHKERRV(ierr);
    ierr = VecZeroEntries(PD.resVEC);       CHKERRV(ierr);

    // Assemble contributions to (K, F) from the catenary cable, contact wire, dropper cables and suspension springs
    Evaluate(time, displacements, PD.massMAT, PD.stiffnessMAT, PD.resVEC);

    // Preserve the non-zero structure during manipulations
    ierr = MatSetOption(PD.stiffnessMAT, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);

    // Set Dirichlet BCs in the matrix-vector system
    std::vector<int> boundary{};
    std::vector<double> bvalues{};
    
    // catenary
    for(auto& bc:catenary_bcs)
      {
	assert(bc.first>=0 && bc.first<nNodes);
	boundary.push_back( bc.first );
	bvalues.push_back( bc.second );
      }
    
    // contact wire
    for(auto& bc:contact_bcs)
      {
	assert(bc.first>=0 && bc.first<nNodes);
	boundary.push_back( nCatenaryCableDofs+bc.first );
	bvalues.push_back( bc.second );
      }

    // Set
    PD.SetDirichletBCs(boundary, bvalues);
    
    // Solve
    linSolver.SetOperator(PD.stiffnessMAT);
    linSolver.Solve(PD.resVEC, PD.solutionVEC);

    // Get the solution
    double* solution;
    ierr = VecGetArray(PD.solutionVEC, &solution); CHKERRV(ierr);
    for(int a=0; a<nTotalDof; ++a)
      displacements[a] = solution[a];
    ierr = VecRestoreArray(PD.solutionVEC, &solution); CHKERRV(ierr);

    // done
    return;
  }


  // Main functionalty: match a prescribed value of sag
  void CatenarySupportedContactWire::AdjustDropperLengths(const double time,
							  const std::vector<std::pair<int,double>>& catenary_bcs,
							  const std::vector<std::pair<int,double>>& contact_bcs,
							  const int nIters)
  {
    assert(isSetup==true);
    assert(pantographs.empty());
    
    const int nDroppers = static_cast<int>(droppers.size());
    assert(static_cast<int>(dropperSchedule.target_sag.size())==nDroppers);
    assert(nDroppers>0);
    std::vector<double> displacements(nTotalDof);
    std::vector<double> dropper_contact_disp(nDroppers), dropper_catenary_disp(nDroppers);
    double length;
    
    // Iterate over dropper lengths
    for(int iter=0; iter<=nIters; ++iter)
      {    
	// solve in the current state
	StaticSolve(time, catenary_bcs, contact_bcs, displacements.data());

	// Get sag values at the contact wire along dropper connections
	GetDropperDisplacements(&displacements[0], &dropper_catenary_disp[0], &dropper_contact_disp[0]);

	// Adjust dropper lengths towards achieving the target
	for(int d=0; d<nDroppers; ++d)
	  {
	    length = droppers[d]->GetLength();
	    length -= (dropperSchedule.target_sag[d]-dropper_contact_disp[d]);
	    assert(length>0.);
	    droppers[d]->SetLength(length);
	  }
      }

    // done
    return;
  }


  // Compute transverse stiffness disrtribution
  void CatenarySupportedContactWire::
  ComputeTransverseStiffnessDistribution(const double time, const double* displacements,
					 const double Force, double* stiffness) const
  {
    assert(isSetup==true);
    assert(pantographs.empty());
    
    PetscErrorCode ierr;
    
    // Apply the given force at each node of the contact wire along the span
    // Measure the displacement and compute the stiffness
    
    // displacements
    std::vector<double> dofValues(nTotalDof);

    // Loop over the loading node
    for(int lnode=0; lnode<nNodes; ++lnode)
      {
	std::fill(dofValues.begin(), dofValues.end(), 0.);
	ierr = MatZeroEntries(PD.stiffnessMAT); CHKERRV(ierr);
	ierr = VecZeroEntries(PD.resVEC);       CHKERRV(ierr);
	
	// Assemble
	Evaluate(time, dofValues.data(), PD.massMAT, PD.stiffnessMAT, PD.resVEC);

	// Add transverse force at "lnode" on the contact wire
	const int dof = nCatenaryCableDofs+lnode;
	ierr = VecSetValues(PD.resVEC, 1, &dof, &Force, ADD_VALUES); CHKERRV(ierr);
	ierr = VecAssemblyBegin(PD.resVEC);                          CHKERRV(ierr);
	ierr = VecAssemblyEnd(PD.resVEC);                            CHKERRV(ierr);
	
	// Solve
	linSolver.SetOperator(PD.stiffnessMAT);
	linSolver.Solve(PD.resVEC, PD.solutionVEC);

	// Get the solution
	double* solution;
	ierr = VecGetArray(PD.solutionVEC, &solution);     CHKERRV(ierr);
	for(int a=0; a<nTotalDof; ++a)
	  dofValues[a] = solution[a];
	ierr = VecRestoreArray(PD.solutionVEC, &solution); CHKERRV(ierr);

	// relative displacement
	double delta_disp = dofValues[dof]-displacements[dof];

	// Compute the stiffness
	stiffness[lnode] = Force/delta_disp;
      }
    
    // done
    return;
  }

  
}
