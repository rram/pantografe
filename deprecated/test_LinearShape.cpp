
/** \file test_LinearShape.cpp
 * \brief Unit test for 1D Linear shape functions
 * \author Ramsharan Rangarajan
 * Last modified: September 13, 2021
 */


#include <pfe_LinearShape.h>
#include <cassert>
#include <random>

using namespace pfe;

int main()
{
  LinearShape<1> LinShp;
  assert(LinShp.GetNumberOfFunctions()==2);
  assert(LinShp.GetNumberOfVariables()==1);

  // Check function values + consistency of derivatives
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(0.,1.);
  for(int trial=0; trial<100; ++trial)
    {
      const double lambda = dis(gen);
      double shp_sum = 0.;
      double dshp_sum = 0.;
      const int nShp = LinShp.GetNumberOfFunctions();
      for(int a=0; a<nShp; ++a)
	{
	  double Na = LinShp.Val(a, &lambda);
	  assert(Na>=0. && Na<=1.);
	  shp_sum += Na;
	  dshp_sum += LinShp.DVal(a, &lambda, 0);
	}
      assert(std::abs(shp_sum-1.)<1.e-5);
      assert(std::abs(dshp_sum-0.)<1.e-5);

      // consistency
      LinShp.ConsistencyTest(&lambda, 1.e-5, 1.e-4);
    }

  // done
}
	 
