
/** \file pfe_Pantograph.h
 * \brief Defines the class pfe::Pantograph
 * \author Ramsharan Rangarajan
 * Last modified: August 02, 2022
 */

#ifndef PFE_PANTOGRAPH_H
#define PFE_PANTOGRAPH_H

#include <pfe_PantographParams.h>
#include <petscvec.h>
#include <petscmat.h>
#include <vector>

namespace pfe
{
  //! Abstract base class for a pantograph
  class Pantograph
  {
  public:
    //! Constructor
    inline Pantograph() {}

    //! Destructor
    inline virtual ~Pantograph() {}

    // Disable copy
    Pantograph(const Pantograph&) = delete;

    //! Get the position of the pantograph
    virtual double GetPosition(const double time) const = 0;
    
    //! Assemble contributions to the mass, stiffness, force vector
    virtual void Evaluate(const double time, const Vec& U, Mat& M, Mat& K, Vec& F) const = 0;

    //! Returns the sparsity
    virtual std::vector<int> GetSparsity() const = 0;

    //! Returns the number of independent dofs interacting with the beam
    virtual int GetInteractingDof() const = 0;

    //! Returns the number of dofs
    virtual int GetTotalNumDof() const = 0;
  };

   
  //! Single spring-mass-dashpot system
  class Pantograph_1: public Pantograph
  {
  public:
    //! Constructor
    inline Pantograph_1(const Pantograph_1_Params& pp)
      
      :Pantograph(),
      params(pp) {}

    //! Destructor
    inline virtual ~Pantograph_1() {}
    
    //! Disable copy and assignment
    Pantograph_1(const Pantograph_1&) = delete;
    Pantograph_1& operator=(const Pantograph_1&) = delete;

    //! Get the position of the pantograph
    inline double GetPosition(const double time) const override
    { return params.xinit + params.speed*time; }
    
    
    //! Compute the energy functional associated with a dof set
    double ComputeStrainEnergy(const double time, const double* dofValues) const;
    
    //! Assemble contributions to the mass, stiffness and force vector
    void Evaluate(const double time, const Vec& U, Mat& M, Mat& K, Vec& F) const override;

    // Returns the sparsity
    inline std::vector<int> GetSparsity() const override
    { return std::vector<int>{1}; }

    //! Identify the dof interacting with the contact wire
    inline int GetInteractingDof() const override
    { return 0; }

    //! Returns the number of dofs
    inline int GetTotalNumDof() const override
    { return 1; }

  protected:
    const Pantograph_1_Params params;
  };
  

  //! Benchmark spring-mass-dashpot system
  class Pantograph_3: public Pantograph
  {
  public:
    //! Constructor
    //! dof2 is on the contact wire
    inline Pantograph_3(const Pantograph_3_Params& pp)
      :Pantograph(),
      params(pp) {}
    
    //! Destructor
    inline virtual ~Pantograph_3() {}

    //! Disable copy and assignment
    Pantograph_3(const Pantograph_3&) = delete;
    Pantograph_3& operator=(const Pantograph_3&) = delete;

    //! Get the position of the pantograph
    inline double GetPosition(const double time) const override
    { return params.xinit + params.speed*time; }
    
    //! Compute the energy functional
    double ComputeStrainEnergy(const double time, const double* dofValues) const;
    
    //! Assemble contributions to the mass, stiffness and force vector
    void Evaluate(const double time, const Vec& U, Mat& M, Mat& K, Vec& F) const override;

    //! Returns the sparsity
    inline std::vector<int> GetSparsity() const override
    { return std::vector<int>{2, 3, 2}; }

    //! dof2 interacts with the contact wire
    inline int GetInteractingDof() const override
    { return 2; }
    
    //! Returns the number of dofs
    inline int GetTotalNumDof() const override
    { return 3; }

  protected:
    const Pantograph_3_Params params;
  };

}

#endif
