
/** \file pfe_Newmark_AForm.cpp
 * \brief Implements the class pfe::Newmark_AForm
 * \author Ramsharan Rangarajan
 */

#include <pfe_Newmark_AForm.h>
#include <vector>

namespace pfe
{
  // Constructor
  Newmark_AForm::Newmark_AForm(const int ndof, const double delta)
    :isInitialized(false),
     isDestroyed(false),
     time(0.),
     dt(delta),
     nDof(ndof),
     state(nDof)
  {
    // Create data structures
    VecCreateSeq(PETSC_COMM_WORLD, nDof, &Unp);
    VecCreateSeq(PETSC_COMM_WORLD, nDof, &RHS);
    VecCreateSeq(PETSC_COMM_WORLD, nDof, &Anp1);
  }

  // Destructor
  Newmark_AForm::~Newmark_AForm()
  {
    assert(isDestroyed==true);
  }

  // Destroy
  void Newmark_AForm::Destroy()
  {
    assert(isDestroyed==false);
    state.Destroy();
    linSolver.Destroy();
    PetscErrorCode ierr;
    ierr = MatDestroy(&Amat); CHKERRV(ierr);
    ierr = VecDestroy(&Unp);  CHKERRV(ierr);
    ierr = VecDestroy(&RHS);  CHKERRV(ierr);
    ierr = VecDestroy(&Anp1); CHKERRV(ierr);
    isDestroyed = true;
    return;
  }

  
  // Initialize
  void Newmark_AForm::SetInitialConditions(const double t0,
					   const double* disp,
					   const double* vel,
					   const double* accn)
  {
    assert(isDestroyed==false && isInitialized==false);
    
    // Initial time
    time = t0;
    
    // Set initial displacement and velocity in the dynamic configuration
    Vec& U = state.U;
    Vec& V = state.V;
    Vec& A = state.A;
    std::vector<int> indx(nDof);
    for(int i=0; i<nDof; ++i)
      indx[i] = i;
    
    // Copy displacements, velocities and accelerations
    PetscErrorCode ierr;
    ierr = VecSetValues(U, nDof, indx.data(), disp, INSERT_VALUES); CHKERRV(ierr);
    ierr = VecSetValues(V, nDof, indx.data(), vel,  INSERT_VALUES); CHKERRV(ierr);
    ierr = VecSetValues(A, nDof, indx.data(), accn, INSERT_VALUES); CHKERRV(ierr);

    ierr = VecAssemblyBegin(U); CHKERRV(ierr);
    ierr = VecAssemblyBegin(V); CHKERRV(ierr);
    ierr = VecAssemblyBegin(A); CHKERRV(ierr);
    
    ierr = VecAssemblyEnd(U);   CHKERRV(ierr);
    ierr = VecAssemblyEnd(V);   CHKERRV(ierr);
    ierr = VecAssemblyEnd(A);   CHKERRV(ierr);

    isInitialized = true;
    return;
  }


  // Advance
  void Newmark_AForm::Advance(const Mat& M, const Mat& K, const Vec& F, const std::vector<int> dirichlet_dofs)
  {
    assert(isInitialized==true && isDestroyed==false);
    PetscErrorCode ierr;
    
    // A = M + (dt^2/4)K
    //ierr = MatDestroy(&Amat);                                       CHKERRV(ierr);
    ierr = MatDuplicate(M, MAT_COPY_VALUES, &Amat);                   CHKERRV(ierr);
    ierr = MatAXPY(Amat, 0.25*dt*dt, K, DIFFERENT_NONZERO_PATTERN);   CHKERRV(ierr); 
    ierr = MatSetOption(Amat, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr); 
    ierr = MatSetOption(Amat, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);  CHKERRV(ierr); 

    // Unp = Un + dt Vn + (dt^2/4) An
    auto& Un = state.U;
    auto& Vn = state.V;
    auto& An = state.A; 
    ierr = VecCopy(Un, Unp);             CHKERRV(ierr); 
    ierr = VecAXPY(Unp, dt, Vn);         CHKERRV(ierr);
    ierr = VecAXPY(Unp, 0.25*dt*dt, An); CHKERRV(ierr); 
    
    // RHS = F - K Unp
    ierr = VecZeroEntries(RHS);  CHKERRV(ierr);
    ierr = MatMult(K, Unp, RHS); CHKERRV(ierr); 
    ierr = VecScale(RHS, -1.);   CHKERRV(ierr);
    ierr = VecAXPY(RHS, 1., F);  CHKERRV(ierr); 
    
    // Dirichlet BCs
    if(!dirichlet_dofs.empty())
      {
	const int nbcs = static_cast<int>(dirichlet_dofs.size());
	std::vector<double> zero(nbcs);
	std::fill(zero.begin(), zero.end(), 0.);
	  
	ierr = MatZeroRows(Amat, nbcs, &dirichlet_dofs[0], 1.0, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRV(ierr);
	ierr = VecSetValues(RHS, nbcs, &dirichlet_dofs[0], &zero[0], INSERT_VALUES);           CHKERRV(ierr);
	ierr = VecAssemblyBegin(RHS); CHKERRV(ierr);
	ierr = VecAssemblyEnd(RHS);   CHKERRV(ierr); 
      }

    // Solve for An+1
    linSolver.SetOperator(Amat);
    linSolver.Solve(RHS, Anp1);

    // New state
    
    // Un+1 = Unp + dt^2/4 An+1 
    ierr = VecCopy(Unp, Un);              CHKERRV(ierr);
    ierr = VecAXPY(Un, 0.25*dt*dt, Anp1); CHKERRV(ierr);

    // Vn+1 = Vn + 0.5*dt*(An+An+1)
    ierr = VecAXPY(Vn, 0.5*dt, An);   CHKERRV(ierr);
    ierr = VecAXPY(Vn, 0.5*dt, Anp1); CHKERRV(ierr);

    // An+1
    ierr = VecCopy(Anp1, An);  CHKERRV(ierr);

    // Update time
    time += dt;

    // done
    return;
  }
    
}
    

