
/** \file pfe_ConstrainedNewmarkIntegrator.cpp
 * \brief Implements the class pfe::ConstrainedNewmarkIntegrator
 * \author Ramsharan Rangarajan
 */

#include <pfe_ConstrainedNewmarkIntegrator.h>
#include <cassert>

namespace pfe
{
  // Constructor
  ConstrainedNewmarkIntegrator::ConstrainedNewmarkIntegrator(const std::vector<int>& nz,
							     const double delta)
    :isInitialized(false),
     isSet(false),
     isDestroyed(false),
     num_dofs(static_cast<int>(nz.size())),
     nsize(num_dofs+1),
     time(0.),
     dt(delta),
     state(num_dofs),
     contact_force(0.),
     dirichlet_dofs({}),
     nnz(nsize),
     dof_indices(num_dofs),
     tempVals(num_dofs)
  {
    // index set of ohe+panto system
    for(int i=0; i<num_dofs; ++i)
      dof_indices[i] = i;
    
    // sparsity of LHS matrix of type
    // [ A  r]
    // [r'  0]
    nnz.resize(nsize);
    for(int i=0; i<num_dofs; ++i)
      nnz[i] = nz[i]+1;
    nnz[nsize-1] = nsize-1;

    // Create PETSc data structures for future use
    PetscErrorCode ierr;
    
    // LHS matrix of size nsize x nsize. permit new nonzero locations. keep nonzero pattern
    ierr = MatCreateSeqAIJ(PETSC_COMM_WORLD, nsize, nsize, PETSC_DEFAULT, &nnz[0], &LHSmat); CHKERRV(ierr);
    ierr = MatSetOption(LHSmat, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);                      CHKERRV(ierr);
    ierr = MatSetOption(LHSmat, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);                       CHKERRV(ierr);
    
    // RHS vector, solution vecor of size nsize x 1
    ierr = VecCreateSeq(PETSC_COMM_WORLD, nsize, &RHSvec);  CHKERRV(ierr);
    ierr = VecCreateSeq(PETSC_COMM_WORLD, nsize, &SOLvec);  CHKERRV(ierr);
    
    // F, Unp, Fnp, An+1 of size num_dofs x 1
    ierr = VecCreateSeq(PETSC_COMM_WORLD, num_dofs, &F);    CHKERRV(ierr);
    ierr = VecCreateSeq(PETSC_COMM_WORLD, num_dofs, &Unp);  CHKERRV(ierr);
    ierr = VecCreateSeq(PETSC_COMM_WORLD, num_dofs, &Fnp);  CHKERRV(ierr);
    ierr = VecCreateSeq(PETSC_COMM_WORLD, num_dofs, &Anp1); CHKERRV(ierr);

    // done
  }


  // Clean up
  void ConstrainedNewmarkIntegrator::Destroy()
  {
    assert(isDestroyed==false);
    
    PetscBool      flag;
    PetscErrorCode ierr;
    ierr = PetscFinalized(&flag); CHKERRV(ierr);
    assert(flag==PETSC_FALSE);

    // destroy data structures
    ierr = MatDestroy(&M);           CHKERRV(ierr);
    ierr = MatDestroy(&K);           CHKERRV(ierr);
    ierr = VecDestroy(&F);           CHKERRV(ierr);
    ierr = MatDestroy(&LHSmat);      CHKERRV(ierr);
    ierr = VecDestroy(&RHSvec);      CHKERRV(ierr);
    ierr = VecDestroy(&SOLvec);      CHKERRV(ierr); 
    ierr = VecDestroy(&Anp1);        CHKERRV(ierr);
    ierr = VecDestroy(&Unp);         CHKERRV(ierr);
    ierr = VecDestroy(&Fnp);         CHKERRV(ierr);
    state.Destroy();
    linSolver.Destroy();

    isDestroyed = true;
  }


  // Set operators
  void ConstrainedNewmarkIntegrator::SetOperators(const Mat& mass,
						  const Mat& stiff,
						  const Vec& force, 
						  const std::vector<int>& dbcs)
  {
    // assume that operators will be set just once
    assert(isSet==false && isDestroyed==false);

    // copy dirichlet dof indices
    dirichlet_dofs = dbcs;

    // Sanity checks on sizes of data
    PetscErrorCode ierr;
    int nrows, ncols;
    ierr = MatGetSize(mass, &nrows, &ncols);  CHKERRV(ierr);
    assert(nrows==nsize-1 && ncols==nsize-1);
    ierr = MatGetSize(stiff, &nrows, &ncols); CHKERRV(ierr);
    assert(nrows==nsize-1 && ncols==nsize-1);
    ierr = VecGetSize(force, &nrows);         CHKERRV(ierr);
    assert(nrows==nsize-1);

    // Duplicate the mass, stiffness & force
    //ierr = MatDestroy(&M);                           CHKERRV(ierr);
    //ierr = MatDestroy(&K);                           CHKERRV(ierr);
    ierr = MatDuplicate(mass,  MAT_COPY_VALUES, &M); CHKERRV(ierr);
    ierr = MatDuplicate(stiff, MAT_COPY_VALUES, &K); CHKERRV(ierr);
    ierr = VecCopy(force, F);                        CHKERRV(ierr);
    
    // LHSmat = [A          -r]
    //          [factor*r^t  0]
    // where:
    //          A = M + dt^2/4 K
    // Assemble A into LHSmat.
    // constraint vector 'r' is time-dependent and is assembled into LHSmat in Advance()
    Mat A;
    ierr = MatDuplicate(M, MAT_COPY_VALUES, &A);                 CHKERRV(ierr);
    ierr = MatAXPY(A, 0.25*dt*dt, K, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);
    ierr = MatZeroEntries(LHSmat);                               CHKERRV(ierr);
    for(int row=0; row<num_dofs; ++row)
      {
	int           ncols;
	const int    *cols;
	const double *vals;
	ierr = MatGetRow(A, row, &ncols, &cols, &vals);                         CHKERRV(ierr);
	ierr = MatSetValues(LHSmat, 1, &row, ncols, cols, vals, INSERT_VALUES); CHKERRV(ierr);
	ierr = MatRestoreRow(A, row, &ncols, &cols, &vals);                     CHKERRV(ierr);
      }

    // Insert zeros in the last row/col of LHSmat for future use
    std::vector<int> all_indices(nsize);
    for(int i=0; i<nsize; ++i)
      all_indices[i] = i;
    std::vector<double> zeros(nsize);
    std::fill(zeros.begin(), zeros.end(), 0.);
    ierr = MatSetValues(LHSmat, 1, &num_dofs, nsize, &all_indices[0], &zeros[0], INSERT_VALUES); CHKERRV(ierr);
    ierr = MatSetValues(LHSmat, nsize, &all_indices[0], 1, &num_dofs, &zeros[0], INSERT_VALUES); CHKERRV(ierr);

    // finish up assembly
    ierr = MatAssemblyBegin(LHSmat, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(LHSmat,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);

    // clean up
    ierr = MatDestroy(&A); CHKERRV(ierr);
    
    // done
    isSet = true;
    return;
  }


  // Initial conditions
  void ConstrainedNewmarkIntegrator::SetInitialConditions(const double t0,
							  const double* uvals, const double* vvals,
							  const double* avals, const double fc)
  {
    assert(isDestroyed==false && isSet==true && isInitialized==false);
    time = t0;
    contact_force = fc;
    
    PetscErrorCode ierr;
    ierr = VecSetValues(state.U, num_dofs, &dof_indices[0], uvals, INSERT_VALUES); CHKERRV(ierr);
    ierr = VecSetValues(state.V, num_dofs, &dof_indices[0], vvals, INSERT_VALUES); CHKERRV(ierr);
    ierr = VecSetValues(state.A, num_dofs, &dof_indices[0], avals, INSERT_VALUES); CHKERRV(ierr);
    
    ierr = VecAssemblyBegin(state.U); CHKERRV(ierr);
    ierr = VecAssemblyBegin(state.V); CHKERRV(ierr);
    ierr = VecAssemblyBegin(state.A); CHKERRV(ierr);

    ierr = VecAssemblyEnd(state.U);   CHKERRV(ierr);
    ierr = VecAssemblyEnd(state.V);   CHKERRV(ierr);
    ierr = VecAssemblyEnd(state.A);   CHKERRV(ierr);

    // done
    return;
  }
  
  
  
  // Advance to the next time step
  void ConstrainedNewmarkIntegrator::Advance(const Vec &rvec,
					     const std::vector<double>& dirichlet_values)
  { 
    // sanity checks
    assert(isSet==true && isDestroyed==false);
    const int nbcs = static_cast<int>(dirichlet_dofs.size());
    assert(static_cast<int>(dirichlet_values.size())==nbcs);
    PetscErrorCode ierr;
    int nrows;
    ierr = VecGetSize(rvec, &nrows); CHKERRV(ierr);
    assert(nrows==num_dofs);

    // state at time tn
    Vec& Un = state.U;
    Vec& Vn = state.V;
    Vec& An = state.A;

    // Constraint vector values
    assert(static_cast<int>(tempVals.size())==num_dofs);
    ierr = VecGetValues(rvec, num_dofs, dof_indices.data(), tempVals.data()); CHKERRV(ierr);
    
    // Assemble last column of the composite matrix:
    // [-r ]
    // [ 0 ]
    for(auto& x:tempVals) x *= -1.;
    ierr = MatSetValues(LHSmat, num_dofs, dof_indices.data(), 1, &num_dofs, tempVals.data(), INSERT_VALUES); CHKERRV(ierr);

    // Assemble last row of the composite matrix: [dt^2/4 r', 0]
    for(auto& x:tempVals) x *= -0.25*dt*dt;
    ierr = MatSetValues(LHSmat, 1, &num_dofs, num_dofs, dof_indices.data(), tempVals.data(), INSERT_VALUES); CHKERRV(ierr);

    // Finish up matrix assembly
    ierr = MatAssemblyBegin(LHSmat, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(LHSmat,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);

    // Update the force vector
    // RHS = [F - Kdnp] = [ Fnp ]
    //       [-r'dnp  ]   [ G   ]
    ierr = VecZeroEntries(RHSvec);        CHKERRV(ierr);

    // displacement predictor: Unp = Un + dt Vn + (dt^2/4) An
    ierr = VecZeroEntries(Unp);           CHKERRV(ierr);
    ierr = VecCopy(Un, Unp);              CHKERRV(ierr);
    ierr = VecAXPY(Unp, dt, Vn);          CHKERRV(ierr);
    ierr = VecAXPY(Unp, 0.25*dt*dt, An);  CHKERRV(ierr);

    // Fnp = F-K Unp
    ierr = MatMult(K, Unp, Fnp);          CHKERRV(ierr);
    ierr = VecScale(Fnp, -1.);            CHKERRV(ierr);
    ierr = VecAXPY(Fnp, 1.0, F);          CHKERRV(ierr);
    
    // Last row of RHS, G = -r' Unp
    double G = 0.;
    ierr = VecDot(rvec, Unp, &G);         CHKERRV(ierr);
    G *= -1.;
    
    // Assemble the RHS
    double* fnp_array;
    ierr = VecGetArray(Fnp, &fnp_array);                                                 CHKERRV(ierr);
    ierr = VecSetValues(RHSvec, num_dofs, dof_indices.data(), fnp_array, INSERT_VALUES); CHKERRV(ierr);
    ierr = VecRestoreArray(Fnp, &fnp_array);                                             CHKERRV(ierr);
    ierr = VecSetValues(RHSvec, 1, &num_dofs, &G, INSERT_VALUES);                        CHKERRV(ierr);
    ierr = VecAssemblyBegin(RHSvec);                                                     CHKERRV(ierr);
    ierr = VecAssemblyEnd(RHSvec);                                                       CHKERRV(ierr);

    // Set dirichlet values
    if(nbcs!=0)
      {
	ierr = MatSetOption(LHSmat, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);                                    CHKERRV(ierr);
	ierr = MatZeroRows(LHSmat, nbcs, dirichlet_dofs.data(), 1., PETSC_NULL, PETSC_NULL);                  CHKERRV(ierr);
	ierr = VecSetValues(RHSvec, nbcs, dirichlet_dofs.data(), dirichlet_values.data(), INSERT_VALUES);     CHKERRV(ierr);
	ierr = VecAssemblyBegin(RHSvec);                                                                      CHKERRV(ierr);
	ierr = VecAssemblyEnd(RHSvec);                                                                        CHKERRV(ierr);
      }

    // Solve
    linSolver.SetOperator(LHSmat);
    linSolver.Solve(RHSvec, SOLvec);
    
    // SOLvec = [ An+1 ]
    //          [ fc   ]
    double* sol_array;
    ierr = VecGetArray(SOLvec, &sol_array);                                            CHKERRV(ierr);
    contact_force = sol_array[num_dofs];
    ierr = VecZeroEntries(Anp1);                                                       CHKERRV(ierr);
    ierr = VecSetValues(Anp1, num_dofs, dof_indices.data(), sol_array, INSERT_VALUES); CHKERRV(ierr);
    ierr = VecRestoreArray(SOLvec, &sol_array);                                        CHKERRV(ierr);
    ierr = VecAssemblyBegin(Anp1);                                                     CHKERRV(ierr);
    ierr = VecAssemblyEnd(Anp1);                                                       CHKERRV(ierr);
    
    // Update the state to tn+1

    // Un+1 = Unp + 0.25 dt^2 An+1
    ierr = VecCopy(Unp, Un);                 CHKERRV(ierr);
    ierr = VecAXPY(Un, 0.25*dt*dt, Anp1);    CHKERRV(ierr);

    // Vn+1 = Vn + 0.5*dt(An + An+1)
    ierr = VecAXPY(Vn, 0.5*dt, An);          CHKERRV(ierr);
    ierr = VecAXPY(Vn, 0.5*dt, Anp1);        CHKERRV(ierr);

    // An+1
    ierr = VecCopy(Anp1, An);                CHKERRV(ierr); 
    
    // Update the time
    time += dt;

    // done
    return;
  }
  
}
