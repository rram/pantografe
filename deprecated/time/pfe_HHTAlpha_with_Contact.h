
/** \file pfe_HHTAlpha_with_Contact.h
 * \brief Defines the class pfe::HHTAlpha_with_Contact
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_DynamicState.h>
#include <pfe_LinearSolver.h>
#include <vector>
#include <petscmat.h>
#include <cassert>

namespace pfe
{
  class HHTAlpha_with_Contact
  {
  public:
    //! Constructor
    HHTAlpha_with_Contact(const double aleph, const int ndof, const double delta);

    //! Disable copy and assignment
    HHTAlpha_with_Contact(const HHTAlpha_with_Contact&) = delete;
    HHTAlpha_with_Contact& operator=(const HHTAlpha_with_Contact&) = delete;

    //! Destructor
    virtual ~HHTAlpha_with_Contact();

    //! Destroy data structures
    void Destroy();

    //! Set operators
    void SetOperators(const Mat& mass, const Mat& stiffness,
		      const Vec& frc, const std::vector<int> dbc_dofs);

    //! Set initial conditions
    void SetInitialConditions(const double t0, const double* disp,
			      const double* vel, const double* accn, const double fc);

    //! Get the evaluation time
    inline double GetNextEvaluationTime() const
    { return time+dt; }

    //! Advance to the next time step
    void Advance(const Vec& rvec);

    //! Access the solution
    inline DynamicState& GetState()
    {
      assert(isInitialized==true && isDestroyed==false);
      return state;
    }

    //! Access the contact force
    inline double GetContactForce()
    { 
      assert(isInitialized==true && isDestroyed==false);
      return contact_force;
    }

    //! Access the time
    inline double GetTime() const
    { return time; }

    //! Access the time step
    inline double GetTimestep() const
    { return dt; }

  private:
        bool isInitialized; //!< have initial conditions been set
    bool isSet;         //!< have the operators been set
    bool isDestroyed;   //!< state of petsc data structures

    const double alpha;   //!< algorithmic parameter
    const double gamma;   //!< algorithmic parameter
    const double beta;    //!< algorithmic parameter
    
    const double dt;   //!< time step
    const int nDof;    //!< number of dofs, not including the contact force
    double time;       //!< current time
    std::vector<int> dirichlet_dofs; //!< dirichlet dofs

    DynamicState state;          //!< Current dynamic state
    double       contact_force;  //!< contact force
    Mat          M;              //!< mass
    Mat          K;              //!< stiffness
    Vec          F;              //!< force 
    Mat          LHSmat;         //!< composite matrix
    Vec          RHS;            //!< RHS vector
    Vec          A1, A2;         //!< intermediate solutions
    Vec          dnp;            //!< predictor displacement
    Vec          Kdn, Kdnp;      //!< Intermediate vectors           
    LinearSolver linSolver;      //!< Linear solver
  };

}
