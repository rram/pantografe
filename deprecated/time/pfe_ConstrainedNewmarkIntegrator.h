
/** \file pfe_ConstrainedNewmarkIntegrator.h
 * \brief Defines the class pfe::ConstrainedNewmarkIntegrator
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_DynamicState.h>
#include <pfe_LinearSolver.h>
#include <vector>
#include <petscmat.h>
#include <cassert>

namespace pfe
{
  class ConstrainedNewmarkIntegrator
  {
  public:
    //! Constructor
    ConstrainedNewmarkIntegrator(const std::vector<int>& nz, const double delta);
      
    //! Disable copy and assignment
    ConstrainedNewmarkIntegrator(const ConstrainedNewmarkIntegrator&) = delete;
    ConstrainedNewmarkIntegrator& operator=(const ConstrainedNewmarkIntegrator&) = delete;

    //! Destructor
    inline virtual ~ConstrainedNewmarkIntegrator() { assert(isDestroyed==true); }
    
    //! Destroy data structures
    void Destroy();

    //! Set operators
    void SetOperators(const Mat& mass, const Mat& stiff, const Vec& force, 
		      const std::vector<int>& dbcs);

    //! Initialize state, assuming zero initial velocity and acceleration
    void SetInitialConditions(const double t0,
			      const double* uvals, const double* vvals,
			      const double* avals, const double fc);
        
    //! Advance to the next time step
    void Advance(const Vec &R_constraint, const std::vector<double>& dirichlet_values);
    
    //! Access the state of the OHE
    inline DynamicState& GetState() { return state; }

    //! Access the contact force
    inline double GetContactForce() { return contact_force; }
    
    //! Access the time
    inline double GetTime() const { return time; }

    //! Access the time step
    inline double GetTimestep() const { return dt; }
    
  private:
    bool isInitialized;     //!< have initial conditions been set
    bool isSet;             //!< are the operators set?
    bool isDestroyed;       //!< state of the petsc data structures

    const int num_dofs;     //!< number of dofs of the ohe + panto system
    const int nsize;        //!< total number of unknowns = dofs + 1
    
    double       time;      //!< current time
    const double dt;        //!< Time step
    
    DynamicState state;             //!< Current dynamic state of the OHE+panto assembly
    double       contact_force;     //!< Current contact force

    Mat M;                                //! Assembled OHE-panto mass
    Mat K;                                //! Assembled OHE-panto stiffness
    Vec F;                                //! Assembled OHE-panto force vector
    std::vector<int> dof_indices;         //! index set for dofs of the ohe+panto system
    std::vector<int> dirichlet_dofs;      //! Dirichlet dofs of ohe+panto system

    std::vector<int> nnz;   //!< sparsity of composite matrix
    Mat          LHSmat;    //!< Composite matrix to be inverted
    Vec          RHSvec;    //!< Composite RHS
    Vec          SOLvec;    //!< solution vector 
    LinearSolver linSolver; //!< Linear solver
    Vec          Fnp;       //!< predictor force
    Vec          Unp;       //!< predictor displacement
    Vec          Anp1;      //!< An+1
    std::vector<double> tempVals; //! temporary vector of size num_ohe_panto_dofs
  };
}
