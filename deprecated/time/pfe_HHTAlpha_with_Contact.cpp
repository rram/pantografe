
/** \file pfe_HHTAlpha_with_Contact.cpp
 * \brief Implements the class pfe::HHTAlpha_with_Contact
 * \author Ramsharan Rangarajan
 */

#include <pfe_HHTAlpha_with_Contact.h>

namespace pfe
{
  // Constructor
  HHTAlpha_with_Contact::HHTAlpha_with_Contact(const double aleph, const int ndof, const double delta)
    :isInitialized(false),
     isSet(false),
     isDestroyed(false),
     alpha(aleph),
     gamma(0.5-alpha),
     beta(0.25*(1.-alpha)*(1.-alpha)),
     time(0.),
     dt(delta),
     nDof(ndof),
     state(nDof),
     contact_force(0.),
     dirichlet_dofs{} {}

  // Destructor
  HHTAlpha_with_Contact::~HHTAlpha_with_Contact()
  {
    assert(isDestroyed==true);
  }

  // Destroy data structures
  void HHTAlpha_with_Contact::Destroy()
  {
    assert(isDestroyed==false);
    state.Destroy();
    if(isSet)
      {
	PetscErrorCode ierr;
	ierr = MatDestroy(&M);      CHKERRV(ierr);
	ierr = MatDestroy(&K);      CHKERRV(ierr);
	ierr = MatDestroy(&LHSmat); CHKERRV(ierr);
	ierr = VecDestroy(&RHS);    CHKERRV(ierr);
	ierr = VecDestroy(&A1);     CHKERRV(ierr);
	ierr = VecDestroy(&A2);     CHKERRV(ierr);
	ierr = VecDestroy(&dnp);    CHKERRV(ierr);
	ierr = VecDestroy(&Kdnp);   CHKERRV(ierr);
	ierr = VecDestroy(&Kdn);    CHKERRV(ierr);
      }
    linSolver.Destroy();
    isDestroyed = true;
  }


  // Set operators
  void HHTAlpha_with_Contact::SetOperators(const Mat& mass, const Mat& stiffness,
					   const Vec& frc, const std::vector<int> dbc_dofs)
  {
    assert(isSet==false);

    // Copy M, K, F
    PetscErrorCode ierr;
    ierr = MatDuplicate(mass, MAT_COPY_VALUES, &M);      CHKERRV(ierr);
    ierr = MatDuplicate(stiffness, MAT_COPY_VALUES, &K); CHKERRV(ierr);
    ierr = VecDuplicate(frc, &F);                        CHKERRV(ierr);
    ierr = VecCopy(frc, F);                              CHKERRV(ierr);
    dirichlet_dofs = dbc_dofs;

    // A = M + (1+alpha)*beta*dt^2 K
    ierr = MatDuplicate(mass, MAT_COPY_VALUES, &LHSmat);                         CHKERRV(ierr);
    ierr = MatSetOption(LHSmat, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);          CHKERRV(ierr);
    ierr = MatAXPY(LHSmat, (1.+alpha)*beta*dt*dt, K, DIFFERENT_NONZERO_PATTERN); CHKERRV(ierr);

    // Dirichlet dofs
    if(!dirichlet_dofs.empty())
      {
	const int nbcs = static_cast<int>(dirichlet_dofs.size());
	ierr = MatZeroRows(LHSmat, nbcs, dirichlet_dofs.data(), 1., PETSC_NULL, PETSC_NULL); CHKERRV(ierr);
      }

    // Set operators in the linear system
    linSolver.SetOperator(LHSmat);

    // allocate vectors
    ierr = VecDuplicate(frc, &RHS);  CHKERRV(ierr);
    ierr = VecDuplicate(frc, &dnp);  CHKERRV(ierr);
    ierr = VecDuplicate(frc, &Kdnp); CHKERRV(ierr);
    ierr = VecDuplicate(frc, &Kdn);  CHKERRV(ierr);
    ierr = VecDuplicate(frc, &A1);   CHKERRV(ierr);
    ierr = VecDuplicate(frc, &A2);   CHKERRV(ierr);

    // done
    isSet = true;
    return;
  }


  // Initial conditions
  void HHTAlpha_with_Contact::SetInitialConditions(const double t0,
						   const double* disp,
						   const double* vel,
						   const double* accn,
						   const double fc)
  {
    assert(isDestroyed==false && isSet==true && isInitialized==false);
    time = t0;

    // Aliases
    Vec& U = state.U;
    Vec& V = state.V;
    Vec& A = state.A;
    std::vector<int> indx(nDof);
    for(int i=0; i<nDof; ++i)
      indx[i] = i;

    // Copy
    contact_force = fc;
    PetscErrorCode ierr;
    ierr = VecSetValues(U, nDof, indx.data(), disp, INSERT_VALUES); CHKERRV(ierr);
    ierr = VecSetValues(V, nDof, indx.data(), vel, INSERT_VALUES);  CHKERRV(ierr);
    ierr = VecSetValues(A, nDof, indx.data(), accn, INSERT_VALUES); CHKERRV(ierr);
    ierr = VecAssemblyBegin(U); CHKERRV(ierr);
    ierr = VecAssemblyBegin(V); CHKERRV(ierr);
    ierr = VecAssemblyBegin(A); CHKERRV(ierr);
    ierr = VecAssemblyEnd(U);   CHKERRV(ierr);
    ierr = VecAssemblyEnd(V);   CHKERRV(ierr);
    ierr = VecAssemblyEnd(A);   CHKERRV(ierr);

    // done
    isInitialized = true;
    return;
  }


  // Advance in time
  void HHTAlpha_with_Contact::Advance(const Vec& rvec)
  {
    assert(isSet==true && isDestroyed==false && isInitialized==true);
    const int nbcs = static_cast<int>(dirichlet_dofs.size());
    const std::vector<double> zero(nbcs, 0.);

    // State at time tn
    Vec& Un = state.U;
    Vec& Vn = state.V;
    Vec& An = state.A;

    // predictor displacement dnp = dn + dt*Vn + (1/2-beta)*dt^2*An
    PetscErrorCode ierr;
    ierr = VecCopy(Un, dnp);                   CHKERRV(ierr);
    ierr = VecAXPY(dnp, dt, Vn);               CHKERRV(ierr);
    ierr = VecAXPY(dnp, (0.5-beta)*dt*dt, An); CHKERRV(ierr);

    // Intermediate quantities
    ierr = VecZeroEntries(Kdn);   CHKERRV(ierr);
    ierr = VecZeroEntries(Kdnp);  CHKERRV(ierr);
    ierr = MatMult(K, Un, Kdn);   CHKERRV(ierr);
    ierr = MatMult(K, dnp, Kdnp); CHKERRV(ierr);

    // Solve LHSmat*A = F-(1+alpha)*Kdnp+alpha*Kdn
    ierr = VecCopy(F, RHS);                 CHKERRV(ierr);
    ierr = VecAXPY(RHS, -(1.+alpha), Kdnp); CHKERRV(ierr);
    ierr = VecAXPY(RHS, alpha, Kdn);        CHKERRV(ierr);
    if(nbcs!=0)
      {
	ierr = VecSetValues(RHS, nbcs, dirichlet_dofs.data(), zero.data(), INSERT_VALUES); CHKERRV(ierr);
	ierr = VecAssemblyBegin(RHS); CHKERRV(ierr);
	ierr = VecAssemblyEnd(RHS);   CHKERRV(ierr);
      }
    linSolver.Solve(RHS, A1);

    // Solve LHSmat*A2 = rvec
    ierr = VecCopy(rvec, RHS); CHKERRV(ierr);
    if(nbcs!=0)
      {
	ierr = VecSetValues(RHS, nbcs, dirichlet_dofs.data(), zero.data(), INSERT_VALUES); CHKERRV(ierr);
	ierr = VecAssemblyBegin(RHS); CHKERRV(ierr);
	ierr = VecAssemblyEnd(RHS);   CHKERRV(ierr);
      }
    linSolver.Solve(RHS, A2);

    // Compute the contact force using the constraint r^T d = 0
    double r_dot_A1;
    ierr = VecDot(rvec, A1, &r_dot_A1);   CHKERRV(ierr);
    double r_dot_A2;
    ierr = VecDot(rvec, A2, &r_dot_A2);   CHKERRV(ierr);
    double r_dot_dnp;
    ierr = VecDot(rvec, dnp, &r_dot_dnp); CHKERRV(ierr);
    contact_force = (r_dot_dnp/(beta*dt*dt)-r_dot_A1)/r_dot_A2;

    // Update state

    // Vnp = Vn + (1-gamma)*dt*An
    ierr = VecAXPY(Vn, (1.-gamma)*dt, An); CHKERRV(ierr);

    // An+1 = A1 + fc*A2
    ierr = VecCopy(A1, An);                CHKERRV(ierr);
    ierr = VecAXPY(An, contact_force, A2); CHKERRV(ierr);

    // Vn+1 = Vnp+gamma*dt*An
    ierr = VecAXPY(Vn, gamma*dt, An); CHKERRV(ierr);

    // dn+1 = dnp + beta*dt^2*An+1
    ierr = VecCopy(dnp, Un);            CHKERRV(ierr);
    ierr = VecAXPY(Un, beta*dt*dt, An); CHKERRV(ierr);

    // Update time
    time += dt;

    // done
    return;
  }

  
}
