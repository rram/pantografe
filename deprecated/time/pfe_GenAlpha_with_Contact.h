
/** \file pfe_GenAlpha_with_Contact.h
 * \brief Defines the class pfe::GenAlpha_with_Contact
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_DynamicState.h>
#include <pfe_LinearSolver.h>
#include <vector>
#include <petscmat.h>
#include <cassert>

namespace pfe
{
  class GenAlpha_with_Contact
  {
  public:
    //! Constructor
    GenAlpha_with_Contact(const double am, const double af, const int ndof, const double delta);
    
    //! Constructor
    GenAlpha_with_Contact(const double rho, const int ndof, const double delta);
    
    //! Disable copy and assignment
    GenAlpha_with_Contact(const GenAlpha_with_Contact&) = delete;
    GenAlpha_with_Contact& operator=(const GenAlpha_with_Contact&) = delete;

    //! Destructor
    virtual ~GenAlpha_with_Contact();

    //! Destroy data structures
    void Destroy();

    //! Set operators
    void SetOperators(const Mat& mass, const Mat& stiffness,
		      const Vec& frc, const std::vector<int> dbcs_dofs);

    //! Main functionality: initial conditions
    void SetInitialConditions(const double t0, const double* disp,
			      const double* vel, const double* accn, const double fc);

    //! Get the evaluation time
    inline double GetNextEvaluationTime() const
    { return (1.-alpha_f)*(time+dt) + alpha_f*time; }
    
    //! Main functionality
    void Advance(const Vec& rvec);

    //! Access the solution
    inline DynamicState& GetState()
    {
      assert(isInitialized==true && isDestroyed==false);
      return state;
    }

    //! Access the contact force
    inline double GetContactForce()
    {
      assert(isInitialized==true && isDestroyed==false);
      return contact_force;
    }
    
    //! Access the time
    inline double GetTime() const
    { return time; }

    //! Access the time step
    inline double GetTimestep() const
    { return dt; }

  private:
    bool isInitialized; //!< have initial conditions been set
    bool isSet;         //!< have the operators been set
    bool isDestroyed;   //!< state of petsc data structures

    const double alpha_f; //!< algorithmic parameter
    const double alpha_m; //!< algorithmic parameter
    const double gamma;   //!< algorithmic parameter
    const double beta;    //!< algorithmic parameter
    
    const double dt;   //!< time step
    const int nDof;    //!< number of dofs, not including the contact force
    double time;       //!< current time
    std::vector<int> dirichlet_dofs; //!< dirichlet dofs

    DynamicState state;          //!< Current dynamic state
    double       contact_force;  //!< contact force
    Mat          M;              //!< mass
    Mat          K;              //!< stiffness
    Vec          F;              //!< force 
    Mat          LHSmat;         //!< composite matrix
    Vec          RHS;            //!< RHS vector
    Vec          A1, A2;         //!< intermediate solutions
    Vec          dnp;            //!< predictor displacement
    Vec          MAn, Kdn, Kdnp; //!< Intermediate vectors           
    LinearSolver linSolver;      //!< Linear solver
  };
}
