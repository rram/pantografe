
/** \file pfe_HHTAlpha.h
 * \brief Defines the class pfe::HHTAlpha
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_DynamicState.h>
#include <pfe_LinearSolver.h>
#include <vector>
#include <petscmat.h>
#include <cassert>


namespace pfe
{
  class HHTAlpha
    {
    public:
      //! Constructor
      HHTAlpha(const double aleph, const int ndof, const double delta);

      //! Disable copy and assignment
      HHTAlpha(const HHTAlpha&) = delete;
      HHTAlpha& operator=(const HHTAlpha&) = delete;

      //! Destructor
      virtual ~HHTAlpha();

      //! Destroy data structures
      void Destroy();

      //! Set operators
      void SetOperators(const Mat& mass, const Mat& stiffness, const std::vector<int> dbcs_dofs);

      //! Set initial conditions
      void SetInitialConditions(const double t0, const double* disp,
				const double* vel, const double* accn);

      //! Get the evaluation time
      inline double GetNextEvaluationTime() const
      { return time + dt; }

      //! Advance the solution
      void Advance(const Vec& Fvec);

      //! Access the solution
      inline DynamicState& GetState()
      {
	assert(isInitialized==true && isDestroyed==false);
	return state;
      }

      //! Access the time
      inline double GetTime() const
      { return time; }

      //! Access the time step
      inline double GetTimestep() const
      { return dt; }

    private:
      bool isInitialized; //!< have initial conditions been set
      bool isSet;         //!< have the operators been set
      bool isDestroyed;   //!< state of petsc data structures

      const double alpha; //!< algorithmic parameter
      const double gamma; //!< algorithmic parameter
      const double beta;  //!< algorithmic parameter
    
      const double dt;   //!< time step
      const int nDof;    //!< number of dofs, not including the contact force
      double time;       //!< current time
      std::vector<int> dirichlet_dofs; //!< dirichlet dofs

      DynamicState state;          //!< Current dynamic state
      Mat          M;              //!< mass
      Mat          K;              //!< stiffness
      Mat          LHSmat;         //!< composite matrix
      Vec          RHS;            //!< RHS vector
      Vec          Anp1;           //!< intermediate solutions
      Vec          dnp;            //!< predictor displacement
      Vec          Kdn, Kdnp;      //!< Intermediate vectors           
      LinearSolver linSolver;      //!< Linear solver
    };
}

