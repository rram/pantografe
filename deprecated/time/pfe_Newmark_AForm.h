
/** \file pfe_Newmark_AForm.h
 * \brief Defines the class pfe::Newmark_AForm
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_DynamicState.h>
#include <pfe_LinearSolver.h>
#include <cassert>
#include <vector>

namespace pfe
{
  //! Assumes homogeneous Dirichlet BCs
  class Newmark_AForm
  {
  public:
    //! Constructor
    Newmark_AForm(const int ndof, const double delta);

    //! Disable copy and assignment
    Newmark_AForm(const Newmark_AForm&) = delete;
    Newmark_AForm& operator=(const Newmark_AForm&) = delete;
    
    //! Destructor
    virtual ~Newmark_AForm();

    //! Destroy data structures
    void Destroy();

    //! Set initial conditions
    void SetInitialConditions(const double t0, const double* disp,
			      const double* vel, const double* accn);
    
    //! Main functionality
    void Advance(const Mat& M, const Mat& K, const Vec& Fnext, const std::vector<int> dirichlet_dofs);

    //! Access the solution
    inline DynamicState& GetState()
    {
      assert(isInitialized==true && isDestroyed==false);
      return state;
    }

    //! Access the time
    inline double GetTime() const
    { return time; }

    //! Access the timestep
    inline double GetTimestep() const
    { return dt; }
    
  private:
    bool isInitialized;     //!< have initial conditions been set
    bool isDestroyed;       //!< state of the petsc data structures
    
    double time;            //!< current time
    const double dt;        //!< Time step
    const int nDof;         //!< # dofs
    
    DynamicState state;     //!< Current dynamic state
    Vec RHS;                //!< RHS vector
    Vec Unp;                //!< Predictor
    Vec Anp1;               //!< An+1

    Mat              Amat;               //!< Intermediate matrix
    LinearSolver     linSolver;          //!< Linear solver
  };
}
