
/** \file test_Newmark.cpp
 * \brief Tests the class NewmarkIntegrator using a 2-dof system
 * \author Ramsharan Rangarajan
 */

#include <pfe_NewmarkIntegrator.h>
#include <fstream>

void Xsol(const double t, double& x, double& xdot, double& xddot);
void Ysol(const double t, double& x, double& xdot, double& xddot);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);
  
  // Mass & stiffness matrices
  const int nDof = 2;
  const int nnz[] = {nDof, nDof};
  const double mass[2][2]  = {{2.,0.},{0.,1.}};
  const double stiff[2][2] = {{6., -2.,},{-2., 4.}};

  // Convert to Mat
  Mat M, K;
  PetscErrorCode ierr;
  ierr = MatCreateSeqAIJ(PETSC_COMM_WORLD, nDof, nDof, PETSC_DEFAULT, &nnz[0], &M); CHKERRQ(ierr);
  ierr = MatCreateSeqAIJ(PETSC_COMM_WORLD, nDof, nDof, PETSC_DEFAULT, &nnz[0], &K); CHKERRQ(ierr);
  for(int i=0; i<nDof; ++i)
    for(int j=0; j<nDof; ++j)
      {
	ierr = MatSetValues(M, 1, &i, 1, &j, &mass[i][j], INSERT_VALUES);  CHKERRQ(ierr);
	ierr = MatSetValues(K, 1, &i, 1, &j, &stiff[i][j], INSERT_VALUES); CHKERRQ(ierr);
      }
  ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatAssemblyEnd(M, MAT_FINAL_ASSEMBLY);   CHKERRQ(ierr);
  ierr = MatAssemblyBegin(K,MAT_FINAL_ASSEMBLY);  CHKERRQ(ierr);
  ierr = MatAssemblyEnd(K, MAT_FINAL_ASSEMBLY);   CHKERRQ(ierr);

  // Create time integrator
  const double dt = 0.1;
  pfe::NewmarkIntegrator TI(nDof, dt);

  // Set operators
  const std::vector<int> dirichlet_dofs{};
  TI.SetOperators(M, K, dirichlet_dofs);
  

  // Set initial conditions
  double x, xdot, xddot;
  double y, ydot, yddot;
  Xsol(0., x, xdot, xddot);
  Ysol(0., y, ydot, yddot);
  const double U0[] = {x, y};
  const double V0[] = {xdot, ydot};
  const double A0[] = {xddot, yddot};
  double time = 0.;
  TI.SetInitialConditions(time, U0, V0, A0);

  // Aliases
  auto& state = TI.GetState();
  auto& U = state.U;
  auto& V = state.V;
  auto& A = state.A;

  // Forcing
  Vec Fvec;
  ierr = VecCreate(PETSC_COMM_WORLD, &Fvec);    CHKERRQ(ierr); 
  ierr = VecSetSizes(Fvec, PETSC_DECIDE, nDof); CHKERRQ(ierr);
  ierr = VecSetFromOptions(Fvec);               CHKERRQ(ierr);

  // No dirichlet bcs
  const std::vector<double> dirichlet_values{};
  
  std::fstream pfile;
  pfile.open((char*)"sol.dat", std::ios::out);
  pfile << "#time U0 U1 V0 V1 A0 A1";
  while(time<10.)
    {
      time = TI.GetTime();

      // Print state to file
      double *u, *v, *a;
      ierr = VecGetArray(U, &u);     CHKERRQ(ierr);
      ierr = VecGetArray(V, &v);     CHKERRQ(ierr);
      ierr = VecGetArray(A, &a);     CHKERRQ(ierr);
      pfile <<"\n"<<time<<" "<<u[0]<<" "<<u[1]<<" "<<v[0]<<" "<<v[1]<<" "<<a[0]<<" "<<a[1];
      ierr = VecRestoreArray(U, &u); CHKERRQ(ierr);
      ierr = VecRestoreArray(V, &v); CHKERRQ(ierr);
      ierr = VecRestoreArray(A, &a); CHKERRQ(ierr);

      // Forcing for the next time step
      double Frc[2];
      Frc[0] = std::cos((time+dt)/3.);
      Frc[1] = std::sin((time+dt)/2.);
      for(int i=0; i<2; ++i)
	{
	  ierr = VecSetValues(Fvec, 1, &i, &Frc[i], INSERT_VALUES); CHKERRQ(ierr);
	}
      ierr = VecAssemblyBegin(Fvec); CHKERRQ(ierr);
      ierr = VecAssemblyEnd(Fvec);   CHKERRQ(ierr);
  
      // Advance to the next time step
      TI.Advance(Fvec, dirichlet_values);
    }
  pfile.close();

  // Exact solution
  pfile.open((char*)"exsol.dat", std::ios::out);
  pfile << "#time U0 U1 V0 V1 A0 A1";
  const int N = 200;
  for(int i=0; i<=N; ++i)
    {
      double t = time*static_cast<int>(i)/static_cast<double>(N);
      Xsol(t, x, xdot, xddot);
      Ysol(t, y, ydot, yddot);
      pfile <<"\n"<<t << " "<<x<<" "<<y<<" "<<xdot<<" "<<ydot<<" "<<xddot<<" "<<yddot;
    }
  pfile.close();
  
  // clean up
  TI.Destroy();
  ierr = MatDestroy(&K);    CHKERRQ(ierr);
  ierr = MatDestroy(&M);    CHKERRQ(ierr);
  ierr = VecDestroy(&Fvec); CHKERRQ(ierr);
  PetscFinalize();
}

void Xsol(const double t, double& x, double& xdot, double& xddot)
{
  x = (315*std::cos(t/3.))/1496. - (3*std::cos(std::sqrt(2)*t))/17. - (3*std::cos(std::sqrt(5)*t))/88. + 
    (16*std::sin(t/2.))/133. - (std::sqrt(2)*std::sin(std::sqrt(2)*t))/21. + (2*std::sin(std::sqrt(5)*t))/(57.*std::sqrt(5));

  xdot = (8*std::cos(t/2.))/133. - (2*std::cos(std::sqrt(2)*t))/21. + (2*std::cos(std::sqrt(5)*t))/57. - 
    (105*std::sin(t/3.))/1496. + (3*std::sqrt(2)*std::sin(std::sqrt(2)*t))/17. + 
    (3*std::sqrt(5)*std::sin(std::sqrt(5)*t))/88.;

  xddot = (-35*std::cos(t/3.))/1496. + (6*std::cos(std::sqrt(2)*t))/17. + 
    (15*std::cos(std::sqrt(5)*t))/88. - (2*
			       (6*std::sin(t/2.) - 19*std::sqrt(2)*std::sin(std::sqrt(2)*t) + 7*std::sqrt(5)*std::sin(std::sqrt(5)*t)))/
    399.;
  
  return;
}

void Ysol(const double t, double& y, double& ydot, double& yddot)
{
  y = (81*std::cos(t/3.))/748. - (3*std::cos(std::sqrt(2)*t))/17. + (3*std::cos(std::sqrt(5)*t))/44. + 
    (44*std::sin(t/2.))/133. - (std::sqrt(2)*std::sin(std::sqrt(2)*t))/21. - 
    (4*std::sin(std::sqrt(5)*t))/(57.*std::sqrt(5));

  ydot = (22*std::cos(t/2.))/133. - (2*std::cos(std::sqrt(2)*t))/21. - (4*std::cos(std::sqrt(5)*t))/57. - 
    (3*(9*std::sin(t/3.) - 44*std::sqrt(2)*std::sin(std::sqrt(2)*t) + 17*std::sqrt(5)*std::sin(std::sqrt(5)*t)))/
    748.;

  yddot = (-9*std::cos(t/3.))/748. + (6*std::cos(std::sqrt(2)*t))/17. - (15*std::cos(std::sqrt(5)*t))/44. + 
    (-33*std::sin(t/2.) + 38*std::sqrt(2)*std::sin(std::sqrt(2)*t) + 28*std::sqrt(5)*std::sin(std::sqrt(5)*t))/
    399.;

  return;
}
  
