
/** \file pfe_ContactSpringParams.cpp
 * \brief Implements the struct pfe::ContactSpringParams
 * \author Ramsharan Rangarajan
 */

#include <pfe_ContactSpringParams.h>
#include <pfe_json.hpp>
#include <cassert>
#include <iomanip>

namespace pfe
{
  // write to a json file
  std::ostream& operator << (std::ostream &out, const ContactSpringParams& params)
  {
    assert(params.tag.empty()==false);

    nlohmann::json j;
    j[params.tag]["stiffness"] = params.stiffness;
    out << std::endl << std::endl << "\"" << params.tag << "\"" << " : " << std::setw(8) << j[params.tag];
									    
    return out;
  }

  // read from a json file
  std::istream& operator >> (std::istream &in, ContactSpringParams& params)
  {
    assert(params.tag.empty()==false);

    // rewind the file
    in.seekg(0, std::ios::beg);

    // parse
    auto J = nlohmann::json::parse(in);
    auto it = J.find(params.tag);
    assert(it!=J.end());
    auto& j = *it;
    
    // check fields
    assert(j.empty()==false);
    assert(j.contains("stiffness"));

    // read. use get_to() to enforce type
    j["stiffness"].get_to(params.stiffness);
    
    return in;
  }
}
