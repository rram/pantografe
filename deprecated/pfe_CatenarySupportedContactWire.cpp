
/** \file pfe_CatenarySupportedContactWire.cpp
 * \brief Implements the class pfe::CatenarySupportedContactWire
 * \author Ramsharan Rangarajan
 * Last modified: August 19, 2022
 */

#include <pfe_CatenarySupportedContactWire.h>
#include <pfe_OffsetLocalToGlobalMap.h>

namespace pfe
{
  // Constructor
  CatenarySupportedContactWire::
  CatenarySupportedContactWire(const CatenaryWireParams& cable_params,
			       const ContactWireParams& wire_params,
			       const IntercableDropperParams& dparams,
			       const DropperArrangement& dschedule,
    			       const int nelms_per_interval)
    :isSetup(false),
     nElements_per_Interval(nelms_per_interval),
     nElements((dschedule.nDroppers+1)*nelms_per_interval), // #intervals = #droppers+1
     nNodes(nElements+1),
     dropperSchedule(dschedule),
     catenary_cable(nullptr),
     contact_wire(nullptr),
     droppers{},
     pantographs{},
     catenary_L2GMap(nullptr),
     contact_L2GMap(nullptr),
     nCatenaryCableDofs(0),
     nContactWireDofs(0),
     nTotalDof(0)
  {
    assert(std::abs(cable_params.span-wire_params.span)<1.e-4);
    
    // Populate the coordinates array
    std::vector<double> coordinates(nNodes);
    coordinates[0] = 0.;
    const int nDroppers = dschedule.nDroppers;
    for(int ival=0, ncount=0; ival<nDroppers+1; ++ival)
      {
	const double xleft = (ival==0) ? 0. : dschedule.coordinates[ival-1];
	const double xright = (ival==nDroppers) ? cable_params.span : dschedule.coordinates[ival];
	const double dx = (xright-xleft)/static_cast<double>(nelms_per_interval);
	for(int e=0; e<nelms_per_interval; ++e, ++ncount)
	  coordinates[ncount+1] = coordinates[ncount] + dx;
      }
    assert(std::abs(coordinates.back()/cable_params.span-1.)<1.e-4);
    
    // Create the catenary cable
    catenary_cable = new HeavyTensionedBeam(cable_params, coordinates);

    // Create the contact wire
    contact_wire = new HeavyTensionedBeam(wire_params, coordinates);

    // Total number of dofs = catenary + contact
    nCatenaryCableDofs = catenary_cable->GetLocalToGlobalMap().GetTotalNumDof();
    nContactWireDofs   = contact_wire->GetLocalToGlobalMap().GetTotalNumDof();
    nTotalDof = nCatenaryCableDofs + nContactWireDofs;

    // local to global maps for the contact and catenary cables
    catenary_L2GMap = new OffsetLocalToGlobalMap(catenary_cable->GetLocalToGlobalMap(), 0);
    contact_L2GMap  = new OffsetLocalToGlobalMap(contact_wire->GetLocalToGlobalMap(),   nCatenaryCableDofs);
        
    // Index set for catenary cable
    std::vector<int> catenary_cable_dofs(nCatenaryCableDofs);
    for(int i=0; i<nCatenaryCableDofs; ++i)
      catenary_cable_dofs[i] = i;
    PetscErrorCode ierr;
    ierr = ISCreateGeneral(PETSC_COMM_WORLD, nCatenaryCableDofs, catenary_cable_dofs.data(), PETSC_COPY_VALUES, &IS_catenary_cable); CHKERRV(ierr);

    // Index set for the contact wire
    std::vector<int> contact_wire_dofs(nContactWireDofs);
    for(int i=0; i<nContactWireDofs; ++i)
      contact_wire_dofs[i] = nCatenaryCableDofs + i;
    ierr = ISCreateGeneral(PETSC_COMM_WORLD, nContactWireDofs, contact_wire_dofs.data(), PETSC_COPY_VALUES, &IS_contact_wire); CHKERRV(ierr);

    // Create inter-cable droppers
    droppers.resize(nDroppers);
    for(int d=0; d<nDroppers; ++d)
      {
	const int node = nelms_per_interval*(d+1);
	assert(std::abs(coordinates[node]/dschedule.coordinates[d]-1.)<1.e-4);
	const int topdof = node;                       // on the catenary
	const int botdof = nCatenaryCableDofs + node;  // on the contact wire
	droppers[d] = new IntercableDropper(dparams, dschedule.nominal_lengths[d], dschedule.zEncumbrance, topdof, botdof);
      }

    // done
  }

  // Destructor
  CatenarySupportedContactWire::~CatenarySupportedContactWire()
  {
    // Check that PETSc has not been finalized
    PetscBool isFinalized;
    auto ierr = PetscFinalized(&isFinalized); CHKERRV(ierr);
    assert(isFinalized==PETSC_FALSE);

    // Clean up
    delete catenary_cable;
    delete contact_wire;
    for(auto& d:droppers)    delete d;
    for(auto& p:pantographs) delete p;
    delete catenary_L2GMap;
    delete contact_L2GMap;
    PD.Destroy();
    linSolver.Destroy();
    ierr = ISDestroy(&IS_catenary_cable); CHKERRV(ierr);
    ierr = ISDestroy(&IS_contact_wire);   CHKERRV(ierr);
    
    // done
  }


  // Complete setup
  void CatenarySupportedContactWire::Setup()
  {
    assert(isSetup==false);
    
    // Concatenate sparsity of catenary + contact wire
    auto nz = catenary_cable->GetSparsity();
    auto contact_wire_nz = contact_wire->GetSparsity();
    nz.insert(nz.end(),                                        // nz <- nz + contact_wire_nz.
	      std::make_move_iterator(contact_wire_nz.begin()),
	      std::make_move_iterator(contact_wire_nz.end()));

    // for each pantograph:
    // (i)  get the sparsity for the independent dofs
    // (ii) get the number of dofs interacting with the beam 
    
    
    
    // Initialize PETSc data structures with known sparsity 
    PD.Initialize(nz);

    // Update state
    isSetup = true;
  }

  
  
  // Access the list of dropper nodes
  std::vector<int> CatenarySupportedContactWire::GetDropperNodes() const
  {
    assert(isSetup==true);
    const int ndroppers = static_cast<int>(droppers.size());
    std::vector<int> nodeList(ndroppers);
    for(int d=0; d<ndroppers; ++d)
      nodeList[d] = nElements_per_Interval*(d+1);
    return nodeList;      
  }

  // Determines the dropper displacement at the catenary and contact wires
  void CatenarySupportedContactWire::
  GetDropperDisplacements(const double* dofValues, double* catenary_disp, double* contact_disp) const
  {
    assert(isSetup==true);
    std::vector<int> dropper_nodes = GetDropperNodes();
    const int nDroppers = static_cast<int>(dropper_nodes.size());
    for(int d=0; d<nDroppers; ++d)
      {
	catenary_disp[d] = dofValues[dropper_nodes[d]];
	contact_disp[d]  = dofValues[dropper_nodes[d]+nCatenaryCableDofs];
      }
    return;
  }
    
  // Number of nonzero per row
  std::vector<int> CatenarySupportedContactWire::GetSparsity() const
  {
    assert(isSetup==true);
    
    // Concatenate sparsity of catenary + contact wire
    auto nz = catenary_cable->GetSparsity();
    auto contact_wire_nz = contact_wire->GetSparsity();
    nz.insert(nz.end(),                                        // nz <- nz + contact_wire_nz.
	      std::make_move_iterator(contact_wire_nz.begin()),
	      std::make_move_iterator(contact_wire_nz.end()));

    // TODO: Add pantograph dofs
    return std::move(nz);
  }

  
  //! Visualize the configuration
  void CatenarySupportedContactWire::Write(const double* displacements,
					   const std::string catenary_fname,
					   const std::string contact_fname,
					   const std::string dropper_fname) const
  {
    assert(isSetup==true);
    
    const auto& coordinates = catenary_cable->GetCoordinates();

    // catenary cable
    const auto& catenary_params = catenary_cable->GetParameters();
    std::fstream pfile;
    pfile.open(catenary_fname.c_str(), std::ios::out);
    assert(pfile.good());
    for(int n=0; n<nNodes; ++n)
      pfile << coordinates[n] <<" "<< catenary_params.zLevel + displacements[n] << std::endl;
    pfile.close();

    // contact wire
    const auto& contact_params = contact_wire->GetParameters();
    pfile.open(contact_fname.c_str(), std::ios::out);
    assert(pfile.good());
    for(int n=0; n<nNodes; ++n)
      pfile << coordinates[n] <<" "<< contact_params.zLevel + displacements[nCatenaryCableDofs+n] << std::endl;
    pfile.close();
    
    // droppers
    const auto dropperNodes = GetDropperNodes();
    const int nDroppers = static_cast<int>(dropperNodes.size());
    pfile.open(dropper_fname.c_str(), std::ios::out);
    assert(pfile.good());
    for(int n=0; n<nDroppers; ++n)
      {
	const int& node = dropperNodes[n];
	pfile << coordinates[node] <<" "<< catenary_params.zLevel + displacements[node]
	      << std::endl
	      << coordinates[node] <<" "<< contact_params.zLevel + displacements[nCatenaryCableDofs+node]
	      << std::endl
	      << std::endl;
      }
    pfile.close();

    // TODO: Add pantograph
    // done
    return;
  }

}
