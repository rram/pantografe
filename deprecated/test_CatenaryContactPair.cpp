
/** \file test_CatenaryContactPair.cpp
 * \brief Unit test for class pfe::CatenarySupportedContactWire
 * \author Ramsharan Rangarajan
 * Last modified: January 18, 2022
 */

#include <pfe_CatenarySupportedContactWire.h>
#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Benchmark study parameters
  std::fstream jfile;
  jfile.open((char*)"benchmark_params.json", std::ios::in);
  assert(jfile.is_open());
  
  // Catenary cable parameters
  pfe::CatenaryWireParams catenaryParams;
  catenaryParams.tag = "catenary cable";
  jfile >> catenaryParams;

  // Contact wire parameters
  pfe::ContactWireParams wireParams;
  wireParams.tag = "contact wire";
  jfile >> wireParams;

  // Dropper parameters
  pfe::IntercableDropperParams dropperParams;
  dropperParams.tag = "droppers";
  jfile >> dropperParams;

  // Dropper arrangement
  pfe::DropperArrangement dropperSchedule;
  dropperSchedule.tag = "dropper schedule";
  jfile >> dropperSchedule;

  // catenary suspension spring
  pfe::SuspensionSpringParams catenary_spring_params;
  catenary_spring_params.tag = "catenary cable suspension spring";
  jfile >> catenary_spring_params;
  
  // contact wire suspension spring
  pfe::SuspensionSpringParams contact_spring_params;
  contact_spring_params.tag = "contact wire suspension spring";
  jfile >> contact_spring_params;
  
  jfile.close();

  // Create an instance of the model
  const int nElements = 20;
  auto model = new pfe::CatenarySupportedContactWire(catenaryParams, wireParams,
						     dropperParams, dropperSchedule, nElements);
  const int nNodes = model->GetNumNodes();
  
  // Suspension springs at the ends of the catenary cable
  model->AddCatenarySuspensionSpring(catenary_spring_params, 0);
  model->AddCatenarySuspensionSpring(catenary_spring_params, nNodes-1);
  
  // Suspension springs at the ends of the contact wire
  model->AddContactWireSuspensionSpring(contact_spring_params, 0);
  model->AddContactWireSuspensionSpring(contact_spring_params, nNodes-1);

  // Finalize
  model->Setup();
  
  // displacements
  const int nDofs = model->GetTotalNumDof();
  std::vector<double> displacements(nDofs);

  // Dirichlet bcs 
  std::vector<std::pair<int,double>> catenary_bcs{}, contact_bcs{};
  
  // solve
  model->StaticSolve(0., catenary_bcs, contact_bcs, displacements.data());
  
  // Plot the catenary cable configuration
  const auto& coordinates = model->GetCoordinates();
  std::fstream pfile;
  pfile.open((char*)"catenary.dat", std::ios::out);
  const double* catenary_cable_disp = &displacements[0];
  for(int a=0; a<nNodes; ++a)
    pfile << coordinates[a]<<" "<<catenaryParams.zLevel+catenary_cable_disp[a] << "\n";
  pfile.close();

  // Plot contact wire configuration
  pfile.open((char*)"contact.dat", std::ios::out);
  const int& nCatenaryCableDofs = model->GetNumCatenaryCableDofs();
  const double* contact_wire_disp = &displacements[nCatenaryCableDofs];
  for(int a=0; a<nNodes; ++a)
    pfile << coordinates[a]<<" "<<wireParams.zLevel+contact_wire_disp[a] << "\n";
  pfile.close();
  
  // Clean up
  delete model;
  PetscFinalize();
}

