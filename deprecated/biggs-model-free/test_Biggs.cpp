
/** \file test_Biggs.cpp
 * \brief Test for the class pfe::Model_MovingLoad
 * \author Ramsharan Rangarajan
 * Exact solution on Pg 318 of Biggs's book.
 */

#include <pfe_Model_MovingLoad.h>
#include <pfe_Newmark.h>
#include <iostream>
#include <fstream>
#include <filesystem>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL); CHKERRQ(ierr);

  // Cable parameters
  pfe::HeavyTensionedBeamParams beam_params;
  beam_params.tag      = "beam";
  beam_params.span     = 40.0*12.0;
  beam_params.EI       = 2.0e10;
  beam_params.Tension  = 0.0;
  beam_params.rhoA     = 0.1;
  beam_params.g        = 0.0;
  beam_params.zLevel   = 0.;

  // Pantograph parameters
  pfe::Pantograph_0_Params panto_params;
  panto_params.tag = "panto";
  panto_params.xinit = 0.0;
  panto_params.speed = 50.0*12.0;
  panto_params.force = -8470.0;
  
  // Create the cable
  const int nElements = 100; 
  auto beam = new pfe::HeavyTensionedBeam(beam_params, nElements);

  // clamp the left end of the cable
  const int nNodes = beam->GetNumNodes();
  const std::vector<int> dirichlet_dofs{0, nNodes-1};

  // Create time integrator
  const double timestep = 0.0025;
  pfe::Newmark TI(beam->GetTotalNumDof(), timestep);
  
  // Create an instance of the model
  auto model = new pfe::Model_Cable_MovingLoad(*beam, panto_params, TI, dirichlet_dofs);
  const int nTotalDof = model->GetTotalNumDof();
    
  // Static solution
  model->StaticInitialize();

  // visualize
  //model->Write({"cable-0.dat"}, "load-0.dat");

  // History
  std::fstream hfile;
  hfile.open("history.dat", std::ios::out);
  assert(hfile.good());

  // # time steps
  const double tend        = beam_params.span/panto_params.speed;
  const int num_time_steps = static_cast<int>(tend/timestep)-1;

  // Normalizing factor
  const double norm = -panto_params.force*std::pow(beam_params.span,3.)/(48.*beam_params.EI);
  
  // Moving pantograph
  for(int tstep=0; tstep<num_time_steps; ++tstep)
    {
      // record the mid-span deflection
      assert(nNodes%2==1);
      const int mid_dof = (nNodes-1)/2;
      double mid_U;
      VecGetValues(model->GetState().U, 1, &mid_dof, &mid_U);
      hfile << model->GetTime() << "\t" << mid_U/norm << std::endl;

      // advance to the next instant
      std::cout << "Time step " << tstep << std::endl;
      model->Advance();
      std::string tstr = std::to_string(tstep+1)+".dat";
      //model->Write({"cable-"+tstr}, "load-"+tstr);
    }
  
  // Clean up
  TI.Destroy();
  delete model;
  delete beam;
  PetscFinalize();
}
