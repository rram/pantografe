
/** \file pfe_MultiSpan_OHE.h
 * \brief Defines the class pfe::MultiSpan_OHE encapsulating multiple span OHE
 * \author Ramsharan Rangarajan
 */


#pragma once

#include <pfe_linear_SingleSpan_OHE.h>
#include <map>

namespace pfe
{
  //! Multiple OHE spans
  //! \ingroup components
  class MultiSpan_OHE
  {
  public:
    //! Constructor
    MultiSpan_OHE(const double x0,  const OHESpan_ParameterPack& pack,
		 const int nspans, const int nelm_per_interval);

    //! Destructor
    virtual ~MultiSpan_OHE();

    //! Disable copy and assignment
    MultiSpan_OHE(const MultiSpan_OHE&) = delete;
    MultiSpan_OHE operator=(const MultiSpan_OHE&) = delete;

    //! Access a specified OHE span
    inline const linear::SingleSpan_OHE& GetSpan(const int num) const
    {
      assert(num>=0 && num<num_spans);
      return *ohe_spans[num];
    }

    //! Access the total number of dofs
    inline int GetTotalNumDof() const
    { return nTotalDof; }

    //! Access the number of spans
    inline int GetNumSpans() const
    { return num_spans; }
    
    //! Main functionlity: evaluate contribution to mass, stiffness and force vectors
    void Evaluate(Mat& M, Mat& K, Vec& F) const;

    //! Get the active displacement dofs and shape function values at a given point along the span
    std::array<std::pair<int,double>,2> GetActiveDisplacementDofs(const double x) const;

    //! Get the active dofs and shape function values at a given point along the span
    std::array<std::pair<int,double>,4> GetActiveDofs(const double x) const;
    
    // Number of nonzero's per row
    std::vector<int> GetSparsity() const;
    
    // Visualize the configuration
    void Write(const double* displacements,
	       const std::string catenary_fname,
	       const std::string contact_fname,
	       const std::string dropper_fname) const;

    void Write(const double* disp, const double* vel, const double* accn,
	       const std::string catenary_fname,
	       const std::string contact_fname,
	       const std::string dropper_fname) const;

    inline void Write(const double* disp, const double* vel, const double* accn,
		      const std::vector<std::string> fnames) const
    {
      assert(static_cast<int>(fnames.size())==3);
      Write(disp, vel, accn, fnames[0], fnames[1], fnames[2]);
    }
      
  private:
    const double          xinit;       //! start of the first span
    const int             num_spans;   //! number of spans
    const double          span_length; //! length of each span
    std::vector<linear::SingleSpan_OHE*> ohe_spans;   //! vector of OHE spans
    std::vector<IS>       ohe_IS;      //! corresponding vector of index sets
    int                   nTotalDof;   //! total number of dofs
    
    std::vector<std::map<int,int>> contact_wire_dof_maps; //!< span-wise local to global map for contact wire dofs
  };
}

