
/** \file test_nonlinear_multispan_StaticBenchmark_DropperLengthAndStiffness.cpp
 * \brief Unit test for class pfe::nonlinear::Model_Static_MultiSpan_OHE
 * \author Ramsharan Rangarajan
 */


#include <pfe_nonlinear_Model_Static_MultiSpan_OHE.h>
#include <filesystem>
#include <iostream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Benchmark study parameters
  pfe::OHESpan_ParameterPack pack;
  std::fstream jfile;
  assert(std::filesystem::exists("benchmark_params.json"));
  jfile.open("benchmark_params.json", std::ios::in);
  assert(jfile.is_open());
  jfile >> pack;
  jfile.close();
  
  // Create 3 spans
  // Use middle span for dropper adjustment and stiffness calculation
  const int nElements = 10;
  const int num_spans = 3;
  const int calc_span = 1;
  auto ohe = new pfe::nonlinear::MultiSpan_OHE(0., pack, num_spans, nElements, 1.e5);

  // Create an instance of the static model
  auto model = new pfe::nonlinear::Model_Static_MultiSpan_OHE(*ohe);

  // Adjust dropper lengths to achieve sag over 20 iterations using span #1
  model->AdjustDropperLengths(calc_span, 20);
  
  // Solve at the adjusted dropper lengths
  const int nTotalDof = model->GetTotalNumDof();
  std::vector<double> displacements(nTotalDof);
  model->Solve(displacements.data()); 
  model->Write(displacements.data(), "catenary.dat", "contact.dat", "droppers.dat");

  /*
  // Access the achieved sag at the dropper nodes
  std::vector<double> dropper_catenary_disp(pack.dropperSchedule.nDroppers);
  std::vector<double> dropper_contact_disp(pack.dropperSchedule.nDroppers);
  ohe_span->GetDropperDisplacements(displacements.data(), dropper_catenary_disp.data(), dropper_contact_disp.data());

  // Compare target and achieved displacements
  std::fstream pfile;
  pfile.open("dropper-sag.dat", std::ios::out);
  assert(pfile.good());
  pfile <<"# dropper-index x-dropper, target sag, realized sag, nominal length, adjusted length, catenary sag";
  for(int d=0; d<pack.dropperSchedule.nDroppers; ++d)
    pfile<<"\n"<<d<<" "<<pack.dropperSchedule.coordinates[d]<<" "
	 <<pack.dropperSchedule.target_sag[d]<<" "<<dropper_contact_disp[d]<<" "
	 <<pack.dropperSchedule.nominal_lengths[d]<<" "<<ohe_span->GetDroppers()[d]->GetLength()<<" "
	 <<dropper_catenary_disp[d];
  pfile.close();
  */
    
  // Compute the transverse stiffness
  const auto& coordinates = ohe->GetSpan(calc_span).ContactWire().GetCoordinates();
  const int nNodes = ohe->GetSpan(calc_span).ContactWire().GetNumNodes();
  std::vector<double> stiffness(nNodes);
  std::cout << "Computing transverse stiffness: " << std::endl;
  model->ComputeTransverseStiffnessDistribution(calc_span, displacements.data(), 200., stiffness.data());
  std::fstream pfile;
  pfile.open("stiffness.dat", std::ios::out);
  for(int a=0; a<nNodes; ++a)
    pfile << coordinates[a]<<" "<<stiffness[a]<<"\n";
  pfile.close();
  
  // Clean up
  delete ohe;
  delete model;
  PetscFinalize();
}
