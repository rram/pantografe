
/** \file test_nonlinear_singlespan_StaticBenchmark_Deflection.cpp
 * \brief Unit test for class pfe::nonlinear::Model_Static_SingleSpan_OHE
 * \author Ramsharan Rangarajan
 */

#include <pfe_nonlinear_Model_Static_SingleSpan_OHE.h>
#include <filesystem>
#include <iostream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Benchmark study parameters
  pfe::OHESpan_ParameterPack pack;
  std::fstream jfile;
  assert(std::filesystem::exists("benchmark_params.json"));
  jfile.open("benchmark_params.json", std::ios::in);
  assert(jfile.is_open());
  jfile >> pack;
  jfile.close();
  
  // Create a single span
  const int nElements = 10;
  auto ohe_span = new pfe::nonlinear::SingleSpan_OHE(0., pack, 1.e5, nElements);
  
  // Create an instance of the model
  auto model = new pfe::nonlinear::Model_Static_SingleSpan_OHE(*ohe_span);
  
  // Solve
  const int nTotalDof = model->GetTotalNumDof();
  std::vector<double> displacements(nTotalDof);
  model->Solve(displacements.data());
  
  // Visualize
  model->Write(displacements.data(), "catenary.dat", "contact.dat", "droppers.dat");
  
  // Clean up
  delete model;
  delete ohe_span;
  PetscFinalize();
}
