
/** \file pfe_nonlinear_Model_Static_MultiSpan_OHE.cpp
 * \brief Function specialization for pfe::nonlinear::Model_Static_MultiSpan_OHE to simulate static behavior of a multispan of the OHE
 * \author Ramsharan Rangarajan
 */

#include <pfe_nonlinear_Model_Static_MultiSpan_OHE.h>

namespace pfe
{
  namespace nonlinear
  {
    template<>
    void Model_Static_OHE<MultiSpan_OHE>::AdjustDropperLengths(const int span_num,
							       const int nIters)
    {
      // access this span
      auto& ohe_span = ohe.GetSpan(span_num);
      auto& droppers = ohe_span.GetDroppers();
      const int nDroppers = static_cast<int>(droppers.size());
      
      // target sag
      const auto& target_sag = ohe_span.GetDropperArrangement().target_sag;

      // length iterations
      const int nTotalDof = ohe.GetTotalNumDof();
      const int num_spans = ohe.GetNumSpans();
      std::vector<double> displacements(nTotalDof, 0.);
      std::vector<double> dropper_contact_disp(nDroppers);
      std::vector<double> dropper_catenary_disp(nDroppers);
      std::vector<double> dropper_lengths(nDroppers);

      // dropper dofs
      auto dropper_dofs = ohe.GetDropperDofs(span_num);

      // iterate
      for(int iter=0; iter<=nIters; ++iter)
	{
	  // solve with the current state
	  Solve(displacements.data());

	  // update dropper lengths
	  for(int d=0; d<nDroppers; ++d)
	    {
	      double diff = target_sag[d]-displacements[dropper_dofs[d].second];
	      dropper_lengths[d] = droppers[d]->GetLength()-diff;
	      assert(dropper_lengths[d]>0.);
	    }
	  
	  // set new dropper lengths for all spans
	  for(int s=0; s<num_spans; ++s)
	    for(int d=0; d<nDroppers; ++d)
	      ohe.GetSpan(s).ResetDropperLengths(dropper_lengths.data());
	}

      // done
      return;
    }


    // Compute transverse stiffness distribution
    template<>
    void Model_Static_OHE<MultiSpan_OHE>::
    ComputeTransverseStiffnessDistribution(const int span_num, const double* displacements,
					   const double Force, double* stiffness) const
    {
      assert(span_num>=0 && span_num<ohe.GetNumSpans());

      // Apply the given force at each node of the contact wire along the span
      // Measure the displacement and compute the stiffness
      auto& ohe_span = ohe.GetSpan(span_num);
      const int nLoadNodes = ohe_span.ContactWire().GetNumNodes();


      SNES_Context ctx;
      ctx.ohe        = &ohe;
      ctx.petsc_data = &petsc_data;

      const int nTotalDof = ohe.GetTotalNumDof();
      std::vector<double> forced_disp(nTotalDof);
      std::copy(displacements, displacements+nTotalDof, forced_disp.begin());

      // linear backtracking
      SNESLineSearch ls;
      PetscErrorCode ierr;
      ierr = SNESGetLineSearch(snes_solver.snes, &ls); CHKERRV(ierr);
      ierr = SNESLineSearchSetOrder(ls, 1);            CHKERRV(ierr);
      ierr = SNESSetFromOptions(snes_solver.snes);     CHKERRV(ierr);

      // load stepping for the first node
      for(int lstep=0; lstep<5; ++lstep)
	{
	  ctx.force = Force*static_cast<double>(lstep)/5.;
	  ctx.dofnum = ohe.MapContactWireNodeToDof(span_num, 0);
	  snes_solver.Solve(forced_disp.data(), &ctx, true);
	}

      // apply specified force sequentially along the contact wire
      ctx.force = Force;
      for(int lnode=0; lnode<nLoadNodes; ++lnode)
	{
	  ctx.dofnum = ohe.MapContactWireNodeToDof(span_num, lnode);

	  // solve
	  snes_solver.Solve(forced_disp.data(), &ctx, true);

	  // increment in displacement due to the force
	  const double delta_disp = forced_disp[ctx.dofnum]-displacements[ctx.dofnum];

	  // stiffness
	  assert(std::abs(delta_disp)>1.e-9);
	  stiffness[lnode] = Force/delta_disp;
	}

      // done
      return;
    }
    
  }
}

