
/** \file pfe_nonlinear_Model_Static_SingleSpan_OHE.h
 * \brief Defines the class pfe::nonlinear::Model_Static_SingleSpan_OHE to simulate static behavior of a single span of the OHE
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_nonlinear_Model_Static_OHE.h>

namespace pfe
{
  namespace nonlinear
  {
    //! instantiation
    using Model_Static_SingleSpan_OHE = Model_Static_OHE<SingleSpan_OHE>;
  }
}
