
/** \file pfe_nonlinear_Model_Static_MultiSpan_OHE.h
 * \brief Defines the class pfe::nonlinear::Model_Static_MultiSpan_OHE to simulate static behavior of a multispan of the OHE
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_nonlinear_Model_Static_OHE.h>
#include <pfe_nonlinear_MultiSpan_OHE.h>

namespace pfe
{
  namespace nonlinear
  {
    //! instantiation
    using Model_Static_MultiSpan_OHE = Model_Static_OHE<MultiSpan_OHE>;
  }
}
