
# add library
add_library(pfe_nonlinear_model_static_ohe STATIC
   		 pfe_nonlinear_Model_Static_SingleSpan_OHE.cpp
		 pfe_nonlinear_Model_Static_MultiSpan_OHE.cpp)
		 

# headers
target_include_directories(pfe_nonlinear_model_static_ohe PUBLIC
  				      $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)

# link
target_link_libraries(pfe_nonlinear_model_static_ohe PUBLIC pfe_nonlinear_ohe)

# Add required flags
target_compile_features(pfe_nonlinear_model_static_ohe PUBLIC ${pfe_COMPILE_FEATURES})

install(FILES
	pfe_nonlinear_Model_Static_OHE.h
	pfe_nonlinear_Model_Static_OHE_impl.h
	pfe_nonlinear_Model_Static_SingleSpan_OHE.h
	#pfe_nonlinear_Model_Static_MultiSpan_OHE.h
	DESTINATION ${PROJECT_NAME}/include)

