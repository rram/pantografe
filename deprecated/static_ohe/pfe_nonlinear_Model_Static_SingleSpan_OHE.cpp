
/** \file pfe_nonlinear_Model_Static_SingleSpan_OHE.cpp
 * \brief Function specialization for pfe::nonlinear::Model_Static_SingleSpan_OHE to simulate static behavior of a single span of the OHE
 * \author Ramsharan Rangarajan
 */

#include <pfe_nonlinear_Model_Static_SingleSpan_OHE.h>
#include <iostream>

namespace pfe
{
  namespace nonlinear
  {
    template<>
      void Model_Static_OHE<SingleSpan_OHE>::AdjustDropperLengths(const int span_num, const int nIters)
      {
	assert(span_num==0);

	const auto& droppers = ohe.GetDroppers();
	const auto& schedule = ohe.GetDropperArrangement();
	const int nDroppers = static_cast<int>(droppers.size());
	const auto& target_sag = schedule.target_sag;


	const int nTotalDof = ohe.GetTotalNumDof();
	std::vector<double> displacements(nTotalDof);
	std::vector<double> dropper_contact_disp(nDroppers);
	std::vector<double> dropper_catenary_disp(nDroppers);
	std::vector<double> lengths(nDroppers);
    
	// Iterate over dropper lengths
	for(int iter=0; iter<=nIters; ++iter)
	  {    
	    // solve in the current state
	    Solve(displacements.data());

	    // Get sag values at the contact wire along dropper connections
	    ohe.GetDropperDisplacements(&displacements[0],
					&dropper_catenary_disp[0],
					&dropper_contact_disp[0]);

	    // Adjust dropper lengths towards achieving the target
	    for(int d=0; d<nDroppers; ++d)
	      {
		lengths[d] = droppers[d]->GetLength();
		lengths[d] -= (target_sag[d]-dropper_contact_disp[d]);
		assert(lengths[d]>0.);
	      }

	    // Set the new dropper lengths
	    ohe.ResetDropperLengths(lengths.data());
	  }

	// done
	return;
      }


    // Compute transverse stiffness disrtribution
    template<>
      void Model_Static_OHE<SingleSpan_OHE>::
      ComputeTransverseStiffnessDistribution(const int span_num, const double* displacements,
					     const double Force, double* stiffness) const
      {
	assert(span_num==0);
    
	// Apply the given force at each node of the contact wire along the span
	// Measure the displacement and compute the stiffness
	const auto& contact_wire = ohe.ContactWire();
	const int nNodes = contact_wire.GetNumNodes();
	const int nTotalDof = ohe.GetTotalNumDof();

	SNES_Context ctx;
	ctx.ohe        = &ohe;
	ctx.petsc_data = &petsc_data;
	ctx.force      = Force;

	std::vector<double> forced_disp(nTotalDof);
	std::copy(displacements, displacements+nTotalDof, forced_disp.begin());

	// linear backtracking (default is 3, which occasionally fails)
	SNESLineSearch ls;
	PetscErrorCode ierr;
	ierr = SNESGetLineSearch(snes_solver.snes, &ls); CHKERRV(ierr);
	ierr = SNESLineSearchSetOrder(ls, 1);            CHKERRV(ierr);
	ierr = SNESSetFromOptions(snes_solver.snes);     CHKERRV(ierr);

	// load stepping for the first node
	for(int lstep=0; lstep<5; ++lstep)
	  {
	    ctx.force = Force*static_cast<double>(lstep)/5.;
	    ctx.dofnum = ohe.GetActiveContactWireDof(0);
	    snes_solver.Solve(forced_disp.data(), &ctx, true);
	  }
	
	// Apply specified force
	ctx.force = Force;
	
	// Loop over the loading node
	for(int lnode=0; lnode<nNodes; ++lnode)
	  {
	    // Add transverse force at "lnode" on the contact wire
	    ctx.dofnum = ohe.GetActiveContactWireDof(lnode);
	  	  
	    // solve
	    snes_solver.Solve(forced_disp.data(), &ctx, true);
	    
	    // relative displacement
	    const double delta_disp = forced_disp[ctx.dofnum]-displacements[ctx.dofnum];
	    
	    // Compute the stiffness
	    assert(std::abs(delta_disp)>1.e-9);
	    stiffness[lnode] = Force/delta_disp;
	  }

	// done
	return;
      }

  }
}
