
/** \file test_LinearBasisFunctions.cpp
 * \brief Unit test for linear basis functions
 * \author Ramsharan Rangarajan
 * Last modified: September 13, 2021
 */

#include <pfe_BasisFunctions.h>
#include <pfe_SegmentGeometry.h>
#include <pfe_LinearShape.h>
#include <pfe_LineQuadratures.h>
#include <random>

using namespace pfe;

template<class GeomType>
void TestBasisFunctions(const BasisFunctions& basis,
			const GeomType& geom,
			const LinearShape<1>& shp,
			const Quadrature& qrule);

int main()
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-1.,1.);

  for(int trial=0; trial<10; ++trial)
    {
      LinearShape<1> LinShp;   // shape functions

      // 1D (only)
      std::vector<double> gcoord_1(2);
      gcoord_1[0] = dis(gen);
      gcoord_1[1] = 2.+dis(gen);
      SegmentGeometry<1>::SetGlobalCoordinatesArray(gcoord_1);  // geometry
      SegmentGeometry<1> seg_1(1, 2);

      BasisFunctions basis_1(&LineQuadratures::Line_2pt, &LinShp, &seg_1); // basis functions
      TestBasisFunctions(basis_1, seg_1, LinShp, LineQuadratures::Line_2pt);

      BasisFunctions basis_2(&LineQuadratures::Line_3pt, &LinShp, &seg_1); // basis functions
      TestBasisFunctions(basis_2, seg_1, LinShp, LineQuadratures::Line_3pt);

      BasisFunctions basis_3(&LineQuadratures::Line_4pt, &LinShp, &seg_1); // basis functions
      TestBasisFunctions(basis_3, seg_1, LinShp, LineQuadratures::Line_4pt);
    }    
  
}


template<class GeomType>
void TestBasisFunctions(const BasisFunctions& basis,
			const GeomType& geom,
			const LinearShape<1>& shp,
			const Quadrature& qrule)
{
  const int SPD = geom.GetEmbeddingDimension();
  assert(basis.GetSpatialDimension()==SPD);
  assert(basis.GetBasisDimension()==shp.GetNumberOfFunctions());
  assert(basis.GetNumberOfDerivativesPerFunction()==geom.GetParametricDimension());

  // Evaluated quadrature, shape functions and derivatives
  auto& ShpValues = basis.GetShapes();
  auto& dShpValues = basis.GetDShapes();
  auto& Qpts = basis.GetQuadraturePointCoordinates();
  auto& Qwts = basis.GetIntegrationWeights();

  // Check sizes
  const int nShape = shp.GetNumberOfFunctions();
  const int nQuad = qrule.GetNumberQuadraturePoints();
  const int pDim = geom.GetParametricDimension();
  const int eDim = geom.GetEmbeddingDimension();
  assert(static_cast<int>(ShpValues.size())==nShape*nQuad);
  assert(static_cast<int>(dShpValues.size())==nShape*nQuad*pDim);
  assert(static_cast<int>(Qwts.size())==nQuad);
  assert(static_cast<int>(Qpts.size())==eDim*nQuad);

  for(int q=0; q<nQuad; ++q)
    {
      // Check shape function values
      const double qpt = qrule.GetQuadraturePoint(q)[0];
      for(int a=0; a<nShape; ++a)
	{
	  const double Na = shp.Val(a, &qpt);
	  assert(Na>=0. && Na<=1.);
	  assert(std::abs(Na-ShpValues[nShape*q+a])<1.e-5);
	}

      // Check partition of unity
      double shp_sum = 0.;
      double dshp_sum = 0.;
      for(int a=0; a<nShape; ++a)
	{
	  shp_sum += ShpValues[nShape*q+a];
	  dshp_sum += dShpValues[nShape*q+a]; // Assumes pDim = 1
	}
      assert(std::abs(shp_sum-1.)<1.e-5);
      assert(std::abs(dshp_sum-0.)<1.e-5);

      // Check integration point coordinates (isoparametric calculation)
      double Xq = 0.;
      for(int a=0; a<nShape; ++a)
	{
	  double Y;
	  geom.Map(qrule.GetQuadraturePoint(q), &Y);
	  Xq += ShpValues[nShape*q+a]*Y;
	}
      assert(std::abs(Xq-Qpts[q])<1.e-5);

      // Check integration weights
      double Y0, Y1;
      double zero = 0.;
      double one = 1.;
      geom.Map(&zero, &Y0);
      geom.Map(&one, &Y1);
      const double len = std::abs(Y1-Y0);
      double wsum = 0.;
      for(int q=0; q<nQuad; ++q)
	{
	  assert(std::abs(Qwts[q]-len*qrule.GetQuadratureWeights(q))<1.e-5);
	  wsum += Qwts[q];
	}
      assert(std::abs(wsum-len)<1.e-5);
    }
}
