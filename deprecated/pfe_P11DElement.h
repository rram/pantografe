
/** \file pfe_P11DElement.h
 * \brief Defines the class pfe::P11DElement and pfe::P11DL2GMap
 * \author Ramsharan Rangarajan
 * Last modified: September 24, 2021
 */


#ifndef PFE_P11DELEMENT_H
#define PFE_P11DELEMENT_H

#include <pfe_Element.h>
#include <pfe_SegmentGeometry.h>
#include <pfe_LinearShape.h>
#include <pfe_LineQuadratures.h>
#include <pfe_LocalToGlobalMap.h>
#include <cassert>

namespace pfe
{
  //! Linear 1D elements
  template<int nFields>
    class P11DElement: public Element
    {
    public:
      //! Constructor
      //! \param[in] i1, i2 Global numbering for nodes
      inline P11DElement(int i1, int i2)
	:Element()
	{
	  SegGeom = new SegmentGeometry<1>(i1, i2);
	  LinearShape<1> LinShp;
	  BasisFunctions SE(&LineQuadratures::Line_2pt, &LinShp, SegGeom);
	  AddBasisFunctions(SE);
	  for(int i=0; i<nFields; ++i)
	    AppendField(i);
	}

      // Disable copy and assignment
      P11DElement(const P11DElement<nFields>&) = delete;
      P11DElement<nFields>& operator=(const P11DElement<nFields>&) = delete;

      //! Destructor
      inline ~P11DElement()
      { delete SegGeom; }

      //! Returns the element geometry
      const SegmentGeometry<1>& GetElementGeometry() const override
      { return *SegGeom; }

      //! No 2nd derivatives of shape functions
      inline const std::vector<double>& GetD2Shape(int field) const override
      { assert(false);
	return empty_; }

      //! No 2nd derivatives of shape functions
      inline double GetD2Shape(const int f, const int q,
			       const int a, const int i, const int j) const override
      { assert(false);
	return 0.; }
      
    protected:
      int GetFieldIndex(int fields) const override
      { return 0; }

    private:
      SegmentGeometry<1>* SegGeom;
      static const std::vector<double> empty_;
    };

  template<int NFields>
    const std::vector<double> P11DElement<NFields>::empty_ = std::vector<double>{};



  /**
     \brief P11DL2GMap class: standard local to global map when only P11D type of 
     Elements are used. 
   
     P11DL2GMap assumes that\n
     1) The int of a node is an int\n
     2) All degrees of freedom are associated with nodes, and their values for each 
     node ordered consecutively according to the field number.
   
     Consequently, the GlobalDofIndex of the degrees of freedom of node N with NField 
     fields are given by
   
     (N-1)*NFields + field-1, where \f$ 1 \le \f$ fields \f$ \le \f$ NFields
  */
  class P11DL2GMap: public LocalToGlobalMap
  {
  public:
    //! @param EA array where the Element objects are. All GlobalElementIndex
    //! indices in this class refer to positions in EA. \n
    //! EA is not copied, only linked to. Of course, it is not destroyed upon
    //! destruction of P11DL2GMap objects.
    inline P11DL2GMap(const std::vector<Element*>& EA)
      :LocalToGlobalMap(),
      ElmArray(EA),
      NFields(ElmArray[0]->GetNumFields()),
      nDof(ElmArray[0]->GetNumDof(0))
	{
	  assert(nDof==2);
	  // Compute the total # dofs
	  int MaxNodeNumber = 0;
	  for(auto& Elm:ElmArray)
	    {
	      const auto& conn = Elm->GetElementGeometry().GetConnectivity();
	      for(auto& n:conn)
		if(MaxNodeNumber<n)
		  MaxNodeNumber = n;
	    }
	  nTotalDof = MaxNodeNumber*NFields;
	}

    //! Destructor
    inline virtual ~P11DL2GMap() {}

    //! Disable copy and assignment
    P11DL2GMap(const P11DL2GMap&) = delete;
    P11DL2GMap operator=(const P11DL2GMap) = delete;
    
    inline int Map(const int field, const int dof, const int elm) const override
    {
      assert(field<NFields && dof>=0 && dof<nDof);
      int nodenum = ElmArray[elm]->GetElementGeometry().GetConnectivity()[dof]-1;
      return NFields*nodenum + field; 
    }
  
    inline int GetNumElements() const override
    { return static_cast<int>(ElmArray.size()); }

    inline int GetNumFields(const int elm) const override
    { return ElmArray[elm]->GetNumFields(); }
  
    inline int GetNumDofs(const int elm, const int field) const override
    { return ElmArray[elm]->GetNumDof(field); }
  
    inline int GetTotalNumDof() const override
    { return nTotalDof; }
  

  protected:
    //! Access to ElementArray  for derived classes
    const std::vector<Element*>& GetElementArray() const
    { return ElmArray; }

  private:
    const std::vector<Element*>& ElmArray;
    const int NFields;
    const int nDof;
    int nTotalDof;
  };

}


#endif
