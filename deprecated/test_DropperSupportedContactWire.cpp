
/** \file test_DropperSupportedContactWire.cpp
 * \brief Unit test for the class pfe::DropperSupportedContactWire
 * \author Ramsharan Rangarajan
 * Last modified: October 19, 2021
 */

#include <pfe_DropperSupportedContactWire.h>
#include <fstream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Catenary parameters
  pfe::ContactWireParams wire_params{.span=55., .EI=195., .Tension=22000., .rhoA=1.35, .g=9.81, .zLevel=0.};
  const int nElements = 100; // keep even to position the dropper in the middle

  // Dropper parameters
  pfe::OverhangDropperParams dropper_params{.EA=1.e6, .length=1., .coord=0., .zEncumbrance=1.};
  std::vector<pfe::OverhangDropperParams> droppers{dropper_params, dropper_params, dropper_params};
  droppers[0].coord = 13.75;
  droppers[1].coord = 27.5;
  droppers[2].coord = 41.25;

  // Instance of model
  auto wire = new pfe::DropperSupportedContactWire(wire_params, nElements, droppers);

  // Solve
  const int nNodes = nElements+1;
  std::vector<std::pair<int,double>> dirichlet_bcs{};
  dirichlet_bcs.push_back( std::make_pair(0, 0.) );
  dirichlet_bcs.push_back( std::make_pair(nNodes-1, 0.) );
  std::vector<double> displacements(wire->GetLocalToGlobalMap().GetTotalNumDof());
  wire->Solve(dirichlet_bcs, displacements.data());

  // Print solution to file
  auto coordinates = wire->GetCoordinates();
  std::fstream pfile;
  pfile.open((char*)"wire-sol.dat", std::ios::out);
  for(int n=0; n<nNodes; ++n)
    pfile<<coordinates[n]<<" "<<wire_params.zLevel+displacements[n]<<"\n";
  pfile.close();

  // Clean up
  delete wire;

  // Finalize PETSc
  PetscFinalize();
}
