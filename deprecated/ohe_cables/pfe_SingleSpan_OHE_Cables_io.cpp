
/** \file pfe_SingleSpan_OHE_Cables_io.cpp
 * \brief Implements the class pfe::SingleSpan_OHE_Cables
 * \author Ramsharan Rangarajan
 */

#include <pfe_SingleSpan_OHE_Cables.h>

namespace pfe
{
  // read the parameter pack from a json file
  std::istream& operator >> (std::istream& in, OHESpan_ParameterPack& p)
  {
    // catenary cable
    p.catenaryParams.tag = "catenary cable";
    in >> p.catenaryParams;

    // contact wire
    p.wireParams.tag = "contact wire";
    in >> p.wireParams;

    // dropper parameters
    p.dropperParams.tag = "droppers";
    in >> p.dropperParams;

    // dropper arrangement
    p.dropperSchedule.tag = "dropper schedule";
    in >> p.dropperSchedule;

    // catenary cable suspension spring
    p.catenary_spring_params.tag = "catenary cable suspension spring";
    in >> p.catenary_spring_params;

    // contact wire suspension spring
    p.contact_spring_params.tag = "contact wire suspension spring";
    in >> p.contact_spring_params;

    return in;
  }

}
