
/** \file pfe_SingleSpan_OHE_Cables.cpp
 * \brief Implements the class pfe::SingleSpan_OHE_Cables
 * \author Ramsharan Rangarajan
 */

#include <pfe_SingleSpan_OHE_Cables.h>
#include <pfe_OffsetLocalToGlobalMap.h>
#include <fstream>

namespace pfe
{
  // Constructor 
  SingleSpan_OHE_Cables::SingleSpan_OHE_Cables(const double x0,
					       const CatenaryWireParams&     cable_params, 
					       const ContactWireParams&      wire_params, 
					       const DropperParams&          dparams, 
					       const DropperArrangement&     dschedule,
					       const SuspensionSpringParams& left_catenary_spring_params,
					       const SuspensionSpringParams& right_catenary_spring_params,
					       const SuspensionSpringParams& left_contact_spring_params,
					       const SuspensionSpringParams& right_contact_spring_params,
					       const int nelms_per_interval)
  :nElements_per_Interval(nelms_per_interval),
   nElements((dschedule.nDroppers+1)*nelms_per_interval), // #intervals = #droppers+1
   nNodes(nElements+1),
   dropperSchedule(dschedule),
   catenary_cable(nullptr),
   contact_wire(nullptr),
   catenary_L2GMap(nullptr),
   contact_L2GMap(nullptr),
   nTotalDof(0)
  {
    assert(std::abs(cable_params.span-wire_params.span)<1.e-4);
    
    // Populate the coordinates array
    std::vector<double> coordinates(nNodes);
    coordinates[0] = x0;
    const int nDroppers = dschedule.nDroppers;
    for(int ival=0, ncount=0; ival<nDroppers+1; ++ival)
      {
	double xleft = (ival==0) ? 0. : dschedule.coordinates[ival-1];
	double xright = (ival==nDroppers) ? cable_params.span : dschedule.coordinates[ival];
	xleft  += x0;
	xright += x0;
	const double dx = (xright-xleft)/static_cast<double>(nelms_per_interval);
	for(int e=0; e<nelms_per_interval; ++e, ++ncount)
	  coordinates[ncount+1] = coordinates[ncount] + dx;
      }
    //assert(std::abs(coordinates.back()/cable_params.span-1.)<1.e-4);
    
    // Create the catenary cable
    catenary_cable = new HeavyTensionedBeam(cable_params, coordinates);
    catenary_cable->AddSuspensionSpring(left_catenary_spring_params, 0);
    catenary_cable->AddSuspensionSpring(right_catenary_spring_params, nNodes-1);
    
    // Create the contact wire
    contact_wire = new HeavyTensionedBeam(wire_params, coordinates);
    contact_wire->AddSuspensionSpring(left_contact_spring_params, 0);
    contact_wire->AddSuspensionSpring(right_contact_spring_params, nNodes-1);
    
    // Total number of dofs = catenary + contact
    const int nCatenaryCableDofs = catenary_cable->GetLocalToGlobalMap().GetTotalNumDof();
    const int nContactWireDofs   = contact_wire->GetLocalToGlobalMap().GetTotalNumDof();
    nTotalDof = nCatenaryCableDofs + nContactWireDofs;

    // local to global maps for the contact and catenary cables
    catenary_L2GMap = new OffsetLocalToGlobalMap(catenary_cable->GetLocalToGlobalMap(), 0);
    contact_L2GMap  = new OffsetLocalToGlobalMap(contact_wire->GetLocalToGlobalMap(),   nCatenaryCableDofs);
        
    // Local index set for catenary cable dofs
    PetscErrorCode ierr;
    std::vector<int> catenary_cable_dofs(nCatenaryCableDofs);
    for(int i=0; i<nCatenaryCableDofs; ++i)
      catenary_cable_dofs[i] = i;
    ierr = ISCreateGeneral(PETSC_COMM_WORLD, nCatenaryCableDofs, catenary_cable_dofs.data(), PETSC_COPY_VALUES, &IS_catenary_cable); CHKERRV(ierr);

    // Local index set for contact wire dof
    std::vector<int> contact_wire_dofs(nContactWireDofs);
    for(int i=0; i<nContactWireDofs; ++i)
      contact_wire_dofs[i] = i+nCatenaryCableDofs;
    ierr = ISCreateGeneral(PETSC_COMM_WORLD, nContactWireDofs, contact_wire_dofs.data(), PETSC_COPY_VALUES, &IS_contact_wire); CHKERRV(ierr);
    
    // done
  }


  
  // Constructor
  SingleSpan_OHE_Cables::SingleSpan_OHE_Cables(const double x0,
					       const OHESpan_ParameterPack& pack,
					       const int nelms_per_interval)
    :SingleSpan_OHE_Cables(x0,
			   pack.catenaryParams,
			   pack.wireParams,
			   pack.dropperParams,
			   pack.dropperSchedule,
			   pack.catenary_spring_params,  // left = right
			   pack.catenary_spring_params,  // left = right
			   pack.contact_spring_params,   // left = right
			   pack.contact_spring_params,   // left = right
			   nelms_per_interval)
  {}  


  // Destructor
  SingleSpan_OHE_Cables::~SingleSpan_OHE_Cables()
  {
    // Check that PETSc has not been finalized
    PetscBool isFinalized;
    auto ierr = PetscFinalized(&isFinalized); CHKERRV(ierr);
    assert(isFinalized==PETSC_FALSE);
    
    // Clean up
    delete catenary_cable;
    delete contact_wire;
    delete catenary_L2GMap;
    delete contact_L2GMap;
    ierr = ISDestroy(&IS_catenary_cable); CHKERRV(ierr);
    ierr = ISDestroy(&IS_contact_wire);   CHKERRV(ierr);

    // done
  }


  // Get the catenary cable dofs at the start and end of the span
  std::pair<int,int> SingleSpan_OHE_Cables::GetCatenaryCableStartDofs() const
  {
    return catenary_cable->GetStartDofs();
  }
  std::pair<int,int> SingleSpan_OHE_Cables::GetCatenaryCableEndDofs() const
  {
    return catenary_cable->GetEndDofs();
  }

  // Get the contact wire dofs at the start and end of the span
  std::pair<int,int> SingleSpan_OHE_Cables::GetContactWireStartDofs() const
  {
    const int nCatenaryCableDofs = catenary_cable->GetLocalToGlobalMap().GetTotalNumDof();
    auto dofs    = contact_wire->GetStartDofs();
    dofs.first  += nCatenaryCableDofs;
    dofs.second += nCatenaryCableDofs;
    return dofs;
  }
  std::pair<int,int> SingleSpan_OHE_Cables::GetContactWireEndDofs() const
  {
    const int nCatenaryCableDofs = catenary_cable->GetLocalToGlobalMap().GetTotalNumDof();
    auto dofs    = contact_wire->GetEndDofs();
    dofs.first  += nCatenaryCableDofs;
    dofs.second += nCatenaryCableDofs;
    return dofs;
  }
  
  // Number of nonzero per row
  std::vector<int> SingleSpan_OHE_Cables::GetSparsity() const
  {
    // Concatenate sparsity of catenary + contact wire
    auto nz = catenary_cable->GetSparsity();
    auto contact_wire_nz = contact_wire->GetSparsity();
    nz.insert(nz.end(),                                        // nz <- nz + contact_wire_nz.
	      std::make_move_iterator(contact_wire_nz.begin()),
	      std::make_move_iterator(contact_wire_nz.end()));

    return std::move(nz);
  }
  

  // Main functionality: evaluate contribution to mass, stiffness, and force vectors
  void SingleSpan_OHE_Cables::Evaluate(Mat& M, Mat& K, Vec& F) const
  {
    PetscErrorCode ierr;

    // Assemble contributions from the catenary cable
    Mat M_catenary_cable;
    Mat K_catenary_cable;
    Vec R_catenary_cable;
    ierr = MatGetLocalSubMatrix(M, IS_catenary_cable, IS_catenary_cable, &M_catenary_cable);     CHKERRV(ierr);
    ierr = MatGetLocalSubMatrix(K, IS_catenary_cable, IS_catenary_cable, &K_catenary_cable);     CHKERRV(ierr);
    ierr = VecGetSubVector(F, IS_catenary_cable, &R_catenary_cable);                             CHKERRV(ierr);
    catenary_cable->Evaluate(M_catenary_cable, K_catenary_cable, R_catenary_cable);
    ierr = VecRestoreSubVector(F, IS_catenary_cable, &R_catenary_cable);                         CHKERRV(ierr);
    ierr = MatRestoreLocalSubMatrix(M, IS_catenary_cable, IS_catenary_cable, &M_catenary_cable); CHKERRV(ierr);
    ierr = MatRestoreLocalSubMatrix(K, IS_catenary_cable, IS_catenary_cable, &K_catenary_cable); CHKERRV(ierr);


    // Assemble contributions form the contact wire
    Mat M_contact_wire;
    Mat K_contact_wire;
    Vec R_contact_wire;
    ierr = MatGetLocalSubMatrix(M, IS_contact_wire, IS_contact_wire, &M_contact_wire);           CHKERRV(ierr);
    ierr = MatGetLocalSubMatrix(K, IS_contact_wire, IS_contact_wire, &K_contact_wire);           CHKERRV(ierr);
    ierr = VecGetSubVector(F, IS_contact_wire, &R_contact_wire);                                 CHKERRV(ierr);
    contact_wire->Evaluate(M_contact_wire, K_contact_wire, R_contact_wire);
    ierr = VecRestoreSubVector(F, IS_contact_wire, &R_contact_wire);                             CHKERRV(ierr);
    ierr = MatRestoreLocalSubMatrix(M, IS_contact_wire, IS_contact_wire, &M_contact_wire);       CHKERRV(ierr);
    ierr = MatRestoreLocalSubMatrix(K, IS_contact_wire, IS_contact_wire, &K_contact_wire);       CHKERRV(ierr);

    // Complete assembly
    ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);   
    ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = VecAssemblyBegin(F);                     CHKERRV(ierr);
    ierr = VecAssemblyEnd(F);                       CHKERRV(ierr);

    // Permit new nonzeros
    ierr = MatSetOption(M, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
    ierr = MatSetOption(K, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
    
    // done
    return;
  }


  // Access the list of dropper nodes
  std::vector<int> SingleSpan_OHE_Cables::GetDropperNodes() const
  {
    const int& ndroppers = dropperSchedule.nDroppers;
    std::vector<int> nodeList(ndroppers);
    for(int d=0; d<ndroppers; ++d)
      nodeList[d] = nElements_per_Interval*(d+1);
    return nodeList;      
  }

  //! Access the dofs of a given dropper
  std::pair<int,int> SingleSpan_OHE_Cables::GetDropperDofPair(const int d) const
  {
    const int nCatenaryCableDofs = catenary_L2GMap->GetTotalNumDof();
    
    const int node = nElements_per_Interval*(d+1);
    const int topdof = node;                       // on the catenary
    const int botdof = nCatenaryCableDofs + node;  // on the contact wire
    return {topdof, botdof};
  }

  // Determines the dropper displacement at the catenary and contact wires
  void SingleSpan_OHE_Cables::GetDropperDisplacements(const double* dofValues,
					       double* catenary_disp, double* contact_disp) const
  {
    std::vector<int> dropper_nodes = GetDropperNodes();
    const int nDroppers = static_cast<int>(dropper_nodes.size());
    const int nCatenaryCableDofs = catenary_L2GMap->GetTotalNumDof();
    for(int d=0; d<nDroppers; ++d)
      {
	catenary_disp[d] = dofValues[dropper_nodes[d]];
	contact_disp[d]  = dofValues[dropper_nodes[d]+nCatenaryCableDofs];
      }
    return;
  }
    
  
  // Get the active displacement dofs and shape function values at a given point along the span
  std::array<std::pair<int,double>,2> SingleSpan_OHE_Cables::GetActiveDisplacementDofs(const double x) const
  {
    auto dpairs = contact_wire->GetActiveDisplacementDofs(x);
    
    // revise the dof#
    const int nCatenaryCableDofs = catenary_L2GMap->GetTotalNumDof();
    for(auto& it:dpairs)
      it.first += nCatenaryCableDofs;

    return dpairs;
  }


  // Get the active dofs and shape function values at a given point along the span
  std::array<std::pair<int,double>,4> SingleSpan_OHE_Cables::GetActiveDofs(const double x) const
  {
    auto dpairs = contact_wire->GetActiveDofs(x);

    // revise the dof#
    const int nCatenaryCableDofs = catenary_L2GMap->GetTotalNumDof();
    for(auto& it:dpairs)
      it.first += nCatenaryCableDofs;

    return dpairs;
  }
  
  // Get the list of contact wire dofs
  std::set<int> SingleSpan_OHE_Cables::GetContactWireDofs() const
  {
    const int num_catenary_dofs = catenary_L2GMap->GetTotalNumDof();
    const int num_contact_dofs = contact_L2GMap->GetTotalNumDof();
    assert(nTotalDof%2==0 && num_contact_dofs==nTotalDof/2);
    std::set<int> contact_dofs{};
    for(int i=0; i<num_contact_dofs; ++i)
      contact_dofs.insert(num_catenary_dofs+i);
    
    return contact_dofs;
  }

  
  // Visualize the configuration
  void SingleSpan_OHE_Cables::Write(const double* displacements,
			     const std::string catenary_fname,
			     const std::string contact_fname,
			     const std::string dropper_fname) const
  {
    // catenary cable
    catenary_cable->Write(displacements, catenary_fname);

    // contact wire
    const int nCatenaryCableDofs = catenary_L2GMap->GetTotalNumDof();
    contact_wire->Write(displacements+nCatenaryCableDofs, contact_fname);

    // droppers
    const auto& coordinates = catenary_cable->GetCoordinates();
    const auto dropperNodes = GetDropperNodes();
    const int nDroppers = static_cast<int>(dropperNodes.size());
    const auto& catenary_params = catenary_cable->GetParameters();
    const auto& contact_params  = contact_wire->GetParameters();
    std::fstream pfile;
    pfile.open(dropper_fname.c_str(), std::ios::app|std::ios::out);
    assert(pfile.good());
    for(int n=0; n<nDroppers; ++n)
      {
	const int& node = dropperNodes[n];
	pfile << coordinates[node] <<" "<< catenary_params.zLevel + displacements[node]
	      << std::endl
	      << coordinates[node] <<" "<< contact_params.zLevel + displacements[nCatenaryCableDofs+node]
	      << std::endl
	      << std::endl;
      }
    pfile.close();

    return;
  }


  // proxy for displacement, velocity and acceleration
  void SingleSpan_OHE_Cables::Write(const double* disp, const double* vel, const double* accn,
			     const std::string catenary_fname,
			     const std::string contact_fname,
			     const std::string dropper_fname) const
  {
    // catenary cable
    catenary_cable->Write(disp, vel, accn, catenary_fname);

    // contact wire
    const int nCatenaryCableDofs = catenary_L2GMap->GetTotalNumDof();
    contact_wire->Write(disp+nCatenaryCableDofs, vel+nCatenaryCableDofs, accn+nCatenaryCableDofs, contact_fname);

    // droppers: write only field 1
    const auto& coordinates = catenary_cable->GetCoordinates();
    const auto dropperNodes = GetDropperNodes();
    const int nDroppers = static_cast<int>(dropperNodes.size());
    const auto& catenary_params = catenary_cable->GetParameters();
    const auto& contact_params  = contact_wire->GetParameters();
    std::fstream pfile;
    pfile.open(dropper_fname.c_str(), std::ios::app|std::ios::out);
    assert(pfile.good());
    for(int n=0; n<nDroppers; ++n)
      {
	const int& node = dropperNodes[n];
	pfile << coordinates[node] <<" "<< catenary_params.zLevel + disp[node]
	      << std::endl
	      << coordinates[node] <<" "<< contact_params.zLevel + disp[nCatenaryCableDofs+node]
	      << std::endl
	      << std::endl;
      }
    pfile.close();

    
  }
    
    

}
