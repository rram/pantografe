
/** \file test_json.cpp
 * \brief Unit test for read/write of parameters using the json format
 * \author Ramsharan Rangarajan
 * Last modified: July 29, 2022
 */


#include <pfe_HeavyTensionedBeamParams.h>
#include <pfe_IntercableDropperParams.h>
#include <pfe_SuspensionSpringParams.h>
#include <random>
#include <cassert>

void Generate(pfe::HeavyTensionedBeamParams& params);

void Generate(pfe::IntercableDropperParams& params);

void Generate(pfe::DropperArrangement& params);

void Generate(pfe::SuspensionSpringParams& params);

void Compare(const pfe::HeavyTensionedBeamParams& A,
	     const pfe::HeavyTensionedBeamParams& B,
	     const double tol);

void Compare(const pfe::IntercableDropperParams& A,
	     const pfe::IntercableDropperParams& B,
	     const double tol);

void Compare(const pfe::DropperArrangement& A,
	     const pfe::DropperArrangement& B,
	     const double tol);

void Compare(const pfe::SuspensionSpringParams& A,
	     const pfe::SuspensionSpringParams& B,
	     const double tol);

int main()
{
  std::fstream jfile;
  jfile.open((char*)"datasheet.json", std::ios::out);

  // Write 
  // Catenary cable
  pfe::HeavyTensionedBeamParams catenary_params;
  catenary_params.tag = "catenary cable";
  Generate(catenary_params);
  jfile << catenary_params;

  // contact cable
  pfe::HeavyTensionedBeamParams contact_params;
  contact_params.tag = "contact wire";
  Generate(contact_params);
  jfile << contact_params;

  // droppers
  pfe::IntercableDropperParams dropper_params;
  dropper_params.tag = "droppers";
  Generate(dropper_params);
  jfile << dropper_params;

  // dropper arrangement
  pfe::DropperArrangement dropper_schedule;
  dropper_schedule.tag = "dropper schedule";
  Generate(dropper_schedule);
  jfile << dropper_schedule;

  // suspension springs
  pfe::SuspensionSpringParams springParams;
  springParams.tag = "suspension spring";
  Generate(springParams);
  jfile << springParams;

  jfile.close();

  // Read & compare
  jfile.open((char*)"datasheet.json", std::ios::in);

  pfe::SuspensionSpringParams spring_copy;
  spring_copy.tag = "suspension spring";
  jfile >> spring_copy;
  Compare(springParams, spring_copy, 1.e-6);
  
  pfe::DropperArrangement schedule_copy;
  schedule_copy.tag = "dropper schedule";
  jfile >> schedule_copy;
  Compare(dropper_schedule, schedule_copy, 1.e-6);
    
  pfe::HeavyTensionedBeamParams copy_params;
  copy_params.tag = "contact wire";
  jfile >> copy_params;
  Compare(contact_params, copy_params, 1.e-6);

  pfe::IntercableDropperParams dropper_copy;
  dropper_copy.tag = "droppers";
  jfile >> dropper_copy;
  Compare(dropper_params, dropper_copy, 1.e-6);

  copy_params.tag = "catenary cable";
  jfile >> copy_params;
  Compare(catenary_params, copy_params, 1.e-6);
 
  jfile.close();
}


void Generate(pfe::HeavyTensionedBeamParams& params)
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(1.0, 100.0);
  params.span = dis(gen);
  params.EI = dis(gen);
  params.Tension = dis(gen);
  params.rhoA = dis(gen);
  params.g = dis(gen);
  params.zLevel = dis(gen);
  return;
}


void Generate(pfe::IntercableDropperParams& params)
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(1.0, 100.0);
  params.EA = dis(gen);
  params.rhoA = dis(gen);
  params.Mcm = dis(gen);
  params.Mcc = dis(gen);
  params.g = dis(gen);
  return;
}


void Generate(pfe::DropperArrangement& params)
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(1.0, 100.0);
  params.nDroppers = 1+static_cast<int>(dis(gen));
  params.zEncumbrance = dis(gen);
  params.nominal_lengths.resize(params.nDroppers);
  params.coordinates.resize(params.nDroppers);
  params.target_sag.resize(params.nDroppers);
  for(int d=0; d<params.nDroppers; ++d)
    {
      params.nominal_lengths[d] = dis(gen);
      params.target_sag[d] = dis(gen);
      params.coordinates[d] = dis(gen);
      if(d>0)
	params.coordinates[d] += params.coordinates[d-1];
    }
  return;
}


void Generate(pfe::SuspensionSpringParams& params)
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(1.0, 100.0);
  params.stiffness = dis(gen);
  return;
}



void Compare(const pfe::HeavyTensionedBeamParams& A,
	     const pfe::HeavyTensionedBeamParams& B,
	     const double tol)
{
  assert(A.tag==B.tag);
  assert(std::abs(A.span-B.span)<tol);
  assert(std::abs(A.zLevel-B.zLevel)<tol);
  assert(std::abs(A.EI-B.EI)<tol);
  assert(std::abs(A.Tension-B.Tension)<tol);
  assert(std::abs(A.zLevel-B.zLevel)<tol);
  assert(std::abs(A.rhoA-B.rhoA)<tol);
  assert(std::abs(A.g-B.g)<tol);
  return;
}


void Compare(const pfe::IntercableDropperParams& A,
	     const pfe::IntercableDropperParams& B,
	     const double tol)
{
  assert(A.tag==B.tag);
  assert(std::abs(A.EA-B.EA)<tol);
  assert(std::abs(A.rhoA-B.rhoA)<tol);
  assert(std::abs(A.Mcm-B.Mcm)<tol);
  assert(std::abs(A.Mcc-B.Mcc)<tol);
  assert(std::abs(A.g-B.g)<tol);
  return;
}


void Compare(const pfe::DropperArrangement& A,
	     const pfe::DropperArrangement& B,
	     const double tol)
{
  A.Validate();
  B.Validate();
  assert(A.tag==B.tag);
  assert(A.nDroppers==B.nDroppers);
  assert(std::abs(A.zEncumbrance-B.zEncumbrance)<tol);
  for(int d=0; d<A.nDroppers; ++d)
    {
      assert(std::abs(A.target_sag[d]-B.target_sag[d])<tol);
      assert(std::abs(A.nominal_lengths[d]-B.nominal_lengths[d])<tol);
      assert(std::abs(A.coordinates[d]-B.coordinates[d])<tol);
    }
  return;
}

void Compare(const pfe::SuspensionSpringParams& A,
	     const pfe::SuspensionSpringParams& B,
	     const double tol)
{
  assert(A.tag.empty()==false && B.tag.empty()==false);
  assert(std::abs(A.stiffness-B.stiffness)<tol);
  return;
}
