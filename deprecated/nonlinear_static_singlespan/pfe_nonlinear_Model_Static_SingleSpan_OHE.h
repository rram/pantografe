
/** \file pfe_nonlinear_Model_Static_SingleSpan_OHE.h
 * \brief Defines the class pfe::nonlinear::Model_Static_SingleSpan_OHE to simulate static behavior of a single span of the OHE
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_nonlinear_Model_Static_OHE.h>

namespace pfe
{
  namespace nonlinear
  {
    //! instantiation
    using Model_Static_SingleSpan_OHE = Model_Static_OHE<SingleSpan_OHE>;
  }
}

// Implementation of function specializations
#include <pfe_nonlinear_Model_Static_SingleSpan_OHE_impl.h>


    //!< Class to simulate static behavior of an single span OHE
    //! \ingroup models
    class Model_Static_SingleSpan_OHE
    {
    public:
      //! Constructor
      Model_Static_SingleSpan_OHE(SingleSpan_OHE& ohe);

      //! Destructor
      virtual ~Model_Static_SingleSpan_OHE();

      //! Disable copy and assignment
      Model_Static_SingleSpan_OHE(const Model_Static_SingleSpan_OHE&) = delete;
      Model_Static_SingleSpan_OHE operator=(const Model_Static_SingleSpan_OHE&) = delete;

      //! Access the span
      inline const SingleSpan_OHE& GetSpan() const
      { return ohe_span; }
    
      //! number of dofs
      inline int GetTotalNumDof() const
      { return ohe_span.GetTotalNumDof(); }
    
      //! Main functionality: static solve
      void Solve(double* displacements);

      //! Main functionality: adjust dropper lengths to match sag
      void AdjustDropperLengths(const int nIters);

      //! Compute transverse stiffness disrtribution
      void ComputeTransverseStiffnessDistribution(const double* displacements, const double Force,
						  double* stiffness) const;

      //! Visualize the configuration
      inline void Write(const double* displacements,
			const std::string catenary_fname,
			const std::string contact_fname,
			const std::string dropper_fname) const
      { ohe_span.Write(displacements, catenary_fname, contact_fname, dropper_fname); }

    private:

      struct SNES_Context
      {
	SingleSpan_OHE *ohe_span;
	PetscData      *data;
	double          force;
	int             dofnum;
      };
      
      static PetscErrorCode Residual_Func(SNES snes, Vec solVec, Vec resVec, void* usr);
      static PetscErrorCode Jacobian_Func(SNES snes, Vec solVec, Mat kMat, Mat pMat, void* usr);
      
      SingleSpan_OHE& ohe_span;        //!< single span
      mutable SNESSolver snes_solver;  //!< Helper for nonlinear solves
      mutable PetscData  petsc_data;   //!< Helper data for snes assembly
    };
  }  
}
