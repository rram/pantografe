
/** \file pfe_nonlinear_Model_Static_SingleSpan_OHE.cpp
 * \brief Implementation of the class pfe::nonlinear::Model_Static_SingleSpan_OHE
 * \author Ramsharan Rangarajan
 */

#include <pfe_nonlinear_Model_Static_SingleSpan_OHE.h>
#include <numeric>

namespace pfe
{
  namespace nonlinear
  {
    // residual evaluation for SNES
    PetscErrorCode Model_Static_SingleSpan_OHE::Residual_Func(SNES snes, Vec solVec, Vec resVec, void* usr)
    {
      PetscErrorCode ierr;

      // access the ohe span
      SNES_Context *ctx; 
      ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr); 
      assert(ctx!=nullptr);
      assert(ctx->ohe_span!=nullptr);
      assert(ctx->data!=nullptr);
      auto* ohe_span = ctx->ohe_span;
      auto* data = ctx->data;

      // evaluate 
      ierr = VecZeroEntries(resVec);             CHKERRQ(ierr);
      ierr = MatZeroEntries(data->massMAT);      CHKERRQ(ierr);
      ierr = MatZeroEntries(data->stiffnessMAT); CHKERRQ(ierr);
      ohe_span->Evaluate(solVec, data->massMAT, data->stiffnessMAT, resVec);

      // add vertical force
      if(ctx->dofnum>=0)
	{
	  double val = -ctx->force;
	  ierr = VecSetValuesLocal(resVec, 1, &ctx->dofnum, &val, ADD_VALUES); CHKERRQ(ierr);
	  ierr = VecAssemblyBegin(resVec); CHKERRQ(ierr);
	  ierr = VecAssemblyEnd(resVec);   CHKERRQ(ierr);
	}
      
      // done
      return 0;
    }
    

    // stiffness evaluation for snes
    PetscErrorCode Model_Static_SingleSpan_OHE::Jacobian_Func(SNES snes, Vec solVec, Mat kMat, Mat pMat, void* usr)
    {
      PetscErrorCode ierr;
      
      // access the ohe span
      SNES_Context *ctx;
      ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
      assert(ctx!=nullptr);
      auto* ohe_span = ctx->ohe_span;
      assert(ohe_span!=nullptr);
      auto* data = ctx->data;
      assert(data!=nullptr);
      
      // evaluate (M, R -> dummy values)
      ierr = MatZeroEntries(data->massMAT); CHKERRQ(ierr);
      ierr = MatZeroEntries(kMat);          CHKERRQ(ierr);
      ierr = VecZeroEntries(data->resVEC);  CHKERRQ(ierr);
      ohe_span->Evaluate(solVec, data->massMAT, kMat, data->resVEC);

      // done
      return 0;
    }

    
    // Constructor
    Model_Static_SingleSpan_OHE::Model_Static_SingleSpan_OHE(SingleSpan_OHE& ohe)
      :ohe_span(ohe)
    {
      PetscErrorCode ierr;
      PetscBool isInitialized;
      ierr = PetscInitialized(&isInitialized); CHKERRV(ierr);
      assert(isInitialized==PETSC_TRUE);
    
      // sparsity
      auto nz = ohe_span.GetSparsity();
      const int ndof = static_cast<int>(nz.size());

      // nonlinear solver
      snes_solver.Initialize(nz, Residual_Func, Jacobian_Func);

      // allocate data for assembly
      petsc_data.Initialize(nz);
      
      // done
    }

    // Destructor
    Model_Static_SingleSpan_OHE::~Model_Static_SingleSpan_OHE()
    {
      // Check that PETSc has not been finalized
      PetscBool isFinalized;
      auto ierr = PetscFinalized(&isFinalized); CHKERRV(ierr);
      assert(isFinalized==PETSC_FALSE);
      snes_solver.Destroy();
      petsc_data.Destroy();
      
      // done
    }

    
    // Main functionality: solve
    void Model_Static_SingleSpan_OHE::Solve(double* displacements)
    {
      SNES_Context ctx;
      ctx.ohe_span = &ohe_span;
      ctx.data     = &petsc_data;
      ctx.force    = 0.;
      ctx.dofnum   = -1;
	
      // solve
      snes_solver.Solve(displacements, &ctx, true);

      // done
      return;
    }


    // Main functionalty: match a prescribed value of sag
    void Model_Static_SingleSpan_OHE::AdjustDropperLengths(const int nIters)
    {
      const auto& droppers = ohe_span.GetDroppers();
      const auto& schedule = ohe_span.GetDropperArrangement();
      const int nDroppers = static_cast<int>(droppers.size());
      const auto& target_sag = schedule.target_sag;


      const int nTotalDof = ohe_span.GetTotalNumDof();
      std::vector<double> displacements(nTotalDof);
      std::vector<double> dropper_contact_disp(nDroppers), dropper_catenary_disp(nDroppers);
      std::vector<double> lengths(nDroppers);
    
      // Iterate over dropper lengths
      for(int iter=0; iter<=nIters; ++iter)
	{    
	  // solve in the current state
	  Solve(displacements.data());

	  // Get sag values at the contact wire along dropper connections
	  ohe_span.GetDropperDisplacements(&displacements[0],
					   &dropper_catenary_disp[0],
					   &dropper_contact_disp[0]);

	  // Adjust dropper lengths towards achieving the target
	  for(int d=0; d<nDroppers; ++d)
	    {
	      lengths[d] = droppers[d]->GetLength();
	      lengths[d] -= (target_sag[d]-dropper_contact_disp[d]);
	      assert(lengths[d]>0.);
	    }

	  // Set the new dropper lengths
	  ohe_span.ResetDropperLengths(lengths.data());
	}

      // done
      return;
    }


    // Compute transverse stiffness disrtribution
    void Model_Static_SingleSpan_OHE::
    ComputeTransverseStiffnessDistribution(const double* displacements,
					   const double Force, 
					   double* stiffness) const
    {
      PetscErrorCode ierr;
    
      // Apply the given force at each node of the contact wire along the span
      // Measure the displacement and compute the stiffness
      const auto& contact_wire = ohe_span.ContactWire();
      const int nNodes = contact_wire.GetNumNodes();
      const int nTotalDof = ohe_span.GetTotalNumDof();

      SNES_Context ctx;
      ctx.ohe_span = &ohe_span;
      ctx.force    = Force;
      ctx.data     = &petsc_data;

      std::vector<double> forced_disp(nTotalDof);
      std::copy(displacements, displacements+nTotalDof, forced_disp.begin());

      // linear backtracking (default is 3, which occasionally fails)
      SNESLineSearch ls;
      ierr = SNESGetLineSearch(snes_solver.snes, &ls); CHKERRV(ierr);
      ierr = SNESLineSearchSetOrder(ls, 1);            CHKERRV(ierr);
      ierr = SNESSetFromOptions(snes_solver.snes);     CHKERRV(ierr);
      
      // load stepping for the first node
      for(int lstep=0; lstep<5; ++lstep)
	{
	  ctx.force = Force*static_cast<double>(lstep)/5.;
	  ctx.dofnum = ohe_span.GetActiveContactWireDof(0);
	  snes_solver.Solve(forced_disp.data(), &ctx, true);
	}

      // Apply specified force
      ctx.force = Force;
      
      // Loop over the loading node
      for(int lnode=0; lnode<nNodes; ++lnode)
	{
	  // Add transverse force at "lnode" on the contact wire
	  ctx.dofnum = ohe_span.GetActiveContactWireDof(lnode);
	  	  
	  // solve
	  snes_solver.Solve(forced_disp.data(), &ctx, true);

	  Write(forced_disp.data(), "cat-"+std::to_string(lnode)+".dat", "con-"+std::to_string(lnode)+".dat", "drop-"+std::to_string(lnode)+".dat");

	  // relative displacement
	  const double delta_disp = forced_disp[ctx.dofnum]-displacements[ctx.dofnum];
	  
	  // Compute the stiffness
	  assert(std::abs(delta_disp)>1.e-9);
	  stiffness[lnode] = Force/delta_disp;
	}
    
      // done
      return;
    }

  } // namespace nonlinear
} // namespace pfe
