
/** \file test_DropperSupportedCatenary.cpp
 * \brief Unit test for the class pfe::DropperSupportedCatenaryCable
 * \author Ramsharan Rangarajan
 * Last modified: October 19, 2021
 */

#include <pfe_DropperSupportedCatenaryCable.h>
#include <fstream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Catenary parameters
  pfe::CatenaryCableParams cable_params{.span=55., .Tension=22000., .rhoAg=13.25, .zLevel=0.};
  const int nElements = 100; // keep even to position the dropper in the middle

  // Dropper parameters
  pfe::OverhangDropperParams dropper_params{.EA=1.e6, .length=1., .coord=0., .zEncumbrance=1.};
  std::vector<pfe::OverhangDropperParams> droppers{dropper_params, dropper_params, dropper_params};
  droppers[0].coord = 13.75;
  droppers[1].coord = 27.5;
  droppers[2].coord = 41.25;

  // Instance of model
  auto cable = new pfe::DropperSupportedCatenaryCable(cable_params, nElements, droppers);

  // Solve
  std::vector<double> displacements(cable->GetLocalToGlobalMap().GetTotalNumDof());
  cable->Solve(displacements.data());

    // Print solution to file
  auto coordinates = cable->GetCoordinates();
  std::fstream pfile;
  pfile.open((char*)"cable-sol.dat", std::ios::out);
  for(int n=0; n<nElements+1; ++n)
    pfile<<coordinates[n]<<" "<<cable_params.zLevel+displacements[n]<<"\n";
  pfile.close();

  // Clean up
  delete cable;

  // Finalize PETSc
  PetscFinalize();
}
