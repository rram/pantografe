
/** \file pfe_DropperSupportedContactWire.h
 * \brief Defines the class pfe::DropperSupportedContactWire
 * \author Ramsharan Rangarajan
 * Last modified: October 19, 2021
 */

#ifndef PFE_DROPPER_SUPPORTED_CONTACT_WIRE_H
#define PFE_DROPPER_SUPPORTED_CONTACT_WIRE_H

#include <pfe_OverhangDropper.h>
#include <pfe_HeavyTensionedBeam.h>

namespace pfe
{
  //! Class for a contact wire supported by a set of droppers
  class DropperSupportedContactWire: public HeavyTensionedBeam
  {
  public:
    DropperSupportedContactWire(const HeavyTensionedBeamParams& beam_params, const int nelm,
				const std::vector<OverhangDropperParams>& dparams);

    //! Destructor
    virtual ~DropperSupportedContactWire();

    //! Disable copy and assignment
    DropperSupportedContactWire(const DropperSupportedContactWire&) = delete;
    DropperSupportedContactWire& operator=(const DropperSupportedContactWire&) = delete;

    //! Assemble the stiffness and force vector
    void Evaluate(const double* dofValues, Mat& K, Vec& F) const override;

    protected:
    const int nDroppers; 
    std::vector<OverhangDropper*> droppers;
  };
  
}

#endif
