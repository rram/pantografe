
/** \file test_StaticPointLoad.cpp
 * \brief Unit test for the component pfe::MovingPointLoad
 * \author Ramsharan Rangarajan
 * Last modified: July 19, 2022
 */

#include <pfe_MovingPointLoad.h>
#include <fstream>
#include <iostream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL); CHKERRQ(ierr);

  // Create contact wire model
  pfe::ContactWireParams params{.span=55., .EI=195., .Tension=22000., .rhoA=1.35, .g=9.81, .zLevel=1.};
  const int nElements = 50;
  const int nNodes = nElements+1;
  auto cwire = new pfe::HeavyTensionedBeam(params, nElements); 

  // Simply supported bcs at left/right ends
  std::vector<int> dirichlet_dofs{0, nNodes-1};
  std::vector<double> dirichlet_values{0.,0.};
  
  // displacement field
  const int nTotalDofs = cwire->GetLocalToGlobalMap().GetTotalNumDof();
  std::vector<double> displacements(nTotalDofs);
  
  // Petsc data
  std::vector<int> nz =  cwire->GetSparsity();
  pfe::PetscData PD;
  PD.Initialize(nz);

  // linear solver
  pfe::LinearSolver linSolver;
      
  // Moving point load
  const double F0 = 100.;  // N
  const double x0 = 1.0;   // initial position
  const double c0 = 1.0;   // speed of the load
  pfe::MovingPointLoad delta_load(*cwire, F0, x0, c0);

  for(int tstep=0; tstep<50; ++tstep)
    {
      std::cout<<"Time step: "<<tstep << std::endl;
      const double time = static_cast<double>(tstep);
      
      // assemble contributions from the beam
      cwire->Evaluate(displacements.data(), PD.massMAT, PD.stiffnessMAT, PD.resVEC);

      // add point load
      delta_load.Evaluate(time, PD.resVEC);
      
      // Set Dirichlet bcs
      PD.SetDirichletBCs(dirichlet_dofs, dirichlet_values);

      // solve
      linSolver.SetOperator(PD.stiffnessMAT);
      linSolver.Solve(PD.resVEC, PD.solutionVEC);
      
      // plot the solution
      double* disp;
      ierr = VecGetArray(PD.solutionVEC, &disp);     CHKERRQ(ierr);
      auto coordinates = cwire->GetCoordinates();
      std::fstream pfile;
      pfile.open(std::string("sol-"+std::to_string(tstep)+".dat").c_str(), std::ios::out);
      for(int n=0; n<nElements+1; ++n)
	pfile<<coordinates[n]<<" "<<disp[n]<<" "<<disp[nElements+1+n]<<"\n";
      pfile.close();
      pfile.open(std::string("load-"+std::to_string(tstep)+".dat").c_str(), std::ios::out);
      double xload, fload;
      delta_load.GetForce(time, xload, fload);
      pfile << xload << " " << 0. << std::endl << xload <<" " << -0.25;
      pfile.close();
      ierr = VecRestoreArray(PD.solutionVEC, &disp); CHKERRQ(ierr);
    }
  
  // Clean up
  linSolver.Destroy();
  PD.Destroy();
  delete cwire;
  PetscFinalize();
}
