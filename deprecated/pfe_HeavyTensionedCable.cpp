
/** \file pfe_HeavyTensionedCable.cpp
 * \brief Implements the class pfe::HeavyTensionedCable
 * \author Ramsharan Rangarajan
 * Last modified: October 19, 2021
 */


#include <pfe_HeavyTensionedCable.h>
#include <cassert>

namespace pfe
{
  // Constructor
  HeavyTensionedCable::HeavyTensionedCable(const HeavyTensionedCableParams& cable_params,
					   const int nelm)
    :params(cable_params),
     nElements(nelm),
     nNodes(nElements+1),
     coordinates(nNodes),
     connectivity(2*nElements),
     ElmArray(nElements),
     TOpsArray(nElements),
     gOpsArray(nElements)
  {
    assert(nElements>0);

    // Check that PETSc has been initialized
    PetscBool is_initialized;
    PetscInitialized(&is_initialized);
    assert(is_initialized==PETSC_TRUE);

    // Coordinates
    const double dx = params.span/static_cast<double>(nElements);
    for(int a=0; a<nNodes; ++a)
      coordinates[a] = static_cast<double>(a)*dx;
    SegmentGeometry<1>::SetGlobalCoordinatesArray(coordinates);

    // Create elements
    for(int e=0; e<nElements; ++e)
      ElmArray[e] = new P11DElement<1>(e+1, e+2);

    // Local to global map
    L2GMap = new P11DL2GMap(ElmArray);

    // Operations
    for(int e=0; e<nElements; ++e)
      {
	TOpsArray[e] = new TensionForce(e, ElmArray[e], params.Tension);
	gOpsArray[e] = new GravityForce(e, ElmArray[e], -params.rhoAg);
      }

    // Assemblers
    Asm_T = new Assembler<TensionForce>(TOpsArray, *L2GMap);
    Asm_g = new Assembler<GravityForce>(gOpsArray, *L2GMap);

    // Petsc data
    const int nTotalDof = L2GMap->GetTotalNumDof();
    std::vector<int> nz{};
    Asm_T->CountNonzeros(nz);
    PD.Initialize(nz);

    // Direct solver
    PC pc;
    auto ierr = KSPGetPC(PD.kspSOLVER, &pc); CHKERRV(ierr);
    ierr = PCSetType(pc, PCLU);              CHKERRV(ierr);
    ierr = KSPSetPC(PD.kspSOLVER, pc);       CHKERRV(ierr);
  }

  // Destructor
  HeavyTensionedCable::~HeavyTensionedCable()
  {
    // PETSc should not be finalized
    PetscBool is_finalized;
    PetscFinalized(&is_finalized);
    assert(is_finalized==PETSC_FALSE);

    PD.Destroy();
    delete L2GMap;
    for(auto& e:ElmArray) delete e;
    for(auto& op:TOpsArray) delete op;
    for(auto& op:gOpsArray) delete op;
    delete Asm_T;
    delete Asm_g;
  }

  // Assemble the stiffness and force vector
  void HeavyTensionedCable::Evaluate(const double* dofValues,  Mat& K, Vec& F)
  {
    // Configuration at which to assemble
    TransverseDisplacementField config;
    config.L2GMap = L2GMap;
    config.displacement = dofValues;
    
    // Assemble stiffness
    Asm_T->Assemble(&config, K);

    // Assemble residual
    Asm_g->Assemble(&config, F);

    // done
    return;
  }

  
  // Main functionality. Solve
  void HeavyTensionedCable::Solve(double* sol)
  {
    // Assemble system
    PetscErrorCode ierr;
    ierr = MatZeroEntries(PD.stiffnessMAT); CHKERRV(ierr);
    ierr = VecZeroEntries(PD.resVEC);       CHKERRV(ierr);
    Evaluate(sol, PD.stiffnessMAT, PD.resVEC);
    
    // Set Dirichlet BCs in the matrix-vector system
    std::vector<int> boundary{0, nNodes-1};
    std::vector<double> bvalues{0., 0.};
    PD.SetDirichletBCs(boundary, bvalues);

    // Solve
    PD.Solve();

    // Get the solution
    const int nTotalDof = L2GMap->GetTotalNumDof();
    double* solution;
    ierr = VecGetArray(PD.solutionVEC, &solution); CHKERRV(ierr);
    for(int a=0; a<nTotalDof; ++a)
      sol[a] = solution[a];
    ierr = VecRestoreArray(PD.solutionVEC, &solution); CHKERRV(ierr);

    // done
    return;
  }
  
}
