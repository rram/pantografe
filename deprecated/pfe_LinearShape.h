/** \file pfe_LinearShape.h
 * \brief Defines the class of linear shape functions pfe::LinearShape
 * \author Ramsharan Rangarajan
 * Last modified: September 12, 2021
 */

#ifndef PFE_LINEAR_SHAPE_H
#define PFE_LINEAR_SHAPE_H

#include <pfe_ShapeFunction.h>

namespace pfe
{
  /**
     \brief Set of linear shape functions in spatial_dimension dimensions

     The linear shape functions in spatial_dimension dimensions are precisely the barycentric
     coordinates of a point in the simplex defined by spatial_dimension+1 points.

     Since quadrature points are often expressed in baricentric coordinates, this class was
     implemented to take the spatial_dimension+1 barycentric coordinates of a point as arguments only. 

     Since barycentric coordinates are linearly dependent, the class chooses spatial_dimension coordinates
     as arguments of the shape functions. This choice is made by shuffling the coordinates so that the first 
     spatial_dimension coordinates are the linearly independent ones.
  */

  template <int spatial_dimension>
    class LinearShape: public ShapeFunction
    {
    public:
      //! Constructor \n
      //! \param iMap Shuffle of the barycentric coordinates. iMap[a] returns the position of the original 
      //! a-th barycentric coordinate after shuffling.
      //! If not provided, an identity mapping is assumed iMap[a]=a

      //! \warning No way to know if iMap has the proper length.
      inline LinearShape() {}
      inline virtual ~LinearShape(){}

      //! Disable copy and assignment
      LinearShape(const LinearShape<spatial_dimension>&) = delete;
      LinearShape<spatial_dimension>& operator=(const LinearShape<spatial_dimension>&) = delete;

      // Accessors/Mutators
      inline int GetNumberOfFunctions() const override
      { return spatial_dimension+1; }

      inline int GetNumberOfVariables() const override
      { return spatial_dimension;   }
  
      // Functionality

      //! @param a shape function number
      //! @param x first spatial_dimension barycentric coordinates of the point
      //! \warning Does not check range for parameter a
      inline double Val(const int a, const double *x) const override
      {
	if(a!=spatial_dimension)
	  return x[a];
	else
	  {
	    double va = 0;
	    for(int k=0; k<spatial_dimension; ++k)
	      va += x[k];
      	    return 1.-va;
	  }
      }

      //! @param a shape function number
      //! @param x first spartial_dimension barycentric coordinates of the point
      //! @param i partial derivative number 
      //! Returns derivative with respect to the barycentric coordinates
      //! \warning Does not check range for parameters a and i
      inline double DVal(const int a, const double *x, int i) const override
      {
	if(a!=spatial_dimension)
	  return a==i? 1.:0.;
	else
	  return -1.;
      }
      
    };
}

#endif
