
/** \file pfe_OverhangDropper.cpp
 * \brief Implements the class pfe::OverhangDropper
 * \author Ramsharan Rangarajan
 * Last modified: October 19, 2021
 */

#include <pfe_OverhangDropper.h>

namespace pfe
{
  // Evaluate contributions to the force and stiffness
  void OverhangDropper::Evaluate(const double* dofValues, Mat& K, Vec& F)
  {
    PetscErrorCode ierr;

    // Functional: 1/2 EA ((zStructure+wStructure)-zDropperSupport + Length)^2
    //           = 1/2 EA (wStructure-zEncumbrance + Length)^2
    
    // Update forces from dropper stiffness
    double value = params.EA*(params.zEncumbrance-params.length);
    ierr = VecSetValues(F, 1, &dofNum, &value, ADD_VALUES); CHKERRV(ierr);
    ierr = VecAssemblyBegin(F); CHKERRV(ierr);
    ierr = VecAssemblyEnd(F);   CHKERRV(ierr);

    
    // Update stiffness
    ierr = MatSetValues(K, 1, &dofNum, 1, &dofNum, &params.EA, ADD_VALUES); CHKERRV(ierr);
    ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);

    // done
  }
  
}
