
/** \file pfe_DynamicConfiguration.h
 * \brief Defines the class pfe::DynamicConfiguration
 * \author Ramsharan Rangarajan
 * Last modified: March 15, 2022
 */

#ifndef PFE_DYNAMIC_CONFIGURATION_H
#define PFE_DYNAMIC_CONFIGURATION_H

#include <petscvec.h>
#include <fstream>
#include <vector>
#include <pfe_LocalToGlobalMap.h>

namespace pfe
{
  struct DynamicConfiguration
  {
    //! Constructor
    DynamicConfiguration(const int ndof);

    //! Destructor
    virtual ~DynamicConfiguration();

    //! Clean up
    void Destroy();

    //! Access
    inline Vec& GetDisplacement()
    { return U; }

    inline Vec& GetVelocity() 
    { return V; }
    
    inline Vec& GetAcceleration()
    { return A; }

    // Output
    void PlotState(const std::string filename,
		   const LocalToGlobalMap& L2GMap,
		   const std::vector<double>& coordinates);
    
  private:
    
    bool isDestroyed;
    Vec U; //!< Displacement
    Vec V; //!< Velocity
    Vec A; //!< Acceleration
    
  };
}

#endif
