
/** \file pfe_CatenarySupportedContactWire.h
 * \brief Defines the class pfe::CatenarySupportedContactWire
 * \author Ramsharan Rangarajan
 * Last modified: August 19, 2022
 */

#ifndef PFE_CATENARY_SUPPORTED_CONTACT_WIRE_H
#define PFE_CATENARY_SUPPORTED_CONTACT_WIRE_H

#include <pfe_HeavyTensionedBeam.h>
#include <pfe_IntercableDropper.h>
#include <pfe_SuspensionSpring.h>
#include <pfe_Pantograph.h>

namespace pfe
{
  //! Model for a contact wire supported by a catenary cable using inter-cable droppers
  class CatenarySupportedContactWire
  {
  public:
    //! Constructor
    CatenarySupportedContactWire(const CatenaryWireParams& cable_params,
				 const ContactWireParams& wire_params,
				 const IntercableDropperParams& dparams,
				 const DropperArrangement& dschedule,
				 const int nelm);
    
    //! Destructor
    virtual ~CatenarySupportedContactWire();

    //! Disable copy and assignment
    CatenarySupportedContactWire(const CatenarySupportedContactWire&) = delete;
    CatenarySupportedContactWire& operator=(const CatenarySupportedContactWire&) = delete;

    //! Add a suspension spring for the catenary
    void AddCatenarySuspensionSpring(const SuspensionSpringParams& spring_params,
				     const int node_num);

    //! Add a suspension spring for the contact wire
    void AddContactWireSuspensionSpring(const SuspensionSpringParams& spring_params,
					const int node_num);

    //! Add a pantograph for the contact wire
    template<class PantographParamType>
      void AddPantograph(const PantographParamType& panto_params);
    template<> void AddPantograph(const Pantograph_0_Params& panto_params);
    template<> void AddPantograph(const Pantograph_1_Params& panto_params);
    template<> void AddPantograph(const Pantograph_3_Params& panto_params);

    //! Complete setup
    void Setup();
    
    //! Access number of nodes
    inline int GetNumNodes() const
    { return nNodes; }
    
    //! Access coordinates
    inline const std::vector<double>& GetCoordinates() const
    { return catenary_cable->GetCoordinates(); }

    //! Access the number of catenary cable dofs
    inline int GetNumCatenaryCableDofs() const
    { return nCatenaryCableDofs; }

    //! Access the number of contact wire dofs
    inline int GetNumContactWireDofs() const
    { return nContactWireDofs; }
    
    //! Access number of dofs
    inline int GetTotalNumDof() const
    { return nTotalDof; }

    //! Access the catenary cable component
    inline const HeavyTensionedBeam& CatenaryCable() const
    { return *catenary_cable; }

    //! Access the contact wire component
    inline const HeavyTensionedBeam& ContactWire() const
    { return *contact_wire; }

    //! Access droppers
    inline const IntercableDropper& GetDropper(const int d) const
    {
      assert(d>=0 && d<static_cast<int>(droppers.size()));
      return *droppers[d];
    }

    //! Number of nonzero per row
    std::vector<int> GetSparsity() const;
    
    //! Main functionality: evaluate mass, stiffness and force vectors
    void Evaluate(const double time, const double* displacements, Mat& M, Mat& K, Vec& F) const;
    
    //! Main functionality: solve
    void StaticSolve(const double time,
		     const std::vector<std::pair<int,double>>& catenary_bcs,
		     const std::vector<std::pair<int,double>>& contact_bcs,
		     double* displacements);

    //! Main functionalty: match a prescribed value of sag
    void AdjustDropperLengths(const double time,
			      const std::vector<std::pair<int,double>>& catenary_bcs,
			      const std::vector<std::pair<int,double>>& contact_bcs,
			      const int nIters);
										     
    //! Determines the dropper displacement at the catenary and contact wires
    void GetDropperDisplacements(const double* dofValues, double* catenary_disp, double* contact_disp) const;

    //! Access the list of dropper nodes
    std::vector<int> GetDropperNodes() const;

    //! Compute transverse stiffness disrtribution
    void ComputeTransverseStiffnessDistribution(const double time, const double* displacements,
						const double Force, double* stiffness) const;


    //! Visualize the configuration
    void Write(const double* displacements,
	       const std::string catenary_fname,
	       const std::string contact_fname,
	       const std::string dropper_fname) const;
    
  protected:
    bool  isSetup;                            //!< has the model been setup
    const int nElements_per_Interval;         //!< Number of elements per interval
    const int nElements;                      //!< Number of elements
    const int nNodes;                         //!< Number of nodes
    const DropperArrangement dropperSchedule; //!< Dropper arrangement
    HeavyTensionedBeam* catenary_cable;       //!< Catenary cable
    HeavyTensionedBeam* contact_wire;         //!< Contact wire
    std::vector<IntercableDropper*> droppers; //!< Droppers
    std::vector<Pantograph*> pantographs;     //!< pantographs

    LocalToGlobalMap* catenary_L2GMap;  //!< local to global map for the catenary cable
    LocalToGlobalMap* contact_L2GMap;   //!< local to global map for the contact wire
    
    int nCatenaryCableDofs;         //!< Number of catenary cable dofs
    int nContactWireDofs;           //!< Number of contact wire dofs
    int nTotalDof;                  //!< Total number of dofs
    mutable PetscData PD;           //!< Monolithic sparse data structures
    mutable LinearSolver linSolver; //!< Helper for linear solves
    IS IS_catenary_cable;           //!< Index set for catenary cable dofs
    IS IS_contact_wire;             //!< Index set for contact wire dofs
  };
 
}


#endif
