

/** \file pfe_DropperSupportedCatenaryCable.cpp
 * \brief Implements the class DropperSupportedCatenaryCable
 * \author Ramsharan Rangarajan
 * Last modified: October 19, 2021
 */

#include <pfe_DropperSupportedCatenaryCable.h>

namespace pfe
{
  // Constructor
  DropperSupportedCatenaryCable::
  DropperSupportedCatenaryCable(const HeavyTensionedCableParams& cable_params, const int nelm,
				const std::vector<OverhangDropperParams>& dparams)
    :HeavyTensionedCable(cable_params, nelm),
     nDroppers(static_cast<int>(dparams.size()))
  {
    droppers.resize(nDroppers);
    for(int d=0; d<nDroppers; ++d)
      {
	// Identify coupling dof of this dropper
	const double dx = cable_params.span/static_cast<double>(nelm);
	int node = static_cast<int>(std::round(dparams[d].coord/dx)); // = dof number as well

	// Create dropper
	droppers[d] = new OverhangDropper(dparams[d], node);
      }
  }
     
  // Destructor
  DropperSupportedCatenaryCable::~DropperSupportedCatenaryCable()
  { for(auto& d:droppers) delete d; }
  
  // Assemble the stiffness and force vector
  void DropperSupportedCatenaryCable::Evaluate(const double* dofValues, Mat& K, Vec& F)
  {
    // Contributions from the catenary
    HeavyTensionedCable::Evaluate(dofValues, K, F);

    // Permit new nonzeros
    PetscErrorCode ierr;
    ierr = MatSetOption(K, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);

    // Assemble contributions from droppers
    for(auto& d:droppers)
      d->Evaluate(dofValues, K, F);

    // done
  }

}
