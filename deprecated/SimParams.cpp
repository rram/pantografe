// Sriramajayam

#include "SimParams.h"
#include <set>


// static simulation parameters
Static_SimParams read_static_simulation_parameters(const std::string sim_file, const std::string sim_tag)
{
  Static_SimParams sp;
  
  assert(std::filesystem::path(sim_file).extension()==".json" &&
	 "Expected simulation options file to have .json extension");
  std::fstream jfile;
  jfile.open(sim_file, std::ios::in);
  assert((jfile.good() && jfile.is_open()) && "Could not open simulation options file");

  // get the data
  auto J = nlohmann::json::parse(jfile);
  jfile.close();

  // read options
  auto it = J.find(sim_tag);
  assert(it!=J.end() && "Could not find simulation id in input file");
  auto& j = *it;
  assert(j.contains("simulation type")              && "Input file missing simulation type tag");
  assert(j.contains("parameters file")              && "Input file missing parameters file tag");
  assert(j.contains("num elements")                 && "Input file missing num elements tag");
  assert(j.contains("output directory")             && "Input file missing output directory tag");
  assert(j.contains("length adjustment iterations") && "Input file missing length adjustment iterations tag");
  assert(j.contains("force")                        && "Input file missing force tag");

  j["simulation type"].get_to(sp.simulation_type);
  j["parameters file"].get_to(sp.params_file);
  j["num elements"].get_to(sp.nelm);
  j["output directory"].get_to(sp.outdir);
  j["length adjustment iterations"].get_to(sp.num_length_iters);
  j["force"].get_to(sp.force_val);

  assert((sp.params_file.empty()==false && std::filesystem::exists(sp.params_file)) &&
	 "Cannot find ohe file specified in input file");
  assert(sp.nelm>0 && "Expected positive number of elements in input file");
  assert(sp.num_length_iters>=0 && "Unexpected number of length adjustment iterations specified in input file");

  // create the output directory if it does not exist, erase .dat files it does
  if(std::filesystem::exists(sp.outdir)==true)
    {
      std::cout << "Output directory " << sp.outdir << " exists. Erasing dat files" << std::endl;
      auto it_dir = std::filesystem::directory_iterator(sp.outdir);
      for(auto& it:it_dir)
	if(it.path().extension()==".dat")
	  {
	    auto flag = std::filesystem::remove(it.path());
	    assert(flag==true && "Could not erase file");
	  }
    }
  else
    {
      std::cout << "Creating output directory \"" << sp.outdir << "\"" << std::endl;
      auto flag  = std::filesystem::create_directory(sp.outdir);
      assert(flag==true && "Could not create output directory");
    }

  // should not have any other tags
  std::set<std::string> tags{"simulation type", "parameters file", "num elements", "output directory", "length adjustment iterations", "force", "_comment"};
  for(auto jt:j.items())
    if(tags.find(jt.key())==tags.end())
      {
	std::cout << "Unexpected tag " << jt.key() << " found in input file " << std::endl;
	assert(false);
      }

  return sp;
}




// dynamic simulation parameters
Dynamic_SimParams read_dynamic_simulation_parameters(const std::string sim_file, const std::string sim_id)
{
  Dynamic_SimParams sp;
  
  assert(std::filesystem::path(sim_file).extension()==".json");
  std::fstream jfile;
  jfile.open(sim_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());

  // get the data
  auto J = nlohmann::json::parse(jfile);
  jfile.close();

  // read options
  auto it = J.find(sim_id);
  assert(it!=J.end());
  auto& j = *it;
  assert(j.contains("simulation type")    && "Input file missing simulation type tag");
  assert(j.contains("parameters file")    && "Input file missing params file tag");
  assert(j.contains("num spans")          && "Input file missing num spans");
  assert(j.contains("pantograph type")    && "Input file missing pantograph type");
  assert(j.contains("pantograph tag")     && "Input file missing pantograph tag");
  assert(j.contains("contact type")       && "Input file missing contact type");
  assert(j.contains("num elements")       && "Input file missing num elements tag");
  assert(j.contains("time integrator")    && "Input file missing time integrator tag");
  assert(j.contains("time step")          && "Input file missing time step tag");
  assert(j.contains("num time steps")     && "Input file missing num time steps tag");
  assert(j.contains("output directory")   && "Input file missing output directory tag");

  j["simulation type"].get_to(sp.simulation_type);
  j["parameters file"].get_to(sp.params_file);
  j["num spans"].get_to(sp.num_spans);
  j["pantograph type"].get_to(sp.panto_type);
  j["pantograph tag"].get_to(sp.panto_tag);
  j["contact type"].get_to(sp.contact_type);
  j["num elements"].get_to(sp.nelm);
  j["time integrator"].get_to(sp.time_integrator_type);
  j["time step"].get_to(sp.timestep); 
  j["num time steps"].get_to(sp.num_time_steps);
  j["output directory"].get_to(sp.outdir);

  // list of tags
  std::set<std::string> tags{"simulation type", "parameters file", "num spans", "pantograph type", "pantograph tag", "contact type",
      "num elements", "time integrator", "time step", "num time steps", "output directory", "_comment"};
  
  // time integrator parameter
  if(sp.time_integrator_type=="generalized alpha")
    {
      assert(j.contains("spectral radius") && "Input file missing spectral radius tag");
      j["spectral radius"].get_to(sp.time_integrator_param);
      tags.insert("spectral radius");
    }
  else if(sp.time_integrator_type=="hht alpha")
    {
      assert(j.contains("alpha") && "Input file missing alpha tag");
      j["alpha"].get_to(sp.time_integrator_param);
      tags.insert("alpha");
    }

  // checks on inputs
  assert(sp.params_file.empty()==false && std::filesystem::exists(sp.params_file));
  assert(sp.nelm>0 && sp.timestep>0 && sp.num_time_steps>0 && sp.num_spans>=1);
  assert((sp.panto_type==0 || sp.panto_type==1 || sp.panto_type==3) && "Expected pantograph type to be 0, 1 or 3");
  assert((sp.contact_type=="persistent" || sp.contact_type=="intermittent") && "Expected contact type to be persistent or intermittent");
  if(sp.simulation_type=="linear")
    assert(sp.contact_type=="persistent" && "Linear simulation more only supports persistent contact type");
  
  // create the output directory if it does not exist, erase .dat files it does
  if(std::filesystem::exists(sp.outdir)==true)
    {
      std::cout << "Output directory " << sp.outdir << " exists. Erasing dat files" << std::endl;
      auto it_dir = std::filesystem::directory_iterator(sp.outdir);
      for(auto& it:it_dir)
	if(it.path().extension()==".dat")
	  {
	    auto flag = std::filesystem::remove(it.path());
	    assert(flag==true && "Could not erase file");
	  }
    }
  else
    {
      std::cout << "Creating output directory \"" << sp.outdir << "\"" << std::endl;
      auto flag  = std::filesystem::create_directory(sp.outdir);
      assert(flag==true && "Could not create output directory");
    }

  // Check consistency of tags
  for(auto jt:j.items())
    if(tags.find(jt.key())==tags.end())
      {
	std::cout << "Unexpected tag " << jt.key() << " found in input file " << std::endl;
	assert(false);
      }

  // done
  return sp;
}

