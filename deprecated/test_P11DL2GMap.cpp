
/** \file test_P11DL2GMap.cpp
 * \brief Unit test for the class pfe::P11DL2GMap
 * \author Ramsharan Rangarajan
 * Last modified: September 24, 2021
 */

#include <pfe_P11DElement.h>
#include <cassert>
#include <random>
#include <algorithm>

using namespace pfe;

int main()
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  for(int trial=0; trial<10; ++trial)
    {
      std::uniform_int_distribution<> int_dis(10,20);
      const int nElements = int_dis(gen);
      const int nNodes = nElements+1;

      // Random node indexing
      std::vector<int> index2nodeMap(nNodes);
      for(int a=0; a<nNodes; ++a)
	index2nodeMap[a] = a;
      std::shuffle(index2nodeMap.begin(), index2nodeMap.end(), gen);

      // nodal coordinates
      std::uniform_real_distribution<> real_dis(0.,1.);
      std::vector<double> coordinates(nNodes);
      for(int n=0; n<nNodes; ++n)
	coordinates[index2nodeMap[n]] = static_cast<double>(n) + real_dis(gen);

      // element connectivity
      std::vector<int> connectivity(2*nElements);
      for(int e=0; e<nElements; ++e)
	{ connectivity[2*e] = index2nodeMap[e]+1;
	  connectivity[2*e+1] = index2nodeMap[e+1]+1; }

      // create elements
      SegmentGeometry<1>::SetGlobalCoordinatesArray(coordinates);
      std::vector<Element*> ElmArray(nElements);
      constexpr int NFields = 3;
      constexpr int nDof = 2;
      for(int e=0; e<nElements; ++e)
	ElmArray[e] = new P11DElement<NFields>(connectivity[2*e], connectivity[2*e+1]);

      // create local to global map
      P11DL2GMap L2GMap(ElmArray);

      // Test
      assert(L2GMap.GetNumElements()==nElements);
      assert(L2GMap.GetTotalNumDof()==nNodes*NFields);
  
      for(int e=0; e<nElements; ++e)
	{
	  assert(L2GMap.GetNumFields(e)==NFields);
	  for(int f=0; f<NFields; ++f)
	    {
	      assert(L2GMap.GetNumDofs(e, f)==nDof);
	      for(int a=0; a<nDof; ++a)
		assert(L2GMap.Map(f,a,e)==NFields*(connectivity[2*e+a]-1)+f);
	    }
	}
  
      // clean up
      for(auto& e:ElmArray)
	delete e;
    }
}
