
/** \file pfe_DynamicConfiguration.cpp
 * \brief Implements the class pfe::DynamicConfiguration
 * \author Ramsharan Rangarajan
 * Last modified: March 15, 2022
 */

#include <pfe_DynamicConfiguration.h>
#include <cassert>

namespace pfe
{
  // Constructor
  DynamicConfiguration::DynamicConfiguration(const int ndof)
    :isDestroyed(false)
  {
    PetscErrorCode ierr;
    ierr = VecCreate(PETSC_COMM_WORLD, &U);    CHKERRV(ierr);
    ierr = VecSetSizes(U, PETSC_DECIDE, ndof); CHKERRV(ierr);
    ierr = VecSetFromOptions(U);               CHKERRV(ierr);
    ierr = VecDuplicate(U, &V);                CHKERRV(ierr);
    ierr = VecDuplicate(U, &A);                CHKERRV(ierr);
  }

  // Destructor
  DynamicConfiguration::~DynamicConfiguration()
  { assert(isDestroyed); }

  // Clean up
  void DynamicConfiguration::Destroy()
  {
    assert(isDestroyed==false);
    PetscErrorCode ierr;
    ierr = VecDestroy(&U); CHKERRV(ierr);
    ierr = VecDestroy(&V); CHKERRV(ierr);
    ierr = VecDestroy(&A); CHKERRV(ierr);
    isDestroyed = true;
  }

  // Output
  void DynamicConfiguration::PlotState(const std::string filename,
				       const LocalToGlobalMap& L2GMap,
				       const std::vector<double>& coordinates)
  {
    assert(isDestroyed==false);

    // # nodes
    const int nNodes = static_cast<int>(coordinates.size());

    // # elements
    const int nElements = nNodes-1;

    // dof indices of nodal displacements/velocities/accelerations
    std::vector<int> dofIndx(nNodes);
    for(int e=0; e<nElements; ++e)
      dofIndx[e] = L2GMap.Map(0, 0, e);
    dofIndx[nNodes-1] = L2GMap.Map(0, 1, nElements-1);
    
    // Access
    PetscErrorCode ierr;
    double *disp, *vel, *acc;
    ierr = VecGetArray(U, &disp); CHKERRV(ierr);
    ierr = VecGetArray(V, &vel);  CHKERRV(ierr);
    ierr = VecGetArray(A, &acc);  CHKERRV(ierr);

    // Print
    std::fstream stream;
    stream.open(filename.c_str(), std::ios::out);
    assert(stream.good());
    for(int n=0; n<nNodes; ++n)
      {
	const int &dof = dofIndx[n];
	stream << coordinates[n]<<" "<<disp[dof]<<" "<<vel[dof]<<" "<<acc[dof]<<std::endl;
      }
    stream.close();

    // Restore
    ierr = VecGetArray(U, &disp); CHKERRV(ierr);
    ierr = VecGetArray(V, &vel);  CHKERRV(ierr);
    ierr = VecGetArray(A, &acc);  CHKERRV(ierr);

    // done
    return;
  }

}


