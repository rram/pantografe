
/** \file pfe_CatenarySupportedContactWire_addcomps.cpp
 * \brief Implements the class pfe::CatenarySupportedContactWire
 * \author Ramsharan Rangarajan
 * Last modified: August 19, 2022
 */

#include <pfe_CatenarySupportedContactWire.h>

namespace pfe
{
  // Add a suspension spring for the catenary
  void CatenarySupportedContactWire::
  AddCatenarySuspensionSpring(const SuspensionSpringParams& spring_params, const int node_num)
  {
    assert(isSetup==false);
    assert(node_num>=0 && node_num<nNodes);
    catenary_cable->AddSuspensionSpring(spring_params, node_num);  // node_num = local dof#
  }

  // Add a suspension spring for the contact wire
  void CatenarySupportedContactWire::
  AddContactWireSuspensionSpring(const SuspensionSpringParams& spring_params, const int node_num)
  {
    assert(isSetup==false);
    assert(node_num>=0 && node_num<nNodes);
    contact_wire->AddSuspensionSpring(spring_params, node_num);  // node_num = local dof#
  }

  // Add a pantograph of type 0
  template<>
  void CatenarySupportedContactWire::
  AddPantograph<Pantograph_0_Params>(const Pantograph_0_Params& panto_params)
  {
    assert(isSetup==false);
    pantographs.push_back( new Pantograph_0(panto_params, nTotalDof) );
    nTotalDof += 1;
    return;
  }

  // Add a pantograph of type 1
  template<>
  void CatenarySupportedContactWire::
  AddPantograph<Pantograph_1_Params>(const Pantograph_1_Params& panto_params)
  {
    assert(isSetup==false);
    pantographs.push_back( new Pantograph_1(panto_params, nTotalDof) );
    nTotalDof += 1;
    return;
  }

  // Add a pantogtaph of type 3
  template<>
  void CatenarySupportedContactWire::
  AddPantograph<Pantograph_3_Params>(const Pantograph_3_Params& panto_params)
  {
    assert(isSetup==false);
    pantographs.push_back( new Pantograph_3(panto_params, nTotalDof, nTotalDof+1, nTotalDof+2) );
    nTotalDof += 3;
    return;
  }
  
}
