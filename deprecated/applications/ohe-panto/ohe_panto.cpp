
/** \file ohe_panto.cpp
 * \brief Simulates the single span dynamic interaction between a pantograph and the OHE
 * \author Ramsharan Rangarajan
 * \application Simulates the single span dynamic interaction between a pantograph and the OHE
 */

/* Input:
   -i input file in json format with simulation options
   -s simulation id
*/

/* Output: 
   beam*.dat     beam configuration indexed by time step, columns: x w w-dot w-ddot 
   load*.dat     position of the load, indexed by time step
   history.dat   time history of the deflection of the free end of the beam
*/

#include <pfe_linear_Model_OHE_Panto.h>
#include <pfe_GenAlpha_with_Contact.h>
#include <pfe_HHTAlpha_with_Contact.h>
#include <pfe_Newmark_with_Contact.h>
#include <pfe_CLI11.hpp>
#include <pfe_json.hpp>
#include <iostream>
#include <fstream>

// simulation parameter
void read_simulation_parameters(const std::string sim_file, const std::string sim_id,
				std::string& params_file,
				int& panto_type, std::string& panto_tag,
				int& num_spans, int& nelm,
				std::string& time_integrator_type,
				double& time_integrator_param,
				double& timestep,
				int& num_time_steps,
				std::string& out_directory);

// time stepping
template<typename ModelType>
void Simulate(ModelType& model, const std::string out_directory, const int num_time_steps);


int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRQ(ierr);

  // read command line options
  std::string sim_file;          // simulation options
  std::string sim_id;            // simulation id
  CLI::App app;
  app.add_option("-i", sim_file, "json file with simulation parameters")->required()->check(CLI::ExistingFile);
  app.add_option("-s", sim_id, "simulation id")->required();
  CLI11_PARSE(app, argc, argv);

  // read simulation parameters
  std::string params_file;            // json file with ohe+pantograph data
  std::string out_directory;          // output directory
  int         num_spans;              // number of spans
  int         nelm;                   // number of elements between successive droppers
  int         panto_type;             // pantograph type = 1 or 3
  std::string panto_tag;              // pantograph tag in the parameter file
  std::string time_integrator_type;   // time integrator type
  double      time_integrator_param;  // parameter to be used with the time integrator
  double      timestep;               // time step
  int         num_time_steps;         // number of time steps
  read_simulation_parameters(sim_file,  sim_id,
			     params_file,
			     panto_type, panto_tag, num_spans, nelm,
			     time_integrator_type, time_integrator_param,
			     timestep, num_time_steps, out_directory);


  
  // ohe + pantograph parameter
  std::fstream jfile;
  jfile.open(params_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());

  pfe::OHESpan_ParameterPack ohe_params;
  jfile >> ohe_params;

  pfe::Pantograph_1_Params panto_1_params{.tag=panto_tag};
  pfe::Pantograph_3_Params panto_3_params{.tag=panto_tag};
  if(panto_type==1)
    jfile >> panto_1_params;
  else if(panto_type==3)
    jfile >> panto_3_params;
  jfile.close();

  // Create OHE + pantograph
  pfe::linear::MultiSpan_OHE ohe(0.0, ohe_params, num_spans, nelm);
  std::unique_ptr<pfe::Pantograph> panto;
  if(panto_type==1)
    panto = std::make_unique<pfe::Pantograph_1>(panto_1_params);
  else if(panto_type==3)
    panto = std::make_unique<pfe::Pantograph_3>(panto_3_params);

  // Time integrator
  const int nTotalDof = ohe.GetTotalNumDof()+panto->GetTotalNumDof();
  std::unique_ptr<pfe::TimeIntegrator_with_Contact> TI;
  if(time_integrator_type=="generalized alpha")
    TI = std::make_unique<pfe::GenAlpha_with_Contact>(time_integrator_param, nTotalDof, timestep);
  else if(time_integrator_type=="hht alpha")
    TI = std::make_unique<pfe::HHTAlpha_with_Contact>(time_integrator_param, nTotalDof, timestep);
  else if(time_integrator_type=="newmark")
    TI = std::make_unique<pfe::Newmark_with_Contact>(nTotalDof, timestep);
  else
    {
      assert(false && "Expected time integrator to be one of 'generalized alpha', 'hht alpha' or 'newmark'"); 
    }
  
  // create the model
  pfe::linear::Model_MultiSpanOHE_Panto model(ohe, *panto, *TI, {});

  // simulate
  Simulate(model, out_directory, num_time_steps);

  // clean up
  TI->Destroy();
  model.Destroy();
  ohe.Destroy();
  PetscFinalize();
}


// time stepping
template<typename ModelType>
void Simulate(ModelType& model, const std::string outdir, const int num_time_steps)
{
  const int nTotalDof = model.GetTotalNumDof();

  // Static initialization
  model.StaticInitialize();
  
  // Visualize
  model.Write({outdir+"/cat-0.dat",outdir+"/con-0.dat",outdir+"/dro-0.dat" }, outdir+"/panto-0.dat");

  // History at the contact point
  std::fstream hfile;
  hfile.open(outdir+"/history.dat", std::ios::out);
  assert(hfile.good());
  hfile << "# time \t contact force \t ohe (u, v, a) \t pantograph (u, v, a) "<< std::endl;
  std::array<double,3> ohe_uva   = model.GetOHEContactPointKinematics();
  std::array<double,3> panto_uva = model.GetPantographContactPointKinematics();
  hfile << 0.0 << "\t" << model.GetContactForce() << "\t"
	<< ohe_uva[0] << "\t" << ohe_uva[1] << "\t" << ohe_uva[2] << "\t"
    	<< panto_uva[0] << "\t" << panto_uva[1] << "\t" << panto_uva[2] << std::endl;

  // Moving pantograph
  std::cout << "Time step (of " << num_time_steps << ") " << std::endl;
  for(int tstep=0; tstep<num_time_steps; ++tstep)
    {
      std::cout << tstep+1 << std::endl;
      model.Advance();

      const std::string str = std::to_string(tstep+1)+".dat";
      model.Write({outdir+"/cat-"+str, outdir+"/con-"+str, outdir+"/dro-"+str}, outdir+"/panto-"+str);
      
      const double time =  model.GetTime();
      ohe_uva   = model.GetOHEContactPointKinematics();
      panto_uva = model.GetPantographContactPointKinematics();
      hfile << model.GetTime() << "\t" << model.GetContactForce() << "\t"
	    << ohe_uva[0]       << "\t" << ohe_uva[1]   << "\t" << ohe_uva[2] << "\t"
	    << panto_uva[0]     << "\t" << panto_uva[1] << "\t" << panto_uva[2] << std::endl;
    }
  hfile.close();
  
  // done
  return;
}



// simulation parameter
void read_simulation_parameters(const std::string sim_file, const std::string sim_id,
				std::string& params_file,
				int& panto_type, std::string& panto_tag, int& num_spans, int& nelm,
				std::string& time_integrator_type, double& time_integrator_param,
				double& timestep, int& num_time_steps,
				std::string& out_directory)
{
  assert(std::filesystem::path(sim_file).extension()==".json");
  std::fstream jfile;
  jfile.open(sim_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());

  // get the data
  auto J = nlohmann::json::parse(jfile);
  jfile.close();

  // read options
  auto it = J.find(sim_id);
  assert(it!=J.end());
  auto& j = *it;
  assert(j.contains("parameters file")    && "Input file missing params file tag");
  assert(j.contains("num spans")          && "Input file missing num spans");
  assert(j.contains("pantograph type")    && "Input file missing pantograph type");
  assert(j.contains("pantograph tag")     && "Input file missing pantograph tag");
  assert(j.contains("num elements")       && "Input file missing num elements tag");
  assert(j.contains("time integrator")    && "Input file missing time integrator tag");
  assert(j.contains("time step")          && "Input file missing time step tag");
  assert(j.contains("num time steps")     && "Input file missing num time steps tag");
  assert(j.contains("output directory")   && "Input file missing output directory tag");
  
  j["parameters file"].get_to(params_file);
  j["num spans"].get_to(num_spans);
  j["pantograph type"].get_to(panto_type);
  j["pantograph tag"].get_to(panto_tag);
  j["num elements"].get_to(nelm);
  j["time integrator"].get_to(time_integrator_type);
  j["time step"].get_to(timestep); 
  j["num time steps"].get_to(num_time_steps);
  j["output directory"].get_to(out_directory);

  // list of tags
  std::set<std::string> tags{"parameters file", "num spans", "pantograph type", "pantograph tag",
      "num elements", "time integrator", "time step", "num time steps", "output directory", "_comment"};
  
  // time integrator parameter
  if(time_integrator_type=="generalized alpha")
    {
      assert(j.contains("spectral radius") && "Input file missing spectral radius tag");
      j["spectral radius"].get_to(time_integrator_param);
      tags.insert("spectral radius");
    }
  else if(time_integrator_type=="hht alpha")
    {
      assert(j.contains("alpha") && "Input file missing alpha tag");
      j["alpha"].get_to(time_integrator_param);
      tags.insert("alpha");
    }

  // checks on inputs
  assert(params_file.empty()==false && std::filesystem::exists(params_file));
  assert(nelm>0 && timestep>0 && num_time_steps>0 && num_spans>=1);
  assert((panto_type==1 || panto_type==3) && "Expected pantograph type to be 1 or 3");
  
  // create the output directory if it does not exist, erase .dat files it does
  if(std::filesystem::exists(out_directory)==true)
    {
      std::cout << "Output directory " << out_directory << " exists. Erasing dat files" << std::endl;
      auto it_dir = std::filesystem::directory_iterator(out_directory);
      for(auto& it:it_dir)
	if(it.path().extension()==".dat")
	  {
	    auto flag = std::filesystem::remove(it.path());
	    assert(flag==true && "Could not erase file");
	  }
    }
  else
    {
      std::cout << "Creating output directory \"" << out_directory << "\"" << std::endl;
      auto flag  = std::filesystem::create_directory(out_directory);
      assert(flag==true && "Could not create output directory");
    }

  // Check consistency of tags
  for(auto jt:j.items())
    if(tags.find(jt.key())==tags.end())
      {
	std::cout << "Unexpected tag " << jt.key() << " found in input file " << std::endl;
	assert(false);
      }

  // done
  return;
}
