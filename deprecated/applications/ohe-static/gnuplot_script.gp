set term jpeg
set ylabel "Displacement [m]"
set xlabel "Distance [s]"
set autoscale xfix
set output "displacement.jpg"
plot "output/contact.dat" u 1:2 w l title "PantograFE"

set term jpeg
set ylabel "Stiffness [N/m]"
set xlabel "Distance [m]"
set autoscale xfix
set output "stiffness.jpg"
plot "output/stiffness.dat" u 1:2 w l title "PantograFE"
