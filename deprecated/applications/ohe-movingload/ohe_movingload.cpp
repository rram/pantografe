
/** \file ohe_movingload.cpp
 * \brief Simulating a moving load interacting with OHE cable system
 * \author Ramsharan Rangarajan
 * \application Simulating a moving load interacting with OHE cable system (\ref app_ohemovl)
 * \application Simulating a moving 3-dof pantograph on multi-span OHE cable system (\ref app_ohepanto)
 */

/* Input:
   -i input json file with simulation options
   -s simulation id in the input file
*/

/* Output: 
   catenary*.dat catenary cable configuration indexed by time step, columns: x w w-dot w-ddot 
   contact*.dat  contact wire configuration indexed by time step,   columns: x w w-dot w-ddot
   dropper*.dat  dropper cable configurations indexed by time step, columns: x w w-dot w-ddot
   load*.dat     pantograph configuration, indexed by time step,    columns: x w
*/

#include <pfe_linear_Model_MovingLoad.h>
#include <pfe_GenAlpha.h>
#include <pfe_HHTAlpha.h>
#include <pfe_Newmark.h>
#include <pfe_CLI11.hpp>
#include <pfe_json.hpp>
#include <iostream>
#include <fstream>

// simulation parameters
void read_simulation_parameters(const std::string sim_file, const std::string sim_id,
				std::string& params_file,
				std::string& panto_tag,
				int& num_spans, int& nelm,
				std::string& time_integrator_type,
				double& time_integrator_params,
				double& timestep,
				int& num_time_steps,
				std::string& out_directory);


// Time integration
template<class ModelType>
void Simulate(ModelType& model, const std::string out_directory, const int num_time_steps);
  
int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRQ(ierr);

  // read commandline options
  std::string sim_file;        // simulation options
  std::string sim_id;          // simulation id
  CLI::App app;
  app.add_option("-i", sim_file, "json file with simulation parameters")->required()->check(CLI::ExistingFile);
  app.add_option("-s", sim_id, "simulation id in input file")->required();
  CLI11_PARSE(app, argc, argv);

  std::string params_file;     // json file with cable+load data
  std::string out_directory;   // output directory
  int         num_spans;       // number of spans to consider
  int         nelm;            // number of elements for one cable span
  std::string panto_tag;       // pantograph tag in the parameter file
  std::string time_integrator_type;  // time integrator type
  double      time_integrator_param; // parameter to be used with the time integrator
  double      timestep;        // time step
  int         num_time_steps;  // number of time steps
  read_simulation_parameters(sim_file, sim_id, params_file,
			     panto_tag, num_spans, nelm,
			     time_integrator_type, time_integrator_param,
			     timestep, num_time_steps, out_directory);


  // OHE + load parameters
  std::fstream jfile;
  jfile.open(params_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());

  // OHE parameters
  pfe::OHESpan_ParameterPack ohe_pack;   
  jfile >> ohe_pack;

  // pantograph parameters
  pfe::Pantograph_0_Params panto_0_params{.tag=panto_tag};
  jfile >> panto_0_params;
  jfile.close();

  // Create the OHE span
  pfe::linear::MultiSpan_OHE ohe(0., ohe_pack, num_spans, nelm);

  // Time integrator
  const int nTotalDof = ohe.GetTotalNumDof();
  std::unique_ptr<pfe::TimeIntegrator> TI;
  if(time_integrator_type=="generalized alpha")
    TI = std::make_unique<pfe::GenAlpha>(time_integrator_param, nTotalDof, timestep);
  else if(time_integrator_type=="hht alpha")
    TI = std::make_unique<pfe::HHTAlpha>(time_integrator_param, nTotalDof, timestep);
  else if(time_integrator_type=="newmark")
    TI = std::make_unique<pfe::Newmark>(nTotalDof, timestep);
  else
    assert(false && "Expected time integrator to be one of 'generalized alpha', 'hht alpha' or 'newmark'");

  // Create an instance of the model and simulate
  pfe::linear::Model_MultiSpanOHE_MovingLoad model(ohe, panto_0_params, *TI);
  Simulate(model, out_directory, num_time_steps);
  
  // Clean up
  model.Destroy();
  ohe.Destroy();
  PetscFinalize();
}


// Time integration
template<class ModelType>
void Simulate(ModelType& model, const std::string out_directory, const int num_time_steps)
{
  const int nTotalDof = model.GetTotalNumDof();
    
  // Static solution
  model.StaticInitialize();

  // visualize
  //model.Write({out_directory+"/catenary-ref.dat", out_directory+"/contact-ref.dat", out_directory+"/dropper-ref.dat"},  out_directory+"/load-ref.dat");
  model.Write({out_directory+"/catenary-0.dat", out_directory+"/contact-0.dat", out_directory+"/dropper-0.dat"},  out_directory+"/load-0.dat");

  // Load point history
  std::fstream hfile;
  hfile.open(out_directory+"history.dat", std::ios::out);
  assert(hfile.good());
  hfile << "Time \t U \t V \t A " << std::endl;
  
  // Moving pantograph
  std::cout << "Time step (of " << num_time_steps << "): " << std::endl;
  for(int tstep=0; tstep<num_time_steps; ++tstep)
    {
      std::cout << tstep+1 << std::endl;
      
      model.Advance();
      std::string tstr = std::to_string(tstep+1)+".dat";
      model.Write({out_directory+"/catenary-"+tstr, out_directory+"/contact-"+tstr, out_directory+"/dropper-"+tstr}, out_directory+"/load-"+tstr);

      // Load point kinematics
      auto uva = model.GetLoadPointKinematics();
      hfile << model.GetTime() << " " << uva[0] << " " << uva[1] << " " << uva[2] << std::endl;
    }
	hfile.close();
      
  // done
  return;
}


// simulation parameter
void read_simulation_parameters(const std::string sim_file, const std::string sim_id,
				std::string& params_file, std::string& panto_tag, 
				int& num_spans, int& nelm,
				std::string& time_integrator_type, double& time_integrator_param,
				double& timestep, int& num_time_steps,
				std::string& out_directory)
{
  assert(std::filesystem::path(sim_file).extension()==".json");
  std::fstream jfile;
  jfile.open(sim_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());

  // get the data
  auto J = nlohmann::json::parse(jfile);
  jfile.close();

  // read options
  auto it = J.find(sim_id);
  assert(it!=J.end());
  auto& j = *it;
  assert(j.contains("parameters file")    && "Input file missing params file tag");
  assert(j.contains("num spans")          && "Input file missing num spans");
  assert(j.contains("load tag")           && "Input file missing load tag");
  assert(j.contains("num elements")       && "Input file missing num elements tag");
  assert(j.contains("time integrator")    && "Input file missing time integrator tag");
  assert(j.contains("time step")          && "Input file missing time step tag");
  assert(j.contains("num time steps")     && "Input file missing num time steps tag");
  assert(j.contains("output directory")   && "Input file missing output directory tag");

  j["parameters file"].get_to(params_file);
  j["num spans"].get_to(num_spans);
  j["load tag"].get_to(panto_tag);
  j["num elements"].get_to(nelm);
  j["time integrator"].get_to(time_integrator_type);
  j["time step"].get_to(timestep); 
  j["num time steps"].get_to(num_time_steps);
  j["output directory"].get_to(out_directory);

  // list of tags
  std::set<std::string> tags{"parameters file", "num spans", "load tag", 
      "num elements", "time integrator", "time step", "num time steps", "output directory", "_comment"};

  // time integrator parameter
  if(time_integrator_type=="generalized alpha")
    {
      assert(j.contains("spectral radius") && "Input file missing spectral radius tag");
      j["spectral radius"].get_to(time_integrator_param);
      tags.insert("spectral radius");
    }
  else if(time_integrator_type=="hht alpha")
    {
      assert(j.contains("alpha") && "Input file missing alpha tag");
      j["alpha"].get_to(time_integrator_param);
      tags.insert("alpha");
    }

  // checks on inputs
  assert(params_file.empty()==false && std::filesystem::exists(params_file));
  assert(nelm>0 && timestep>0 && num_time_steps>0 && num_spans>=1);
  
  // create the output directory if it does not exist, erase .dat files it does
  if(std::filesystem::exists(out_directory)==true)
    {
      std::cout << "Output directory " << out_directory << " exists. Erasing dat files" << std::endl;
      auto it_dir = std::filesystem::directory_iterator(out_directory);
      for(auto& it:it_dir)
	if(it.path().extension()==".dat")
	  {
	    auto flag = std::filesystem::remove(it.path());
	    assert(flag==true && "Could not erase file");
	  }
    }
  else
    {
      std::cout << "Creating output directory \"" << out_directory << "\"" << std::endl;
      auto flag  = std::filesystem::create_directory(out_directory);
      assert(flag==true && "Could not create output directory");
    }

  // Check consistency of tags
  for(auto jt:j.items())
    if(tags.find(jt.key())==tags.end())
      {
	std::cout << "Unexpected tag " << jt.key() << " found in input file " << std::endl;
	assert(false);
      }

  // done
  return;
}
