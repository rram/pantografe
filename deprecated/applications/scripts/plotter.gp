# Sriramajayam

# displacement
unset key
set term gif animate delay 5
set output "displacement.gif"
set yrange [-0.35:-0.1]
do for [i=0:600] {
cable_file = sprintf("cable-%d.dat", i)
load_file  = sprintf("load-%d.dat", i)
plot cable_file u 1:2 w lp, load_file u 1:2 w lp
}

# velocity
set term gif animate delay 5
set output "velocity.gif"
set yrange [-0.15:0.2]
do for [i=0:600] {
cable_file = sprintf("cable-%d.dat", i)
load_file = sprintf("load-%d.dat", i)
plot cable_file u 1:3 w lp, load_file u 1:($2+0.2) w lp
}

