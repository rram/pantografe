/*  Syntax:
   ./executable.out NUM_COLS filename colnum1 colname1 colnum2 colname2 ...
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <algorithm>

int NUM_COLS; // Number of columns in the data (user specified)
int NUM_ROWS=0; // Number of rows in the data

void calculateStats(const std::vector<std::vector<double>>& dataArray, std::ofstream& outputFile, int colnum, std::string colname) {
    if (dataArray.empty()) {
        std::cout << "No data found.\n";
        return;
    }
    
    double minVal=+1.e99;
    double maxVal=-1.e99;
    double sum = 0.0;
    double sumSquares = 0.0;
    
    for(int i=0; i<NUM_ROWS; i++){        
        double val=dataArray[i][colnum-1];
        //outputFile << colname << "  " << i << " : " << val << "\n";
        sum += val;
        sumSquares += val * val;
        minVal=(val<minVal)?val:minVal;
        maxVal=(val>maxVal)?val:maxVal;
    }
    double mean = sum / NUM_ROWS;
    double variance = sumSquares/NUM_ROWS - (mean * mean);
    double stdDev = std::sqrt(variance);

    outputFile << colname << " :\n";
    outputFile << "Mean: " << mean << std::endl;
    outputFile << "Standard Deviation: " << stdDev << std::endl;
    outputFile << "Minimum: " << minVal << std::endl;
    outputFile << "Maximum: " << maxVal << std::endl;
    outputFile << "---------------------\n";

}

int main(int argc, char** argv) {

    NUM_COLS = atoi(argv[1]);
    std::string filename=argv[2];
    std::vector<std::vector<double>> dataArray;
    std::string line;

    std::ifstream inputFile(filename);
    if (!inputFile) {
        std::cerr << "Error opening the file." << std::endl;
        return 1;
    }

    while (std::getline(inputFile, line)) {
        if (line[0] != '#') {
            std::istringstream ss(line);
            double value;
            std::vector<double> rowValues;

            for (int col = 0; ss >> value && col < NUM_COLS; ++col) {
                rowValues.push_back(value);
            }

            if (rowValues.size() == NUM_COLS) {
                dataArray.push_back(rowValues);
                NUM_ROWS++;
            } else {
                std::cout << "Warning: Skipping a line due to incorrect number of columns.\n";
            }
        }
    }
    inputFile.close();

    std::ofstream outputFile("statistics.dat");
    if (!outputFile) {
        std::cerr << "Error creating output file." << std::endl;
        return 1;
    }

    for(int i=3; i<argc; i=i+2){
        int colnum = atoi(argv[i]);
        std::string colname = argv[i+1];
        calculateStats(dataArray, outputFile, colnum, colname);
    }

    outputFile.close();
    return 0;
}

