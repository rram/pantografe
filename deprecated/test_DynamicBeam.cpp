
/** \file test_DynamicCatenary.cpp
 * \brief Unit test for the component pfe::HeavyTensionedBeam including dynamics
 * \author Ramsharan Rangarajan
 * Last modified: April 05, 2022
 */

#include <pfe_HeavyTensionedBeam.h>
#include <pfe_NewmarkIntegrator.h>
#include <fstream>
#include <iostream>

int main(int argc, char **argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  PetscErrorCode ierr;

  // Create beam without selfweight/tension
  pfe::ContactWireParams params{.span=0.41, .EI=16.276e-3, .Tension=0., .rhoA=60.e-3, .g=0., .zLevel=0.};
  const int nElements = 50;
  auto cwire = new pfe::HeavyTensionedBeam(params, nElements);

  // displacement field
  const auto& L2GMap = cwire->GetLocalToGlobalMap();
  const int nTotalDofs = L2GMap.GetTotalNumDof();
  std::vector<double> displacements(nTotalDofs);
  
  // Clamp left end, displace right end
  const int nNodes = nElements+1;
  std::vector<std::pair<int,double>> dirichlet_bcs{};
  dirichlet_bcs.push_back( std::make_pair(0, 0.) );                 // displacement at the left end
  dirichlet_bcs.push_back( std::make_pair(L2GMap.Map(0,2,0), 0.) ); // slope at the left end
  dirichlet_bcs.push_back( std::make_pair(nNodes-1,-0.04) );

  // Solve for static equilibrium
  cwire->StaticSolve(dirichlet_bcs, displacements.data());

  // Plot the static solution
  const auto& coordinates = cwire->GetCoordinates();
  std::fstream stream;
  stream.open((char*)"static-sol.dat", std::ios::out);
  stream << "#coordinate \t displacment";
  for(int n=0; n<nElements+1; ++n)
    stream << std::endl << coordinates[n] <<" "<<displacements[n];
  stream.close();
  
  // Petsc data structure
  pfe::PetscData PD;
  auto nnz = cwire->GetSparsity();
  PD.Initialize(nnz);
  
  // Evaluate the mass and stiffness matrices
  cwire->Evaluate(displacements.data(), PD.massMAT, PD.stiffnessMAT, PD.resVEC);

  // Create time integrator
  const double dt = 0.005;
  auto ts = new pfe::NewmarkIntegrator(nTotalDofs, dt);

  // Release right side node
  const std::vector<int> dirichlet_dofs{dirichlet_bcs[0].first, dirichlet_bcs[1].first};
  const std::vector<double> dirichlet_values{0.,0.};
  
  // Set operators
  ts->Set(PD.massMAT, PD.stiffnessMAT, dirichlet_dofs);
  
  // Initialize a dynamic configuration
  std::vector<double> zero(nTotalDofs);
  std::fill(zero.begin(), zero.end(), 0.);
  std::vector<int> indx(nTotalDofs);
  for(int a=0; a<nTotalDofs; ++a)
    indx[a] = a;
  ts->Initialize(displacements.data(), zero.data(), zero.data());
   
  // Access the state
  auto& dynConfig = ts->GetState();
  
  // Zero external force
  Vec Frc;
  ierr = VecDuplicate(PD.resVEC, &Frc); CHKERRQ(ierr);
  ierr = VecZeroEntries(Frc);           CHKERRQ(ierr);
  std::vector<double> FrcVec(nTotalDofs);

  // Tip trajectory, reaction force and reaction moment
  std::vector<double> times{}, Uy{}, Vy{}, Ry{}, Mz{};

  // Time stepping
  for(int tstep=0; tstep<400; ++tstep)
    {
      // Print the current state
      //dynConfig.PlotState("config-"+std::to_string(tstep)+".dat", L2GMap, coordinates);

      // Advance in time
      std::cout<<"\nTime step: "<<tstep;
      ts->Advance(Frc, dirichlet_values);
      times.push_back( ts->GetTime() );
      
      // tip displacement
      double val;
      int indx[] = {nNodes-1};
      VecGetValues(dynConfig.GetDisplacement(), 1, indx, &val);
      Uy.push_back(val);

      // tip velocity
      VecGetValues(dynConfig.GetVelocity(), 1, indx, &val);
      Vy.push_back(val);
      
      // reaction force and moment
      ts->GetForce(FrcVec.data());
      Ry.push_back(FrcVec[L2GMap.Map(0,0,0)]);
      Mz.push_back(FrcVec[L2GMap.Map(0,2,0)]);
    }
  
  const int nsteps = static_cast<int>(times.size());
  stream.open((char*)"tip.dat", std::ios::out);
  stream <<"# time \t Uy \t Vy \t Ry \t Mz";
  for(int i=0; i<nsteps; ++i)
    stream << std::endl << times[i] <<" "<< Uy[i]<<" "<<Vy[i]<<" "<<Ry[i]<<" "<<Mz[i];
  stream.close();
  
  // Clean up
  ts->Destroy();
  PD.Destroy();
  VecDestroy(&Frc);
  delete cwire;
  delete ts;
 
  // Finalize PETSc
  PetscFinalize();
}
