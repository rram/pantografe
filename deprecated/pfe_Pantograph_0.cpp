
/** \file pfe_Pantograph_0.cpp
 * \brief Implements the class pfe::Pantograph_0
 * \author Ramsharan Rangarajan
 * Last modified: April 04, 2023
 */

#include <pfe_Pantograph.h>

namespace pfe
{
  // Assemble contributions to the mass, stiffness and force vector
  void Pantograph_0::Evaluate(const double time,
			      const std::array<double,2>& ohe_shpvals, const Vec& U,
			      Mat& M, Mat& K, Vec& F) const
  {
    // check sizes of M, K and F
    int nrows;
    PetscErrorCode ierr;
    ierr = VecGetSize(U, &nrows); CHKERRV(ierr);
    assert(nrows==2);
    ierr = VecGetSize(F, &nrows); CHKERRV(ierr);
    assert(nrows==2);
    
    // check partition of unity property of shape function values
    assert(std::abs(ohe_shpvals[0]+ohe_shpvals[1]-1.)<1.e-4);
    
    // add contribution from the pantograph 
    const double& F0     = panto_0_params.force;
    const int indx[]     = {0,1};
    const double fvals[] = {F0*ohe_shpvals[0], F0*ohe_shpvals[1]};
    ierr = VecSetValuesLocal(F, 2, indx, fvals, ADD_VALUES); CHKERRV(ierr);
    ierr = VecAssemblyBegin(F);                              CHKERRV(ierr);
    ierr = VecAssemblyEnd(F);                                CHKERRV(ierr);
    
    // done
    return;
  }

}
