
/** \file pfe_CatenarySupportedContactWire_evaluate.cpp
 * \brief Implements the class pfe::CatenarySupportedContactWire
 * \author Ramsharan Rangarajan
 * Last modified: August 19, 2022
 */

#include  <pfe_CatenarySupportedContactWire.h>

namespace pfe
{
  // Main functionality: evaluate stiffness and force vectors
  void CatenarySupportedContactWire::Evaluate(const double time, const double* displacements, Mat& M, Mat& K, Vec& F) const
  {
    assert(isSetup==true);
    
    PetscErrorCode ierr;

    // Assemble contributions from the catenary cable
    Mat M_catenary_cable;
    Mat K_catenary_cable;
    Vec R_catenary_cable;
    ierr = MatGetLocalSubMatrix(M, IS_catenary_cable, IS_catenary_cable, &M_catenary_cable);     CHKERRV(ierr);
    ierr = MatGetLocalSubMatrix(K, IS_catenary_cable, IS_catenary_cable, &K_catenary_cable);     CHKERRV(ierr);
    ierr = VecGetSubVector(F, IS_catenary_cable, &R_catenary_cable);                             CHKERRV(ierr); 
    catenary_cable->Evaluate(displacements+0, M_catenary_cable, K_catenary_cable, R_catenary_cable);
    ierr = MatRestoreLocalSubMatrix(M, IS_catenary_cable, IS_catenary_cable, &M_catenary_cable); CHKERRV(ierr);
    ierr = MatRestoreLocalSubMatrix(K, IS_catenary_cable, IS_catenary_cable, &K_catenary_cable); CHKERRV(ierr);
    ierr = VecRestoreSubVector(F, IS_catenary_cable, &R_catenary_cable);                         CHKERRV(ierr);

    // Assemble contributions from the contact wire
    Mat M_contact_wire;
    Mat K_contact_wire;
    Vec R_contact_wire;
    ierr = MatGetLocalSubMatrix(M, IS_contact_wire, IS_contact_wire, &M_contact_wire);           CHKERRV(ierr);
    ierr = MatGetLocalSubMatrix(K, IS_contact_wire, IS_contact_wire, &K_contact_wire);           CHKERRV(ierr);
    ierr = VecGetSubVector(F, IS_contact_wire, &R_contact_wire);                                 CHKERRV(ierr);
    contact_wire->Evaluate(displacements+nCatenaryCableDofs, M_contact_wire, K_contact_wire, R_contact_wire);
    ierr = MatRestoreLocalSubMatrix(M, IS_contact_wire, IS_contact_wire, &M_contact_wire);       CHKERRV(ierr);
    ierr = MatRestoreLocalSubMatrix(K, IS_contact_wire, IS_contact_wire, &K_contact_wire);       CHKERRV(ierr);
    ierr = VecRestoreSubVector(F, IS_contact_wire, &R_contact_wire);                             CHKERRV(ierr);

    // TODO: Add contributions from pantographs
    
    // Complete assembly
    ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);   
    ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = VecAssemblyBegin(F);                     CHKERRV(ierr);
    ierr = VecAssemblyEnd(F);                       CHKERRV(ierr);

    // Permit new nonzeros
    ierr = MatSetOption(M, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
    ierr = MatSetOption(K, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE); CHKERRV(ierr);
    
    // Assemble contributions from droppers
    for(auto& dropper:droppers)
      dropper->Evaluate(displacements, M, K, F);

    // done
    return;
  }
 
}
