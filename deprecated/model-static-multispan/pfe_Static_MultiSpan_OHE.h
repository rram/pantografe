
/** \file pfe_Static_MultiSpan_OHE.h
 * \brief Defines the class pfe::Static_MultiSpan_OHE to simulate static behavior of a multiple spans of the OHE
 * \author Ramsharan Rangarajan
 * Last modified: January 17, 2023
 */

#ifndef PFE_STATIC_MULTI_SPAN_OHE_H
#define PFE_STATIC_MULTI_SPAN_OHE_H

#include <pfe_MultiSpan_OHE.h>

namespace pfe
{
  //!< Class to simulate static behavior of a multi-span OHE
  class Static_MultiSpan_OHE
  {
  public:
    //! Constructor
    Static_MultiSpan_OHE(const MultiSpan_OHE& input_ohe);

    //! Destructor
    virtual ~Static_MultiSpan_OHE();

    //! Disable copy and assignment
    Static_MultiSpan_OHE(const Static_MultiSpan_OHE&) = delete;
    Static_MultiSpan_OHE operator=(const Static_MultiSpan_OHE&) = delete;

    //! Access the ohe
    inline const MultiSpan_OHE& GetOHE() const
    { return ohe; }

    //! number of dofs
    inline int GetTotalNumDof() const
    { return ohe.GetTotalNumDof(); }

    //! Main functionality: static solve
    void Solve(double* displacements);
    
    //! Visualize the configuration
    inline void Write(const double* displacements,
		      const std::string catenary_fname,
		      const std::string contact_fname,
		      const std::string dropper_fname) const
    {
      return ohe.Write(displacements, catenary_fname, contact_fname, dropper_fname);
    }

  private:
    const MultiSpan_OHE& ohe;          //!< Multi span OHE
    mutable PetscData     PD;          //!< Monolithic sparse data structires
    mutable LinearSolver  linSolver;  //!< Helper for linear solves
  };
}

#endif
