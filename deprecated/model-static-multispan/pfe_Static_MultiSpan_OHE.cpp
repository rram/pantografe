
/** \file pfe_Static_MultiSpan_OHE.cpp
 * \brief Implementation of the class pfe::Static_MultiSpan_OHE
 * \author Ramsharan Rangarajan
 * Last modified: January 17, 2023
 */

#include <pfe_Static_MultiSpan_OHE.h>

namespace pfe
{
  // Constructor
  Static_MultiSpan_OHE::Static_MultiSpan_OHE(const MultiSpan_OHE& input_ohe)
    :ohe(input_ohe)
  {
    PetscErrorCode ierr;
    PetscBool      isInitialized;
    ierr = PetscInitialized(&isInitialized); CHKERRV(ierr);
    assert(isInitialized==PETSC_TRUE);

    // sparsity
    auto nz = ohe.GetSparsity();

    // Initialize PETSc data structures with known sparsity
    PD.Initialize(nz);
  }

  // Destructor
  Static_MultiSpan_OHE::~Static_MultiSpan_OHE()
  {
    // Check that PETSc has not been finalized
    PetscBool isFinalized;
    PetscErrorCode ierr;
    ierr = PetscFinalized(&isFinalized); CHKERRV(ierr);
    assert(isFinalized==PETSC_FALSE);
    PD.Destroy();
    linSolver.Destroy();

    // done
  }

  // Main functionality: solve
  void Static_MultiSpan_OHE::Solve(double* displacements)
  {
    PetscErrorCode ierr;
    
    // zero
    ierr = MatZeroEntries(PD.stiffnessMAT); CHKERRV(ierr);
    ierr = VecZeroEntries(PD.resVEC);       CHKERRV(ierr);
    ierr = VecZeroEntries(PD.solutionVEC);  CHKERRV(ierr);

    // Assemble contributions from all spans
    ohe.Evaluate(0., PD.solutionVEC, PD.massMAT, PD.stiffnessMAT, PD.resVEC);

    // solve
    linSolver.SetOperator(PD.stiffnessMAT);
    linSolver.Solve(PD.resVEC, PD.solutionVEC);

    // Get the solution
    int num_dofs;
    ierr = VecGetSize(PD.solutionVEC, &num_dofs);      CHKERRV(ierr);
    double* solution;
    ierr = VecGetArray(PD.solutionVEC, &solution);     CHKERRV(ierr);
    for(int i=0; i<num_dofs; ++i)
      displacements[i] = solution[i];
    ierr = VecRestoreArray(PD.solutionVEC, &solution); CHKERRV(ierr);
    
    // done
    return;
  }

}
