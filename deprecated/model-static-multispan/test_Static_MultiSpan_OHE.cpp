
/** \file test_Static_MultiSpan_OHE.cpp
 * \brief Unit test for class pfe::Static_MultiSpan_OHE
 * \author Ramsharan Rangarajan
 */

#include <pfe_Static_MultiSpan_OHE.h>
#include <filesystem>
#include <iostream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Parameters
  pfe::OHESpan_ParameterPack param_pack;
  assert(std::filesystem::exists("benchmark_params.json"));
  std::fstream jfile;
  jfile.open("benchmark_params.json", std::ios::in);
  assert(jfile.is_open() && jfile.good());
  jfile >> param_pack;
  jfile.close();

  // Create OHE spans
  const int nElm = 10;     // #elements between successive droppers
  const int num_spans = 4; // #spans
  auto ohe = new pfe::MultiSpan_OHE(0., param_pack, num_spans, nElm);

  // Create an instance of the model
  auto model = new pfe::Static_MultiSpan_OHE(*ohe);
  const int nTotalNumDof = model->GetTotalNumDof();
  std::cout << "Total dof: "<< nTotalNumDof << std::endl;
  
  // Solve
  std::vector<double> disp(nTotalNumDof, 0.);
  model->Solve(disp.data());

  // Visualize
  model->Write(disp.data(), "m-catenary.dat", "m-contact.dat", "m-droppers.dat");
  
  // Clean up
  delete ohe;
  PetscFinalize();
  
}
