
/** \file pfe_OverhangDropper.h
 * \brief Defines the class pfe::OverhangDropper
 * \author Ramsharan Rangarajan
 * Last modified: October 19, 2021
 */

#ifndef PFE_OVERHANG_DROPPER_H
#define PFE_OVERHANG_DROPPER_H

#include <petscvec.h>
#include <petscmat.h>

namespace pfe
{
  //! Struct defining parameters of a dropper suspended from a support
  struct OverhangDropperParams
  {
    double EA;
    double length;
    double coord;
    double zEncumbrance; // z(dropper support) - z(coupling structure)
  };

  
  //! Class defining a dropper suspended from a support
  class OverhangDropper
  {
  public:
    //! Constructor
    inline OverhangDropper(const OverhangDropperParams& dp, const int dof)
      :params(dp),
      dofNum(dof) {}
    
    //! Destructor
    inline virtual ~OverhangDropper() {}
    
    //! Disable copy and assignment
    OverhangDropper(const OverhangDropper&) = delete;
    OverhangDropper& operator=(const OverhangDropper&) = delete;

    //! Assemble contributions to the stiffness and force vector
    void Evaluate(const double* dofValues, Mat& K, Vec& F);

  protected:
    const OverhangDropperParams params;
    const int dofNum;
  };
}

#endif
