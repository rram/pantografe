// Sriramajayam

// Example 5.1 from Bowe & Mullarkey
// wheel: no intertia, only gravity effect
// beam:  inertial included, no gravity effect
// beam clamped at the left end, wheel starts from the left end
// monitor the deflection of the free end
#include <pfe_Newmark.h>
#include <pfe_HeavyTensionedBeam.h>
#include <pfe_PantographParams.h>
#include <iostream>
#include <fstream>
#include <filesystem>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL); CHKERRQ(ierr);

  {
    // Create beam
    pfe::HeavyTensionedBeamParams beam_params;
    beam_params.tag     = "beam";
    beam_params.span    = 7.62;
    beam_params.EI      = 2.07*4.58*1.e6;
    beam_params.Tension = 0.0;
    beam_params.rhoA    = 46.0;
    beam_params.g       = 0.0;
    beam_params.zLevel  = 0.;
    pfe::HeavyTensionedBeam beam(beam_params, 100);
    const int nNodes = beam.GetNumNodes();
    const int nDof   = 2*nNodes;

    // Mass, stiffness and force of beam
    const std::vector<int> nz = beam.GetSparsity();
    Mat Mb, Kb;
    Vec Ub, Fb;
    MatCreateSeqAIJ(PETSC_COMM_WORLD, nDof, nDof, PETSC_DEFAULT, nz.data(), &Mb);
    MatCreateSeqAIJ(PETSC_COMM_WORLD, nDof, nDof, PETSC_DEFAULT, nz.data(), &Kb);
    MatSetOption(Mb, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
    MatSetOption(Kb, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
    VecCreateSeq(PETSC_COMM_WORLD, nz.size(), &Ub);
    VecCreateSeq(PETSC_COMM_WORLD, nz.size(), &Fb);
    MatZeroEntries(Mb);
    MatZeroEntries(Kb);
    VecZeroEntries(Ub);
    VecZeroEntries(Fb);
    beam.Evaluate(0.0, Ub, Mb, Kb, Fb);
    
    
    // Pantograph parameters 
    pfe::Pantograph_1_Params panto_params;
    panto_params.tag = "panto";
    panto_params.xinit = 0.0;
    panto_params.speed = 50.8;
    panto_params.m     = 0.0;
    panto_params.force = -2629.0*9.81;
    panto_params.k     = 0.0;
    
    // Force for the cable + wheel system
    Vec F;
    VecCreateSeq(PETSC_COMM_WORLD, nDof, &F);
    
    // Time integrator
    const int num_time_steps = 500;
    const double timestep = 0.14/static_cast<double>(num_time_steps);
    pfe::Newmark TI(nDof, timestep);

    // Dirichlet bcs: left end clamped
    const std::vector<int> dirichlet_dofs{0, nNodes};

    // Set operators in the time integrator
    TI.SetOperators(Mb, Kb, dirichlet_dofs);
    
    // Set initial conditions
    std::vector<double> zero(nDof);
    std::fill(zero.begin(), zero.end(), 0.);
    TI.SetInitialConditions(0.0, zero.data(), zero.data(), zero.data());
    
    std::fstream pfile;
    pfile.open("history.dat", std::ios::out);

    // Time stepping
    for(int tstep=0; tstep<num_time_steps; ++tstep)
      {
	std::cout << "Time step " << tstep+1 << " of " << num_time_steps << std::endl;
	const double time = static_cast<double>(tstep+1)*timestep;
      
	// mass position
	const double x = panto_params.xinit + time*panto_params.speed;

	// active dofs
	auto dof_shp = beam.GetActiveDisplacementDofs(x);

	// F = Fb + fp r
	VecZeroEntries(F);
	VecCopy(Fb, F);
	for(auto& it:dof_shp)
	  {
	    const int&     a = it.first;
	    const double& Na = it.second;
	    double val = panto_params.force*Na;
	    VecSetValues(F, 1, &a, &val, ADD_VALUES);
	  }
	VecAssemblyBegin(F);
	VecAssemblyEnd(F);
	
	// Advance
	TI.Advance(F);
		
	const auto& state = TI.GetState();
	auto& U = state.U;
	auto& V = state.V;
	auto& A = state.A;
	double *uvals, *vvals, *avals;
	VecGetArray(U, &uvals);
	VecGetArray(V, &vvals);
	VecGetArray(A, &avals);
	beam.Write(uvals, vvals, avals, "beam-"+std::to_string(tstep+1)+".dat");

	// history
	pfile << time << "\t" << uvals[nNodes-1] << std::endl;
	
	VecRestoreArray(U, &uvals);
	VecRestoreArray(V, &vvals);
	VecRestoreArray(A, &avals); 
      }
    pfile.close();
    
    // Clean up
    TI.Destroy();
    MatDestroy(&Mb);
    MatDestroy(&Kb);
    VecDestroy(&Ub);
    VecDestroy(&Fb);
    VecDestroy(&F);
  }
  
  // Clean up
  PetscFinalize();
}
