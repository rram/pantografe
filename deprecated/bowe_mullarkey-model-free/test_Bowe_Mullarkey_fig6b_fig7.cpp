// Sriramajayam

// Example 5.2 from Bowe & Mullarkey
// wheel: intertia and gravity effect
// beam:  inertia included, no gravity effect, complete acceleration accounted for
// beam clamped at the left end, wheel starts from the left end
// monitor the deflection of the free end

#include <pfe_GenAlpha_with_Contact.h>
#include <pfe_HeavyTensionedBeam.h>
#include <pfe_PantographParams.h>
#include <iostream>
#include <fstream>
#include <filesystem>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL); CHKERRQ(ierr);

  {
    // Create beam
    pfe::HeavyTensionedBeamParams beam_params;
    beam_params.tag     = "beam";
    beam_params.span    = 7.62;
    beam_params.EI      = 2.07*4.58*1.e6;
    beam_params.Tension = 0.0;
    beam_params.rhoA    = 46.0;
    beam_params.g       = 0.0;
    beam_params.zLevel  = 0.;
    pfe::HeavyTensionedBeam beam(beam_params, 400);
    const int nNodes = beam.GetNumNodes();

    // Mass, stiffness and force of beam
    std::vector<int> nz = beam.GetSparsity();
    Mat Mb, Kb;
    Vec Ub, Fb;
    MatCreateSeqAIJ(PETSC_COMM_WORLD, nz.size(), nz.size(), PETSC_DEFAULT, nz.data(), &Mb);
    MatCreateSeqAIJ(PETSC_COMM_WORLD, nz.size(), nz.size(), PETSC_DEFAULT, nz.data(), &Kb);
    MatSetOption(Mb, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
    MatSetOption(Kb, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
    VecCreateSeq(PETSC_COMM_WORLD, nz.size(), &Ub);
    VecCreateSeq(PETSC_COMM_WORLD, nz.size(), &Fb);
    MatZeroEntries(Mb);
    MatZeroEntries(Kb);
    VecZeroEntries(Ub);
    VecZeroEntries(Fb);
    beam.Evaluate(0.0, Ub, Mb, Kb, Fb);
  		 
    // Pantograph parameters
    pfe::Pantograph_1_Params panto_params;
    panto_params.tag = "panto";
    panto_params.xinit = 0.0;
    panto_params.speed = 50.8;
    panto_params.m     = 2629.0;
    panto_params.force = -panto_params.m*9.81;
    panto_params.k     = 0.0;

    // Sparsity: cable + wheel
    const int nDof    = beam.GetTotalNumDof()+1;
    const int lastDof = nDof-1;
    for(auto& x:nz) x+=2;
    nz.push_back(nDof);

    // Mass of beam + wheel system
    Mat M;
    MatCreateSeqAIJ(PETSC_COMM_WORLD, nDof, nDof, PETSC_DEFAULT, nz.data(), &M);
    MatSetOption(M, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
    MatZeroEntries(M);
    for(int row=0; row<nDof-1; ++row)
      {
	int           ncols;
	const int    *cols;
	const double *vals;
	MatGetRow(Mb, row, &ncols, &cols, &vals);
	MatSetValues(M, 1, &row, ncols, cols, vals, INSERT_VALUES);
	MatRestoreRow(Mb, row, &ncols, &cols, &vals);
      }
    MatSetValues(M, 1, &lastDof, 1, &lastDof, &panto_params.m, INSERT_VALUES);
    MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(M, MAT_FINAL_ASSEMBLY);
        
    // Stiffness of beam + wheel
    Mat K;
    MatCreateSeqAIJ(PETSC_COMM_WORLD, nDof, nDof, PETSC_DEFAULT, nz.data(), &K);
    MatSetOption(K, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
    MatZeroEntries(K);
    for(int row=0; row<nDof-1; ++row)
      {
	int           ncols;
	const int    *cols;
	const double *vals;
	MatGetRow(Kb, row, &ncols, &cols, &vals);
	MatSetValues(K, 1, &row, ncols, cols, vals, INSERT_VALUES);
	MatRestoreRow(Kb, row, &ncols, &cols, &vals);
      }
    MatSetValues(K, 1, &lastDof, 1, &lastDof, &panto_params.k, INSERT_VALUES);
    MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(K, MAT_FINAL_ASSEMBLY);
        
    // Force of beam + wheel
    Vec F;
    VecCreateSeq(PETSC_COMM_WORLD, nDof, &F);
    VecZeroEntries(F);
    {
      double* fvals;
      VecGetArray(Fb, &fvals);
      std::vector<int> indx(nDof-1);
      for(int i=0; i<nDof-1; ++i)
	indx[i] = i;
      VecSetValues(F, nDof-1, indx.data(), fvals, INSERT_VALUES);
      VecSetValues(F, 1, &lastDof, &panto_params.force, INSERT_VALUES);
      VecAssemblyBegin(F);
      VecAssemblyEnd(F);
      VecRestoreArray(Fb, &fvals);
    }
  
    // Time integrator
    const int num_time_steps = 1500;
    const double timestep = 0.15/static_cast<double>(num_time_steps);
    pfe::GenAlpha_with_Contact TI(1.0, nDof, timestep);

    // Dirichlet bcs: left end clamped
    const std::vector<int>    dirichlet_dofs{0, nNodes};
    
    // Set operators in the time integrator
    TI.SetOperators(M, K, F, dirichlet_dofs);
    
    // Set initial conditions
    std::vector<double> zero(nDof);
    std::fill(zero.begin(), zero.end(), 0.);
    TI.SetInitialConditions(0.0, zero.data(), zero.data(), zero.data(), 0.);

    // Constraint vector
    Vec rvec;
    VecCreateSeq(PETSC_COMM_WORLD, nDof, &rvec);

    std::fstream pfile;
    pfile.open("history.dat", std::ios::out);
    
    // Time stepping
    for(int tstep=0; tstep<num_time_steps; ++tstep)
      {
	std::cout << "Time step " << tstep+1 << " of " << num_time_steps << std::endl;
									    
	const double time = static_cast<double>(tstep+1)*timestep;
      
	// mass position
	const double x = panto_params.xinit + TI.GetNextEvaluationTime()*panto_params.speed;
	
	// active dofs
	auto dof_shp = beam.GetActiveDofs(x);

	// constraint vector
	VecZeroEntries(rvec);
	for(auto& it:dof_shp)
	  VecSetValues(rvec, 1, &it.first, &it.second, INSERT_VALUES);
	const double mone = -1.;
	VecSetValues(rvec, 1, &lastDof, &mone, INSERT_VALUES);
	VecAssemblyBegin(rvec);
	VecAssemblyEnd(rvec);

	// advance
	TI.Advance(rvec);
	
	const auto& state = TI.GetState();
	auto& U = state.U;
	auto& V = state.V;
	auto& A = state.A;
	double *uvals, *vvals, *avals;
	VecGetArray(U, &uvals);
	VecGetArray(V, &vvals);
	VecGetArray(A, &avals);
	//beam.Write(uvals, vvals, avals, "beam-"+std::to_string(tstep+1)+".dat");

	// history
	pfile << time << "\t" << uvals[nNodes-1] << "\t" << TI.GetContactForce()/panto_params.force << std::endl;
	
	VecRestoreArray(U, &uvals);
	VecRestoreArray(V, &vvals);
	VecRestoreArray(A, &avals);
      }
    pfile.close();
    
  // Clean up
  TI.Destroy();
  MatDestroy(&Mb);
  MatDestroy(&Kb);
  VecDestroy(&Ub);
  VecDestroy(&Fb);
  MatDestroy(&M);
  MatDestroy(&K);
  VecDestroy(&F);
  VecDestroy(&rvec);
}
  
// Clean up
PetscFinalize();
}
