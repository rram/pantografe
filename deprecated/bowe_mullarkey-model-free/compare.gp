# fig 5a
set term png size 1024,768
set output 'fig5a_compare.png'
set xtics font "monaco, 15"
set xtics out offset 0,-1
set ytics font "monaco, 15"
set ytics out offset -1,0
set bmargin 5
set lmargin 15
set rmargin 5
set key noautotitle
set grid
set xrange [0:0.15]
set yrange [-0.5:0.1]
plot \
"./../.././../../../data/bowe_mullarkey/5a.csv" u 1:2 w p pt 6 lc rgb "red", \
"history.dat" u 1:2 w p pt 7 lc rgb "blue"



# fig 5b
set term png size 1024,768
set output 'fig5b_compare.png'
set xtics font "monaco, 15"
set xtics out offset 0,-1
set ytics font "monaco, 15"
set ytics out offset -1,0
set bmargin 5
set lmargin 15
set rmargin 5
set key noautotitle
set grid
set xrange [0:0.15]
set yrange [-0.5:0.0]
plot \
"./../.././../../../data/bowe_mullarkey/5b.csv" u 1:2 w p pt 6 lc rgb "red", \
"history.dat" u 1:2 w p pt 7 lc rgb "blue"





# fig 6a
set term png size 1024,768
set output 'fig6a_compare.png'
set xtics font "monaco, 15"
set xtics out offset 0,-1
set ytics font "monaco, 15"
set ytics out offset -1,0
set bmargin 5
set lmargin 15
set rmargin 5
set key noautotitle
set grid
set xrange [0:0.15]
set yrange [-0.5:0.05]
plot \
"./../.././../../../data/bowe_mullarkey/6a.csv" u 1:2 w p pt 6 lc rgb "red", \
"history.dat" u 1:2 w p pt 7 lc rgb "blue"


# fig 6b
set term png size 1024,768
set output 'fig6b_compare.png'
set xtics font "monaco, 15"
set xtics out offset 0,-1
set ytics font "monaco, 15"
set ytics out offset -1,0
set bmargin 5
set lmargin 15
set rmargin 5
set key noautotitle
set grid
set xrange [0:0.15]
set yrange [-0.07:0.01]
plot \
"./../.././../../../data/bowe_mullarkey/6b.csv" u 1:2 w p pt 6 lc rgb "red", \
"history.dat" u 1:2 w p pt 7 lc rgb "blue"


# fig 7
set term png size 1024,768
set output 'fig7_compare.png'
set xtics font "monaco, 15"
set xtics out offset 0,-1
set ytics font "monaco, 15"
set ytics out offset -1,0
set bmargin 5
set lmargin 15
set rmargin 5
set key noautotitle
set grid
set xrange [0:0.15]
set yrange [-0.5:2]
plot \
"./data/7.csv" u 1:2 w p pt 6 lc rgb "red", \
"history.dat" u 1:3 w p pt 7 lc rgb "blue"





# fig 9a
set term png size 1024,768
set output 'fig9a_compare.png'
set xtics font "monaco, 15"
set xtics out offset 0,-1
set ytics font "monaco, 15"
set ytics out offset -1,0
set bmargin 5
set lmargin 15
set rmargin 5
set key noautotitle
set grid
set xrange [0:0.15]
set yrange [-0.12:0.02]
plot \
"./../.././../../../data/bowe_mullarkey/9a.csv" u 1:2 w p pt 6 lc rgb "red", \
"history.dat" u 1:2 w p pt 7 lc rgb "blue"


# fig 9b
set term png size 1024,768
set output 'fig9b_compare.png'
set xtics font "monaco, 15"
set xtics out offset 0,-1
set ytics font "monaco, 15"
set ytics out offset -1,0
set bmargin 5
set lmargin 15
set rmargin 5
set key noautotitle
set grid
set xrange [0:0.15]
set yrange [-0.5:0.1]
plot \
"./../../../validation/bowe_mullarkey/data/9b.csv" u 1:2 w p pt 6 lc rgb "red", \
"history.dat" u 1:2 w p pt 7 lc rgb "blue"




# fig 10a_local
set term png size 1024,768
set output 'fig10a_local_compare.png'
set xtics font "monaco, 15"
set xtics out offset 0,-1
set ytics font "monaco, 15"
set ytics out offset -1,0
set bmargin 5
set lmargin 15
set rmargin 5
set key noautotitle
set grid
set xrange [0:1.0]
set yrange [-0.5:0.1]
plot \
"./../.././../../../data/bowe_mullarkey/10a.csv" u 1:2 w p pt 6 lc rgb "red", \
"history-1.dat" u 1:2 w p pt 7 lc rgb "blue", \
"history-27.dat" u 1:2 w p pt 8 lc rgb "green"




# fig 10a_full
set term png size 1024,768
set output 'fig10a_full_compare.png'
set xtics font "monaco, 15"
set xtics out offset 0,-1
set ytics font "monaco, 15"
set ytics out offset -1,0
set bmargin 5
set lmargin 15
set rmargin 5
set key noautotitle
set grid
set xrange [0:1.0]
set yrange [-0.5:0.1]
plot \
"./data/10a.csv" u 1:2 w p pt 6 lc rgb "red", \
"history-1.dat" u 1:2 w p pt 7 lc rgb "blue", \
"history-27.dat" u 1:2 w p pt 8 lc rgb "green"




# fig 10b_local
set term png size 1024,768
set output 'fig10b_local_compare.png'
set xtics font "monaco, 15"
set xtics out offset 0,-1
set ytics font "monaco, 15"
set ytics out offset -1,0
set bmargin 5
set lmargin 15
set rmargin 5
set key noautotitle
set grid
set xrange [0:1.0]
set yrange [-0.6:0.1]
plot \
"./../.././../../../data/bowe_mullarkey/10b.csv" u 1:2 w p pt 6 lc rgb "red", \
"history.dat" u 1:2 w p pt 7 lc rgb "blue"



# fig 10b_full
set term png size 1024,768
set output 'fig10b_full_compare.png'
set xtics font "monaco, 15"
set xtics out offset 0,-1
set ytics font "monaco, 15"
set ytics out offset -1,0
set bmargin 5
set lmargin 15
set rmargin 5
set key noautotitle
set grid
set xrange [0:1.0]
set yrange [-0.6:0.1]
plot \
"./data/10b.csv" u 1:2 w p pt 6 lc rgb "red", \
"history.dat" u 1:2 w p pt 7 lc rgb "blue"



# fig 10c_local
set term png size 1024,768
set output 'fig10c_local_compare.png'
set xtics font "monaco, 15"
set xtics out offset 0,-1
set ytics font "monaco, 15"
set ytics out offset -1,0
set bmargin 5
set lmargin 15
set rmargin 5
set key noautotitle
set grid
set xrange [0:1.0]
set yrange [-1.2:1.2]
plot \
"./../.././../../../data/bowe_mullarkey/10c.csv" u 1:2 w p pt 6 lc rgb "red", \
"history.dat" u 1:2 w l lw 1.5 lc rgb "blue"




# fig 10c_full
set term png size 1024,768
set output 'fig10c_full_compare.png'
set xtics font "monaco, 15"
set xtics out offset 0,-1
set ytics font "monaco, 15"
set ytics out offset -1,0
set bmargin 5
set lmargin 15
set rmargin 5
set key noautotitle
set grid
set xrange [0:1.0]
set yrange [-1.2:1.2]
plot \
"./data/10c.csv" u 1:2 w p pt 6 lc rgb "red", \
"history.dat" u 1:2 w l lw 1.5 lc rgb "blue"



