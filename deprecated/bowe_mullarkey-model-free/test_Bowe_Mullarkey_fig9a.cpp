// Sriramajayam

// Example 5.2 from Bowe & Mullarkey
// wheel: intertia and gravity effect
// beam:  inertia included, no gravity effect
// beam clamped at the right end, wheel starts from the left end
// monitor the deflection of the free end


#include <pfe_Newmark.h>
#include <pfe_HeavyTensionedBeam.h>
#include <pfe_PantographParams.h>
#include <iostream>
#include <fstream>
#include <filesystem>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL); CHKERRQ(ierr);

  {
    // Create beam
    pfe::HeavyTensionedBeamParams beam_params;
    beam_params.tag     = "beam";
    beam_params.span    = 7.62;
    beam_params.EI      = 2.07*4.58*1.e6;
    beam_params.Tension = 0.0;
    beam_params.rhoA    = 46.0;
    beam_params.g       = 0.0;
    beam_params.zLevel  = 0.;
    pfe::HeavyTensionedBeam beam(beam_params, 800);
    const int nNodes = beam.GetNumNodes();
    const int nDof   = 2*nNodes;

    // Mass, stiffness and force of beam
    const std::vector<int> nz = beam.GetSparsity();
    Mat Mb, Kb;
    Vec Ub, Fb;
    MatCreateSeqAIJ(PETSC_COMM_WORLD, nDof, nDof, PETSC_DEFAULT, nz.data(), &Mb);
    MatCreateSeqAIJ(PETSC_COMM_WORLD, nDof, nDof, PETSC_DEFAULT, nz.data(), &Kb);
    MatSetOption(Mb, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
    MatSetOption(Kb, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
    VecCreateSeq(PETSC_COMM_WORLD, nz.size(), &Ub);
    VecCreateSeq(PETSC_COMM_WORLD, nz.size(), &Fb);
    MatZeroEntries(Mb);
    MatZeroEntries(Kb);
    VecZeroEntries(Ub);
    VecZeroEntries(Fb);
    beam.Evaluate(0.0, Ub, Mb, Kb, Fb);
    
  		 
    // Pantograph parameters
    pfe::Pantograph_1_Params panto_params;
    panto_params.tag = "panto";
    panto_params.xinit = 0.0;
    panto_params.speed = 50.8;
    panto_params.m     = 2629.0;
    panto_params.force = -panto_params.m*9.81;
    panto_params.k     = 0.0;
    
    // Data structures for cable + wheel system
    Mat M, K;
    Vec F;
    MatDuplicate(Mb, MAT_DO_NOT_COPY_VALUES, &M);
    MatDuplicate(Kb, MAT_DO_NOT_COPY_VALUES, &K);
    MatSetOption(M, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);
    MatSetOption(K, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);
    MatSetOption(M, MAT_KEEP_NONZERO_PATTERN,  PETSC_TRUE);
    MatSetOption(K, MAT_KEEP_NONZERO_PATTERN,  PETSC_TRUE);
    VecCreateSeq(PETSC_COMM_WORLD, nDof, &F);
    
    // Time integrator
    const int num_time_steps = 1500;
    const double timestep = 0.15/static_cast<double>(num_time_steps);
    pfe::Newmark TI(nDof, timestep);

    // Dirichlet bcs: right end clamped
    const std::vector<int> dirichlet_dofs{nNodes-1, 2*nNodes-1};
    
    // Set initial conditions
    std::vector<double> zero(nDof);
    std::fill(zero.begin(), zero.end(), 0.);
    TI.SetInitialConditions(0.0, zero.data(), zero.data(), zero.data());
    
    std::fstream pfile;
    pfile.open("history.dat", std::ios::out);

    // Time stepping
    for(int tstep=0; tstep<num_time_steps; ++tstep)
      {
	std::cout << "Time step " << tstep+1 << " of " << num_time_steps << std::endl;
	const double time = static_cast<double>(tstep+1)*timestep;
      
	// mass position
	const double x = panto_params.xinit + time*panto_params.speed;

	// active dofs
	auto dof_shp = beam.GetActiveDisplacementDofs(x);

	// M = Mb + m (r x r)
	// K = Kb + k (r x r)
	MatZeroEntries(M);
	MatZeroEntries(K);
	MatCopy(Mb, M, DIFFERENT_NONZERO_PATTERN);
	MatCopy(Kb, K, DIFFERENT_NONZERO_PATTERN);
	for(auto& it:dof_shp)
	  {
	    const int&     a = it.first;
	    const double& Na = it.second;
	    double val;
	    for(auto& jt:dof_shp)
	      {
		const int&     b = jt.first;
		const double& Nb = jt.second;
		val = panto_params.m*Na*Nb;
		MatSetValues(M, 1, &a, 1, &b, &val, ADD_VALUES);
		val = panto_params.k*Na*Nb;
		MatSetValues(K, 1, &a, 1, &b, &val, ADD_VALUES);
	      }
	  }
	MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(M,   MAT_FINAL_ASSEMBLY);
	MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(K,   MAT_FINAL_ASSEMBLY);

	// Set operators
	TI.SetOperators(M, K, dirichlet_dofs);
	
	// F = Fb + fp r
	VecZeroEntries(F);
	VecCopy(Fb, F);
	for(auto& it:dof_shp)
	  {
	    const int&     a = it.first;
	    const double& Na = it.second;
	    double val = panto_params.force*Na;
	    VecSetValues(F, 1, &a, &val, ADD_VALUES);
	  }
	VecAssemblyBegin(F);
	VecAssemblyEnd(F);
	
	// Advance
	TI.Advance(F);
		
	const auto& state = TI.GetState();
	auto& U = state.U;
	auto& V = state.V;
	auto& A = state.A;
	double *uvals, *vvals, *avals;
	VecGetArray(U, &uvals);
	VecGetArray(V, &vvals);
	VecGetArray(A, &avals);
	//beam.Write(uvals, vvals, avals, "beam-"+std::to_string(tstep+1)+".dat");

	// history
	pfile << time << "\t" << uvals[0] << std::endl;
	
	VecRestoreArray(U, &uvals);
	VecRestoreArray(V, &vvals);
	VecRestoreArray(A, &avals); 
      }
    pfile.close();
    
    // Clean up
    TI.Destroy();
    MatDestroy(&Mb);
    MatDestroy(&Kb);
    VecDestroy(&Ub);
    VecDestroy(&Fb);
    MatDestroy(&M);
    MatDestroy(&K);
    VecDestroy(&F);
  }
  
  // Clean up
  PetscFinalize();
}
