
/** \file cable_movingload.cpp
 * \brief Eample of a moving load interacting with an overhead cable system
 * \author Ramsharan Rangarajan
 * \application Simulate dynamic interactions of a moving load with a cable.
 */

/* Input:
   -i input file in json format with simulation options
   -s simulation id
*/

/* Output: 
   cable*.dat cable configuration indexed by time step, columns: x w w-dot w-ddot 
   load*.dat" load location indexed by time step,       columns: x w
*/

#include <pfe_Model_MovingLoad.h>
#include <pfe_GenAlpha.h>
#include <pfe_HHTAlpha.h>
#include <pfe_Newmark.h>
#include <pfe_CLI11.hpp>
#include <pfe_json.hpp>
#include <iostream>
#include <fstream>

// simulation parameter
void read_simulation_parameters(const std::string sim_file, const std::string sim_id,
				std::string& params_file,
				std::string& cable_tag, std::string& panto_tag, std::string& spring_tag,
				int& num_spans, int& nelm,
				std::string& time_integrator_type,
				double& time_integrator_params,
				double& timestep,
				int& num_time_steps,
				std::string& out_directory);

// time stepping
template<class ModelType>
void Simulate(ModelType model, const std::string out_directory, const int num_time_steps);


int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR); CHKERRQ(ierr);

  // read command line options
  std::string sim_file;          // simulation options
  std::string sim_id;            // simulation id
  CLI::App app;
  app.add_option("-i", sim_file, "json file with simulation parameters")->required()->check(CLI::ExistingFile);
  app.add_option("-s", sim_id, "simulation id")->required();
  CLI11_PARSE(app, argc, argv);

  // read simulation parameters
  std::string params_file;     // json file with cable+load data
  std::string out_directory;   // output directory
  int         num_spans;       // number of spans to consider
  int         nelm;            // number of elements for one cable span
  std::string cable_tag;       // cable tag in the parameter file
  std::string panto_tag;       // pantograph tag in the parameter file
  std::string spring_tag;      // suspension spring tag in the parameter file
  std::string time_integrator_type;  // time integrator type
  double      time_integrator_param; // parameter to be used with the time integrator
  double      timestep;        // time step
  int         num_time_steps;  // number of time steps
  read_simulation_parameters(sim_file, sim_id, params_file,
			     cable_tag, panto_tag, spring_tag, num_spans, nelm,
			     time_integrator_type, time_integrator_param,
			     timestep, num_time_steps, out_directory);

  // cable + load + suspension spring parameters
  std::fstream jfile;
  jfile.open(params_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());
  // cable parameters
  pfe::ContactWireParams wireParams{.tag=cable_tag};
  std::cout << "Cable tag: " << cable_tag << std::endl;
  jfile >> wireParams;

  // cable suspension spring parameters
  pfe::SuspensionSpringParams spring_params{spring_tag};
  jfile >> spring_params;

  // pantograph parameters
  pfe::Pantograph_0_Params panto_0_params{.tag=panto_tag};
  jfile >> panto_0_params;
  jfile.close();
  
  // Create the cable system + springs
  auto cable = new pfe::MultiSpan_Cable(0., wireParams, spring_params, num_spans, nelm);
  
  // Time integrator
  const int nTotalDof = cable->GetTotalNumDof();
  pfe::TimeIntegrator *TI;
  if(time_integrator_type=="generalized alpha")
    TI = new pfe::GenAlpha(time_integrator_param, nTotalDof, timestep);
  else if(time_integrator_type=="hht alpha")
    TI = new pfe::HHTAlpha(time_integrator_param, nTotalDof, timestep);
  else if(time_integrator_type=="newmark")
    TI = new pfe::Newmark(nTotalDof, timestep);
  else
    assert(false && "Expected time integrator to be one of 'generalized alpha', 'hht alpha' or 'newmark'");

  // Create an instance of the model + moving load
  auto model = new pfe::Model_MultiSpanCable_MovingLoad(*cable, panto_0_params, *TI);
  Simulate(model, out_directory, num_time_steps);
  
  // Clean up
  delete model;
  delete cable;
  PetscFinalize();
}


// time stepping
template<class ModelType>
void Simulate(ModelType model, const std::string out_directory, const int num_time_steps)
{
  const int nTotalDof = model->GetTotalNumDof();
  
  // Static initialization. 
  model->StaticInitialize();

  // Visualize
  //model->Write({out_directory+"/cable-ref.dat"}, out_directory+"/load-ref.dat");
  model->Write({out_directory+"/cable-0.dat"}, out_directory+"/load-0.dat");
  
  // Moving pantograph
  std::cout << "Time step (of " << num_time_steps << ") " << std::endl;
  for(int tstep=0; tstep<num_time_steps; ++tstep)
    {
      std::cout << tstep+1 << std::endl;
      model->Advance();
      model->Write({out_directory+"/cable-"+std::to_string(tstep+1)+".dat"}, out_directory+"/load-"+std::to_string(tstep+1)+".dat");
    }

  // done
  return;
  
}


// simulation parameter
void read_simulation_parameters(const std::string sim_file, const std::string sim_id,
				std::string& params_file,
				std::string& cable_tag, std::string& panto_tag, std::string& spring_tag,
				int& num_spans, int& nelm,
				std::string& time_integrator_type, double& time_integrator_param,
				double& timestep, int& num_time_steps,
				std::string& out_directory)
{
  assert(std::filesystem::path(sim_file).extension()==".json");
  std::fstream jfile;
  jfile.open(sim_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());

  // get the data
  auto J = nlohmann::json::parse(jfile);
  jfile.close();

  // read options
  auto it = J.find(sim_id);
  assert(it!=J.end());
  auto& j = *it;
  assert(j.contains("parameters file")    && "Input file missing params file tag");
  assert(j.contains("num spans")          && "Input file missing num spans");
  assert(j.contains("cable tag")          && "Input file missing cable tag");
  assert(j.contains("load tag")           && "Input file missing load tag");
  assert(j.contains("spring tag")         && "Input file missing spring tag");
  assert(j.contains("num elements")       && "Input file missing num elements tag");
  assert(j.contains("time integrator")    && "Input file missing time integrator tag");
  assert(j.contains("time step")          && "Input file missing time step tag");
  assert(j.contains("num time steps")     && "Input file missing num time steps tag");
  assert(j.contains("output directory")   && "Input file missing output directory tag");

  j["parameters file"].get_to(params_file);
  j["num spans"].get_to(num_spans);
  j["cable tag"].get_to(cable_tag);
  j["load tag"].get_to(panto_tag);
  j["spring tag"].get_to(spring_tag);
  j["num elements"].get_to(nelm);
  j["time integrator"].get_to(time_integrator_type);
  j["time step"].get_to(timestep); 
  j["num time steps"].get_to(num_time_steps);
  j["output directory"].get_to(out_directory);

  // list of tags
  std::set<std::string> tags{"parameters file", "num spans", "cable tag", "load tag", "spring tag",
      "num elements", "time integrator", "time step", "num time steps", "output directory", "_comment"};

  // time integrator parameter
  if(time_integrator_type=="generalized alpha")
    {
      assert(j.contains("spectral radius") && "Input file missing spectral radius tag");
      j["spectral radius"].get_to(time_integrator_param);
      tags.insert("spectral radius");
    }
  else if(time_integrator_type=="hht alpha")
    {
      assert(j.contains("alpha") && "Input file missing alpha tag");
      j["alpha"].get_to(time_integrator_param);
      tags.insert("alpha");
    }

  // checks on inputs
  assert(params_file.empty()==false && std::filesystem::exists(params_file));
  assert(nelm>0 && timestep>0 && num_time_steps>0 && num_spans>=1);
  
  // create the output directory if it does not exist, erase .dat files it does
  if(std::filesystem::exists(out_directory)==true)
    {
      std::cout << "Output directory " << out_directory << " exists. Erasing dat files" << std::endl;
      auto it_dir = std::filesystem::directory_iterator(out_directory);
      for(auto& it:it_dir)
	if(it.path().extension()==".dat")
	  {
	    auto flag = std::filesystem::remove(it.path());
	    assert(flag==true && "Could not erase file");
	  }
    }
  else
    {
      std::cout << "Creating output directory \"" << out_directory << "\"" << std::endl;
      auto flag  = std::filesystem::create_directory(out_directory);
      assert(flag==true && "Could not create output directory");
    }

  // Check consistency of tags
  for(auto jt:j.items())
    if(tags.find(jt.key())==tags.end())
      {
	std::cout << "Unexpected tag " << jt.key() << " found in input file " << std::endl;
	assert(false);
      }

  // done
  return;
}
