
/** \file test_DynamicModel.cpp
 * \brief Unit test for the component pfe::HeavyTensionedBeam including dynamics
 * \author Ramsharan Rangarajan
 * Last modified: May 30, 2022
 */

#include <pfe_CatenarySupportedContactWire.h>
#include <pfe_NewmarkIntegrator.h>
#include <fstream>
#include <iostream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  PetscErrorCode ierr;

  // Benchmark study parameters
  std::fstream jfile;
  jfile.open((char*)"benchmark_params.json", std::ios::in);
  assert(jfile.is_open());
  
  // Catenary cable parameters
  pfe::CatenaryWireParams catenaryParams;
  catenaryParams.tag = "catenary cable";
  jfile >> catenaryParams;

  // Contact wire parameters
  pfe::ContactWireParams wireParams;
  wireParams.tag = "contact wire";
  jfile >> wireParams;

  // Dropper parameters
  pfe::IntercableDropperParams dropperParams;
  dropperParams.tag = "droppers";
  jfile >> dropperParams;

  // Dropper arrangement
  pfe::DropperArrangement dropperSchedule;
  dropperSchedule.tag = "dropper schedule";
  jfile >> dropperSchedule;

  // catenary suspension spring
  pfe::SuspensionSpringParams catenary_spring_params;
  catenary_spring_params.tag = "catenary cable suspension spring";
  jfile >> catenary_spring_params;
  
  // contact wire suspension spring
  pfe::SuspensionSpringParams contact_spring_params;
  contact_spring_params.tag = "contact wire suspension spring";
  jfile >> contact_spring_params;
    
  jfile.close();

  // Create an instance of the model
  const int nElements = 20;
  auto cpair = new pfe::CatenarySupportedContactWire(catenaryParams, wireParams,
						     dropperParams, dropperSchedule, nElements);
  const int nNodes = cpair->GetNumNodes();

  // Suspension springs at the ends of the catenary cable
  cpair->AddCatenarySuspensionSpring(catenary_spring_params, 0);
  cpair->AddCatenarySuspensionSpring(catenary_spring_params, nNodes-1);

  // Suspension springs at the ends of the contact wire
  cpair->AddContactWireSuspensionSpring(contact_spring_params, 0);
  cpair->AddContactWireSuspensionSpring(contact_spring_params, nNodes-1);

  // No dirichlet bcs
  std::vector<std::pair<int,double>> static_null_bcs{};
  
  // Solve for static equilibrium
  const int nTotalDofs = cpair->GetTotalNumDof();
  std::vector<double> displacements(nTotalDofs,0.);
  cpair->StaticSolve(static_null_bcs, static_null_bcs, displacements.data());

  // Plot contact wire configuration
  const auto& coordinates = cpair->GetCoordinates();
  const int nCatenaryCableDofs = cpair->GetNumCatenaryCableDofs();
  std::fstream stream;
  stream.open((char*)"cpair.dat", std::ios::out);
  for(int a=0; a<nNodes; ++a)
    stream << coordinates[a]<<" "
	   <<catenaryParams.zLevel+displacements[a]<<" "
	   <<wireParams.zLevel+displacements[nCatenaryCableDofs+a]<<std::endl;
  stream.close();

  // Petsc data structure 
  pfe::PetscData PD;
  auto nnz = cpair->GetSparsity();
  PD.Initialize(nnz);
  
  // Evaluate the mass and stiffness matrices
  cpair->Evaluate(displacements.data(), PD.massMAT, PD.stiffnessMAT, PD.resVEC);

  // Create time integrator
  const double dt = 0.01;
  auto ts = new pfe::NewmarkIntegrator(nTotalDofs, dt);

  // Dirichlet bcs: displace midpoint of the contact wire
  std::vector<int> dyn_dirichlet_dofs{};
  std::vector<double> dyn_dirichlet_values{};
  
  // Set operators
  ts->Set(PD.massMAT, PD.stiffnessMAT, dyn_dirichlet_dofs);

  // Initialize state
  std::vector<double> zero(nTotalDofs);
  std::fill(zero.begin(), zero.end(), 0.);
  ts->Initialize(displacements.data(), zero.data(), zero.data());
  
  // External force = static force + small dynamic oscillations
  Vec Frc_static;
  ierr = VecDuplicate(PD.resVEC, &Frc_static);  CHKERRQ(ierr);
  ierr = VecCopy(PD.resVEC, Frc_static);        CHKERRQ(ierr);
  Vec Frc_dyn;
  ierr = VecDuplicate(Frc_static, &Frc_dyn);    CHKERRQ(ierr);
  
  // Time stepping
  auto& dynConfig = ts->GetState();
  for(int tstep=0; tstep<400; ++tstep)
    {
      // Print the current state
      auto& U = dynConfig.GetDisplacement();
      double* uvec;
      ierr = VecGetArray(U, &uvec); CHKERRQ(ierr);
      stream.open(std::string("config-"+std::to_string(tstep)+".dat").c_str(), std::ios::out); assert(stream.good());
      for(int n=0; n<nNodes; ++n)
	stream << coordinates[n]<<" "<< catenaryParams.zLevel + uvec[n] <<" " << wireParams.zLevel+uvec[nCatenaryCableDofs+n]<<std::endl;
      stream.close();
      ierr = VecRestoreArray(U, &uvec); CHKERRQ(ierr);
      
      // Advance in time
      std::cout<<"\nTime step: "<<tstep;
      const double time = static_cast<double>(tstep+1)*dt;
      const int frc_dof = nCatenaryCableDofs+nNodes/2;
      const double frc_val = 50.*std::sin(M_PI*time);
      ierr = VecCopy(Frc_static, Frc_dyn);                             CHKERRQ(ierr);
      ierr = VecSetValues(Frc_dyn, 1, &frc_dof, &frc_val, ADD_VALUES); CHKERRQ(ierr);
      ierr = VecAssemblyBegin(Frc_dyn);                                CHKERRQ(ierr);
      ierr = VecAssemblyEnd(Frc_dyn);                                  CHKERRQ(ierr);
      ts->Advance(Frc_dyn, dyn_dirichlet_values);
    }
  
  // Clean up
  delete cpair;
  ts->Destroy();
  delete ts;
  PD.Destroy();
  ierr = VecDestroy(&Frc_static); CHKERRQ(ierr);
  ierr = VecDestroy(&Frc_dyn);    CHKERRQ(ierr);
  PetscFinalize();
}

