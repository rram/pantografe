
/** \file test_MovingLoadOnBeam.cpp
 * \brief Unit test for the component pfe::MovingPointLoad including dynamics
 * \author Ramsharan Rangarajan
 * Last modified: July 19, 2022
 */

#include <pfe_MovingPointLoad.h>
#include <pfe_NewmarkIntegrator.h>
#include <fstream>
#include <iostream>

int main(int argc, char **argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  PetscErrorCode ierr;

  // Create beam with selfweight/tension
  pfe::ContactWireParams params{.span=55., .EI=195., .Tension=22000., .rhoA=1.35, .g=9.81, .zLevel=1.};
  const int nElements = 100;
  auto cwire = new pfe::HeavyTensionedBeam(params, nElements);

  // displacement field
  const auto& L2GMap = cwire->GetLocalToGlobalMap();
  const int nTotalDofs = L2GMap.GetTotalNumDof();
  std::vector<double> displacements(nTotalDofs);

  // simple supports at the left and right end
  const int nNodes = nElements+1;
  std::vector<std::pair<int,double>> dirichlet_bcs{};
  dirichlet_bcs.push_back( std::make_pair(0, 0.) );
  dirichlet_bcs.push_back( std::make_pair(nNodes-1, 0.) );
  const std::vector<int> dirichlet_dofs{0, nNodes-1};
  const std::vector<double> dirichlet_values{0.,0.};
  
  // Solve for static equilibrium
  cwire->StaticSolve(dirichlet_bcs, displacements.data());

  // Plot the static solution
  const auto& coordinates = cwire->GetCoordinates();
  std::fstream stream;
  stream.open((char*)"static-sol.dat", std::ios::out);
  stream << "#coordinate \t displacment";
  for(int n=0; n<nElements+1; ++n)
    stream << std::endl << coordinates[n] <<" "<<displacements[n];
  stream.close();

  // Moving point load
  const double F0 = 100.; // N
  const double x0 = 1.;   // initial position
  const double c0 = 100.;   // speed
  pfe::MovingPointLoad delta_load(*cwire, F0, x0, c0);

  // Petsc data structure
  pfe::PetscData PD;
  auto nnz = cwire->GetSparsity();
  PD.Initialize(nnz);
  
  // Evaluate the mass and stiffness matrices
  cwire->Evaluate(displacements.data(), PD.massMAT, PD.stiffnessMAT, PD.resVEC);

  // Create time integrator
  const double dt = 0.0005;
  auto ts = new pfe::NewmarkIntegrator(nTotalDofs, dt);

  // Set operators
  ts->Set(PD.massMAT, PD.stiffnessMAT, dirichlet_dofs);

  // Initialize dynamic configuration
  std::vector<double> zero(nTotalDofs);
  std::fill(zero.begin(), zero.end(), 0.);
  std::vector<int> indx(nTotalDofs);
  for(int a=0; a<nTotalDofs; ++a)
    indx[a] = a;
  ts->Initialize(displacements.data(), zero.data(), zero.data()); // U, V, A

  // Access the state
  auto& dynConfig = ts->GetState();

  // External force = static pre-load + moving point load
  Vec Frc_static;
  ierr = VecDuplicate(PD.resVEC, &Frc_static); CHKERRQ(ierr);
  ierr = VecCopy(PD.resVEC, Frc_static);       CHKERRQ(ierr);
  Vec Frc_dyn;
  ierr = VecDuplicate(PD.resVEC, &Frc_dyn);    CHKERRQ(ierr);

  // Time stepping
  for(int tstep=0; tstep<1000; ++tstep)
    {
      // Advance in time
      std::cout<<"Time step: "<< tstep << std::endl;
      const double time = dt*static_cast<double>(tstep);

      // Loading = static + dynamic
      ierr = VecZeroEntries(Frc_dyn);      CHKERRQ(ierr);
      ierr = VecCopy(Frc_static, Frc_dyn); CHKERRQ(ierr);
      delta_load.Evaluate(time, Frc_dyn);

      // note the location and magnitude of the load
      double fval, xval;
      delta_load.GetForce(time, xval, fval);
      std::cout<<"Load location: "<<xval << "value: "<<fval << std::endl;

      // Advance in time
      ts->Advance(Frc_dyn, dirichlet_values);

      // print the current state
      dynConfig.PlotState("config-"+std::to_string(tstep)+".dat", L2GMap, coordinates);

      // plot the location of the moving load
      std::fstream pfile;
      pfile.open(std::string("load-"+std::to_string(tstep)+".dat"), std::ios::out);
      pfile << xval << " " << -0.25 << std::endl << xval << " " << 0.02 << std::endl;
      pfile.close();
    }

  // Clean up
  ts->Destroy();
  PD.Destroy();
  ierr = VecDestroy(&Frc_static); CHKERRQ(ierr);
  ierr = VecDestroy(&Frc_dyn);    CHKERRQ(ierr);
  delete cwire;
  delete ts;
  PetscFinalize();
}
  
