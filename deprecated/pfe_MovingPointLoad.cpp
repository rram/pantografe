
/** \file pfe_MovingPointLoad.cpp
 * \brief Implements the class pfe::MovingPointLoad
 * \author Ramsharan Rangarajan
 * Last modified: July 18, 2022
 */

#include <pfe_MovingPointLoad.h>

namespace pfe
{

  // Constructor
  MovingPointLoad::MovingPointLoad(const HeavyTensionedBeam& bm,
				   const double f0, 
				   const double x0,
				   const double c0)
    :beam(bm), F0(f0), X0(x0), C0(c0),
     L2GMap(beam.GetLocalToGlobalMap())
  {
    // populate the list of element coordinates
    elmCoords.clear();
    const auto& coordinates = beam.GetCoordinates();
    const auto& ElmArray = beam.GetElementArray();
    double xleft, xright;
    for(auto& elm:ElmArray)
      {
	const auto& conn = elm->GetElementGeometry().GetConnectivity();
	xleft  = coordinates[conn[0]-1];
	xright = coordinates[conn[1]-1];
	assert(static_cast<int>(conn.size())==2 && xleft<xright);
	elmCoords.push_back({xleft,xright});
      }

    // element shape functions
    ShpFuncs.clear();
    for(auto& coords:elmCoords)
      ShpFuncs.push_back(new HermiteShape(coords.first,coords.second));
  }

  // Destructor
  MovingPointLoad::~MovingPointLoad()
  {
    for(auto& shp:ShpFuncs)
      delete shp;
  }
  
  // Main functionality: evaluate the force at a prescribed time
  void MovingPointLoad::GetForce(const double time, double& x, double& F) const
  {
    x = X0+C0*time;
    F = F0;
    return;
  }

  // Main functionality: append contribution from the forcing to the load vector
  void MovingPointLoad::Evaluate(const double time, Vec& F)
  {
    // location at which the force acts
    const double x = X0 + C0*time;

    // which element does the force act in
    int eIndx = -1;
    const int nElements = static_cast<int>(elmCoords.size());
    for(int e=0; e<nElements; ++e)
      if(x>=elmCoords[e].first && x<=elmCoords[e].second)
	{
	  eIndx = e;
	  break;
	}
    assert(eIndx!=-1);
    const auto& xleft  = elmCoords[eIndx].first;
    const auto& xright = elmCoords[eIndx].second;
    assert(x>=xleft && x<=xright);
    
    // basis functions of element 'eIndx'
    const auto& Shp     = *ShpFuncs[eIndx];
    const double lambda = (xright-x)/(xright-xleft);
    
    // Update force vector
    int indx[2];
    double frc[2];
    for(int a=0; a<2; ++a)
      {
	indx[a] = L2GMap.Map(0,a,eIndx);
	frc[a]  = F0*Shp.Val(a, &lambda);
      }
    assert(std::abs(F0-frc[0]-frc[1])<1.e-4*std::abs(F0));
    PetscErrorCode ierr = VecSetValues(F, 2, indx, frc, ADD_VALUES); CHKERRV(ierr);
    ierr = VecAssemblyBegin(F); CHKERRV(ierr);
    ierr = VecAssemblyEnd(F);   CHKERRV(ierr);
    return;
  }
  
}
