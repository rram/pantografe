
/** \file pfe_Cable_Panto0.h
 * \brief Defines the class pfe::Cable_Panto0
 * \author Ramsharan Rangarajan
 * Last modified: August 29, 2022
 */

#ifndef PFE_CONSTANT_FORCE_CABLE_H
#define PFE_CONSTANT_FORCE_CABLE_H

#include <pfe_HeavyTensionedBeam.h>
#include <pfe_PantographParams.h>
#include <pfe_NewmarkIntegrator.h>

namespace pfe
{
  //!< Class defining a single span cable with one moving line load
  class Cable_Panto0
  {
  public:

    //! Constructor
    Cable_Panto0(const HeavyTensionedBeam& obj1,
		 const Pantograph_0_Params& obj2,
		 const double timestep);

    //! Destructor
    virtual ~Cable_Panto0();

    //! Disable copy and assignment
    Cable_Panto0(const Cable_Panto0&) = delete;
    Cable_Panto0 operator=(const Cable_Panto0&) = delete;

    //! Access the cable
    inline const HeavyTensionedBeam& GetCable() const
    { return cable; }

    //! Total number of dofs
    inline int GetTotalNumDof() const
    { return nTotalDof; }

    //! Main functionality: evaluate mass, stiffness and force vectors
    void Evaluate(const double time, Vec& U, Mat &M, Mat &K, Vec &F) const;

    //! Main functionality: static solve
    void StaticInitialize(const double time) const;

    //! Advance the state to the next time step
    void Advance();
    
    //! Access the state
    inline const DynamicState& GetState() const
    { return TI.GetState(); }
      
    //! Visualize
    void Write(const std::string cable_fname,
	       const std::string load_fname) const;

  private:
    const HeavyTensionedBeam  &cable;
    const Pantograph_0_Params panto_params;
    const int nTotalDof;

    mutable NewmarkIntegrator  TI;
    mutable PetscData          PD;
    mutable LinearSolver       linSolver;
  };
			 
}

#endif
