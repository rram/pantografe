// Sriramajayam

/** \file pfe_OHE_Panto1.h
 * \brief Defines the class pfe::OHE_Panto1
 * \author Ramsharan Rangarajan
 * Last modified: October 31, 2022
 */

#pragma once

#include <pfe_OHE_Panto0.h>

namespace pfe
{
  //! Class defining a single snap OHE with a one-level moving pantograph
  class OHE_Panto1: public OHE_Panto0
  {
  public:
    //! Constructor
    OHE_Panto1(const OHESpan& obj1,
	       const Pantograph_1_Params& obj2,
	       const double timestep);

    //! Destructor
    inline virtual ~OHE_Panto1() {}

    //! Disable copy and assignment
    OHE_Panto1(const OHE_Panto1&) = delete;
    OHE_Panto1 operator=(const OHE_Panto1&) = delete;

    //! Main functionality: evaluate mass, stiffness and force
    void Evaluate(const double time, const Vec &U, Mat &M, Mat &K, Vec &F) const override;
    
  private:
    const Pantograph_1_Params panto_1_params;
  };
  
}
