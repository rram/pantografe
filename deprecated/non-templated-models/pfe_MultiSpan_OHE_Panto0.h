
/** \file pfe_MultiSpanOHE_Panto0.h
 * \brief Defines the class pfe::MultiSpanOHE_Panto0
 * \author Ramsharan Rangarajan
 * Last modified: February 15, 2023
 */

#pragma once

#include <pfe_MultiSpan_OHE.h>
#include <pfe_PantographParams.h>
#include <pfe_NewmarkIntegrator.h>

namespace pfe
{
  //! Class defining a multi-span OHE with a moving line load
  class MultiSpan_OHE_Panto0
  {
  public:
    //! Constructor
    MultiSpan_OHE_Panto0(const MultiSpan_OHE& obj1,
			 const Pantograph_0_Params& obj2,
			 const double timestep);

    //! Destructor
    virtual ~MultiSpan_OHE_Panto0();

    //! Disable copy and assignment
    MultiSpan_OHE_Panto0(const MultiSpan_OHE_Panto0&) = delete;
    MultiSpan_OHE_Panto0 operator=(const MultiSpan_OHE_Panto0&) = delete;

    //! Access the ohe spans
    inline const MultiSpan_OHE& GetOHE() const
    { return ohe; }

    //! Total number of dofs
    inline int GetTotalNumDof() const
    { return nTotalDof; }

    //! Main functionality: evaluate mass, stiffness and force vectors
    virtual void Evaluate(const double time, const Vec &U, Mat &M, Mat &K, Vec &F) const;

    //! Main functionality: static solve
    void StaticInitialize(const double time) const;

    //! Advance the state to the next time step
    void Advance();

    //! Access the state
    inline const DynamicState& GetState() const
    { return TI.GetState(); }

    //! Visualize
    void Write(const std::string catenary_fname, const std::string contact_fname,
	       const std::string dropper_fname,  const std::string load_fname) const;

  protected:
    const MultiSpan_OHE       &ohe;
    const Pantograph_0_Params panto_0_params;
    const int                 nTotalDof;

    mutable NewmarkIntegrator TI;
    mutable PetscData         PD;
    mutable LinearSolver      linSolver;
  };
  
}
