
/** \file pfe_MultiSpan_Cable_Panto0.cpp
 * \brief Implements the class pfe::MultiSpan_Cable_Panto0
 * \author Ramsharan Rangarajan
 * Last modified: February 24, 2023
 */

#include <pfe_MultiSpan_Cable_Panto0.h>

namespace pfe
{
  // Constructor
  MultiSpan_Cable_Panto0::MultiSpan_Cable_Panto0(const MultiSpan_Cable& obj1,
						 const Pantograph_0_Params& obj2,
						 const double timestep)
    :cable(obj1),
     panto_0_params(obj2),
     nTotalDof(cable.GetTotalNumDof()),
     TI(nTotalDof, timestep)
  {
    // sparsity
    std::vector<int> nz = cable.GetSparsity();

    // Create PETSc data structures
    PD.Initialize(nz);

    // done
  }


  // Destructor
  MultiSpan_Cable_Panto0::~MultiSpan_Cable_Panto0()
  {
    PetscErrorCode ierr;
    PetscBool      isFinalized;
    ierr = PetscFinalized(&isFinalized); CHKERRV(ierr);
    assert(isFinalized==PETSC_FALSE);

    TI.Destroy();
    PD.Destroy();
    linSolver.Destroy();

    // done
  }


  // Main functionality: evaluate mass, stiffness and force vectors
  void MultiSpan_Cable_Panto0::Evaluate(const double time, const Vec& U, Mat& M, Mat& K, Vec& F) const
  {
    // evaluate contribution from cable
    cable.Evaluate(time, U, M, K, F);
  }

  
