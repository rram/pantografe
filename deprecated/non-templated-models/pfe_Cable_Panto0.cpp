
/** \file pfe_Cable_Panto0.h
 * \brief Implements the class pfe::Cable_Panto0
 * \author Ramsharan Rangarajan
 * Last modified: August 29, 2022
 */

#include <pfe_Cable_Panto0.h>

namespace pfe
{
  // Constructor
  Cable_Panto0::Cable_Panto0(const HeavyTensionedBeam& obj1,
					 const Pantograph_0_Params& obj2,
					 const double timestep)
    :cable(obj1),
     panto_params(obj2),
     nTotalDof(cable.GetTotalNumDof()),
     TI(nTotalDof, timestep)
  {
    // sparsity
    std::vector<int> nz = cable.GetSparsity();

    // create PETSc data structure
    PD.Initialize(nz);

    // done
  }
     

  // Destructor
  Cable_Panto0::~Cable_Panto0()
  {
    PetscErrorCode ierr;
    PetscBool isFinalized;
    ierr = PetscFinalized(&isFinalized); CHKERRV(ierr);
    assert(isFinalized==PETSC_FALSE);

    TI.Destroy();
    PD.Destroy();
    linSolver.Destroy();

    // done
  }

  
  // Main functionality: evaluate mass, stiffness and force vectors
  void Cable_Panto0::Evaluate(const double time, Vec& U, Mat &M, Mat &K, Vec &F) const
  {
    // Evaluate contribution from the cable
    PetscErrorCode ierr;
    double *uvals;
    ierr = VecGetArray(U, &uvals);     CHKERRV(ierr);
    cable.Evaluate(uvals, M, K, F);
    ierr = VecRestoreArray(U, &uvals); CHKERRV(ierr);

    // add contribution from the pantograph
    const double& F0   = panto_params.force;
    const double x     = panto_params.xinit + time*panto_params.speed;
    auto dof_shp_pairs = cable.GetActiveDisplacementDofs(x);
    assert(std::abs(dof_shp_pairs[0].second+dof_shp_pairs[1].second-1.)<1.e-4);
    const int indx[]     = {dof_shp_pairs[0].first,     dof_shp_pairs[1].first};
    const double fvals[] = {dof_shp_pairs[0].second*F0, dof_shp_pairs[1].second*F0};


    ierr = VecSetValues(F, 2, indx, fvals, ADD_VALUES); CHKERRV(ierr);
    ierr = VecAssemblyBegin(F);                         CHKERRV(ierr);
    ierr = VecAssemblyEnd(F);                           CHKERRV(ierr);

    // done
    return;
  }
  
  // Main functionality: static solve
  void Cable_Panto0::StaticInitialize(const double time) const
  {
    PetscErrorCode ierr;

    // Aliases
    auto& M = PD.massMAT;
    auto& K = PD.stiffnessMAT;
    auto& F = PD.resVEC;
    auto& U = PD.solutionVEC;
    
    // Zero
    ierr = MatZeroEntries(M); CHKERRV(ierr);
    ierr = MatZeroEntries(K); CHKERRV(ierr);
    ierr = VecZeroEntries(F); CHKERRV(ierr);
    ierr = VecZeroEntries(U); CHKERRV(ierr);
    
    // Evaluate
    Evaluate(time, U, M, K, F);

    // Preserve nonzero structure
    ierr = MatSetOption(K, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE); CHKERRV(ierr);

    // Set operators for the time integrator
    std::vector<int> dirichlet_dofs{};
    TI.SetOperators(M, K, dirichlet_dofs);
    
    // Solve
    linSolver.SetOperator(K);
    linSolver.Solve(F, U);
    
    // Set as initial conditions in time integrator
    std::vector<double> zero(nTotalDof);
    std::fill(zero.begin(), zero.end(), 0.);
    double *uvals;
    ierr = VecGetArray(U, &uvals);     CHKERRV(ierr);
    TI.Initialize(time, uvals, zero.data(), zero.data());
    ierr = VecRestoreArray(U, &uvals); CHKERRV(ierr);

    // done
    return;
  }

  
  // Advance the state to the next time step
  void Cable_Panto0::Advance()
  {
    // TODO: USE PETSC_NULL TO AVOID EVALUATION OF MASS AND STIFFNESS
    
    // Evaluate the force vector
    PetscErrorCode ierr;
    
    // Aliases
    auto& M = PD.massMAT;
    auto& K = PD.stiffnessMAT;
    auto& F = PD.resVEC;
    auto& U = PD.solutionVEC;
    
    // Zero
    ierr = MatZeroEntries(M); CHKERRV(ierr);
    ierr = MatZeroEntries(K); CHKERRV(ierr);
    ierr = VecZeroEntries(F); CHKERRV(ierr);
    ierr = VecZeroEntries(U); CHKERRV(ierr);
    
    // Evaluate
    const double time = TI.GetTime()+TI.GetTimestep();
    Evaluate(time, U, M, K, F);

    // No dirichlet bcs
    std::vector<double> dirichlet_values{};

    // Advance the solution
    TI.Advance(F, dirichlet_values);

    // done
    return;
  }

  
  // Visualize
  void Cable_Panto0::Write(const std::string cable_fname,
			   const std::string load_fname) const
  {
    // Plot the cable
    
    // Aliases
    const auto& state = TI.GetState();
    const Vec& U      = state.U;
    const Vec& V      = state.V;
    const Vec& A      = state.A;
    
    // Arrays
    PetscErrorCode ierr;
    double *uvals, *vvals, *avals;
    ierr = VecGetArray(U, &uvals); CHKERRV(ierr);
    ierr = VecGetArray(V, &vvals); CHKERRV(ierr);
    ierr = VecGetArray(A, &avals); CHKERRV(ierr);

    // write cable data to file
    cable.Write(uvals, vvals, avals, cable_fname);
    
    // Restore arrays
    ierr = VecRestoreArray(U, &uvals); CHKERRV(ierr);
    ierr = VecRestoreArray(V, &vvals); CHKERRV(ierr);
    ierr = VecRestoreArray(A, &avals); CHKERRV(ierr);

    // Plot the pantograph

    // Current time
    const double time = TI.GetTime();
    
    // position of the load
    const double x = panto_params.xinit + time*panto_params.speed;

    // contact wire displacement here
    double wval = 0.;
    auto dof_shp_pairs = cable.GetActiveDofs(x);
    for(auto& it:dof_shp_pairs)
      {
	auto& dof    = it.first;
	auto& shpval = it.second;
	double wnode;
	ierr = VecGetValues(U, 1, &dof, &wnode); CHKERRV(ierr);
	wval += wnode*shpval;
      }

    // vertical extent of the pantograph
    const double zlevel = cable.GetParameters().zLevel;
    const double z0 = panto_params.zbase;
    const double z1 = zlevel+wval;

    // write to file
    std::fstream pfile;
    pfile.open(load_fname.c_str(), std::ios::out);
    assert(pfile.good());
    pfile << x <<" "<<z0 << std::endl
	  << x <<" "<<z1 << std::endl;
    pfile.close();

    return;
  }
  
} // pfe::
