
/** \file pfe_MultiSpan_OHE_Panto1.cpp
 * \brief Implements the class pfe::MultiSpan_OHE_Panto1
 * \author Ramsharan Rangarajan
 * Last modified: February 17, 2022
 */

#include <pfe_MultiSpan_OHE_Panto1.h>

namespace pfe
{
  // Constructor
  MultiSpan_OHE_Panto1::MultiSpan_OHE_Panto1(const MultiSpan_OHE& obj1,
					     const Pantograph_1_Params& obj2,
					     const double timestep)
    :MultiSpan_OHE_Panto0(obj1,
			  Pantograph_0_Params({
			      .tag=obj2.tag,
				.xinit=obj2.xinit,
				.zbase=obj2.zbase,
				.speed=obj2.speed,
				.force=obj2.force}),
			  timestep),
     panto_1_params(obj2)
  {}

  // Destructor
  MultiSpan_OHE_Panto1::~MultiSpan_OHE_Panto1()
  {}

  // Main functionality: evaluate mass, stiffness and force vectors
  void MultiSpan_OHE_Panto1::Evaluate(const double time, const Vec &U, Mat &M, Mat &K, Vec &F) const
  {
    PetscErrorCode ierr;
    
    // Evaluate contribution from OHE + uplift force of the pantograph
    MultiSpan_OHE_Panto0::Evaluate(time, U, M, K, F);

    // current location of the pantograph
    const double x_panto = panto_1_params.xinit + panto_1_params.speed*time;

    // interacting dofs of the contact wire
    const auto active_ohe_pair = ohe.GetActiveContactWireDisplacementDofs(x_panto);
    const int& dof_a = active_ohe_pair[0].first;
    const int& dof_b = active_ohe_pair[1].first;
    const double& Na = active_ohe_pair[0].second;
    const double& Nb = active_ohe_pair[1].second;
    double Ua, Ub;
    ierr = VecGetValues(U, 1, &dof_a, &Ua); CHKERRV(ierr);
    ierr = VecGetValues(U, 1, &dof_b, &Ub); CHKERRV(ierr);

    // Add contributions from mass and spring of the panto
    double val = 0.;

    // M
    const double &m = panto_1_params.m;
    val  = m*Na*Na;
    ierr = MatSetValues(M, 1, &dof_a, 1, &dof_a, &val, ADD_VALUES); CHKERRV(ierr);
    val = m*Nb*Nb;
    ierr = MatSetValues(M, 1, &dof_b, 1, &dof_b, &val, ADD_VALUES); CHKERRV(ierr);
    val = m*Na*Nb;
    ierr = MatSetValues(M, 1, &dof_a, 1, &dof_b, &val, ADD_VALUES); CHKERRV(ierr);
    ierr = MatSetValues(M, 1, &dof_b, 1, &dof_a, &val, ADD_VALUES); CHKERRV(ierr);
    ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY);                 CHKERRV(ierr);
    ierr = MatAssemblyEnd(M, MAT_FINAL_ASSEMBLY);                   CHKERRV(ierr);

    // K
    const double &k = panto_1_params.k;
    val  = k*Na*Na;
    ierr = MatSetValues(K, 1, &dof_a, 1, &dof_a, &val, ADD_VALUES); CHKERRV(ierr);
    val = k*Nb*Nb;
    ierr = MatSetValues(K, 1, &dof_b, 1, &dof_b, &val, ADD_VALUES); CHKERRV(ierr);
    val = k*Na*Nb;
    ierr = MatSetValues(K, 1, &dof_a, 1, &dof_b, &val, ADD_VALUES); CHKERRV(ierr);
    ierr = MatSetValues(K, 1, &dof_b, 1, &dof_a, &val, ADD_VALUES); CHKERRV(ierr);
    ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
    ierr = MatAssemblyEnd(K, MAT_FINAL_ASSEMBLY);   CHKERRV(ierr);

    // done
    return;
  }
  
}
