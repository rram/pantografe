
/** \file pfe_MultiSpanOHE_Panto1.h
 * \brief Defines the class pfe::MultiSpanOHE_Panto1
 * \author Ramsharan Rangarajan
 * Last modified: February 17, 2023
 */

#pragma once

#include <pfe_MultiSpan_OHE_Panto0.h>

namespace pfe
{
  //! Class defining s multi-span OHE with a moving pantograph type 1
  class MultiSpan_OHE_Panto1: public MultiSpan_OHE_Panto0
  {
  public:
    //! Constructor
    MultiSpan_OHE_Panto1(const MultiSpan_OHE& obj1,
			 const Pantograph_1_Params& obj2,
			 const double timestep);

    //! Destructor
    virtual ~MultiSpan_OHE_Panto1();

    //! Disable copy and assignment
    MultiSpan_OHE_Panto1(const MultiSpan_OHE_Panto1&) = delete;
    MultiSpan_OHE_Panto1 operator=(const MultiSpan_OHE_Panto1&) = delete;

    //! Override evaluate method
    void Evaluate(const double time, const Vec &U, Mat &M, Mat &K, Vec &F) const override;

  private:
    const Pantograph_1_Params panto_1_params;
      
  };
     
}
