
/** \file pfe_OHE_Panto0.h
 * \brief Defines the class pfe::OHE_Panto0
 * \author Ramsharan Rangarajan
 * Last modified: August 29, 2022
 */

#pragma once

#include <pfe_OHESpan.h>
#include <pfe_PantographParams.h>
#include <pfe_NewmarkIntegrator.h>

namespace pfe
{
  //! Class defining a single span OHE with one moving line load
  class OHE_Panto0
  {
  public:
    //! Constructor
    OHE_Panto0(const OHESpan& obj1,
	       const Pantograph_0_Params& obj2,
	       const double timestep);

    //! Destructor
    virtual ~OHE_Panto0(); 

    //! Disable copy and assignment
    OHE_Panto0(const OHE_Panto0&) = delete;
    OHE_Panto0 operator=(const OHE_Panto0&) = delete;
    
    //! Access ohe span
    inline const OHESpan& GetOHESpan() const
    { return ohe; }

    //! Total number of dofs
    inline int GetTotalNumDof() const
    { return nTotalDof; }
      
    //! Main functionality: evaluate mass, stiffness and force vectors
    virtual void Evaluate(const double time, const Vec& U, Mat &M, Mat &K, Vec &F) const;

    //! Main functionality: static solve
    void StaticInitialize(const double time) const;

    //! Advance the state to the next time step
    void Advance();

    //! Access the state
    inline const DynamicState& GetState() const
    { return TI.GetState(); }
    
    //! Visualize
    void Write(const std::string catenary_fname, const std::string contact_fname,
	       const std::string dropper_fname,  const std::string load_fname) const;

  protected:
    const OHESpan             &ohe;
    const Pantograph_0_Params panto_0_params;
    const int nTotalDof;

    mutable NewmarkIntegrator TI;
    mutable PetscData         PD;
    mutable LinearSolver      linSolver;
  };

}
