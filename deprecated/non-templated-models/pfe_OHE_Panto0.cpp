
/** \file pfe_OHE_Panto0.cpp
 * \brief Implements the class pfe::OHE_Panto0
 * \author Ramsharan Rangarajan
 * Last modified: August 29, 2022
 */

#include <pfe_OHE_Panto0.h>

namespace pfe
{
  // Constructor
  OHE_Panto0::OHE_Panto0(const OHESpan& obj1, const Pantograph_0_Params& obj2, const double timestep)
    :ohe(obj1),
     panto_0_params(obj2),
     nTotalDof(ohe.GetTotalNumDof()),
     TI(nTotalDof, timestep)
  {
    // sparsity
    std::vector<int> nz   = ohe.GetSparsity();
    
    // Create PETSc data structure
    PD.Initialize(nz);

    // done
  }
     

  // Destructor
  OHE_Panto0::~OHE_Panto0()
  {
    PetscErrorCode ierr;
    PetscBool isFinalized;
    ierr = PetscFinalized(&isFinalized); CHKERRV(ierr);
    assert(isFinalized==PETSC_FALSE);

    TI.Destroy();
    PD.Destroy();
    linSolver.Destroy();
    
    // done
  }

  
  // Main functionality: evaluate mass, stiffness and force vectors
  void OHE_Panto0::Evaluate(const double time, const Vec& U, Mat &M, Mat &K, Vec &F) const
  {
    // Evaluate contribution from OHE
    ohe.Evaluate(time, U, M, K, F);

    // add contribution from the pantograph
    const double& F0   = panto_0_params.force;
    const double x     = panto_0_params.xinit + time*panto_0_params.speed;
    auto dof_shp_pairs = ohe.GetActiveContactWireDisplacementDofs(x);
    assert(std::abs(dof_shp_pairs[0].second+dof_shp_pairs[1].second-1.)<1.e-4);
    const int indx[]     = {dof_shp_pairs[0].first,     dof_shp_pairs[1].first};
    const double fvals[] = {dof_shp_pairs[0].second*F0, dof_shp_pairs[1].second*F0};

    PetscErrorCode ierr;
    ierr = VecSetValues(F, 2, indx, fvals, ADD_VALUES); CHKERRV(ierr);
    ierr = VecAssemblyBegin(F);                         CHKERRV(ierr);
    ierr = VecAssemblyEnd(F);                           CHKERRV(ierr);

    // done
    return;
  }


  // Main functionality: static solve
  void OHE_Panto0::StaticInitialize(const double time) const
  {
    PetscErrorCode ierr;

    // Aliases
    auto& M = PD.massMAT;
    auto& K = PD.stiffnessMAT;
    auto& F = PD.resVEC;
    auto& U = PD.solutionVEC;
    
    // Zero
    ierr = MatZeroEntries(M); CHKERRV(ierr);
    ierr = MatZeroEntries(K); CHKERRV(ierr);
    ierr = VecZeroEntries(F); CHKERRV(ierr);
    ierr = VecZeroEntries(U); CHKERRV(ierr);

    // Evaluate
    Evaluate(time, U, M, K, F);

    // Preserve nonzero structure
    ierr = MatSetOption(K, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE); CHKERRV(ierr);

    // Set operators for the time integrator
    std::vector<int> dirichlet_dofs{};
    TI.SetOperators(M, K, dirichlet_dofs);
    
    // Solve
    linSolver.SetOperator(K);
    linSolver.Solve(F, U);

    // Set as initial conditions in the time integrator
    std::vector<double> zero(nTotalDof);
    std::fill(zero.begin(), zero.end(), 0.);
    double *uvals;
    ierr = VecGetArray(U, &uvals);     CHKERRV(ierr);
    TI.Initialize(time, uvals, zero.data(), zero.data());
    ierr = VecRestoreArray(U, &uvals); CHKERRV(ierr);
    
    // done
    return;
  }

  
  // Advance the state to the next time step
  void OHE_Panto0::Advance()
  {
    // TODO: USE PETSC_NULL TO AVOID EVALUATION OF MASS AND STIFFNESS

    // Evaluate
    PetscErrorCode ierr;
    
    // Aliases
    auto& M = PD.massMAT;
    auto& K = PD.stiffnessMAT;
    auto& F = PD.resVEC;
    auto& U = PD.solutionVEC;
    
    // Zero
    ierr = MatZeroEntries(M); CHKERRV(ierr);
    ierr = MatZeroEntries(K); CHKERRV(ierr);
    ierr = VecZeroEntries(F); CHKERRV(ierr);
    ierr = VecZeroEntries(U); CHKERRV(ierr);
    
    // Evaluate
    const double time = TI.GetTime()+TI.GetTimestep();
    Evaluate(time, U, M, K, F);

    // No dirichlet bcs
    std::vector<double> dirichlet_values{};

    // Advance the solution
    TI.Advance(F, dirichlet_values);

    // done
    return;
  }
  
  
  // Visualize
  void OHE_Panto0::Write(const std::string catenary_fname, const std::string contact_fname,
			 const std::string dropper_fname,  const std::string load_fname) const
  {
    // Aliases
    const auto& state = TI.GetState();
    const Vec& U = state.U;
    const Vec& V = state.V;
    const Vec& A = state.A;

    // Arrays
    PetscErrorCode ierr;
    double *disp, *vel, *accn;
    ierr = VecGetArray(U, &disp);          CHKERRV(ierr);
    ierr = VecGetArray(V, &vel);           CHKERRV(ierr);
    ierr = VecGetArray(A, &accn);          CHKERRV(ierr);
    ohe.Write(disp, vel, accn, catenary_fname, contact_fname, dropper_fname);
    ierr = VecRestoreArray(U, &disp);      CHKERRV(ierr);
    ierr = VecRestoreArray(V, &vel);       CHKERRV(ierr);
    ierr = VecRestoreArray(A, &accn);      CHKERRV(ierr);

    // position of the load
    const double time = TI.GetTime();
    const double x    = panto_0_params.xinit + time*panto_0_params.speed;

    // contact wire displacement here
    double wval = 0.;
    auto dof_shp_pairs = ohe.GetActiveContactWireDofs(x);
    for(auto& it:dof_shp_pairs)
      {
	auto& dof    = it.first;
	auto& shpval = it.second;
	double wnode;
	ierr = VecGetValues(U, 1, &dof, &wnode); CHKERRV(ierr);
	wval += wnode*shpval;
      }

    // vertical extent for the pantograph
    const double z0 = panto_0_params.zbase;
    const double z1 = ohe.ContactWire().GetParameters().zLevel + wval;

    // write to file
    std::fstream pfile;
    pfile.open(load_fname.c_str(), std::ios::out);
    assert(pfile.good());
    pfile << x << " " << z0 << std::endl
	  << x << " " << z1 << std::endl;
    pfile.close();
    
    return;
  }

}
