/** \file pfe_MultiSpan_Cable_Panto0.h
 * \brief Defines the class pfe::MultiSpan_Cable_Panto0
 * \author Ramsharan Rangarajan
 * Last modified: Feb 24, 2023
 */


#pragma once

#include <pfe_MultiSpan_Cable.h>
#include <pfe_PantographParams.h>
#include <pfe_NewmarkIntegrator.h>

namespace pfe
{
  //! Class defining a multi span cable with one moving line load
  class MultiSpan_Cable_Panto0
  {
  public:
    //! Constructor
    MultiSpan_Cable_Panto0(const MultiSpan_Cable& obj1,
			   const Pantograph_0_Params& obj2,
			   const double timestep);

    //! Destructor
    virtual ~MultiSpan_Cable_Panto0();

    //! Disable copy and assignment
    MultiSpan_Cable_Panto0(const MultiSpan_Cable_Panto0&) = delete;
    MultiSpan_Cable_Panto0 operator=(const MultiSpan_Cable_Panto0&) = delete;

    //! Access the cable spans
    inline const MultiSpan_Cable& GetCables() const
    { return cable; }

    //! Total number of dofs
    inline int GetTotalNumDof() const
    { return nTotalDof; }

    //! Main functionality: evaluate mass, stiffness and force
    virtual void Evaluate(const double time, const Vec& U, Mat& M, Mat& K, Vec& F) const;

    //! Main functionality: static solve
    void StaticInitialize(const double time) const;

    //! Advance the state to the next time step
    void Advance();

    //! Access the state
    inline const DynamicState& GetState() const
    { return TI.GetState(); }

    //! Visualize
    void Write(const std::string cable_fname, const std::string load_fname) const;

  protected:
    const MultiSpan_Cable     &cable;
    const Pantograph_0_Params panto_0_params;
    const int                 nTotalDof;

    mutable NewmakrIntegrator TI;
    mutable PetscData         PD;
    mutable LinearSolver      linSolver;
  };
  
}
