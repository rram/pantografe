
/** \file test_P11DElement.cpp
 * \brief Unit test for class pfe::P11DElement
 * \author Ramsharan Rangarajan
 * Last modified: September 23, 2021
 */


#include <pfe_P11DElement.h>
#include <cassert>
#include <random>
#include <iostream>

using namespace pfe;

template<int NFields>
void Test(const P11DElement<NFields>&);

int main()
{
  std::random_device rd; 
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-1.,1.);
  for(int trial=0; trial<100; ++trial)
    {
      // Coordinates
      std::vector<double> gcoords{dis(gen), 2.+dis(gen)};
      SegmentGeometry<1>::SetGlobalCoordinatesArray(gcoords);
      
      // 1 field test
      P11DElement<1> elm_1(1, 2);
      Test(elm_1);

      // 2 field test
      P11DElement<2> elm_2(1, 2);
      Test(elm_2);
      
      // 3 field test
      P11DElement<3> elm_3(1, 2);
      Test(elm_3);
    }
}

template<int NFields>
void Test(const P11DElement<NFields>& elm)
{
  const int nDof = 2; // 2 dofs per element
  assert(elm.GetNumFields()==NFields);
  for(int f=0; f<NFields; ++f)
    { assert(elm.GetNumDof(f)==nDof);
      assert(elm.GetNumDerivatives(f)==1); }
  auto& fields = elm.GetFields();
  assert(static_cast<int>(fields.size())==NFields);
  for(int f=0; f<NFields; ++f)
    assert(fields[f]==f);

  // verify array sizes
  const int nQuad = static_cast<int>(elm.GetIntegrationWeights(fields[0]).size());
  for(int f=0; f<NFields; ++f)
    {
      assert(static_cast<int>(elm.GetIntegrationPointCoordinates(f).size())==nQuad);
      assert(static_cast<int>(elm.GetShape(f).size())==nQuad*nDof);
      assert(static_cast<int>(elm.GetDShape(f).size())==nQuad*nDof);
    }

  // verify partition of unity for shape functions and derivatives
  for(int f=0; f<NFields; ++f)
    for(int q=0; q<nQuad; ++q)
      {
	double sum = 0.;
	double dsum = 0.;
	for(int a=0; a<nDof; ++a)
	  { sum += elm.GetShape(f,q,a);
	    dsum += elm.GetDShape(f,q,a,0); }
	assert(std::abs(sum-1.)<1.e-6);
	assert(std::abs(dsum)<1.e-6);
      }

  // geometry
  auto& geom = elm.GetElementGeometry();
  assert(static_cast<int>(geom.GetConnectivity().size())==2);
  assert(geom.GetConnectivity()[0]==1);
  assert(geom.GetConnectivity()[1]==2);
  double xleft, xright;
  double lambda = 0.;
  geom.Map(&lambda, &xright);
  lambda = 1.;
  geom.Map(&lambda, &xleft);
  const double len = xright-xleft;
  assert(len>0.);

  // quadrature weights should sum to the length
  for(int f=0; f<NFields; ++f)
    {
      double qsum = 0.;
      const auto& qwts = elm.GetIntegrationWeights(f);
      assert(static_cast<int>(qwts.size())==nQuad);
      for(int q=0; q<nQuad; ++q)
	qsum += qwts[q];
      assert(std::abs(qsum-len)<1.e-6);
    }

  // quadrature points should lie within the element
  for(int f=0; f<NFields; ++f)
    {
      const auto& qpts = elm.GetIntegrationPointCoordinates(f);
      assert(static_cast<int>(qpts.size())==nQuad);
      for(int q=0; q<nQuad; ++q)
	{ assert(qpts[q]>=xleft && qpts[q]<=xright); }
    }

  // done
  return;
}
