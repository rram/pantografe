
/** \file pfe_HeavyTensionedCable.h
 * \brief Defines the class pfe::HeavyTensionedCable
 * \author Ramsharan Rangarajan
 * Last modified: October 19, 2021
 */

#ifndef PFE_HEAVY_TENSIONED_CABLE_H
#define PFE_HEAVY_TENSIONED_CABLE_H

#include <pfe_TensionForce.h>
#include <pfe_GravityForce.h>
#include <pfe_P11DElement.h>
#include <pfe_Assembler.h>
#include <pfe_PetscData.h>

namespace pfe
{
  struct HeavyTensionedCableParams
  {
    double span;
    double Tension;
    double rhoAg;
    double zLevel;
  };

  using CatenaryCableParams = HeavyTensionedCableParams;
  
  //! Class for a heavy pre-tensioned cable
  class HeavyTensionedCable
  {
  public:
    //! Constructor
    HeavyTensionedCable(const HeavyTensionedCableParams& cable_params, const int nelm);

    //! Destructor
    virtual ~HeavyTensionedCable();

    //! Disable copy and assignment
    HeavyTensionedCable(const HeavyTensionedCable&) = delete;
    HeavyTensionedCable& operator=(const HeavyTensionedCable&) = delete;

    //! Assemble the stiffness and force vector
    virtual void Evaluate(const double* dofValues, Mat& K, Vec& F);

    //! Main functionality
    void Solve(double* sol);

    //! Access local to global map
    inline const LocalToGlobalMap& GetLocalToGlobalMap() const
    { return *L2GMap; }

    //! Access coordinates
    inline const std::vector<double>& GetCoordinates() const
    { return coordinates; }

    //! Access the number of nonzeros
    inline void GetSparsity(std::vector<int>& nz) const
    {  nz.clear();
      Asm_T->CountNonzeros(nz); }

    //! Access parameters
    inline const HeavyTensionedCableParams& GetParameters() const
    { return params; }
      
  protected:
    const HeavyTensionedCableParams params;
    const int nElements;
    const int nNodes;
    std::vector<double> coordinates;
    std::vector<int> connectivity;
    std::vector<Element*> ElmArray;
    LocalToGlobalMap* L2GMap;
    std::vector<TensionForce*> TOpsArray;
    std::vector<GravityForce*> gOpsArray;
    Assembler<TensionForce>* Asm_T;
    Assembler<GravityForce>* Asm_g;
    PetscData PD;
  };
}

#endif

