
/** \file pfe_Pantograph_1.cpp
 * \brief Implements the class pfe::Pantograph_1
 * \author Ramsharan Rangarajan
 * Last modified: August 02, 2022
 */

#include <pfe_Pantograph.h>

namespace pfe
{
  // Strain energy functiona;
  double Pantograph_1::ComputeStrainEnergy(const double time, const double* dofValues) const
  {
    // 1/2 k w^2, w = deflection of the spring
    const double& wval = dofValues[0];
    return 0.5*params.k*wval*wval;
  }

  
  // Assemble contributions to the mass, stiffness and force vector
  void Pantograph_1::Evaluate(const double time, const Vec& U, Mat& M, Mat& K, Vec& F) const
  {
    PetscErrorCode ierr;
    
    // deflection of the spring
    double wval;
    const int dof = 0;
    ierr = VecGetValues(U, 1, &dof, &wval); CHKERRV(ierr);
    
    // (i) add inertia of the mass
    // (ii) add stiffness of the spring
    // (iii) add force of the spring
    double val;
    
    // mass
    val = params.m;
    ierr = MatSetValuesLocal(M, 1, &dof, 1, &dof, &val, ADD_VALUES); CHKERRV(ierr);
    ierr = MatAssemblyBegin(M, MAT_FINAL_ASSEMBLY);                  CHKERRV(ierr);
    ierr = MatAssemblyEnd(M, MAT_FINAL_ASSEMBLY);                    CHKERRV(ierr);
    

    // stiffness
    val = params.k;
    ierr = MatSetValuesLocal(K, 1, &dof, 1, &dof, &val, ADD_VALUES); CHKERRV(ierr);
    ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY);                  CHKERRV(ierr);
    ierr = MatAssemblyEnd(K, MAT_FINAL_ASSEMBLY);                    CHKERRV(ierr);

    // force
    val = params.k*wval;
    ierr = VecSetValues(F, 1, &dof, &val, ADD_VALUES); CHKERRV(ierr);
    ierr = VecAssemblyBegin(F);                        CHKERRV(ierr);
    ierr = VecAssemblyEnd(F);                          CHKERRV(ierr);

    return;
  }
  
}
