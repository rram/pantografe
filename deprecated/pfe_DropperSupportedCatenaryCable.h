
/** \file pfe_DropperSupportedCatenaryCable.h
 * \brief Defines the class pfe::DropperSupportedCatenaryCable
 * \author Ramsharan Rangarajan
 * Last modified: October 19, 2021
 */

#ifndef PFE_DROPPER_SUPPORTED_CATENARY_CABLE_H
#define PFE_DROPPER_SUPPORTED_CATENARY_CABLE_H

#include <pfe_HeavyTensionedCable.h>
#include <pfe_OverhangDropper.h>

namespace pfe
{
  //! Class for a catenary cable supported by a set of droppers
  class DropperSupportedCatenaryCable: public HeavyTensionedCable
  {
  public:
    //! Constructor
    DropperSupportedCatenaryCable(const HeavyTensionedCableParams& cable_params, const int nelm,
				  const std::vector<OverhangDropperParams>& dparams);
    
    //! Destructor
    virtual ~DropperSupportedCatenaryCable();
    
    //! Disable copy and assignment
    DropperSupportedCatenaryCable(const DropperSupportedCatenaryCable&) = delete;
    DropperSupportedCatenaryCable& operator=(const DropperSupportedCatenaryCable&) = delete;

    //! Assemble the stiffness and force vector
    void Evaluate(const double* dofValues, Mat& K, Vec& F) override;

  protected:
    const int nDroppers; 
    std::vector<OverhangDropper*> droppers;
  };
}

#endif
