
/** \file pfe_Model_Static_OHE.h
 * \brief Defines the class pfe::Model_Static_OHE to simulate static behavior of a single span of the OHE
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <pfe_SingleSpan_OHE.h>

namespace pfe
{
  //!< Class to simulate static behavior of an single span OHE
  //! \ingroup models
  class Model_Static_OHE
  {
  public:
    //! Constructor
    Model_Static_OHE(SingleSpan_OHE& ohe);

    //! Destructor
    virtual ~Model_Static_OHE();

    //! Disable copy and assignment
    Model_Static_OHE(const Model_Static_OHE&) = delete;
    Model_Static_OHE operator=(const Model_Static_OHE&) = delete;

    //! Access the span
    inline const SingleSpan_OHE& GetSpan() const
    { return ohe_span; }
    
    //! number of dofs
    inline int GetTotalNumDof() const
    { return ohe_span.GetTotalNumDof(); }
    
    //! Main functionality: static solve
    void Solve(double* displacements);

    //! Main functionality: adjust dripper lengths to match sag
    void AdjustDropperLengths(const int nIters);

    //! Compute transverse stiffness disrtribution
    void ComputeTransverseStiffnessDistribution(const double* displacements, const double Force,
						double* stiffness) const;

    //! Visualize the configuration
    inline void Write(const double* displacements,
		      const std::string catenary_fname,
		      const std::string contact_fname,
		      const std::string dropper_fname) const
    { ohe_span.Write(displacements, catenary_fname, contact_fname, dropper_fname); }
    
  private:
    SingleSpan_OHE& ohe_span;              //!< single span
    mutable PetscData PD;           //!< Monolithic sparse data structures
    mutable LinearSolver linSolver; //!< Helper for linear solves
  };
  
}
