
/** \file pfe_Model_Static_OHE.cpp
 * \brief Implementation of the class pfe::Model_Static_OHE
 * \author Ramsharan Rangarajan
 */

#include <pfe_Model_Static_OHE.h>

namespace pfe
{
  // Constructor
  Model_Static_OHE::Model_Static_OHE(SingleSpan_OHE& ohe)
    :ohe_span(ohe)
  {
    PetscErrorCode ierr;
    PetscBool isInitialized;
    ierr = PetscInitialized(&isInitialized); CHKERRV(ierr);
    assert(isInitialized==PETSC_TRUE);
    
    // sparsity
    auto nz = ohe_span.GetSparsity();

    // Initialize PETSc data structures with known sparsity 
    PD.Initialize(nz);
  }

  // Destructor
  Model_Static_OHE::~Model_Static_OHE()
  {
    // Check that PETSc has not been finalized
    PetscBool isFinalized;
    auto ierr = PetscFinalized(&isFinalized); CHKERRV(ierr);
    assert(isFinalized==PETSC_FALSE);
    PD.Destroy();
    linSolver.Destroy();

    // done
  }


  // Main functionality: solve
  void Model_Static_OHE::Solve(double* displacements)
  {
    // Zero
    PetscErrorCode ierr;
    ierr = MatZeroEntries(PD.stiffnessMAT); CHKERRV(ierr);
    ierr = VecZeroEntries(PD.resVEC);       CHKERRV(ierr);
    ierr = VecZeroEntries(PD.solutionVEC);  CHKERRV(ierr);

    // Assemble contributions to (K, F) from the catenary cable, contact wire,
    // dropper cables and suspension springs
    ohe_span.Evaluate(PD.massMAT, PD.stiffnessMAT, PD.resVEC);

    // Solve
    linSolver.SetOperator(PD.stiffnessMAT);
    linSolver.Solve(PD.resVEC, PD.solutionVEC);

    // Get the solution
    double* solution;
    ierr = VecGetArray(PD.solutionVEC, &solution); CHKERRV(ierr);
    const int nTotalDof = ohe_span.GetTotalNumDof();
    for(int a=0; a<nTotalDof; ++a)
      displacements[a] = solution[a];
    ierr = VecRestoreArray(PD.solutionVEC, &solution); CHKERRV(ierr);

    // done
    return;
  }


  // Main functionalty: match a prescribed value of sag
  void Model_Static_OHE::AdjustDropperLengths(const int nIters)
  {
    const auto& droppers = ohe_span.GetDroppers();
    const auto& schedule = ohe_span.GetDropperArrangement();
    const int nDroppers = static_cast<int>(droppers.size());
    const auto& target_sag = schedule.target_sag;


    const int nTotalDof = ohe_span.GetTotalNumDof();
    std::vector<double> displacements(nTotalDof);
    std::vector<double> dropper_contact_disp(nDroppers), dropper_catenary_disp(nDroppers);
    std::vector<double> lengths(nDroppers);
    
    // Iterate over dropper lengths
    for(int iter=0; iter<=nIters; ++iter)
      {    
	// solve in the current state
	Solve(displacements.data());

	// Get sag values at the contact wire along dropper connections
	ohe_span.GetDropperDisplacements(&displacements[0],
					 &dropper_catenary_disp[0],
					 &dropper_contact_disp[0]);

	// Adjust dropper lengths towards achieving the target
	for(int d=0; d<nDroppers; ++d)
	  {
	    lengths[d] = droppers[d]->GetLength();
	    lengths[d] -= (target_sag[d]-dropper_contact_disp[d]);
	    assert(lengths[d]>0.);
	  }

	// Set the new dropper lengths
	ohe_span.ResetDropperLengths(lengths.data());
      }

    // done
    return;
  }


  // Compute transverse stiffness disrtribution
  void Model_Static_OHE::
  ComputeTransverseStiffnessDistribution(const double* displacements,
					 const double Force,
					 double* stiffness) const
  {
    PetscErrorCode ierr;
    
    // Apply the given force at each node of the contact wire along the span
    // Measure the displacement and compute the stiffness
    const auto& contact_wire = ohe_span.ContactWire();
    const int nNodes = contact_wire.GetNumNodes();
    const int nTotalDof = ohe_span.GetTotalNumDof();
    
    // Loop over the loading node
    for(int lnode=0; lnode<nNodes; ++lnode)
      {
	ierr = MatZeroEntries(PD.stiffnessMAT); CHKERRV(ierr);
	ierr = VecZeroEntries(PD.resVEC);       CHKERRV(ierr);
	ierr = VecZeroEntries(PD.solutionVEC);  CHKERRV(ierr);
	
	// Assemble
	ohe_span.Evaluate(PD.massMAT, PD.stiffnessMAT, PD.resVEC);

	// Add transverse force at "lnode" on the contact wire
	const int dof = ohe_span.GetActiveContactWireDof(lnode);
	ierr = VecSetValues(PD.resVEC, 1, &dof, &Force, ADD_VALUES); CHKERRV(ierr);
	ierr = VecAssemblyBegin(PD.resVEC);                          CHKERRV(ierr);
	ierr = VecAssemblyEnd(PD.resVEC);                            CHKERRV(ierr);
	
	// Solve
	linSolver.SetOperator(PD.stiffnessMAT);
	linSolver.Solve(PD.resVEC, PD.solutionVEC);

	// Get the solution
	double* solution;
	ierr = VecGetArray(PD.solutionVEC, &solution);     CHKERRV(ierr);

	// relative displacement
	double delta_disp = solution[dof]-displacements[dof];

	// restore
	ierr = VecRestoreArray(PD.solutionVEC, &solution); CHKERRV(ierr);


	// Compute the stiffness
	assert(std::abs(delta_disp)>1.e-9);
	stiffness[lnode] = Force/delta_disp;
      }
    
    // done
    return;
  }

}
