
/** \file test_StaticBenchmark_DropperLengthAndStiffness.cpp
 * \brief Unit test for class pfe::Model_Static_OHE
 * \author Ramsharan Rangarajan
 */


#include <pfe_Model_Static_OHE.h>
#include <filesystem>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);

  // Benchmark study parameters
  pfe::OHESpan_ParameterPack pack;
  std::fstream jfile;
  assert(std::filesystem::exists("benchmark_params.json"));
  jfile.open("benchmark_params.json", std::ios::in);
  assert(jfile.is_open());
  jfile >> pack;
  jfile.close();
  
  // Create a single span
  const int nElements = 20;
  auto ohe_span = new pfe::SingleSpan_OHE(0., pack, nElements);

  // Create an instance of the static model
  auto model = new pfe::Model_Static_OHE(*ohe_span);

  // Adjust dropper lengths to achieve sag over 20 iterations
  model->AdjustDropperLengths(20);

  // Solve at the adjusted dropper lengths
  const int nTotalDof = model->GetTotalNumDof();
  std::vector<double> displacements(nTotalDof);
  model->Solve(displacements.data());

  // Access the achieved sag at the dropper nodes
  std::vector<double> dropper_catenary_disp(pack.dropperSchedule.nDroppers);
  std::vector<double> dropper_contact_disp(pack.dropperSchedule.nDroppers);
  ohe_span->GetDropperDisplacements(displacements.data(),
				    dropper_catenary_disp.data(), dropper_contact_disp.data());

  // Compare target and achieved displacements
  std::fstream pfile;
  pfile.open((char*)"dropper-sag.dat", std::ios::out);
  assert(pfile.good());
  pfile <<"# dropper-index x-dropper, target sag, realized sag, nominal length, adjusted length, catenary sag";
  for(int d=0; d<pack.dropperSchedule.nDroppers; ++d)
    pfile<<"\n"<<d<<" "<<pack.dropperSchedule.coordinates[d]<<" "
	 <<pack.dropperSchedule.target_sag[d]<<" "<<dropper_contact_disp[d]<<" "
	 <<pack.dropperSchedule.nominal_lengths[d]<<" "<<ohe_span->GetDroppers()[d]->GetLength()<<" "
	 <<dropper_catenary_disp[d];
  pfile.close();


  // Compute the transverse stiffness
  const auto& coordinates = ohe_span->ContactWire().GetCoordinates();
  const int nNodes = ohe_span->ContactWire().GetNumNodes();
  std::vector<double> stiffness(nNodes);
  model->ComputeTransverseStiffnessDistribution(displacements.data(), 100., stiffness.data());
  pfile.open((char*)"stiffness.dat", std::ios::out);
  for(int a=0; a<nNodes; ++a)
    pfile << coordinates[a]<<" "<<stiffness[a]<<"\n";
  pfile.close();
  
  // Clean up
  delete ohe_span;
  delete model;
  PetscFinalize();
}
