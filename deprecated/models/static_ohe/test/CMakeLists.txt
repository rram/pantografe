
# Project
project(model-static-ohe-unit-tests)

# copy benchmark data
file(GLOB BENCHMARK_FILES "${CMAKE_SOURCE_DIR}/data/static-benchmark/*.data")
file(COPY ${BENCHMARK_FILES} DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
file(COPY "${CMAKE_SOURCE_DIR}/data/static-benchmark/benchmark_params.json" DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

# Identical for all targets
foreach(TARGET
    test_StaticBenchmark_Deflection
    test_StaticBenchmark_ContactWireDeflection
    test_StaticBenchmark_DropperLengthAndStiffness)
  
  # Add this target
  add_executable(${TARGET}  ${TARGET}.cpp)
  
  # Link
  target_link_libraries(${TARGET} PUBLIC pfe_model_static_ohe)

  # choose appropriate compiler flags
  target_compile_features(${TARGET} PUBLIC ${pfe_COMPILE_FEATURES})
  
  add_test(NAME ${TARGET} COMMAND ${TARGET})

endforeach()
