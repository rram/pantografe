
# add library
add_library(pfe_model_static_ohe STATIC
  		 pfe_Model_Static_OHE.cpp)

# headers
target_include_directories(pfe_model_static_ohe PUBLIC
  				      $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)

# link
target_link_libraries(pfe_model_static_ohe PUBLIC pfe_component pfe_core)

# Add required flags
target_compile_features(pfe_model_static_ohe PUBLIC ${pfe_COMPILE_FEATURES})

install(FILES
	pfe_Model_Static_OHE.h
	DESTINATION ${PROJECT_NAME}/include)

