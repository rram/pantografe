
/** \file test_HeavyTensionedBeam.cpp
 * \brief Unit test for the model pfe::HeavyTensionedBeam
 * \author Ramsharan Rangarajan
 * Last modified: October 18, 2021
 */

#include <pfe_HeavyTensionedCable.h>
#include <fstream>

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a Catenary cable model
  pfe::CatenaryCableParams cable_params{.span=55., .Tension=11000., .rhoAg=13.25, .zLevel=1.};
  const int nElements = 100;
  auto cat_cable = new pfe::HeavyTensionedCable(cable_params, nElements);

  // displacement field
  const int nTotalDofs = cat_cable->GetLocalToGlobalMap().GetTotalNumDof();
  std::vector<double> displacements(nTotalDofs);

  // Solve
  cat_cable->Solve(displacements.data());

    // Print solution to file
  auto coordinates = cat_cable->GetCoordinates();
  std::fstream pfile;
  pfile.open((char*)"sol.dat", std::ios::out);
  for(int n=0; n<nElements+1; ++n)
    pfile<<coordinates[n]<<" "<<displacements[n]<<"\n";
  pfile.close();

  // Clean up
  delete cat_cable;

  // Finalize PETSc
  PetscFinalize();
}
