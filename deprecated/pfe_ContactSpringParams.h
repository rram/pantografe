
/** \file pfe_ContactSpringParams.h
 * \brief Defines the struct pfe::ContactSpringParams
 * \author Ramsharan Rangarajan
 */

#pragma once

#include <fstream>

namespace pfe
{
  //! \ingroup components
  struct ContactSpringParams
  {
    std::string tag;
    double stiffness;
    
    friend std::ostream& operator << (std::ostream &out, const ContactSpringParams& params);

    friend std::istream& operator >> (std::istream &in, ContactSpringParams& params);
  };
    
}
