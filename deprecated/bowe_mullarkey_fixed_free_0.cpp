
/** \file bowe_mullarkey_fixed_free_0.cpp
 * \brief Example 5.1 from Bowe & Mullarkey
 * wheel: no intertia, only gravity effect
 * beam:  inertia included, no gravity effect
 * beam clamped at the left end, wheel starts from the left end
 * monitor the deflection of the free end
 * \author Ramsharan Rangarajan
 */

/* Input:
   -i input file in json format with simulation options
   -s simulation id
*/

/* Output: 
   beam*.dat     beam configuration indexed by time step, columns: x w w-dot w-ddot 
   load*.dat     position of the load, indexed by time step
   history.dat   time history of the deflection of the free end of the beam
*/

#include <pfe_OHE_Panto0.h>
#include <CLI/CLI.hpp>
#include <nlohmann/json.hpp>
#include <iostream>
#include <fstream>

// simulation parameter
void read_simulation_parameters(const std::string json_file, const std::string tag,
				std::string& beam_file, std::string& panto_file, std::string& out_directory,
				int& pantograph_type, int& num_spans, int& nelm,
				double& timestep, int& num_time_steps);

// time stepping
template<class ModelType>
void Simulate(ModelType model, const std::string out_directory, const int num_time_steps);


int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL); CHKERRQ(ierr);

  // read command line options
  std::string sim_file;          // simulation options
  std::string sim_id;            // simulation id
  CLI::App app;
  app.add_option("-i", sim_file, "json file with simulation parameters")->required()->check(CLI::ExistingFile);
  app.add_option("-s", sim_id, "simulation id")->required();
  CLI11_PARSE(app, argc, argv);

  // read simulation parameters
  std::string beam_file;      // json file with beam data
  std::string panto_file;      // json file with panto data
  std::string out_directory;   // output directory
  int         pantograph_type; // type of pantograph
  int         num_spans;       // number of spans to consider
  int         nelm;            // number of elements for one beam span
  double      timestep;        // time step
  int         num_time_steps;  // number of time steps
  read_simulation_parameters(sim_file, sim_id,
			     beam_file, panto_file, out_directory,
			     pantograph_type, num_spans, nelm, timestep, num_time_steps);
  assert(num_spans==1 && "Expected 1 span");

  // beam parameters
  pfe::ContactWireParams wireParams{.tag="beam"};
  std::fstream jfile;
  jfile.open(beam_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());
  jfile >> wireParams;
  jfile.close();

  // Create the beam system 
  auto beam = new pfe::HeavyTensionedBeam(wireParams, nelm);
  
  // pantograph parameters
  pfe::Pantograph_0_Params panto_0_params{.tag="wheel-0"};
  jfile.open(panto_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());
  assert(pantograph_type==0 && "Expected pantograph type to be 0");
  jfile >> panto_0_params;
  jfile.close();

  // Create an instance of the model depending on the pantograph type
  {
    // clamp the left end
    const int nNodes = beam->GetNumNodes();
    const std::vector<int> dirichlet_dofs{0, nNodes};
    auto model = new pfe::Cable_Panto0(*beam, panto_0_params, timestep, dirichlet_dofs);
    Simulate(model, out_directory, num_time_steps);
    delete model;
  }
  
  // Clean up
  delete beam;
  PetscFinalize();
}


// time stepping
template<class ModelType>
void Simulate(ModelType model, const std::string out_directory, const int num_time_steps)
{
  const int nTotalDof = model->GetTotalNumDof();

  // clamp the left end
  const std::vector<double> dirichlet_values{0.,0.};
  
  // Static initialization.
  model->StaticInitialize(dirichlet_values);
    
  // Visualize
  //model->Write({out_directory+"/beam-ref.dat"}, out_directory+"/load-ref.dat");
  model->Write({out_directory+"/beam-0.dat"}, out_directory+"/load-0.dat");

  // History of the free end
  std::fstream hfile;
  hfile.open(out_directory+"/history.dat", std::ios::out);
  assert(hfile.good());
  const int indx = model->GetOHE().GetNumNodes()-1;
  
  // Moving pantograph
  std::cout << "Time step (of " << num_time_steps << ") " << std::endl;
  for(int tstep=0; tstep<num_time_steps; ++tstep)
    {
      std::cout << tstep+1 << std::endl;
      model->Advance(dirichlet_values);
      model->Write({out_directory+"/beam-"+std::to_string(tstep+1)+".dat"}, out_directory+"/load-"+std::to_string(tstep+1)+".dat");
      auto& U = model->GetState().U;
      double uval;
      VecGetValues(U, 1, &indx, &uval);
      hfile << model->GetTime() << "\t" << uval << std::endl;
    }
  hfile.close();
  
  // done
  return;
  
}


// simulation parameter
void read_simulation_parameters(const std::string json_file, const std::string tag,
				std::string& beam_file, std::string& panto_file, std::string& out_directory,
				int& pantograph_type, int& num_spans, int& nelm,
				double& timestep, int& num_time_steps)
{
  assert(std::filesystem::path(json_file).extension()==".json");
  std::fstream jfile;
  jfile.open(json_file, std::ios::in);
  assert(jfile.good() && jfile.is_open());

  // get the data
  auto J = nlohmann::json::parse(jfile);
  jfile.close();

  // read options
  auto it = J.find(tag);
  assert(it!=J.end());
  auto& j = *it;
  assert(j.contains("cable file")       && "Input file missing \"cable file\" tag");
  assert(j.contains("panto file")       && "Input file missing \"panto file\" tag");
  assert(j.contains("output directory") && "Input file missing \"output directory\" tag");
  assert(j.contains("pantograph type")  && "Input file missing \"pantograph type\" tag");
  assert(j.contains("num spans")        && "Input file missing \"num spans type\" tag");
  assert(j.contains("num elements")     && "Input file missing \"num elements\" tag");
  assert(j.contains("time step")        && "Input file missing \"time step\" tag");
  assert(j.contains("num time steps")   && "Input file missing \"num time steps\" tag");
  
  j["cable file"].get_to(beam_file);
  j["panto file"].get_to(panto_file);
  j["output directory"].get_to(out_directory);
  j["pantograph type"].get_to(pantograph_type);
  j["num spans"].get_to(num_spans);
  j["num elements"].get_to(nelm); 
  j["time step"].get_to(timestep); 
  j["num time steps"].get_to(num_time_steps);

  assert(beam_file.empty()==false && std::filesystem::exists(beam_file));
  assert(panto_file.empty()==false && std::filesystem::exists(panto_file));
  assert(num_spans>0 && nelm>0 && timestep>0 && num_time_steps>0);
  assert(pantograph_type==0 || pantograph_type==1);

  // create the output directory if it does not exist, erase .dat files it does
  if(std::filesystem::exists(out_directory)==true)
    {
      std::cout << "Output directory " << out_directory << " exists. Erasing dat files" << std::endl;
      auto it_dir = std::filesystem::directory_iterator(out_directory);
      for(auto& it:it_dir)
	if(it.path().extension()==".dat")
	  {
	    auto flag = std::filesystem::remove(it.path());
	    assert(flag==true && "Could not erase file");
	  }
    }
  else
    {
      std::cout << "Creating output directory \"" << out_directory << "\"" << std::endl;
      auto flag  = std::filesystem::create_directory(out_directory);
      assert(flag==true && "Could not create output directory");
    }

  // should not have any other tags
  std::set<std::string> tags{"cable file", "panto file", "output directory", "pantograph type", "num spans", "num elements", "time step", "num time steps", "_comment"};
  for(auto jt:j.items())
    if(tags.find(jt.key())==tags.end())
      {
	std::cout << "Unexpected tag " << jt.key() << " found in input file " << std::endl;
	assert(false);
      }

  // done
  return;
}
