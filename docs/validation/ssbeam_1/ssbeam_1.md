\section ssbeam Example CB1

Description
===========
A simply supported beam pre-stressed with a tensile load at the ends and allowed to deform under self-weight.

Setup
==========

![Problem setup](validation/ssbeam_1/diagram.png)


Simulation inputs and parameters
=======================

Problem parameters
------------------

Parameter Name  | Value | Unit | Description
------------- | ------------- | ------ | --
"span"  | 60 | m | Length of the beam
"line density"  | 1.35 | kg/m | mass per unit length 
"EI" | 136 | Pa m<SUP>4</SUP> | Flexural rigidity
"gravity" | 9.81 | m/s<SUP>2</SUP> | acceleration due to gravity
"tension" | 20000 | N |  Axial pre-tension

Simulation parameters
--------------------

Parameter Name  | Value | Unit
------------- | ------------- | ------
"num elements"  | 500 | N/A

Abaqus simulation
=====
[Abaqus input file](https://www.dropbox.com/scl/fi/bmxnr613bknyh10apsw6c/beam_selfwt_in_tension.inp?rlkey=32e7gegc8wb0grq513mlw103d&dl=0)

COMSOL simulation
=====
[COMSOL application file](https://www.dropbox.com/scl/fi/3uedq4qvt8kn9yk3rybhe/beam_selfwt_in_tension.mph?rlkey=t8xdt3om5xx0wk6oz2z9e9bnq&dl=0)

Simulation comparisons
=================


![Vertical displacement of the beam](validation/ssbeam_1/ssbeam_selfwt_tension.png)
