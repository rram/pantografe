\section ohemovl Example LOP1

Description
===========

Single span OHE with a point load F=100 N moving at a speed of 50 m/s.

Setup
==========

![Problem setup](validation/OHE_moving_load/OHE_moving_load_diagram.png)


Simulation inputs and parameters
=======================

Problem parameters
------------------

All problem parameters and inputs are same as \ref ohestat with the following additions:
Parameter Name  | Value | Unit
------------- | ------------- | ------
Force (F)  | 100 | N
Velocity (c)  | 50 | m/s

Simulation parameters
--------------------

Parameter Name  | Value | Unit
------------- | ------------- | ------
Number of elements in CW & MW  | 400 | N/A

Abaqus simulation
=====



Simulation comparisons
=================

The static initialization is done by applying the uplift force at a small distance from the left end (x=0.1125 m). The deformed profile under this static initialization step is compared before comparing the time histories.

![Initial vertical displacement of the contact wire at t=0](validation/OHE_moving_load/OHE_moving_load_initialdef.png)

Comparison with ABAQUS
----------------------

Comparison with COMSOL
----------------------

Simulation Parameter Name  | Value | Unit
------------- | ------------- | ------
Time-step  | 0.002 | s
Time integrator  | generalized-&alpha; | N/A
Spectral radius | 0.8 | N/A

In COMSOL the point load is approximated as a Gaussian distribution with &sigma;=L/10<SUP>4</SUP>sqrt(&pi;). The accleration plot for COMSOL is smoother because the loading is applied as a smooth analytic function COMSOL.

![Vertical displacement at the mid-span of the CW](validation/OHE_moving_load/OHE_moving_load_comsol_u.png)
![Vertical displacement at the mid-span of the CW](validation/OHE_moving_load/OHE_moving_load_comsol_v.png)
![Vertical displacement at the mid-span of the CW](validation/OHE_moving_load/OHE_moving_load_comsol_a.png)
