\section NL_3span_3dofi Example NOP5

Description
===========

Three span OHE with three degrees of freedom pantograph (comprising masses m1=6 kg, m2=9 kg, m3=7.5 kg, and, spring stiffness k1=160 N/m, k2=15500 N/m, k3=7000 N/m) loaded with F=50 N moving at a speed of 88.89 m/s.

Setup
==========

![Problem setup (n=3)](nspan_diagram.png)


Simulation inputs and parameters
=======================

Problem parameters
------------------

All problem parameters and inputs are same as \ref nonlinear_static with the following additions:
Parameter Name  | Value | Unit
------------- | ------------- | ------
Force (F)  | 50 | N
Velocity (c)  | 88.89 | m/s
mass (m1) | 6 | kg
spring stiffness (k1) | 160 | N/m
mass (m2) | 9 | kg
spring stiffness (k2) | 15500 | N/m
mass (m3) | 7.5 | kg
spring stiffness (k3) | 7000 | N/m


Simulation parameters
--------------------

Parameter Name  | Value | Unit
------------- | ------------- | ------
number of spans | 3 | N/A
Number of elements between droppers in CW & MW  | 10 | N/A
Time-step  | 0.0015 | s
Time integrator  | generalized-&alpha; | N/A
spectral radius | 0.1 | N/A

Abaqus simulation
=====

COMSOL simulation
=====

Simulation comparisons
=================

The static initialization is done by applying the uplift force at a distance from the left end (x=6 m).

![Contact point displacement of pantograph](int_3span_3dof_u2.jpg)
![Contact point velocity](int_3span_3dof_v2.jpg)
![Contact point acceleration](int_3span_3dof_a2.jpg)
![Contact force](int_3span_3dof_fc.jpg)
