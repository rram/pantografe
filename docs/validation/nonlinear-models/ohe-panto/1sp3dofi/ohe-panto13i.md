\section NL_1span_3dofi Example NOP3

Description
===========

Single span OHE with moving three degrees of freedom pantograph loaded with F=50 N moving at a speed of 88.89 m/s.

Setup
==========

![Problem setup](validation/OHE_moving_panto/OHE_moving_panto_diagram.png)


Simulation inputs and parameters
=======================

Problem & Simulation parameters
------------------

All problem parameters, simulation parameters and inputs are same as \ref NL_1span_3dof with the following difference:
Parameter Name  | Value | Unit
------------- | ------------- | ------
Force (F)  | 50 | N

Abaqus simulation
=====

COMSOL simulation
=====

Simulation comparisons
=================

The static initialization is done by applying the uplift force at a distance from the left end (x=6 m).

![Contact point displacement of pantograph](int_1span_3dof_u2.jpg)
![Contact point velocity](int_1span_3dof_v2.jpg)
![Contact point acceleration](int_1span_3dof_a2.jpg)
![Contact force](int_1span_3dof_fc.jpg)
