\section NL_1span_1dof Example NOP1

Description
===========

Single span OHE with a single degree of freedom (sdof) pantograph with mass m=7.5 kg, spring stiffness k=150 N/m, load F=169 N moving at a speed of 88.89 m/s.

Setup
==========

![Problem setup](validation/OHE_moving_sdof/OHE_moving_sdof_diagram.png)


Simulation inputs and parameters
=======================

Problem parameters
------------------

All problem parameters and inputs are same as \ref nonlinear_static with the following additions:
Parameter Name  | Value | Unit
------------- | ------------- | ------
Force (F)  | 169 | N
Velocity (c)  | 88.89 | m/s
mass (m) | 7.5 | kg
spring stiffness (k) | 150 | N/m

Simulation parameters
--------------------

Parameter Name  | Value | Unit
------------- | ------------- | ------
Number of elements between droppers in CW & MW  | 10 | N/A
Time-step  | 0.0015 | s
Time integrator  | generalized-&alpha; | N/A
spectral radius | 0.1 | N/A

Abaqus simulation
=====

COMSOL simulation
=====

Simulation comparisons
=================

The static initialization is done by applying the uplift force at a distance from the left end (x=6 m).

![Contact point displacement of pantograph](pen_1span_1dof_u2.jpg)
![Contact point velocity](pen_1span_1dof_v2.jpg)
![Contact point acceleration](pen_1span_1dof_a2.jpg)
![Contact force](pen_1span_1dof_fc.jpg)
