\section moving_load_1nl Example NML1

Description
===========

Single span OHE with a point load F=169 N moving at a speed of 88.89 m/s (320 kmph).

Setup
==========

![Problem setup](validation/OHE_moving_load/OHE_moving_load_diagram.png)


Simulation inputs and parameters
=======================

Problem parameters
------------------

All problem parameters and inputs are same as \ref nonlinear_static with the following additions:
Parameter Name  | Value | Unit
------------- | ------------- | ------
Force (F)  | 169 | N
Velocity (c)  | 88.89 | m/s
\n
Simulation Parameter Name  | Value | Unit
------------- | ------------- | ------
number of elements between droppers | 40 | N/A
Time-step  | 0.001 | s
Time integrator  | generalized-&alpha; | N/A
Spectral radius | 0.1 | N/A

Simulation comparisons
=================

The static initialization is done by applying the uplift force at a small distance from the left end (x=6 m). The deformed profile under this static initialization step is compared before comparing the time histories.

![Initial vertical displacement of the contact wire at t=0](1span_load_uplift.jpg)

Comparison with COMSOL
----------------------

In COMSOL the point load is approximated as a Gaussian distribution with &sigma;=L/10<SUP>3</SUP>sqrt(2). The differences with COMSOL are due to the loading is applied as a smooth analytic function COMSOL.

![Vertical displacement at the contact point](1span_load_u2.jpg)
![Vertical velocity at the contact point](1span_load_v2.jpg)
![Vertical acceleration at the contact point](1span_load_a2.jpg)
