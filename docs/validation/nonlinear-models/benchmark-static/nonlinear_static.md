\section nonlinear_static Example NBS1

Description
===========

Single span OHE with all components assembled and allowed to deform under self-weight.

Setup
==========

![Problem setup](validation/OHE_static/OHE_static_diagram.png)


Simulation inputs and parameters
=======================

Problem parameters
------------------

Parameter Name  | Value | Unit | Description
------------- | ------------- | ------ | ---
"span" | 55 | m | Length of span
"height" | 1.2 | m | Initial height of messenger wire
"contact wire suspension spring" | 2672.0 | N/m | CW suspension spring (k<SUB>1</SUB>)
"contact wire suspension mass" | 0.94 | kg | Registration Arm mass
"catenary cable suspension spring" | 500000.0 | N/m | MW suspension spring (k<SUB>2</SUB>)
"gravity" | 9.81 | m/s<SUP>2</SUP> | acceleration due to gravity
\n
Contact Wire (CW) parameter  | Value | Unit | Description
------------- | ------------- | ------ | ----
"line density"  | 1.35 | kg/m | Mass per unit length
"EI" | 195.0 | Pa m<SUP>4</SUP> | Flexural rigidity
"tension" | 22000 | N | Axial pre-tension
\n
Messenger Wire (MW) parameter  | Value | Unit | Description
------------- | ------------- | ------ | ---
"line density"  | 1.08 | kg/m |  Mass per unit length
"EI" | 131.7 | Pa m<SUP>4</SUP> | Flexural rigidity
"tension" | 16000 | N | Axial pre-tension
\n
Dropper Wire (DW) parameter  | Value | Unit | Description
------------- | ------------- | ------ | ----
"line density" | 0.117 | kg/m |  Mass per unit length
"EA" | 20000 | Pa m<SUP>2</SUP> | Axial stiffness
"catenary cable clamp mass" | 0.165 | kg | CW clamp mass
"contact wire clamp mass" | 0.195 | kg | MW clamp mass

Dropper number  | D1 | D2 | D3 | D4 | D5 | D6 | D7 | D8 | D9
--------------- | -- | -- | -- | -- | -- | -- | -- | -- | --- 
"dropper positions" (in m) | 4.5 | 10.25 | 16.0 | 21.75 | 27.5 | 33.25 | 39.0 | 44.75 | 50.5
"nominal lengths" (in m) | 1.017 | 0.896 | 0.810 | 0.758 | 0.741 | 0.758 | 0.810 | 0.896 | 1.017
\n
Nonlinear Parameters | value
------ |  -------
dropper slack factor | 1.0e5

Simulation parameters
--------------------

Parameter Name  | Value
------------- | -------------
"number of elements between successive droppers"  | 40
"force value at which to evaluate stiffness at the contact wire" | 100, 200 N

Simulation comparisons
=================
In PantograFE, the droppers are modelled as springs with initial deflection. In ABAQUS, the droppers are modelled using truss elements with prescribed initial stress. In COMSOL, the droppers are modelled using truss elements with prescribed initial strain. Linear analysis is done in COMSOL.


![Vertical displacement of the contact wire](benchmark_static.jpg)

Next, 100 N and 200 N uplift forces are applied on the nodes of the contact wire. The stiffness distribution of the OHE is calculated by taking the ratio of the force applied to the change in displacement at the point of application of the load.

![Stiffness distribution for F=100 N](benchmark_NL_stiff_100.jpg)
![Stiffness distribution for F=200 N](benchmark_NL_stiff_200.jpg)
