\section ohemovpt Example LOP4

Description
===========

Single span OHE with three degrees of freedom pantograph (comprising masses m1=6 kg, m2=9 kg, m3=7.5 kg, and, spring stiffness k1=160 N/m, k2=15500 N/m, k3=7000 N/m) loaded with F=100 N moving at a speed of 50 m/s.

Setup
==========

![Problem setup](validation/OHE_moving_panto/OHE_moving_panto_diagram.png)


Simulation inputs and parameters
=======================

Problem parameters
------------------

All problem parameters and inputs are same as \ref ohestat with the following additions:
Parameter Name  | Value | Unit
------------- | ------------- | ------
Force (F)  | 100 | N
Velocity (c)  | 50 | m/s
mass (m1) | 6 | kg
spring stiffness (k1) | 160 | N/m
mass (m2) | 9 | kg
spring stiffness (k2) | 15500 | N/m
mass (m3) | 7.5 | kg
spring stiffness (k3) | 7000 | N/m


Simulation parameters
--------------------

Parameter Name  | Value | Unit
------------- | ------------- | ------
Number of elements in CW & MW  | 400 | N/A

Abaqus simulation
=====

Simulation comparisons
=================

The static initialization is done by applying the uplift force at a small distance from the left end (x=0.3 m).

Comparison with ABAQUS
----------------------

Simulation Parameter Name  | Value | Unit
------------- | ------------- | ------
Time-step  | 0.002 | s
Time integrator  | hht-&alpha; | N/A
alpha | -0.1 | N/A

![Contact Force](validation/OHE_moving_panto/OHE_moving_3dof_contactforce.png)
