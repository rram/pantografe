\section ssbmovms Example CB2

Description
===========

Simply supported beam with a point mass m and load F moving at speed c.

Setup
==========

![Problem setup](validation/ssbeam_moving_mass/ssbeam_movms_diag.png)

Simulation inputs and parameters
=======================

Problem parameters
------------------

Parameter Name  | Value | Unit | Description
------------- | ------------- | ------ | ---
"span"   | 60.0 | m |  Length of the beam
"line density"  | 1.35 | kg/m | mass per unit length
"EI" | 136.0 | Pa m<SUP>4</SUP> | Flexural rigidity
"Tension" | 20000 | N | Axial pre-stress
"force" | 50.0 | N | Load (F)
"speed" | 60.0 | m/s | Velocity (c)
"m" | 3.0 | kg | Mass


Simulation parameters
--------------------

Parameter Name  | Value 
---|---
	"boundary condition"| "fixed-fixed",
	"num elements"      | 500,
	"time integrator"   | "hht alpha",
	"alpha"             | -0.1,
	"time step"         | 0.002,
	"num time steps"    | 500,

