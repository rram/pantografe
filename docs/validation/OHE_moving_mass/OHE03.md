\section ohemovms Example LOP2

Description
===========

Single span OHE with a point mass m=7.5 kg, load F=100 N moving at a speed of 50 m/s.

Setup
==========

![Problem setup](validation/OHE_moving_mass/OHE_moving_mass_diagram.png)


Simulation inputs and parameters
=======================

Problem parameters
------------------

All problem parameters and inputs are same as \ref ohestat with the following additions:
Parameter Name  | Value | Unit
------------- | ------------- | ------
Force (F)  | 100 | N
Velocity (c)  | 50 | m/s
mass (m) | 7.5 | kg

Simulation parameters
--------------------

Parameter Name  | Value | Unit
------------- | ------------- | ------
Number of elements in CW & MW  | 400 | N/A

Abaqus simulation
=====

Simulation comparisons
=================

The static initialization is done by applying the uplift force at a small distance from the left end (x=0.3 m).

Comparison with ABAQUS
----------------------

Simulation Parameter Name  | Value | Unit
------------- | ------------- | ------
Time-step  | 0.002 | s
Time integrator  | hht-&alpha; | N/A
alpha | -0.1 | N/A

![Contact Force](validation/OHE_moving_mass/OHE_moving_mass_contactforce.png)
