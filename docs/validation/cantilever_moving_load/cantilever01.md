\section cantload Example BM1

Description
===========

Cantilever beam with a point load moving at speed c.

Setup
==========

![Problem setup](validation/cantilever_moving_load/cantilever_load_diagram.png)

Simulation inputs and parameters
=======================

Problem parameters
------------------


Parameter Name  | Value | Unit | Description
------------- | ------------- | ------ | ---
"span"  | 7.62 | m | Length of the beam
"line density"  | 46 | kg/m | mass per unit length
"EI" | 9.4806E+06 | Pa m<SUP>4</SUP> | Flexural rigidity
"force" | 25790.49 | N | Load (F)
"speed" | 50.8 | m/s | Velocity (c)

Simulation comparisons
=================


Simulation parameters for comparison with ABAQUS
--------------------

Parameter Name  | Value 
---|---
	      "panto inertia"     | "none"
	      "panto type"        | 0
	      "boundary condition"| "fixed-free"
	      "time integrator"   | "hht alpha"
	      "alpha"             | -0.1
	      "num spans"         | 1
	      "num elements"      | 500
	      "time step"         | 0.0003
	      "num time steps"    | 500
	      
[Abaqus input file](https://www.dropbox.com/scl/fi/99d775v2z4ozek4yesys1/cantilever_moving_load.inp?rlkey=vi4s5henv1jeav37drvj8861k&dl=0)

![Vertical displacement at the free end of the beam](validation/cantilever_moving_load/cantilever01_abaqus_u.png)
![Vertical velocity at the free end of the beam](validation/cantilever_moving_load/cantilever01_abaqus_v.png)
![Vertical acceleration at the free end of the beam](validation/cantilever_moving_load/cantilever01_abaqus_a.png)


Simulation parameters for comparison with COMSOL
--------------------

Parameter Name  | Value 
---|---
	      "panto inertia"     | "none"
	      "panto type"        | 0
	      "boundary condition"| "fixed-free"
	      "time integrator"   | "generalized alpha"
	      "spectral radius"   | 1.0
	      "num spans"         | 1
	      "num elements"      | 500
	      "time step"         | 0.0005
	      "num time steps"    | 300

In COMSOL linear analysis is done with fixed time-stepping by applying the load as the following function:\n
(-F/x<SUB>g</SUB>)*exp(-(x-c*t)<SUP>2</SUP>/x<SUB>g</SUB><SUP>2</SUP>)/sqrt(&pi;),\n
where, x<SUB>g</SUB> is taken as length/200

[COMSOL application file](https://www.dropbox.com/scl/fi/fnmc3mmlma7vi4z7tejkh/cantilever_moving_load.mph?rlkey=zbqsvjapwpoaf0fvkwlflxokl&dl=0)


![Vertical displacement at the free end of the beam](validation/cantilever_moving_load/cantilever01_comsol_u.png)
![Vertical velocity at the free end of the beam](validation/cantilever_moving_load/cantilever01_comsol_v.png)
![Vertical acceleration at the free end of the beam](validation/cantilever_moving_load/cantilever01_comsol_a.png)
