\section cantmass Example BM2

Description
===========

Cantilever beam with a point mass and load moving at speed c.

Setup
==========

![Problem setup](validation/cantilever_moving_mass/cantilever_mass_diagram.png)

Simulation inputs and parameters
=======================

Problem parameters
------------------


Parameter Name  | Value | Unit | Description
------------- | ------------- | ------ | ---
"span"   | 7.62 | m |  Length of the beam
"line density"  | 46.0 | kg/m | mass per unit length
"EI" | 9.4806E+06 | Pa m<SUP>4</SUP> | Flexural rigidity
"force" | 25790.49 | N | Load (F)
"speed" | 25790.49 | m/s | Velocity (c)
"m" | 2629.0 | kg | Mass

Simulation parameters for comparison with ABAQUS
--------------------

Parameter Name  | Value 
---|---
            "panto type"        | 1
	      "panto inertia"     | "full"
	      "boundary condition"| "fixed-free"
	      "time integrator"   | "hht alpha"
	      "alpha"             | 0.0
	      "num spans"         | 1
	      "num elements"      | 500
	      "time step"         | 0.0002
	      "num time steps"    | 750
	      

![Vertical displacement at the free end of the beam](validation/cantilever_moving_mass/cantilever02_abaqus1.png)
![Contact force between the mass and the beam](validation/cantilever_moving_mass/cantilever02_abaqus2.png)

Simulation parameters for comparison with COMSOL
--------------------
Following parameters were used in pantograFE:
Parameter Name  | Value 
---|---
            "panto type"        | 1
	      "panto inertia"     | "full"
	      "boundary condition"| "fixed-free"
	      "time integrator"   | "generalized alpha"
	      "spectral radius"   | 0.0
	      "num spans"         | 1
	      "num elements"      | 500
	      "time step"         | 0.0002
	      "num time steps"    | 750
	      
[COMSOL application file](https://www.dropbox.com/scl/fi/cen5wawzyfpb5gikkv9l6/cantilever_moving_mass.mph?rlkey=wzimwlvmkerq9c3ya6wgt0qz9&dl=0)
	      
For solving the same example in COMSOL, the inertia force due to the mass is calculated from the solution at time t=n and applied on the beam at time t=n+1. The sum of the applied force and the inertia force, i.e. the contact force is modelled as "Edge Load" on the beam as a gaussian distribution. Periodic "Explicit Events" are used to compute the inertia load at each time-step. BDF time-integrator of order 2 is used in comsol with time step of 0.00002 s.

![Vertical displacement at the free end of the beam](validation/cantilever_moving_mass/cantilever02_comsol1.png)
![Contact force between the mass and the beam](validation/cantilever_moving_mass/cantilever02_comsol2.png)

