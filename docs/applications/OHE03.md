\section app_ohepanto Multispan OHE with moving 3-dof pantograph

Description
===========

5-span OHE with a 3-dof pantograph moving at a speed of 50 m/s.

Setup
==========

![Problem setup](/validation/OHE_5span_3dof/5span_diagram.png)


Simulation inputs and parameters
=======================

Problem parameters
------------------

Detailed description will be added soon.


Solving in PantograFE
--------------------

Inside the main directory open terminal and run the following in the terminal to get to build the examples in the validation cases:
\code{.sh}
cd validation/linear-models/
mkdir -p build
cd build
cmake ../
make -j
\endcode

To solve this example we need to get into ohe-panto directory:
\code{.sh}
cd ohe-panto
\endcode

Save the parameters.json file with the following contents:
\code{.json}

{
    "catenary cable": {
	"EI": 131.7,
	"height": 1.2,
	"line density": 1.08,
	"span": 55.0,
	"tension": 16000.0,
	"gravity": 9.81
    },

    "contact wire": {
	"EI": 195.0,
	"height": 0.0,
	"line density": 1.35,
	"span": 55.0,
	"tension": 22000.0,
	"gravity": 9.81
    },

    "droppers": {
	"EA": 200000.0,
	"line density": 0.117,
	"catenary cable clamp mass": 0.195,
	"contact wire clamp mass": 0.165,
	"gravity": 9.81
    },

    "dropper schedule": {
	"dropper positions": [4.5, 10.25, 16.0, 21.75, 27.5, 33.25, 39.0, 44.75, 50.5],
	"encumbrance": 1.2,
	"nominal lengths": [1.017, 0.896, 0.810, 0.758, 0.741, 0.758, 0.810, 0.896, 1.017],
	"number of droppers": 9,
	"target sag": [-0.0, -0.024, -0.041, -0.052, -0.055, -0.052, -0.041, -0.024, -0.0]
    },

    "contact wire suspension spring": {
	"stiffness": 2672.0,
	"mass": 0.94,
	"gravity": 9.81
    },

    "catenary cable suspension spring": {
	"stiffness": 500000.0,
	"mass": 0.0,
	"gravity": 9.81
    },

    "pantograph 3_6m": {
	"xinit": 6.0,
	"zbase": -0.15,
	"speed": 50.0,
	"force": 100.0,
	"m1"   : 6.0,
	"k1"   : 160.0,
	"c1"   : 0.0,
	"m2"   : 9.0,
	"k2"   : 15500.0,
	"c2"   : 0.0,
	"m3"   : 7.5,
	"k3"   : 7000.0,
	"c3"   : 0.0
    }

}
\endcode


Save the sim_options.json file with the following contents:

 \code{.json}
{
    "hht3_5span": {
	"parameters file"   : "parameters.json",
	"num spans"         : 5,
	"pantograph type"   : 3,
	"pantograph tag"    : "pantograph 3_6m",
	"num elements"      : 40,
	"time integrator"   : "hht alpha",
	"alpha"             : -0.2,
	"time step"         : 0.0025,
	"num time steps"    : 2151,
	"output directory"  : "output_5span"
    }
}
  \endcode

To solve the moving panto on multispan OHE example run the following:
\code{.sh}
./ohe_panto -i sim_options.json -s hht3_5span
\endcode

As specified in the sim_options.json the results will be generated in the output_5span directory. 
Output file name | Description
--- | ---
con-0.dat | initial profile of the contact wire with static uplift force.
con-\[i\].dat | contact wire profile at i<SUP>th</SUP> time-step.
cat-\[i\].dat | catenary wire profile at i<SUP>th</SUP> time-step.
dro-\[i\].dat | dropper end-positions at i<SUP>th</SUP> time-step.
panto-\[i\].dat | ?? at i<SUP>th</SUP> time-step.
history.dat | Contact force, displacement, velocity and acceleration history of the moving point. 

To visualise (say) the contact forces vs time, run the following:

\code{.sh}
gnuplot
plot "output_5span/history.dat" u 1:2 w l
exit
\endcode

