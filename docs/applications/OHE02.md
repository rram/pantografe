\section app_ohemovl OHE with moving load

Description
===========

Single span OHE with a point load F=100 N moving at a speed of 50.0 m/s.

Setup
==========

![Problem setup](validation/OHE_moving_load/OHE_moving_load_diagram.png)


Simulation inputs and parameters
=======================

Problem parameters
------------------

Detailed description of the problem parameters can be found here \ref ohemovl


Solving in PantograFE
--------------------

Inside the main directory open terminal and run the following in the terminal to get to the application directory:
\code{.sh}
cd applications/ohe-movingload/
\endcode
Run the following command to check if there is a build directory:
\code{.sh}
ls
\endcode
If there is no build directory then run the following (otherwise skip this step):
\code{.sh}
mkdir build
\endcode
Get into the build directory with the following command:
\code{.sh}
cd build/
\endcode
If you are using this application for the first time run the following (otherwise skip):
\code{.sh}
cmake ../
\endcode
Run the following command to build the application:
\code{.sh}
make
\endcode

Save the parameters.json file with the following contents:
\code{.json}
{
    "catenary cable": {
	"EI": 131.7,
	"height": 1.2,
	"line density": 1.08,
	"span": 55.0,
	"tension": 16000.0,
	"gravity": 9.81
    },

    "contact wire": {
	"EI": 195.0,
	"height": 0.0,
	"line density": 1.35,
	"span": 55.0,
	"tension": 22000.0,
	"gravity": 9.81
    },

    "droppers": {
	"EA": 200000.0,
	"line density": 0.117,
	"catenary cable clamp mass": 0.195,
	"contact wire clamp mass": 0.165,
	"gravity": 9.81
    },

    "dropper schedule": {
	"dropper positions": [4.5, 10.25, 16.0, 21.75, 27.5, 33.25, 39.0, 44.75, 50.5],
	"encumbrance": 1.2,
	"nominal lengths": [1.017, 0.896, 0.810, 0.758, 0.741, 0.758, 0.810, 0.896, 1.017],
	"number of droppers": 9,
	"target sag": [-0.0, -0.024, -0.041, -0.052, -0.055, -0.052, -0.041, -0.024, -0.0]
    },

    "contact wire suspension spring": {
	"stiffness": 2672.0
    },

    "catenary cable suspension spring": {
	"stiffness": 500000.0
    },

    "moving load": {
	"xinit" : 1.0e-4,
	"zbase" : -0.15,
	"speed" : 50.0,
	"force" : 100.0
    }
}
\endcode


Save the sim_options.json file with the following contents:

 \code{.json}
{
    "p0": {
	"parameters file" : "parameters.json",
	"load tag"        : "moving load",
	"num spans"       : 1,
	"num elements"    : 40,
	"time integrator" : "generalized alpha",
	"spectral radius" : 0.8,
	"time step"       : 0.002,
	"num time steps"  : 550,
	"output directory": "output"
    }
}
  \endcode

Run the following in the terminal:
\code{.sh}
./ohe_movingload -i sim_options.json -s p0
\endcode

As specified in the sim_options.json the results will be generated in the output directory. 
Output file name | Description
--- | ---
con-0.dat | initial profile of the contact wire
history.dat | displacement, velocity and acceleration history of the moving point. 

