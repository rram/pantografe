
cmake_minimum_required(VERSION 3.19)

project(pantograFE
  VERSION 1.0
  DESCRIPTION "OHE-Pantograph interactions"
  LANGUAGES CXX)

# Set runtime path for Linux
set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/${PROJECT_NAME}/lib")
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

# Build options
option(PFE_BUILD_DOCS "Build documentation" OFF)
option(PFE_BUILD_TESTS "Run unit tests" ON)

# Dependencies
find_package(PkgConfig REQUIRED)
pkg_search_module(PETSC REQUIRED PETSc)
find_package(LAPACK REQUIRED)
pkg_search_module(GTKMM REQUIRED gtkmm-3.0)
pkg_search_module(FFTW REQUIRED fftw3)
if(${PFE_BUILD_DOCS})
	find_package(Doxygen REQUIRED)
endif()

# desired compile features
list(APPEND pfe_COMPILE_FEATURES cxx_std_17)

# Compile libraries in src
add_subdirectory(external)
add_subdirectory(src)  

if(PFE_BUILD_TESTS)
	include(CTest)
	add_subdirectory(src/core/element/test)
	add_subdirectory(src/core/physics/test)
	add_subdirectory(src/core/time/test)
	add_subdirectory(src/component/suspension_spring/test)
	add_subdirectory(src/component/beam/test)
	add_subdirectory(src/component/dropper/test)
	add_subdirectory(src/component/pantograph/test)
	add_subdirectory(src/linear_models/static_ohe/test)
	add_subdirectory(src/nonlinear_models/static_ohe/test)
	add_subdirectory(src/linear_models/moving_load/test)
	add_subdirectory(src/linear_models/ohe_panto/test)
	add_subdirectory(src/nonlinear_models/ohe_damping/test)
	add_subdirectory(src/nonlinear_models/time/test)
	add_subdirectory(src/nonlinear_models/moving_load/test)
	add_subdirectory(src/nonlinear_models/ohe_panto/test)
	add_subdirectory(src/application/test)
	add_subdirectory(src/gui/output/test)
	add_subdirectory(src/gui/input/test)
endif()

# Install
include(GNUInstallDirs)
include(CMakePackageConfigHelpers)

install(TARGETS
	pfe_ext
	pfe_time_contact
	pfe_time
	pfe_nonlinear_time
	pfe_dyn_state
	pfe_assembly
	pfe_petsc
	pfe_physics
	pfe_element
	pfe_core
	pfe_linear_ohe
	pfe_nonlinear_ohe
	pfe_beam
	pfe_pantograph
	pfe_dropper
	pfe_susp_spring
	pfe_contact_spring
	pfe_component
	pfe_linear_model_static_ohe
	pfe_linear_model_moving_load
	pfe_linear_model_ohe_panto
	pfe_nonlinear_model_static_ohe
	pfe_nonlinear_model_moving_load
	pfe_nonlinear_model_ohe_panto
	pfe_ohe_damping
	pfe_ohe_base
	pfe_application
	pfe_gui_output
	pfe_gui_input
	EXPORT ${PROJECT_NAME}Targets
  	DESTINATION ${PROJECT_NAME}/${CMAKE_INSTALL_LIBDIR}
  	INCLUDES DESTINATION ${PROJECT_NAME}/${CMAKE_INSTALL_INCLUDEDIR})

install(EXPORT ${PROJECT_NAME}Targets
  	  DESTINATION ${PROJECT_NAME}/${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
  	  NAMESPACE pfe::
  	  FILE ${PROJECT_NAME}Targets.cmake)

set(PFE_INCLUDE_DIRS_VAR ${PROJECT_NAME}/${CMAKE_INSTALL_INCLUDEDIR})
set(PFE_LIBRARY_DIRS_VAR  ${PROJECT_NAME}/${CMAKE_INSTALL_LIBDIR})

configure_package_config_file(
  "cmake/${PROJECT_NAME}Config.cmake.in" "${PROJECT_NAME}Config.cmake"
  INSTALL_DESTINATION
  ${PROJECT_NAME}/${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
  PATH_VARS PFE_INCLUDE_DIRS_VAR PFE_LIBRARY_DIRS_VAR)

write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake
  VERSION ${PACKAGE_VERSION}
  COMPATIBILITY AnyNewerVersion)

install(FILES
  "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
  "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake"
  DESTINATION "${PROJECT_NAME}/${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}")


if(PFE_BUILD_DOCS)
	add_subdirectory(docs)
endif()
